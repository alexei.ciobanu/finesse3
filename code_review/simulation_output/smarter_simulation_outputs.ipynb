{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Smarter simulation outputs in Finesse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One of my frustrations with Finesse < 3 is that the output is always just numbers. For example, the output file column names are just the names of the detectors, but don't tell you how the data was produced, e.g. which mirror(s) had signals applied, or which noise (e.g. noise from open port `p1`) dominates at a readout. In principle, all of this information is available in Finesse when it builds the output file, but only some of this is dumped into the output file in the comments. Furthermore, all transfer functions and noise are always calculated, but only those that you ask to plot in your `kat` script are dumped. That means if you want to look at another output, you have to edit the file and run it again - and this is especially frustrating for long-running scripts. In Finesse 3, why not store this information in the output in case the user wants it later?\n",
    "\n",
    "I propose that we add some mechanism for Finesse 3 to produce an output *object* - I call it a \"solution\" in Zero - that contains the simulation results but also those results' metadata all in one place. This would:\n",
    "\n",
    "  - Solve the problem of having to rerun Finesse to look at different sensors.\n",
    "  - Still support the traditional mechanism of dumping data into a file, and the \"just plot it without any BS\" approach\n",
    "  - Open up the possibility of doing cool new stuff with the results, like:\n",
    "    - Transforming units.\n",
    "    - Plotting multiple simulation results together.\n",
    "    - Comparing the difference between old and new results.\n",
    "    - Smarter plotting tools:\n",
    "      - Bode, spectral density, phasor etc. plot methods built-in with sensible defaults (e.g. dB scaling, 20 dB/decade tick spacing, etc.) (wouldn't it be cool to be able to generate phasor plots for ports!?)\n",
    "      - Having some mechanism to group unimportant noise sources together in one sum curve.\n",
    "\n",
    "I have implemented some of this behaviour in Zero already, so here I will explain how it works, what we might want to copy to Finesse, and what we might want to do differently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Zero high level overview"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zero operates in a modular way where the definition of the circuit, the analysis to perform on the circuit, and the results from that analysis are separate modules.\n",
    "\n",
    "The `Circuit` module defines the circuit. This is much like the `Model` class in Finesse 3.\n",
    "\n",
    "The `Analysis` module implements certain types of calculation to perform with the circuit, such as signal and noise analyses. These take as input a `Circuit` object and outputs a `Solution` object (see below).\n",
    "\n",
    "The `Solution` module provides a class to hold the results from analyses. Right now I have only one `Solution` class for the results from AC small signal or noise analyses, but in the future I'll probably add more to hold e.g. DC or stability analysis results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analyses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Analysis` objects do the following:\n",
    "\n",
    "  * Extract the relevant parameters required for the analysis (e.g. resistances) from the `Circuit`\n",
    "  * Use these parameters to build the matrix to be solved.\n",
    "  * Solve the matrix.\n",
    "  * Build the `Solution` intelligently, using what it knows about the `Circuit`.\n",
    "  * Performs any requested post-processing of the solution, such as projection of noise to another\n",
    "    node or component in the circuit.\n",
    "  \n",
    "Here is a stripped down version of the `AcNoiseAnalysis` class (see [this](https://git.ligo.org/sean-leavey/zero/blob/28467cc2cd7248222a0ac5e28c8ac5bef58b6ee9/zero/analysis/ac/noise.py) for the full file). I've added some extra comments to try to describe what's happening. It's in roughly the right order to understand what's happening."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class AcNoiseAnalysis(AcSignalAnalysis): # For technical reasons, the noise analysis subclasses the signal analysis.\n",
    "    \"\"\"Small signal circuit analysis\"\"\"\n",
    "    def __init__(self, *args, **kwargs):\n",
    "        super().__init__(*args, **kwargs)\n",
    "        # Noise analyses are performed at a single node or component where the noise is to be\n",
    "        # calculated - I call this the noise \"sink\".\n",
    "        self._noise_sink = None\n",
    "\n",
    "    # This function performs the analysis and outputs the Solution object.\n",
    "    def calculate(self, input_type, sink, impedance=None, incoherent_sum=False, input_refer=False,\n",
    "                  **kwargs):\n",
    "        \"\"\"Calculate noise from circuit elements at a particular element.\n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        input_type : str\n",
    "            Input type, either \"voltage\" or \"current\".\n",
    "        sink : str or :class:`.Component` or :class:`.Node`\n",
    "            The element to calculate noise at.\n",
    "        impedance : float or :class:`.Quantity`, optional\n",
    "            Circuit input impedance. If None, the default is used.\n",
    "        incoherent_sum : :class:`bool`, optional\n",
    "            Incoherent sum specification. If True, the incoherent sum of all noise in the circuit at\n",
    "            the sink is calculated and added to the solution.\n",
    "        input_refer : bool, optional\n",
    "            Refer the noise to the input.\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        :class:`~.solution.Solution`\n",
    "            Solution containing noise spectra at the specified sink (or projected sink).\n",
    "        \"\"\"\n",
    "        # This calls the signal analysis to calculate the transfer matrix from each component or node\n",
    "        # in the circuit to the noise sink specified above. It then passes this to _build_solution\n",
    "        # below which takes this transfer matrix and builds `NoiseDensity` objects which represent\n",
    "        # the noise from each noise source at the noise sink.\n",
    "        self._do_calculate(input_type, impedance=impedance, is_noise=True, **kwargs)\n",
    "        if incoherent_sum:\n",
    "            self._compute_sums(incoherent_sum)\n",
    "        if input_refer:\n",
    "            self._refer_sink_noise_to_input()\n",
    "        return self.solution\n",
    "\n",
    "    # This function builds the individual noise functions from the transfer matrix calculated above\n",
    "    # and adds them to the solution.\n",
    "    #\n",
    "    # Incidentally, by avoiding direct matrix multiplications with e.g. noise vectors here, we can\n",
    "    # allow many noise sources per component or node to be present. This is useful in the case of\n",
    "    # op-amps which have both current and voltage noise at their nodes. Keeping these as separate\n",
    "    # noise sources also allows us to plot them separately in the solution.\n",
    "    def _build_solution(self, noise_matrix):\n",
    "        empty = []\n",
    "        # Loop over all noise sources in the circuit.\n",
    "        for noise in self._current_circuit.noise_sources:\n",
    "            # Get this noise source's spectral density.\n",
    "            spectral_density = noise.spectral_density(frequencies=self.frequencies)\n",
    "\n",
    "            if np.all(spectral_density) == 0:\n",
    "                # This is a null noise source.\n",
    "                empty.append(noise)\n",
    "\n",
    "            # Get the relevant matrix index for this noise source.\n",
    "            if noise.element_type == \"component\":\n",
    "                index = self.component_matrix_index(noise.component)\n",
    "            elif noise.element_type == \"node\":\n",
    "                index = self.node_matrix_index(noise.node)\n",
    "            else:\n",
    "                raise ValueError(\"unrecognised noise source present in circuit\")\n",
    "\n",
    "            # Get transfer function from this element to the noise sink.\n",
    "            response = noise_matrix[index, :]\n",
    "            # Multiply transfer function by noise entering at the element.\n",
    "            projected_noise = np.abs(response * spectral_density)\n",
    "            # Create a data series.\n",
    "            series = Series(x=self.frequencies, y=projected_noise)\n",
    "            # Create and add the NoiseDensity to solution.\n",
    "            self.solution.add_noise(NoiseDensity(source=noise, sink=self.noise_sink, series=series))\n",
    "\n",
    "        if empty:\n",
    "            # Print log message specifying empty noise sources.\n",
    "            empty_sources = \", \".join([str(response) for response in empty])\n",
    "            LOGGER.debug(f\"empty noise sources: {empty_sources}\")\n",
    "\n",
    "    # This function takes the noise computed at the noise sink and projects it to the input.\n",
    "    # This gives you the input-referred noise which is useful for e.g. calculating the effective\n",
    "    # circuit noise in terms of photocurrent in photodetectors.\n",
    "    #\n",
    "    # It takes advantage of the linear algebraic operators implemented in the NoiseDensity and\n",
    "    # Response objects produced by _build_solution() above. It simply finds the NoiseDensity\n",
    "    # produced above, runs a separate AcSignalAnalysis to compute the response from the input\n",
    "    # to the noise sink, then multiplies the NoiseDensity by the Response's inverse. The\n",
    "    # resulting object is then a NoiseDensity with an updated noise sink (the input).\n",
    "    def _refer_sink_noise_to_input(self):\n",
    "        \"\"\"Project the calculated noise to the input.\"\"\"\n",
    "        # Create a signal analysis using the same settings as this noise analysis.\n",
    "        projection_analysis = self.to_signal_analysis()\n",
    "        # Perform the signal analysis.\n",
    "        projection = projection_analysis.calculate(frequencies=self.frequencies)\n",
    "        # Grab the response from the circuit input to the noise sink.\n",
    "        input_response = projection.get_response(source=input_element, sink=self.noise_sink)\n",
    "        # Replace the sink noise in the solution by the input noise.\n",
    "        for __, noise_spectra in self.solution.noise.items():\n",
    "            for noise in noise_spectra:\n",
    "                self.solution.replace(noise, noise * input_response.inverse())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions are wrappers around `NumPy` objects that hold the data produced by the `Analysis` and its metadata. They:\n",
    "\n",
    "  - Hold the frequency axis and data for the function they represent (e.g. a noise spectral density).\n",
    "  - Know their source and sink units.\n",
    "  - Can plot themselves to a figure.\n",
    "  - Can transform and return their data in various ways, e.g. dB scaled magnitude and phase in degrees or radians (`Response`) or power or amplitude spectral density (`NoiseDensity`)\n",
    "  - Can be scaled, multiplied, etc. smartly. For instance, a `NoiseDensity` can be multiplied by a `Response` to project noise to somewhere else (e.g. scale it to meters), and a `NoiseDensity` can be added to another `NoiseDensity` in quadrature.\n",
    "  - Do not store data provenance - too complicated to get right and probably not that useful for quick and dirty simulations.\n",
    "  \n",
    "All functions have one or more sources and a single sink. In the case of a `Response` (a single-source, single-sink function), the source and sink are components or nodes, and the data is the complex response from the source to the sink. In the case of a `NoiseDensity`, the source is one or many `Noise` sources (these are objects too - representing the type of noise, e.g. Johnson noise, op-amp voltage or current noise, etc.), and the sink is a component or node (the noise node in the `AcNoiseAnalysis`).\n",
    "\n",
    "In order to keep data containers decoupled from the analysis and circuit, sources and sinks only have to be strings. This allows them to be serialised and unserialised without having to save the full circuit itself.\n",
    "\n",
    "Data containers support various mathematical operations by overloading their operators. For instance, a `NoiseDensity` can be multiplied by a `Response`, resulting in another `NoiseDensity`. This lets you perform noise projections like the `input_refer` flag of the `AcNoiseAnalysis` above, but it also lets you perform studies with other, non-simulated responses or noise - you can for instance create a bunch of transfer functions representing the rest of a control loop and calculate the open loop gain of the whole loop, just with these functions and some algebra.\n",
    "\n",
    "Units in functions are just stored as strings. There is no smart unit algebra going on with e.g. fractions - if unit strings don't match identically, they are considered different. The operator overloading above checks that the units are compatible before allowing an operation - so when multipliying a noise by a response, the noise sink unit must be the same as the response source unit.\n",
    "\n",
    "Here is an example of the `Response` data container ([original file](https://git.ligo.org/sean-leavey/zero/blob/28467cc2cd7248222a0ac5e28c8ac5bef58b6ee9/zero/data.py)):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Response(SingleSourceFunction, SingleSinkFunction):\n",
    "    \"\"\"Data set representing a response at a sink from a source.\"\"\"\n",
    "    @property\n",
    "    def complex_magnitude(self):\n",
    "        return self.series.y\n",
    "\n",
    "    @property\n",
    "    def magnitude(self):\n",
    "        \"\"\"Absolute magnitude.\"\"\"\n",
    "        return np.abs(self.complex_magnitude)\n",
    "\n",
    "    @property\n",
    "    def db_magnitude(self):\n",
    "        return db(self.magnitude)\n",
    "\n",
    "    @property\n",
    "    def phase(self):\n",
    "        \"\"\"Phase in degrees.\"\"\"\n",
    "        return np.angle(self.complex_magnitude) * 180 / np.pi\n",
    "\n",
    "    # This methods is used for comparing Solutions to each other. It inspects the specified\n",
    "    # other Response to see if it has the same data as this one.\n",
    "    def series_equivalent(self, other):\n",
    "        \"\"\"Checks if the specified function has an equivalent series to this one.\"\"\"\n",
    "        return vectors_match(self.magnitude, other.magnitude)\n",
    "\n",
    "    # These methods allow the function to draw itself to a figure.\n",
    "    def _draw_magnitude(self, axes, label_suffix=None, scale_db=True):\n",
    "        \"\"\"Add magnitude plot to axes\"\"\"\n",
    "        label = self._format_label(tex=True, suffix=label_suffix)\n",
    "        if scale_db:\n",
    "            axes.semilogx(self.frequencies, self.db_magnitude, label=label, **self.plot_options)\n",
    "        else:\n",
    "            axes.loglog(self.frequencies, self.magnitude, label=label, **self.plot_options)\n",
    "    def _draw_phase(self, axes):\n",
    "        \"\"\"Add phase plot to axes\"\"\"\n",
    "        axes.semilogx(self.frequencies, self.phase, **self.plot_options)\n",
    "    def draw(self, *axes, **kwargs):\n",
    "        if len(axes) != 2:\n",
    "            raise ValueError(\"two axes (magnitude and phase) must be provided\")\n",
    "        self._draw_magnitude(axes[0], **kwargs)\n",
    "        self._draw_phase(axes[1])\n",
    "\n",
    "    # This function smartly formats the label for the plot using the source and sink.\n",
    "    def _format_label(self, tex=False, ignore_user_label=False):\n",
    "        if not ignore_user_label and self._label is not None:\n",
    "            return self._label\n",
    "        if tex:\n",
    "            format_str = r\"$\\bf{%s}$ to $\\bf{%s}$ (%s)%s\"\n",
    "        else:\n",
    "            format_str = \"%s to %s (%s)%s\"\n",
    "        return format_str % (self.source.label, self.sink.label, self.unit_str)\n",
    "\n",
    "    # This property produces a string contains the function's units.\n",
    "    @property\n",
    "    def unit_str(self):\n",
    "        if self.sink_unit is None and self.source_unit is None:\n",
    "            return \"dimensionless\"\n",
    "        if self.sink_unit is None:\n",
    "            return f\"1/{self.source_unit}\"\n",
    "        if self.source_unit is None:\n",
    "            return self.sink_unit\n",
    "        return f\"{self.sink_unit}/{self.source_unit}\"\n",
    "\n",
    "    def __mul__(self, other):\n",
    "        if isinstance(other, Response):\n",
    "            other_sink = other.sink\n",
    "            other_value = other.series\n",
    "            if self.sink_unit != other.source_unit:\n",
    "                raise ValueError(f\"Cannot multiply this response by {other}: the sink unit of this \"\n",
    "                                 f\"response, {self.sink_unit}, is incompatible with the source \"\n",
    "                                 f\"unit of {other}, {other.source_unit}.\")\n",
    "        elif isinstance(other, Number):\n",
    "            other_sink = self.sink\n",
    "            other_value = other\n",
    "        else:\n",
    "            # E.g. NoiseDensityBase.\n",
    "            raise TypeError(f\"Cannot multiply {self.__class__.__name__} by \"\n",
    "                            f\"{other.__class__.__name__}.\")\n",
    "        return self._new_response(other_sink, self.series * other_value)\n",
    "\n",
    "    def __rmul__(self, other):\n",
    "        # Note: \"other\" should never be another Response or NoiseDensityBase, since these functions\n",
    "        # implement the corresponding __mul__ methods for Response.\n",
    "        if not isinstance(other, Number):\n",
    "            raise TypeError(f\"Cannot multiply {other.__class__.__name__} by \"\n",
    "                            f\"{self.__class__.__name__}.\")\n",
    "        # Response-scalar multiplication is commutative.\n",
    "        return self * other\n",
    "\n",
    "    def __truediv__(self, other):\n",
    "        if not isinstance(other, Number):\n",
    "            raise TypeError(f\"Cannot divide {self.__class__.__name__} by \"\n",
    "                            f\"{other.__class__.__name__}. To invert, call inverse().\")\n",
    "        return self._new_response(self.sink, self.series * 1 / other)\n",
    "\n",
    "    def __rtruediv__(self, other):\n",
    "        if not isinstance(other, Number):\n",
    "            raise TypeError(f\"Cannot divide {other.__class__.__name__} by \"\n",
    "                            f\"{self.__class__.__name__}. To invert, call inverse().\")\n",
    "        return self._new_response(self.sink, other / self.series)\n",
    "\n",
    "    def inverse(self):\n",
    "        \"\"\"Inverse response.\"\"\"\n",
    "        return self.__class__(source=self.sink, sink=self.source, series=self.series.inverse())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solutions:\n",
    "\n",
    "  - Contain many `Function`s.\n",
    "  - Don't store the circuit or analysis that produced those functions.\n",
    "  - Can be queried for their functions, e.g. `get_noise(source=\"r1\", sink=\"nout\")`\n",
    "  - Can plot some or all of their functions together.\n",
    "  - Can have default functions that are displayed when `plot()` is called (can be used to support Finesse 2 scripts which define their plot outputs)\n",
    "  - Can be compared to other solutions to check for equivalency.\n",
    "  - Can be *combined* with other solutions.\n",
    "  - Can serialise their data.\n",
    "  \n",
    "In Zero, solution combination and comparison is used to check results against LISO. There is a separate LISO output file parser that builds a `Solution` to look the same way as one produced by a `Zero` analysis. In the Zero CLI I have a `--compare` flag that will show the results from Zero and LISO overlaid. Under the hood, I just obtain both `Solution` objects, add them together (`solution = sol_zero + sol_liso`) and plot them. The `Solution` has a group mechanism that plots the results from each tool with different line styles so you can quickly see the difference.\n",
    "\n",
    "There is scope here to do smart plotting of the solution data. We could for example let the user ask for only the `N` most important noise sources contributing to the noise at the detector, and group the rest into a \"Other noise\" curve on the plot."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is an example of solution combination. This shows a circuit with two different inputs - one for RF frequencies and one for low frequencies. The data originally came from two separate solutions that modelled each input to the same output, then I combined them into one and plotted it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circuit = ... # build circuit here\n",
    "lf_solution = AcSignalAnalysis(circuit).calculate(input=\"n_lf\")\n",
    "rf_solution = AcSignalAnalysis(circuit).calculate(input=\"n_rf\")\n",
    "solution = lf_solution + rf_solution\n",
    "solution.plot_response(sources=[\"n_lf\", \"n_rf\"], sink=\"nout\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![caption](solution-combination.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Potential Finesse implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are a lot of potential overlaps between Zero's processes and Finesse's, at least for small signal AC analyses. For other types of analysis, like DC operating point or beam tracing, we probably need different types of function and solution containers to provide appropriate ways to extract useful information."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finesse deals with very similar data objects: responses (transfer functions) and noise. I expect these could be implemented in the same form quite easily."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Analyses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We should remember that there are multiple types of analysis in Finesse, some not AC, e.g. the DC operating point or a DC mirror sweep. That probably entails we need different types of `Solution` to hold different classes of analysis - e.g. an `ACSolution` for data with frequency components, an `OperatingPoint` solution with the DC powers in each field in the interferometer, a `TraceSolution` containing the beam parameters at each point and with methods to compute e.g. beam sizes, cavity finesses, etc. Some analyses may require as their input other analyses, e.g. the `ACSolution` will require the `OperatingPoint` and `TraceSolution`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As mentioned above, there will probably have to be multiple types of solution depending on the type of analyis performed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### AC solutions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "AC solutions in Finesse (i.e. small signal AC transfer function calculations) would be somewhat similar to Zero's solutions. It would have a frequency axis, but you could also apply a drive and probe matrix to easily map degrees of freedom to readouts. Given that some people might not want to specify drive and probe matrices, we may want to have some hierarchy of `ACSolution` classes that apply progressively more post-processing to the analysis results:\n",
    "\n",
    "  - A `BaseACSolution` akin to the one presented above, that holds all of the *individual* response and noise functions from one port to another port.\n",
    "  - A `DrivePostProcess(BaseACSolution)` that applies a drive matrix to the sources of the `BaseACSolution`, thus mapping a collection of many drives (e.g. `0.5 * EMX` and `-0.5 * EMY` for `DARM`) into single functions.\n",
    "  - A `ProbePostProcess(BaseACSolution)` that applies a probe matrix to the sinks of the `BaseACSolution`, thus mapping a collection of many readouts (e.g. `0.5 * BHDA` and `-0.5 * BHDB` for balanced homodyne readout) into single functions.\n",
    "  - A `DriveProbePostProcess(DrivePostProcess, ProbePostProcess)` that does both of the above. I expect this would be the analysis most frequently used by end users.\n",
    "  \n",
    "The `Analysis` could do all of this mapping on behalf of the user. They would need to provide a `probe` and `drive` map in the case of the last three, but the analysis would do the rest transparently."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Other solution types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, for the results of other types of analysis, such as the trace results of a circuit, the above solution container with its frequency axis is not appropriate. We could build different types of `Solution` that don't rely on AC data, but other types of `Function` or data container, but still containing convenience methods for plotting, dumping, quickly getting e.g. the beam parameters at various ports, etc.\n",
    "\n",
    "Here is a rough idea of how the various objects could be structured:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Class graph](class-graph.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Link to draw.io graph](https://www.draw.io/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Finesse.drawio#R7VxZl9o2GP01PCbHtrzA4wwMzUPa5JSeNnnqEbYAN8Yishggv76SLeFFNphlEAzMQ2J%2FlrXdb9OVTAf05%2BvfCFzMfscBijqWEaw7YNCx2J%2Fhsf%2B4ZJNJTMNxM8mUhIGQ5YJR%2BAvJgkK6DAOUlApSjCMaLspCH8cx8mlJBgnBq3KxCY7KrS7gFCmCkQ8jVfpPGNBZJu1abi7%2FhMLpTLZsur3syRj6P6YEL2PRXscCk%2FQvezyHsi4x0GQGA7wqiMBLB%2FQJxjS7mq%2F7KOKTK6cte2%2FY8HTbb4Ji2uaFl59PFjT%2F%2FjP%2B1%2F46dH4QZ2QGH0xDjCahGzkjKGATJG4xoTM8xTGMXnLpczpqxOs12V1e5jPGCyH8D1G6EWjDJcVMNKPzSDydhFHUxxEmaYtg0vWR7zN5Qgn%2BgQpPxl3HdoztE4kQm7znrN%2B8s40TIkQJXhJflLKEikEyRXTnxHS3GDHlR3iOKNmwNwmKIA1fy41CoYTTbbkcCHYhsKjHRfToFUZLJDXJjVjfnhclXNyfS64qz3PW9TDugCf21Fis2b%2FpZBiZ%2FAPlEPBnduEZRWv6AUbhVLznsylCpPw4QD4mbGhYlOEQkyiMUd40u5qK%2F9MOclDiqZR%2BWSD%2BPhcYCxwyEESxMam%2ByOak%2FG7hwaIqmymvB%2BGrFA3DOEhYg4N%2BoYrC85pXcN7Plm%2FsH03p%2FYo9vSJCQ%2BZwnrL5H8zDIEjNSAAyEGiwnrGikyh1EtxCuKHgmAorMi1xP4TzMOLu9hOKXhGvumxcZetMaxCdMRVDAqoxvrjDYb%2FfaEx8NGhdEKkmIoODJ2KDCA0WED51lftZU4aBWcHH9ozTrareqGVrl%2FV2qu863P8d5%2B3A1Xm7%2BuYt54FL%2FcTYenFx7x4X60IAACUNEMl2Zf5Xs5Ci0QKmXV6xpLwukDSGnIOjwXB4pmgATLMUDYChBINeTSxwzxAL5IqklGLtUGsUjfGqqNGpgD2YYRL%2BYjMIowNzVbQO6Td2bYjr7%2Fz6oyPuBuvCo8FG3DTpPIOAbL7lFfDb77ICfpNXl97J%2Bs6cHlu1FqbCXwz2Rg3AW2FrixJtfM2Ssq1%2BGRX9qmpONkzxVnGdVKnI2ldRNjlKRU%2BEwE2hWJo0Js0dthy73I5tHNWvpvLAKpe3jT31O5X6e6Xy7CIbYW5yW5RPiDCGlsh%2FrA3pi0y7rPCE6FSvCma3rApOlh6e3QSq7dhW9xIq591XUrPbgbdw9e6FUqD65m0tDFXC5oU%2BcX6R51URTJLQl%2BJhujZvQFUPRrbe9ZtjKBnWiGKCEgU6TjVV6AqUhL%2FgOC1gSLIl7aHz3HEGB6W3ERyj6HlLzBatK6NmJT5K3rolmEVPOlvetojcLg1tzHyNj6Dn9UqOzjwt5ZFF8GSSIKrAdxYn2UxLjiXTNeiPcLRMGcOcCRs38mBlm6xbxzQzxAFE3Umtc3T9LhrvBFYxvUakzF4lJeuJuFdMYq03WqU04KCuC2sISgWaz2FCq6zoeB9vub%2FaBV4hkpy3TkjP3UfCc45hjPm%2B0i7Sdm9jLZbeBZWFySLboJqEa67mZ9FIu6yR20ysoJGerSqklL2BQtYtpisQfI0w175khdCilWvQMLOWWG5dz8yau1mJe8lLu21zHrMB4AvlPLKjt79x98RmwBjNYTroEWsLRpfcukubpwTGyYQPraXfnyxjn4%2BZ1zAheH7AZl4aHwxmCHyfMr25v529k3ynbVeoIlMlcy%2B9s2frcJ7vhUeS3HwLpwu0Ol1Ty4bUHcLs6IVZE%2Bej7NscCfzNwOydCHM9r%2Bt6Ff74jbZQ3MqeorNnC8Wzu7vKvxHfDO7scMOpSglOPfNwlC7ZVV0yL6EbstFdq%2Bmn90azOZWdRtutSR8vS7OZx9Fsgujgq4jiiqJ%2BPXMGbquw6LltXsvuuWVr81QVuDD74pyiATEOE3RerNNZJ5APNtex20bd6XnXhrp3AupnRRvFSciiW0g3t46yV42k2lGWh%2BvuOwOTEfXqmVVL3U2%2BZWb1j2JsuBihWo1Iu%2F1RGmJgmzDz4ENVj1ddh14BH2pdCYOi2eXZbV2epfcADbAecB0Cl%2BbvVcAjoTjMugytcMmO3n5C8YzgPGMXfHRUAnFj44XMPjZJuJvvOFP69Bef1KQp1VGKh3y4E0S4kuY8zyNnanuyqUICmrb2nAmYCjr36NVbH8ABer26YyleveHQ8b2cF28PnaN5hd98dmpccsfvbOfDLfs8q6t94wM0cy1Vjnuc5R4JNwH2AmcaFgQv4BQWEdrHWurnJ%2B0yCQ26ug9%2BgscHSdtt31ZxR%2B8HSeBBrRwGl961uv3I6g6Cy9ab1YHmtfo2igwI68efCAZ4Sd9ZhgAqyyK3ZvPs0ilC%2By8%2BBl%2BGHXkOguAx6hQPQhiTZXzQ4QX9qYLVK6Phad%2FKBM2Z81UcUyCZVd76HrZtelcGvN2cqD9OKhyJsiN5%2FOtBWT2Hpo%2FTyE8%2Fd4pnn%2FOj0K2PvbdkR86Z8NhW6%2FxU729syY4WIB8Wvm0aI7pCKM6Ca%2FZda%2BrDDRgHMsyq2nGoMfl4zhEp%2FFDQiR9Slz%2BudCXxUTSsbo1hdd9sltXjfXdOFtqtVwSajwPZ6oqg3kCEZQzlF37CQrK76zIQD5QNxHa0G4iaWA5DFPEDRUP52X%2BW4iHoz9LZDtC1TSvogY9OeWLl2T9tEyuRLUys2MVl44Lp5uEtTO2Hiku3vDfTWHab%2Fyx29qlF%2FuPj4OV%2F)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Scribbles from telecon:\n",
    "\n",
    "\"Model\" is the node graph. Model parameters are a property of every analysis.\n",
    "\n",
    "\"Model\" should be \"Simulation\", performs whatever analyses it needs to perform\n",
    "\n",
    "Analyses need as input what they should store in their output\n",
    "\n",
    "\n",
    "Diagram changes:\n",
    "\n",
    "Separate Finesse object that talks to the analyses, tells them what to store in their solution outputs\n",
    "\n",
    "Solutions: together in column\n",
    "Analyses together in column\n",
    "\n",
    "Model input to the green column"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Updated diagram:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Class graph](class-graph-updated.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
