"""Setup file"""
from setuptools import setup, find_packages
from Cython.Build import build_ext, cythonize
from Cython.Distutils import Extension
import pkg_resources
import os
import sys
import platform

SYS_NAME = platform.system()

def is_installed(requirement):
    try:
        pkg_resources.require(requirement)
    except pkg_resources.ResolutionError:
        return False
    return True


def get_conda_paths():
    import os
    import sys
    try:
        library = sys.prefix
        #library = os.environ["CONDA_PREFIX"]
        if sys.platform == 'win32':
            library = os.path.join(library, "Library")
        return (os.path.join(library, "include"), os.path.join(library, "lib"))
    except KeyError:
        return (None, None)


if not is_installed('numpy>=1.11.0'):
    print('Error: numpy needs to be installed first. '
          'You can install it via:\n$ pip install numpy',
          file=sys.stderr)
    exit(1)


def ext_modules():
    import numpy as np

    conda_include, conda_lib = get_conda_paths()
    print("Conda include: ", conda_include)
    include_dirs = ['.', np.get_include(), '/usr/include/suitesparse']
    library_dirs = []
    # Stops numpy version warning, cython uses an older API on purpose
    define_macros = [('NPY_NO_DEPRECATED_API', 'NPY_1_7_API_VERSION')]
    extra_compile_args = ['-O3']
    num_jobs = os.cpu_count()

    if conda_include is not None:
        include_dirs.append(conda_include)
        library_dirs.append(conda_lib)
    root_dir = os.path.abspath(os.path.dirname(__file__))
    cmatrix_ext = Extension(
        'cmatrix',
        sources=[root_dir+'/finesse/cmatrix.pyx'],
        libraries=['klu'],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,
        define_macros=define_macros,
        language='c',
    )
    constants_ext = Extension(
        "constants",
        sources=[root_dir+"/finesse/constants.pyx"],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,
        define_macros=define_macros,
        language='c',
    )
    cmath_ext = Extension(
        "cmath.cmath",
        sources=[root_dir+"/finesse/cmath/cmath.pyx"],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,
        define_macros=define_macros,
        language='c',
    )
    # FIXME (sjr) handle openmp properly for different systems
    knm_ext = Extension(
        "knm",
        sources=[root_dir+"/finesse/knm.pyx"],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,# + ['/openmp'] if SYS_NAME == "Windows" else ['-fopenmp'],
        #extra_link_args=['/openmp'] if SYS_NAME == "Windows" else ['-fopenmp'],
        define_macros=define_macros,
        language='c'
    )
    matrixfill_ext = Extension(
        "components.matrixfill",
        sources=[root_dir+"/finesse/components/matrixfill.pyx"],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,# + ['/openmp'] if SYS_NAME == "Windows" else ['-fopenmp'],
        #extra_link_args=['/openmp'] if SYS_NAME == "Windows" else ['-fopenmp'],
        define_macros=define_macros,
        language='c'
    )
    detector_ext = Extension(
        "detectors.compute",
        sources=[root_dir+"/finesse/detectors/compute.pyx"],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,# + ['/openmp'] if SYS_NAME == "Windows" else ['-fopenmp'],
        #extra_link_args=['/openmp'] if SYS_NAME == "Windows" else ['-fopenmp'],
        define_macros=define_macros,
        language='c'
    )
    util_math_ext = Extension(
        "utilities.maths",
        sources=[root_dir+"/finesse/utilities/maths.pyx"],
        include_dirs=include_dirs,
        library_dirs=library_dirs,
        extra_compile_args=extra_compile_args,
        define_macros=define_macros,
        language='c',
    )

    exts = [
        cmatrix_ext,
        constants_ext,
        cmath_ext,
        knm_ext,
        matrixfill_ext,
        detector_ext,
        util_math_ext
    ]

    # annotate flag produces html files showing level of python interaction in each cython file
    return cythonize(exts, annotate=True, language_level=3, nthreads=num_jobs,
                     compiler_directives={
                         "embedsignature" : True,
                         "cdivision" : True
                     })


EXTRAS = {
    'dev': ['sphinx',
            'sphinx_rtd_theme',
            'sphinxcontrib-bibtex',
            'sphinxcontrib-katex',
            'numpydoc']
}

CLASSIFIERS = [
        "Development Status :: 3 - Alpha",
        "Topic :: Scientific/Engineering :: Physics",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
]

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

def read_requirements(fname):
    reqfile = read(fname)
    reqs = []
    for requirement in reqfile.splitlines():
        requirement = requirement.strip()
        if "sphinx" in requirement or requirement == "pylint" or requirement == "numpydoc":
            continue
        reqs.append(requirement)
    return reqs

#vformat='3.0.dev0'
#version_format='3.0.dev0+{gitsha}'
#setup_requires=['setuptools-git-version'],


MAJOR = 0
MINOR = 0
MICRO = 0
VERSION = f"{MAJOR}.{MINOR}.{MICRO}"


# Return the git revision
def git_version():
    import subprocess
    try:
        rev = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD'])
        rev = rev.decode('ascii').strip()
        count = subprocess.check_output(['git', 'rev-list', '--count', 'HEAD'])
        count = count.decode('ascii').strip()
    except (FileNotFoundError, subprocess.CalledProcessError):
        rev = "Unknown"
    return f"{count}.{rev}"


# Write version info
def write_version_py(filename='finesse/version.py'):
    f = open(filename, 'w')
    try:
        f.write(f"# THIS FILE IS GENERATED FROM FINESSE SETUP.PY\n"
                f"version = '{VERSION}'\n"
                f"git_revision = '{git_version()}'\n")
    finally:
        f.close()


def make_parallel_build():
    import multiprocessing
    import pickle

    def build_extensions(self):
        """
        Function to monkey-patch Cython.Build.build_ext.build_extensions to
        build extensions in parallel.
        """
        self.check_extensions_list(self.extensions)
        num_jobs = os.cpu_count()
        try:
            with multiprocessing.Pool(num_jobs) as pool:
                pool.map(self.build_extension, self.extensions)
        except pickle.PicklingError:
            for ex in self.extensions:
                self.build_extension(ex)

    build_ext.build_extensions = build_extensions


write_version_py()
make_parallel_build()

setup(
    name='finesse',
    description=("Finesse is a simulation program for laser interferometers"),
    license="GPL",
    long_description=read('README.md'),
    long_description_content_type='text/markdown',
    packages=find_packages(),
    ext_package='finesse',
    ext_modules=ext_modules(),
    cmdclass={'build_ext': build_ext},
    zip_safe=False,
    package_data={
        'finesse': ['plotting/style/*.mplstyle', 'cmath/*.pxd']
    },
    install_requires=read_requirements("requirements.txt"),
    extras_require=EXTRAS,
    classifiers=CLASSIFIERS,
    entry_points={
        'console_scripts': [
            'kat3 = finesse.__main__:cli'
            ],
        }
    )
