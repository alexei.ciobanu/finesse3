"""
Tests a R=0.9 mirror with a laser amplitude being modulated by a signal
generator at 1Hz
"""
from finesse import Model
from finesse.components import Laser, Mirror, SignalGenerator
from finesse.detectors import PowerDetector

ifo = Model()

ifo.fsig.f = 1  # [Hz]

# Create componants
l1 = Laser('l1')
sgen = SignalGenerator('sgen', l1.amp_sig.i)
m1 = Mirror('m1', R=0.9, T=0.1)
pd = PowerDetector('refl', m1.p2.o)

# Model
#           |Mirror
# Laser ----|----------- D PowerDetector
#  |        |
# Sig
ifo.add(sgen)
ifo.connect(l1.p1, m1.p1)

carrier, signal = ifo.build()

with carrier, signal:
    # Solve the carrier fields
    out = carrier.run()

    # Then solve the audio sidebands
    sigout = signal.run(carrier) # use a manual RHS vector

print("Carrier:")
print(out)
print("\nSignal:")
print(sigout)
