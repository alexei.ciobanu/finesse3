"""
Simple test to see if a variable can be added and then used in another model parameter
"""

import finesse

model = finesse.parse("""
l l1 0 0 n0
pd pd1 n0
""", True)

ifo = model.model

x = finesse.components.Variable('x', 10)
ifo.add(x)

ifo.l1.P = x.value.ref**2
