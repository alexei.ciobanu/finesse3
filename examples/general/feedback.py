"""
Toy example of a feedback loop to reduce intensity transfer function of a
laser.

Here we just have a laser incident on a photodiode. The photodiode output has
some filter applied to it then it is fed back into the amplitude modulation
input of the laser.

The phase of the feedback is then varied to see which values increase or
decrease the intensity coupling.

A plot of the closed loop RIN to photodiode output is plotted against feedback
gain, with varying phase.
"""

import finesse
from finesse import Model
from finesse.components import Laser, Photodiode, FilterZPK, SignalGenerator
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal

finesse.plotting.style.use('default')

ifo = Model()
ifo.fsig.f = 10  # [Hz]

l1 = Laser('l1', P=1)
pd = Photodiode('pd')

# Some filter to scale the feedback
F = FilterZPK('F', signal.butter(2, 2 * np.pi * 10, 'lp', output='zpk', analog=True))

# Signal generator we'll use to drive the laser amplitude modulation
ifo.add(SignalGenerator('sg', l1.amp_sig.i))

ifo.connect(l1, pd)
ifo.connect(pd.p2, F.p1)
ifo.connect(F.p2, l1.amp_sig)

carrier, signal = ifo.build()

G = np.logspace(-1, 3, 1000)

fig1 = plt.figure(figsize=(10, 8))
ax1 = plt.subplot(211)
ax2 = plt.subplot(212)

_k = F.zpk[2]  # store initial gain

with carrier, signal:
    # Solve the carrier fields
    carrier.run()

    for p in np.linspace(0, 180, 7):
        res = []

        for i in G:
            # Scale the filter gain
            F.zpk[2] = _k * i * np.exp(1j * np.rad2deg(p))
            signal.run(carrier)
            E = signal.out[signal.field(ifo.pd.p2.o)]  # Get photodiode output
            res.append(E)

        ax1.loglog(G, np.abs(res), label='%i deg' % p)
        ax2.semilogx(G, np.angle(res, True), label='%i deg' % p)

ax1.set_title('Closed loop 10 Hz RIN vs gain')
ax1.legend()

ax1.set_xlabel("Feedback gain")
ax1.set_ylabel('Laser RIN [W/W]')
ax2.set_xlabel("Feedback gain")
ax2.set_ylabel('Phase [deg]')

plt.tight_layout()
plt.show()
