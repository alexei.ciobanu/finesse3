"""Example of using multidimensional output objects in Finesse."""

import finesse

from finesse import Model
from finesse.solutions import ArraySolution
from finesse.components import Laser, Mirror
from finesse.detectors import PowerDetector

finesse.LOGGER.setLevel("INFO")

ifo = Model()

l1 = Laser('l1', P=1)
m1 = Mirror('m1', T=0.5, R=0.5)

ifo.connect(l1, m1)

ifo.add(PowerDetector('Pt', m1.p2.o))
ifo.add(PowerDetector('Pr', m1.p1.o))

sims = ifo.build()

out1D = ArraySolution(ifo, 10)

with sims[0] as carrier:
    for i in range(10):
        carrier.run()
        # Set the ith entry of out1D to the current value of carrier.out
        out1D.update(i)

out2D = ArraySolution(ifo, (10, 10))

with sims[0] as carrier:
    for i in range(10):
        for j in range(10):
            carrier.run()
            out2D.update((i, j))

out3D = ArraySolution(ifo, (5, 5, 5))

with sims[0] as carrier:
    for i in range(5):
        for j in range(5):
            for k in range(5):
                carrier.run()
                out3D.update((i, j, k))
