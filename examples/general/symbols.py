"""
Not really a concrete test but a bunch of random symbol stuff from a notebook that should all run without exceptions
"""
import finesse
from finesse.parse.legacy import KatParser

k = KatParser()

k.parse("""
l l1 1 0 n0
s s1 0 n0 n1
mod eo1 10000 .05 5 pm n1 n2
ad bessel0 0 n2
ad bessel1 2 n2
ad bessel2 2 n2
ad bessel3 3 n2
""")

ifo = k.build().model

ifo.add(finesse.components.Variable('fmod', 100))

(ifo.eo1.f.ref + ifo.l1.f.ref) == (ifo.l1.f.ref + ifo.eo1.f.ref)

ifo.eo1.f.ref*3

ifo.eo1.f = ifo.fmod.value.ref
ifo.eo1.f

a = (ifo.fmod.value.ref*2)**2 + 4

ifo.eo1.f = ifo.fmod.value
ifo.eo1.f

import math
import cmath

cos   = lambda x: finesse.element.Operation(math.cos, x)
sin   = lambda x: finesse.element.Operation(math.sin, x)
atan2 = lambda y, x: finesse.element.Operation(math.atan2, y, x)

sin(cos(ifo.fmod.value.ref*2)) * atan2(ifo.fmod.value.ref, 2)

y = (sin(cos(ifo.fmod.value.ref*2)) * atan2(ifo.fmod.value.ref, 2))
ifo.fmod.f = 45000

import numpy as np
angle = lambda x: finesse.element.Operation(np.angle, x)
f"{angle(1+1j)} = {angle(1+1j).eval()}"

import numpy as np
y = ifo.eo1.f.ref + ifo.l1.f.ref
y.eval(subs={ifo.fmod.value: np.linspace(0,100,5)})
