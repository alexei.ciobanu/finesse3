import finesse
from finesse import Model
from finesse.components import Laser, Photodiode
from finesse.detectors import PowerDetector
from finesse.analysis import xaxis, x2axis

finesse.LOGGER.setLevel("INFO")

ifo = Model()

l1 = Laser('l1', P=1)

ifo.add( l1 )
ifo.add( PowerDetector('P_pd', l1.p1.o) )

axis1 = xaxis(l1.P, 0, 1, 5)
axis2 = x2axis(l1.P, 0, 1, 5, l1.phase, 90, 180, 3)

out1D = axis1.run()
out2D = axis2.run()


kat = finesse.parse("""
l l1 1 0 n0
s s1 0 n0 n1
mod eo1 40k .1 1 pm n1 n2
s s2 1 n2 n3
m m1 0.99 0.01 0 n3 n4
s s3 1 n4 n5
m m2 0.9999 0.0001 0 n5 n6

ad R 0 n3
ad C 0 n5
ad T 0 n6
""", True)
ifo = kat.model

ifo.m1.T = 1-ifo.m1.R.ref
ifo.add(finesse.detectors.SymbolDetector('m1_T', ifo.m1.T, float))

out = xaxis(ifo.m1.R, 0.95, 0.99999, 1000).run()

print(out1D)
print(out2D)
print(out)
