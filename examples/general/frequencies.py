import finesse
from finesse.parse.legacy import KatParser
from finesse.analysis import xaxis
from copy import deepcopy

k = KatParser()

k.parse("""
l l1 1 10 n0
s s1 0 n0 n1
mod eo1 40k .1 2 pm n1 n2
s s2 1 n2 n3
m m1 0.99 0.01 0 n3 n4
s s3 1 n4 n5
m m2 0.9999 0.0001 0 n5 n6

ad circ_c 0 n5
ad circ_u 0 n5
ad circ_l 0 n5
""")

model = k.build()
ifo = model.model

ifo.fsig.f = 10

# No need to do puts or anything now, just using symbols we can say what carrier to look at
ifo.circ_c.f = ifo.l1.f.ref + 2*ifo.eo1.f.ref
# reference other detector to get the carrier frequency and then find the audio sidebands
ifo.circ_u.f = ifo.circ_c.f + ifo.fsig.f.ref
ifo.circ_l.f = ifo.circ_c.f - ifo.fsig.f.ref

# scan the audio frequency
out = xaxis(ifo.fsig.f, 10, 10e3, 10).run()

print(out['circ_c'], out['circ_u'], out['circ_l'])
