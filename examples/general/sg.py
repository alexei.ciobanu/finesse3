"""
Demonstration of solution outputs, copied and modified from the feedback.py example.
"""

import numpy as np
import scipy.signal as signal
import finesse
from finesse import Model
from finesse.components import Laser, Photodiode, FilterZPK, SignalGenerator
from finesse.analysis import xaxis

ifo = Model()
ifo.fsig.f = 10  # [Hz]

l1 = Laser('l1', P=1)
pd = Photodiode('pd')

# Some filter to scale the feedback
F = FilterZPK('F', signal.butter(2, 2 * np.pi * 10, 'lp', output='zpk', analog=True))

# Signal generator we'll use to drive the laser amplitude modulation
ifo.add(SignalGenerator('sg', l1.amp_sig.i))

ifo.connect(l1, pd)
ifo.connect(pd.p2, F.p1)
ifo.connect(F.p2, l1.amp_sig)

solution = xaxis(ifo.sg, 0, 3, 100).run()

print(solution)
