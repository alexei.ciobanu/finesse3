import finesse

finesse.plotting.use("default")

model = finesse.parse("modulator.kat", True)
out = model.run()
out.plot()
