default:
	python3 setup.py build_ext --build-lib .
	
clean:
	find . -name "*.so" -type f -delete
	find . -name "*.dll" -type f -delete

realclean: clean
	git clean -fX
	
lint:
	pylint --rcfile=.pylintrc ./finesse/
