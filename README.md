# Finesse 3

Finesse 3 is a Python based interferometer simulation software. It is currently in an early
development stage. For modelling interferometers we currently recommend that you use our mature packages:
* Finesse 2 http://www.gwoptics.org/finesse
* Pykat http://www.gwoptics.org/pykat

At the same time we invite participation to the development of Finesse. You can find the Finesse 3 resources at:
* Source code, issue tracker etc. https://git.ligo.org/finesse/finesse3
* Documentation https://finesse.readthedocs.io/

## Installing the development environment

For example, using Anaconda you could install the required packages as shown below.

Into a new conda environment:<br>
```conda env create -n finesse3 -f environment.yml```

Or into your existing conda environment:<br>
```conda env update -f environment.yml```

Finally you could collect the dependencies manually:<br>
```conda install numpy cython```<br>
```conda install -c conda-forge suitesparse```

For building the documentation:<br>
```conda install sphinx sphinx_rtd_theme numpydoc```<br>
```conda install -c conda-forge sphinxcontrib-bibtex```<br>
```pip install sphinxcontrib-katex```

To install Finesse 3 (from the above conda environment) do:
```make```
```pip install -e .```

## Some guiding principles for the development

In-depth guide for developers (WIP): https://finesse.readthedocs.io/en/latest/developer/index.html

1. Don't fix what isn't broken: Finesse has wide user base and people know it's "quirks"
2. Backward compatiability: Should aim to keep everything working and giving the same results as before where possible. In the previous rewrite all the test files were withing 1e-10 relative error of the previous version (Unless something was physically incorrect)
3. Both physical and code development conventions will be stuck to at all times!! Hopefully less spurious -1 throughout the code
4. A central git repository will be run, developers will fork and merge to it. Their fork should be checked and verified before merging

## Wish list

The following list collects new features that have been suggested or discussed:

* Dummy and pro mode:
    - Dummy mode:
        Runs like Finesse does now, specify xaxis, funcs, parse the same files
    - Pro mode
        Run with more indepth tuning possible, custom stepping of simulation, performing analysis before next step, etc.

* Python Binary: Can still call `kat` from command line even though we have a Python executable
    - [sean] Note: this doesn't necessarily need to be a binary. The `setuptools` module allows `entry_points` to be defined which create a platform-dependent command pointing to the program. See for example this [__main__.py](https://git.ligo.org/sean-leavey/circuit/blob/43b617bcf2efd6af4020838532fb77e23fecb544/circuit/__main__.py) class.

* Better abstraction of IFO DOFs, its error signal and the feedback

* No node names. Have ability to tag them though. The for most part node names aren't that important.
    - For example: ```m m1 1 0 0
        m m2 1 0 0
        s s1 2 m1->2 m2->1
        s s1 2 m1.2 m2.1 # or
        node m2->1 n1 # Could name them though
        s s1 2 m1->1 n1
        ```
* Also interesting syntax we could use here `m1->1` for field leaving, `m1<-1` for field incoming, less confusion over node * directions?

* HOM include list. Ability to decide which specific HOMs you want. Say you only want to model 00, 20, 40, and don't want all the Y modes.

* Customisable frequency list. Have this already in Finesse but would be nice to have names for frequencies and have those names accesible throughout the syntax, which would remove the need for `put` with changing frequencies, or `func` when trying to readout an `ad` with a changing frequency of a RF carrier audio sideband

* Interface for simulink or something like it (PyLinX perhaps https://www.youtube.com/watch?v=tqRpmVDMERM) are LIGO people writing their own??

* Parallel interface for running multiple models

* Sub matrix solving for particular blocks of components, i.e. "black box"

* Polarisation: worth it or not worth it? Could be interesting for future polarised topologies, or people doing tabletops. Would this have been useful for Sam and Conors modelling if we had had it?

* Multiple carrier laser frequencies

* Low frequency sidebands become noisy from numerical problems, probably where f/f0. Andreas and I thought about this at the Beijing School but we couldn't see how to fix it easily

* Aperture effects: Apertures have an ABCD matrix apparently, we could also do with having analytic coupling calculations for the scattering matrices too, as currently they are done with numerical integration which is slow
    - https://www.ncbi.nlm.nih.gov/pubmed/20700323

* Further detailed ABCD matrices for displacements and misalignments: Generalized Beam Matrices: Gaussian Beam Propagation in Misaligned Complex Optical Systems, 1995

## TODO

### Distribution

It may seem early to think about distributing the final product, but we should keep in mind that we want Finesse to run on Windows, Linux, and OSX. Windows, for example, is by far the largest download number, yet none of us regularly use it. We should be careful not to use libraries or code that are limited by OS, this creates separate development forks and is more work to test and support later.

I believe distributing through Anaconda is the future. I think we should aim for that first and from the Anaconda distribution generate the PyPi package, and then finally a downloadable unit that others can access.

Similarly we should be wary of how we compile all these Python packages and accompanying C code. Numba and Cython seem like the two ideal ones. Cython seems more mature on the distribution front, Numba might be great but how do we distribute code using it? It has ahead-of-time compiling, but we should try this out and see how it fits into development.

### Define conventions

#### Coding

Can decide this later, but we'll have a mixture of Python and C, need to define how we name functions, variables, classes, filenames, etc. With a mix of people writing the code it could become messy fast.

#### Physical

I believe the majority of -1 issues we have in Finesse are because we forget which way things should move or rotate. Then we fudge minus signs to match our understanding at the time. This should be written in bold somewhere and we all stick to it! Sounds simple but it's easily forgotten...

* Coordinate system, LH or RH, which way is positive rotation which way is negative


## Libraries/Modules

I envision trying to separate out the different tasks in Finesse into different libraries/modules. Hopefully a more modular fashion will allow us to write cleaner code and be easier to work with in the future, in terms or support or upgrades.

### Matrix construction and solving library

This could be vary barebones, doesn't need to know any physics. Really just a wrapper to easily access the matrix memory and call the solver on it. This will require inputs from the layout library.

* Parallel version of KLU https://github.com/trilinos/Trilinos/tree/master/packages/common/auxiliarySoftware/SuiteSparse/src/PARAKLETE. This uses MPI (distributed memory) which could be a bit overkill, maybe could make an openMP/pthread (shared memory) version seeing where they have parallised things?

* Same projected has made "KLU2" https://github.com/trilinos/Trilinos/tree/master/packages/amesos2/src/KLU2. How is it different from original KLU? Just recoded for C++ templates perhaps?

* Add ports individually

* What outputs do we want

* Set whether there is coupling between ports or not

* Set number of HOMs

* Set number of frequencies

* Set frequency coupling

* Rust based
    pros
        - Speed, fast as C apparently...
        - Nicer looking language compared to C/C++
        - Better multiprocessing cabailities and memory mangement built in
        - Looks like easier wrapping to Python
    cons
        - We don't use it much
        - Will it still be around in 20 years?
        - Still being developed so might change a bit still, good or bad I guess

* C/C++ based
    pros
        - Speed (hopefully)
        - Fine control over the largest chunk of memory in the simulation
        - should be abstract enough it doesn't need to be touched once built
        - can map allocated memory straight to numpy arrays or whatever in Python
    cons
        - Not easily extendable as python
        - What to do with different matrix formats?

* Python based matrix builder/solver
    Have a class that builds and makes a matrix
    - networkx has ability to generate sparse matrix

### Layout  library

This would generate an optical layout to give to the matrix builder. This is where we could do clever things to "black box" components together. Hopefully we can have different libraries here. A base one that represents what Finesse does now, then people can write their own to try and be smart about it.

This will have to tie in to the beam tracer and scattering matrix libraries below.

### Beam tracer library

It has become apparent that a more accessible beam tracer would be useful. This should be a separately callable algorithm which both the user and the internals of Finesse can call. Beam properties and tracing outcomes should be more visible and accessible. It should take from the Finesse script the geometry data, `cav` and `gauss` commands, and from these do the beam tracing. Users should then easily be able to access beam parameters, see mismatches, see ABCD, stabilties, etc.

More graphical outputs would also be nice, for example easily generate how beam changes along particular paths throughout the IFO. Useful for designing Gouy phase telescopes.

Can this also be tied into optivis to visually show beam parameters?

We might consider using symbolic matrix calculations for ABCD tracing, or having an option for it. Round trip ABCDs should have A=D, however numerically this is not always the case, and was only fixed with symbolic math. This was causing an issue when we were trying to perfectly mode match a LIGO file and even with the mismatch paramter zeroed, we still saw significant 00-20 couplings, for example.

### Scattering matrix calculations library

The coupling matrix calculations in Finesse currently could do with tidying up. Alot was learnt during the code writing about how it should or should not work: martix separation, matrix ordering, different quadrature methods, interpolation. This should be written up in a more modular way.

* Can we incorporate OpenCL, GPU, OpenMP to speed up calculations here?

### Overseer library

This should be the main one. It will handle the parsing of commands and general user interaction with the simulation. This in turn will then go and call the various aspects of the other libraries to run an actual simulation.

