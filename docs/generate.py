"""
Script to generate documentation pages from the Finesse source code.

Author: Samuel Rowlinson
"""

from collections import namedtuple
from enum import Enum
import os

import finesse

ClassInfo = namedtuple("ClassInfo", "name is_enum is_data is_abstract is_private no_doc streampos")
FunctionInfo = namedtuple("FunctionInfo", "name is_private no_doc streampos")
MethodInfo = namedtuple("MethodInfo", "owner name is_property is_private is_constructor no_doc streampos")

class SourceFileInfo(object):
    def __init__(self):
        self.classes = []
        self.class_methods = {}
        self.functions = []

class SectionState(Enum):
    CLASSES = 0
    OVERVIEW = 1
    ENUMS = 2
    DATACLASSES = 3
    PROPERTIES = 4
    METHODS = 5
    FUNCTIONS = 6
    NULL = 7

def parse_methods(classes, file):
    methods = {}
    for i, class_info in enumerate(classes):
        if class_info.is_enum: continue

        file.seek(class_info.streampos)
        methods[class_info.name] = []

        if class_info.streampos == classes[-1].streampos:
            allclass = file.read()
        else:
            allclass = file.read(classes[i + 1].streampos - class_info.streampos)

        class_lines = list(iter(allclass.splitlines()))

        for j, line in enumerate(class_lines):
            stripped_line = line.strip()

            if stripped_line.startswith("def") and len(line) - len(stripped_line) == 4:
                method_name = (stripped_line.split(' ')[1]).split('(')[0]
                is_property = class_lines[j - 1].lstrip().startswith("@property")
                is_constructor = method_name == "__init__"
                is_private = method_name.startswith('_') and not is_constructor

                methods[class_info.name].append(MethodInfo(
                    owner=class_info.name,
                    name=method_name,
                    is_property=is_property,
                    is_private=is_private,
                    is_constructor=is_constructor,
                    no_doc="__NODOC__" in line,
                    streampos=file.tell()
                ))

    return methods


def parse(file):
    classes = [] # holds tuples of (streampos, class_name, is_abstract, is_private)
    functions = [] # holds tuples of (streampos, function_name)
    with open(file) as f:
        line = f.readline()
        prev_line = ""
        in_mod_docstrings = False
        while line:
            if line.startswith("\"\"\"") and not in_mod_docstrings:
                in_mod_docstrings = True
            elif line.startswith("\"\"\"") and in_mod_docstrings:
                in_mod_docstrings = False

            # don't consider any "class" string found in docstrings
            if line.startswith("class") and not in_mod_docstrings:
                name_base = (line.split(' ')[1]).split('(')
                classes.append(ClassInfo(
                    name=name_base[0].split(':')[0],
                    is_enum="Enum" in name_base[1] or "Flag" in name_base[1] if len(name_base) > 1 else False,
                    is_data="dataclass" in prev_line,
                    is_abstract="ABC" in name_base[1] if len(name_base) > 1 else False,
                    is_private=name_base[0].startswith('_'),
                    no_doc="__NODOC__" in line,
                    streampos=f.tell()
                ))

            # check for free function docstrings
            elif line.startswith("def") and not in_mod_docstrings:
                name = (line.split(' ')[1]).split('(')[0]
                functions.append(FunctionInfo(
                    name=name,
                    is_private=name.startswith('_'),
                    no_doc="__NODOC__" in line,
                    streampos=f.tell()
                ))

            prev_line = line
            line = f.readline()

        sfi = SourceFileInfo()
        sfi.classes = classes
        sfi.class_methods = parse_methods(classes, f)
        sfi.functions = functions

    return sfi

def make_subdirs(doc_file):
    source_dir_idx = doc_file.index("source/api")
    const_pre_path = doc_file[:source_dir_idx]
    from_source_path = doc_file[source_dir_idx:]

    accumulated_path = ""
    subdirs = from_source_path.split('/')
    filename = subdirs[-1]
    if "version" in filename or "test" in filename or "__main__" in filename:
        return -1

    for dirname in subdirs[-1]:
        full_dirname = os.path.join(os.path.join(const_pre_path, accumulated_path), dirname)
        if not os.path.isdir(full_dirname):
            os.mkdir(full_dirname)
        accumulated_path = os.path.join(accumulated_path, dirname)

    return 0

def enums_section(enums):
    enums_str = ""
    if enums:
        enums_str += """
Enums
=====

.. autosummary::
    :toctree: generated/

        """
        for enum in enums:
            enums_str += f"\n\t{enum}"

    return enums_str

def dataclasses_section(dataclasses):
    dataclasses_str = ""
    if dataclasses:
        dataclasses_str += """
Dataclasses
===========

.. autosummary::
    :toctree: generated/

        """
        for dc in dataclasses:
            dataclasses_str += f"\n\t{dc}"

    return dataclasses_str

def properties_section(properties, class_name):
    properties_str = ""
    if properties:
        properties_str += """
Properties
==========

.. autosummary::
    :toctree: generated/

        """
        for prop in properties:
            properties_str += f"\n\t{class_name}.{prop}"

    return properties_str

def methods_section(methods, constructor, class_name):
    methods_str = ""
    if methods or constructor is not None:
        methods_str += """
Methods
=======

.. autosummary::
    :toctree: generated/

        """

        if constructor is not None:
            methods_str += f"\n\t{class_name}.{constructor}"

        for meth in methods:
            methods_str += f"\n\t{class_name}.{meth}"

    return methods_str

def functions_section(functions):
    functions_str = ""
    if functions:
        functions_str += """
Functions
=========

.. autosummary::
    :toctree: generated/

        """
        for func in functions:
            functions_str += f"\n\t{func}"

    return functions_str

def make_docfile_contents(
    header_name, module_name,
    class_str,
    enums_str, dataclasses_str,
    properties_str, methods_str,
    functions_str,
    doc_automod
):
    header_thing = ''.join(['=' for _ in range(len('``'+header_name+'``'))])
    if doc_automod: automodule = f".. automodule:: {module_name}"
    else: automodule = ""
    contents = f"""{header_thing}
``{header_name}``
{header_thing}

{automodule}

.. currentmodule:: {module_name}

{class_str}

{enums_str}

{dataclasses_str}

{properties_str}

{methods_str}

{functions_str}
    """

    return contents

def generate_doc_file(doc_file, sfi, doc_automod=True):
    exists = os.path.isfile(doc_file)

    class_names = [ci.name for ci in sfi.classes
                   if not ci.is_private and not ci.is_enum and not ci.is_data
                   and not ci.no_doc]

    if len(class_names) > 1:
        leading_dir, fname = os.path.split(doc_file)
        new_dirname = fname.split('.')[-2]

        full_new_dirname = os.path.join(leading_dir, new_dirname)
        if not os.path.isdir(full_new_dirname):
            os.mkdir(full_new_dirname)

        for classname in class_names:
            sfi_for_class = SourceFileInfo()
            sfi_for_class.classes = [c for c in sfi.classes if c.name == classname]
            assert(len(sfi_for_class.classes) == 1)
            sfi_for_class.class_methods = sfi.class_methods.copy()

            new_fname = '.'.join(fname.split('.')[:-1]) + f".{classname}.rst"
            generate_doc_file(
                os.path.join(leading_dir, os.path.join(new_dirname, new_fname)),
                sfi_for_class,
                doc_automod=False
            )

        class_str = f"""
Classes
=======

.. autosummary::
    :toctree: {new_dirname}/

        """
        for classname in class_names:
            class_str += f"\n\t{classname}"

        properties = []
        methods = []
        constructor = None
        class_name = None

    else:
        class_str = ""
        if not class_names:
            class_name = None
            properties = []
            methods = []
            constructor = None
        else:
            class_name = class_names[0]
            class_str = f"""
Overview
========

.. autoclass:: {class_name}
            """

            if class_name in sfi.class_methods:
                all_public_methods = [m for m in sfi.class_methods[class_name]
                                      if not m.is_private]
                properties = [m.name for m in all_public_methods if m.is_property]
                methods = [m.name for m in all_public_methods
                           if not m.is_property and not m.is_constructor and not m.no_doc]
                constructors = [m.name for m in all_public_methods if m.is_constructor]
                if constructors: constructor = constructors[0]
                else: constructor = None
            else:
                properties = []
                methods = []
                constructors = []
                constructor = None

    enums = [c.name for c in sfi.classes if c.is_enum and not c.is_private]
    dataclasses = [c.name for c in sfi.classes if c.is_data and not c.is_private]
    functions = [f.name for f in sfi.functions if not f.is_private and not f.no_doc]

    # TODO read in the file and check for mismatches between its contents and sfi
    if exists:
        to_add, to_rm, fcontents = read_doc_file(
            doc_file, class_names, class_name, properties, methods, functions
        )

        # TODO add what needs to be added, fix issues with items in to_rm - I've noticed
        #      lots of things getting removed which shouldn't be (constructors are an easy
        #      fix but other items seem to be getting deleted)
        if needs_update(to_add, to_rm) and False:
            ss = SectionState.NULL
            with open(doc_file, 'w') as f:
                for i, line in enumerate(fcontents):
                    if line not in to_rm:
                        f.write(line)

                    ss = section_state(line, fcontents, i)
        return

    # make each sub-directory if they don't exist already
    if make_subdirs(doc_file) == -1: return

    # generate each section
    enums_str = enums_section(enums)
    dataclasses_str = dataclasses_section(dataclasses)
    properties_str = properties_section(properties, class_name)
    methods_str = methods_section(methods, constructor, class_name)
    functions_str = functions_section(functions)

    module_name = (os.path.split(doc_file)[1]).replace(".rst", '')
    header_name = module_name
    if class_name is not None and module_name.split('.')[-1] == class_name:
        module_name = module_name.replace(f".{class_name}", '')

    contents = make_docfile_contents(
        header_name, module_name,
        class_str,
        enums_str, dataclasses_str,
        properties_str, methods_str,
        functions_str,
        doc_automod
    )

    with open(doc_file, 'w+') as f:
        f.write(contents.strip())

def section_state(line, fcontents, idx):
    ss = SectionState.NULL

    if fcontents[idx] == fcontents[-1]:
        return ss

    if line == "Classes\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.CLASSES
    elif line == "Overview\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.OVERVIEW
    elif line == "Enums\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.ENUMS
    elif line == "Dataclasses\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.DATACLASSES
    elif line == "Properties\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.PROPERTIES
    elif line == "Methods\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.METHODS
    elif line == "Functions\n" and fcontents[idx+1].startswith('='):
        ss = SectionState.FUNCTIONS

    return ss

def needs_update(to_add, to_remove):
    any_add = any([thing is not None and thing for thing in to_add.values()])
    any_remove = True if to_remove else False

    return any_add | any_remove

def section_start_index(contents, section):
    try:
        ssi = contents.index(section+'\n')
    except ValueError:
        return None

    try:
        if contents[ssi + 1] != ''.join(['=' for _ in range(len(section))]) + '\n':
            return None
    except IndexError:
        return None

    return ssi

def parse_classes_section(contents, class_names):
    ssi = section_start_index(contents, "Classes")
    if ssi is None: return None, None

    try:
        asi = contents[ssi:].index(".. autosummary::\n") + ssi
    except ValueError:
        return class_names, None

    classnames_in_docfile = set()
    for line in contents[(asi + 2):]:
        if not line or line == '\n': continue
        if not line.startswith('\t') and not line.startswith("    "):
            break

        classnames_in_docfile.add(line.strip())

    classnames_in_srcfile = set(class_names)

    to_remove = classnames_in_docfile.difference(classnames_in_srcfile)
    to_add = classnames_in_srcfile.difference(classnames_in_docfile)

    return to_add, to_remove

def parse_overview_section(contents, class_name):
    ssi = section_start_index(contents, "Overview")
    if ssi is None: return None, None

    to_add = set()
    to_remove = set()

    if not f".. autoclass:: {class_name}\n" in contents[ssi:]:
        to_add.add(class_name)

    for line in contents[ssi:]:
        lstrip = line.strip()
        if lstrip.startswith("..autoclass::") and line != f".. autoclass:: {class_name}\n":
            to_remove.add(lstrip.split("::")[1].strip())

    return to_add, to_remove

def parse_properties_section(contents, class_name, properties):
    ssi = section_start_index(contents, "Properties")
    if ssi is None: return None, None

    try:
        asi = contents[ssi:].index(".. autosummary::\n") + ssi
    except ValueError:
        return properties, None

    properties_in_docfile = set()
    for line in contents[(asi + 2):]:
        if not line or line == '\n':
            continue
        if not line.startswith('\t') and not line.startswith("    "):
            break

        properties_in_docfile.add(line.strip())

    properties_in_srcfile = set([f"{class_name}.{prop}" for prop in properties])

    to_remove = properties_in_docfile.difference(properties_in_srcfile)
    to_add = properties_in_srcfile.difference(properties_in_docfile)

    return to_add, to_remove

def parse_methods_section(contents, class_name, methods):
    ssi = section_start_index(contents, "Methods")
    if ssi is None: return None, None

    try:
        asi = contents[ssi:].index(".. autosummary::\n") + ssi
    except ValueError:
        return methods, None

    methods_in_docfile = set()
    for line in contents[(asi + 2):]:
        if not line or line == '\n':
            continue
        if not line.startswith('\t') and not line.startswith("    "):
            break
        methods_in_docfile.add(line.strip())

    methods_in_srcfile = set([f"{class_name}.{meth}" for meth in methods])

    to_remove = methods_in_docfile.difference(methods_in_srcfile)
    to_add = methods_in_srcfile.difference(methods_in_docfile)

    return to_add, to_remove

def parse_functions_section(contents, functions):
    ssi = section_start_index(contents, "Functions")
    if ssi is None: return None, None

    try:
        asi = contents[ssi:].index(".. autosummary::\n") + ssi
    except ValueError:
        return functions, None

    functions_in_docfile = set()
    for line in contents[(asi + 2):]:
        if not line or line == '\n':
            continue
        if not line.startswith('\t') and not line.startswith("    "):
            break
        functions_in_docfile.add(line.strip())

    functions_in_srcfile = set(functions)

    to_remove = functions_in_docfile.difference(functions_in_srcfile)
    to_add = functions_in_srcfile.difference(functions_in_docfile)

    return to_add, to_remove

def read_doc_file(doc_file, class_names, class_name, properties, methods, functions):
    with open(doc_file) as f:
        contents = f.readlines()

        classes_to_add, classes_to_rm = parse_classes_section(contents, class_names)
        class_to_add, class_to_rm = parse_overview_section(contents, class_name)
        props_to_add, props_to_rm = parse_properties_section(contents, class_name, properties)
        meths_to_add, meths_to_rm = parse_methods_section(contents, class_name, methods)
        funcs_to_add, funcs_to_rm = parse_functions_section(contents, functions)

    lines_to_rm = []
    if classes_to_rm is not None:
        for c in classes_to_rm:
            lines_to_rm.append(f"    {c}\n")
            lines_to_rm.append(f"\t{c}\n")

    if class_to_rm is not None:
        for c in class_to_rm:
            lines_to_rm.append(f"    {c}\n")
            lines_to_rm.append(f"\t{c}\n")

    if props_to_rm is not None:
        for p in props_to_rm:
            lines_to_rm.append(f"    {p}\n")
            lines_to_rm.append(f"\t{p}\n")

    if meths_to_rm is not None:
        for m in meths_to_rm:
            lines_to_rm.append(f"    {m}\n")
            lines_to_rm.append(f"\t{m}\n")

    if funcs_to_rm is not None:
        for f in funcs_to_rm:
            lines_to_rm.append(f"    {f}\n")
            lines_to_rm.append(f"\t{f}\n")

    return {
        "classes" : classes_to_add,
        "class" : class_to_add,
        "properties" : props_to_add,
        "methods" : meths_to_add,
        "functions" : funcs_to_add
    }, lines_to_rm, contents


def recursive_generate(file, pre_path=""):
    if file.endswith(".py") and file != "__init__.py":
        src_file = os.path.abspath(finesse.__file__.replace("__init__.py", pre_path + file))
        sfi = parse(src_file)

        filename = file.split('.')[0]
        doc_real_path = os.path.dirname(os.path.realpath(__file__))
        #src_to_doc_path = doc_real_path + f"/source/api/{pre_path}{filename}"
        this_doc_file = doc_real_path + "/source/api{0}finesse.{1}{2}.rst".format(
            '/' + pre_path if pre_path else '/',
            pre_path.replace('/', '.') if pre_path else '',
            filename
        )

        generate_doc_file(this_doc_file, sfi)
    elif not '.' in file and not file.startswith('_'):
        for sub_file in os.listdir(finesse.__file__.replace("__init__.py", pre_path + file + '/')):
            recursive_generate(sub_file, pre_path + file + '/')

def run():
    for file in os.listdir(finesse.__file__.replace("__init__.py", '')):
        recursive_generate(file)

if __name__ == "__main__":
    run()
