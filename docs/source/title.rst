.. include:: /defs.hrst

.. figure:: images/ascii_rider_finesse_3.png
    :align: center


----

.. note:: This is the documentation for |Finesse| 3, which is in a
        pre-alpha stage of development. You can find the documentaton, executables and code
        for the stable |Finesse| 2 at `<http://www.gwoptics.org/finesse>`_.

Overview
========

Finesse is a fast and easy to use Python-based interferometer simulation program.
It uses frequency-domain optical modelling to build accurate
quasi-static simulations of arbitrary interferometer configurations.

For convenience, a number of standard analyses can be performed
automatically by the program, namely computing modulation-demodulation error
signals, transfer functions, quantum-noise-limited sensitivities, and beam
shapes. The optical system can be modelled using the plane-wave
approximation or Hermite-Gauss modes, allowing the computation of
optical systems like telescopes or the effects of mode matching
and mirror angular positions.

Finesse is based on an object-oriented design and provides a large set of utility
functions for ease of use when handling complex simulation tasks, for example:

- chained simulations,
- advanced plotting of outputs,
- automated simulations,
- the use of Jupyter notebooks to document simulation tasks,
- and much more!
