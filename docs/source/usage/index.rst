.. _usage:

Usage
=====

This section explains the most common methods of
using Finesse.

.. toctree::
    :maxdepth: 1

    simple_mode
    advanced_usage/index
    syntax
