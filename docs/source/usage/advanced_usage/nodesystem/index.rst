.. _ports:
.. include:: /defs.hrst

The Port and Node System
========================

One of the key structural improvements made to |Finesse| is the introduction of a flexible
port and node system. This system underpins the code structure of |Finesse| as all couplings
and interactions are performed via ports and nodes of components.

A :class:`.Port` in |Finesse| 3 is fundamentally similar to the definition of a node in |Finesse| 2;
i.e. it represents a point of connections at any arbitrary component. These connection points are then
:class:`.Node` instances which represent the direction of the coupling into/from the component and
also the type of the connection. This connection type is a new feature for |Finesse| 3 as previous
versions only allowed optical nodes at a component and in a model, whereas |Finesse| 3 has three
different node types (see :class:`.NodeType`) - optical, electrical and mechanical - with inter-couplings
allowed between these different nodes.

.. toctree::
    :maxdepth: 1

    basic_usage
    beam_parameters
