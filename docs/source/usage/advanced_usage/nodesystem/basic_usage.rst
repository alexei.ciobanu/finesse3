Basic usage of ports and nodes
------------------------------

All components in Finesse 3 have :class:`.Port` instances associated with them, which in turn
contain :class:`.Node` objects representing the coupling type and direction at this connection
point.

.. rubric:: Accessing ports

Ports of a component can be accessed via the component through the names that they were assigned during construction.
For example, a :class:`.Mirror` has two ports (`p1` and `p2`) which can be grabbed directly::

    from finesse.components import Mirror

    M1 = Mirror("M1")
    print(M1.p1)
    print(M2.p2)

>>> <Port M1.p1 @ [memory_address]>
>>> <Port M1.p2 @ [memory_address]>

One can also get a *read-only tuple* of all the ports of a component::

    print(M1.ports)

>>> (<Port M1.p1 @ [memory_address]>,
     <Port M1.p2 @ [memory_address]>,
     <Port M1.mech @ [memory_address]>,
     <Port M1.phase_sig @ [memory_address]>)

.. rubric:: What does a port contain?

In the example code below we show the main properties of a :class:`.Port` - namely its type, the nodes that it
holds and the component that it is attached to (which is always a :class:`.Space` instance for optical connections)::

    import finesse
    from finesse.components import Mirror, Space

    M1 = Mirror("M1)
    M2 = Mirror("M2")

    model = finesse.Model()
    # connect M1 <-> M2 in a model via a Space of length 1m
    model.chain(M1, fc.Space("M1_M2", L=10), M2)

    print(f"Port M1.p2 name = {M1.p2.name}")
    print(f"Port M1.p2 type = {M1.p2.type}")
    print(f"Port M1.p2 owning component = {M1.p2.component}")
    print(f"Port M1.p2 attached component = {M1.p2.attached_to}")
    print(f"Port M1.p2 nodes = {M1.p2.nodes}")

>>> Port M1.p2 name = p2
>>> Port M1.p2 type = NodeType.OPTICAL
>>> Port M1.p2 owning component = <'M1' @ [memory_address] (Mirror)>
>>> Port M1.p2 attached component = <'M1_M2' @ [memory_address] (Space)>
>>> Port M1.p2 nodes = (<OpticalNode M1.p2.i @ [memory_address]>, <OpticalNode M1.p2.o @ [memory_address]>)

.. rubric:: Accessing nodes --- via a component

Nodes can be accessed directly through components or via the :class:`.Model` instance that their owning
component is associated with (see next section for details). To access all the nodes of a component::

    print(f"All nodes of M1 = {M1.nodes}")

>>> All nodes of M1 =
    {'M1.p1.i': <OpticalNode M1.p1.i @ [memory_address]>,
    'M1.p1.o': <OpticalNode M1.p1.o @ [memory_address]>,
    'M1.p2.i': <OpticalNode M1.p2.i @ [memory_address]>,
    'M1.p2.o': <OpticalNode M1.p2.o @ [memory_address]>,
    'M1.mech.z': <MechanicalNode M1.mech.z @ [memory_address]>,
    'M1.mech.yaw': <MechanicalNode M1.mech.yaw @ [memory_address]>,
    'M1.mech.pitch': <MechanicalNode M1.mech.pitch @ [memory_address]>,
    'M1.phase_sig.i': <ElectricalNode M1.phase_sig.i @ [memory_address]>,
    'M1.phase_sig.o': <ElectricalNode M1.phase_sig.o @ [memory_address]>}

Or all the optical nodes::

    print(f"Optical nodes of M1 = {M1.optical_nodes}")

>>> Optical nodes of M1 =
    (<OpticalNode M1.p1.i @ [memory_address]>,
    <OpticalNode M1.p1.o @ [memory_address]>,
    <OpticalNode M1.p2.i @ [memory_address]>,
    <OpticalNode M1.p2.o @ [memory_address]>)

Get a single optical node of a component by its *direction*::

    print(f"Input node of port M1.n1 = {M1.p1.i}")
    print(f"Output node of port M1.n1 = {M1.p1.o}")

>>> Input node of port M1.n1 = <OpticalNode M1.p1.i @ [memory_address]>
>>> Output node of port M1.n1 = <OpticalNode M1.p1.o @ [memory_address]>

.. rubric:: Accessing nodes --- via the model

Nodes play an important role in the :class:`.Model` class as the :attr:`.Node.full_name` property forms the `node_type`
of the underlying directed graph object (stored in :attr:`.Model.network`). Thus, we use Node instances to report on
graph data as well as perform operations on this graph - see `the networkx DiGraph documentation <https://networkx.github.io/documentation/stable/reference/classes/digraph.html>`_ for details on these methods and attributes.

You can also access all the nodes of a given :class:`.NodeType` in a Model instance with (e.g. for optical nodes)::

    print(f"All optical nodes in model: {model.optical_nodes}")

>>> All optical nodes in model:
    [<OpticalNode M1.p1.i @ [memory_address]>,
    <OpticalNode M1.p1.o @ [memory_address]>,
    <OpticalNode M1.p2.i @ [memory_address]>,
    <OpticalNode M1.p2.o @ [memory_address]>,
    <OpticalNode M2.p1.i @ [memory_address]>,
    <OpticalNode M2.p1.o @ [memory_address]>,
    <OpticalNode M2.p2.i @ [memory_address]>,
    <OpticalNode M2.p2.o @ [memory_address]>]

Node names are used as keys in the network of a Model to get data on the node itself and edges connected to the node::

    print(model.network.node[model.M2.p1.i.full_name])
    print(model.network[model.M2.p1.i.full_name][model.M2.p1.o.full_name])

>>> {'weakref': <weakref at [memory_address]; to 'OpticalNode' at [memory_address]>,
    'owner': <weakref at [memory_address]; to 'Mirror' at [memory_address]>,
    'optical': True}
>>> {'name': 'M2.p1.i->M2.p1.o',
    'in_ref': <weakref at [memory_address]; to 'OpticalNode' at [memory_address]>,
    'out_ref': <weakref at [memory_address]; to 'OpticalNode' at [memory_address]>,
    'owner': <weakref at [memory_address]; to 'Mirror' at [memory_address]>,
    'length': 1,
    'coupling_type': <CouplingType.OPTICAL_TO_OPTICAL: 0>}

