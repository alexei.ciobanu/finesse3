.. _beam_param_node_usage:

Beam parameters at optical nodes
--------------------------------

All instances of an :class:`.OpticalNode` in Finesse 3 have associated beam parameters
which are referenced through the :class:`.Model` object that the nodes belong to.

.. rubric:: In brief

- Access the beam parameter at a node of a model with ``model.comp.port.node.q``.
- Set the beam parameter at a specified node with ``model.comp.port.node.q = value``.
- Display the beam parameters for all optical nodes in a model (after tracing the beam) with
  ``print(model.last_trace)``.

.. rubric:: Accessing beam parameters

The beam parameters of a model can be accessed in several different ways - either
directly via an instance of :class:`.Model` or indirectly via the :attr:`.OpticalNode.q`
attribute. Direct access is performed through a call to :attr:`.Model.last_trace` if
a beam trace has already been carried out on the model or through :attr:`.Model.gauss_commands`
for nodes which have been set manually before the beam is traced.

.. note::

    If a beam trace has not been performed yet on a model then :attr:`.Model.last_trace` will
    return ``None``. After a beam trace has been carried out then the beam parameters set manually can
    be accessed either via :attr:`.Model.last_trace` or :attr:`.Model.gauss_commands` - the resulting
    values will be the same.

The type of :attr:`.Model.last_trace` is :class:`.TraceSolution` which acts as a wrapper around a Python ordered dictionary. The keys and mapped values of this dictionary are
identical for this attribute and the :attr:`.Model.gauss_commands` dictionary. Keys are
:class:`.OpticalNode` instances and mapped values are a 3-element list formatted as
[qx, qx, :class:`.QSetMode`]. Therefore, one can acess a specific nodes beam parameter in
the following way (where ``L0`` is a :class:`.Laser` instance owned by the model)::

    # model has had a beam trace performed on it in this case
    L0_q = model.last_trace[model.L0.n1.o]
    # then can access the individual parameters with, e.g:
    L0_qx = L0_q.qx

Note that the last element of the mapped tuple is always ``QSetMode.USER`` in the case
of :attr:`.Model.gauss_commands`.

.. rubric:: Setting beam parameters

To set the beam parameter at a given optical node in the model simply requires accessing the
:attr:`.OpticalNode.q` attribute and assigning either a complex number or an instance
of :class:`.BeamParam` to it. An example is given below where ``L0`` is a :class:`.Laser`
instance already owned by the model::

    # set both qx and qy of the output node of L0 using a complex number
    model.L0.n1.o.q = -0.8 + 1.4j
    # set the q value of the same node using a BeamParam instance directly
    model.L0.n1.o.q = BeamParam(q=(-0.5 + 1j))

When assigning a beam parameter in this way, the :meth:`.Model.set_q_at` method is called
on the node and an entry in the :attr:`.Model.gauss_commands` dictionary is created for
this node and its associated beam parameter. These values are then used when performing
beam tracing on the model.

One can also call :meth:`.Model.set_q_at` directly to set the beam parameter at a node,
the equivalent code to the above case would then be::

    model.set_q_at(model.L0.n1.o, BeamParam(q=(-0.5 + 1j)), direction="both")

where ``direction="both"`` means that both the tangential and sagittal plane parameters
are set to the specified value - ``direction`` can be set to 'x' or 'y' to just set
one of these two planes, respectively.
