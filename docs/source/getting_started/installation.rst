Installation
============

Release version
---------------


Development version
-------------------

Finesse 3 is written primarily in Python, therefore you will need to install the Python ecosystem first. We recommend
using the `Conda package manager <https://docs.conda.io/en/latest/miniconda.html>`_ to do this as this will make it easier to
install Finesse and update requirements in your environment later on.

To get the latest development version of Finesse 3 you need to `clone the repository <https://git.ligo.org/finesse/finesse3>`_. Do
this by opening a terminal and changing directory to where you want to store your local copy of the code, then run::


    git clone https://git.ligo.org/finesse/finesse3


The master branch will be the only branch guaranteed to be stable so it is recommended to stay on this branch (which will be
the default when cloning the repository) whilst using the development version of Finesse 3.

Once you have cloned the repository and installed Anaconda / Miniconda, open a terminal and change directory to your
local copy of the repository. Then run the following to create a new Conda environment and install the packages required
for Finesse 3 into this environment (substitute ``<name_of_environment>`` for a suitable name, e.g. ``finesse3env``)::

    conda env create -n <name_of_environment> -f environment.yml

Or if you already have an existing environment in which you want to run Finesse 3 then type the following instead::

    conda env update -f environment.yml

To then install Finesse 3 in this environment, simply run::

    make
    pip install -e .

The first step builds all the Cython extensions used by Finesse 3 whilst the second installs the package
in editable mode so that it can be used from any directory on your machine, with any changes made to the
source files automatically carrying over. Note that the ``.`` here in the command assumes that you are within
the root directory of your local copy of the Finesse 3 repository (i.e. the directory where ``setup.py`` sits).
