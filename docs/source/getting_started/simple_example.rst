.. _simple_example:

A Simple Example
================

The best way to familiarise yourself with Finesse is with example models. In this
section we will cover the simple case of modelling resonances in a Fabry-Perot cavity
as a starting point.

The Optical Configuration
-------------------------

In the figure below we show the layout that we want to model. This consists of an input
laser incident upon a two-mirror optical cavity. Measuring the reflected, transmitted
and circulating power requires the use of three photodiodes - PD\ :sub:`refl`, PD\ :sub:`trans`
and PD\ :sub:`circ`, respectively.

.. figure:: ../../resources/fabry_perot.png
    :align: center

To find the resonances of this cavity we can either change the tuning(s) of the mirrors
or scan over the frequency of the input laser. In this case we will alter the tuning of
the end test mass (ETM) in the range :math:`\phi_{\mathrm{ETM}} \in [-450^{\circ}, 90^{\circ}]` to
show multiple (repeated) resonances.

The Finesse Model
-----------------

To represent the above layout with a Finesse model we can write the following code:

Plotting the Outputs
--------------------
