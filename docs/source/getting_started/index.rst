.. _getting_started:

Getting Started
===============

This section contains information regarding the key concepts of Finesse and
how to use them to produce accurate frequency-domain models of any interferometer
configuration you are considering.

.. toctree::
    :maxdepth: 1

    installation
    key_concepts
    simple_example
    changes
