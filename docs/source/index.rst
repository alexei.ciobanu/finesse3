.. Finesse 3 documentation master file

.. only:: latex

    =======
    Finesse
    =======

.. only:: not latex

    .. include:: title.rst

    ----

.. toctree::
    :maxdepth: 1
    :name: sec-intro

    introduction/index

.. toctree::
    :maxdepth: 1
    :name: sec-usage

    getting_started/index
    manual/index
    usage/index
    api/index
    developer/index

.. toctree::
    :maxdepth: 1
    :name: sec-back

    backmatter/contributors
    backmatter/todo
    zbiblio/index
