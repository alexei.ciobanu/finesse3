.. include:: /defs.hrst
.. _abcd_manual:

ABCD matrices
=============

The transformation of the beam parameter can be performed by the
ABCD matrix-formalism :cite:`siegman`. When a beam passes a mirror, beam splitter, lens
or free space, a beam parameter :math:`q_1` is transformed to :math:`q_2`. This
transformation can be described by four real coefficients like so:

.. math::
    \frac{q_2}{n_2}=\frac{A\frac{q_1}{n_1}+B}{C\frac{q_1}{n_1}+D},

with the coefficient matrix,

.. math::
    M=\left( \begin{array}{cc} A& B \\ C& D \end{array} \right),

and :math:`n_1` being the index of refraction at the beam segment defined by
:math:`q_1` and :math:`n_2` the index of refraction at the beam segment described by
:math:`q_2`.

The ABCD matrices for the optical components used by |Finesse| are given
below, for the sagittal and tangential plane respectively.

Transmission through a mirror
-----------------------------

A mirror in this context is a single, partly reflecting surface with an
angle of incidence of :math:`90^\circ`. The transmission is described by:

.. math::
    M = \left(\begin{array}{cc} 1 & 0\\ \frac{n_2-n_1}{R_{\rm C}}& 1 \end{array}\right)

with :math:`R_{\rm C}` being the radius of curvature of the spherical surface. The
sign of the radius is defined such that :math:`R_{\rm C}` is negative if the
centre of the sphere is located in the direction of propagation. The
curvature shown above, for example, is described by a
positive radius.

.. _fig_abcd_mirror_transmission:
.. figure:: images/abcd_mi.*
    :align: center

The matrix for the transmission in the opposite direction of propagation is
identical.

Reflection at a mirror
----------------------

The matrix for reflection is given by:

.. math::
    M=\left( \begin{array}{cc} 1 & 0\\ -\frac{2 n_1}{R_{\rm C}}& 1 \end{array} \right)

The reflection at the back surface can be described by the same type of
matrix by setting :math:`C=2n_2/R_{\rm C}`.

.. _fig_abcd_mirror_reflection:
.. figure:: images/abcd_mr.*
    :align: center

Transmission through a beam splitter
------------------------------------

A beam splitter is understood as a single surface with an arbitrary angle of
incidence :math:`\alpha_1`.  The matrices for transmission and reflection are
different for the sagittal and tangential planes (:math:`M_{\rm s}` and :math:`M_{\rm
t}`):

.. math::
    M_{\rm t}=\left(
        \begin{array}{cc}
            \frac{\mCos{\alpha_2}}{\mCos{\alpha_1}} & 0\\
            \frac{\Delta n}{R_{\rm C}} & \frac{\mCos{\alpha_1}}{\mCos{\alpha_2}}
        \end{array}
    \right),

.. math::
    M_{\rm s}=\left(
        \begin{array}{cc}
            1 & 0\\
            \frac{\Delta n}{R_{\rm C}} &1
        \end{array}
    \right),

with :math:`\alpha_2` given by Snell's law:

.. math::
    n_1\sin{\alpha_1} = n_2\sin{\alpha_2},

and :math:`\Delta n` for the tangential beam is:

.. math::
    \Delta n=\frac{n_2\mCos{\alpha_2}-n_1\mCos{\alpha_1}}{\mCos{\alpha_1}\mCos{\alpha_2}},

and for the sagittal beam,

.. math::
    \Delta n=n_2\mCos{\alpha_2}-n_1\mCos{\alpha_1}.

If the direction of propagation is reversed, the matrix for the sagittal
plane is identical and the matrix for the tangential plane can be obtained
by changing the coefficients A and D as follows:

.. math::
    \begin{array}{l}
        A\longrightarrow1/A,\\
        D\longrightarrow1/D.
    \end{array}

.. _fig_abcd_bs_transmission:
.. figure:: images/abcd_bst.*
    :align: center

Reflection at a beam splitter
-----------------------------

The reflection at the front surface of a beam splitter is given by:

.. math::
    M_{\rm t}=\left(
        \begin{array}{cc}
            1 & 0\\
            -\frac{2 n_1}{R_{\rm C} \mCos{\alpha_1}} & 1
        \end{array}
    \right)

.. math::
    M_{\rm s}=\left(
        \begin{array}{cc}
            1 & 0\\
            -\frac{2 n_1 \mCos{\alpha_1}}{R_{\rm C} } & 1
        \end{array}
    \right)

To describe a reflection at the back surface the matrices have to be changed as follows:

.. math::
    \begin{array}{l}
        R_{\rm C}\longrightarrow-R_{\rm C},\\
        n_1\longrightarrow n_2,\\
        \alpha_1\longrightarrow-\alpha_2.
    \end{array}

.. _fig_abcd_bs_reflection:
.. figure:: images/abcd_bsr.*
    :align: center

Transmission through a thin lens
--------------------------------

A thin lens transforms the beam parameter as follows:

.. math::
    M=\left(
        \begin{array}{cc}
            1 & 0\\
            -\frac{1}{f}& 1
        \end{array}
    \right)

where :math:`f` is the focal length.  The matrix for the opposite direction of
propagation is identical.  Please note that a thin lens has to be surrounded
by spaces with index of refraction :math:`n=1`.

.. _fig_abcd_lens_transmission:
.. figure:: images/abcd_lenst.*
    :align: center

Transmission through a free space
---------------------------------

As mentioned above, the beam in free space can be described by one base
parameter :math:`q_0`. In some cases it is convenient to use a similar matrix as
for the other components to describe the :math:`z`-dependency of :math:`q(z)=q_0+z`.
On propagation through a free space of the length :math:`L` and index of refraction
:math:`n` the beam parameter is transformed as follows:

.. math::
    M=\left(
        \begin{array}{cc}
            1 & \frac{L}{n}\\
            0& 1
        \end{array}
    \right)

The matrix for the opposite direction of propagation is identical.

.. _fig_abcd_space_transmission:
.. figure:: images/abcd_spacet.*
    :align: center

Reflection at a grating
-----------------------

Consider a curved diffraction grating with radius of curvature :math:`R`, ruling
in the :math:`y`-direction and grating spacing :math:`d` in the :math:`x`-direction. An
incident beam striking the grating at an angle :math:`\alpha` from the normal in
the :math:`x-z` plane will be diffracted in m-th order into angle :math:`\phi_m` in the
same plane given by the grating equation.

The matrix for reflection from the grating in the tangential or :math:`x-z` plane
is given by:

.. math::
    M_{\rm t}=\left(
        \begin{array}{cc}
            M & 0\\
            -2/R_t &1/M\\
        \end{array}
    \right),

.. math::
    M_{\rm s}=\left(
        \begin{array}{cc}
            1 & 0\\
            -2/R_s &1\\
        \end{array}
    \right)

with :math:`M` given by:

.. math::
    M~=~\cos(\phi_m)/\cos(\alpha),

and effective radii as:

.. math::
    R_t~=~R~\frac{\cos(\alpha)\cos(\phi_m)}{\cos(\alpha)+\cos(\phi_m)},

.. math::
    R_s~=~\frac{2\,R}{\cos(\alpha)+\cos(\phi_m)}.
