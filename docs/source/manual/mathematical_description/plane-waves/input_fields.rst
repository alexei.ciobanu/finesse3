.. include:: /defs.hrst

Input fields or the 'right hand side' vector
============================================

After the set of linear equations for a system has been determined, the input
fields have to be given by the user. The respective fields are entered into the
'right hand side' (RHS) vector of the set of linear equations. The RHS vector
consists of complex numbers that specify the amplitude and phase of every input
field. Input fields are initially set to zero, and every non-zero entry
describes a source. The possible sources in the carrier simulation are lasers
and modulators, and in the signal simulation the only sources are signal
generators.

.. contents:: :local:
   :depth: 2

Carrier frequencies
-------------------

Laser
~~~~~

The principal light sources are, of course, the lasers. They are connected to
one optical node only. The input power is specified by the user in the input
file. For every laser the field amplitude is set as:

.. math::
   a_{in} = C \sqrt{(P/\epsilon_c)}~e^{\I \varphi},

with

.. math::
   P = C^2|a|^2,

as the laser power and :math:`\varphi` the specified phase. The constant factor
:math:`C` depends on the chosen internal scaling for the field amplitudes that
can be set in the init file 'kat.ini'. The default value is :math:`C=1`. This
setting does not yield correct absolute values for light field amplitudes, i.e.
when amplitude detectors are used. Instead, one obtains more intuitive numbers
from which the respective light power can be easily computed.

Modulators
~~~~~~~~~~

Modulators produce non-zero entries in the carrier RHS vector for every
modulation sideband generated. Depending on the order (:math:`k\ge 0`) and the
modulation index (:math:`m`), the input field amplitude for amplitude
modulation is:

.. math::
   a_{\rm in} = \frac{m}{4},

and for phase modulation:

.. math::
   a_{\rm in} = (-1)^k ~ J_k(m) ~ \mEx{\I\varphi},

with :math:`\varphi` given as (Equation :eq:`bessel0`):

.. math::
   \varphi = \pm k \cdot (\frac{\pi}{2} + \varphi_{\rm s}),

where :math:`\varphi_{\rm s}` is the user-specified phase from the modulator
description. The sign of :math:`\varphi` is the same as the sign of the
frequency offset of the sideband. For 'lower' sidebands (:math:`f_{\rm
mod}<0`) we get :math:`\varphi=-\dots`, for 'upper' sidebands (:math:`f_{\rm
mod}>0`) it is :math:`\varphi=+\dots`.

Signal frequencies
------------------

The most complex light fields are the signal sidebands. They can be generated
by many different types of modulation inside the interferometer (signal
modulation in the following). The components mirror, beam splitter, space,
laser and modulator can be used as a source of optical signal sidebands.
Primarily, signal sidebands are used as the input signal for computing transfer
functions of the optical system. The amplitude, in fact the modulation index,
of the signal is assumed to be much smaller than unity so that the effects of
the modulation can be described by a linear analysis. If linearity is assumed,
however, the computed transfer functions are independent of the signal
amplitude; thus, only the relative amplitudes of output and input are
important, and the modulation index of the signal modulation can be arbitrarily
set to unity in the simulation.

Contrary to previous versions, |Finesse| 3 includes electronic and mechanical
components in the signal simulation, in addition to optics. There is also only
one component that can create signals in the RHS vector, the signal generator.
This creates an electrical signal which can then be fed into the modulation
inputs of various optical components. The output of any other electrical
component can also be used in this way to generate optical signals, allowing
the creation of feedback loops and other such constructs. Signal frequencies
can be 'applied' to a number of different components using the command
``fsig``. This will create a signal generator, and attach it to the relevant
port of the specified component. The connection of the signal frequency causes
the component to in some way modulate the light fields at the component. The
frequency, amplitude and phase of the modulation can be specified by ``fsig``.

|Finesse| always assumes a numerical signal amplitude of 1. The numerical value
of 1 has a different meaning for applying signals to different components (see
below). The amplitude specified with ``fsig`` can be used to define the
relative amplitudes of the source when the signal is applied to several
components at once. Please note that |Finesse| does not correct the transfer
functions for strange amplitude settings. An amplitude setting of two, for
example, will scale the output (a transfer function) by a factor of two.

In order to have a determined number of light fields and electrical and
mechanical frequencies, the signal modulation of a signal sideband has to be
neglected. This approximation is sensible because in the steady state the
signal modulations are expected to be tiny so that second-order effects (signal
modulation of the signal modulation fields) can be omitted.

In general, the carrier field at the 'signal component' can be written as:

.. math::
   E_{in} ~ = ~ E_0 ~ \mEx{\I\w_c\T + \varphi_c},

with :math:`\w_c` the carrier frequency, and :math:`\varphi_c` the phase of the
carrier. In most cases the modulation of the light will be a phase modulation.
Then the field after the modulation can be expressed in general as:

.. math::
   E_{out} ~ = ~ A ~ E_0 ~ \mEx{\I\w_c\T + \varphi_c + \varphi(t) + B},

with :math:`A` as a real amplitude factor, :math:`B` a constant phase term and

.. math::
   \varphi(t)~=~m\cos{(\w_s\T +\varphi_s)},

with :math:`m` the modulation index, :math:`\w_s` the signal frequency and
:math:`\varphi_s` the phase as defined by ``fsig``. The modulation index will
in general depend on the signal amplitude :math:`a_s` as given by ``fsig`` and
also other parameters (see below). As mentioned in Section :ref:`trans+err`,
the simple form for very small modulation indices (:math:`m \ll 1`) can be
used: only the two sidebands of the first order are taken into account and the
Bessel functions can be approximated by:

.. math::
   \begin{array}{l}
      J_0(m) \approx 1,\\
      J_{\pm1}(m) \approx \pm\frac{m}{2}.
   \end{array}

Thus, the modulation results in two sidebands ('upper' and 'lower' with a
frequency offset of :math:`\pm\w_s` to the carrier) which can be written as:

.. math::
   \begin{array}{rcl}
      E_{sb} &=& \I\frac{m}{2} ~ A ~ E_0 ~
         \mEx{\I((\w_c\pm\w_s)\T + \varphi_c + B \pm \varphi_s)}\\
      &=&\frac{m}{2} ~ A ~ E_0 ~
         \mEx{\I(\w_{sb}\T + \pi / 2 + \varphi_c + B \pm \varphi_s)}.
   \end{array}

Mirror
~~~~~~

.. _fig_mirror_mod:
.. figure:: images/mirror_modulation.*
   :align: center

   Signal applied to a mirror: modulation of the mirror position.

Mirror motion does not change transmitted light. The reflected light will be
modulated in phase by any mirror movement. The relevant parameters are shown in
:numref:`fig_mirror_mod`. At a reference plane (at the nominal mirror
position when the tuning is zero), the field impinging on the mirror is:

.. math::
   E_{\rm in} ~ = ~ E_0 ~ \mExB{\I(\w_{\rm c}t + \varphi_{\rm c} - k_{\rm c}x)}
      ~ = ~ E_0 ~ \mEx{\I\w_{\rm c}t + \varphi_{\rm c}}.

If the mirror is detuned by :math:`x_{\rm t}` (here given in meters) then the
electric field at the mirror is:

.. math::
   E_{\rm mir} ~ = ~ E_{\rm in} ~ \mEx{-\I k_{\rm c} x_{\rm t}}.

With the given parameters for the signal frequency, the position modulation can
be written as :math:`x_{\rm m} = a_{\rm s}\cos(\w_{\rm s}t + \varphi_{\rm s})`
and thus the reflected field at the mirror is:

.. math::
   E_{\rm refl} ~ = ~ r ~ E_{\rm mir} ~ \mEx{\I2k_{\rm c} x_{\rm m}}
      ~ = ~ r ~ E_{\rm mir} ~
      \mExB{\I2k_{\rm c} a_{\rm s}\cos(\w_{\rm s}t + \varphi_{\rm s})},

setting :math:`m=2k_{\rm c} a_{\rm s}`, this can be expressed as:

.. math::
   \begin{array}{rcl}
      E_{\rm refl} &=& r ~ E_{\rm mir} ~
         \Bigl(
            1 + \I\frac{m}{2}\mExB{-\I(\w_{\rm s}t + \varphi_{\rm s})}
              + \I\frac{m}{2}\mExB{\I(\w_{\rm s}t + \varphi_{\rm s})}
         \Bigr)\\
      &=& r ~ E_{\rm mir} ~
         \Bigl(
            1 + \frac{m}{2}\mExB{-\I(\w_{\rm s}t + \varphi_{\rm s} - \pi/2)} ~
         \Bigr)\\
         & & +
         \Bigl(
            \frac{m}{2}\mExB{\I(\w_{\rm s}t + \varphi_{\rm s} + \pi/2)}
         \Bigr).
   \end{array}

This gives an amplitude for both sidebands of:

.. math::
   a_{\rm sb}~=r~m/2~E_0~=~r~k_{\rm c} a_{\rm s}~E_0.

The phase back at the reference plane is:

.. math::
   \varphi_{\rm sb} ~ = ~ \varphi_{\rm c} + \frac{\pi}{2} \pm \varphi_{\rm s}
      - (k_{\rm c} + k_{\rm sb}) ~ x_{\rm t},
   :label: plane-phisb

where the plus sign refers to the 'upper' sideband and the minus sign to the
'lower' sideband. As in |Finesse| the tuning is given in degrees, i.e. the
conversion from :math:`x_{\rm t}` to :math:`\Tun` has to be taken into account:

.. math::
   \begin{array}{rcl}
      \varphi_{\rm sb} &=& \varphi_{\rm c} + \frac{\pi}{2} \pm \varphi_{\rm s}
         - (\w_{\rm c} + \w_{\rm sb})/c ~ x_{\rm t}\\
      &=& \varphi_{\rm c} + \frac{\pi}{2} \pm \varphi_{\rm s}
         - (\w_{\rm c} + \w_{\rm sb})/c ~ \lambda_0/360 ~ \Tun\\
      &=& \varphi_{\rm c} + \frac{\pi}{2} \pm \varphi_{\rm s}
         - (\w_{\rm c} + \w_{\rm sb})/\w_0 ~ 2\pi/360 ~ \Tun.
   \end{array}

With a nominal signal amplitude of :math:`a_{\rm s}=1`, the sideband amplitudes
become very large. For an input light field at the default wavelength one
typically obtains:

.. math::
   a_{\rm sb} = r ~ k_{\rm c} ~ E_0
   = r ~ \w_{\rm c}/c ~ E_0
   = r ~ 2\pi/\lambda_0 ~ E_0
   \approx 6 \cdot 10^6.

Numerical algorithms have the best accuracy when the various input numbers are
of the same order of magnitude, usually set to a number close to one.
Therefore, the signal amplitudes for mirrors (and beam splitters) should be
scaled: a natural scale is to define the modulation in radians instead of
meters. The scaling factor is then :math:`\w_0/c`, and setting
:math:`a=\w_0/c~a'` the reflected field at the mirror becomes:

.. math::
   \begin{array}{rl}
      E_{\rm refl} &= ~ r ~ E_{\rm mir} ~
         \mEx{\I2 \w_{\rm c}/\w_0 ~ x_{\rm m}}\\
      &= ~ r ~ E_{\rm mir} ~
         \mExB{\I2 \w_{\rm c}/\w_0 ~ a'_{\rm s}\cos(\w_{\rm s}t
               + \varphi_{\rm s})},
   \end{array}

and thus the sideband amplitudes are:

.. math::
   a_{\rm sb} ~ = r ~ \w_{\rm c}/\w_0 ~ a'_{\rm s} ~ E_0,

with the factor :math:`\w_{\rm c}/\w_0` typically being close to one. The units
of the computed transfer functions are 'output unit per radian'; which are
neither common nor intuitive. The command ``scale meter`` converts the units
into the more common 'Watts per meter' by applying the inverse scaling factor
:math:`c/\w_0`.

When a light field is reflected at the back surface of the mirror, the sideband
amplitudes are computed accordingly. The same formulae as above can be applied
with :math:`x_{\rm m} \rightarrow -x_{\rm m}` and :math:`x_{\rm t} \rightarrow
-x_{\rm t}`, yielding the same amplitude as for the reflection at the front
surface, but with a slightly different phase:

.. math::
   \begin{array}{rcl}
      \varphi_{\rm sb,back} &=& \varphi_{\rm c} + \frac{\pi}{2}
         \pm (\varphi_{\rm s} + \pi) + (k_{\rm c} + k_{\rm sb}) ~ x_{\rm t}\\
      &=& \varphi_{\rm c} + \frac{\pi}{2} \pm (\varphi_{\rm s} + \pi) +
         (\w_{\rm c} + \w_{\rm sb})/\w_0 ~ 2\pi/360 ~ \Tun.
   \end{array}

Beam splitter
~~~~~~~~~~~~~

When the signal frequency is applied to the beam splitter, the reflected light
is modulated in phase. In fact, the same computations as for mirrors can be
used for beam splitters. However, all distances have to be scaled by
:math:`\cos(\alpha)` (see Section :ref:`coupling`). Again, only the reflected
fields are changed by the modulation and the front side and back side
modulation have different phases. The amplitude and phases compute to:

.. math::
   \begin{aligned}
      a_{\rm sb} ~ &= r ~ \w_c/\w_0 ~ a_{\rm s}\cos(\alpha) ~ E_0,\\
      \phi_{\rm sb,front} ~ &= ~ \varphi_c + \frac{\pi}{2} \pm \varphi_{\rm s}
         - (k_{c} + k_{sb}) x_t\cos(\alpha),\\
      \phi_{\rm sb,back} ~ &= ~ \varphi_c + \frac{\pi}{2}
         \pm (\varphi_{\rm s} + \pi) + (k_{c} + k_{sb}) x_t\cos(\alpha).
   \end{aligned}

Space
~~~~~

For interferometric gravitational wave detectors, the 'free space' is an
important source for a signal frequency: a passing gravitational wave modulates
the length of the space (i.e. the distance between two test masses). A light
field passing this length will thus be modulated in phase. The phase change
:math:`\phi(t)` which is accumulated over the full length is (see, for example,
:cite:`Mizuno95`):

.. math::
   \phi(t) ~ = ~ \frac{\w_c ~ n ~ L}{c} + \frac{a_s}{2}\frac{\w_c}{\w_s}
      \sin{\left(\w_s\frac{n ~ L}{c}\right)}
      \cos{\left(\w_s\left(t - \frac{n ~ L}{c}\right)\right)},

with :math:`L` the length of the space, :math:`n` the index of refraction and
:math:`a_s` the signal amplitude given in strain (h). This results in a signal
sideband amplitude of:

.. math::
   \begin{aligned}
      a_{\rm sb} ~ &= ~ \frac{1}{4} ~ \frac{\w_c}{\w_s}
         \sin{\left(\w_s\frac{n ~ L}{c}\right)} a_{\rm s} ~ E_0,\\
      \phi_{\rm sb} ~ &= ~ \varphi_c + \frac{\pi}{2} \pm \varphi_s
         - (\w_c + \w_s)\frac{nL}{c}.
   \end{aligned}

Laser
~~~~~

A laser has three types of signal modulation available: phase, frequency and
amplitude. These are treated as in :ref:`modulation_of_light_fields`.

Frequency
`````````

Frequency modulation gives a field

.. math::
   E ~ = ~ E_0
      ~ e^{\I (\w_c\,t + a_s/\w_s\cos(\w_s\,t + \varphi_s) + \varphi_c)},

with sideband amplitude and phase

.. math::
   \begin{array}{l}
      a_{sb} = \frac{a_{\rm s}}{2 \w_{\rm s}} ~ E_0,\\
      \phi_{\rm sb} = \varphi_c + \frac{\pi}{2} \pm \varphi_s.
   \end{array}

Phase
`````

With phase modulation, the laser produces a field

.. math::
    E = E_0 ~
      \exp\Bigl(
         \mathrm{i}\,(\omega_0\,t + a_{\rm s}\cos{\left(\omega_s\,t\right)})
      \Bigr),

giving sidebands with

.. math::
   \begin{array}{l}
      a_{sb} = \frac{a_{\rm s}}{2} ~ E_0,\\
      \phi_{\rm sb} = \varphi_c \pm \frac{\pi}{2} \pm \varphi_s.
   \end{array}

Amplitude
`````````

Amplitude modulation gives us

.. math::
   \begin{array}{lcl}
      E &=& E_0 ~ \mEx{\mathrm{i}\,\omega_0\,t}
         ~ \left(
            1 - \frac{m}{2}\Bigl(1 - \cos{\left(\omega_m \,t\right)}\Bigr)
         \right),
   \end{array}

with sidebands

.. math::
   \begin{array}{l}
      a_{sb} = \frac{a_{\rm s}}{4} ~ E_0,\\
      \phi_{\rm sb} = \varphi_c \pm \varphi_s.
   \end{array}

Modulator
~~~~~~~~~

Signal frequencies at a modulator are treated as 'phase noise'. See Section
:ref:`phasenoise` for details. The electric field that leaves the modulator can
be written as:

.. math::
   \begin{array}{rl}
      E ~ = ~ E_0 & e^{\I(\w_0 \T + \varphi_0)}
         ~ \sum_{k=-order}^{order}
            i^{\,k} ~ J_k(m) ~ e^{\I k(\w_m\T + \varphi_m)}\\[5pt]
      & \times \left(
            1 + \I\frac{k ~ m_2 ~ a_s}{2} ~ e^{-i(\w_s\T + \varphi_s)}
            + \I\frac{k ~ m_2 ~ a_s}{2} ~ e^{\I(\w_s\T + \varphi_s)}
            + O((km_2)^2)
         \right),
   \end{array}

with

.. math::
   \begin{array}{l}
      E_{\rm mod} ~ = ~ E_0 ~ J_k(m),\\
      \varphi_{\rm mod} = \varphi_0 + k\frac{\pi}{2} + k\varphi_m.
   \end{array}

The sideband amplitudes are:

.. math::
   \begin{array}{l}
      a_{sb} = \frac{a_{\rm s}k m_2}{2} ~ E_{\rm mod},\\
      \phi_{\rm sb} = \varphi_{\rm mod} + \frac{\pi}{2} \pm \varphi_s.
   \end{array}

TODO
----

- Swap from using :math:`a_s` to :math:`a_e` (?), and mention electrical inputs
  rather than fsig everywhere

- Update each components signal modulation text as we go

- Insert something like the following, maybe not in this section:
  
  - As we must generate a conjugate pair of sidebands, one may think that we
    need to carry two electrical fields throughout the simulation. To get
    around this, |Finesse| instead carries the *conjugate* of the lower optical
    sideband everywhere, so we have for e.g. amplitude modulation of a laser:

    .. math::
      \begin{array}{l}
         a_+ = C\frac{E_0}{4} a_e \\[5pt]
         a_-^* = C\frac{E_0^*}{4} a_e
      \end{array}
