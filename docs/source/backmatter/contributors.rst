.. _contributors:

.. include:: /defs.hrst

Contributors (the |Finesse| team)
=================================

Towards |Finesse| 3 (2018)
--------------------------

Daniel Brown and Andreas Freise have started the re-implementation of
|Finesse| in Python, with the idea to provide a modern and clean
code base that makes further developing and extending the software
simpler, especially for external contributors. The aim is to merge
the established features and reliability of |Finesse| with a modular
and hackable object based design, and to add some cool new
features in the process!

Further contributors to the |Finesse| development are:

 * Philip Jones, PhD student at the University of Birmingham, ...
 * Sean Leavey, ...
 * Samuel Rowlinson, PhD student at the University of Birmingham, ...

The beginnings (1997 - 2010)
----------------------------

|Finesse| has been developped by Andreas Freise during his PhD
at |GEO600|.
Gerhard Heinzel greatly supported the orginal development;
he had the idea of using the LISO routines on interferometer problems
and he provided his
code for that purpose. Furthermore he has been very busy as the first
beta tester and has helped to develop the right ideas in many discussions.
Andreas received many helpful bug reports from Guido Müller early on and always
enjoyed discussing interferometer configurations with him. The latter is
also true for Roland Schilling: He woudl spend hours with Andreas on the phone
dicsussing C and Fortran, or interferometers and optics.
Ken Strain has been a constant source of help and support during the initial
years of development. During Andreas' time at the Virgo detector
Gabriele Vajente and Maddalena Mantovani have
acted as faithful test pilots for the extension with the ``lock`` command.
Alexander Bunkowski has initiated and helped debugging the grating
implementation. Jerome Degallaix has often helped with suggestions,
examples and test results based on his code OSCAR
to further develop and test |Finesse|. Paul Cochrane
has made a big difference with his help on transforming the source code from its
messy original form into a more professional package, including a testsuite,
an API documentation and above all a readable source code.

|Finesse| 1 and 2 (2011-2017)
-----------------------------

At the University of Birmingham Andreas Freise encouraged
several members of his research group to start a new phase of
development, testing and using |Finesse|. Daniel Brown became lead
programmer and provided a large number of bug fixes and new
features navigating the tricky grounds of optics with high-order
modes. Due to his work, |Finesse| reached version 1.0 and was made
available as open source. Charlotte Bond became a specialist in using
|Finesse|, in particular with mirror surface maps or strange beam shapes;
and her help with |Finesse| has been invaluable for getting the physics
of higher-order modes right. Further, Keiko Kokeyama, Paul Fulda and Ludovico
Carbone have worked very hard to help making |Finesse| do useful things for
the Advanced LIGO commissioning team.

In 2013 Daniel Brown implemented the long requested
feature of radiation pressure effects in addition to a full
quantum noise treatment in the two-photon formalism. Daniel was
supported in this activity by other members of Andreas' research group,
especially Mengyao Wang and Rebecca Palmer; and we once again could
rely on crucial assistance from Jan Harms.

Acknowledgements
----------------
Many people in the gravitational wave community have helped with
feedback, bug reports and encouragement. Some of them are Seiji Kawamura,
Simon Chelkowski, Keita Kawabe, Osamu Miyakawa, Rainer Künnemeyer,
Uta Weiland, Michaela Malec, Oliver Jennrich, James Mason, Julien Marque,
Mirko Prijatelj, Jan Harms, Oliver Bock, Kentaro Somiya, Antonio
Chiummo, Holger Wittel, Hartmut Grote, Bryan Barr, Sabina Huttner,
Haixing Miao, Benjamin Jacobs, Stefan Ballmer, Nicolas Smith-Lefebvre,
Daniel Shaddock and probably many more not mentioned here.

Last but not least we would like to thank the |GEO600| group, especially
Karsten Danzmann and Benno Willke, who allowed Andreas to work on |Finesse|
in parallel to his experimental work on the GEO site. |Finesse| would not
exist without their positive and open attitude towards young scientists.
