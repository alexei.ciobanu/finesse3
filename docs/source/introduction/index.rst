.. _intro:
.. include:: /defs.hrst

Introduction to Finesse
=======================

|Finesse| is a simulation program for interferometers.  The user can build
any kind of virtual interferometer using the following components:

- lasers, with user-defined power, wavelength and shape of the output beam;
- free spaces with arbitrary index of refraction;
- mirrors and beam splitters, with flat or spherical surfaces;
- modulators to change amplitude and phase of the laser light;
- amplitude or power detectors with the possibility of demodulating the detected signal with one or more given demodulation frequencies;
- lenses and isolators.

For a given optical setup, the program computes the light field amplitudes
at every point in the interferometer assuming a steady state.  To do so, the
interferometer description is translated into a set of linear equations that
are solved numerically. For convenience, a number of standard analyses can
be performed automatically by the program, namely computing
modulation-demodulation error signals and transfer functions.  |Finesse| can
perform the analysis using plane waves or Hermite-Gauss modes. The latter allows
computation of the effects of mode matching and misalignments. In addition,
error signals for automatic alignment systems can be simulated.

.. figure:: /images/pound_drever_hall01.*
    :align: center

    A schematic diagram of a laser interferometer which can be
    modelled using |Finesse| (in this case a Fabry-Perot cavity with a
    Pound-Drever-Hall control scheme).


Literally every parameter of the interferometer description can be tuned
during the simulation. The typical output is a plot of a photodetector
signal as a function of one or two  parameters of the interferometer
(e.g. arm length, mirror reflectivity, modulation frequency, mirror
alignment).  Optional text output provides information about the optical setup
including, but not limited to, mode mismatch coefficients, eigenmodes of cavities
and beam sizes.

|Finesse| provides a fast and versatile tool that has proven to be very
useful during design and commissioning of interferometric gravitational wave
detectors.  However, the program has been designed to allow the analysis of
arbitrary, user-defined optical setups. In addition, it is easy to install
and easy to use.  Therefore |Finesse| is very well suited to study basic
optical properties, like, for example, the power enhancement in a resonating
cavity or modulation-demodulation methods.

Motivation and History
**********************

The search for gravitational waves with interferometric detectors has led
to a new type of
laser interferometer: new topologies are formed combining known
interferometer types.  In addition, the search for gravitational waves requires optical
systems with a very long baseline, large circulating power and an enormous
stability.  The properties of this new class of laser interferometers have
been the subject of extensive research.


.. figure:: /images/geo600_birds_eye.jpg
    :align: center

    Bird's eye view of the |GEO600| gravitational wave detector
    near Hannover, Germany. Image courtesy of Harald Lück, Albert
    Einstein Institute Hannover.

Several prototype interferometers have been built during the last few
decades to investigate their performance in detecting gravitational waves.
The optical systems, Fabry-Perot cavities, a Michelson interferometer and combinations thereof are in
principle simple and have been used in many fields of science for many
decades.  The sensitivity required for the detection of the expected small
signal amplitudes of gravitational waves, however, has put new constraints on the design of
laser interferometers.  The work of the gravitational wave research groups has led to a new
exploration of the theoretical analysis of laser interferometers.
Especially, the clever combination of known interferometers has produced new
types of interferometric detectors that offer an optimised sensitivity for
detecting gravitational waves.  Work on prototype interferometers has shown that the models describing the
optical system become very complex even though they are based on simple
principles.  Consequently, computer programs have been developed to automate
the computational part of the analysis.  To date, several programs for
analysing optical systems are available to the gravitational wave community :cite:`gwic`.

The idea for |Finesse| was first raised in 1997, when (Andreas) was visiting
the Max-Planck-Institute for Quantum Optics
in Garching, to assist Gerhard Heinzel with his work on Dual Recycling at
the 30 m prototype interferometer.  We were using optical simulations which
were rather slow and not very flexible.  At the same time Gerhard Heinzel
had developed a linear circuit simulation Liso that used a numerical
algorithm to solve the set of linear equations representing an electronic
circuit. The similarities of the two computational tasks and the outstanding
performance of Liso lead to the idea to use the same methods for an optical
simulation. Gerhard Heinzel kindly allowed me to copy the Liso source code
which saved me much time and trouble in the beginning.

In the following years |Finesse| was continually developed during my work at
the university in Hannover within the GEO project :cite:`geo2010,geo_proposal`.

|Finesse| has been most frequently utilised during the commissioning of
|GEO600|, some of these simulation results have been published in
:cite:`amaldi03_lueck,amaldi03_malec,amaldi03_grote,amaldi03_freise`
and in :cite:`phd.Freise`.

More recently |Finesse| has been used during the commissioning of Advanced LIGO.
|Finesse| is now actively developed as an open source project and it is
now widely used in many other projects; the |Finesse|
home page lists more than 100 documents citing it :cite:`finesse_impact`.

