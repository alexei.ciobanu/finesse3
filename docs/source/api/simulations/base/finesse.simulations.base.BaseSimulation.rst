===========================================================================
``finesse.simulations.base.BaseSimulation`` --- Underlying simulation class
===========================================================================

.. currentmodule:: finesse.simulations.base

Overview
========

.. autoclass:: BaseSimulation

Properties
==========

.. autosummary::
    :toctree: generated/

    BaseSimulation.is_audio
    BaseSimulation.is_modal
    BaseSimulation.frequency_map
    BaseSimulation.model
    BaseSimulation.nodes
    BaseSimulation.homs
    BaseSimulation.frequencies
    BaseSimulation.name
    BaseSimulation.num_equations

Methods
=======

.. rubric:: Constructing a base simulation

.. autosummary::
    :toctree: generated/

    BaseSimulation.__init__

.. rubric:: Indexing and field retrieval

.. autosummary::
    :toctree: generated/

    BaseSimulation.findex
    BaseSimulation.field
    BaseSimulation.get_DC_out
    BaseSimulation.get_frequency_object
