============================================================================
``finesse.simulations.base.BaseMatrixSimulation`` --- Base matrix simulation
============================================================================

.. currentmodule:: finesse.simulations.base

Overview
========

.. autoclass:: BaseMatrixSimulation

Properties
==========

.. autosummary::
    :toctree: generated/

    BaseMatrixSimulation.M
    BaseMatrixSimulation.Mq
