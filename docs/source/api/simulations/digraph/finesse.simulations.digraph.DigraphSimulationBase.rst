=====================================================
``finesse.simulations.digraph.DigraphSimulationBase``
=====================================================


.. currentmodule:: finesse.simulations.digraph


Overview
========

.. autoclass:: DigraphSimulationBase









Methods
=======

.. autosummary::
    :toctree: generated/


	DigraphSimulationBase.print_matrix
	DigraphSimulationBase.active
	DigraphSimulationBase.set_source
	DigraphSimulationBase.component_edge_fill
	DigraphSimulationBase.component_edge_get
	DigraphSimulationBase.solve
	DigraphSimulationBase.get_DC_out