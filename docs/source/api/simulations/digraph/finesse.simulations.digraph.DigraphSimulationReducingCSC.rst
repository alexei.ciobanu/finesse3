============================================================
``finesse.simulations.digraph.DigraphSimulationReducingCSC``
============================================================


.. currentmodule:: finesse.simulations.digraph


Overview
========

.. autoclass:: DigraphSimulationReducingCSC









Methods
=======

.. autosummary::
    :toctree: generated/


	DigraphSimulationReducingCSC.solve
	DigraphSimulationReducingCSC.get_DC_out