==========================================================
``finesse.simulations.base`` --- Base simulation interface
==========================================================

.. automodule:: finesse.simulations.base
.. currentmodule:: finesse.simulations.base

Classes
=======

.. autosummary::
    :toctree: base/

    BaseSimulation
    BaseMatrixSimulation
