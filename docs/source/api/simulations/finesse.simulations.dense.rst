=========================================================
``finesse.simulations.dense`` --- Dense matrix simulation
=========================================================

.. automodule:: finesse.simulations.dense
.. currentmodule:: finesse.simulations.dense

Overview
========

.. autoclass:: DenseSimulation
    :show-inheritance:
