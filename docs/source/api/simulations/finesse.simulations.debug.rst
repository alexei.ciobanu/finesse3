======================================================
``finesse.simulations.debug`` --- Debugging simulation
======================================================

.. automodule:: finesse.simulations.debug
.. currentmodule:: finesse.simulations.debug

Overview
========

.. autoclass:: DebugSimulation
    :show-inheritance:
