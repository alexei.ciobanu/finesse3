===============================================================
``finesse.simulations.matrix_digraph.SREdense_numeric_inverse``
===============================================================

.. automodule:: finesse.simulations.matrix_digraph.SREdense_numeric_inverse

.. currentmodule:: finesse.simulations.matrix_digraph.SREdense_numeric_inverse












Functions
=========

.. autosummary::
    :toctree: generated/

        
	SREdense_numeric_inverse