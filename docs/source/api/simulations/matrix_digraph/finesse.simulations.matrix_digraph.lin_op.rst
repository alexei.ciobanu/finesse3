=============================================
``finesse.simulations.matrix_digraph.lin_op``
=============================================

.. automodule:: finesse.simulations.matrix_digraph.lin_op

.. currentmodule:: finesse.simulations.matrix_digraph.lin_op


Overview
========

.. autoclass:: DeferredMatrixStore
            


Enums
=====

.. autosummary::
    :toctree: generated/

        
	MatrixStoreTypes




Properties
==========

.. autosummary::
    :toctree: generated/

        
	DeferredMatrixStore.matrix


Methods
=======

.. autosummary::
    :toctree: generated/

        
	DeferredMatrixStore.__init__
	DeferredMatrixStore.ensure_built
	DeferredMatrixStore.matadd
	DeferredMatrixStore.matmul
	DeferredMatrixStore.matmuladd
	DeferredMatrixStore.matinv
	DeferredMatrixStore.matinvN
	DeferredMatrixStore.abs_sq