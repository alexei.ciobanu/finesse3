=================================================
``finesse.simulations.matrix_digraph.SRE_graphs``
=================================================

.. automodule:: finesse.simulations.matrix_digraph.SRE_graphs

.. currentmodule:: finesse.simulations.matrix_digraph.SRE_graphs












Functions
=========

.. autosummary::
    :toctree: generated/

        
	SRE_graph
	SRABGE_graph