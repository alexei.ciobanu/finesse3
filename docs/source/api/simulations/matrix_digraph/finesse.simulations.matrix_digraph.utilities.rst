================================================
``finesse.simulations.matrix_digraph.utilities``
================================================

.. automodule:: finesse.simulations.matrix_digraph.utilities

.. currentmodule:: finesse.simulations.matrix_digraph.utilities












Functions
=========

.. autosummary::
    :toctree: generated/

        
	SRE_matrix_mult
	SRE_check
	dictset_copy
	SRE_copy
	check_seq_req_balance
	color_purge_inplace
	purge_reqless_inplace
	purge_seqless_inplace
	purge_inplace
	edgedelwarn
	purge_subgraph_inplace
	pre_purge_inplace
	SRABE_copy