=============================================================
``finesse.simulations.matrix_digraph.SREdense_graph_inverse``
=============================================================

.. automodule:: finesse.simulations.matrix_digraph.SREdense_graph_inverse

.. currentmodule:: finesse.simulations.matrix_digraph.SREdense_graph_inverse


Overview
========

.. autoclass:: SREDenseInverter
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	SREDenseInverter.__init__
	SREDenseInverter.set_changing_edges
	SREDenseInverter.reinject_changed_edges
	SREDenseInverter.simplify_trivial
	SREDenseInverter.plot_graph
	SREDenseInverter.numeric_finish


Functions
=========

.. autosummary::
    :toctree: generated/

        
	nullprint
	SRABE_simplify_trivial
	traverse_simple_diagonals_reqs