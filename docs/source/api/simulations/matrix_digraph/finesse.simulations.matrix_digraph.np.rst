=========================================
``finesse.simulations.matrix_digraph.np``
=========================================

.. automodule:: finesse.simulations.matrix_digraph.np

.. currentmodule:: finesse.simulations.matrix_digraph.np












Functions
=========

.. autosummary::
    :toctree: generated/

        
	matrix_stack
	broadcast_deep
	broadcast_shapes