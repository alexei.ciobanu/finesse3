========================================================
``finesse.simulations.matrix_digraph.SRE_presolved_CSC``
========================================================

.. automodule:: finesse.simulations.matrix_digraph.SRE_presolved_CSC

.. currentmodule:: finesse.simulations.matrix_digraph.SRE_presolved_CSC


Overview
========

.. autoclass:: SREPresolvedCSC
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	SREPresolvedCSC.__init__
	SREPresolvedCSC.from_inverter
	SREPresolvedCSC.update_solve


Functions
=========

.. autosummary::
    :toctree: generated/

        
	to_csc