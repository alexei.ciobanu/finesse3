===============================================
``finesse.simulations.matrix_digraph.SRABE_GE``
===============================================

.. automodule:: finesse.simulations.matrix_digraph.SRABE_GE

.. currentmodule:: finesse.simulations.matrix_digraph.SRABE_GE












Functions
=========

.. autosummary::
    :toctree: generated/

        
	reduceGE
	reduceGE_testing