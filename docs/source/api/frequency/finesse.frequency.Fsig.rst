==========================
``finesse.frequency.Fsig``
==========================


.. currentmodule:: finesse.frequency


Overview
========

.. autoclass:: Fsig









Methods
=======

.. autosummary::
    :toctree: generated/


	Fsig.__init__