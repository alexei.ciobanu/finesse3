======================================================
``finesse.solutions.base`` --- Base solution interface
======================================================

.. automodule:: finesse.solutions.base
.. currentmodule:: finesse.solutions.base

Overview
========

.. autoclass:: BaseSolution
