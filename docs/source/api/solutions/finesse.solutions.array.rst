=======================================================
``finesse.solutions.array`` --- Structured array output
=======================================================

.. automodule:: finesse.solutions.array
.. currentmodule:: finesse.solutions.array

Overview
========

.. autoclass:: ArraySolution

Properties
==========

.. autosummary::
    :toctree: generated/

    ArraySolution.axes
    ArraySolution.entries
    ArraySolution.outputs
    ArraySolution.shape

Methods
=======

.. autosummary::
    :toctree: generated/

    ArraySolution.__init__
    ArraySolution.expand
    ArraySolution.plot
    ArraySolution.update
