===========================================
``finesse.plotting`` --- Tools for plotting
===========================================

.. automodule:: finesse.plotting

Functions
=========
.. currentmodule:: finesse.plotting

.. rubric:: Setting the plotting style

.. autosummary::
    :toctree: generated/

    init
    list_styles
    use
    context
