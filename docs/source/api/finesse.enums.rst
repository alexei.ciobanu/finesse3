=================
``finesse.enums``
=================

.. automodule:: finesse.enums

.. currentmodule:: finesse.enums

Overview
========

.. autoclass:: SpatialType