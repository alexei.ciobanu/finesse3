============================================
``finesse.detectors.general.SymbolDetector``
============================================



.. currentmodule:: finesse.detectors.general


Overview
========

.. autoclass:: SymbolDetector
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	SymbolDetector.__init__
	SymbolDetector.get_output