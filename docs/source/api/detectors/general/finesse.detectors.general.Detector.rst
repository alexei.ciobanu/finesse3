======================================
``finesse.detectors.general.Detector``
======================================



.. currentmodule:: finesse.detectors.general


Overview
========

.. autoclass:: Detector
            






Properties
==========

.. autosummary::
    :toctree: generated/

        
	Detector.dtype
	Detector.unit
	Detector.label


Methods
=======

.. autosummary::
    :toctree: generated/

        
	Detector.__init__
	Detector.get_output