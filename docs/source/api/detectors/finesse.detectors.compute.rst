=========================================================
``finesse.detectors.compute`` --- Fast detection routines
=========================================================

.. automodule:: finesse.detectors.compute

.. currentmodule:: finesse.detectors.compute

Functions
=========

.. autosummary::
    :toctree: generated/

    ccd_output
