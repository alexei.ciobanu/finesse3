============================
``finesse.detectors.camera``
============================

.. automodule:: finesse.detectors.camera

.. currentmodule:: finesse.detectors.camera


Classes
=======

.. autosummary::
    :toctree: camera/

        
	Camera
	CCD
	Pixel