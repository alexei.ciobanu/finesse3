=============================
``finesse.detectors.general``
=============================

.. automodule:: finesse.detectors.general

.. currentmodule:: finesse.detectors.general


Classes
=======

.. autosummary::
    :toctree: general/

        
	Detector
	SymbolDetector