.. _amplitude_detector:

==========================================================================================
``finesse.detectors.amplitude_detector`` --- Single-frequency amplitude and phase detector
==========================================================================================

.. automodule:: finesse.detectors.amplitude_detector

Overview
========
.. currentmodule:: finesse.detectors.amplitude_detector
.. autoclass:: AmplitudeDetector
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated

    AmplitudeDetector.f

Methods
=======

.. rubric:: Constructing an amplitude detector

.. autosummary::
    :toctree: generated/

    AmplitudeDetector.__init__

.. rubric:: Computing the output

.. autosummary::
    :toctree: generated/

    AmplitudeDetector.get_output
