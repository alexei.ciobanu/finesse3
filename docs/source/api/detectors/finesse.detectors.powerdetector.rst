.. _powerdetector:

===========================================================================
``finesse.detectors.powerdetector`` --- Laser and electrical power detector
===========================================================================

.. automodule:: finesse.detectors.powerdetector

Overview
========
.. currentmodule:: finesse.detectors.powerdetector
.. autoclass:: PowerDetector
    :show-inheritance:

Methods
=======

.. rubric:: Constructing a power detector

.. autosummary::
    :toctree: generated/

    PowerDetector.__init__

.. rubric:: Obtaining the output

.. autosummary::
    :toctree: generated/

    PowerDetector.get_output
