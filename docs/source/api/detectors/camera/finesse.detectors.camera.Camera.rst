===================================
``finesse.detectors.camera.Camera``
===================================



.. currentmodule:: finesse.detectors.camera


Overview
========

.. autoclass:: Camera
            






Properties
==========

.. autosummary::
    :toctree: generated/

        
	Camera.is_waist_scaled
	Camera.is_monochromatic


Methods
=======

.. autosummary::
    :toctree: generated/

        
	Camera.__init__
	Camera.get_output