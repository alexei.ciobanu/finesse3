================================
``finesse.detectors.camera.CCD``
================================



.. currentmodule:: finesse.detectors.camera


Overview
========

.. autoclass:: CCD
            






Properties
==========

.. autosummary::
    :toctree: generated/

        
	CCD.x
	CCD.y


Methods
=======

.. autosummary::
    :toctree: generated/

        
	CCD.__init__
	CCD.get_output
	CCD.at