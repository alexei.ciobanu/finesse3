==================================
``finesse.detectors.camera.Pixel``
==================================



.. currentmodule:: finesse.detectors.camera


Overview
========

.. autoclass:: Pixel
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	Pixel.__init__
	Pixel.get_output