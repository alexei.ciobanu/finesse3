===========================================================
``finesse.detectors.bpdetector`` --- Beam property detector
===========================================================

.. automodule:: finesse.detectors.bpdetector
.. currentmodule:: finesse.detectors.bpdetector

Overview
========

.. autoclass:: BeamProperty

.. autoclass:: BeamPropertyDetector
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated

    BeamPropertyDetector.detecting

Methods
=======

.. rubric:: Constructing a beam property detector

.. autosummary::
    :toctree: generated/

    BeamPropertyDetector.__init__

.. rubric:: Computing the output

.. autosummary::
    :toctree: generated/

    BeamPropertyDetector.get_output
