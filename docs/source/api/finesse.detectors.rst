============================================
``finesse.detectors`` --- Nonphysical probes
============================================

.. automodule:: finesse.detectors
    :members:
