========================================
``finesse.exceptions`` --- Custom errors
========================================

.. automodule:: finesse.exceptions
.. currentmodule:: finesse.exceptions


Classes
=======

.. autosummary::
    :toctree: exceptions/

    ComponentNotConnected
    NodeException
    ParameterLocked
    TotalReflectionError
