==================================================
``finesse.solutions`` --- Outputs from an analysis
==================================================

.. automodule:: finesse.solutions
    :members:
