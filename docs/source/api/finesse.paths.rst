===============================================
``finesse.paths`` --- Traversed path containers
===============================================

.. automodule:: finesse.paths

.. currentmodule:: finesse.paths

Overview
========

.. autoclass:: OpticalPath

Properties
==========

.. autosummary::
    :toctree: generated/

    OpticalPath.components_only
    OpticalPath.data
    OpticalPath.length
    OpticalPath.nodes_only

Methods
=======

.. rubric:: Constructing an optical path

.. autosummary::
    :toctree: generated/

    OpticalPath.__init__
