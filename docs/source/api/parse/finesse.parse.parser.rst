========================
``finesse.parse.parser``
========================

.. automodule:: finesse.parse.parser

.. currentmodule:: finesse.parse.parser


Overview
========

.. autoclass:: KatParser
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	KatParser.__init__
	KatParser.reset
	KatParser.parse
	KatParser.build


Functions
=========

.. autosummary::
    :toctree: generated/

        
	maxtem
	gauss
	modes