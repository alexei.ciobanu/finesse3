========================
``finesse.parse.legacy``
========================

.. automodule:: finesse.parse.legacy

.. currentmodule:: finesse.parse.legacy


Overview
========

.. autoclass:: KatParser
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	KatParser.__init__
	KatParser.reset
	KatParser.parse
	KatParser.build