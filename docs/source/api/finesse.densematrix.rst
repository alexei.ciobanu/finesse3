=======================
``finesse.densematrix``
=======================

.. automodule:: finesse.densematrix

.. currentmodule:: finesse.densematrix


Overview
========

.. autoclass:: DenseMatrix
        


Properties
==========

.. autosummary::
    :toctree: generated/

        
	DenseMatrix.name
	DenseMatrix.num_equations


Methods
=======

.. autosummary::
    :toctree: generated/

        
	DenseMatrix.__init__
	DenseMatrix.get_sub_matrix_view
	DenseMatrix.get_sub_diagonal_view
	DenseMatrix.add_diagonal_elements
	DenseMatrix.add_block
	DenseMatrix.add_submatrix
	DenseMatrix.construct
	DenseMatrix.get_submatrix
	DenseMatrix.get_matrix_elements
	DenseMatrix.print_matrix
	DenseMatrix.print_rhs
	DenseMatrix.set_rhs
	DenseMatrix.clear_rhs
	DenseMatrix.solve