=================================================
``finesse.analysis.base`` --- Base analysis class
=================================================

.. automodule:: finesse.analysis.base
.. currentmodule:: finesse.analysis.base

Overview
========

.. autoclass:: BaseAnalysis

Properties
==========

.. autosummary::
    :toctree: generated/

    BaseAnalysis.solution

Methods
=======

.. autosummary::
    :toctree: generated/

    BaseAnalysis.add_constraint
    BaseAnalysis.run
