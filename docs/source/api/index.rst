.. _api:

=================
API Documentation
=================

The API documentation provides in-depth descriptions of Finesse's classes
and functions. Included in these descriptions, where necessary, are mathematical
details of the underlying methods used.

.. rubric:: The top-level module

.. autosummary::
    :toctree: .

    finesse

.. rubric:: Components, detectors and probes

.. autosummary::
    :toctree: .

    finesse.components
    finesse.detectors

.. rubric:: The model and its associated elements

.. autosummary::
    :toctree: .

    finesse.element
    finesse.frequency
    finesse.kat
    finesse.model

.. rubric:: The simulation, analyses and their outputs

.. autosummary::
    :toctree: .

    finesse.analysis
    finesse.simulations
    finesse.solutions

.. rubric:: Matrix construction and solving

.. autosummary::
    :toctree: .

    finesse.cmatrix

.. rubric:: Higher Order Modes

.. autosummary::
    :toctree: .

    finesse.gaussian
    finesse.knm
    finesse.tracing

.. rubric:: Parsing `.kat` files

.. autosummary::
    :toctree: .

    finesse.parse

.. rubric:: Auxiliary classes and functions

.. autosummary::
    :toctree: .

    finesse.constants
    finesse.enums
    finesse.exceptions
    finesse.freeze
    finesse.paths
    finesse.plotting
    finesse.utilities

.. rubric:: Pure Cython utilities

.. autosummary::
    :toctree: .

    finesse.cmath
