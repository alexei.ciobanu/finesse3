===============
``finesse.kat``
===============

.. automodule:: finesse.kat

.. currentmodule:: finesse.kat

Overview
========

.. autoclass:: Kat


Properties
==========

.. autosummary::
    :toctree: generated/

        
	Kat.model


Methods
=======

.. autosummary::
    :toctree: generated/

        
	Kat.__init__
	Kat.run