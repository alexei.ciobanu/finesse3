============================
``finesse.tracing.assembly``
============================

.. automodule:: finesse.tracing.assembly

.. currentmodule:: finesse.tracing.assembly


Classes
=======

.. autosummary::
    :toctree: assembly/

        
	Assembly
	FreeParameter
	TargetParameter