=========================================================
``finesse.tracing.tracesolution`` --- TraceSolution class
=========================================================

.. automodule:: finesse.tracing.tracesolution

.. currentmodule:: finesse.tracing.tracesolution

Overview
========

.. autoclass:: TraceSolution

Properties
==========

.. autosummary::
    :toctree: generated/

    TraceSolution.data_qx
    TraceSolution.data_qy

Methods
=======

.. rubric:: Constructing a tracer output object

Note that the construction of a `TraceSolution` instance is typically an internal
operation - e.g. :meth:`.Model.beam_trace` makes and returns an object of this
type.

.. autosummary::
    :toctree: generated/

    TraceSolution.__init__
