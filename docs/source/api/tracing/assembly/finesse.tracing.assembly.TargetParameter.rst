======================================================================
``finesse.tracing.assembly.TargetParameter`` --- TargetParameter class
======================================================================

.. currentmodule:: finesse.tracing.assembly

Overview
========

.. autoclass:: TargetParameter

Properties
==========

.. rubric:: Current value

.. autosummary::
    :toctree: generated/

    TargetParameter.value

Methods
=======

.. rubric:: Constructing a target parameter

.. autosummary::
    :toctree: generated/

    TargetParameter.__init__

.. rubric:: Cost function

.. autosummary::
    :toctree: generated/

    TargetParameter.cost
