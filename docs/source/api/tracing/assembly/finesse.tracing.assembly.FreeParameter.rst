==================================================================
``finesse.tracing.assembly.FreeParameter`` --- FreeParameter class
==================================================================

.. currentmodule:: finesse.tracing.assembly

Overview
========

.. autoclass:: FreeParameter

Methods
=======

.. rubric:: Constructing a free parameter

.. autosummary::
    :toctree: generated/

    FreeParameter.__init__
