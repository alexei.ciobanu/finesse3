==================================================
``finesse.tracing.tracer`` -- Beam trace execution
==================================================

.. automodule:: finesse.tracing.tracer

.. currentmodule:: finesse.tracing.tracer


Overview
========

.. autoclass:: Tracer

Methods
=======

.. rubric:: Constructing a tracer object

.. autosummary::
    :toctree: generated/

    Tracer.__init__

.. rubric:: Executing/modifying a trace

.. autosummary::
    :toctree: generated/

    Tracer.run
