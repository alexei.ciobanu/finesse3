.. _components_signal:

==============================================================
``finesse.components.signal`` --- Signal generation components
==============================================================

.. automodule:: finesse.components.signal

Overview
========
.. currentmodule:: finesse.components.signal
.. autoclass:: SignalGenerator
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    SignalGenerator.amplitude
    SignalGenerator.phase

Methods
=======

.. rubric:: Constructing a SignalGenerator

.. autosummary::
    :toctree: generated/

    SignalGenerator.__init__
