.. _components_mirror:

================================================================
``finesse.components.mirror`` --- Mirror type optical components
================================================================

.. automodule:: finesse.components.mirror

Overview
========
.. currentmodule:: finesse.components.mirror
.. autoclass:: Mirror
    :show-inheritance:

Methods
=======

.. rubric:: Constructing a mirror

.. autosummary::
    :toctree: generated/

    Mirror.__init__

.. rubric:: Beam parameter tracing

.. autosummary::
    :toctree: generated/

    Mirror.ABCD
