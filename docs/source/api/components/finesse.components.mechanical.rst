.. _components_mechanical:

===========================================================================
``finesse.components.mechanical`` --- Mechanical components and connections
===========================================================================

.. automodule:: finesse.components.mechanical
.. currentmodule:: finesse.components.mechanical

Classes
=======

.. autosummary::
    :toctree: mechanical/

    Joint
    PZT
