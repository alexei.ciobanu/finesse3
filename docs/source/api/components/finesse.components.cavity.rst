.. _components_cavity:

=================================================
``finesse.components.cavity`` --- Cavity wrappers
=================================================

.. automodule:: finesse.components.cavity

Overview
========
.. currentmodule:: finesse.components.cavity
.. autoclass:: Cavity
    :show-inheritance:

Properties
==========

.. rubric:: Paths and nodes

.. autosummary::
    :toctree: generated/

    Cavity.path
    Cavity.source
    Cavity.target

.. rubric:: Round-trip ABCD matrices

.. autosummary::
    :toctree: generated/

    Cavity.ABCD
    Cavity.ABCDx
    Cavity.ABCDy

.. rubric:: Eigenmodes

.. autosummary::
    :toctree: generated/

    Cavity.eigenmode
    Cavity.eigenmode_x
    Cavity.eigenmode_y

.. rubric:: Stability factors

.. autosummary::
    :toctree: generated/

    Cavity.is_stable
    Cavity.stability
    Cavity.stability_x
    Cavity.stability_y

.. rubric:: Physical properties

.. autosummary::
    :toctree: generated/

    Cavity.FSR
    Cavity.FWHM
    Cavity.finesse
    Cavity.loss
    Cavity.mode_separation
    Cavity.pole
    Cavity.power
    Cavity.resolution
    Cavity.round_trip_gouy
    Cavity.round_trip_length

Methods
=======

.. rubric:: Constructing a cavity

.. autosummary::
    :toctree: generated/

    Cavity.__init__

.. rubric:: Update parameters

.. autosummary::
    :toctree: generated/

    Cavity.update