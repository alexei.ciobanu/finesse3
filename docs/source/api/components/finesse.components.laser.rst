.. _components_laser:

=================================================
``finesse.components.laser`` --- Laser components
=================================================

.. automodule:: finesse.components.laser

Overview
========
.. currentmodule:: finesse.components.laser
.. autoclass:: Laser
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    Laser.f
    Laser.P
    Laser.phase

Methods
=======

.. rubric:: Constructing a laser

.. autosummary::
    :toctree: generated/

    Laser.__init__
