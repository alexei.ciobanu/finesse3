=======================================
``finesse.components.general.Variable``
=======================================



.. currentmodule:: finesse.components.general


Overview
========

.. autoclass:: Variable
            






Properties
==========

.. autosummary::
    :toctree: generated/

        
	Variable.ref


Methods
=======

.. autosummary::
    :toctree: generated/

        
	Variable.__init__