============================================================
``finesse.components.general.Connector`` --- Connector class
============================================================

.. currentmodule:: finesse.components.general

Overview
========

.. autoclass:: Connector

Properties
==========

.. autosummary::
    :toctree: generated/

    Connector.nodes
    Connector.electrical_nodes
    Connector.mechanical_nodes
    Connector.optical_nodes
    Connector.ports

Methods
=======

.. rubric:: Constructing a connector

.. autosummary::
    :toctree: generated/

    Connector.__init__

.. rubric:: Node coupling and interaction information

.. autosummary::
    :toctree: generated/

    Connector.coupling_type
    Connector.interaction_type

.. rubric:: Beam parameter tracing

.. autosummary::
    :toctree: generated/

    Connector.ABCD
