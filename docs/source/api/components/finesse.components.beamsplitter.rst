.. _components_beamsplitter:

=================================================================
``finesse.components.beamsplitter`` --- Beam-splitting components
=================================================================

.. automodule:: finesse.components.beamsplitter

Overview
========
.. currentmodule:: finesse.components.beamsplitter
.. autoclass:: Beamsplitter
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    Beamsplitter.alpha

Methods
=======

.. rubric:: Constructing a beam splitter

.. autosummary::
    :toctree: generated/

    Beamsplitter.__init__

.. rubric:: Beam parameter tracing

.. autosummary::
    :toctree: generated/

    Beamsplitter.ABCD
