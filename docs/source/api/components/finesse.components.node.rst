.. _components_node:

=================================================================
``finesse.components.node`` --- Objects for connecting components
=================================================================

.. automodule:: finesse.components.node
.. currentmodule:: finesse.components.node

Enums
=====

.. autoclass:: QSetMode
.. autoclass:: NodeType
.. autoclass:: NodeDirection

Classes
=======

.. autosummary::
    :toctree: node/

    Port
    Node
    ElectricalNode
    MechanicalNode
    OpticalNode
