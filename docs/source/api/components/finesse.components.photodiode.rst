.. _components_photodiode

=========================================================
``finesse.components.photodiode`` -- Photodiode component
=========================================================

.. automodule:: finesse.components.photodiode

.. currentmodule:: finesse.components.photodiode

Overview
========

.. autoclass:: Photodiode
      :show-inheritance:

Methods
=======

.. rubric:: Constructing a photodiode

.. autosummary::
      :toctree: generated/

      Photodiode.__init__
