.. _components_nothing:

=================================================================
``finesse.components.nothing`` --- Object representing null point
=================================================================

.. automodule:: finesse.components.nothing

Overview
========
.. currentmodule:: finesse.components.nothing
.. autoclass:: Nothing
    :show-inheritance:

Methods
=======

.. rubric:: Constructing a null point

.. autosummary::
    :toctree: generated/

    Nothing.__init__
