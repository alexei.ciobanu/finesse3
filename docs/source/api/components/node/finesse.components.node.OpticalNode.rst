=============================================================
``finesse.components.node.OpticalNode`` --- OpticalNode class
=============================================================

.. currentmodule:: finesse.components.node

Overview
========

.. autoclass:: OpticalNode

Properties
==========

.. autosummary::
    :toctree: generated/

    OpticalNode.space
    OpticalNode.is_input
    OpticalNode.opposite
    OpticalNode.port
    OpticalNode.q
    OpticalNode.q_mode
    OpticalNode.qx
    OpticalNode.qy

Methods
=======

.. autosummary::
    :toctree: generated/

    OpticalNode.__init__
