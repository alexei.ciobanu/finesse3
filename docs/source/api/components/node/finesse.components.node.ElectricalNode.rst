===================================================================
``finesse.components.node.ElectricalNode`` --- ElectricalNode class
===================================================================

.. currentmodule:: finesse.components.node

Overview
========

.. autoclass:: ElectricalNode

Methods
=======

.. rubric:: Constructing an electrical node

.. autosummary::
    :toctree: generated/

    ElectricalNode.__init__
