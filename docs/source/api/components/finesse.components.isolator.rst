.. _components_isolator:

====================================================================
``finesse.components.isolator`` --- Isolator type optical components
====================================================================

.. automodule:: finesse.components.isolator

Overview
========
.. currentmodule:: finesse.components.isolator
.. autoclass:: Isolator
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    Isolator.S

Methods
=======

.. rubric:: Constructing an isolator

.. autosummary::
    :toctree: generated/

    Isolator.__init__
