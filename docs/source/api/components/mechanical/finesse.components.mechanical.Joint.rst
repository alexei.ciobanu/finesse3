=======================================
``finesse.components.mechanical.Joint``
=======================================



.. currentmodule:: finesse.components.mechanical


Overview
========

.. autoclass:: Joint
            








Methods
=======

.. autosummary::
    :toctree: generated/

        
	Joint.__init__
	Joint.connect