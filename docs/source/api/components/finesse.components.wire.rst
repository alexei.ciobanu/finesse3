.. _components_wire:

====================================================================
``finesse.components.wire`` --- Wire-type objects between components
====================================================================

.. automodule:: finesse.components.wire

Overview
========
.. currentmodule:: finesse.components.wire
.. autoclass:: Wire
    :show-inheritance:

Methods
=======

.. rubric:: Constructing a wire

.. autosummary::
    :toctree: generated/

    Wire.__init__
    Wire.connect
