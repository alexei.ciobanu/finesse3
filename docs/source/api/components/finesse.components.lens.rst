.. _components_lens:

============================================================
``finesse.components.lens`` --- Lens type optical components
============================================================

.. automodule:: finesse.components.lens

Overview
========
.. currentmodule:: finesse.components.lens
.. autoclass:: Lens
    :show-inheritance:

Properties
==========

.. autosummary::
    :toctree: generated/

    Lens.f

Methods
=======

.. rubric:: Constructing a lens

.. autosummary::
    :toctree: generated

    Lens.__init__

.. rubric:: Beam parameter tracing

.. autosummary::
    :toctree: generated/

    Lens.ABCD