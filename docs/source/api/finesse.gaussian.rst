============================================
``finesse.gaussian`` --- Gaussian beam tools
============================================

.. automodule:: finesse.gaussian

Overview
========
.. currentmodule:: finesse.gaussian
.. autoclass:: BeamParam

Properties
==========

.. autosummary::
    :toctree: generated/

    BeamParam.divergence
    BeamParam.imag
    BeamParam.nr
    BeamParam.q
    BeamParam.Rc
    BeamParam.real
    BeamParam.w0
    BeamParam.wavelength
    BeamParam.z
    BeamParam.zr

Methods
=======

.. rubric:: Constructing a beam parameter

.. autosummary::
    :toctree: generated/

    BeamParam.__init__

.. rubric:: Arbritary beam properties

.. autosummary::
    :toctree: generated/

    BeamParam.beamsize
    BeamParam.conjugate
    BeamParam.curvature
    BeamParam.gouy

.. rubric:: Mismatches and overlaps

.. autosummary::
    :toctree: generated/

    BeamParam.mismatch
    BeamParam.overlap
    BeamParam.overlap_contour

.. rubric:: Modifying the beam parameter

.. autosummary::
    :toctree: generated/

    BeamParam.reverse
