======================================================
``finesse.cmatrix`` --- Sparse matrix solving with KLU
======================================================

.. automodule:: finesse.cmatrix

.. currentmodule:: finesse.cmatrix

Overview
========

.. autoclass:: KLUMatrix

Methods
=======

.. rubric:: Factoring

.. autosummary::
    :toctree: generated/

    KLUMatrix.factor
    KLUMatrix.refactor
    KLUMatrix.solve
