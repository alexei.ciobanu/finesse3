=====================================================
``finesse.utilities.misc`` -- Miscellaneous utilities
=====================================================

.. automodule:: finesse.utilities.misc

.. currentmodule:: finesse.utilities.misc

Functions
=========

.. autosummary::
    :toctree: generated/

    check_name
    pairwise
    find
