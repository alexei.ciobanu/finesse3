=======================================================
``finesse.utilities.components`` -- Component utilities
=======================================================

.. automodule:: finesse.utilities.components

.. currentmodule:: finesse.utilities.components

Functions
=========

.. rubric:: Space utilities

.. autosummary::
    :toctree: generated/

    refractive_indices
