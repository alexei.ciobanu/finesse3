============================================
``finesse.model`` --- Configuration handling
============================================

.. automodule:: finesse.model

.. currentmodule:: finesse.model

Overview
========

.. autoclass:: Model

Properties
==========

.. rubric:: Directed graph accessors

Attributes for retrieving properties related to the directed graph stored
in the model. These include a reference to the graph object itself (see
:attr:`Model.network`) as well as fields storing the different :class:`.NodeType`
node instances contained within this graph (e.g. :attr:`Model.optical_nodes`).

.. autosummary::
    :toctree: generated/

    Model.electrical_nodes
    Model.mechanical_nodes
    Model.network
    Model.optical_network
    Model.optical_nodes

.. rubric:: Stored components, detectors and probes

Fields for accessing the various components that a model consists of.

.. autosummary::
    :toctree: generated/

    Model.cavities
    Model.components
    Model.detectors
    Model.elements

.. rubric:: Physical properties

Properties related to the physical state of the model, including
higher-order-mode content and the frequencies present.

.. autosummary::
    :toctree: generated/

    Model.epsilon
    Model.frequencies
    Model.gauss_commands
    Model.homs
    Model.last_trace
    Model.lambda0
    Model.maxtem
    Model.Nhoms
    Model.spatial_type

.. rubric:: State of the model

Use these attributes to check a model's state.

.. autosummary::
    :toctree: generated/

    Model.is_built

Methods
=======

.. rubric:: Constructing a model

.. autosummary::
    :toctree: generated/

    Model.__init__

.. rubric:: Adding and removing objects

Member functions for modifying the contents of the model configuration.

.. autosummary::
    :toctree: generated/

    Model.add
    Model.remove

.. rubric:: Connecting components

Methods to connect components in the configuration together, these functions
are responsible for creating the :class:`.Space` instances in the model.

.. autosummary::
    :toctree: generated/

    Model.chain
    Model.connect

.. rubric:: Selecting Higher Order Modes (HOMs)

Select specific HOMs to include in the model.

.. autosummary::
    :toctree: generated/

    Model.select_modes

.. rubric:: Building and updating

Use these methods to add the necessary frequencies to the model and finalise
the configuration through the build process.

.. autosummary::
    :toctree: generated/

    Model.is_built
    Model.build

.. rubric:: Finding paths

Path-finding algorithms operating on the model's directed graph object.

.. autosummary::
    :toctree: generated/

    Model.path

.. rubric:: Merging models

Functions for merging several models together.

.. autosummary::
    :toctree: generated/

    Model.merge

.. rubric:: Tracing beams

Methods for tracing a beam through the model configuration.

See also: :mod:`finesse.tracing`

.. autosummary::
    :toctree: generated/

    Model.ABCD
    Model.beam_trace
    Model.beam_trace_path
    Model.create_mismatch
    Model.set_q_at

.. rubric:: Creating sub-models

Constructing sub-model views on sections of the model configuration.

.. autosummary::
    :toctree: generated/

    Model.sub_model

.. rubric:: Plotting the configuration

.. autosummary::
    :toctree: generated/

    Model.plot_graph
