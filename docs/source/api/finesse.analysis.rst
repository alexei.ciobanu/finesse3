============================================
``finesse.analysis`` --- Analyses on a model
============================================

.. automodule:: finesse.analysis
    :members:
