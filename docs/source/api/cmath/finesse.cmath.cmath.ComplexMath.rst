==================================================================
``finesse.cmath.cmath.ComplexMath`` --- Namespace of complex maths
==================================================================

.. currentmodule:: finesse.cmath.cmath

Overview
========

.. autoclass:: ComplexMath

Functions
=========
