=====================================================================
``finesse.cmath.cmath.Gaussian`` --- Namespace of Gaussian beam maths
=====================================================================

.. currentmodule:: finesse.cmath.cmath

Overview
========

.. autoclass:: Gaussian

Functions
=========
