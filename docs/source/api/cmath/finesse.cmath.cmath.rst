======================================================
``finesse.cmath.cmath`` --- Fast mathematical routines
======================================================

.. automodule:: finesse.cmath.cmath

.. currentmodule:: finesse.cmath.cmath

Namespaces
==========

Due to the internal mechanics of Cython, free ``cdef`` functions cannot be exposed to external
Cython extensions directly. Instead, these functions must be wrapped in classes as static-methods.
The result is that all the functions in this extension are effectively in the scope of a "namespace"
where there were defined. The "classes" below should thus be treated as such, see :ref:`cython_guide`
for usage details.

.. autosummary::
    :toctree: .

    Math
    ComplexMath
    Gaussian
