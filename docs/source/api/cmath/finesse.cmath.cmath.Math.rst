========================================================
``finesse.cmath.cmath.Math`` --- Namespace of real maths
========================================================

.. currentmodule:: finesse.cmath.cmath

Overview
========

.. autoclass:: Math

Functions
=========
