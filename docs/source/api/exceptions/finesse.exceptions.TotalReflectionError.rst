===========================================
``finesse.exceptions.TotalReflectionError``
===========================================


.. currentmodule:: finesse.exceptions


Overview
========

.. autoclass:: TotalReflectionError







Properties
==========

.. autosummary::
    :toctree: generated/


	TotalReflectionError.coupling


Methods
=======

.. autosummary::
    :toctree: generated/


	TotalReflectionError.__init__