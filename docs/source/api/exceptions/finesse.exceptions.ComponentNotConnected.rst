============================================
``finesse.exceptions.ComponentNotConnected``
============================================

.. currentmodule:: finesse.exceptions


Overview
========

.. autoclass:: ComponentNotConnected