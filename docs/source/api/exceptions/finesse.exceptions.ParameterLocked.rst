======================================
``finesse.exceptions.ParameterLocked``
======================================


.. currentmodule:: finesse.exceptions


Overview
========

.. autoclass:: ParameterLocked