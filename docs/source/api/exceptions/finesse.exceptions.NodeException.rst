====================================
``finesse.exceptions.NodeException``
====================================


.. currentmodule:: finesse.exceptions


Overview
========

.. autoclass:: NodeException







Properties
==========

.. autosummary::
    :toctree: generated/


	NodeException.node


Methods
=======

.. autosummary::
    :toctree: generated/


	NodeException.__init__