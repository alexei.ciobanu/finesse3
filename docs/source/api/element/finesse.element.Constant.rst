===============================================
``finesse.element.Constant`` --- Constant class
===============================================

.. currentmodule:: finesse.element

Overview
========

.. autoclass:: Constant

Methods
=======

.. autosummary::
    :toctree: generated/

    Constant.__init__
    Constant.eval
