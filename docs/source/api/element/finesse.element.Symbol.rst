===========================================
``finesse.element.Symbol`` --- Symbol class
===========================================

.. currentmodule:: finesse.element

Overview
========

.. autoclass:: Symbol

Properties
==========

.. autosummary::
    :toctree: generated/

    Symbol.is_changing
    Symbol.value

.. rubric:: Mathematical operations

.. autosummary::
    :toctree: generated/

    Symbol.conjugate
    Symbol.conj
    Symbol.exp
    Symbol.radians
    Symbol.sin
    Symbol.cos
    Symbol.tan
    Symbol.sqrt

Methods
=======

.. autosummary::
    :toctree: generated/

    Symbol.changing_parameters
    Symbol.parameters
