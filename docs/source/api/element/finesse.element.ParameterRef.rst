==============================================================
``finesse.element.ParameterRef`` --- Parameter reference class
==============================================================

.. currentmodule:: finesse.element

Overview
========

.. autoclass:: ParameterRef

Properties
==========

.. autosummary::
    :toctree: generated/

    ParameterRef.parameter
    ParameterRef.name
    ParameterRef.owner

Methods
=======

.. autosummary::
    :toctree: generated/

    ParameterRef.__init__
    ParameterRef.eval
