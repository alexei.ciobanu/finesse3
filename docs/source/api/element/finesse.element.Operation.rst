=================================================
``finesse.element.Operation`` --- Operation class
=================================================

.. currentmodule:: finesse.element

Overview
========

.. autoclass:: Operation

Methods
=======

.. autosummary::
    :toctree: generated/

    Operation.__init__
    Operation.eval
