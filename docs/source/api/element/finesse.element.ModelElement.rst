=======================================================
``finesse.element.ModelElement`` --- Element of a model
=======================================================

.. currentmodule:: finesse.element

Overview
========

.. autoclass:: ModelElement

Properties
==========

.. autosummary::
    :toctree: generated/

    ModelElement.parameters
    ModelElement.name
    ModelElement.has_model

Methods
=======

.. autosummary::
    :toctree: generated/

    ModelElement.__init__
