=============================
``finesse.element.Parameter``
=============================


.. currentmodule:: finesse.element


Overview
========

.. autoclass:: Parameter







Properties
==========

.. autosummary::
    :toctree: generated/


	Parameter.rebuild
	Parameter.ref
	Parameter.is_changing
	Parameter.is_tunable
	Parameter.value
	Parameter.units
	Parameter.name
	Parameter.component
	Parameter.owner
	Parameter.locked


Methods
=======

.. autosummary::
    :toctree: generated/


	Parameter.__init__
	Parameter.is_tunable
	Parameter.resolve
	Parameter.eval
	Parameter.value