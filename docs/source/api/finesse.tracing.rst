==========================================
``finesse.tracing`` --- Beam tracing tools
==========================================

.. automodule:: finesse.tracing
    :members:
