Checking a Cython extension for CPython interaction
---------------------------------------------------

When installing Finesse, during the build process of the Cython extensions, HTML files are generated
for all `.pyx` files which show the level of interaction each line has with the Python Virtual Machine.

TODO include image (e.g. screenshot from knm.html)
