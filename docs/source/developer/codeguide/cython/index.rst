.. _cython_guide:

======================================
Working with Finesse Cython extensions
======================================

For performance reasons, all of the code which involves factoring and solving the interferometer
matrix, and much of the code associated with the mathematically intensive tasks, must be written
in the form of Cython extensions in order to leverage the speed of the produced C code.

If you are new to Cython, or need a refresher on some key language concepts, then see
:ref:`cython_resources` for details from the developers.

.. toctree::
    :maxdepth: 1

    extension_structure
    cmath_extension
    note_on_class_pxd
    optimising
    exceptions
    annotations
    documenting


.. _cython_resources:

Useful resources
----------------

`The Cython documentation <https://cython.readthedocs.io/en/latest/>`_
    for the latest documentation pages.

`Cython for NumPy users <https://cython.readthedocs.io/en/latest/src/userguide/numpy_tutorial.html#numpy-tutorial>`_
    on the use of Cython with NumPy.

`Musings on Cython <https://notes-on-cython.readthedocs.io/en/latest/index.html>`_
    for details on the ways of declaring functions in Cython, with profiling for real numerical problems.

`Managing exceptions <http://freshkiss3d.gforge.inria.fr/dev/exceptions.html>`_
    a note on raising exceptions from `cdef` functions.
