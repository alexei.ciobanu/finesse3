.. _biblio:

.. only:: not latex

    Bibliography
    ============
 
.. bibliography:: refs.bib
    :cited:    
    :style: unsrt

