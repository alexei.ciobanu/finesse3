# Finesse3 Documentation Files

These directories contain the reStuctured Text (reST) files (`.rst` extension) making
up the documentation for the `finesse` package. The package `sphinx` is used for
building the documentation with all docstrings in the `finesse` code itself formatted
using the `numpydoc` style (see below).

## Building the documentation

The documentation can be built in several formats by running `make <format>`
where `<format>` can be, for example, `html` or `latex`. The built documentation
files will then be found in `build/` under the directory corresponding to the
format chosen.

### Requirements for the build process

The following components are required to build the documenation for the `finesse` package from this
directory:

- an up-to-date version of [Sphinx](http://www.sphinx-doc.org/en/master/),
- the [Sphinx Read the Docs theme](http://sphinx-rtd-theme.readthedocs.io/en/latest/),
- the [numpydoc extension](https://numpydoc.readthedocs.io/en/latest/install.html),
- the [sphinxcontrib-bibtex extension](http://sphinxcontrib-bibtex.readthedocs.io/en/latest/index.html),
- the [sphinxcontrib-katex extension](https://sphinxcontrib-katex.readthedocs.io/en/latest/index.html),
- and all the requirements for building the `cython` extensions in the `finesse` package (see
the README in the package root directory for information).

### Finding missing class method documentation

This directory contains a script `check_missing.py` which, when run, will show you which functions,
classes and class methods are currently missing from the `source/api` documentation files and so need
to be added. It will also notify of any public functions, classes andclass methods in the source code
which are missing docstrings.

## Writing `numpydoc` docstrings

To produce well formatted API documentation for Finesse, all module, class and function
docstrings will be written in the [numpydoc style](https://numpydoc.readthedocs.io/en/latest/format.html). If
you installed Python through Anaconda then you should already have the `numpydoc` extension installed, otherwise
run `pip install numpydoc` (or any other suitable package manager such as `conda` or `pacman`) to retrieve
it (note that building the documentation will fail without `numpydoc` installed).
