import cProfile
import pstats

import finesse
from finesse.detectors import CCD

model = finesse.parse("""
l L0 P=1

s s2 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01

gauss L0.p1.o q=(-2+3j)
maxtem 2
""")
ifo = model.model

ifo.add(CCD("ccd", ifo.ITM.p1.o, 3, 3, 300, f=0))

cProfile.run("model.run()", "ccd_single.prof")
p = pstats.Stats("ccd_single.prof")
p.sort_stats("time")
p.print_stats(25)
