import cProfile
import pstats

import finesse

model = finesse.parse("""
l L0 1 0 n0

s s0 0 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2
s CAV 1 nITM2 nETM1
m ETM 0.99 0.01 0 nETM1 nETM2

attr ITM Rc -10
attr ETM Rc 10

maxtem 4

cav FP ITM nITM2 ETM nETM1

xaxis L0 f lin -100M 100M 2000

pd C nETM1
""", True)

cProfile.run("model.run()", "matched_cav_scan_profile.prof")
p = pstats.Stats("matched_cav_scan_profile.prof")
p.sort_stats("time")
p.print_stats(25)

