import cProfile
import pstats

import finesse

model = finesse.parse("""
l L0 1 0 n0

s s0 1 n0 nITM1

m ITM 0.99 0.01 0 nITM1 nITM2

attr ITM Rc -10

gauss gL0 L0 n0 10u 0
maxtem 6

xaxis ITM xbeta lin 0 100u 100

pd R nITM1
""", True)

cProfile.run("model.run()", "misaligned_mirror.prof")
p = pstats.Stats("misaligned_mirror.prof")
p.sort_stats("time")
p.print_stats(25)

