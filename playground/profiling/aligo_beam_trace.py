import cProfile
import pstats

from finesse.parse.legacy import KatParser

k = KatParser()
with open("../aligo/design.kat", "r") as file:
    katfile = file.read()

k.parse(katfile)
model = k.build()
ifo = model.model
cProfile.run("ifo.beam_trace()", "aligo_beamtrace_profile.prof")
p = pstats.Stats("aligo_beamtrace_profile.prof")
p.sort_stats("time")
p.print_stats(25)
