import cProfile
import pstats

import finesse

model = finesse.parse("""
l L0 P=1

s s0 L0.p1 EOM.p1
mod EOM (
    f=100M
    midx=0.1
    order=6
)
s s1 EOM.p2 ITM.p1 L=1

m ITM R=0.99 T=0.01
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01

xaxis &L0.f (-100M) 100M 500 lin

pd circ ETM.p1.i
""")

cProfile.run("model.run()", "multi_freq_plane_wave_cav.prof")
p = pstats.Stats("multi_freq_plane_wave_cav.prof")
p.sort_stats("time")
p.print_stats(25)

