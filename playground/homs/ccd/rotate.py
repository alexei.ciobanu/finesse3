import finesse
import pykat

CODE = """
maxtem 3
trace 8

l i1 1 0 n1

s s1 100 1 n1 n2

# a beam splitter as a turning mirror:
bs bs1 1 0 0 0 n2 n3 dump dump
# setting the misalignment angle xbeta to zero
attr bs1 xbeta 0.0

s s3 500 1 n3 n4

gauss g1 bs1 n3 0.01 100

beam b1 n4

xaxis b1 x lin -5 5 40
x2axis bs1 xbeta lin -100u 100u 40
"""

model = finesse.parse(CODE, True)
ifo = model.model

out = model.run()

#base = pykat.finesse.kat(kat_code=CODE)
#base.verbose = False
#out = base.run()
#print(out.stdout)
