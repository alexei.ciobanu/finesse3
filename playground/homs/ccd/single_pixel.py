import finesse
from finesse.detectors import Pixel

ifo = finesse.parse("""
l L0 P=1

s s2 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01

gauss L0.p1.o q=(-2+3j)
maxtem 2

#pixel refl_px ITM.p1.o f=0

xaxis &ITM.xbeta 0 100u 400 lin
""")
ifo.model.add(Pixel("pixel", ifo.model.ITM.p1.o, f=0))

out = ifo.run()
out.plot()
