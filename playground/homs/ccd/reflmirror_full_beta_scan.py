import finesse

ifo = finesse.parse("""
l L0 P=1

s s2 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01

gauss L0.p1.o q=(-2+3j)
maxtem 2

ccd refl_im ITM.p1.o xlim=3 ylim=3 npts=80 f=0

xaxis &ITM.xbeta 0 500u 20 lin
""")

out = ifo.run()
out.plot()
