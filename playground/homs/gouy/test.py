import finesse
from finesse.detectors import Gouy

model = finesse.parse("""
l L0 P=1

s s0 L0.p1 ITM.p1 L=1

m ITM R=0.99 T=0.01 Rc=inf
s CAV ITM.p2 ETM.p1 L=2.3
m ETM R=0.99 T=0.01 Rc=10

modes x maxtem=4

gauss L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i

xaxis &s0.L 1 10 100 lin
""")
ifo = model.model

ifo.add(Gouy("g1", ifo.elements['s0'], ifo.elements['CAV']))

out = model.run()
out.plot()
