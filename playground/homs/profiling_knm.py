import numpy as np

import finesse
from finesse.knm import run_bayer_helms

qx1 = -1.5 + 24.0j
qy1 = -1.7 + 23.4j

qx2 = -2.1 + 35.5j
qy2 = -1.98 + 42.42j

xgamma = 2.4e-6
ygamma = -5e-7

model = finesse.Model()

def init(n):
    model.maxtem = n
    return np.eye(model.homs.shape[0], dtype=np.complex128)

def run(out):
    run_bayer_helms(
        out,
        qx1, qy1,
        qx2, qy2,
        xgamma, ygamma,
        1.0,
        model.homs,
        1064e-9,
        False
    )

