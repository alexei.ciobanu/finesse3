from pykat.finesse import kat

base = kat()
base.parse("""
l L0 1 0 nL0
s s0 1 nL0 nM1
m M 0.5 0.5 0 nM1 nM2

attr M xbeta 1u
attr M Rc -2.5

gauss* gL0 L0 nL0 -1.4 0.8

bp bpM_q x q nM2

yaxis re:im
noxaxis
trace 32

maxtem 1
""")
out = base.run()
print(out.stdout)
