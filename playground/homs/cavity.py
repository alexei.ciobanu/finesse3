import finesse

finesse.plotting.init()

model = finesse.parse("""
l L0 P=1

s s0 L0.p1 ITM.p1

m ITM R=0.99 T=0.01 Rc=inf
s CAV ITM.p2 ETM.p1 L=1
m ETM R=0.99 T=0.01 Rc=10

modes x maxtem=4

gauss L0.p1.o q=(-0.2+2.5j)
cav FP ITM.p2.o ITM.p2.i

xaxis &ITM.phi (-180) (180) 400 lin

pd refl ITM.p1.o
pd circ ETM.p1.i
pd trns ETM.p2.o
""")
ifo = model.model
ifo.add_all_ad(ifo.ETM.p1.i, f=ifo.L0.f.ref)

out = model.run()
out.plot(logy=True, figsize_scale=2)
