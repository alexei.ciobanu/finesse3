from finesse import Model
from finesse.components import Beamsplitter, Laser, Mirror

def fabry_perot():
    model = Model()
    model.chain(
        Laser('lsr'), 1.0,
        Mirror('itm'),
        2.0,
        Mirror('etm')
    )
    via = model.itm.nodes.n2i
    print("Fabry-Perot path with via_node = {}"
          .format(via))
    print(
        model.full_path(
            model.lsr.nodes.n1o,
            model.etm.nodes.n2o,
            via_node=via
        )
    )

def dodgy_michelson():
    ifo = Model()
    ifo.add([
        Laser('lsr'),
        Beamsplitter('bsp'),
        Mirror('mx'),
        Mirror('my'),
        Mirror('mback')
    ])
    ifo.connect(ifo.lsr, 1, ifo.bsp, 1, L=0.5)
    ifo.connect(ifo.bsp, 3, ifo.mx, 1, L=1.0)
    ifo.connect(ifo.bsp, 2, ifo.my, 1, L=1.0)
    ifo.connect(ifo.bsp, 4, ifo.mback, 1, L=0.2)
    via = [ifo.bsp.nodes.n2o, ifo.mback.nodes.n1o]
    print("Dodgy-Michelson path with via_node = {}"
          .format(via))
    print(
        ifo.full_path(
            ifo.lsr.nodes.n1o,
            ifo.my.nodes.n2o,
            via_node=via
        )
    )


def michelson():

    ifo = Model()
    ifo.chain(Laser('lsr'), Beamsplitter('bs'))
    # Y ARM
    ifo.chain(
       Mirror('itmy'),
       1,
       Mirror('etmy'),
       start=ifo.bs, port=2,
    )
    # X ARM
    ifo.chain(
       Mirror('itmx'),
       1,
       Mirror('etmx'),
       start=ifo.bs, port=3,
    )
    # Dark port
    ifo.chain(
       1,
       Mirror('srm'),
       start=ifo.bs, port=4,
    )

    via  = [ifo.itmx.nodes.n1o]
    path = ifo.full_path(ifo.lsr.nodes.n1o, ifo.srm.nodes.n2o, via_node=via)
    for _ in path: print(_)

fabry_perot()
print("\n-----------------------\n")
dodgy_michelson()
print("\n-----------------------\n")
michelson()