"""Signal generation tests"""

import unittest

from finesse.parse import KatParser
from pykat import finesse
import numpy as np

basekatfile = f"""
    l l1 1 0 12 n1
    s s1 0 n1 n2
    m m1 1 0 0 n2 n3
    s s2 0 n3 n4
    l l2 1 0 12 n4
    ad ad1 $fs n1*
    ad ad1m $mfs n1*
    ad ad2 $fs n1
    ad ad2m $mfs n1
    ad ad3 $fs n2*
    ad ad3m $mfs n2*
    ad ad4 $fs n2
    ad ad4m $mfs n2
    ad ad5 $fs n3*
    ad ad5m $mfs n3*
    ad ad6 $fs n3
    ad ad6m $mfs n3
    ad ad7 $fs n4*
    ad ad7m $mfs n4*
    ad ad8 $fs n4
    ad ad8m $mfs n4
"""

# TODO: remove this once the $fs variable is implemented
kat3file = """
    const fs 1.3
    const mfs -1.3
"""

kat2file = """
    noxaxis
    yaxis abs:deg
"""

f2_detectors = ["ad1", "ad1m", "ad2", "ad2m", "ad3", "ad3m", "ad4", "ad4m",
                "ad5", "ad5m", "ad6", "ad6m", "ad7", "ad7m", "ad8", "ad8m"]

# We have to swap the input/output for laser inputs, as finesse 2 does these
# the other way round
f3_detectors = ["ad2", "ad2m", "ad1", "ad1m", "ad3", "ad3m", "ad4", "ad4m",
                "ad5", "ad5m", "ad6", "ad6m", "ad8", "ad8m", "ad7", "ad7m"]


class SignalGenerationTestCase(unittest.TestCase):
    """Resettable base test case"""
    def setUp(self):
        self.parser = KatParser()
        self.reset()

    def reset(self):
        self.parser.reset()
        self.parser.parse(basekatfile)
        self.parser.parse(kat3file)
        self.kat = finesse.kat()
        self.kat.verbose = False
        self.kat.parse(basekatfile)
        self.kat.parse(kat2file)

    def run_test(self, code):
        self.reset()

        # Finesse 3
        self.parser.parse(code)
        ifo = self.parser.build()
        ifo.homs.append('00')
        carrier, signal = ifo.build()
        with carrier, signal:
            carrier.run()
            signal.run(carrier)
        f3_results = []
        for det in f3_detectors:
            f3_results.append(signal.detector_out[det])
        f3_results = np.array(f3_results)

        # Finesse 2
        self.kat.parse(code)
        out = self.kat.run()
        f2_results = out[f2_detectors]

        # Comparison
        np.testing.assert_allclose(f3_results, f2_results, rtol=1e-10)


class LaserTestCase(SignalGenerationTestCase):
    def test_laser_amplitude_signal(self):
        code = "fsig sig l1 amp 1.3 40 1"
        self.run_test(code)

    def test_laser_phase_signal(self):
        code = "fsig sig l1 phase 1.3 40 1"
        self.run_test(code)

    def test_laser_frequency_signal(self):
        code = "fsig sig l1 freq 1.3 40 1"
        self.run_test(code)


class MirrorTestCase(SignalGenerationTestCase):
    def test_mirror_phase_signal(self):
        code = """
        attr m1 mass 1
        fsig sig m1 phase 1.3 40 1
        """
        self.run_test(code)


class SpaceTestCase(SignalGenerationTestCase):
    def test_space_phase_signal(self):
        code = """
        fsig sig s1 phase 1.3 40 1
        """
        self.run_test(code)
