import numpy as np
from finesse.components import Beamsplitter, Cavity, Laser, Lens, Mirror, Modulator
from finesse.gaussian import BeamParam
from finesse.model import Model

# debugging issue with PRY round-trip ABCD
def make_PRY_model():
    nsilica = 1.44963098985906
    Mloss = 37.5e-6
    TLY_f = 34.5e3
    PRM = Mirror("PRM", T=0.03, L=8.5e-6, Rc=11.009)
    PR2 = Beamsplitter("PR2", T=250e-6, L=Mloss, alpha=-0.79, Rc=-4.545)
    PR3 = Beamsplitter("PR3", T=0.0, L=Mloss, alpha=0.615, Rc=36.027)
    BS = Beamsplitter("BS", T=0.5, L=Mloss, alpha=45.0)
    BSAR1 = Beamsplitter("BSAR1", R=50e-6, L=0.0, alpha=-29.195)
    BSAR2 = Beamsplitter("BSAR2", R=50e-6, L=0.0, alpha=29.195)
    ITMY_lens = Lens("ITMY_lens", f=TLY_f)
    ITMYAR = Mirror("ITMYAR", R=0.0, L=20e-6)
    ITMY = Mirror("ITMY", T=0.014, L=Mloss, Rc=-1934.0)
    model = Model()
    model.add([PRM, PR2, PR3, BS, BSAR1, BSAR2, ITMY_lens, ITMYAR, ITMY])
    # connect
    model.connect(model.PRM.p2, model.PR2.p1, name="lp1", L=16.6107)
    model.connect(model.PR2.p2, model.PR3.p1, name="lp2", L=16.1647)
    model.connect(model.PR3.p2, model.BS.p1, name="lp3", L=19.5381)
    model.connect(model.BS.p3, model.BSAR1.p1, name="BSsub1", L=0.0687, nr=nsilica)
    model.connect(model.BS.p4, model.BSAR2.p1, name="BSsub2", L=0.0687, nr=nsilica)
    model.connect(model.BS.p2, model.ITMY_lens.p1, name="ly1", L=5.0126)
    model.connect(model.ITMY_lens.p2, model.ITMYAR.p1, name="sITMY_th2")
    model.connect(model.ITMYAR.p2, model.ITMY.p1, name="ITMYsub", L=0.2, nr=nsilica)
    # make cavity
    model.add(Cavity("cavPRY", model.PRM.p2.o, model.PRM.p2.i, model.ITMY.p1.i))
    return model

def make_model(itmRc, etmRc, L):
    model = Model()
    L0 = Laser("L0")
    ITM = Mirror("ITM", T=0.014, L=37.5e-6, Rc=itmRc)
    ETM = Mirror("ETM", T=5e-6, L=37.5e-6, Rc=etmRc)
    model.add([L0, ITM, ETM])
    model.connect(L0.p1, ITM.p1)
    model.connect(ITM.p2, ETM.p1, L=L)
    return model

def add_cavity(model):
    model.add(Cavity("cav", model.ITM.p2.o, model.ITM.p2.i))

def analytic_rt_abcd(itmRc, etmRc, L):
    Ms = np.array([[1.0, L], [0.0, 1.0]])
    Metm_refl = np.array([[1.0, 0.0], [-2.0/etmRc, 1.0]])
    Mitm_refl = np.array([[1.0, 0.0], [2.0/itmRc, 1.0]])
    return Ms * Metm_refl * Ms * Mitm_refl

def main():
    ITM_ROC = -1934.0
    ETM_ROC = 2245.0
    L = 3994.4692
    model = make_model(ITM_ROC, ETM_ROC, L)
    add_cavity(model)
    print(model.cav.ABCD)
    print(analytic_rt_abcd(ITM_ROC, ETM_ROC, L))
    print(model.cav.stability)
    print(model.cav.eigenmode)

    model2 = make_PRY_model()
    print(model2.cavPRY.stability)

if __name__ == "__main__":
    main()
