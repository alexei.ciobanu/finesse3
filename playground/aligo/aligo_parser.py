from finesse.parse import KatParser

k = KatParser()
with open("design.kat", "r") as file:
    katfile = file.read()

k.parse(katfile)
ifo = k.build()

trace_ETMY_PRM = ifo.beam_trace_path(
    q_in=ifo.cavYARM.eigenmode,
    from_node=ifo.ITMY.p1.o,
    to_node=ifo.PRM.p2.i
)[0]

trace_ETMY_PRM.print()
trace_ETMY_PRM.plot(show=True)
