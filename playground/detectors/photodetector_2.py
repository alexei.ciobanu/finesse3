import pykat
import numpy as np

from finesse.components import Laser, Mirror, Modulator
from finesse.detectors import PowerDetector
from finesse.model import Model

kat = pykat.finesse.kat()
kat.verbose = False
kat.parse("""
    l laser 1 0 nlaser
    s s0 0 nlaser nmod1
    mod mod 10 0.6 2 pm nmod1 nmod2
    s s1 0 nmod2 ncavin
    m m1 0.9 0.1 0 ncavin ncav1
    s s2 0 ncav1 ncav2
    m m2 1 0 0 ncav2 ncavout

    pd2 pd 20 0 40 0 ncavin*

    noxaxis
    yaxis lin abs:deg
""")

ifo = Model()

ifo.homs.append('00')

l1 = Laser('l1', P=kat.laser.P.value)
mod = Modulator("mod", kat.mod.f.value, kat.mod.midx.value, kat.mod.order, "pm")
m1 = Mirror("m1", R=kat.m1.R.value, T=kat.m1.T.value)
m2 = Mirror("m2", R=kat.m2.R.value, T=kat.m2.T.value)
pd = PowerDetector("pd", m1.nodes.n1i, [20, 40], [0, 0])

ifo.add(l1)
ifo.add(mod)
ifo.add(m1)
ifo.add(m2)
ifo.add(pd)

ifo.connect(l1, 1, mod, 1)
ifo.connect(mod, 2, m1, 1)
ifo.connect(m1, 2, m2, 1)

carrier, _ = ifo.build()

with carrier:
    # Solve the carrier fields
    carrier.run()

    out = [carrier.detector_out[x] for x in ["pd"]]
print("CMatrix:")
print(out)

# Pairs of (node_name, reverse_order)
kat_out = kat.run()
kat_out = kat_out["pd"]
print("\nFinesse:")
print(kat_out)

tol = 1e-10
print("\nAll Close:")
print(np.allclose(out, kat_out, tol, tol))
