import pykat
import numpy as np

from finesse.components import Laser, Mirror, Modulator
from finesse.detectors import AmplitudeDetector
from finesse.model import Model

kat = pykat.finesse.kat()
kat.verbose = False
kat.parse("""
    l laser 1 0 nlaser
    s s0 0 nlaser nmod1
    mod mod 10 0.6 2 pm nmod1 nmod2
    s s1 0 nmod2 ncavin
    m m1 0.9 0.1 0 ncavin ncav1
    s s2 0 ncav1 ncav2
    m m2 1 0 0 ncav2 ncavout

    ad ad 0 ncavin*
    ad ad_psb 10 ncav2
    ad ad_msb -10 ncav1

    noxaxis
    yaxis lin abs:deg
""")

ifo = Model()

ifo.homs.append('00')

l1 = Laser('l1', P=kat.laser.P.value)
mod = Modulator("mod", 10, 0.6, 2, "pm")
m1 = Mirror("m1", R=kat.m1.R.value, T=kat.m1.T.value)
m2 = Mirror("m2", R=kat.m2.R.value, T=kat.m2.T.value)
ad = AmplitudeDetector("ad", m1.nodes.n1i, f=0)
ad_psb = AmplitudeDetector("ad_psb", m2.nodes.n1o, f=10)
ad_msb = AmplitudeDetector("ad_msb", m1.nodes.n2o, f=-10)

ifo.add(l1)
ifo.add(mod)
ifo.add(m1)
ifo.add(m2)
ifo.add(ad)
ifo.add(ad_psb)
ifo.add(ad_msb)

ifo.connect(l1, 1, mod, 1)
ifo.connect(mod, 2, m1, 1)
ifo.connect(m1, 2, m2, 1)

carrier, _ = ifo.build()

with carrier:
    # Solve the carrier fields
    carrier.run()

    out = [carrier.detector_out[x] for x in ["ad", "ad_psb", "ad_msb"]]
print("CMatrix:")
print(out)

kat_out = kat.run()
kat_out = kat_out["ad", "ad_psb", "ad_msb"]
print("\nFinesse:")
print(kat_out)

tol = 1e-10
print("\nAll Close:")
print(np.allclose(out, kat_out, tol, tol))
