import finesse

finesse.LOGGER.setLevel("INFO")

model = finesse.parse(r"""
l i1 1.7 0 nlaser
s sin 0 nlaser nMU3in
gauss beam_in i1 nlaser 268u -550m

mod  eom3 $fPR 0.126433 2 pm 0 nMU3in nMU3_1_1    # PR control
s s0 0 nMU3_1_1 nMU3_1_2
mod  eom4 $fSR 0.199417 2 pm 0 nMU3_1_2 nMU3_2_1     # Schnupp1 (SR control)
s s1 0 nMU3_2_1 nMU3_2_2
mod  eom5 $fMI 0.1225 2 pm 0 nMU3_2_2 nMU3_3_1     # Schnupp2 (MI control)
s s2 0 nMU3_3_1 nMU3_3_2
lens lpr 1.8 nMU3_3_2 nMU3_4_1
s s3 0 nMU3_4_1 nMU3_4_2
# some rather arbitrary thermal lense for the isolators and the EOMs:
lens therm 5.2 nMU3_4_2 nMU3_5_1
s s4 0 nMU3_5_1 nMU3_5_2
isol d2 120 nMU3_5_2 nMU3out               	# Faraday Isolator

# 070502 corrected length with respect to OptoCad (Roland Schilling)
s    smcpr3 4.391 nMU3out nBDIPR1
bs1  BDIPR 50e-6 30e-6 0 45 nBDIPR1 nBDIPR2 dump dump

s    smcpr4 0.11 nBDIPR2 nMPRo


m    mPRo 0 1 0 nMPRo nMPRi
attr mPRo Rc -1.85842517051051 # Rc as used in OptoCad (layout_1.41.ocd)
s    smpr 0.075 1.44963 nMPRi nPRo
# second (inner) surface of MPR
m   MPR 0.99905 0.0009 0. nPRo nPRi       	# T= 900 ppm PR

s    swest 1.146 nPRi nBSwest		# new length with T_PR=900 ppm



## BS
##
##
##                       nBSnorth     ,'-.
##                             |     +    `.
##                             |   ,'       :'
##            nBSwest          |  +i1      +
##         ---------------->    ,:._  i2 ,'
##                             + \  `-. +       nBSeast
##                           ,' i3\   ,' ---------------
##                          +      \ +
##                        ,'     i4.'
##                       `._      ..
##                          `._ ,' |nBSsouth
##                             -   |
##                                 |
##                                 |


bs   BS 0.4859975 0.5139975 0.0 42.834 nBSwest nBSnorth nBSi1 nBSi3
s    sBS1a 0.041 1.44963 nBSi1 nBSi1b
# here thermal lense of beam splitter (Roland: f about 1000m for 10kW at BS)
lens bst 100k nBSi1b nBSi1c
s    sBS1 0.051 1.44963 nBSi1c nBSi2
s    sBS2 0.091 1.44963 nBSi3 nBSi4
bs   BS2 150u 0.99982 0 -27.9694 nBSi2 dump nBSeast nBSAR
bs   BS3 150u 0.99982 0 -27.9694 nBSi4 dump nBSsouth dump

## north arm
s snorth1 598.5682 nBSnorth nMFN1
bs1 MFN 150e-6 10e-6 0.0 0.0 nMFN1 nMFN2 dump dump
attr MFN Rc 666

s snorth2 597.0158 nMFN2 nMCN1
m1 MCN 150e-6 10e-6 -0.0 nMCN1 dump
attr MCN Rc 636
attr MCN xbeta -0.00000004
attr MCN ybeta -0.000000015

## east arm
s seast1 598.4497 nBSeast nMFE1
bs1 MFE 150e-6 10e-6 0.0 0.0 nMFE1 nMFE2 dump dump
attr MFE Rcx 664	# 71 W
attr MFE Rcy 660	# 71 W

s seast2 597.0713 nMFE2 nMCE1
m1 MCE 150e-6 10e-6 0.0 nMCE1 dump
attr MCE Rc 622

## south arm
s ssouth 1.115 nBSsouth nMSRi
beam b1 nMSRi		# detecting the beam behind the space

maxtem 8
phase 3
# PR cavity (north arm)
cav prc1 MPR nPRi MCN nMCN1
# PR cavity (east arm)
cav prc2 MPR nPRi MCE nMCE1


const fSR   9012589
const fMI   14.904875M
const fPR  37.16M


xaxis  b1 x lin -2.5 2.5 100
x2axis b1 y lin -2.5 2.5 100
""", True)
ifo = model.model

ifo.beam_trace_logging.trace_info = True
#ifo.beam_trace_logging.ABCDs = True

print(ifo.beam_trace())
