import finesse

finesse.LOGGER.setLevel("INFO")

model = finesse.parse("""
l i1 1 0 n1

s s1 100 1 n1 n2

bs bs1 1 0 0 0 n2 n3 dump dump
attr bs1 xbeta 0.0

s s3 500 1 n3 n4

gauss g1 bs1 n3 0.01 100
""", True)
ifo = model.model

ifo.beam_trace_logging.trace_info = True
ifo.beam_trace_logging.ABCDs = True

print(ifo.beam_trace())
