# run via:
# python -m timeit -s "import profiling" "profiling.numeric_trace()"
# python -m timeit -s "import profiling" "profiling.symbolic_trace()"

import numpy as np

from finesse.parse import KatParser

XARM_MODEL = r"""
l L0 125 0.0 0.0 ni

const nsilica 1.44963098985906
const Mloss 37.5u
const TLX_f 34.5k         # Thermal lens ITMX
const Larm 3994.4692

%%% FTblock Xarm
###########################################################################

# Distance from beam splitter to X arm input mirror
s lx1 4.993 ni nITMX1a

lens ITMX_lens $TLX_f nITMX1a nITMX1b
s sITMX_th2 0 nITMX1b nITMX1

# X arm input mirror
m2 ITMXAR 0 20u 0 nITMX1 nITMXs1
s ITMXsub 0.2 $nsilica nITMXs1 nITMXs2
m1 ITMX 0.014 $Mloss 0.0 nITMXs2 nITMX2
attr ITMX Rc -1934

# X arm length
s LX $Larm nITMX2 nETMX1

# X arm end mirror
m1 ETMX 5u $Mloss 0.0 nETMX1 nETMXs1
s ETMXsub 0.2 $nsilica nETMXs1 nETMXs2
m2 ETMXAR 0 500u 0 nETMXs2 nPTX
attr ETMX Rc 2245
attr ETMX mass 40
attr ITMX mass 40

###########################################################################
%%% FTend Xarm

cav cavXARM ITMX nITMX2 ETMX nETMX1
"""

kp = KatParser()
kp.parse(XARM_MODEL)
model = kp.build()

SYM_TRACE = model.beam_trace(symbolic=True)

ARM_LENGTHS = np.linspace(4000, 4200, 50)

def numeric_trace():
    for L in ARM_LENGTHS:
        model.LX.L = L
        model.beam_trace()

def symbolic_trace():
    for L in ARM_LENGTHS:
        model.LX.L = L
        SYM_TRACE.evaluate()
