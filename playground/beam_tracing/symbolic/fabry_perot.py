import finesse
from finesse.components import Laser, Mirror, Space, Cavity


model = finesse.Model()
model.chain(Laser("L0"), Space("s0", L=0.5), Mirror("ITM", Rc=-2.5),
            Space("ARM", L=1.0), Mirror("ETM", Rc=2.5))
model.add(Cavity("CAV", model.ITM.p2.o, model.ITM.p2.i))
trace = model.beam_trace(symbolic=True)

print(trace.evaluate())

