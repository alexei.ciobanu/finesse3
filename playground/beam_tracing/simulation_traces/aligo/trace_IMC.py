from finesse import Model
from finesse.parse import KatParser
from finesse.components import Beamsplitter, Cavity, Laser, Mirror
from finesse.gaussian import BeamParam

from pykat import finesse
from pykat.tools.detecting import all_bp_detectors

IMC_MODEL = r"""
%%% FTblock laser
###########################################################################
# Laser and input optics
l L0 125 0.0 0.0 ni
#bs jitter 1 0 0 0 ni n0 dump dump

s lmod1 1 ni n1

# modulators for core interferometer sensing - Advanced LIGO, CQG, 2015
# http://iopscience.iop.org/article/10.1088/0264-9381/32/7/074001/meta#cqg507871s4-8
# 9MHz (CARM, PRC, SRC loops)
const f1 9099471
const f2 45497355
const nsilica 1.44963098985906

mod mod1 $f1 0.18 1 pm n1 n2
s lmod2 1 n2 n3

# 45MHz (MICH, SRC loops)
mod mod2 $f2 0.18 1 pm n3 nLaserOut
###########################################################################
%%% FTend laser

%%% FTblock IMC
#####################################################################################
s sIMCin 0 nLaserOut nMC1in

bs1 MC1 6000u 0 0 44.59 nMC1in nMC1refl nMC1trans nMC1fromMC3
s sMC1_MC2 16.24057 nMC1trans nMC2in

bs1 MC2 0 0u 0 0.82 nMC2in nMC2refl nMC2trans dump
s sMC2_MC3 16.24057 nMC2refl nMC3in
attr MC2 Rc 27.24

bs1 MC3 6000u 0 0 44.59 nMC3in nMC3refl nMC3trans nMCreturn_refl
s sMC3substrate 0.0845 $nsilica nMC3trans nMC3ARin
bs2 MC3AR 0 0 0 28.9661 nMC3ARin dump nIMCout dump

s sMC3_MC1 0.465 nMC3refl nMC1fromMC3
#####################################################################################
%%% FTend IMC

cav cavIMC MC2 nMC2in MC2 nMC2refl
"""

kp = KatParser()
kp.parse(IMC_MODEL)
model = kp.build()

print(model.beam_trace())

base = finesse.kat()
base.verbose = False
base.parse(IMC_MODEL)
base.parse(all_bp_detectors(base))
base.parse("""
noxaxis
yaxis re:im
""")
out = base.run()

print("\nFinesse 2 trace results:")
print("--------------------------")
for probe in base.detectors.keys():
    print(probe[2:] + " qx = {}".format(out[probe]))
