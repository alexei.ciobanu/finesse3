from finesse import Model
from finesse.parse import KatParser
from finesse.components import Beamsplitter, Cavity, Laser, Mirror
from finesse.gaussian import BeamParam

from pykat import finesse
from pykat.tools.detecting import all_bp_detectors

OMC_MODEL = r"""
###########################################################################
# Laser and input optics
l L0 125 0.0 0.0 ni

%%% FTblock OMC
###########################################################################
# OMC (as built parameters: D1300507-v1)
s sSRM_FI 0.7278 ni nF12a

# Input Coupler IC (flat mirror)
bs1 OMC_IC 0.0076 10u 0 2.7609 nF12a nOMC_ICb nOMC_ICc nOMC_ICd

# Distance from IC to OC
s lIC_OC 0.2815 1 nOMC_ICc nOMC_OCa

# Output Coupler OC (flat mirror)
bs1 OMC_OC 0.0075 10u 0 4.004 nOMC_OCa nOMC_OCb nAS nOMC_OCd

# Distance from OC to CM1
s lOC_CM1 0.2842 1 nOMC_OCb nOMC_CM1a

# Curved Mirror CM1
bs1 OMC_CM1 36u 10u 0 4.004 nOMC_CM1a nOMC_CM1b nOMC_CM1c nOMC_CM1d
attr OMC_CM1 Rc 2.57321

# Distance from CM1 to CM2
s lCM1_CM2 0.2815 1 nOMC_CM1b nOMC_CM2a

# Curved Mirror CM2
bs1 OMC_CM2 35.9u 10u 0 4.004 nOMC_CM2a nOMC_CM2b nOMC_CM2c nOMC_CM2d
attr OMC_CM2 Rc 2.57369

# Distance from CM2 to IC
s lCM2_IC 0.2842 1 nOMC_CM2b nOMC_ICd

###########################################################################
%%% FTend OMC

cav cavOMC OMC_IC nOMC_ICc OMC_IC nOMC_ICd
"""
kp = KatParser()
kp.parse(OMC_MODEL)
model = kp.build()
print(model.beam_trace())

base = finesse.kat()
base.verbose = False
base.parse(OMC_MODEL)
base.parse(all_bp_detectors(base))
base.parse("""
noxaxis
yaxis re:im
""")
out = base.run()
print("\nFinesse 2 trace results:")
print("--------------------------")
for probe in base.detectors.keys():
    print(probe[2:] + " qx = {}".format(out[probe]))
