import matplotlib.pyplot as plt
from finesse import Model
from finesse.components import Beamsplitter, Laser, Mirror
from finesse.gaussian import BeamParam
import pykat
import pykat.optics.gaussian_beams as gb

def comparison_plot(zs, ws_f, gs_f, ws_p, gs_p):
    fig, axes = plt.subplots(2, 2, sharex=True)
    axes[0, 0].plot(zs, ws_f, linestyle='-', color='r',
                    label=r"Finesse 3 trace")
    axes[0, 0].plot(zs, ws_p, linestyle='--', color='r',
                    label=r"Pykat trace")
    axes[1, 0].plot(zs, gs_f, linestyle='-', color='b',
                    label=r"Finesse 3 trace")
    axes[1, 0].plot(zs, gs_p, linestyle='--', color='b',
                    label=r"Pykat trace")

    axes[0, 1].plot(zs, (ws_f - ws_p)/ws_p, color='k')
    axes[0, 1].set_ylabel("Beamsize relative error")
    axes[1, 1].plot(zs, (gs_f - gs_p)/gs_p, color='k')
    axes[1, 1].set_ylabel("Gouy phase relative error")

    axes[0, 0].legend()
    axes[1, 0].legend()
    axes[0, 0].set_ylabel("Beamsize [mm]")
    axes[1, 0].set_xlabel("Distance z [m]")
    axes[1, 1].set_xlabel("Distance z [m]")
    axes[1, 0].set_ylabel("Gouy phase [deg]")
    fig.tight_layout()
    return fig, axes

def finesse_michelson(L1, Lx0, Lxcav, Ly0, Lycav, Rcx1, Rcx2, Rcy1, Rcy2):
    ifo = Model()
    lsr = Laser('lsr')
    bsp = Beamsplitter('bsp')
    itmx = Mirror('itmx', Rc=Rcx1)
    etmx = Mirror('etmx', Rc=Rcx2)
    itmy = Mirror('itmy', Rc=Rcy1)
    etmy = Mirror('etmy', Rc=Rcy2)
    comps = [lsr, bsp, itmx, etmx, itmy, etmy]
    ifo.add(comps)
    ifo.connect(lsr.p1, bsp.p1, L=L1)
    ifo.connect(bsp.p2, itmy.p1, L=Ly0)
    ifo.connect(bsp.p3, itmx.p1, L=Lx0)
    ifo.connect(itmy.p2, etmy.p1, L=Lycav)
    ifo.connect(itmx.p2, etmx.p1, L=Lxcav)
    return ifo

def pykat_michelson(L1, Lx0, Lxcav, Ly0, Lycav, Rcx1, Rcx2, Rcy1, Rcy2):
    base = pykat.finesse.kat()
    base.verbose = False
    base.noxaxis = True
    base.parse("""
    l lsr 1 0 n0
    s s1 {l1} n0 n1
    bs bsp 0.5 0.5 0 0 n1 n2 n3 n4
    # YArm
    s sn {ly0} n2 nITMY1
    m itmy 0.5 0.5 0 nITMY1 nITMY2
    s sy {lycav} nITMY2 nETMY1
    m etmy 0.5 0.5 0 nETMY1 nETMY2
    # XArm
    s se {lx0} n3 nITMX1
    m itmx 0.5 0.5 0 nITMX1 nITMX2
    s sx {lxcav} nITMX2 nETMX1
    m etmx 0.5 0.5 0 nETMX1 nETMX2

    attr itmx Rc {Rcx1}
    attr itmy Rc {Rcy1}
    attr etmx Rc {Rcx2}
    attr etmy Rc {Rcy2}
    """.format(
        l1=L1, ly0=Ly0, lycav=Lycav, lx0=Lx0, lxcav=Lxcav,
        Rcx1=Rcx1, Rcx2=Rcx2, Rcy1=Rcy1, Rcy2=Rcy2
    ))
    return base

def main():
    L1 = 0.2
    Lx0 = 0.4
    Ly0 = 0.3
    Lxcav = 1.0
    Lycav = 1.0
    Rcx1 = -2.5
    Rcx2 = 2.5
    Rcy1 = -2.4
    Rcy2 = 2.5
    ### finesse 3
    m_finesse = finesse_michelson(L1, Lx0, Lxcav, Ly0, Lycav, Rcx1, Rcx2, Rcy1, Rcy2)
    q_in = BeamParam(w0=5e-4, z=-1.0)
    trace_lsr_to_etmx = m_finesse.beam_trace_path(
        q_in=q_in,
        from_node=m_finesse.lsr.p1.o,
        to_node=m_finesse.etmx.p1.i
    )[0]
    zsx_f, wsx_f, gsx_f = trace_lsr_to_etmx.plot(show=False)
    trace_lsr_to_etmy = m_finesse.beam_trace_path(
        q_in=q_in,
        from_node=m_finesse.lsr.p1.o,
        to_node=m_finesse.etmy.p1.i
    )[0]
    zsy_f, wsy_f, gsy_f = trace_lsr_to_etmy.plot(show=False)
    ### pykat
    m_pykat = pykat_michelson(L1, Lx0, Lxcav, Ly0, Lycav, Rcx1, Rcx2, Rcy1, Rcy2)
    q_in_ = gb.BeamParam(w0=5e-4, z=-1.0)
    zsx_p, wsx_p, gsx_p = m_pykat.beamTrace(q_in_, 'n0', 'nETMX1').plot()
    zsy_p, wsy_p, gsy_p = m_pykat.beamTrace(q_in_, 'n0', 'nETMY1').plot()
    ### compare
    figx = comparison_plot(zsx_f, wsx_f, gsx_f, wsx_p, gsx_p)[0]
    figy = comparison_plot(zsy_f, wsy_f, gsy_f, wsy_p, gsy_p)[0]
    plt.show()
    #figx.savefig('pykat_finesse_michelson_xarm_test.png')
    #figy.savefig('pykat_finesse_michelson_yarm_test.png')

if __name__ == "__main__":
    main()
