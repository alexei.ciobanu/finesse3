import matplotlib.pyplot as plt
import finesse.plotting as fplt
from finesse import Model
from finesse.components import Laser, Mirror
from finesse.gaussian import BeamParam
import pykat
import pykat.optics.gaussian_beams as gb

def comparison_plot(zs, ws_f, gs_f, ws_p, gs_p):
    fig, axes = plt.subplots(2, 2, sharex=True)
    axes[0, 0].plot(zs, ws_f, linestyle='-', color='r',
                    label=r"Finesse 3 trace")
    axes[0, 0].plot(zs, ws_p, linestyle='--', color='r',
                    label=r"Pykat trace")
    axes[1, 0].plot(zs, gs_f, linestyle='-', color='b',
                    label=r"Finesse 3 trace")
    axes[1, 0].plot(zs, gs_p, linestyle='--', color='b',
                    label=r"Pykat trace")

    axes[0, 1].plot(zs, (ws_f - ws_p)/ws_p, color='k')
    axes[0, 1].set_ylabel("Beamsize relative error")
    axes[1, 1].plot(zs, (gs_f - gs_p)/gs_p, color='k')
    axes[1, 1].set_ylabel("Gouy phase relative error")

    axes[0, 0].legend()
    axes[1, 0].legend()
    axes[0, 0].set_ylabel("Beamsize [mm]")
    axes[1, 0].set_xlabel("Distance z [m]")
    axes[1, 1].set_xlabel("Distance z [m]")
    axes[1, 0].set_ylabel("Gouy phase [deg]")
    fig.tight_layout()
    return fig, axes

def finesse_fabry_perot(L1, L2, Rc1, Rc2):
    ifo = Model()
    lsr = Laser('lsr')
    itm = Mirror('itm', Rc=Rc1)
    etm = Mirror('etm', Rc=Rc2)
    ifo.add([lsr, itm, etm])
    ifo.connect(lsr.p1, itm.p1, L=L1)
    ifo.connect(itm.p2, etm.p1, L=L2)
    return ifo

def pykat_fabry_perot(L1, L2, Rc1, Rc2):
    base = pykat.finesse.kat()
    base.verbose = False
    base.noxaxis = True
    base.parse("""
    l lsr 1 0 n0
    s s1 {} n0 n1
    m itm 0.5 0.5 0 n1 n2
    s s2 {} n2 n3
    m etm 0.5 0.5 0 n3 n4

    attr itm Rc {}
    attr etm Rc {}
    """.format(L1, L2, Rc1, Rc2))
    return base

def main():
    #fplt.use('default')
    L1 = 0.2
    L2 = 1.0
    Rc1 = -2.5
    Rc2 = 2.5
    ### finesse 3
    fp_finesse = finesse_fabry_perot(L1, L2, Rc1, Rc2)
    q_in = BeamParam(w0=5e-4, z=-1.0)
    trace = fp_finesse.beam_trace_path(
        q_in=q_in,
        from_node=fp_finesse.lsr.p1.o,
        to_node=fp_finesse.etm.p1.i
    )[0]
    zs_f, ws_f, gs_f = trace.plot(show=False)
    ### pykat
    fp_pykat = pykat_fabry_perot(L1, L2, Rc1, Rc2)
    q_in_ = gb.BeamParam(w0=5e-4, z=-1.0)
    zs_p, ws_p, gs_p = fp_pykat.beamTrace(q_in_, 'n0', 'n3').plot()
    ### compare
    fig = comparison_plot(zs_f, ws_f, gs_f, ws_p, gs_p)[0]
    plt.show()
    #fig.savefig('pykat_finesse_fp_test.png')

if __name__ == "__main__":
    main()
