import numpy as np

from finesse import Model, BeamParam
from finesse.components import Cavity, Laser, Mirror
from finesse.parse import KatParser
from finesse.tracing.assembly import Assembly

MODEL_CODE = r"""
bs BS 0.5 0.5 0.0 45 nBS1 nBS2 nBS3 nBS4

s BS_to_TR3 50 nBS4 nTR3b

bs1 TR3 0 $Mloss 0 0.785 nTR3b nTR3a nTR3dump1 nTR3dump2

s TR3_to_TR2 10 nTR3a nTR2b

bs1 TR2 0 $Mloss 0 -0.87 nTR2b nTR2a nTR2dump1 nTR2dump2

s TR2_to_ITMlens 40 nTR2a nITMlens1

lens ITM_lens $TL_f nITMlens1 nITMlens2

s ITMlens_to_ITM 0 nITMlens2 nITM1

m1 ITM 0.014 $Mloss 0 nITM1 nITM2
attr ITM Rc -5691

const Mloss 37.5u
const TL_f inf
"""

kp = KatParser()
kp.parse(MODEL_CODE)
model = kp.build()

qcav = BeamParam(w0=2.51e-2, z=-5e3)
model.ITM.n2.i.q = -qcav.conjugate()

model.beam_trace()

print("Prior to solving:")
print(f"Beam size at ITM n2.i = {model.ITM.n2.i.q.beamsize()} m")
print(f"Beam size at BS n4.i = {model.BS.n4.i.q.beamsize()} m")

system = Assembly(model)
system.allow(model.BS_to_TR3, 'L', bounds=(0, 200))
system.allow(model.TR3_to_TR2, 'L', bounds=(0, 100))
system.allow(model.TR2_to_ITMlens, 'L', bounds=(0, 100))
system.allow(model.TR2, "Rc", bounds=(-500, 500))
system.allow(model.TR3, "Rc", bounds=(-500, 500))
system.target(model.BS.n4.i, 'qx', "beamsize", value=4.7e-3) # targeting beam size of 5mm at BS...
system.target(model.ITM.n1.i, 'qx', "beamsize", value=11.79e-2) # ... and 11cm at ITM port 2

result = system.solve()

print("After solving:")
print(f"Beam size at ITM n1.i = {model.ITM.n1.i.q.beamsize()} m")
print(f"Beam size at BS n4.i = {model.BS.n4.i.qx.beamsize()} m")

print(result)
