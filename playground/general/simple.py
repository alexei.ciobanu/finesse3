import finesse

from finesse.components import Laser, Nothing
from finesse.model import Model

ifo = Model()
ifo.homs.append('00')
l1 = Laser('l1')
nth1 = Nothing("nth1")

ifo.add(l1)
ifo.add(nth1)
ifo.connect(l1, nth1.n1, "s1")

print(l1.nodes)
print(nth1.nodes)
print(ifo.spaces.s1.nodes)
print(l1.n1.i)
print(ifo.spaces.s1.n1.i)

print(ifo.l1.n1.i.space)

assert(l1.n1.o == ifo.spaces.s1.n1.i)
assert(l1.n1.i == ifo.spaces.s1.n1.o)

carrier = ifo.build()

with carrier:
    pass
