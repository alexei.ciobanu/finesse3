import pykat
import numpy as np

from finesse.components import Laser, Mirror
from finesse.detectors import AmplitudeDetector
from finesse.model import Model

kat = pykat.finesse.kat()
kat.verbose = False
kat.parse("""
l laser 1 0 nlaser
s s1 0 nlaser ncavin
m m1 0.9 0.1 0 ncavin ncav1
s s2 0 ncav1 ncav2
m m2 0.99 0.01 0 ncav2 ncavout
noxaxis
yaxis lin abs:deg
""")

ifo = Model()

ifo.homs.append('00')

l1 = Laser('l1',  P=kat.laser.P.value)
m1 = Mirror("m1", R=kat.m1.R.value,   T=kat.m1.T.value)
m2 = Mirror("m2", R=kat.m2.R.value,   T=kat.m2.T.value)

ifo.add(l1)
ifo.add(m1)
ifo.add(m2)

ifo.connect(l1, m1)
ifo.connect(m1, m2)

carrier = ifo.build()

with carrier:
    # Solve the carrier fields
    carrier.run()

    out = carrier.out / np.sqrt(2)

    carrier.print_matrix()

print("CMatrix:")
print(out)

# Pairs of (node_name, reverse_order)
ad_nodes = [("laser", True), ("cavin", False), ("cav1", False),
            ("cav2", False), ("cavout", False)]
ads = []
for node, reverse in ad_nodes:
    for name, freq in ifo.frequencies.items():
        kat.parse("""
                ad ad{node}_{name} {freq} n{node}
                ad ad{node}_{name}* {freq} n{node}*
            """.format(node=node, freq=freq.f, name=name))
        if reverse:
            ads.append("ad{node}_{name}".format(node=node, name=name))
        else:
            ads.append("ad{node}_{name}*".format(node=node, name=name))
    for name, freq in ifo.frequencies.items():
        if reverse:
            ads.append("ad{node}_{name}*".format(node=node, name=name))
        else:
            ads.append("ad{node}_{name}".format(node=node, name=name))

kat_out = kat.run()
kat_out = kat_out[ads]
print("\nFinesse:")
print(kat_out)

tol = 1e-10
print("\nAll Close:")
print(np.allclose(out, kat_out, tol, tol))

print(abs(out-kat_out))
