"""
Outputs from a simulation / analysis run.

Listed below are all the sub-modules of the ``solutions`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: solutions/
    {contents}
"""

from .array import ArraySolution

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    __doc__ = __doc__.format(
        contents=_collect_submodules("solutions")
    )

