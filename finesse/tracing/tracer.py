"""
Beam traces used by models and simulations for setting
beam parameters at nodes of a :class:`.Model`.
"""

from collections import OrderedDict
from functools import reduce
import logging

import numpy as np

from finesse.components import Beamsplitter, Space
from finesse.components.general import InteractionType, Surface
from finesse.components.node import QSetMode

from finesse.gaussian import BeamParam
from finesse.exceptions import NodeException, TotalReflectionError
from finesse.paths import OpticalPath
from finesse.utilities import refractive_indices
from finesse.utilities.maths import apply_ABCD as apply

from finesse.tracing import TraceType
from finesse.tracing.tracesolution import TraceDataEntry, TraceSolution

LOGGER = logging.getLogger(__name__)


class Tracer(object):
    """
    An object handling any type of tracing to be performed on a model
    or components of a model. This includes the beam tracing required
    for HOM simulations and utility beam traces with custom input beam
    parameters; as well as computations such as round-trip ABCD matrices
    for :class:`.Cavity` objects.
    """
    def __init__(self, model):
        """Constructs a new instance of `Tracer` with the specified properties.

        Parameters
        ----------
        model : :class:`.Model`

            The model to be associated with the tracer.
        """
        self.__model = model
        self.__identity = np.array([[1.0, 0.0], [0.0, 1.0]])
        self.symbolic = False

    def run(self, trace_type, **kwargs):
        """Runs a tracing algorithm given by the `trace_type` parameter used, see
        :class:`.TraceType` for the options available.

        .. note::

            The `kwargs` that can be passed to this method depend upon the type of trace
            being performed. Below are shown the lists of accepted keyword arguments for
            each `trace_type` used. **Note that these are given for development information,
            the use of these arguments is largely an implementation detail.**

            .. rubric:: ``TraceType.SIMULATION``

            Compulsory arguments:

            * `current` (current optical node in the trace)
            * `trace_results` (an ordered dictionary storing the results of a beam trace so far)

            Optional arguments:

            * `traced` -- a set of all the nodes traced so far
            * `traced_cavs` -- a set of all cavities traced so far
            * `branched_beamsplitters` -- a set of all the beam splitter instances
              on which all branch tracing has been accounted for - see :ref:`tracing_manual`
              for details
            * `from_user` -- a flag indicating whether the trace started from a node with
              :class:`.QSetMode` as ``QSetMode == QSetMode.USER``
            * `from_cav` -- a reference to the :class:`.Cavity` from which the trace
              was started - if any
            * `set_symmetric` -- a flag indicating whether to set the :attr:`.OpticalNode.opposite`
              instance beam parameter to :math:`-q^*` where :math:`q` is the beam parameter of the
              current node

            .. rubric:: ``TraceType.UTILITY``

            Compulsory arguments:

            * `from_node` -- optical node from which to start trace
            * `to_node` -- optical node to trace to

            Optional arguments:

            * `via_node` -- optical node(s) that the trace must traverse through
            * `q_in` -- beam parameter value to use at the starting node
            * `datax` -- an empty ordered dictionary for storing utility beam trace data
              for tangential plane
            * `datay` -- an empty ordered dictionary for storing utility beam trace data
              for sagittal plane

            Note that `datax` and `datay` cannot both be `None`.

            .. rubric:: ``TraceType.ABCD``

            Compulsory arguments:

            * `from_node` -- optical node from which to start trace
            * `to_node` -- optical node to trace to

            Optional arguments:

            * `via_node` -- optical node(s) that the trace must traverse through

            .. rubric:: ``TraceType.RT_ABCD`` or ``TraceType.CAVPOWER``

            Compulsory arguments:

            * `cavity_path` -- the :attr:`.Cavity.path` instance.

        Parameters
        ----------
        trace_type : :class:`.TraceType`
            The type of tracing to perform.

        **kwargs
            The parameters passed as keyword arguments are dependent upon the `trace_type`
            used. See the note above for details.
        """
        if not isinstance(trace_type, TraceType):
            raise TypeError("trace_type arg must be of type finesse.beamtrace.TraceType")

        if trace_type == TraceType.SIMULATION:
            if "current" not in kwargs:
                raise KeyError("current node arg must be specified when performing "
                               "a {} trace.".format(trace_type))

            if "trace_results" not in kwargs:
                raise KeyError("trace_results arg must be specified when performing "
                               "a {} trace.".format(trace_type))

            current = kwargs["current"]
            trace_results = kwargs["trace_results"]

            traced = kwargs.get("traced", set())
            branched_beamsplitters = kwargs.get("branched_beamsplitters", set())
            from_user = kwargs.get("from_user", False)
            from_cav = kwargs.get("from_cav", None)
            traced_cavs = kwargs.get("traced_cavs", set())
            set_symmetric = kwargs.get("set_symmetric", True)

            self.__run_simtrace(
                current,
                trace_results,
                traced, branched_beamsplitters,
                from_user, from_cav,
                traced_cavs,
                set_symmetric
            )

        if (trace_type == TraceType.UTILITY or
                trace_type == TraceType.ABCD):
            if "from_node" not in kwargs or "to_node" not in kwargs:
                raise KeyError("from_node and to_node args must be specified when "
                               "performing a {} trace.".format(trace_type))
            from_node = kwargs["from_node"]
            to_node = kwargs["to_node"]
            try: via_node = kwargs["via_node"]
            except KeyError: via_node = None

            import networkx as nx
            try: path = self.__model.path(from_node, to_node, via_node=via_node)
            except (NodeException, nx.exception.NetworkXNoPath): raise

            if trace_type == TraceType.UTILITY:
                q_in = kwargs["q_in"]
                if isinstance(q_in, complex):
                    q_in = BeamParam(q_in)

                try: datax = kwargs["datax"]
                except KeyError: datax = None
                try: datay = kwargs["datay"]
                except KeyError: datay = None
                if datax is None and datay is None:
                    raise ValueError("datax and datay args cannot both be None "
                                     "when performing a TraceType.UTILITY trace")
                return self.__run_utiltrace(path, q_in, datax, datay)

            elif trace_type == TraceType.ABCD:
                return self.__run_abcd(path.data, round_trip=False)

        # use the path already stored by Cavity instances rather
        # than recomputing it for RT ABCD and cavity power tracing
        elif (trace_type == TraceType.RT_ABCD or
              trace_type == TraceType.CAVPOWER):
            if "cavity_path" not in kwargs:
                raise KeyError("cavity_path arg must be specified when "
                               "performing a {} trace".format(trace_type))
            path = kwargs["cavity_path"]
            if isinstance(path, OpticalPath): path = path.data
            if trace_type == TraceType.RT_ABCD:
                return self.__run_abcd(path, round_trip=True)
            if trace_type == TraceType.CAVPOWER:
                return self.__run_cavpower(path)

    def __run_abcd(self, path, round_trip=False):
        """Runs a round-trip ABCD matrix trace - to be used
        by instances of :class:`.Cavity`.

        Returns
        -------
        out : tuple of :class:`numpy.ndarray`
            The round-trip ABCD matrices in both directions.
        """
        matrices_x = []
        matrices_y = []
        if self.symbolic:
            raise NotImplementedError()
        else:
            reflection_transform = np.array([[-1.0, 0.0], [0.0, -1.0]])

        for i, (curr_node, to_comp) in enumerate(path):
            if to_comp != path[-1][1]:
                next_node = path[i+1][0]
            else:
                # at final component => set next_node to first in path
                # to compute our final reflection matrix for round-trip
                if round_trip:
                    next_node = path[0][0]
                else:
                    break

            Mabcd_x = to_comp.ABCD(curr_node, next_node, direction='x', symbolic=self.symbolic)

            # co-ordinate system transformation on reflection due
            # to rotation around vertical axis (inversion)
            if isinstance(to_comp, Surface):
                if to_comp.interaction_type(curr_node, next_node) == InteractionType.REFLECTION:
                    Mabcd_x = reflection_transform @ Mabcd_x

            Mabcd_y = to_comp.ABCD(curr_node, next_node, direction='y', symbolic=self.symbolic)

            matrices_x.append(Mabcd_x)
            matrices_y.append(Mabcd_y)

        #if self.symbolic:
        #    return reduce(dot_sym, matrices_x), reduce(dot_sym, matrices_y)

        return reduce(np.dot, matrices_x), reduce(np.dot, matrices_y)

    def __run_cavpower(self, path):
        """Runs a cavity power trace - to be used by instances
        of :class:`.Cavity`.

        Returns
        -------
        The power in the cavity as a fraction of the input power.
        """
        cav_power = 1.0
        for i, (curr_node, to_comp) in enumerate(path):
            # at final component => set next_node to first in path
            # to compute our final reflection
            if to_comp == path[-1][1]:
                next_node = path[0][0]
            else:
                next_node = path[i+1][0]
            if isinstance(to_comp, Surface):
                if to_comp.interaction_type(curr_node, next_node) == InteractionType.REFLECTION:
                    cav_power *= to_comp.R
                else:
                    cav_power *= to_comp.T
        return cav_power

    def __run_simtrace(
            self, current, trace_results,
            traced, branched_beamsplitters,
            from_user, from_cav, traced_cavs,
            set_symmetric):
        opt_network = self.__model.optical_network
        # successors stored as [(Node, is_branch_node)] where a branch
        # node indicates the trace from the port of a beamsplitter
        # which we otherwise cannot directly propagate to
        successors = [
            (self.__model.network.node[n]["weakref"](), False)
            for n in opt_network.successors(current.full_name)
        ]

        for node in successors:
            if node[0] in traced: # stop repetitions
                successors.remove(node)

        if not successors:
            traced.add(current)
            return
        successors.reverse() # want to do transmissions first

        if self.__model.beam_trace_logging.trace_info:
            LOGGER.info(f"\tPropagating trace from {current.full_name} with successors: "
                        f"{[succ[0].full_name for succ in successors]}")

        # if current node is at a beam splitter then the beam needs to be propagated
        # along all branches - grab the branch that the beam cannot physically reach
        # from the current node and add it to successors for future tracing
        if (isinstance(current.component, Beamsplitter) and current.is_input
                and current.component not in branched_beamsplitters):
            if self.__model.beam_trace_logging.trace_info:
                LOGGER.info(f"\t\tBranching trace at beam splitter: {current.component.name}")

            output_nodes = [n for n in current.component.optical_nodes
                            if not n.is_input and n != current.opposite]
            branch_nodes = list(set(output_nodes).difference([s[0] for s in successors]))

            for node in branch_nodes: # erase already traced nodes from branch_nodes
                if node in traced:
                    branch_nodes.remove(node)

            if branch_nodes:
                if self.__model.beam_trace_logging.trace_info:
                    LOGGER.info(f"\t\tFound branch node: {branch_nodes[0].full_name}")

                successors.append((branch_nodes[0], True))
                branched_beamsplitters.add(current.component)

        for nxt, is_branch_node in successors:
            # now check to see if any nodes connected to the next node in successors
            # have been set with a gauss command - if so then stop this trace branch
            # and restart from that node (user traced qs take precedence)
            # -> note we only want to do this if nxt is an output node so that this
            #    check is only performed across a space rather than across any component
            if not nxt.is_input:
                for n_name in opt_network.successors(nxt.full_name):
                    n = self.__model.network.node[n_name]["weakref"]()
                    if (n in trace_results and trace_results[n].set_mode == QSetMode.USER
                            and n.opposite.full_name not in opt_network.predecessors(current.full_name)):
                        if self.__model.beam_trace_logging.trace_info:
                            LOGGER.info(f"\t\tSuccessor node: {n.full_name} of {nxt.full_name} has a user-set beam parameter, "
                                        "stopping this trace branch.")

                        successors.clear()
                        if self.__model.beam_trace_logging.trace_info:
                            LOGGER.info(f"Tracing the beam from user-set beam parameter node: {n.opposite.full_name}.")
                        self.__run_simtrace(
                            n.opposite,
                            trace_results,
                            traced,
                            branched_beamsplitters=set(),
                            from_user=True,
                            from_cav=None,
                            traced_cavs=traced_cavs,
                            set_symmetric=set_symmetric)
                        return

            # doing a trace from a user defined q node and a cavity has been encountered
            # => stop this trace branch and let the cavity trace its own nodes
            if from_user:
                for cav in self.__model.cavities:
                    # TODO (sjr) repeated calls to is_stable and is_critical are slow for large
                    #            files -- pass in only the stable cavities to tracer.run
                    if cav.is_stable and not cav.is_critical:
                        # TODO (sjr) make sure this fix is working
                        #if any(nxt == n for n in cav.path.nodes_only):
                        if nxt == cav.source or nxt == cav.target:
                            if self.__model.beam_trace_logging.trace_info:
                                LOGGER.info(f"\t\tEncountered cavity: {cav.name}, stopping "
                                             "this trace branch to let this cavity trace its own nodes.")
                            return

            # doing a trace from a cavity and a different cavity has been encountered
            # => stop this trace branch and let that cavity trace its own nodes
            elif from_cav is not None:
                for cav in self.__model.cavities:
                    # TODO (sjr) repeated calls to is_stable and is_critical are slow for large
                    #            files -- pass in only the stable cavities to tracer.run
                    if cav.is_stable and not cav.is_critical:
                        if (cav != from_cav and cav not in traced_cavs
                                and (nxt == cav.source or nxt == cav.target)):
                                # TODO (sjr) make sure this fix is working properly
                                #and any(nxt == n for n in cav.path.nodes_only)):
                            if self.__model.beam_trace_logging.trace_info:
                                LOGGER.info(f"\t\tEncountered cavity: {cav.name}, stopping "
                                             "this trace branch to let this cavity trace its own nodes.")
                            traced_cavs.add(cav)
                            return

            # next node is a branch node so trace back from the beamsplitter port
            if is_branch_node:
                # branch node has now been traced from a different path so need to retrace
                if nxt in traced: continue
                # reset current to one of the previously traced nodes
                pre = [
                    self.__model.network.node[n]["weakref"]()
                    for n in list(opt_network.predecessors(nxt.full_name))
                ]
                current = pre[0]
                # if a previously traced node is traced from a user set one
                # then use this one preferentially
                for p in pre:
                    if trace_results[p].set_mode == QSetMode.USER_TRACED:
                        current = p
                        break

            # now compute the ABCD transformations and fill tracer.out accordingly
            self.__simtrace_step(
                path=self.__model.path(current, nxt).data,
                data=trace_results,
                from_user=from_user,
                set_symmetric=set_symmetric
            )

            traced.add(current)
            self.__run_simtrace(
                nxt,
                trace_results,
                traced, branched_beamsplitters,
                from_user, from_cav,
                traced_cavs,
                set_symmetric
            )

    def __simtrace_step(self, path, data, from_user, set_symmetric):
        """Runs a beam trace required for HOM simulations, this method
        fills / reinitialises the `tracer.out` dictionary.
        """
        for i, (curr_node, to_comp) in enumerate(path):
            if to_comp != path[-1][1]:
                next_node = path[i+1][0]
                try:
                    Mabcd_x = to_comp.ABCD(curr_node, next_node, direction='x', symbolic=self.symbolic)
                    Mabcd_y = to_comp.ABCD(curr_node, next_node, direction='y', symbolic=self.symbolic)
                except TotalReflectionError:
                    raise
            else:
                next_node = curr_node
                Mabcd_x = self.__identity
                Mabcd_y = self.__identity

            if (next_node in data and
                data[next_node].qx is not None and data[next_node].qy is not None):
                if self.__model.beam_trace_logging.trace_info:
                    LOGGER.info(f"\t\tBeam parameter at node: {next_node} already set, skipping.")
                continue

            # get the refractive indices of the spaces attached
            # to the current and next node
            if isinstance(to_comp, Space):
                curr_node_nr = to_comp.nr.value
                next_node_nr = to_comp.nr.value
            else:
                curr_node_nr, next_node_nr = refractive_indices(
                    curr_node, next_node, symbols=self.symbolic
                )

            if self.symbolic:
                raise NotImplementedError()
            else:
                qx = BeamParam(
                    q=apply(Mabcd_x, data[curr_node].qx, curr_node_nr, next_node_nr)
                )
                qy = BeamParam(
                    q=apply(Mabcd_y, data[curr_node].qy, curr_node_nr, next_node_nr)
                )

                if self.__model.beam_trace_logging.ABCDs:
                    LOGGER.info(f"\t\tSetting tangential beam parameter of {next_node.full_name} ...")
                    LOGGER.info(f"\t\t\t{curr_node.full_name}.qx =  {data[curr_node].qx}")
                    LOGGER.info(f"\t\t\tABCD_x at {to_comp.name} for coupling {curr_node.full_name} "
                                f"-> {next_node.full_name} = {Mabcd_x.tolist()}")
                    LOGGER.info(f"\t\t\tnr1 = {curr_node_nr}, nr2 = {next_node_nr}")
                    LOGGER.info(f"\t\t\tComputed {next_node.full_name}.qx = {qx}")

                    LOGGER.info(f"\t\tSetting sagittal beam parameter of {next_node.full_name} ...")
                    LOGGER.info(f"\t\t\t{curr_node.full_name}.qy =  {data[curr_node].qy}")
                    LOGGER.info(f"\t\t\tABCD_y at {to_comp.name} for coupling {curr_node.full_name} "
                                f"-> {next_node.full_name} = {Mabcd_y.tolist()}")
                    LOGGER.info(f"\t\t\tnr1 = {curr_node_nr}, nr2 = {next_node_nr}")
                    LOGGER.info(f"\t\t\tComputed {next_node.full_name}.qy = {qy}")

            q_set_mode = QSetMode.USER_TRACED if from_user else QSetMode.AUTO
            data[next_node] = TraceDataEntry(qx, qy, q_set_mode)

            if set_symmetric:
                if self.__model.beam_trace_logging.trace_info:
                    LOGGER.info(f"\t\tSetting {next_node.opposite.full_name} beam parameters with -q*.")

                data[next_node.opposite] = TraceDataEntry(
                    -qx.conjugate(),
                    -qy.conjugate(),
                    q_set_mode
                )

    def __run_utiltrace(self, path, q_in, datax, datay):
        """Runs a utility beam trace using a user-specified input
        beam parameter at a given node.
        """
        qs_x = [q_in]
        qs_y = [q_in]
        L = 0.0
        gouy_x = 0.0
        gouy_y = 0.0
        _gouys_x = [0.0]
        _gouys_y = [0.0]
        gouy_ref_x = None
        gouy_ref_y = None

        path_nodes = path.nodes_only
        path_comps = path.components_only

        if datax is not None:
            datax['nodes'] = path_nodes
            datax['components'] = path_comps
        if datay is not None:
            datay['nodes'] = path_nodes
            datay['components'] = path_comps

        for i, (curr_node, to_comp) in enumerate(path):
            if to_comp != path_comps[-1]:
                next_node = path_nodes[i + 1]
                Mabcd_x = to_comp.ABCD(curr_node, next_node, direction='x')
                Mabcd_y = to_comp.ABCD(curr_node, next_node, direction='y')
            else:
                next_node = curr_node
                Mabcd_x = self.__identity
                Mabcd_y = self.__identity

            # get the refractive indices of the spaces attached
            # to the current and next node
            if isinstance(to_comp, Space):
                curr_node_nr = to_comp.nr
                next_node_nr = to_comp.nr
            else:
                curr_node_nr, next_node_nr = refractive_indices(curr_node, next_node)

            qx_new = BeamParam(q=apply(Mabcd_x, qs_x[-1], curr_node_nr, next_node_nr))
            qy_new = BeamParam(q=apply(Mabcd_y, qs_y[-1], curr_node_nr, next_node_nr))

            if not Mabcd_x[1, 0]:
                gouy_x = _gouys_x[-1]
                gouy_ref_x = None
            if not Mabcd_y[1, 0]:
                gouy_y = _gouys_y[-1]
                gouy_ref_y = None

            if isinstance(to_comp, Space):
                qx = qs_x[-1]
                qy = qs_y[-1]
                z = np.linspace(0, to_comp.L, 1000)
                gx = np.degrees(qx.gouy(z + qx.z))
                gy = np.degrees(qy.gouy(z + qy.z))

                if gouy_ref_x is None: gouy_ref_x = gx[0]
                if gouy_ref_y is None: gouy_ref_y = gy[0]

                _gouys_x = gouy_x + gx - gouy_ref_x
                _gouys_y = gouy_y + gy - gouy_ref_y

                if datax is not None:
                    datax[curr_node] = {'q' : qs_x[-1], 'z' : L, 'gouy' : _gouys_x[0]}
                if datay is not None:
                    datay[curr_node] = {'q' : qs_y[-1], 'z' : L, 'gouy' : _gouys_y[0]}

                L += to_comp.L
            else:
                if datax is not None:
                    datax[curr_node] = {'q' : qs_x[-1], 'L' : L, 'gouy' : _gouys_x[-1]}
                if datay is not None:
                    datay[curr_node] = {'q' : qs_y[-1], 'L' : L, 'gouy' : _gouys_y[-1]}

            if datax is not None:
                datax[to_comp] = {
                    'qin' : qs_x[-1],
                    'qout' : qx_new,
                    'z' : L,
                    'gouy' : _gouys_x[-1],
                    'gouy_i' : gouy_x,
                    'gouy_ref' : gouy_ref_x,
                    'is_space': isinstance(to_comp, Space)
                }
            if datay is not None:
                datay[to_comp] = {
                    'qin' : qs_y[-1],
                    'qout' : qy_new,
                    'z' : L,
                    'gouy' : _gouys_y[-1],
                    'gouy_i' : gouy_y,
                    'gouy_ref' : gouy_ref_y,
                    'is_space' : isinstance(to_comp, Space)
                }

            qs_x.append(qx_new)
            qs_y.append(qy_new)

            if isinstance(to_comp, Space):
                if datax is not None: datax[to_comp]['L'] = to_comp.L
                if datay is not None: datay[to_comp]['L'] = to_comp.L

        if datax is not None:
            trace_x = TraceSolution(
                datax, False, self.__model,
                TraceType.UTILITY,
                q_in=q_in, q_out=qx_new
            )

        if datay is not None:
            trace_y = TraceSolution(
                datay, False, self.__model,
                TraceType.UTILITY,
                q_in=q_in, q_out=qy_new
            )

        if datax is not None and datay is not None:
            return trace_x, trace_y
        if datax is not None: return trace_x
        return trace_y
