"""
Output data structure for non-utility beam trace executions.
"""

from collections import OrderedDict, namedtuple
from copy import deepcopy

import networkx as nx
import numpy as np

from finesse.components import Laser
from finesse.gaussian import BeamParam
from finesse.utilities import SI, SI_VALUE, SI_LABEL

from finesse.tracing import TraceType

TraceDataEntry = namedtuple("TraceDataEntry", "qx qy set_mode")


class TraceSolution(object):
    """
    Stores the results of a beam trace.

    This class is used for both simulation beam traces (i.e. calls
    to :meth:`.Model.beam_trace`) and utility beam traces (i.e.
    calls to :meth:`.Model.beam_trace_path`).
    """
    def __init__(self, trace_results, symbolic, model, trace_type, **kwargs):
        """Constructs a new instance of `TraceSolution`.

        Parameters
        ----------
        trace_results : OrderedDict
            The results of the beam trace.

        symbolic : bool
            Flag notifying whether the trace was symbolic or not.

        model : :class:`.Model`
            The model instance associated with the trace.

        trace_type : :class:`.TraceType`
            The type of trace that was performed.
        """
        if symbolic:
            raise NotImplementedError()
        self.__data = trace_results
        self.__symbolic = symbolic
        self.__model = model
        self.__trace_type = trace_type
        if trace_type == TraceType.SIMULATION:
            if not self.__symbolic:
                self.__informative = OrderedDict()
                self.__astigmatism = any([qx != qy for qx, qy, _ in self.__data.values()])
                for node, (qx, qy, _) in self.__data.items():
                    if self.__astigmatism:
                        self.__informative[node] = (qx, qx.w, qx.gouy(), qy, qy.w, qy.gouy())
                    else:
                        self.__informative[node] = (qx, qx.w, qx.gouy())
        elif trace_type == TraceType.UTILITY:
            q_in = kwargs.get("q_in")
            if q_in is None:
                raise ValueError("q_in must be specified for a utility trace.")

            q_out = kwargs.get("q_out")
            if q_out is None:
                raise ValueError("q_out must be specified for a utility trace.")

            self.q_in = q_in
            self.q_out = q_out
        else:
            raise ValueError("A TraceSolution can only be initialised with trace_type "
                             "arguments of TraceType.SIMULATION or TraceType.UTILITY.")

    def __ordered_informative(self):
        lsr_nodes = [n for n in self.__informative.keys()
                     if isinstance(n.component, Laser)]
        if not lsr_nodes:
            return None

        lsr_out_node = lsr_nodes[0] if not lsr_nodes[0].is_input else lsr_nodes[1]
        # traverse the network from the laser output
        ordered = list(nx.dfs_preorder_nodes(
            self.__model.optical_network, source=lsr_out_node.full_name
        ))

        d = OrderedDict()
        for node_name in ordered:
            node = self.__model.network.node[node_name]["weakref"]()
            d[node] = self.__informative[node]

        # tack on nodes which weren't directly reachable from the laser source traversal
        unreachable = set(self.__informative.keys()).difference(d.keys())
        for node in unreachable:
            d[node] = self.__informative[node]

        return d

    def __str__(self):
        from tabulate import tabulate

        table = []
        headers = []
        if self.__trace_type == TraceType.SIMULATION:
            headers.append("Optical node")

            ordered = self.__ordered_informative()
            if ordered is None: ordered = self.__informative

            if self.__astigmatism:
                headers.extend(["Beam parameter qx (tangential)", "Beam size [cm]", "Gouy phase [deg]",
                                "Beam parameter qy (sagittal)", "Beam size [cm]", "Gouy phase [deg]"])
                for node, (qx, w_x, gouy_x, qy, w_y, gouy_y) in ordered.items():
                    gouy_x = np.degrees(gouy_x)
                    gouy_y = np.degrees(gouy_y)
                    w_x /= SI_VALUE[SI.CENTI]
                    w_y /= SI_VALUE[SI.CENTI]
                    table.append([node.full_name, qx, w_x, gouy_x, qy, w_y, gouy_y])
            else:
                headers.extend(["Beam parameter q", "Beam size [cm]", "Gouy phase [deg]"])

                for node, (q, w, gouy) in ordered.items():
                    gouy_x = np.degrees(gouy)
                    w /= SI_VALUE[SI.CENTI]
                    table.append([node.full_name, q, w, gouy])

        elif self.__trace_type == TraceType.UTILITY:
            headers = ["Name", "z [m]", "w0 [mm]", "Beam radius [mm]",
                       "RoC [m]", "Acc. Gouy [deg]", "q"]
            nodes = self.__data["nodes"]
            comps = self.__data["components"]

            table = [
                [
                    comp.name,
                    self.__data[comp]['z'],
                    self.__data[nodes[comps.index(comp)]]['q'].w0/1e-3,
                    self.__data[nodes[comps.index(comp)]]['q'].w/1e-3,
                    self.__data[nodes[comps.index(comp)]]['q'].Rc,
                    self.__data[comp]['gouy'],
                    self.__data[comp]['qout']
                ] for comp in comps if not self.__data[comp]['is_space']
            ]

            first_node = nodes[1] if nodes[0].is_input else nodes[0]

            table.insert(
                0,
                [
                    first_node,
                    0,
                    self.__data[first_node]['q'].w0/1e-3,
                    self.__data[first_node]['q'].w/1e-3,
                    self.__data[first_node]['q'].Rc,
                    0,
                    self.__data[first_node]['q'],
                ]
            )

            last_space_node = nodes[-2] if nodes[-1].is_input else nodes[-1]

            table.append([
                last_space_node.full_name,
                self.__data[last_space_node]['z'],
                self.__data[last_space_node]['q'].w0/1e-3,
                self.__data[last_space_node]['q'].w/1e-3,
                self.__data[last_space_node]['q'].Rc,
                self.__data[last_space_node]['gouy'],
                self.__data[last_space_node]['q'],
            ])

        return tabulate(table, headers, tablefmt="fancy_grid", missingval="None")

    def print(self):
        print(str(self))

    def __deepcopy__(self, memo):
        if self.__trace_type == TraceType.SIMULATION:
            result = type(self)(
                deepcopy(self.__data), self.__symbolic, self.__model,
                TraceType.SIMULATION
            )
        else:
            result = type(self)(
                deepcopy(self.__data), self.__symbolic, self.__model,
                TraceType.UTILITY, q_in=self.q_in, q_out=self.q_out
            )

        memo[id(self)] = result
        for k, v in self.__dict__.items():
            setattr(result, k, deepcopy(v, memo))
        return result

    def __getitem__(self, node):
        return self.__data[node]

    def get(self, node, default=None):
        return self.__data.get(node, default)

    def evaluate(self, params=None):
        if not self.__symbolic:
            raise RuntimeError("evalulate can only be called on symbolic TraceSolution objects.")

        if self.__trace_type == TraceType.UTILITY:
            raise NotImplementedError()

        # TODO (sjr) might be able to re-use some of this for non-sympy symbolic evaluations
        #as_symbols = params is None
        #if params is None:
        #    params = {}
        #    for comp in self.__model.components:
        #        for p in comp._params:
        #            if p.symbol is not None:
        #                params[p.symbol] = p.value
        #    for space in self.__model.spaces:
        #        for p in space._params:
        #            if p.symbol is not None:
        #                params[p.symbol] = p.value

        #numeric = OrderedDict()

        #for node, (qx, qy, q_set_mode) in self.__data.items():
        #    sub_params_x = {}
        #    sub_params_y = {}
        #    for sx in qx.free_symbols:
        #        if as_symbols:
        #            sub_params_x[sx] = params[sx]
        #        else:
        #            sub_params_x[sx] = params[str(sx)]
        #    for sy in qy.free_symbols:
        #        if as_symbols:
        #            sub_params_y[sy] = params[sy]
        #        else:
        #            sub_params_y[sy] = params[str(sy)]
        #    numeric[node] = TraceDataEntry(
        #        BeamParam(q=qx.subs(sub_params_x)),
        #        BeamParam(q=qy.subs(sub_params_y)),
        #        q_set_mode
        #    )

        #return TraceSolution(numeric, False, self.__model, TraceType.SIMULATION)

    @property
    def data_qx(self):
        """A copy of the underlying `data` dictionary but with only
        the tangential plane beam parameters selected.

        :getter: Returns a dictionary of traced nodes with corresponding
                 tangential plane beam parameters (read-only).
        """
        if self.__trace_type == TraceType.UTILITY:
            raise RuntimeError("TraceSolution.data_qx can only be accessed for simulation "
                               "type traces.")

        qxs = OrderedDict()
        for node, (qx, _, _) in self.__data.items():
            qxs[node] = qx
        return qxs

    @property
    def data_qy(self):
        """A copy of the underlying `data` dictionary but with only
        the sagittal plane beam parameters selected.

        :getter: Returns a dictionary of traced nodes with corresponding
                 sagittal plane beam parameters (read-only).
        """
        if self.__trace_type == TraceType.UTILITY:
            raise RuntimeError("TraceSolution.data_qy can only be accessed for simulation "
                               "type traces.")

        qys = OrderedDict()
        for node, (_, qy, _) in self.__data.items():
            qys[node] = qy
        return qys

    def plot(self, filename=None, show=True, w_scale=SI.MILLI, markers=None):
        """Plots the beam size and Gouy phase as a function of distance
        along the optical axis which the beam was traced through. The
        positions of components along the traced path are marked on the
        figure.

        Parameters
        ----------
        filename : str, optional
            Optional name of file for saving the figure to.

        show : bool, optional
            Flag determining whether to show the figure, `True` by default.

        w_scale : :class:`.SI`, optional
            Distance scale for beam sizes, `SI.MILLI` by default.

        markers : list, optional
            Optional list of markers to place on the figure.

        Returns
        -------
        fig, axes : :class:`matplotlib.figure.Figure`, tuple of :class:`matplotlib.axis.Axis`
            A tuple of the figure and another tuple containing the two axis handles.
        """
        if self.__trace_type == TraceType.SIMULATION:
            raise NotImplementedError()

        import matplotlib.pyplot as plt

        data = self.__data

        fig = plt.figure()
        ax1 = plt.subplot(211)
        ax2 = plt.subplot(212)

        w_max = 0
        g_max = 0
        gouy = 0

        for comp, from_node in zip(data['components'], data['nodes']):
            gouy = data[comp]['gouy_i']
            gouy_ref = data[comp]['gouy_ref']

            if data[comp]['is_space']:
                L = data[comp]['L']
                q = data[from_node]['q']
                z = data[from_node]['z']
                _z = np.linspace(0, L, 1000)
                g = np.rad2deg(q.gouy(_z+q.z))
                # want to plot accumulated gouy phase so need to use
                # a reference from where it started
                _g = gouy + g - gouy_ref
                w = q.beamsize(_z + q.z)/SI_VALUE[w_scale]
                w_max = max(w_max, w.max())
                g_max = max(g_max, _g.max())
                ax1.plot(z+_z, w)
                ax2.plot(z+_z, _g)
            else:
                z = data[comp]['z']
                ax1.scatter(z, 0, marker='x', color='k')
                ax1.text(z, 0, comp.name+"\n", ha="center", va='bottom', zorder=100)

                ax2.scatter(z, 0, marker='x', color='k')
                ax2.text(z, 0, comp.name+"\n", ha="center", va='bottom', zorder=100)

        if markers is not None:
            for _, z in markers:
                ax1.scatter(z, 0, marker='x', color='r')
                ax1.text(z, 0, _+"\n", ha="center", va='bottom', zorder=100)

                ax2.scatter(z, 0, marker='x', color='r')
                ax2.text(z, 0, _+"\n", ha="center", va='bottom', zorder=100)

        ax1.grid(True, zorder=-10)
        ax1.set_xlim(0, None)

        if w_scale is None:
            ax1.set_ylabel("Beam size [m]")
        else:
            ax1.set_ylabel("Beam size [{}m]".format(SI_LABEL[w_scale]))

        ax1.set_xlabel("Distance [m]")
        ax1.set_ylim(0, w_max)
        ax2.set_xlim(0, None)
        ax2.grid(True, zorder=-10)
        ax2.set_ylabel("Gouy phase [deg]")
        ax2.set_xlabel("Distance [m]")
        ax2.set_ylim(0, g_max)

        plt.tight_layout()

        if filename is not None: plt.savefig(filename)
        if show: plt.show()

        return z + _z, w, _g
