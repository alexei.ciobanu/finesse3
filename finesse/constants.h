#ifndef FINESSE_CONSTANTS_H_
#define FINESSE_CONSTANTS_H_

#include <complex.h>
#include <float.h>

double C_LIGHT = 299792458.0;
long double PI = 3.14159265358979323846;

double complex COMPLEX_0 = 0.0 + 0.0*I;
double complex COMPLEX_1 = 1.0 + 0.0*I;
double complex COMPLEX_I = 0.0 + 1.0*I;

#endif // !FINESSE_CONSTANTS_H_
