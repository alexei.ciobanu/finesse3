"""
The fundamental elements of a Finesse model - including parameters,
symbols and operations.
"""

from collections import defaultdict
from enum import Flag, auto
import weakref

import numpy as np
import sympy

from finesse.exceptions import ComponentNotConnected, ParameterLocked
from finesse.utilities import check_name
from copy import deepcopy

import operator
import finesse.element
import logging

LOGGER = logging.getLogger(__name__)

MAKE_LOP = lambda opfn : lambda self,other : finesse.element.Operation(opfn, self, other)
MAKE_ROP = lambda opfn : lambda self,other : finesse.element.Operation(opfn, other, self)
identity = lambda x : x

add = operator.add
sub = operator.sub
mul = operator.mul
pow = operator.pow

op_repr = {
        operator.add :  '({}+{})',
        operator.sub :  '({}-{})',
        operator.mul :  '{}*{}',
        operator.pow :  '{}**{}',
        operator.truediv :  '{}/{}',
        operator.floordiv :  '{}//{}',
        operator.mod :  '({}%{})',
        operator.matmul :  '({}@{})',
        operator.neg : '-{}',
        operator.pos : '-{}',
        operator.abs : '|{}|',
        np.conj : 'conj{}',
        np.sqrt : '√({})',
        identity : '{}'
    }

def display(a):
    """
    For a given Symbol this method will return a human readable string
    representing the various operations it contains.

    Parameters
    ----------
    a : :class:`.Symbol`
        Symbol to print

    Returns
    -------
    String form of Symbol
    """
    if hasattr(a, 'op'):
        # Check if operation has a predefined string format
        if a.op in op_repr:
            sop = op_repr[a.op]
        else: # if not just treat it as a function
            sop = a.op.__name__ + "(" + ("{}," * len(a.args)).rstrip(',') + ")"

        sargs = (display(_) for _ in a.args)

        return sop.format(*sargs)
    elif hasattr(a, 'name'): # Anything with a name attribute just display that
        return a.name
    else:
        return str(a)

class Symbol(object):
    __add__  = MAKE_LOP(operator.add)
    __sub__  = MAKE_LOP(operator.sub)
    __mul__  = MAKE_LOP(operator.mul)
    __radd__ = MAKE_ROP(operator.add)
    __rsub__ = MAKE_ROP(operator.sub)
    __rmul__ = MAKE_ROP(operator.mul)
    __pow__  = MAKE_LOP(operator.pow)
    __truediv__ = MAKE_LOP(operator.truediv)
    __rtruediv__ = MAKE_ROP(operator.truediv)
    __floordiv__ = MAKE_LOP(operator.floordiv)
    __rfloordiv__ = MAKE_ROP(operator.floordiv)
    __matmul__  = MAKE_LOP(operator.matmul)

    def conjugate(self): return Operation(np.conj, self)
    def conj(self): return Operation(np.conj, self)
    def exp(self):  return Operation(np.exp, self)
    def sin(self):  return Operation(np.sin, self)
    def cos(self):  return Operation(np.cos, self)
    def tan(self):  return Operation(np.tan, self)
    def sqrt(self): return Operation(np.sqrt, self)
    def radians(self): return Operation(np.radians, self)

    def __abs__(self): return Operation(operator.abs, self)
    def __neg__(self): return Operation(operator.neg, self)
    def __pos__(self): return Operation(operator.pos, self)

    def __float__(self): return float(self.eval())
    def __complex__(self): return complex(self.eval())

    @property
    def value(self):
        return self.eval()

    def __str__(self):
        return display(self)

    def __repr__(self):
        return display(self)

    @property
    def is_changing(self):
        """
        Returns True if one of the arguements of this symbolic object
        is varying whilst a :class:`` is running.
        """
        res = False

        if hasattr(self, 'op'):
            res = any([_.is_changing for _ in self.args])
        elif hasattr(self, 'parameter'):
            res = self.parameter.is_tunable or self.parameter.is_changing

        return res

    def parameters(self, memo=None):
        """
        Returns all the parameters that are present in this symbolic statement
        """
        if memo is None:
            memo = set()

        if hasattr(self, 'op'):
            for _ in self.args: _.parameters(memo)
        elif hasattr(self, 'parameter'):
            memo.add(self)

        return list(memo)

    def changing_parameters(self):
        p = np.array(self.parameters())
        return list(p[list(map(lambda x: x.is_changing, p))])

class Constant(Symbol):
    def __init__(self, value):
        self.__value = value
        self.op = identity
        self.args = (self.__value,)

    def __str__(self):
        return str(self.__value)

    def __eq__(self, obj):
        if isinstance(obj, Constant):
            return obj.value == self.__value
        else:
            return False

    def eval(self, **kwargs):
        return self.__value

class AlgebraicSymbol(Symbol):
    def __init__(self, name):
        import copy
        self.__name = name
        self.op = identity
        self.args = (self.__name,)

    def __str__(self):
        return str(self.__name)

    def __eq__(self, obj):
        if isinstance(obj, Constant):
            return obj.name == self.__name
        else:
            return False

    def eval(self, **kwargs):
        return self

class Operation(Symbol):
    """
    This is a symbol to represent a mathematical operation. This could be
    a simple addition, or a more complicated multi-argument function.

    It supports creating new mathematical operations::

        import math
        import cmath

        cos   = lambda x: finesse.element.Operation(math.cos, x)
        sin   = lambda x: finesse.element.Operation(math.sin, x)
        atan2 = lambda y, x: finesse.element.Operation(math.atan2, y, x)

    Complex math can also be used::

        import numpy as np
        angle = lambda x: finesse.element.Operation(np.angle, x)
        print(f"{angle(1+1j)} = {angle(1+1j).eval()}")

    The equality operator is overridden to provide a very basic
    symbolic equality test. It only tests whether two symbolic
    statements are *exactly* the same, e.g.:

        >>> y+x == y+x # True
        >>> y+x == x+y # False

    This could be fixed by making operators more
    """
    def __init__(self, operation, *args):
        self.args = tuple(asSymbol(_) for _ in args)
        self.op   = operation

    def eval(self, **kwargs):
        return self.op(*(_.eval(**kwargs) for _ in self.args))

    def __eq__(self, obj):
        if isinstance(obj, Operation):
            if self.op == obj.op:
                return all([a==b for a,b in zip(self.args, obj.args)])
            else:
                return False
        else:
            return False

class ParameterRef(Symbol):
    """
    A symbolic instance of a parameter in the model. A parameter
    is owned by some model element, which can be used to connect
    its value to other aspects of the simulation.
    """
    def __init__(self, param):
        self.__param = weakref.ref(param)

    @property
    def parameter(self):
        if self.__param is None:
            self.__param = weakref.ref(getattr(self.__new_comp(), self.__param_name))

        return self.__param()

    @property
    def name(self):
        if self.__param is None:
            return f"{self.__new_comp.name}.{self.__param_name}"
        else:
            p = self.__param()
            return f"{p.component.name}.{p.name}"

    @property
    def owner(self):
        return self.parameter.component

    def __eq__(self, obj):
        if isinstance(obj, ParameterRef):
            if obj.parameter is self.parameter: return True
        elif isinstance(obj, Parameter):
            if obj is self.parameter: return True

    def __hash__(self):
        return hash(self.parameter)

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        result.__dict__.update(deepcopy(self.__dict__, memo))

        if id(self.parameter.owner) in memo:
            comp = memo[id(self.parameter.owner)]
            result.__new_comp = weakref.ref(comp)
            result.__param_name = self.parameter.name
            result.__param = None
        else:
            raise Exception(f"Symbol {self.name} could not find a new deepcopied parameter for {self.parameter.name}")

        return result

    def eval(self, keep_changing_symbols=False, subs={}, **kwargs):
        p = self.parameter

        if keep_changing_symbols and p.is_changing:
            return self
        if self in subs:
            return subs[self]
        if hasattr(p.value, "eval"):
            return p.value.eval()
        return p.value


asSymbol = lambda x : x if isinstance(x, Symbol) else Constant(x)

class Rebuild(Flag):
    NA = auto() # No rebuild required
    PlaneWave = auto()
    Frequencies = auto()
    HOM = auto()


class Parameter(object):
    def __init__(self, name, value, units, flag, component):
        self.__value = value
        self.__units = units
        self.__name  = name
        self.__component = weakref.ref(component)
        self._locked = False
        self.__rebuild_flag = flag
        self.__is_tunable = False

    def __deepcopy__(self, memo):
        new = object.__new__(type(self))
        memo[id(self)] = new
        new.__dict__.update(deepcopy(self.__dict__, memo))
        # Manually update the weakrefs to be correct
        new.__component = weakref.ref(memo[id(self.component)])
        return new

    @property
    def rebuild(self): return self.__rebuild_flag

    @property
    def ref(self): return ParameterRef(self)

    @property
    def is_changing(self):
        if isinstance(self.value, Symbol):
            return self.value.is_changing
        else:
            return self.is_tunable

    @property
    def is_tunable(self):
        return self.__is_tunable

    @is_tunable.setter
    def is_tunable(self, value):
        if self.locked: raise ParameterLocked()
        if value and isinstance(self.__value, Symbol):
            raise Exception(f"Tunable state of {repr(self)} cannot be set to True when its value is symbolic.")

        self.__is_tunable = value

    def resolve(self, mapping={}):
        if callable(self.value):
            self.value = self.value(self.__component()._model, mapping)

    def eval(self, keep_changing_symbols=False):
        """
        Evaluates the value of this parameter. If the parameter
        is dependant on some symbolic statement this will evaluate
        that. If it is not the value itself is returned. This method
        should be used when filling in matrices for computing solutions
        of a model.
        """
        self.resolve()
        if hasattr(self.value, 'eval'):
            y = self.value.eval(keep_changing_symbols=keep_changing_symbols)

            fn = ModelElement._validators[type(self.owner)][self.name]

            if fn is None:
                return y
            else:
                return fn(self.owner, y)

        else:
            return self.value

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, value):
        if self.locked: raise ParameterLocked()
        if self.__is_tunable and isinstance(value, Symbol):
            LOGGER.warn(f"{repr(self)}: Setting `is_tunable` to False as new "
                                 "value is symbolic, is_tunable was previously True.")
            self.__is_tunable = False

        fn = ModelElement._validators[type(self.owner)][self.name]
        if fn is None:
            self.__value = value
        else:
            self.__value = fn(self.owner, value)

    @property
    def units(self):
        if self.__units is None: return ''
        return self.__units

    @property
    def name(self): return self.__name

    @property
    def component(self): return self.__component()

    @property
    def owner(self): return self.__component()

    @property
    def locked(self):
        return self._locked

    @property
    def _model(self):
        """
        Returns the model that this component is connected to. Raises an exception if
        it is not connected to anything to stop code accidently using unconnected
        components by accident.

        Returns
        -------
        :class:`.model.Model`

        Raises
        ------
        :class:`.exceptions.ComponentNotConnected`
        """
        if self.__component() is None:
            raise ComponentNotConnected("Lost weak reference")
        else:
            return self.__component()._model

    def _set(self, value):
        self.value = value

    def __repr__(self):
        if self.__component() is None:
            return f"<LOST WEAKREF.{self.name}={self.value}>"
        else:
            return f"<{self.__component().name}.{self.name}={self.value} @ {hex(id(self))}>"

    def __pow__(self, power): return self.value**power
    def __mul__(self, other): return  other * self.value
    def __rmul__(self, other): return other * self.value
    def __add__(self, other): return  other + self.value
    def __radd__(self, other): return other + self.value
    def __sub__(self, other): return  self.value - other
    def __rsub__(self, other): return other - self.value
    def __floordiv__(self, other): return self.value // other
    def __truediv__(self, other): return self.value / other
    def __rtruediv__(self, other): return other / self.value
    def __complex__(self): return complex(self.value)
    def __float__(self): return float(self.value)
    def __int__(self): return int(self.value)
    def __str__(self): return str(self.value)


class parameter(object): # __NODOC__
    """
    Descriptor class for declaring a simulation parameter. A simulation parameter
    is one that can be changed during a simulation and affect the resulting outputs.
    The idea is that output dependant variables should be marked as having been changed
    or will be changed during a simulation run. This allows us to then optimise parts of
    the model, as we can determine what will or will not be changing. This descriptor is
    paired with the :class:Parameter.

    Parameters can then be superficially locked once a model has been built so accidentally
    changing some parameter that isn't expected to change can flag a warning.
    """
    def __init__(self, fget, fset, flocked, doc=None):
        self.fget = fget
        self.fset = fset
        self.flocked = flocked

        if doc is None and fget is not None:
            doc = fget.__doc__
        self.__doc__ = doc

    def __get__(self, obj, objtype=None):
        if obj is None:
            return self
        else:
            return self.fget(obj)

    def __set__(self, obj, value):
        if self.flocked(obj):
            raise ParameterLocked()
        else:
            self.fset(obj, value)

    def __delete__(self, obj):
        raise AttributeError("can't delete parameter")

    def getter(self, fget):
        return type(self)(fget, self.fset, self.fdel, self.__doc__)

    def setter(self, fset):
        return type(self)(self.fget, fset, self.fdel, self.__doc__)


def model_parameter(
    name, default_value, rebuild_flag,
    units=None, validate=None
):
    if validate is None:
        vld    = None
    else:
        vld    = lambda x,v: getattr(x, validate)(v)

    def func(cls):
        p = parameter(lambda x: getattr(x, f'__param_{name}'),
                      lambda x, v: getattr(x, f'__param_{name}')._set(v),
                      lambda x: getattr(x, f'__param_{name}').locked
                     )

        cls._param_dict[cls].append((name, default_value, units, rebuild_flag))
        cls._validators[cls][name] = vld

        setattr(cls, name, p)
        return cls

    return func


class ModelElement(object):
    """
    The base class for any object which can be an element of a :class:`.Model`.

    When added to a model it will attempt to call the method `_on_add` so that
    the element can do some initialisation if required.
    """

    # A global dictionary to keep a record of all the declared
    # model parameters, this does not store actual references to
    #
    _param_dict = defaultdict(list)
    _validators = defaultdict(dict)

    def __new__(cls, *args, **kwargs):
        instance = super(ModelElement, cls).__new__(cls)
        instance._params = []

        # Loop through each of the parameters that have been defined
        # in the class and instantiate an object to represent them
        # for this instance of the object
        for name, val, unit, flag in instance._param_dict[cls]:
            p = Parameter(name, val, unit, flag, instance)
            setattr(instance, f"__param_{name}", p)
            instance._params.append(p)

        return instance

    def __init__(self, name):
        """
        Constructs a new `ModelElement` instance.

        Parameters
        ----------
        name : str
            Name of newly created sub-instance of `ModelElement`.
        """
        self.__model = None
        self.__name  = check_name(name)
        self._add_to_model_namespace = True
        self._namespace = "."

    def __deepcopy__(self, memo):
        new = object.__new__(type(self))
        memo[id(self)] = new
        new.__dict__.update(deepcopy(self.__dict__, memo))
        # Manually update the weakrefs to be correct
        new.__model = weakref.ref(memo[id(self.__model())])
        return new

    @property
    def parameters(self):
        return self._params.copy()

    @property
    def name(self):
        """Name of the element.

        Returns
        -------
        str
            The name of the element.
        """
        return self.__name

    @property
    def full_name(self):
        return self.component.name + self.name

    @property
    def _model(self):
        """
        Internal reference to the model this element has been added to.

        Raises
        ------
        ComponentNotConnected when not connected
        """
        if self.__model is None:
            raise ComponentNotConnected(f"{self.name} is not connected to a model")
        else:
            return self.__model()

    @property
    def has_model(self):
        return self.__model is not None

    def _set_model(self, model):
        """
        A :class:`.Model` instance calls this to associate itself
        with the element.

        .. note::
            This method should never be called by the user, it should
            only be called internally by the :class:`.Model` class.

        Parameters
        ----------
        model : :class:`.Model`
            The model to associate with this element.

        Raises
        ------
        Exception
            If the model is already set for this element.
        """
        if self.__model is not None:
            raise Exception("Model is already set for this element")

        self.__model = weakref.ref(model)

    def _reset_model(self, new_model):
        self.__model = weakref.ref(new_model)

    def _eval_parameters(self):
        return ({p.name:(p.value.eval() if hasattr(p.value,'eval') else p.value) for p in self.parameters},
                {p.name:(p.value.is_changing if hasattr(p.value,'eval') else p.is_tunable) for p in self.parameters})



def my_measure(expr):
    ADD, SUB, MUL, DIV, NEG, EXP, POW, SIN = sympy.symbols('ADD, SUB, MUL, DIV, NEG, EXP, POW, SIN')
    CONJUGATE = sympy.symbols('CONJUGATE')
    count = sympy.count_ops(expr, visual=True)
#     display(count)
    count = count.subs(NEG, 1)
    count = count.subs(CONJUGATE, 1)
    count = count.subs(ADD, 64)
    count = count.subs(SUB, 64)
    count = count.subs(MUL, 64**2)
    count = count.subs(DIV, 64**2)
    count = count.subs(EXP, 64**2*np.log(64))
    count = count.subs(POW, 64**2*np.log(64))
    count = count.subs(SIN, 64**2*np.log(64))
#     display(count)
    # Every other operation gets max weight
    count = count.replace(sympy.Symbol, lambda: 64**2*np.log(64))
    return count

def finesse2sympy(expr, iter_num=0):
#     print(iter_num)
    iter_num += 1
    if isinstance(expr, Constant):
        return expr.value
    elif isinstance(expr, ParameterRef):
        return sympy.Symbol(expr.name)
    elif isinstance(expr,Operation):
        sympy_args = [finesse2sympy(arg,iter_num) for arg in expr.args]
        if expr.op == mul:
            op = sympy.Mul
        elif expr.op == add:
            op = sympy.Add
        elif expr.op == np.conj:
            op = sympy.conjugate
        elif expr.op == np.radians:
            op = sympy.rad
        elif expr.op == np.exp:
            op = sympy.exp
        elif expr.op == np.sqrt:
            op = sympy.sqrt
        else:
            raise Exception(f"undefined Operation {expr.op} in {expr}")
        return op(*sympy_args)
    else:
        raise Exception(f'{expr} undefined')
    
def sympy2finesse(expr, symbol_dict, iter_num=0):
#     print(iter_num,expr)
    iter_num += 1
    if expr.is_number:
        return complex(expr)
    elif expr.is_symbol:
        return symbol_dict[str(expr)]
    elif isinstance(expr,sympy.Mul):
        return np.product([sympy2finesse(arg, symbol_dict, iter_num=iter_num) for arg in expr.args])
    elif isinstance(expr,sympy.Add):
        return np.sum([sympy2finesse(arg, symbol_dict, iter_num=iter_num) for arg in expr.args])
    elif isinstance(expr,sympy.conjugate):
        return np.conj(sympy2finesse(*expr.args, symbol_dict))
    elif isinstance(expr,sympy.exp):
        return np.exp(sympy2finesse(*expr.args, symbol_dict))
    elif isinstance(expr,sympy.Pow):
        return np.power(sympy2finesse(expr.args[0], symbol_dict), sympy2finesse(expr.args[1], symbol_dict))
    else:
        raise Exception(f'{expr} undefined')