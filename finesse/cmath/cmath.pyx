"""Fast C functions for common mathematical routines
used across all the various Cython extensions of Finesse.

**This pure Cython extension is for those working on their own
Cython extensions for Finesse only. There are no Python functions
exposed by this sub-module, users of Finesse can safely ignore
this extension.**

.. note::
    **For developers:**

    The functions in this extension are to be used by the Cython extensions
    of Finesse, as they are optimised C-based functions only - no Python API
    is exposed.

    If a function you are writing requires exposure to Python such that it can be
    called from the non-Cython parts of Finesse, then use :mod:`finesse.utilities.maths`.

    See :ref:`cython_guide` for further details.
"""

cimport numpy as np

cdef extern from "complex.h" nogil:
    double cabs(double complex z)
    double complex cpow(double complex x, double complex y)
    double cimag(double complex z)
    double creal(double complex z)
    double complex csqrt(double complex z)
    double complex conj(double complex z)

cdef extern from "math.h" nogil:
    double atan2(double y, double x)
    double cos(double arg)
    double exp(double arg)
    double fabs(double arg)
    double fmax(double x, double y)
    double sin(double arg)
    double sqrt(double arg)

cdef extern from "../constants.h":
    long double PI
    double complex COMPLEX_0 # 0 + 0i
    double complex COMPLEX_1 # 1 + 0i

ctypedef np.complex128_t complex_t

#ctypedef fused real_numeric_t: # non-complex numeric type
#    short
#    int
#    long
#    float
#    double

cdef class ComplexMath:
    """A namespace of functions for complex mathematical routines."""
    @staticmethod
    cdef double norm(double complex z) nogil:
        return creal(z) * creal(z) + cimag(z) * cimag(z)

    @staticmethod
    cdef bint ceq(complex_t z1, complex_t z2) nogil:
        cdef:
            double maximum
            int re = 0
            int im = 0

        maximum = fmax(
            fmax(fabs(creal(z1)), fabs(creal(z2))),
            fmax(fabs(cimag(z1)), fabs(cimag(z2)))
        )

        if maximum < 1e-13: return True

        if creal(z1) == 0.0 and creal(z2) == 0.0:
            re = 1
        else:
            re = fabs(creal(z1) - creal(z2)) / maximum < 1e-13

        if cimag(z1) == 0.0 and cimag(z2) == 0.0:
            im = 1
        else:
            im = fabs(cimag(z1) - cimag(z2)) / maximum < 1e-13

        return re & im

    @staticmethod
    cdef complex_t pow_complex(complex_t z, double n) nogil:
        # NOTE cpow is implementation-dependent, so adding check here
        #      first to avoid problems with (0+0i)^0
        if n == 0.0:
            return COMPLEX_1
        return cpow(z, n)

    @staticmethod
    cdef int inverse(complex_t* res, complex_t z) except -1:
        cdef double inv_abs_sqd_z

        if ComplexMath.ceq(z, COMPLEX_0):
            raise ValueError("Complex division by zero.")

        inv_abs_sqd_z = 1.0 / ComplexMath.norm(z)

        res[0] = creal(z) * inv_abs_sqd_z - 1.0j * cimag(z) * inv_abs_sqd_z
        return 0

    @staticmethod
    cdef complex_t inverse_unsafe(complex_t z) nogil:
        cdef double inv_abs_sqd_z = 1.0 / ComplexMath.norm(z)

        return creal(z) * inv_abs_sqd_z - 1.0j * cimag(z) * inv_abs_sqd_z

    @staticmethod
    cdef complex_t rotate(complex_t z, double ph) nogil:
        cdef:
            complex_t zz
            double cph = cos(ph)
            double sph = sin(ph)

        zz = creal(z) * cph - cimag(z) * sph + 1j * (creal(z) * sph + cimag(z) * cph)

        return zz

    @staticmethod
    cdef complex_t u_nm(
        int n, int m, complex_t qx, complex_t qy,
        double x, double y, double nr, double lambda0
    ) nogil:
        cdef:
            complex_t zx, zy

        zx = ComplexMath.u_single_plane(n, qx, x, nr, lambda0)
        zy = ComplexMath.u_single_plane(m, qy, y, nr, lambda0)

        return zx * zy

    @staticmethod
    cdef complex_t u_single_plane(int n, complex_t q, double x, double nr, double lambda0) nogil:
        cdef:
            double factor
            complex_t phase, z1, z2, z3
            double k = 2.0 * PI / lambda0 * nr

        factor = (
            (2.0/PI)**0.25 / sqrt(2**n * Math.factorial(n) * Gaussian.w0_size(q, nr, lambda0))
            * Math.hermite(n, sqrt(2) * x / Gaussian.w_size(q, nr, lambda0))
        )

        z1 = csqrt((0.0 + cimag(q)*1j) / q)
        z2 = csqrt((0.0 + cimag(q)*1j) * conj(q) / ((0.0 - cimag(q)*1j) * q))
        z3 = z1 * ComplexMath.pow_complex(z2, n)

        phase = (-1.0 * k * x * x / 2.0) * ComplexMath.inverse_unsafe(q)

        factor *= exp(-1.0 * cimag(phase))

        return factor * ComplexMath.rotate(z3, creal(phase))

cdef class Gaussian:
    """A namespace of functions for Gaussian beam physics."""
    @staticmethod
    cdef double gouy(complex_t q) nogil:
        return atan2(creal(q), cimag(q))

    @staticmethod
    cdef double w0_size(complex_t q, double nr, double lambda0) nogil:
        return sqrt(lambda0 * cimag(q) / (PI * nr))

    @staticmethod
    cdef double w_size(complex_t q, double nr, double lambda0) nogil:
        return cabs(q) * sqrt(lambda0 / (nr * PI * cimag(q)))

    @staticmethod
    cdef double z_q(complex_t q) nogil:
        return creal(q)

    @staticmethod
    cdef double z_R(complex_t q) nogil:
        return cimag(q)

cdef class Math:
    """A namespace of functions for non-complex mathematical routines."""
    @staticmethod
    cdef double degrees(double x) nogil:
        return x * 180.0 / PI

    @staticmethod
    cdef double radians(double x) nogil:
        return x * PI / 180.0

    @staticmethod
    cdef double factorial(int n) nogil:
        cdef:
            int i
            double ret

        ret = 1.0
        for i in range(1, n+1):
            ret *= i

        return ret

    @staticmethod
    cdef double msign(int n) nogil:
        return -1.0 if n % 2 else 1.0

    @staticmethod
    cdef int nmin(int n, int m) nogil:
        return n if n < m else m

    @staticmethod
    cdef bint float_eq(double x, double y) nogil:
        if x == 0.0 and y == 0.0:
            return 1
        else:
            return fabs(x - y) / fmax(fabs(x), fabs(y)) < 1e-13

    @staticmethod
    cdef double hermite(int n, double x) nogil:
        if n == 0:
            return (1.0)
        if n == 1:
            return (2.0 * x)
        if n == 2:
            return (4.0 * x * x - 2.0)
        if n == 3:
            return (8.0 * x * x * x - 12.0 * x)
        if n == 4:
            return (16.0 * x**4 - 48.0 * x * x + 12.0)
        if n == 5:
            return (32.0 * x**5 - 160.0 * x * x * x + 120.0 * x)
        if n == 6:
            return (64.0 * x**6 - 480.0 * x**4 + 720.0 * x * x - 120.0)
        if n == 7:
            return (128.0 * x**7 - 1344.0 * x**5 + 3360.0 * x * x * x -
                    1680.0 * x)
        if n == 8:
            return (256.0 * x**8 - 3584.0 * x**6 + 13440.0 * x**4
                    - 13440.0 * x * x + 1680.0)
        if n == 9:
            return (512.0 * x**9 - 9216.0 * x**7 + 48384.0 * x**5
                    - 80640.0 * x * x * x + 30240.0 * x)
        if n == 10:
            return (1024.0 * x**10 - 23040.0 * x**8 + 161280.0 * x**6
                    - 403200.0 * x**4 + 302400.0 * x * x - 30240.0)
        if n == 11:
            return -665280 * x + 2217600 * x**3 - 1774080 * x**5 + 506880 * x**7 - 56320 * x**9 + 2048 * x**11
        if n == 12:
            return 4096 * x**12 - 135168 * x**10 + 1520640 * x**8 - 7096320 * x**6 + 13305600 * x**4 - 7983360 * x*x + 665280
        if n == 13:
            return 8192 * x**13 - 319488 * x**11 + 4392960 * x**9 - 26357760 * x**7 + 69189120 * x**5 - 69189120 * x**5 + 17297280 * x
        if n == 14:
            return 16384 * x**14-745472 * x**12+12300288 * x**10-92252160 * x**8+322882560 * x**6-484323840 * x**4+242161920 * x*x-17297280
        if n == 15:
            return 32768.0 * x**15 - 1720320.0 * x**13 + 33546240.0 * x**11 - 307507200.0 * x**9 + 1383782400.0 * x**7 - 2905943040.0 * x**5 + 2421619200.0 * x**3 - 518918400.0 * x
        if n == 16:
            return 65536.0 * x**16 - 3932160.0 * x**14 + 89456640.0 * x**12 - 984023040.0 * x**10 + 5535129600.0 * x**8 - 15498362880.0 * x**6 + 19372953600.0 * x**4 - 8302694400.0 * x*x + 518918400.0
        if n == 17:
            return 131072.0 * x**17-8912896.0 * x**15+233963520.0 * x**13-3041525760.0 * x**11+20910489600.0 * x**9-75277762560.0 * x**7+131736084480.0 * x**5-94097203200.0 * x**3+17643225600.0 * x
        if n == 18:
            return 262144.0 * x**18-20054016.0 * x**16+601620480.0 * x**14-9124577280.0 * x**12+75277762560.0 * x**10-338749931520.0 * x**8+790416506880.0 * x**6-846874828800.0 * x**4+317578060800.0 * x*x-17643225600.0
        if n == 19:
            return 524288.0 * x**19-44826624.0 * x**17+1524105216.0 * x**15-26671841280.0 * x**13+260050452480.0 * x**11-1430277488640.0 * x**9+4290832465920.0 * x**7-6436248698880.0 * x**5+4022655436800.0 * x**3-670442572800.0 * x
        if n == 20:
            return 1048576.0 * x**20 -99614720.0 * x**18+3810263040.0 * x**16-76205260800.0 * x**14+866834841600.0 * x**12-5721109954560.0 * x**10+21454162329600.0 * x**8-42908324659200.0 * x**6+40226554368000.0 * x**4-13408851456000.0 * x*x+670442572800.0

        return (2 * x * Math.hermite(n - 1, x) - 2 * (n - 1) * Math.hermite(n - 2, x))
