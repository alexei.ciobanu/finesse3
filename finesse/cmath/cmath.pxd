"""Fast C functions for common mathematical routines
used across all the various Cython extensions of Finesse."""

cimport cython
cimport numpy as np

cdef extern from "complex.h" nogil:
    double cabs(double complex z)
    double carg(double complex z)
    double complex cexp(double complex z)
    double complex conj(double complex z)
    double cimag(double complex z)
    double creal(double complex z)
    double complex csqrt(double complex z)

cdef extern from "math.h" nogil:
    double cos(double arg)
    double exp(double arg)
    double fabs(double arg)
    double fmax(double x, double y)
    double sin(double arg)
    double sqrt(double arg)

### Types

ctypedef np.complex128_t complex_t

#ctypedef fused real_numeric_t: # non-complex numeric type
#    short
#    int
#    long
#    float
#    double

cdef class ComplexMath:
    @staticmethod
    cdef double norm(double complex z) nogil

    @staticmethod
    cdef bint ceq(complex_t z1, complex_t z2) nogil

    @staticmethod
    cdef complex_t pow_complex(complex_t z, double n) nogil

    @staticmethod
    cdef int inverse(complex_t* res, complex_t z) except -1

    @staticmethod
    cdef complex_t inverse_unsafe(complex_t z) nogil

    @staticmethod
    cdef complex_t rotate(complex_t z, double ph) nogil

    @staticmethod
    cdef complex_t u_nm(int n, int m, complex_t qx, complex_t qy, double x, double y, double nr, double lambda0) nogil

    @staticmethod
    cdef complex_t u_single_plane(int n, complex_t q, double x, double nr) nogil

cdef class Gaussian:
    @staticmethod
    cdef double gouy(complex_t q) nogil

    @staticmethod
    cdef double w0_size(complex_t q, double nr, double lambda0) nogil

    @staticmethod
    cdef double w_size(complex_t q, double nr) nogil

    @staticmethod
    cdef double z_q(complex_t q) nogil

    @staticmethod
    cdef double z_R(complex_t q) nogil

cdef class Math:
    @staticmethod
    cdef double degrees(double x) nogil

    @staticmethod
    cdef double radians(double x) nogil

    @staticmethod
    cdef double factorial(int n) nogil

    @staticmethod
    cdef double msign(int n) nogil

    @staticmethod
    cdef int nmin(int n, int m) nogil

    @staticmethod
    cdef bint float_eq(double x, double y) nogil

    @staticmethod
    cdef double hermite(int n, double x) nogil

