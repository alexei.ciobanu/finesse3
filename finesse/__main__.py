"""Finesse command line interface"""

import logging
import datetime
import numpy as np # see note below; this shouldn't be needed
import click

from . import __version__, PROGRAM, DESCRIPTION, set_log_verbosity
from .parse import parse

LOGGER = logging.getLogger(__name__)


class KatState:
    """Shared state for all CLI subcommands."""
    VERBOSITY_QUIET = 0 # still shows errors
    VERBOSITY_WARN = 1 # show warnings
    VERBOSITY_INFO = 2 # show info
    VERBOSITY_DEBUG = 3 # show debug

    VERBOSITY_DEFAULT = VERBOSITY_INFO

    def __init__(self):
        self._verbosity = None

        # Default values.
        self.verbosity = self.VERBOSITY_DEFAULT

    @property
    def verbosity(self):
        """Log verbosity on stdout"""
        return self._verbosity

    @verbosity.setter
    def verbosity(self, verbosity):
        verbosity = int(verbosity)

        if verbosity < self.VERBOSITY_QUIET:
            verbosity = self.VERBOSITY_QUIET

        self._verbosity = verbosity
        self.update_log_verbosity()

        # Write some debug info now that we've set up the logger.
        LOGGER.debug("init %s %s", PROGRAM, __version__)

    @property
    def verbosity_log_level(self):
        """Verbosity level for logger."""
        level = logging.ERROR - 10 * self.verbosity

        if level < 0:
            level = 0

        return level

    @property
    def verbose(self):
        """Verbose output enabled

        Returns True if the verbosity is enough for INFO or DEBUG messages to be displayed.
        """
        return self.verbosity >= self.VERBOSITY_INFO

    def update_log_verbosity(self):
        """Update logger verbosity level from this object's verbosity settings"""
        set_log_verbosity(self.verbosity_log_level)

    def print_banner(self, kat_file=None):
        """Print Finesse banner."""
        if not self.verbose:
            return

        if kat_file:
            input_file = kat_file.name

        timenow = datetime.datetime.now().strftime("%d %B %Y, %H:%M:%S")
        click.echo(
"""------------------------------------------------------------------------
                     FINESSE {}
        o_.-=.       Frequency domain INterferomEter Simulation SoftwarE
        (\\'\".\\|                         http://www.gwoptics.org/finesse/
        .>' (_--.
    _=/d   ,^\\       Input file {input_file}
    ~~ \\)-'   '
    / |
    '  '                                         {time}
------------------------------------------------------------------------""".format(
            __version__, time=timenow, input_file=input_file))

def set_verbosity(ctx, option, value):
    """Callback to set stdout verbosity for root CLI command."""
    state = ctx.ensure_object(KatState)

    if option.name == "verbose" and value:
        state.verbosity = state.VERBOSITY_DEBUG
    elif option.name == "quiet" and value:
        state.verbosity = state.VERBOSITY_QUIET

@click.command(help=DESCRIPTION)
@click.argument("file", type=click.File())
@click.option("--start", type=float,
              help="Simulation start value. If specified, this overrides the start value "
              "specified in the parsed file.")
@click.option("--stop", type=float,
              help="Simulation stop value. If specified, this overrides the stop value "
              "specified in the parsed file.")
@click.option("--steps", type=int,
              help="Number of steps to simulate between --start and --stop. If specified, "
              "this overrides the number of steps specified in the parsed file.")
@click.option("--legacy", is_flag=True, default=False,
              help="Parse kat files using the legacy Finesse 2 syntax."
              "this overrides the number of steps specified in the parsed file.")
@click.option("--ignore-block", "ignored_blocks", multiple=True,
              help="Ignore the specified block. Can be specified multiple times.")
@click.option("--plot/--no-plot", default=True, show_default=True,
              help="Display results as figure.")
@click.option("--save-figure", type=click.File("wb", lazy=False), multiple=True,
              help="Save image of figure to file. Can be specified multiple times.")
@click.option("--kat2-compare", is_flag=True, default=False,
              help="Simulate using both this tool and Finesse 2, and overlay the results.")
@click.option("--kat2-path", type=click.Path(exists=True), envvar='KATPATH',
              help="Path to Finesse 2 directory for --kat2-compare. If not specified, the "
              "environment variable KATPATH is searched.")
@click.option("-v", "--verbose", is_flag=True, default=False, callback=set_verbosity,
              expose_value=False, is_eager=True, help="Increase verbose output.")
@click.option("-q", "--quiet", is_flag=True, default=False, callback=set_verbosity,
              expose_value=False,
              help="Silence output.")
@click.version_option(version=__version__, prog_name=PROGRAM)
@click.pass_context
def cli(ctx, file, start, stop, steps, legacy, ignored_blocks, plot, save_figure, kat2_compare,
        kat2_path):
    """Base CLI command group"""
    # Get state object to set verbosity, etc.
    state = ctx.ensure_object(KatState)

    # Print Finesse banner, if verbosity allows.
    state.print_banner(file)

    if kat2_compare:
        raise NotImplementedError("This feature is not yet available.")

    try:
        # Parse kat file.
        kat = parse(file.name, legacy)
    except Exception as error:
        click.echo(f"There was an error parsing '{file.name}': {error}",
                   err=True)
        return

    try:
        # Parse kat file.
        out = kat.run()
    except Exception as error:
        click.echo(f"There was an error running '{file.name}': {error}",
                   err=True)
        return

    print(out)
    out.plot()
