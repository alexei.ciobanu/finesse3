
import finesse
import numpy as np
import networkx as nx

from .abstract_operators import NullOperator, IdentityOperator
from .func_funs import (
    recursive_list_map,
    recursive_reduce,
    list_roll,
    list_flatten,
    ordered_intersect,
    recursive_list_comprehension,
    recursive_map
)


def get_node_obj(G, node_str):
    node_obj = G.nodes[node_str]['weakref']()
    return node_obj


def get_optical_network(model):
    optical_network = model.network.copy()
    for node_str in model.network.nodes:
        node_obj = get_node_obj(optical_network, node_str)
        # 0 being the enum for optical node
        if node_obj.type.value == 0:
            # keep optical node
            pass
        else:
            # remove non optical node
            # the edges should be updated accordingly by networkx
            optical_network.remove_node(node_str)
    return optical_network


def get_edge_data(G, u, v, data=True):
    if data is True:
        # return all dict members
        return G.edges[u, v]
    else:
        # return the "data" member
        return G.edges[u, v][data]


def get_edge_operator(G, u, v, op_type='LCT'):
    if (u is None or v is None):
        # special case
        # Implicitly make assumption that unconnected nodes
        # in a graph can couple via the Null operator
        return NullOperator(node1=u, node2=v)
    elif u == v:
        # Another special case (very dangeorus)
        # We implicitly make an assumption that all
        # nodes couple to themselves via the identity op,
        # which may not be true for all graphs.
        #
        # This was introduced as a hack for constructing
        # operator linear equations.
        return IdentityOperator(node1=u, node2=v)
    else:
        edge_owner_weakref = get_edge_data(G, u, v, 'owner')
        return edge_owner_weakref().get_operator(u, v, op_type=op_type)


def get_path_operators(G, path, op_type='LCT'):
    if path == []:
        return []
    elif type(path[0]) is list:
        return [get_path_operators(G, path[0], op_type=op_type)] + get_path_operators(G, path[1:], op_type=op_type)
    else:
        t = []
        N = len(path)
        for i in range(1, N):
            u = path[i-1]
            v = path[i]
            t.append(get_edge_operator(G, u, v, op_type=op_type))
        return t


def reduce_operator(op_list):
    acc = op_list[0]
    for op in op_list[1:]:
        acc = op@acc
    return acc


def get_reduced_path_operators(G, path, op_type='LCT', debug=False):
    reduced_operators = recursive_list_map(
        reduce_operator, get_path_operators(G, path, op_type=op_type))
    return reduced_operators


def get_reduced_summed_operators(G, path, op_type='LCT', debug=False):
    reduced_operators = get_reduced_path_operators(
        G,
        path,
        op_type=op_type,
        debug=debug)
    summed_reduced_operators = recursive_list_map(
        lambda l: recursive_reduce(lambda x, y: x+y, l, init=0),
        reduced_operators)
    return summed_reduced_operators

def are_nodes_unique(path):
    seen = []
    for node in path:
        if node not in seen:
            seen.append(node)
        else:
            return False
    return True


def only_unique_nodes(path):
    seen = []
    for node in path:
        if node not in seen:
            seen.append(node)
    return seen

def get_connectivity_matrix(m_couple):
    '''Mostly for debugging. The elements of the connectivity elements
    indicate the amount of terms for that coupling.'''
    N = len(m_couple)
    m_conn = [[None for x in range(N)] for y in range(N)]
    m_head = [[None for x in range(N)] for y in range(N)]
    for i, col in enumerate(m_couple):
        for j, el in enumerate(col):
            if el == []:
                m_conn[i][j] = 0
            elif type(el[0]) is list:
                m_conn[i][j] = len(el)
                m_head[i][j] = el[0][0]
            else:
                m_conn[i][j] = 1
                m_head[i][j] = el[0]
    return np.array(m_conn), m_head


####################################################################################


def align_path(path, align_nodes=[]):
    if not isinstance(path, np.ndarray):
        path = np.array(path)
    if not isinstance(align_nodes, np.ndarray):
        align_nodes = np.array(align_nodes)
    matches = np.isin(path, align_nodes)
    incidence_count = np.sum(matches)
    incidence_loc = np.argwhere(matches)
    if incidence_count > 1:
        raise Exception(
            f'Multiple align nodes from {align_nodes} in path {path}.\
Cannot align path to multiple nodes.')
    elif incidence_count == 0:
        return path.tolist()
    elif incidence_count == 1:
        return np.roll(path, -incidence_loc[0]).tolist()
    else:
        raise Exception(f'Undefined alignment')


def align_paths(paths, align_nodes=[]):
    return recursive_list_map(lambda x: align_path(x, align_nodes=align_nodes), paths)

def cycle2path2(cycle):
    path = cycle + [cycle[0]]
    return path

def cycles2paths(cycles):
    paths = recursive_list_map(cycle2path2, cycles)
    return paths

def make_n_couple(g_cycles, graph):
    solved_nodes = list(g_cycles.keys())
    N = len(solved_nodes)
    n_couple = np.zeros(shape=(N,N), dtype=object)
    for j, j_node in enumerate(solved_nodes):
        for i, i_node in enumerate(solved_nodes):
            if i==j:
                n_couple[i,i] = g_cycles[i_node]
            else:
                n_couple[i,j] = get_all_valid_paths(graph, [j_node], [i_node])[0][0]
    return n_couple


####################################################################################


def get_n_couple_diag(n_couple):
    diag = []
    for i in range(len(n_couple)):
        diag.append(n_couple[i][i][0][0])
    return diag


###############################################################################


def prune_invalid_path(path, solved_nodes, del_invalid=False):
    if any(node in path[1:-1] for node in solved_nodes):
        if del_invalid:
            return None
        else:
            return [path[0], None, path[-1]]
    else:
        return path


def prune_invalid_paths(paths, solved_nodes, del_invalid=False):
    out_paths = recursive_list_map(
        lambda path: prune_invalid_path(
            path, solved_nodes, del_invalid),
        paths)

    if del_invalid:
        out_paths = recursive_list_comprehension(
            lambda x: x, out_paths, lambda x: x is not None)

    return out_paths


def get_all_valid_paths(G, sources, targets, del_invalid=False):
    '''
    Returns an (N, M) matrix where N is the number of target nodes M is the 
    number of source nodes. The elements of the matrix are lists of paths 
    that sum to that matrix element.

    The intention is to do a recursive_list_map on this matrix to get
    the path operator matrix, which is then acted on the source
    field vector to get the target field vector which can be built.

    * first index: row of path matrix
    * second index: column of path matrix
    * third index: list of paths that sum to get that matrix element
    '''
    out_list = []
    for target_node in targets:
        temp = []
        for source_node in sources:
            if source_node == target_node:
                pruned_paths = [[source_node, target_node]]
            else:
                if target_node in sources:
                    paths = [[source_node, None, target_node]]
                else:
                    paths = list(nx.all_simple_paths(G, source_node, target_node))
                    if paths == []:
                        paths = [[source_node, None, target_node]]
                pruned_paths = prune_invalid_paths(paths, sources, del_invalid)
            temp.append(pruned_paths)
        out_list.append(temp)

    # return prune_invalid_paths(out_list, sources, del_invalid)
    return out_list

###################################################################################


def vec2field(vec, op_context, out_nodes=None):
    N = op_context['N']
    field_type = op_context['type']
    vec_len = len(vec)
    if vec_len % N != 0:
        raise Exception(
            f"field size {N} doesn't evenly divide vector length {vec_len}")
    num_fields = vec_len//N
    out_fields = []
    if out_nodes is not None:
        if len(out_nodes) != num_fields:
            raise Exception(
                f"number of nodes {len(out_nodes)} has to match the number of fields {num_fields}")
    for i in range(num_fields):
        if out_nodes is not None:
            out_node = out_nodes[i]
        else:
            out_node = None
        vec_slice = vec[i*N:(i+1)*N]
        vec_slice = np.ravel(vec_slice)
        field = field_type(vec=vec_slice, node1=None, node2=out_node)
        out_fields.append(field)

    return out_fields

########################################################################################


def get_solved_nodes(all_cycles=None, graph=None, model=None):
    if all([x is not None for x in [all_cycles, graph, model]]):
        raise Exception("Specify only one of all_cycles, model, or graph.")
    if model is not None:
        graph = model.network
        all_cycles = nx.simple_cycles(graph)
    elif graph is not None:
        all_cycles = nx.simple_cycles(graph)
    elif all_cycles is not None:
        pass
    else:
        raise Exception("undefined")

    return only_unique_nodes([cycle[0] for cycle in all_cycles])
