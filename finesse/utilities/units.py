"""Convenience objects and functions for unit manipulation."""

from enum import Enum

class SI(Enum):
    """Enum defining common SI unit constant names. Look-up
    the corresponding values with the `finesse.utilities.SI_MAP`
    dictionary.

    Examples
    --------
    To get the constant for `SI.MILLI` simply look-up:

    >>> SI_MAP[SI.MILLI]
    1e-3
    """
    YOCTO = 0
    ZEPTO = 1
    ATTO = 2
    FEMTO = 3
    PICO = 4
    NANO = 5
    MICRO = 6
    MILLI = 7
    CENTI = 8
    DECI = 9
    NONE = 10
    KILO = 11
    MEGA = 12
    GIGA = 13
    TERA = 14
    PETA = 15

SI_VALUE = {
    SI.YOCTO : 1E-24,
    SI.ZEPTO : 1E-21,
    SI.ATTO : 1E-18,
    SI.FEMTO : 1E-15,
    SI.PICO : 1E-12,
    SI.NANO : 1E-9,
    SI.MICRO : 1E-6,
    SI.MILLI : 1E-3,
    SI.CENTI : 1E-2,
    SI.DECI : 1E-1,
    SI.NONE : 1.0,
    SI.KILO : 1E3,
    SI.MEGA : 1E6,
    SI.GIGA : 1E9,
    SI.TERA : 1E12,
    SI.PETA : 1E15
}

SI_LABEL = {
    SI.YOCTO : 'y',
    SI.ZEPTO : 'z',
    SI.ATTO : 'a',
    SI.FEMTO : 'f',
    SI.PICO : 'p',
    SI.NANO : 'n',
    SI.MICRO : 'u',
    SI.MILLI : 'm',
    SI.CENTI : 'c',
    SI.DECI : 'd',
    SI.KILO : 'k',
    SI.MEGA : 'M',
    SI.GIGA : 'G',
    SI.TERA : 'T',
    SI.PETA : 'P'
}
