"""Common mathematical routines used by several sub-modules of the Finesse package.

.. note::
    **For developers:**

    The functions in this extension are to be used by the Python sub-modules of
    Finesse. When writing additional Cython extensions, use the functions found
    in `finesse.cmath` instead for the best performance.

    This is the principle behind the separation of ``finesse.utilities.maths`` and
    ``finesse.cmath``. The latter only exposes `cdef` functions and therefore cannot
    be used from within the pure Python parts of Finesse.

    See :ref:`cython_guide` for further details.
"""

import cython
from finesse.cmath.cmath cimport complex_t
from finesse.cmath.cmath cimport ComplexMath

@cython.boundscheck(False) # disable bounds checking
@cython.wraparound(False) # disable negative indexing
cdef complex_t apply_ABCD_c(double[:, ::1] Mabcd, complex_t q1, double nr1, double nr2):
    return nr2 * (Mabcd[0][0] * q1 / nr1 + Mabcd[0][1]) / (Mabcd[1][0] * q1 / nr1 + Mabcd[1][1])

def apply_ABCD(double[:, ::1] Mabcd, complex_t q1, double nr1, double nr2):
    r"""
    apply_ABCD(Mabcd, q1, nr1, nr2)

    Applies the ABCD matrix `Mabcd` to the beam parameter `q1`.

    Uses a source medium index of refraction `nr1` and
    a target medium index of refraction `nr2` when computing
    the new beam parameter.

    The value of the new beam parameter returned is given by,

    .. math::
        q = n_{r,2} \frac{A \frac{q_1}{n_{r,1}} + B}{ C \frac{q1}{n_{r_1}} + D},

    where :math:`A`, :math:`B`, :math:`C`, :math:`D` are the
    corresponding elements of the ABCD matrix `Mabcd`.

    Parameters
    ----------
    Mabcd : :class:`numpy.ndarray`
        The ABCD matrix with which to transform `q1`.

    q1 : np.complex128
        The input beam parameter.

    nr1 : float
        Index of refraction of source medium.

    nr2 : float
        Index of refraction of target medium.

    Returns
    -------
    out : np.complex128
        A new beam parameter equal to the transformation
        of `q1` with `Mabcd`.
    """
    cdef complex_t q2 = apply_ABCD_c(Mabcd, q1, nr1, nr2)
    return q2

def zrotate(complex_t z, double ph):
    """
    zrotate(z, ph)

    Rotation of the phase of a complex number by an amount in radians.

    Parameters
    ----------
    z : np.complex128
        A complex number, this parameter is not modified in this method.

    ph : float
        The amount, in radians, to rotate the phase of `z` by.

    Returns
    -------
    out : np.complex128
        A new complex number defined as the rotation of the phase of `z`
        by `ph` radians.
    """
    cdef complex_t z2 = ComplexMath.rotate(z, ph)
    return z2

def complex_equal(complex_t z1, complex_t z2):
    """
    complex_equal(z1, z2)

    Determines whether the two complex numbers, `z1` and `z2`, are equal.

    Parameters
    ----------
    z1 : np.complex128
        A complex number.

    z2 : np.complex128
        A complex number.

    Returns
    -------
    out : bool
        True if z1 == z2 (to within `1e-13`), otherwise False.
    """
    return ComplexMath.ceq(z1, z2)
