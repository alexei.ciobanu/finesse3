"""
Useful common utility functions and classes used throughout the
Finesse package.

Listed below are all the sub-modules of the ``utilities`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: utilities/

    finesse.utilities.components
    finesse.utilities.maths
    finesse.utilities.misc
    finesse.utilities.units
"""

# TODO: sort out which things get imported at the module level here
from finesse.utilities.components import refractive_indices
from finesse.utilities.units import SI, SI_LABEL, SI_VALUE
from finesse.utilities.misc import check_name, pairwise, valid_name

#if __doc__:
#    from finesse.utilities.misc import _collect_submodules
#
#    __doc__ = __doc__.format(contents=_collect_submodules("utilities"))
