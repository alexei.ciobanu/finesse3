"""
Space-type objects representing physical distances between components.
"""

from dataclasses import dataclass
import enum
import logging
import numpy as np

from finesse import constants
from finesse.element import model_parameter, Rebuild
from finesse.utilities.maths import apply_ABCD as apply
from finesse.utilities.maths import complex_equal

from finesse.components.general import Connector, Surface, InteractionType
from finesse.components.node import NodeDirection, NodeType

from finesse.utilities.operators.optical_operators import AbstractOpticalOperator

LOGGER = logging.getLogger(__name__)


@model_parameter('L', 0.0, Rebuild.HOM, validate="_check_L", units='m')
@model_parameter('nr', 1.0, Rebuild.HOM, "_check_nr")
class Space(Connector):
    """
    A class representing a space between two components in the
    interferometer configuration, with a given length and index
    of refraction.
    """

    def __init__(self, name=None, portA=None, portB=None, L=0, nr=1):
        """
        Constructs a new instance of `Space` with the specified properties.

        Parameters
        ----------
        name : str, optional
            Name of newly created :class:`.Space` instance.

        portA : :class:`.Port`, optional
            Port to connect

        portB : :class:`.Port`, optional
            Port to connect

        L : float, optional
            Length of newly created :class:`.Space` instance, default is 0.0.

        nr : float, optional
            Index of refraction of newly created :class:`.Space` instance,
            default is unity.
        """
        if (portA is None) != (portB is None):
            LOGGER.warn("Can't construct a space with only one port "
                        "connected, ignoring ports.")
            portA = None
            portB = None

        if portA is not None and portA.type != NodeType.OPTICAL:
            raise Exception("PortA argument is not an optical port")
        if portB is not None and portB.type != NodeType.OPTICAL:
            raise Exception("PortB argument is not an optical port")

        if name is None:
            if portA is not None and portB is not None:
                compA = portA.component.name
                compB = portB.component.name
                name = f"{compA}_{portA.name}__{compB}_{portB.name}"
            else:
                raise ValueError("Cannot create an unconnected space without "
                                 "providing a name")

        super().__init__(name)

        self._add_to_model_namespace = True
        self._namespace = '.spaces'

        self.nr = nr
        self.L = L
        self.__gouy_x = 0.0
        self.__gouy_y = 0.0

        self._add_port('p1', NodeType.OPTICAL)
        self._add_port('p2', NodeType.OPTICAL)

        # Modulation input
        self._add_port('phase_sig', NodeType.ELECTRICAL)
        self.phase_sig._add_node("i", NodeDirection.INPUT)
        self.phase_sig._add_node("o", NodeDirection.OUTPUT)

        if portA is not None and portB is not None:
            self.connect(portA, portB)

        self.valid_edges = [
            {'node1': self.p1.i.full_name, 'node2': self.p2.o.full_name},
            {'node1': self.p2.i.full_name, 'node2': self.p1.o.full_name},
        ]

    @property
    def gouy_x(self):
        return np.degrees(self.__gouy_x)

    @property
    def gouy_y(self):
        return np.degrees(self.__gouy_y)

    def connect(self, portA, portB):
        """
        Sets the ports of this `Space`.

        Parameters
        ----------
        portA : :class:`.Port`, optional
            Port to connect

        portB : :class:`.Port`, optional
            Port to connect
        """
        # From the Space's perspective the input and output
        # nodes are swapped around for its ports
        self.p1._add_node('i', None, portA.o)
        self.p1._add_node('o', None, portA.i)
        self.p2._add_node('i', None, portB.o)
        self.p2._add_node('o', None, portB.i)

        self._register_node_coupling(
            self.p1.i,
            self.p2.o,
            interaction_type=InteractionType.TRANSMISSION,
            forced_name=f'{self.name}.p1.i->{self.name}.p2.o')
        self._register_node_coupling(
            self.p2.i,
            self.p1.o,
            interaction_type=InteractionType.TRANSMISSION,
            forced_name=f'{self.name}.p2.i->{self.name}.p1.o')

        base_conn_str = f'{self.name}.{self.phase_sig.name}.i->{self.name}'
        self._register_node_coupling(self.phase_sig.i,
                                     self.p1.o,
                                     forced_name=f'{base_conn_str}.p1.o')
        self._register_node_coupling(self.phase_sig.i,
                                     self.p2.o,
                                     forced_name=f'{base_conn_str}.p2.o')

    def _check_L(self, value):
        self._recompute_abcd = self._recompute_abcd.fromkeys(self._recompute_abcd, True)

        if self.has_model:
            self._model._retrace_required = True

        return value

    def _check_nr(self, value):
        self._recompute_abcd = self._recompute_abcd.fromkeys(self._recompute_abcd, True)

        if self.has_model:
            self._model._retrace_required = True
            if isinstance(self.p1.component, Surface):
                self.p1.component._recompute_abcd = self.p1.component._recompute_abcd.fromkeys(
                    self.p1.component._recompute_abcd, True
                )
            if isinstance(self.p2.component, Surface):
                self.p2.component._recompute_abcd = self.p2.component._recompute_abcd.fromkeys(
                    self.p2.component._recompute_abcd, True
                )

        return value

    def get_operator(self, from_node, to_node, op_type=AbstractOpticalOperator):
        edge_op_dict = {
            tuple(self.valid_edges[0].values()): op_type(
                scalar=1,
                Mx=self.ABCD(
                    *self.valid_edges[0].values(), direction='x', symbolic=False),
                My=self.ABCD(
                    *self.valid_edges[0].values(), direction='y', symbolic=False),
                **self.valid_edges[0]),
            tuple(self.valid_edges[1].values()): op_type(
                scalar=1,
                Mx=self.ABCD(
                    *self.valid_edges[1].values(), direction='x', symbolic=False),
                My=self.ABCD(
                    *self.valid_edges[1].values(), direction='y', symbolic=False),
                **self.valid_edges[1]),
        }

        return_val = edge_op_dict.get((from_node, to_node), None)
        if return_val is None:
            raise Exception(
                f"edge {(from_node,to_node)} not found in object {self}")
        else:
            return return_val

    def ABCD(self, from_node, to_node, direction='x', symbolic=False):
        r"""Computes and returns the ABCD matrix of this space
        component. This is given by,

        .. math::
            M = \begin{pmatrix}
                    1 & \frac{L}{n_r} \\
                    0 & 1
                \end{pmatrix},

        where :math:`L` is the length of the space and :math:`n_r` is
        the index of refraction.

        Parameters
        ----------
        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        direction : str, optional
            Direction of ABCD matrix computation, default is 'x'. Note
            the ABCD matrix returned is dimension-independent for a
            space, thus this method will return the same result for both
            'x' and 'y'.

        Returns
        -------
        out : :class:`numpy.ndarray`
            The ABCD matrix for this space object.

        Raises
        ------
        err : :class:`.NodeException`
            If either `from_node` or `to_node` are not in ``self.nodes``
            or if `from_node` and `to_node` are both inputs or both
            outputs.
        """
        if symbolic:
            raise NotImplementedError()
        if not symbolic:
            recompute = self._recompute_abcd.get((from_node, to_node, direction), True)
            if not recompute:
                stored_M = self._abcd_matrices.get((from_node, to_node, direction), None)
                if stored_M is not None:
                    return stored_M

        if type(from_node) is str:
            from_node = self.nodes[from_node]
        if type(to_node) is str:
            to_node = self.nodes[to_node]

        self._ABCD_prechecks(from_node, to_node)
        values, _ = self._eval_parameters()

        L = values["L"] #self.L.eval()
        nr = values["nr"] #self.nr.eval()

        underlying = [
            [1.0, L / nr],
            [0.0, 1.0]
        ]

        M = np.array(underlying)

        if not symbolic:
            self._store_ABCD(M, from_node, to_node, direction)
        return M

    def _on_build(self, sim):
        self.__Is = np.ones((sim.nhoms, ))
        self._n = self._model.homs[:, 0]
        self._m = self._model.homs[:, 1]

    def _set_gouy_phase(self, sim, force=False):
        if not force and not self._model._trace_changed:
            return

        # get beam parameters
        node_q_map = sim.model.last_trace
        qx_p1o, qy_p1o, _ = node_q_map[self.p1.i]
        qx_p2i, qy_p2i, _ = node_q_map[self.p2.o]

        # make sure there are no mismatches across the space
        nr = self.nr.eval()
        qx_p1o_propagated = apply(
            self.ABCD(self.p1.i, self.p2.o, 'x'), qx_p1o, nr, nr)
        qy_p1o_propagated = apply(
            self.ABCD(self.p1.i, self.p2.o, 'y'), qy_p1o, nr, nr)

        if not complex_equal(qx_p2i.q, qx_p1o_propagated):
            raise RuntimeError(f"Mismatch in space: {self.name} from p1.o->p2.i: "
                               f"qx_p2.i = {qx_p2i}, qx_p1.o propagated = {qx_p1o_propagated}")
        if not complex_equal(qy_p2i.q, qy_p1o_propagated):
            raise RuntimeError(f"Mismatch in space: {self.name} from p1.o->p2.i: "
                               f"qy_p2.i = {qy_p2i}, qy_p1.o propagated = {qy_p1o_propagated}")

        self.__gouy_x = abs(qx_p2i.gouy() - qx_p1o.gouy())
        self.__gouy_y = abs(qy_p2i.gouy() - qy_p1o.gouy())

    def _fill_matrix(self, sim):
        values, is_changing = self._eval_parameters()

        pre_factor = 2 * np.pi * values['nr'] * values['L'] / constants.C_LIGHT

        for freq in sim.frequencies:
            phi = pre_factor * freq.f
            if sim.model.phase_config.ZERO_TEM00_GOUY:
                a = np.exp(
                    1j * (
                        -phi
                        + self._n * self.__gouy_x
                        + self._m * self.__gouy_y
                    )
                )
            else:
                a = np.exp(
                    1j * (
                        -phi
                        + (self._n + 0.5) * self.__gouy_x
                        + (self._m + 0.5) * self.__gouy_y
                    )
                )
            with sim.component_edge_fill(self, 'p1.i->p2.o', freq, freq) as mat:
                mat[:] = a
            with sim.component_edge_fill(self, 'p2.i->p1.o', freq, freq) as mat:
                mat[:] = a

        if sim.is_audio:
            di = np.diag_indices(sim.nhoms)
            if di == (np.zeros(1), np.zeros(1)): di = 0
            for freq in sim.frequencies:
                # conjugate lower sideband of signal matrix
                if freq.audio_order == -1:
                    with sim.component_edge_fill(self, 'p1.i->p2.o', freq, freq) as mat:
                        mat[:][di] = mat[:][di].conjugate()
                    with sim.component_edge_fill(self, 'p2.i->p1.o', freq, freq) as mat:
                        mat[:][di] = mat[:][di].conjugate()

            if self.phase_sig.enabled:
                self._fill_space_phase_signal(sim)

    def _fill_space_phase_signal(self, sim):
        f0 = constants.C_LIGHT / sim.model.lambda0
        kc = 2 * np.pi / sim.model.lambda0  # Carrier wavenumber

        #c_p1_i = carrier.out[carrier.field(self.p1.i)]
        #c_p2_i = carrier.out[carrier.field(self.p2.i)]
        c_p1_i = sim.DC.get_DC_out(self.p1.i)
        c_p2_i = sim.DC.get_DC_out(self.p2.i)

        for freq in sim.frequencies:
            ks = kc * (1 + freq.f / f0)

            if freq.audio_order < 0:
                # Need to conjugate the carrier as we carry the conjugate of
                # the lower sideband everywhere.
                c_p1_i = c_p1_i.conj()
                c_p2_i = c_p2_i.conj()

            factor = 0.25 * (kc / ks) * np.sin(ks * self.nr * self.L.eval())
            factor *= np.exp(1j * (-np.pi / 2 + (kc + ks)
                                   * self.nr * self.L.eval()))

            with sim.component_edge_fill(self, "phase_sig.i->p1.o", None, freq) as mat:
                np.multiply(c_p2_i * factor,
                            self.__Is,
                            out=mat[:])
            with sim.component_edge_fill(self, "phase_sig.i->p2.o", None, freq) as mat:
                np.multiply(c_p1_i * factor,
                            self.__Is,
                            out=mat[:])
