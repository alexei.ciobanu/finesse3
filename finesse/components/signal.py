"""
Signal-type electrical component for producing signal inputs.
"""

import numpy as np

from finesse.components.general import Connector
from finesse.element import model_parameter, Rebuild


@model_parameter("amplitude", 1, Rebuild.NA, units="V")
@model_parameter("phase", 0, Rebuild.NA, units="degrees")
class SignalGenerator(Connector):
    """
    An object which produces a signal with a given amplitude and phase.
    """
    def __init__(self, name, node, amplitude=1, phase=0):
        """
        Constructs a new `SignalGenerator` element which is used to inject
        signals into a model.

        Parameters
        ----------
        name : str
            Name of the SignalGenerator instance.

        node : .class:`finesse.components.node.Node`
            A node to inject a signal into

        amplitude : float, optional
            Amplitude of the signal, units depends on the type of the `node`
            or `type` parameter

        phase : float, optional
            Phase-offset of the signal from the default in degrees,
            defaults to zero.

        """
        Connector.__init__(self, name)

        self._add_port('port', node.type)
        self.port._add_node('o', None, node)

        self.amplitude = amplitude
        self.phase = phase

    @property
    def f(self):
        return self._model.fsig.f

    def _fill_rhs(self, sim):
        if not sim.is_audio:
            return

        values, is_changing = self._eval_parameters()
        amplitude = values["amplitude"]
        phase = values["phase"]

        A = amplitude * np.exp(1j * phase * np.pi / 180)
        sim.M().set_rhs(sim.field(self.port.o), A)
