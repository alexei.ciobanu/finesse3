"""
Laser-type optical components for producing beams.
"""

import numpy as np

from finesse.utilities.maths import zrotate
from finesse.element import model_parameter, Rebuild

from finesse.components.general import Connector, FrequencyGenerator
from finesse.components.node import NodeType, NodeDirection

@model_parameter('P', 1, Rebuild.PlaneWave, units='W')
@model_parameter('phase', 0, Rebuild.PlaneWave, units="degrees")
@model_parameter('f', 0, Rebuild.Frequencies & Rebuild.PlaneWave, units="Hz")
class Laser(Connector, FrequencyGenerator):
    """
    An object which produces a beam with associated
    properties such as power and frequency.
    """
    def __init__(self, name, P=1, f=0, phase=0):
        """
        Constructs a new `Laser` instance with the specified properties.

        Parameters
        ----------
        name : str
            Name of the laser instance.

        P : float, optional
            Power of the laser (in Watts), defaults to 1 W.

        f : float or :class:`.SourceFrequency` or :class:`.Frequency`, optional
            Frequency-offset of the laser from the default (in Hz) or
            :class:`.SourceFrequency` or :class:`.Frequency` object.
            Defaults to 0 Hz offset.

        phase : float, optional
            Phase-offset of the laser from the default, defaults to zero.
        """
        Connector.__init__(self, name)
        FrequencyGenerator.__init__(self)

        self._add_port('p1', NodeType.OPTICAL)
        self.p1._add_node("i", NodeDirection.INPUT)
        self.p1._add_node("o", NodeDirection.OUTPUT)

        # Modulation inputs
        self._add_port('amp_sig', NodeType.ELECTRICAL)
        self.amp_sig._add_node("i", NodeDirection.INPUT)
        self.amp_sig._add_node("o", NodeDirection.OUTPUT)
        self._add_port('phase_sig', NodeType.ELECTRICAL)
        self.phase_sig._add_node("i", NodeDirection.INPUT)
        self.phase_sig._add_node("o", NodeDirection.OUTPUT)
        self._add_port('freq_sig', NodeType.ELECTRICAL)
        self.freq_sig._add_node("i", NodeDirection.INPUT)
        self.freq_sig._add_node("o", NodeDirection.OUTPUT)

        self._register_node_coupling(self.amp_sig.i, self.p1.o)
        self._register_node_coupling(self.phase_sig.i, self.p1.o)
        self._register_node_coupling(self.freq_sig.i, self.p1.o)

        self.f = f

        self.P = P
        self.phase = phase
        self.__power_coeffs = {(0, 0) : 1.0 + 0j}
        self.__workspace = {}

    def get_amplitude(self):
        return (np.sqrt(2*self.P.ref/self._model._EPSILON0_C)
               * np.exp(1j*np.radians(self.phase.ref)))

    def _source_frequencies(self):
        return [self.f.ref]

    def get_field(self,field_type):
        return self.get_amplitude()*field_type(node=self.p1.o.full_name)

    @property
    def power_coeffs(self):
        return self.__power_coeffs

    def tem(self, n, m, factor, phase=0.0):
        if not self._model.is_modal:
            raise ValueError(f"model is not modal, cannot set TEMs for laser: {self}")
        if n + m > self._model.maxtem:
            raise ValueError("n + m larger than maximum order for TEM.")
        self.__power_coeffs[(n, m)] = zrotate(complex(np.sqrt(factor), 0), np.radians(phase))

    def _update_tem_gouy_phases(self, sim, force=False):
        if not force and not self._model._trace_changed:
            return

        qx, qy, _ = sim.model.last_trace[self.p1.o]

        if sim.model.phase_config.ZERO_TEM00_GOUY:
            phase00 = 0.5 * qx.gouy() + 0.5 * qy.gouy()
        else:
            phase00 = 0.0

        for (n, m), pc in self.__power_coeffs.items():
            phase = (n + 0.5) * qx.gouy() + (m + 0.5) * qy.gouy()
            pc = zrotate(pc, phase - phase00)

        # normalise input fields to overall power
        s = np.sqrt(np.sum(np.array(list(self.__power_coeffs.values()))))
        for pc in self.__power_coeffs.values():
            pc /= s

    def _on_unbuild(self, sim):
        if sim in self.__workspace:
            del self.__workspace[sim]

    def _on_build(self, sim):
        self.__workspace[sim] = {}
        self.__I = np.eye(sim.nhoms, dtype=np.complex128)
        self.__workspace[sim]['fsrc'] = None

        # Let's get the correct frequency references from the simulation for filling later
        if self.f.is_tunable:
            # if it's tunable we want to look for the symbol that is just this
            # lasers frequency, as it will be changing
            for f in sim.frequencies:
                if f.symbol == self.f.ref:
                    self.__workspace[sim]['fsrc'] = f
                    break
        else:
            # otherwise do some value comparisons
            for f in sim.frequencies:
                if float(f.f) == float(self.f):
                    self.__workspace[sim]['fsrc'] = f
                    break

        # Didn't find a Frequency bin for this laser in carrier simulation
        if not sim.is_audio: assert(self.__workspace[sim]['fsrc'] is not None)

        #if sim.is_modal: self._update_tem_gouy_phases(sim)

    def _fill_matrix(self, sim):
        if not sim.is_audio:
            return

        # TODO: HOMs can have different input powers, so we'll need to account
        # for that.
        for freq in sim.frequencies:
            carrier = sim.DC.frequencies[freq.audio_carrier_index]

            if carrier.f != self.f:
                continue

            for i, (n, m) in enumerate(sim.homs):
                car = carrier.out[carrier.field(self.p1.o, self.f.index, i)]

                # As we carry the conjugate of the lower sideband everywhere, we
                # have to use the conjugate of the carrier
                if (freq.order < 0):
                    car = car.conj()

                # factor in power coefficient map corresponding to hom
                # defaulting to 0+0j if not in power_coeffs
                pc = self.__power_coeffs.get((n, m), 0 + 0j)

                # Common prefactor on modulations
                # EPSILON0_C converts from volts to watts (TODO: check this)
                # 0.5 comes from a common factor of m/2
                factor = sim.model._EPSILON0_C * 0.5

                # For explanations of the below equations, see in the manual
                # "Input fields or the 'right hand side' vector".
                amp_sig = 0.5
                phase_sig = np.exp(1j * freq.order * np.pi / 2)
                freq_sig = np.exp(1j * (np.pi / 2 - freq.order * np.pi / 2))
                freq_sig /= 4 * np.pi**2 * sim.model.fsig.f

                with sim.component_edge_fill(self, "amp_sig.i->p1.o", None, freq) as mat:
                    np.multiply(car * pc * factor * amp_sig,
                                self.__I,
                                out=mat[:])

                with sim.component_edge_fill(self, "phase_sig.i->p1.o", None, freq) as mat:
                    np.multiply(car * factor * phase_sig,
                                self.__I,
                                out=mat[:])

                with sim.component_edge_fill(self, "freq_sig.i->p1.o", None, freq) as mat:
                    np.multiply(car * factor * freq_sig,
                                self.__I,
                                out=mat[:])

    def _fill_rhs(self, sim):
        if sim.is_audio:
            return  # no RHS signal injections heres

        fsrc = self.__workspace[sim]['fsrc']

        # Carrier laser injection
        Ein = (np.sqrt(2*self.P/self._model._EPSILON0_C)
               * np.exp(1j*np.deg2rad(self.phase.value)))

        for i, (n, m) in enumerate(sim.homs):
            # TODO: (sjr) I think this is adding power beyond Laser.P value,
            #       need to fix this with normalisations
            pc = self.__power_coeffs.get((n, m), 0 + 0j)
            #sim.M().set_rhs(sim.field(self.p1.o, fsrc.index, i), Ein*pc)
            sim.set_source(
                field_idx = (self.p1.o, fsrc.index, i),
                vector = Ein*pc,
            )

    def _fill_qnoise_rhs(self, sim):
        for freq in sim.frequencies:
            for hom in range(sim.nhoms):
                idx = sim.field(self.p1.o, freq.index, hom)
                # TODO: this should be max(UNIT_VACUUM, self.noise) once/if
                # laser noise attribute is implemented
                sim.Mq[idx] = sim.model._UNIT_VACUUM / 2 * (1 + freq.carrier.f / sim.model.f0)

    def _couples_frequency(self, sim, connection, frequency_in, frequency_out):
        return (frequency_in == frequency_out) or (frequency_in is None)
