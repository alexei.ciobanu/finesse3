"""
Objects defining optical-cavities existing as "node-wrappers"
around components inside a cavity.
"""

from copy import copy
import cmath
import math

import numpy as np

from finesse import constants
from finesse.element import ModelElement
from finesse.gaussian import BeamParam
from finesse.utilities.maths import apply_ABCD

from finesse.components.space import Space
from finesse.components.beamsplitter import Beamsplitter
from finesse.components.mirror import Mirror

class Cavity(ModelElement):
    """
    A class representing a cavity-type object in an interferometer
    configuration.

    This class stores the shortest path between the start node
    and end node of the cavity and performs operations on it such
    as setting all the node :class:`.BeamParam` instances along the
    path.
    """
    def __init__(self, name, source_node, target_node, via_node=None):
        """
        Constructs a new `Cavity` instance around the specified
        nodes.

        Parameters
        ----------
        name : str
            Name of newly created `Cavity` instance.

        source_node : :class:`.OpticalNode`
            Node that the cavity path starts from.

        target_node : :class:`.OpticalNode`
            Node that the cavity path ends at, must be associated with
            the same component as `source_node`.

        via_node : :class:`.OpticalNode`, optional
            Node that the cavity path must traverse via, defaults to
            `None` so that no specified via node must be traversed.
        """
        super().__init__(name)
        if source_node.component != target_node.component:
            raise Exception("Source and target nodes must "
                            "belong to the same component "
                            "in a Cavity instance.")
        self.__source = source_node
        self.__target = target_node
        self.__via = via_node
        self.__source_comp = source_node.component
        self.__path = None

    @property
    def path(self):
        """Retrieves an ordered container of the cavity path.

        The return type is an :class:`.OpticalPath` instance which
        stores an underlying list of the path data.

        :getter: Returns the path of the cavity (read-only).

        See Also
        --------
        finesse.model.Model.path : Retrieves an ordered container of the path trace between two
                     specified nodes.
        """
        return self.__path

    @property
    def source(self):
        """Starting node of the cavity.

        :getter: Returns the cavity starting node (read-only).
        """
        return self.__source

    @property
    def target(self):
        """Ending node of the cavity.

        :getter: Returns the cavity ending node (read-only).
        """
        return self.__target

    @property
    def ABCD(self):
        """The round-trip ABCD matrix/matrices of the cavity.

        If the round-trip matrices are the same for both the
        tangential and sagittal planes then this returns the
        singular round-trip ABCD matrix, otherwise it returns
        a tuple of the (tangential, sagittal) round-trip matrices.

        :getter: Returns a copy of the cavity round-trip matrix/matrices (read-only).
        """
        if np.all(np.equal(self.__rt_ABCD_x, self.__rt_ABCD_y)):
            return copy(self.__rt_ABCD_x)
        return (copy(self.__rt_ABCD_x), copy(self.__rt_ABCD_y))

    @property
    def ABCDx(self):
        """The tangential round-trip ABCD matrix of the cavity.

        :getter: Returns a copy of the cavity round-trip matrix in the
                 tangential plane (read-only).
        """
        return copy(self.__rt_ABCD_x)

    @property
    def ABCDy(self):
        """The sagittal round-trip ABCD matrix of the cavity.

        :getter: Returns a copy of the cavity round-trip matrix in the
                 sagittal plane (read-only).
        """
        return copy(self.__rt_ABCD_y)

    @property
    def round_trip_length(self):
        """The round-trip length of the cavity [in metres].

        :getter: Returns the length of a single round-trip of the cavity (read-only).
        """
        return self.__length

    @property
    def round_trip_gouy(self):
        r"""The accumulated round-trip Gouy phase of the cavity.

        This is given by,

        .. math::
            \psi_{\mathrm{rt}} = 2\,\arccos{\left( \mathrm{sgn}(B) \sqrt{g} \right)},

        where :math:`B` is the corresponding element of the round-trip
        ABCD matrix and :math:`g` is the cavity stability parameter
        returned by :attr:`.Cavity.stability`. If the stabilities in
        the tangential and sagittal planes are different, then this property
        will return a tuple of both planes' round-trip Gouy phases.

        :getter: Returns the accumulated round-trip Gouy phase (read-only).
        """
        if self.__rt_gouy_x == self.__rt_gouy_y:
            return self.__rt_gouy_x
        return (self.__rt_gouy_x, self.__rt_gouy_y)

    @property
    def power(self):
        """The steady-state cavity power as a fraction
        of the incoming power.

        :getter: Returns the fractional steady-state cavity power (read-only).
        """
        return self.__power

    @property
    def loss(self):
        """The round-trip loss of the cavity as a
        fraction of the incoming power.

        :getter: Returns the fractional round-trip cavity loss (read-only).
        """
        return self.__loss

    @property
    def finesse(self):
        r"""The finesse of the cavity.

        This quantity is defined as,

        .. math::
            \mathcal{F} = \frac{\pi}{2\arcsin{\left(
                \frac{1 - \widetilde{l}}{2\sqrt{\widetilde{l}}}
            \right)
            }},

        where :math:`\widetilde{l} = \sqrt{1 - l}` and :math:`l`
        is the cavity loss.

        :getter: Returns the cavity finesse (read-only).
        """
        return self.__finesse

    @property
    def FSR(self):
        r"""The free-spectral-range (FSR) of the cavity.

        This quantity is defined as,

        .. math::
            \mathrm{FSR} = \frac{c}{L},

        where :math:`c` is the speed of light and :math:`L` is the
        length of the cavity.

        :getter: Returns the cavity free-spectral-range (read-only).
        """
        return self.__FSR

    @property
    def FWHM(self):
        r"""The cavity full-width-half-maximum (FWHM).

        This quantity is defined as,

        .. math::
            \mathrm{FWHM} = \frac{\mathrm{FSR}}{\mathcal{F}},

        where :math:`\mathcal{F}` is the cavity finesse.

        :getter: Returns the FWHM of the cavity (read-only).

        See Also
        --------
        Cavity.FSR : Free-spectral-range of a cavity.
        Cavity.finesse : Finesse of a cavity.
        """
        return self.__FWHM

    @property
    def pole(self):
        r"""The pole-frequency of the cavity.

        This quantity is defined as,

        .. math::
            f_{\mathrm{pole}} = \frac{\mathrm{FWHM}}{2},

        where :math:`\mathrm{FWHM}` is the full-width-half-maximum
        of the cavity.

        :getter: Returns the cavity pole-frequency (read-only).

        See Also
        --------
        Cavity.FWHM : Full-width at half-maximum (FWHM) of a cavity.
        """
        return self.__pole

    @property
    def mode_separation(self):
        r"""The mode separation frequency of the cavity.

        This is defined by,

        .. math::
            \delta f =
                \begin{cases}
                    \frac{\psi_{\mathrm{rt}}}{2\pi} \Delta f, & \text{if } \psi_{\mathrm{rt}} \leq \pi\\
                    (1 - \frac{\psi_{\mathrm{rt}}}{2\pi}) \Delta f, & \text{if } \psi_{\mathrm{rt}} > \pi,
                \end{cases}

        where :math:`\psi_{\mathrm{rt}}` is the accumulated round-trip Gouy phase
        and :math:`\Delta f` is the FSR of the cavity.

        :getter: Returns the mode separation frequency (read-only).
        """
        if self.__mode_separation_x == self.__mode_separation_y:
            return self.__mode_separation_x
        return (self.__mode_separation_x, self.__mode_separation_y)

    @property
    def eigenmode(self):
        """The eigenmode(s) of the cavity.

        If the eigenmodes for both the tangential and sagittal planes
        are equal then this will return the singular eigenmode value,
        otherwise it will return a tuple of the (tangential, sagittal)
        eigenmodes.

        .. note::
            The return value of this property is a complex number
            which can be directly passed to a :class:`.BeamParam`
            constructor to instantiate an object of this type.

        :getter: Returns the cavity eigenmode(s) (read-only).
        """
        if self.__eigenmode_x == self.__eigenmode_y:
            return self.eigenmode_x
        return (
            self.eigenmode_x,
            self.eigenmode_y
        )

    @property
    def eigenmode_x(self):
        """The eigenmode of the cavity for the tangential plane.

        .. note::
            The return value of this property is a complex number
            which can be directly passed to a :class:`.BeamParam`
            constructor to instantiate an object of this type.

        :getter: Returns the cavity's tangential plane eigenmode (read-only).
        """
        if self._model._tracer.symbolic:
            return self.__eigenmode_x

        return None if self.__eigenmode_x is None else BeamParam(q=self.__eigenmode_x)

    @property
    def eigenmode_y(self):
        """The eigenmode of the cavity for the sagittal plane.

        .. note::
            The return value of this property is a complex number
            which can be directly passed to a :class:`.BeamParam`
            constructor to instantiate an object of this type.

        :getter: Returns the cavity's sagittal plane eigenmode (read-only).
        """
        if self._model._tracer.symbolic:
            return self.__eigenmode_y

        return None if self.__eigenmode_y is None else BeamParam(q=self.__eigenmode_y)

    @property
    def resolution(self):
        r"""The resolution of the cavity.

        Cavity resolution, :math:`S`, is defined by,

        .. math::
            S =
                \begin{cases}
                    \frac{\psi_{\mathrm{rt}}}{2\pi} \mathcal{F}, & \text{if } \psi_{\mathrm{rt}} \leq \pi\\
                    (1 - \frac{\psi_{\mathrm{rt}}}{2\pi}) \mathcal{F}, & \text{if } \psi_{\mathrm{rt}} > \pi,
                \end{cases}

        where :math:`\psi_{\mathrm{rt}}` is the round-trip Gouy phase and :math:`\mathcal{F}` is the
        cavity finesse.

        :getter: Returns the cavity's resolution (read-only).
        """
        if self.__resolution_x == self.__resolution_y:
            return self.__resolution_x
        return (self.__resolution_x, self.__resolution_y)

    @property
    def stability(self):
        r"""The stability of the cavity given by
        the :math:`g`-factor:

        .. math::
            g = \frac{A + D + 2}{4},

        where :math:`A` and :math:`D` are the relevant entries of the
        cavity round-trip ABCD matrix. The cavity is stable if the
        following condition is satisfied:

        .. math::
            0 \leq g \leq 1.

        If the :math:`g` factors for both the tangential and sagittal
        planes are equal then this property gives the singular value,
        otherwise it returns a tuple of the tangential, sagittal plane
        stabilities.

        :getter: Returns the cavity stability/stabilities (read-only).
        """
        if self.__stability_x == self.__stability_y:
            return self.__stability_x
        return (self.__stability_x, self.__stability_y)

    @property
    def stability_x(self):
        r"""The stability of the cavity in the tagential plane given by
        the :math:`g`-factor:

        .. math::
            g_x = \frac{A_x + D_x + 2}{4},

        where :math:`A_x` and :math:`D_x` are the relevant entries of the
        cavity round-trip ABCD matrix in the tangential plane. The cavity
        is stable in this plane if the following condition is satisfied:

        .. math::
            0 \leq g_x \leq 1.

        :getter: Returns the cavity stability in the tangential plane (read-only).
        """
        return self.__stability_x

    @property
    def stability_y(self):
        r"""The stability of the cavity in the sagittal plane given by
        the :math:`g`-factor:

        .. math::
            g_y = \frac{A_y + D_y + 2}{4},

        where :math:`A_y` and :math:`D_y` are the relevant entries of the
        cavity round-trip ABCD matrix in the sagittal plane. The cavity
        is stable in this plane if the following condition is satisfied:

        .. math::
            0 \leq g_y \leq 1.

        :getter: Returns the cavity stability in the sagittal plane (read-only).
        """
        return self.__stability_y

    @property
    def is_stable(self):
        """Flag indicating whether the cavity is stable or not.

        :getter: Returns `True` if `0.0 <= self.stability <= 1.0`,
                 `False` otherwise (for both tangential, sagittal planes).
        """
        return not (self.__stability_x < 0.0 or self.__stability_x > 1.0
                    or self.__stability_y < 0.0 or self.__stability_y > 1.0)

    @property
    def is_critical(self):
        return (self.__stability_x == 0.0 or self.__stability_y == 0.0 or
            math.isclose(self.__stability_x, 1.0) or math.isclose(self.__stability_y, 1.0))

    def _update_round_trip_length(self):
        rt_L = 0.0
        for from_node, to_comp in self.__path.data:
            if isinstance(to_comp, Space):
                rt_L += to_comp.L
        self.__length = rt_L

    def _update_round_trip_gouy(self):
        _B_x = self.ABCDx[0, 1]
        _B_y = self.ABCDy[0, 1]
        self.__rt_gouy_x = 2.0 * np.arccos(np.sign(_B_x) * np.sqrt(self.__stability_x))
        self.__rt_gouy_y = 2.0 * np.arccos(np.sign(_B_y) * np.sqrt(self.__stability_y))

    def _update_FSR(self):
        self.__FSR = constants.C_LIGHT / self.__length

    def _update_power(self):
        from finesse.tracing import TraceType

        power = self._model._tracer.run(TraceType.CAVPOWER, cavity_path=self.__path.data)
        self.__power = power
        self.__loss = 1.0 - power

    def _update_finesse(self):
        _loss = np.sqrt(1.0 - self.__loss)
        self.__finesse = 0.5*np.pi / np.arcsin(0.5*(1.0 - _loss) / np.sqrt(_loss))

    def _update_FWHM(self):
        self.__FWHM = self.__FSR/self.__finesse

    def _update_pole(self):
        self.__pole = 0.5*self.__FWHM

    def _update_mode_separation(self):
        if self.__rt_gouy_x > np.pi:
            self.__mode_separation_x = (1.0 - 0.5*self.__rt_gouy_x/np.pi)*self.__FSR
        else:
            self.__mode_separation_x = self.__FSR*0.5*self.__rt_gouy_x/np.pi
        if self.__rt_gouy_y > np.pi:
            self.__mode_separation_y = (1.0 - 0.5*self.__rt_gouy_y/np.pi)*self.__FSR
        else:
            self.__mode_separation_y = self.__FSR*0.5*self.__rt_gouy_y/np.pi

    def _update_resolution(self):
        if self.__rt_gouy_x > np.pi:
            self.__resolution_x = (1.0 - 0.5*self.__rt_gouy_x/np.pi)*self.__finesse
        else:
            self.__resolution_x = self.__finesse*0.5*self.__rt_gouy_x/np.pi
        if self.__rt_gouy_y > np.pi:
            self.__resolution_y = (1.0 - 0.5*self.__rt_gouy_y/np.pi)*self.__finesse
        else:
            self.__resolution_y = self.__finesse*0.5*self.__rt_gouy_y/np.pi

    def _update_ABCD(self):
        from finesse.tracing import TraceType

        self.__rt_ABCD_x, self.__rt_ABCD_y = self._model._tracer.run(
            TraceType.RT_ABCD, cavity_path=self.__path.data
        )

    def _update_eigenmode(self):
        matrices = [self.ABCDx, self.ABCDy]

        for i, rt in enumerate(matrices):
            _C = rt[1, 0]
            _D_minus_A = rt[1, 1] - rt[0, 0]
            _minus_B = -rt[0, 1]

            _sqrt_term = cmath.sqrt(_D_minus_A * _D_minus_A - 4 * _C * _minus_B)
            upper = 0.5*(_D_minus_A + _sqrt_term)/_C
            lower = 0.5*(_D_minus_A - _sqrt_term)/_C

            if not i:
                self.__eigenmode_x = None
                if np.imag(upper) > 0.0: self.__eigenmode_x = upper
                elif np.imag(lower) > 0.0: self.__eigenmode_x = lower
            else:
                self.__eigenmode_y = None
                if np.imag(upper) > 0.0: self.__eigenmode_y = upper
                elif np.imag(lower) > 0.0: self.__eigenmode_y = lower

    def _update_stability(self):
        matrices = [self.ABCDx, self.ABCDy]

        for i, rt in enumerate(matrices):
            _A = rt[0, 0]
            _D = rt[1, 1]
            _m = 0.5*(_A + _D)

            if not i:
                self.__stability_x = 0.5*(1 + _m)
            else:
                self.__stability_y = 0.5*(1 + _m)

    def update(self):
        """Performs a full update of this cavity instance, recomputing
        the node path and all parameters."""
        if self.__path is None:
            self.__path = self._model.path(self.source, self.target, self.__via)
        # update all the cavity parameters - order matters here as
        # some parameters depend on others having been computed first
        self._update_ABCD()
        self._update_eigenmode()
        self._update_stability()
        self._update_round_trip_length()
        self._update_round_trip_gouy()
        self._update_power()
        self._update_finesse()
        self._update_FSR()
        self._update_FWHM()
        self._update_pole()
        self._update_mode_separation()
        self._update_resolution()
