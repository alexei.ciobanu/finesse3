#cython: boundscheck=False
#cython: wraparound=False
#cython: initializedcheck=False

cimport numpy as np
import numpy as np

from finesse.cmath.cmath cimport conj, cexp
from finesse.cmath.cmath cimport exp, sqrt, cos
from finesse.cmath cimport complex_t # type: np.complex128_t (i.e. double complex)
from finesse.cmath cimport Math # namespace of common mathematical routines
from finesse.cmath cimport ComplexMath # namespace of complex mathematical routines
from finesse.cmath cimport Gaussian # namespace of Gaussian beam routines

cdef extern from "../constants.h":
    long double PI
    double C_LIGHT

def mirror_fill(mirror, sim, dict values, double lambda0):
    cdef:
        double f0 = C_LIGHT / lambda0
        double k = 2 * PI / lambda0

        complex_t _it = 1.0j * sqrt(values['T'])
        double _r = sqrt(values['R'])

        double phi = Math.radians(values['phi'])
        double phi_scaled
        complex_t _tuning
        complex_t _ctuning

        double fval # current value of the frequency object

        complex_t fac = 1.0j * k * _r # scaling factor for longitudinal motion

        # NOTE (sjr) see TODO in simulations/base.py line 427

        #object[::1] freqs_view = sim.frequencies
        #Py_ssize_t Nfreqs = freqs_view.shape[0]
        #Py_ssize_t freq_idx

    for freq in sim.frequencies:
    #for freq_idx in range(Nfreqs):
        #freq = freqs_view[freq_idx]
        fval = <double>freq.f
        phi_scaled = phi * (1 + fval / f0)

        _tuning = cexp(2.0j * phi_scaled)
        _ctuning = conj(_tuning)

        # TODO (sjr) these context manager calls are expensive (see matrixfill.html), need
        #            an alternative if we find that this function needs more optimisation
        with sim.component_edge_fill(mirror, 'p1.i->p1.o', freq, freq) as mat:
            np.multiply(_r * _tuning, mirror.K11, out=mat[:])

        with sim.component_edge_fill(mirror, 'p2.i->p2.o', freq, freq) as mat:
            np.multiply(_r * _ctuning, mirror.K22, out=mat[:])

        with sim.component_edge_fill(mirror, 'p1.i->p2.o', freq, freq) as mat:
            np.multiply(_it, mirror.K12, out=mat[:])

        with sim.component_edge_fill(mirror, 'p2.i->p1.o', freq, freq) as mat:
            np.multiply(_it, mirror.K21, out=mat[:])

        if sim.is_audio:
            carrier = sim.DC

            # -------------------------------------------------
            # Mechanical to optical connections
            # -------------------------------------------------
            # - Longitudinal
            # -------------------------------------------------
            # Need to modulate the reflected carrier field
            _i = carrier.field(mirror.p1.i, freq.audio_carrier_index, 0)
            c_p1_i = np.atleast_2d(carrier.out[slice(_i, _i+sim.nhoms, 1)]).T

            _i = carrier.field(mirror.p2.i, freq.audio_carrier_index, 0)
            c_p2_i = np.atleast_2d(carrier.out[slice(_i, _i+sim.nhoms, 1)]).T

            # As we've already computed the scattering matrix on
            # reflection we should use that
            with sim.component_edge_fill(
                mirror, 'mech.z->p1.o', None, freq
            ) as mat:
                mat_refl = sim.component_edge_get(
                    mirror, 'p1.i->p1.o', freq, freq
                )[:]
                np.dot(mat_refl, fac * c_p1_i, out=mat[:])

            # extra 180 phase here as we're doing the opposite
            # modulation when looked at from the other side of the mirror
            with sim.component_edge_fill(
                mirror, 'mech.z->p2.o', None, freq
            ) as mat:
                mat_refl = sim.component_edge_get(
                    mirror, 'p2.i->p2.o', freq, freq
                )[:]
                np.dot(mat_refl, fac * c_p2_i, out=mat[:])


def beamsplitter_fill(beamsplitter, sim, dict values, double lambda0):
    cdef:
        double f0 = C_LIGHT / lambda0

        complex_t _it = 1.0j * sqrt(values['T'])
        double _r = sqrt(values['R'])

        double phi = Math.radians(values['phi'])
        double phi_scaled

        complex_t _tuning
        complex_t _ctuning
        double _cos_alpha = cos(Math.radians(values['alpha']))

        double fval

        bint do_conj

    for freq in sim.frequencies:
        fval = <double>freq.f
        phi_scaled = phi * (1 + fval / f0)

        _tuning = cexp(2.0j * phi_scaled * _cos_alpha)
        _ctuning = conj(_tuning)

        # Need to conjugate lower sideband in audio calculations
        do_conj = False
        if sim.is_audio and freq.audio_order == -1:
            do_conj = True

        with sim.component_edge_fill(
            beamsplitter, "p1.i->p2.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _r * _tuning,
                beamsplitter.K12,
                out=mat[:],
            )
            if do_conj:
                mat[:].imag *= -1
        with sim.component_edge_fill(
            beamsplitter, "p2.i->p1.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _r * _tuning,
                beamsplitter.K21,
                out=mat[:],
            )
            if do_conj:
                mat[:].imag *= -1
        with sim.component_edge_fill(
            beamsplitter, "p3.i->p4.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _r * _ctuning,
                beamsplitter.K34,
                out=mat[:],
            )
            if do_conj:
                mat[:].imag *= -1
        with sim.component_edge_fill(
            beamsplitter, "p4.i->p3.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _r * _ctuning,
                beamsplitter.K43,
                out=mat[:],
            )
            if do_conj:
                mat[:].imag *= -1
        with sim.component_edge_fill(
            beamsplitter, "p1.i->p3.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _it,
                beamsplitter.K13,
                out=mat[:],
            )
            if do_conj:
                mat[:].imag *= -1
        with sim.component_edge_fill(
            beamsplitter, "p2.i->p4.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _it,
                beamsplitter.K24,
                out=mat[:],
            )
        with sim.component_edge_fill(
            beamsplitter, "p3.i->p1.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _it,
                beamsplitter.K31,
                out=mat[:],
            )
        with sim.component_edge_fill(
            beamsplitter, "p4.i->p2.o", freq, freq, conjugate=do_conj
        ) as mat:
            np.multiply(
                _it,
                beamsplitter.K42,
                out=mat[:],
            )
