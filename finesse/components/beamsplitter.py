"""
Optical components representing physical beamsplitters.
"""

from dataclasses import dataclass
import enum
import logging
import math

import numpy as np

from finesse import constants
from finesse.exceptions import TotalReflectionError
from finesse.element import model_parameter, Rebuild
from finesse.knm import run_bayer_helms, zero_tem00_phase, rev_all_gouy, flip_odd_horizontal, knm_loss
from finesse.knm import log_knm_matrix
from finesse.utilities import refractive_indices
from finesse.utilities.maths import apply_ABCD as apply

from finesse.components.general import Surface, InteractionType, SurfaceType
from finesse.components.matrixfill import beamsplitter_fill
from finesse.components.node import NodeDirection, NodeType

from finesse.utilities.operators.optical_operators import AbstractOpticalOperator

LOGGER = logging.getLogger(__name__)


class KnmBSNodeDirection(enum.IntEnum):
    BS12 = 1
    BS21 = 2
    BS34 = 3
    BS43 = 4
    BS13 = 5
    BS31 = 6
    BS24 = 7
    BS42 = 8


@dataclass
class BeamsplitterKnmMatrices:
    K12: np.ndarray
    K21: np.ndarray
    K34: np.ndarray
    K43: np.ndarray
    K13: np.ndarray
    K31: np.ndarray
    K24: np.ndarray
    K42: np.ndarray

    K12_loss: np.ndarray
    K21_loss: np.ndarray
    K34_loss: np.ndarray
    K43_loss: np.ndarray
    K13_loss: np.ndarray
    K31_loss: np.ndarray
    K24_loss: np.ndarray
    K42_loss: np.ndarray

    K12_bh: np.ndarray
    K21_bh: np.ndarray
    K34_bh: np.ndarray
    K43_bh: np.ndarray
    K13_bh: np.ndarray
    K31_bh: np.ndarray
    K24_bh: np.ndarray
    K42_bh: np.ndarray

    K12_map: np.ndarray
    K21_map: np.ndarray
    K34_map: np.ndarray
    K43_map: np.ndarray
    K13_map: np.ndarray
    K31_map: np.ndarray
    K24_map: np.ndarray
    K42_map: np.ndarray


# FIXME (sjr) these have to be repeated currently as the descriptors
#             are not inherited from Surface
@model_parameter('R', 0.99, Rebuild.PlaneWave, "_check_R")
@model_parameter('T', 0.01, Rebuild.PlaneWave, "_check_T")
@model_parameter('L', 0.0, Rebuild.PlaneWave, "_check_L")
@model_parameter('phi', 0, Rebuild.PlaneWave, validate="_check_phi", units="degrees")
@model_parameter("alpha", 0.0, Rebuild.HOM, validate="_check_alpha", units="degrees")
@model_parameter('Rcx', np.inf, Rebuild.HOM, units='m')
@model_parameter('Rcy', np.inf, Rebuild.HOM, units='m')
@model_parameter('xbeta', 0.0, Rebuild.HOM, validate="_check_xbeta", units="radians")
@model_parameter('ybeta', 0.0, Rebuild.HOM, validate="_check_ybeta", units="radians")
class Beamsplitter(Surface):
    """
    A class representing a beamsplitter optical component with
    associated properties such as reflectivity, tuning and
    angle of incidence.
    """

    def __init__(self,
                 name,
                 R=None, T=None, L=None,
                 phi=0,
                 alpha=0,
                 Rc=np.inf,
                 xbeta=0, ybeta=0
                 ):
        """
        Constructs a new `Beamsplitter` instance with
        the specified properties.

        Parameters
        ----------
        name : str
            Name of newly created `Beamsplitter` instance.

        R : float, optional
            Reflectivity of the beam splitter.

        T : float, optional
            Transmissivity of the beam splitter.

        L : float, optional
            Loss of the beam splitter.

        phi : float, optional
            Microscopic tuning of the beam splitter [in degrees].

        alpha : float, optional
            Angle of incidence [in degrees].

        Rc : float, optional
            Radius of curvature [in metres], defaults to ``numpy.inf`` to
            indicate a planar surface.
        """
        # NOTE this is needed (for now) as it is used in _check_phi which is currently called
        #      in Surface constructor when initialising phi value
        self._cos_alpha = 1

        super().__init__(name, R, T, L, phi, Rc, xbeta, ybeta, SurfaceType.BEAMSPLITTER)
        self.alpha = alpha

        self._add_port("p1", NodeType.OPTICAL)
        self.p1._add_node('i', NodeDirection.INPUT)
        self.p1._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p2", NodeType.OPTICAL)
        self.p2._add_node('i', NodeDirection.INPUT)
        self.p2._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p3", NodeType.OPTICAL)
        self.p3._add_node('i', NodeDirection.INPUT)
        self.p3._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p4", NodeType.OPTICAL)
        self.p4._add_node('i', NodeDirection.INPUT)
        self.p4._add_node('o', NodeDirection.OUTPUT)

        # optic to optic couplings => reflections
        self._register_node_coupling(self.p1.i, self.p2.o,
                                     interaction_type=InteractionType.REFLECTION)
        self._register_node_coupling(self.p2.i, self.p1.o,
                                     interaction_type=InteractionType.REFLECTION)
        self._register_node_coupling(self.p3.i, self.p4.o,
                                     interaction_type=InteractionType.REFLECTION)
        self._register_node_coupling(self.p4.i, self.p3.o,
                                     interaction_type=InteractionType.REFLECTION)
        # optic to optic couplings => transmissions
        self._register_node_coupling(self.p1.i, self.p3.o,
                                     interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p2.i, self.p4.o,
                                     interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p3.i, self.p1.o,
                                     interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p4.i, self.p2.o,
                                     interaction_type=InteractionType.TRANSMISSION)

        # mirror motion couplings
        self._add_port("mech", NodeType.MECHANICAL)
        self.mech._add_node('z')

        # optic to motion couplings
        self._register_node_coupling(self.p1.i, self.mech.z)
        self._register_node_coupling(self.p2.i, self.mech.z)
        self._register_node_coupling(self.p3.i, self.mech.z)
        self._register_node_coupling(self.p4.i, self.mech.z)
        self._register_node_coupling(self.p1.o, self.mech.z)
        self._register_node_coupling(self.p2.o, self.mech.z)
        self._register_node_coupling(self.p3.o, self.mech.z)
        self._register_node_coupling(self.p4.o, self.mech.z)
        # motion to optic couplings: phase coupling on reflection
        self._register_node_coupling(self.mech.z, self.p1.o)
        self._register_node_coupling(self.mech.z, self.p2.o)
        self._register_node_coupling(self.mech.z, self.p3.o)
        self._register_node_coupling(self.mech.z, self.p4.o)
        

        self._knm_logging_info = {
            # set(["K12", "K21", "K34", "K43", "K24", "K42", "K13", "K31"]),
            "matrices": [],
            "couplings": None
        }

        self._r_sym = np.sqrt(self.R.ref)
        self._it_sym = 1j*np.sqrt(self.T.ref)
        self._phi_rad_sym = np.radians(self.phi.ref)
        self._tuning_sym = np.exp(-2j*self._phi_rad_sym)
        self._ctuning_sym = np.conj(self._tuning_sym)

        self.valid_edges = [
            {'node1': self.p1.i.full_name, 'node2': self.p2.o.full_name},
            {'node1': self.p1.i.full_name, 'node2': self.p3.o.full_name},
            {'node1': self.p2.i.full_name, 'node2': self.p1.o.full_name},
            {'node1': self.p2.i.full_name, 'node2': self.p4.o.full_name},
            {'node1': self.p3.i.full_name, 'node2': self.p4.o.full_name},
            {'node1': self.p3.i.full_name, 'node2': self.p1.o.full_name},
            {'node1': self.p4.i.full_name, 'node2': self.p3.o.full_name},
            {'node1': self.p4.i.full_name, 'node2': self.p2.o.full_name}
        ]

    def get_operator(self, from_node, to_node, op_type=AbstractOpticalOperator):
        edge_op_dict = {
            tuple(self.valid_edges[0].values()) : op_type(
                scalar=self._r_sym*self._tuning_sym,
                Mx=self.ABCD(*self.valid_edges[0].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[0].values(), direction='y', symbolic=False),
                **self.valid_edges[0]),
            tuple(self.valid_edges[1].values()) : op_type(
                scalar=self._it_sym,
                Mx=self.ABCD(*self.valid_edges[1].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[1].values(), direction='y', symbolic=False),
                **self.valid_edges[1]),
            tuple(self.valid_edges[2].values()) : op_type(
                scalar=self._r_sym*self._tuning_sym,
                Mx=self.ABCD(*self.valid_edges[2].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[2].values(), direction='y', symbolic=False),
                **self.valid_edges[2]),
            tuple(self.valid_edges[3].values()) : op_type(
                scalar=self._it_sym,
                Mx=self.ABCD(*self.valid_edges[3].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[3].values(), direction='y', symbolic=False),
                **self.valid_edges[3]),
            tuple(self.valid_edges[4].values()) : op_type(
                scalar=self._r_sym*self._ctuning_sym,
                Mx=self.ABCD(*self.valid_edges[4].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[4].values(), direction='y', symbolic=False),
                **self.valid_edges[4]),
            tuple(self.valid_edges[5].values()) : op_type(
                scalar=self._it_sym,
                Mx=self.ABCD(*self.valid_edges[5].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[5].values(), direction='y', symbolic=False),
                **self.valid_edges[5]),
            tuple(self.valid_edges[6].values()) : op_type(
                scalar=self._r_sym*self._ctuning_sym,
                Mx=self.ABCD(*self.valid_edges[6].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[6].values(), direction='y', symbolic=False),
                **self.valid_edges[6]),
            tuple(self.valid_edges[7].values()) : op_type(
                scalar=self._it_sym,
                Mx=self.ABCD(*self.valid_edges[7].values(), direction='x', symbolic=False),
                My=self.ABCD(*self.valid_edges[7].values(), direction='y', symbolic=False),
                **self.valid_edges[7])
        }

        return_val = edge_op_dict.get((from_node, to_node), None)
        if return_val is None:
            raise Exception(
                f"edge {(from_node,to_node)} not found in object {self}")
        else:
            return return_val

    def _check_alpha(self, value):
        self._cos_alpha = np.cos(np.radians(value))
        self._recompute_abcd = self._recompute_abcd.fromkeys(self._recompute_abcd, True)
        self._angle_changed = True

        return value

    def ABCD(self, from_node, to_node, direction='x', symbolic=False):
        r"""Computes and returns the ABCD matrix of the beam splitter in
        the specified direction ('x' is the tangential plane and 'y' is
        the sagittal plane).

        The matrices for transmission and reflection are different for
        the sagittal and tangential planes (:math:`M_s` and :math:`M_t`),
        as shown below.

        Transmission through the beam splitter:
            For the tangential plane (`direction = 'x'`),

            .. math::
                M_t = \begin{pmatrix}
                          \frac{\cos{\alpha_2}}{\cos{\alpha_1}} & 0 \\
                          \frac{\Delta n}{R_c} & \frac{\cos{\alpha_1}}{\cos{\alpha_2}}
                      \end{pmatrix},

            and for the sagittal plane (`direction = 'y'`),

            .. math::
                M_s = \begin{pmatrix}
                          1 & 0 \\
                          \frac{\Delta n}{R_c} & 1
                      \end{pmatrix},

            where :math:`\alpha_1` is the angle of incidence of the beam splitter and
            :math:`\alpha_2` is given by Snell's law (:math:`n_1\sin{\alpha_1} =
            n_2\sin{\alpha_2}`). The quantity :math:`\Delta n` is given by,

            .. math::
                \Delta_n = \frac{n_2 \cos{\alpha_2} - n_1 \cos{\alpha_1}}{
                    \cos{\alpha_1} \cos{\alpha_2}
                 }.

            If the direction of propagation is reversed such that the radius of curvature
            of the beam splitter is in this direction, then the elements :math:`A` and
            :math:`D` of the tangential matrix (:math:`M_t`) are swapped.

        Reflection at a beam splitter:
            The reflection at the front surface of the beam splitter is given by,

            .. math::
                M_t = \begin{pmatrix}
                          1 & 0 \\
                          -\frac{2n_1}{R_c \cos{\alpha_1}} & 1
                      \end{pmatrix},

            for the tangential plane, and,

            .. math::
                M_s = \begin{pmatrix}
                          1 & 0 \\
                          -\frac{2n_1 \cos{\alpha_2}}{R_c} & 1
                      \end{pmatrix},

            for the sagittal plane.

            At the back surface :math:`R_c \rightarrow - R_c` and
            :math:`\alpha_1 \rightarrow - \alpha_2`.

        Parameters
        ----------
        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        direction : str, optional
            Direction of ABCD matrix computation, default is 'x' (tangential plane).

        Returns
        -------
        out : :class:`numpy.ndarray`
            The ABCD matrix for the beam splitter in the given `direction`.

        Raises
        ------
        err1 : :class:`.NodeException`
            If either `from_node` or `to_node` are not in ``self.nodes``
            or if `from_node` and `to_node` are both inputs or both outputs.

        err2 : :class:`.TotalReflectionError`
            If total reflection occurs at the beamsplitter - i.e. if :math:`\sin{\alpha_2} > 1.0`.
        """
        if symbolic:
            raise NotImplementedError()
        if not symbolic:
            recompute = self._recompute_abcd.get((from_node, to_node, direction), True)
            if not recompute:
                stored_M = self._abcd_matrices.get((from_node, to_node, direction), None)
                if stored_M is not None:
                    return stored_M

        if type(from_node) is str:
            from_node = self.nodes[from_node]
        if type(to_node) is str:
            to_node = self.nodes[to_node]

        self._ABCD_prechecks(from_node, to_node)
        values, _ = self._eval_parameters()

        nr1, nr2 = refractive_indices(from_node, to_node, symbols=symbolic)

        if direction == 'x':
            Rc = values["Rcx"]
        else:
            Rc = values["Rcy"]

        alpha = values["alpha"]
        alpha1 = math.radians(alpha)

        # we get alpha2 from Snell's law
        if from_node.port is self.p3 or from_node.port is self.p4:
            sin_alpha2 = (nr2 / nr1) * math.sin(alpha1)
        else:
            sin_alpha2 = (nr1 / nr2) * math.sin(alpha1)

        if abs(sin_alpha2) > 1.0:
            raise TotalReflectionError("Total reflection at beamsplitter: {}, when "
                                        "tracing from {} to {}".format(self, from_node, to_node),
                                        from_node, to_node)
        alpha2 = math.asin(sin_alpha2)

        if self.interaction_type(from_node, to_node) == InteractionType.REFLECTION:
            nr = nr1
            if (from_node.port is self.p3 or from_node.port is self.p4) and Rc > 0.0:
                Rc *= -1
                alpha = -alpha2
            else:
                alpha = alpha1
            if direction == 'x':  # tangential plane
                M = np.array(
                    [
                        [1.0, 0.0],
                        [-2.0 * nr / (Rc * math.cos(alpha)), 1.0]
                    ]
                )
            else:  # sagittal plane
                M = np.array(
                    [
                        [1.0, 0.0],
                        [-2.0 * nr * math.cos(alpha) / Rc, 1.0]
                    ]
                )
            if not symbolic:
                self._store_ABCD(M, from_node, to_node, direction)

            return M

        # transmission
        cos_alpha1 = math.cos(alpha1)
        cos_alpha2 = math.cos(alpha2)
        delta_n = (nr2 * cos_alpha2 - nr1 * cos_alpha1) / (cos_alpha1 * cos_alpha2)
        if direction == 'x': # tangential plane
            _A = cos_alpha2 / cos_alpha1
            _D = cos_alpha1 / cos_alpha2

            # propagation direction reversed
            if from_node.port is self.p3 or from_node.port is self.p4:
                _A = _D
                _D = _A

            M = np.array(
                [
                    [_A, 0.0],
                    [delta_n / Rc, _D]
                ]
            )
        else:  # sagittal plane
            M = np.array(
                [
                    [1.0, 0.0],
                    [delta_n / Rc, 1.0]
                ]
            )

        if not symbolic:
            self._store_ABCD(M, from_node, to_node, direction)

        return M

    def _on_build(self, sim):
        nhom = sim.nhoms

        self._allocate_knm_matrices(sim)
        # self._compute_knm_matrices(sim)

        # set references
        self.K12 = self.knm_matrices.K12
        self.K21 = self.knm_matrices.K21
        self.K34 = self.knm_matrices.K34
        self.K43 = self.knm_matrices.K43
        self.K13 = self.knm_matrices.K13
        self.K31 = self.knm_matrices.K31
        self.K24 = self.knm_matrices.K24
        self.K42 = self.knm_matrices.K42

        self.K12_loss = self.knm_matrices.K12_loss
        self.K21_loss = self.knm_matrices.K21_loss
        self.K34_loss = self.knm_matrices.K34_loss
        self.K43_loss = self.knm_matrices.K43_loss
        self.K13_loss = self.knm_matrices.K13_loss
        self.K31_loss = self.knm_matrices.K31_loss
        self.K24_loss = self.knm_matrices.K24_loss
        self.K42_loss = self.knm_matrices.K42_loss

    def _fill_matrix(self, sim):
        values, is_changing = self._eval_parameters()

        beamsplitter_fill(self, sim, values, sim.model.lambda0)

    def _fill_qnoise_rhs(self, sim):
        if sim.is_modal:
            for freq in sim.frequencies:
                for k in range(sim.nhoms):
                    L1 = self.R * self.K21_loss[k] + self.T * self.K31_loss[k]
                    L2 = self.R * self.K12_loss[k] + self.T * self.K42_loss[k]
                    L3 = self.R * self.K43_loss[k] + self.T * self.K13_loss[k]
                    L4 = self.R * self.K34_loss[k] + self.T * self.K24_loss[k]
                    # TODO, can't access matrix like this!
                    sim.Mq[sim.field(self.p1.o, freq.index, k)
                           ] = (L1 + self.L) / 2
                    sim.Mq[sim.field(self.p2.o, freq.index, k)
                           ] = (L2 + self.L) / 2
                    sim.Mq[sim.field(self.p3.o, freq.index, k)
                           ] = (L3 + self.L) / 2
                    sim.Mq[sim.field(self.p4.o, freq.index, k)
                           ] = (L4 + self.L) / 2
        else:
            for freq in sim.frequencies:
                # TODO, can't access matrix like this!
                sim.Mq[sim.field(self.p1.o, freq.index, 0)] = self.L / 2
                sim.Mq[sim.field(self.p2.o, freq.index, 0)] = self.L / 2

    def _allocate_knm_matrices(self, sim):
        I = np.eye(sim.nhoms, dtype=np.complex128)
        losses = np.ones(sim.nhoms)

        self.knm_matrices = BeamsplitterKnmMatrices(
            K12=I.copy(), K21=I.copy(), K34=I.copy(), K43=I.copy(),
            K13=I.copy(), K31=I.copy(), K24=I.copy(), K42=I.copy(),
            K12_loss=losses.copy(), K21_loss=losses.copy(),
            K34_loss=losses.copy(), K43_loss=losses.copy(),
            K13_loss=losses.copy(), K31_loss=losses.copy(),
            K24_loss=losses.copy(), K42_loss=losses.copy(),
            K12_bh=I.copy(), K21_bh=I.copy(), K34_bh=I.copy(), K43_bh=I.copy(),
            K13_bh=I.copy(), K31_bh=I.copy(), K24_bh=I.copy(), K42_bh=I.copy(),
            K12_map=I.copy(), K21_map=I.copy(), K34_map=I.copy(), K43_map=I.copy(),
            K13_map=I.copy(), K31_map=I.copy(), K24_map=I.copy(), K42_map=I.copy(),
        )

    def _compute_knm_matrix_bayer_helms(
        self, sim,
        qx1, qy1, qx2, qy2,
        nr1, nr2, co2,
        direction, Knm
    ):
        # port p1 -> p2 (reflection)
        if direction == KnmBSNodeDirection.BS12:
            xgamma = 2.0 * self.xbeta
            ygamma = 2.0 * self._cos_alpha * self.ybeta
            nr = nr1
        # port p2 -> p1 (reflection)
        elif direction == KnmBSNodeDirection.BS21:
            xgamma = 2.0 * self.xbeta
            ygamma = 2.0 * self._cos_alpha * self.ybeta
            nr = nr1
        # port p3 -> p4 (reflection)
        elif direction == KnmBSNodeDirection.BS34:
            xgamma = 2.0 * self.xbeta
            ygamma = -2.0 * co2 * self.ybeta
            nr = nr2
        # port p4 -> p3 (reflection)
        elif direction == KnmBSNodeDirection.BS43:
            xgamma = 2.0 * self.xbeta
            ygamma = -2.0 * co2 * self.ybeta
            nr = nr2
        # port p1 -> p3 (transmission)
        elif direction == KnmBSNodeDirection.BS13:
            GAMMA_FACTOR = -(1 - nr1 / nr2)
            xgamma = GAMMA_FACTOR * self.xbeta
            ygamma = GAMMA_FACTOR * self.ybeta
            nr = nr2
        # port p3 -> p1 (transmission)
        elif direction == KnmBSNodeDirection.BS31:
            GAMMA_FACTOR = (1 - nr2 / nr1)
            xgamma = -GAMMA_FACTOR * self.xbeta
            ygamma = GAMMA_FACTOR * self.ybeta
            nr = nr1
        # port p2 -> p4 (transmission)
        elif direction == KnmBSNodeDirection.BS24:
            GAMMA_FACTOR = -(1 - nr1 / nr2)
            xgamma = GAMMA_FACTOR * self.xbeta
            ygamma = GAMMA_FACTOR * self.ybeta
            nr = nr2
        # port p4 -> p2 (transmission)
        elif direction == KnmBSNodeDirection.BS42:
            GAMMA_FACTOR = (1 - nr2 / nr1)
            xgamma = -GAMMA_FACTOR * self.xbeta
            ygamma = GAMMA_FACTOR * self.ybeta
            nr = nr1

        run_bayer_helms(
            Knm,
            qx1, qy1, qx2, qy2,
            xgamma, ygamma,
            nr,
            sim.homs,
            sim.model.lambda0,
            reverse_gouy=False
        )

    def _apply_knm_sequence(self, sim, qx1, qy1, qx2, qy2, nr1, nr2, co2, direction, total_refl=False):
        if direction == KnmBSNodeDirection.BS12:
            Knm = self.knm_matrices.K12
            Knm_loss = self.knm_matrices.K12_loss
            Knm_bh = self.knm_matrices.K12_bh
            Knm_map = self.knm_matrices.K12_map
            turn = True
        elif direction == KnmBSNodeDirection.BS21:
            Knm = self.knm_matrices.K21
            Knm_loss = self.knm_matrices.K21_loss
            Knm_bh = self.knm_matrices.K21_bh
            Knm_map = self.knm_matrices.K21_map
            turn = True
        elif direction == KnmBSNodeDirection.BS34:
            Knm = self.knm_matrices.K34
            Knm_loss = self.knm_matrices.K34_loss
            Knm_bh = self.knm_matrices.K34_bh
            Knm_map = self.knm_matrices.K34_map
            turn = True
        elif direction == KnmBSNodeDirection.BS43:
            Knm = self.knm_matrices.K43
            Knm_loss = self.knm_matrices.K43_loss
            Knm_bh = self.knm_matrices.K43_bh
            Knm_map = self.knm_matrices.K43_map
            turn = True
        elif direction == KnmBSNodeDirection.BS13:
            Knm = self.knm_matrices.K13
            Knm_loss = self.knm_matrices.K13_loss
            Knm_bh = self.knm_matrices.K13_bh
            Knm_map = self.knm_matrices.K13_map
            turn = False
        elif direction == KnmBSNodeDirection.BS31:
            Knm = self.knm_matrices.K31
            Knm_loss = self.knm_matrices.K31_loss
            Knm_bh = self.knm_matrices.K31_bh
            Knm_map = self.knm_matrices.K31_map
            turn = False
        elif direction == KnmBSNodeDirection.BS24:
            Knm = self.knm_matrices.K24
            Knm_loss = self.knm_matrices.K24_loss
            Knm_bh = self.knm_matrices.K24_bh
            Knm_map = self.knm_matrices.K24_map
            turn = False
        elif direction == KnmBSNodeDirection.BS42:
            Knm = self.knm_matrices.K42
            Knm_loss = self.knm_matrices.K42_loss
            Knm_bh = self.knm_matrices.K42_bh
            Knm_map = self.knm_matrices.K42_map
            turn = False

        # total internal reflection at beam splitter so set couplings to zero
        if total_refl:
            Knm *= 0
            Knm_bh *= 0
            Knm_map *= 0

        else:
            # TODO (sjr) map coupling coefficient calculations

            # compute coupling coefficients via Bayer-Helms
            self._compute_knm_matrix_bayer_helms(
                sim, qx1, qy1, qx2, qy2, nr1, nr2, co2, direction, Knm_bh
            )

            # compute combined Knm matrix
            Knm_no_rgouy = Knm_bh @ Knm_map

            # apply the reverse gouy phase
            rev_all_gouy(Knm_no_rgouy, sim.homs, qx1, qy1, qx2, qy2, out=Knm)
            # zero the phase of TEM00 -> TEM00 coefficient
            if sim.model.phase_config.ZERO_K00: zero_tem00_phase(Knm)

            if turn: flip_odd_horizontal(Knm, sim.homs)

        # compute total losses from each (n,m) to all
        # couplings (for quantum noise calcs)
        knm_loss(Knm, out=Knm_loss)

    def _compute_knm_matrices(self, sim, force=False):
        if not force:
            if not sim.is_modal or (not self._model._trace_changed and not self._angle_changed):
                LOGGER.info("No knm dependent parameters have changed, skipping recomputation "
                            f"of knm matrices for: {self.name}")
                return

        LOGGER.info(f"Computing knm matrices for: {self.name}")

        # get beam parameters...
        node_q_map = sim.model.last_trace
        qx_p1i, qy_p1i, _ = node_q_map[self.p1.i]
        qx_p1o, qy_p1o, _ = node_q_map[self.p1.o]
        qx_p2i, qy_p2i, _ = node_q_map[self.p2.i]
        qx_p2o, qy_p2o, _ = node_q_map[self.p2.o]
        qx_p3i, qy_p3i, _ = node_q_map[self.p3.i]
        qx_p3o, qy_p3o, _ = node_q_map[self.p3.o]
        qx_p4i, qy_p4i, _ = node_q_map[self.p4.i]
        qx_p4o, qy_p4o, _ = node_q_map[self.p4.o]

        # ... and refractive indices of spaces
        nr1, nr2 = refractive_indices(self.p1, self.p3)

        if nr1 == 0:
            raise ValueError(
                f"refractive index nr1 zero in _compute_knm_matrices of {self}")
        if nr2 == 0:
            raise ValueError(
                f"refractive index nr2 zero in _compute_knm_matrices of {self}")

        total_reflections = {
            KnmBSNodeDirection.BS12 : False, KnmBSNodeDirection.BS21 : False,
            KnmBSNodeDirection.BS34 : False, KnmBSNodeDirection.BS43 : False
        }

        # compute propagations of q -> q' for each coupling
        # to be used as the input bases for computing knm
        # -> reflections
        try:
            qx_p1i_prefl = apply(self.ABCD(self.p1.i, self.p2.o, 'x'), qx_p1i.q, nr1, nr1)
            qy_p1i_prefl = apply(self.ABCD(self.p1.i, self.p2.o, 'y'), qy_p1i.q, nr1, nr1)
        except TotalReflectionError:
            total_reflections[KnmBSNodeDirection.BS12] = True
            qx_p1i_prefl = None
            qy_p1i_prefl = None

        try:
            qx_p2i_prefl = apply(self.ABCD(self.p2.i, self.p1.o, 'x'), qx_p2i.q, nr1, nr1)
            qy_p2i_prefl = apply(self.ABCD(self.p2.i, self.p1.o, 'y'), qy_p2i.q, nr1, nr1)
        except TotalReflectionError:
            total_reflections[KnmBSNodeDirection.BS21] = True
            qx_p2i_prefl = None
            qy_p2i_prefl = None

        try:
            qx_p3i_prefl = apply(self.ABCD(self.p3.i, self.p4.o, 'x'), qx_p3i.q, nr2, nr2)
            qy_p3i_prefl = apply(self.ABCD(self.p3.i, self.p4.o, 'y'), qy_p3i.q, nr2, nr2)
        except TotalReflectionError:
            total_reflections[KnmBSNodeDirection.BS34] = True
            qx_p3i_prefl = None
            qy_p3i_prefl = None

        try:
            qx_p4i_prefl = apply(self.ABCD(self.p4.i, self.p3.o, 'x'), qx_p4i.q, nr2, nr2)
            qy_p4i_prefl = apply(self.ABCD(self.p4.i, self.p3.o, 'y'), qy_p4i.q, nr2, nr2)
        except TotalReflectionError:
            total_reflections[KnmBSNodeDirection.BS43] = True
            qx_p4i_prefl = None
            qy_p4i_prefl = None

        # -> transmissions
        qx_p1i_ptrns = apply(
            self.ABCD(self.p1.i, self.p3.o, 'x'), qx_p1i.q, nr1, nr2)
        qy_p1i_ptrns = apply(
            self.ABCD(self.p1.i, self.p3.o, 'y'), qy_p1i.q, nr1, nr2)
        qx_p2i_ptrns = apply(
            self.ABCD(self.p2.i, self.p4.o, 'x'), qx_p2i.q, nr1, nr2)
        qy_p2i_ptrns = apply(
            self.ABCD(self.p2.i, self.p4.o, 'y'), qy_p2i.q, nr1, nr2)
        qx_p3i_ptrns = apply(
            self.ABCD(self.p3.i, self.p1.o, 'x'), qx_p3i.q, nr2, nr1)
        qy_p3i_ptrns = apply(
            self.ABCD(self.p3.i, self.p1.o, 'y'), qy_p3i.q, nr2, nr1)
        qx_p4i_ptrns = apply(
            self.ABCD(self.p4.i, self.p2.o, 'x'), qx_p4i.q, nr2, nr1)
        qy_p4i_ptrns = apply(
            self.ABCD(self.p4.i, self.p2.o, 'y'), qy_p4i.q, nr2, nr1)

        # angle coefficient that scales depending on beam rotation
        co2 = np.cos(
            np.arcsin(np.sin(np.radians(self.alpha.value) * (nr1 / nr2))))

        # K12 calculations
        self._apply_knm_sequence(
            sim, qx_p1i_prefl, qy_p1i_prefl, qx_p2o.q, qy_p2o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS12,
            total_reflections[KnmBSNodeDirection.BS12]
        )

        # K21 calculations
        self._apply_knm_sequence(
            sim, qx_p2i_prefl, qy_p2i_prefl, qx_p1o.q, qy_p1o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS21,
            total_reflections[KnmBSNodeDirection.BS21]
        )

        # K34 calculations
        self._apply_knm_sequence(
            sim, qx_p3i_prefl, qy_p3i_prefl, qx_p4o.q, qy_p4o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS34,
            total_reflections[KnmBSNodeDirection.BS34]
        )

        # K43 calculations
        self._apply_knm_sequence(
            sim, qx_p4i_prefl, qy_p4i_prefl, qx_p3o.q, qy_p3o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS43,
            total_reflections[KnmBSNodeDirection.BS43]
        )

        # K13 calculations
        self._apply_knm_sequence(
            sim, qx_p1i_ptrns, qy_p1i_ptrns, qx_p3o.q, qy_p3o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS13
        )

        # K31 calculations
        self._apply_knm_sequence(
            sim, qx_p3i_ptrns, qy_p3i_ptrns, qx_p1o.q, qy_p1o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS31
        )

        # K24 calculations
        self._apply_knm_sequence(
            sim, qx_p2i_ptrns, qy_p2i_ptrns, qx_p4o.q, qy_p4o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS24
        )

        # K42 calculations
        self._apply_knm_sequence(
            sim, qx_p4i_ptrns, qy_p4i_ptrns, qx_p2o.q, qy_p2o.q,
            nr1, nr2, co2,
            KnmBSNodeDirection.BS42
        )

        matrices_to_log = self._knm_logging_info["matrices"]
        couplings_to_log = self._knm_logging_info["couplings"]
        if "K12" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K12,
                                       sim.homs, "K12 = \n", couplings_to_log))
        if "K21" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K21,
                                       sim.homs, "K21 = \n", couplings_to_log))
        if "K34" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K34,
                                       sim.homs, "K34 = \n", couplings_to_log))
        if "K43" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K43,
                                       sim.homs, "K43 = \n", couplings_to_log))
        if "K13" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K13,
                                       sim.homs, "K13 = \n", couplings_to_log))
        if "K31" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K31,
                                       sim.homs, "K31 = \n", couplings_to_log))
        if "K24" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K24,
                                       sim.homs, "K24 = \n", couplings_to_log))
        if "K42" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K42,
                                       sim.homs, "K42 = \n", couplings_to_log))
