"""
Optical components performing directional suppression of beams.
"""

import numpy as np

from finesse.components.general import Connector, InteractionType
from finesse.components.node import NodeDirection, NodeType
from finesse.element import model_parameter, Rebuild


@model_parameter('S', 0.0, Rebuild.PlaneWave)
class Isolator(Connector):
    """
    A class representing an isolator optical component with a suppression
    factor.
    """
    def __init__(self, name, S=0.0):
        """
        Constructs a new `Isolator` with the specified properties.

        Parameters
        ----------
        name : str
            Name of newly created `Isolator` instance.

        S : float
            Power suppression in dB.
        """
        super().__init__(name)

        self.S = S

        self._add_port("p1", NodeType.OPTICAL)
        self.p1._add_node('i', NodeDirection.INPUT)
        self.p1._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p2", NodeType.OPTICAL)
        self.p2._add_node('i', NodeDirection.INPUT)
        self.p2._add_node('o', NodeDirection.OUTPUT)

        # optic to optic couplings
        self._register_node_coupling(self.p1.i, self.p2.o,
                                     interaction_type= InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p2.i, self.p1.o,
                                     interaction_type= InteractionType.TRANSMISSION)

    def _on_build(self, sim):
        self.__I = np.eye(sim.nhoms, dtype=np.complex128)

    def _fill_matrix(self, sim):
        values, is_changing = self._eval_parameters()
        suppression = 10**(-values["S"] / 20)
        for freq in sim.frequencies:
            with sim.component_edge_fill(self, 'p1.i->p2.o', freq, freq) as mat:
                mat[:] = self.__I[:]
            with sim.component_edge_fill(self, 'p2.i->p1.o', freq, freq) as mat:
                np.multiply(suppression, self.__I, out=mat[:])
