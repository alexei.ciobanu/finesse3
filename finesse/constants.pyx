"""
Physical constants module exposing a single tuple (`values`)
containing the constant values.

This tuple is imported in the top-level Finesse module via
``from finesse.constants import values as constants``, allowing
access to this structure with::

    from finesse import constants

    # get the speed of light
    c = constants.C_LIGHT

TODO add list of all defined constants exposed by the values tuple
"""

cdef extern from "constants.h":
    long double _PI "PI"
    double _C_LIGHT "C_LIGHT"

from collections import namedtuple

values = namedtuple("Constants", "PI C_LIGHT")(_PI, _C_LIGHT)
