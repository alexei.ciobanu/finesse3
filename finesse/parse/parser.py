import logging
import re
import operator
import numpy as np
import sys
from collections import namedtuple

from sly import Lexer, Parser

import finesse.analysis as analysis
import finesse.components as components
import finesse.detectors as detectors
import finesse.gaussian as gaussian
from finesse.model import Model
from finesse.kat import Kat

LOGGER = logging.getLogger(__name__)

Instruction = namedtuple("Instruction", ["type", "name", "args", "line"])


def maxtem(model, value):
    """Set the model maxtem to the specified value"""
    model.maxtem = value


def gauss(model, node, **kwargs):
    """
    Set the q value at a node, either directly or by specifiying waist
    size w0 and distance to waist z. Additionally, to set different q
    values in x and y, x-y pairs of any of the above parameters can be
    specified, such as qx, qy, w0x etc. For more detail, see
    :class:`.gaussian.BeamParam`

    Parameters
    ==========
    model: :class:`.Model`:
        The model to modify

    node: :class:`.components.Node`:
        The node to set the q value at.

    q: complex, optional:
        Complex beam parameter at the node.

    w0: float, optional:
        Size of the beam waist (in metres). Must be specified together
        with z.

    z: float, optional:
        Distance to the beam waist(in metres). Must be specified
        together with w0
    """
    if "q" in kwargs:
        if len(kwargs) > 1:
            raise ValueError("Cannot specify both q and another parameter")
        node.q = complex(kwargs["q"])
    elif len({"qx", "qy"} & kwargs.keys()) > 0:
        if len({"qx", "qy"} & kwargs.keys()) != 2:
            raise ValueError("Must specify both qx and qy, or just q")
        elif len(kwargs) != 2:
            raise ValueError("Cannot specify qx, qy and another parameter")
        node.q = (kwargs["qx"], kwargs["qy"])
    elif len({"w0", "z"} & kwargs.keys()) > 0:
        if len({"w0", "z"} & kwargs.keys()) != 2:
            raise ValueError("Must specify both w0 and z")
        elif len(kwargs) != 2:
            raise ValueError("Cannot specify w0, z and another parameter")
        node.q = gaussian.BeamParam(**kwargs)
    elif len({"w0x", "w0y", "zx", "zy"} & kwargs.keys()) > 0:
        if len({"w0x", "w0y", "zx", "zy"} & kwargs.keys()) != 4:
            raise ValueError("Must specify all of w0[xy] and z[xy]")
        elif len(kwargs) != 4:
            raise ValueError("Cannot specify w0[xy], z[xy] and another parameter")
        node.q = (gaussian.BeamParam(w0=kwargs["w0x"], z=kwargs["zx"]),
                  gaussian.BeamParam(w0=kwargs["w0y"], z=kwargs["zy"]),)
    else:
        raise ValueError("Invalid arguments to gauss")


def modes(model, *args, **kwargs):
    """Wrapper around :func:`.Model.select_modes`"""
    if isinstance(args[0], list):
        for i, mode in enumerate(args[0]):
            args[0][i] = f"{mode:02d}"
    model.select_modes(*args, **kwargs)


def startnode(model, node):
    """Set the starting node for beamtracing"""
    model.beam_trace_args["startnode"] = node


class KatParser:
    """Kat file lexer, parser and builder."""

    # Mappings of component names to constructors. These are defined in
    # separate dictionaries, which are constructed in separate passes,
    # as eg. Spaces need the components they are connected to to already be
    # constructed.
    component_constructors = [
        {
            "laser": components.Laser,
            "l": components.Laser,
            "mirror": components.Mirror,
            "m": components.Mirror,
            "beam_splitter": components.Beamsplitter,
            "bs": components.Beamsplitter,
            "isolator": components.Isolator,
            "isol": components.Isolator,
            "modulator": components.Modulator,
            "mod": components.Modulator,
            "lens": components.Lens,
            "variable": components.Variable,
            "var": components.Variable,
        },
        {
            "space": components.Space,
            "s": components.Space,
            "cavity": components.Cavity,
            "cav": components.Cavity,
            "amplitude_detector": detectors.AmplitudeDetector,
            "ad": detectors.AmplitudeDetector,
            "beam_property_detector": detectors.BeamPropertyDetector,
            "bp": detectors.BeamPropertyDetector,
            "ccd": detectors.CCD,
            "power_detector": detectors.PowerDetector,
            "pd": detectors.PowerDetector,
            "quantum_noise_detector": detectors.QuantumNoiseDetector,
            "qd": detectors.QuantumNoiseDetector,
            "quantum_shot_noise_detector": None,
            "qshot": None,
        },
    ]

    # Commands that don't construct a component, but perform some other
    # function (and hence don't have a mandatory name parameter)
    commands = {
        "gauss": gauss,
        "maxtem": maxtem,
        "modes": modes,
        "startnode": startnode,
    }

    analysis_constructors = {
        "xaxis": analysis.xaxis,
    }

    def __init__(self, prefix=None):
        """
        Parameters
        ----------
        prefix : str, optional
            String to prefix each constructed component's name with.
        """

        self.lexer = _KatLEX()
        self.parser = _KatYACC(prefix)
        self.reset()
        self.prefix = prefix

    def reset(self):
        """
        Delete all parsed code, resetting the parser to a newly constructed
        state.
        """
        self.text = ""
        self.parser.reset()
        self.lexer.reset()

    def parse(self, text=None, path=None):
        """
        Parses a kat file into an intermediate internal representation, which
        can then be built by :meth:`.KatParser.build`. One of either `text` or
        `path` must be provided.

        Parameters
        ----------
        text : str, optional
            String containing the kat code to be parsed.

        path : str, optional
            Path of a file containing the kat code to be parsed.

        Raises
        ------
        ValueError
            If either both or neither of `text` and `path` are specified.

        KatParserError
            If an error occurs during parsing.
        """
        if text is None and path is None:
            raise ValueError("must provide either text or a path")

        if path is not None:
            if text is not None:
                raise ValueError("cannot specify both text and a file to parse")

            with open(path, "r") as obj:
                text = obj.read()

            source = path
        else:
            source = "from user input"

        # As we have no way of detecting EOF and calling pop_state() from
        # within the component lexer, we must ensure that all files end in a
        # newline
        self.text += f"{text}\n"

        tokens = self.lexer.tokenize(self.text)
        self.parser.parse(tokens)
        errors = sorted(self.lexer.errors + self.parser.errors,
                        key=lambda tup: tup[1])
        if len(errors) > 0:
            raise KatParserError(errors, self.text)

        LOGGER.info("Parsed %s", source)

    def build(self, model=None):
        """
        Constructs a :class:`.Kat` object from parsed kat code.

        Parameters
        ----------
        model : :class:`.Model`, optional
            Model object to add components to. If not specified, create
            a new model.

        Returns
        -------
        Kat
            The constructed :class:`.Kat` object.
        """
        def resolve_ports(model, args, kwargs):
            for i, v in enumerate(args):
                if isinstance(v, str) and "." in v:
                    if self.prefix is not None:
                        v = self.prefix + "_" + v
                    if v in imports:
                        v = imports[v]
                    port = model.elements[v.split(".")[0]]
                    for attr in v.split(".")[1:]:
                        port = getattr(port, attr)
                    args[i] = port
            for k, v in kwargs.items():
                if isinstance(v, str) and "." in v:
                    if self.prefix is not None:
                        v = self.prefix + "_" + v
                    if v in imports:
                        v = imports[v]
                    port = model.elements[v.split(".")[0]]
                    for attr in v.split(".")[1:]:
                        port = getattr(port, attr)
                    kwargs[k] = port

        def resolve_parameters(model, args, kwargs):
            for i, v in enumerate(args):
                if callable(v):
                    args[i] = v(model, imports)
                    try:
                        args[i] = args[i].parameter
                    except AttributeError:
                        pass
            for k, v in kwargs.items():
                if callable(v):
                    kwargs[k] = v(model, imports).parameter
                    try:
                        kwargs[k] = kwargs[k].parameter
                    except AttributeError:
                        pass

        def clean_exception(e, instruction):
            def sub1(match):
                n = int(match.group())
                return str(n - 1)

            line = self.text.split("\n")[instruction.line - 1]
            msg = e.args[0].replace("__init__()", f"'{instruction.type}'")
            msg = re.sub(r"([a-zA-Z0-9]+)\(\)", r"'\1'", msg)
            if "arguments" in msg:
                msg = re.sub(r"\b[0-9]+\b", sub1, msg)
            msg = f"line {instruction.line}: {msg}\n> {line}\n"
            if len(e.args) >= 1:
                e.args = (msg,) + e.args[1:]

        model = model or Model()
        imports = {}
        if self.prefix is not None:
            prefix = self.prefix + "_"
        else:
            prefix = ""

        for name, file in self.parser.kittens.items():
            if self.prefix is not None:
                prefix = self.prefix + "_" + name
            else:
                prefix = name
            k = KatParser(prefix)
            k.parse(path=file)
            k.build(model=model)
            imports.update(k.parser.exports)

        for k, v in self.parser.exports.items():
            if v in imports:
                self.parser.exports[k] = imports[v]

        for constructor_pass in self.component_constructors:
            for component in self.parser.components:
                if component.type not in constructor_pass:
                    continue
                constructor = constructor_pass[component.type]
                if self.prefix is not None:
                    name = self.prefix + "_" + component.name
                else:
                    name = component.name
                args = component.args[0] or []
                kwargs = component.args[1] or {}

                resolve_ports(model, args, kwargs)
                try:
                    c = constructor(name, *args, **kwargs)
                    model.add(c)
                except Exception as e:
                    clean_exception(e, component)
                    raise

        for command in self.parser.commands:
            fn = self.commands[command.type]
            args = command.args[0] or []
            kwargs = command.args[1] or {}

            resolve_ports(model, args, kwargs)
            try:
                fn(model, *args, **kwargs)
            except Exception as e:
                clean_exception(e, command)
                raise

        analyses = []

        for _analysis in self.parser.analyses:
            constructor = self.analysis_constructors[_analysis.type]
            args = _analysis.args[0] or []
            kwargs = _analysis.args[1] or {}
            resolve_parameters(model, args, kwargs)
            try:
                a = constructor(*args, **kwargs)
                analyses.append(a)
            except Exception as e:
                clean_exception(e, _analysis)
                raise

        if len(analyses) == 0:
            analyses.append(analysis.noxaxis(model))

        for comp in model.elements.values():
            for param in comp._params:
                param.resolve(imports)

        return Kat(model, analyses)


class _KatLEX(Lexer):
    """Kat lexer, default state."""

    tokens = {"ANALYSIS", "COMMAND", "COMPONENT", "EXTERN", "NAME"}

    # Ignored patterns.
    ignore = " \t"
    ignore_ftblock = "%%%"
    ignore_comment = "#.*"

    def __init__(self):
        self.reset()

    def reset(self):
        self.errors = []
        self.nesting = []

    @_(r"\n+")
    def ignore_newline(self, t):
        self.lineno += t.value.count('\n')

    @_("xaxis")
    def ANALYSIS(self, t):
        self.push_state(_KatComponentLEX)
        return t

    @_("gauss", "maxtem", "modes", "startnode")
    def COMMAND(self, t):
        self.push_state(_KatComponentLEX)
        return t

    @_("beam_property_detector", "quantum_shot_noise_detector",
       "directional_beam_splitter", "quantum_noise_detector",
       "amplitude_detector", "signal_generator", "power_detector",
       "beam_splitter", "attribute", "modulator", "constant", "function",
       "isolator", "variable", "cavity", "mirror", "const", "laser", "qshot",
       "space", "xaxis", "attr", "fsig", "func", "isol", "lens", "lock",
       "mask", "cav", "ccd", "dbs", "mod", "put", "tem", "var", "ad", "bp",
       "bs", "pd", "qd", "sg", "l", "m", "s")
    def COMPONENT(self, t):
        self.push_state(_KatComponentLEX)
        return t

    @_("extern")
    def EXTERN(self, t):
        self.push_state(_KatComponentLEX)
        return t

    @_(r"[a-zA-Z_][a-zA-Z0-9_]*")
    def NAME(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def error(self, t):
        command = t.value.split("\n")[0].split(" ")[0]
        self.errors.append((f"Command '{command}' unrecognised", self.lineno, self.index))
        self.index += len(command)
        return t


class _KatComponentLEX(Lexer):
    tokens = {"PLUS", "MINUS", "TIMES", "DIVIDE", "FUNCTION_NAME",
              "PARAMETER", "NUMBER", "STRING", "LPAREN", "RPAREN", "LBRACKET",
              "RBRACKET", "END"}
    literals = {'='}

    FUNCTION_NAME = "cos|sin"
    PARAMETER = "&[a-zA-Z_][a-zA-Z0-9_.]*"
    PLUS = r"\+"
    MINUS = r"-"
    TIMES = r"\*"
    DIVIDE = r"/"

    # Ignored patterns.
    ignore = " \t"
    ignore_ftblock = "%%%"
    ignore_comment = "#.*"

    def reset(self):
        self.pop_state()
        self.reset()

    # Number token including scientific notation, float,
    # or +/- inf (all states).
    @_(r"[+-]?inf",
       r"[+-]?(\d+\.\d*|\d*\.\d+|\d+)([eE]-?\d*\.?\d*)?j?([pnumkMGT])?")
    def NUMBER(self, t):
        if re.match(".*[pnumkMGT]$", t.value):
            t.value = t.value.replace("p", "e-12")
            t.value = t.value.replace("n", "e-9")
            t.value = t.value.replace("u", "e-6")
            t.value = t.value.replace("m", "e-3")
            t.value = t.value.replace("k", "e3")
            t.value = t.value.replace("M", "e6")
            t.value = t.value.replace("G", "e9")
            t.value = t.value.replace("T", "e12")
        if "j" in t.value:
            t.value = complex(t.value)
        else:
            t.value = float(t.value)
            if t.value.is_integer():
                t.value = int(t.value)
        return t

    @_(r"[a-zA-Z_][a-zA-Z0-9_.]*\*?", "inf")
    def STRING(self, t):
        if t.value == "inf":
            t.type = "NUMBER"
            return self.NUMBER(t)
        return t

    @_("\n")
    def END(self, t):
        self.lineno += t.value.count('\n')
        if len(self.nesting) == 0:
            self.pop_state()
            return t

    @_(r'\(')
    def LPAREN(self, t):
        self.nesting.append(('(', self.lineno))
        self.push_state(_KatComponentLEX)
        return t

    @_(r'\)')
    def RPAREN(self, t):
        try:
            assert(self.nesting.pop()[0] == "(")
            self.pop_state()
        except (IndexError, AssertionError):
            self.errors.append((f"Extraneous ')'", self.lineno, self.index))
        return t

    @_(r'\[')
    def LBRACKET(self, t):
        self.nesting.append(('[', self.lineno))
        return t

    @_(r'\]')
    def RBRACKET(self, t):
        try:
            self.nesting.pop()
        except IndexError:
            self.errors.append((f"Extraneous ']'", self.lineno, self.index))
        return t


class _KatYACC(Parser):
    # Silence shift/reduce conflict warning
    log = logging.getLogger()
    log.setLevel(logging.ERROR)
    #debugfile = "parser.out"

    tokens = set.union(_KatLEX.tokens, _KatComponentLEX.tokens)

    precedence = (
       ('left', "RPAREN"),
       ('left', "PLUS", "MINUS"),
       ('left', "TIMES", "DIVIDE"),
       ('right', "UMINUS"),
    )

    _operators = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": operator.truediv,
            }

    _functions = {
            "cos": np.cos,
            "sin": np.sin,
            }

    def __init__(self, prefix):
        self.reset()
        self.prefix = prefix

    def reset(self):
        self.components = []
        self.commands = []
        self.analyses = []
        self.exports = {}
        self.kittens = {}
        self.errors = []

    @_("instruction END", "statement instruction END")
    def statement(self, p):
        pass

    @_("expr",
       "STRING",
       "tags",
       "numbers")
    def value(self, p):
        return p[0]

    @_("expr PLUS expr",
       "expr MINUS expr",
       "expr TIMES expr",
       "expr DIVIDE expr",)
    def expr(self, p):
        operation = self._operators[p[1]]
        lhs = p[0]
        rhs = p[2]
        if not callable(lhs) and not callable(rhs):
            return operation(lhs, rhs)
        if not callable(lhs):
            return lambda model, mapping: operation(lhs, rhs(model, mapping))
        if not callable(rhs):
            return lambda model, mapping: operation(lhs(model, mapping), rhs)
        return lambda model, mapping: operation(lhs(model, mapping), rhs(model, mapping))

    @_("MINUS expr %prec UMINUS")
    def expr(self, p):
        rhs = p[1]
        if not callable(rhs):
            return -rhs
        return lambda model, mapping: -rhs(model, mapping)

    @_("LPAREN expr RPAREN")
    def expr(self, p):
        return p[1]

    @_("NUMBER",
       "function")
    def expr(self, p):
        return p[0]

    @_("PARAMETER")
    def expr(self, p):
        string = p[0]
        if self.prefix is not None:
            c = string[0]
            string = string.lstrip("&")
            string = c + self.prefix + "_" + string
        def fn(model, mapping, string):
            param = string.lstrip("&")
            if param in mapping:
                param = mapping[param]
            comp = model.elements[param.split(".")[0]]
            for attr in param.split(".")[1:]:
                comp = getattr(comp, attr)
            return comp.ref
        return lambda model, mapping: fn(model, mapping, string)

    @_("FUNCTION_NAME LPAREN expr RPAREN")
    def function(self, p):
        expr = p.expr
        fn = self._functions[p[0]]
        if not callable(expr):
            return fn(expr)
        return lambda model, mapping: fn(expr(model, mapping))

    @_("value", "value_list value")
    def value_list(self, p):
        if len(p) == 1:
            return [p[0]]
        p[0].append(p[1])
        return p[0]

    @_("STRING '=' value")
    def key_value_pair(self, p):
        return {p.STRING: p.value}

    @_("key_value_pair", "key_value_list key_value_pair")
    def key_value_list(self, p):
        if len(p) == 1:
            return p[0]
        p[0].update(p[1])
        return p[0]

    @_("value_list",
       "value_list key_value_list",
       "key_value_list")
    def params(self, p):
        try:
            vlist = p.value_list
        except KeyError:
            vlist = None
        try:
            kvlist = p.key_value_list
        except KeyError:
            kvlist = None
        return (vlist, kvlist)

    @_("NUMBER", "number_list NUMBER")
    def number_list(self, p):
        if len(p) == 1:
            return [p[0]]
        p[0].append(p[1])
        return p[0]

    @_("LBRACKET number_list RBRACKET")
    def numbers(self, p):
        return p.number_list

    @_("STRING", "tag_list STRING")
    def tag_list(self, p):
        if len(p) == 1:
            return [p[0]]
        p[0].append(p[1])
        return p[0]

    @_("LBRACKET tag_list RBRACKET")
    def tags(self, p):
        return p.tag_list

    @_("COMPONENT STRING params",
       "COMPONENT STRING LPAREN params RPAREN")
    def instruction(self, p):
        self.components.append(
                Instruction(p.COMPONENT, p.STRING, p.params, p.lineno)
        )

    @_("COMMAND params",
       "COMMAND LPAREN params RPAREN")
    def instruction(self, p):
        self.commands.append(Instruction(p.COMMAND, None, p.params, p.lineno))

    @_("ANALYSIS params",
       "ANALYSIS LPAREN params RPAREN")
    def instruction(self, p):
        self.analyses.append(Instruction(p.ANALYSIS, None, p.params, p.lineno))

    @_("EXTERN LPAREN STRING STRING RPAREN")
    def instruction(self, p):
        name = self.prefix + "." + p.STRING1
        param = self.prefix + "_" + p.STRING0
        self.exports[name] = param

    @_("EXTERN LPAREN PARAMETER STRING RPAREN")
    def instruction(self, p):
        name = self.prefix + "." + p.STRING
        param = self.prefix + "_" + p.PARAMETER.lstrip("&")
        self.exports[name] = param

    @_("NAME '=' STRING")
    def instruction(self, p):
        self.kittens[p.NAME] = p.STRING

    def error(self, p):
        if p is None:
            msg = "Unexpected end of file"
            self.errors.append((msg, None, None))
            return
        elif p.type == "ERROR":
            return
        msg = f"got unexpected token {p.value} of type {p.type}"
        self.errors.append((msg, p.lineno, p.index))


class KatParserError(ValueError): # __NODOC__
    """Kat file parser error"""
    def __init__(self, errors, text, **kwargs):
        message = "\n"
        for error in errors:
            lineno = error[1]
            idx = error[2]
            if lineno is None:
                # There is no lineno, as this was an end-of-file error,
                # so assume error was on last non-empty line
                line = re.findall(r"[^\s]", text)[-1]
                pos = len(text) - 1
            else:
                line = text.split("\n")[lineno - 1]
                pos = self.find_column(text, idx)
            expected = ""
            for pattern, exp in self.expected.items():
                if exp is not None and re.match(pattern, line) is not None:
                    expected = f", expected '{exp}'"
                    break
            message += f"{lineno}:{pos}: " + error[0] + expected + "\n"
            message += line + "\n"
            message += " " * (pos - 1) + "^\n"

        super().__init__(message.rstrip("\n"), **kwargs)

    @staticmethod
    def find_column(text, index):
        last_cr = text.rfind('\n', 0, index)
        if last_cr < 0:
            last_cr = 0
        column = (index - last_cr)
        return column

    expected = {
    }
