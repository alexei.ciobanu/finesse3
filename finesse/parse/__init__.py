"""
Parsing of both Finesse 3 model files and Finesse 2 `.kat` files.

Listed below are all the sub-modules of the ``parse`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: parse/
    {contents}
"""

import os

import finesse.parse.legacy as legacy
from finesse.parse.parser import KatParser, KatParserError


def parse(commands, finesse2=False):
    """
    Parses a mutliline string of Finesse commands and returns
    a constructed `.class`:finesse.model.Model to use.

    Parameters
    ----------
    commands : :class:`str`
        The kat commands to parse.
    finesse2 : :class:`bool`, optional
        Use the legacy parser.

    Returns
    -------
    :class:`finesse.model.Model`
        The constructed model.
    """
    k = get_parser(finesse2)
    if os.path.isfile(commands):
        k.parse(path=commands)
    else:
        k.parse(text=commands)
    return k.build()


def get_parser(finesse2=False):
    """Get Finesse parser.

    Parameters
    ----------
    finesse2 : :class:`bool`, optional
        Use the legacy parser.

    Returns
    -------
    :class:`.parser.KatParser` or :class:`.legacy.KatParser`
        The kat parser.
    """
    if finesse2:
        k = legacy.KatParser()
    else:
        k = KatParser()
    return k


if __doc__:
    from finesse.utilities.misc import _collect_submodules

    __doc__ = __doc__.format(
        contents=_collect_submodules("parse")
    )
