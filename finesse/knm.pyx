#cython: boundscheck=False, wraparound=False, initializedcheck=False

## no bounds-checking performed on arrays / memoryviews
## no negative indexing of arrays
## no checks for memoryview initialisation on every access operation

"""
Functions for the computation of coupling coefficients.
"""

import cython
# TODO (sjr) use prange where commented out once openmp configured properly in setup.py
#from cython.parallel import prange

from finesse.cmath.cmath cimport carg, conj, creal, cimag, csqrt # complex.h functions
from finesse.cmath.cmath cimport exp, sqrt # math.h functions
from finesse.cmath cimport complex_t # type: np.complex128_t (i.e. double complex)
from finesse.cmath cimport Math # namespace of common mathematical routines
from finesse.cmath cimport ComplexMath # namespace of complex mathematical routines
from finesse.cmath cimport Gaussian # namespace of Gaussian beam routines

cdef extern from "constants.h":
    double complex COMPLEX_0 # 0 + 0i
    double complex COMPLEX_1 # 1 + 0i
    double complex COMPLEX_I # 0 + 1i

cdef complex_t rev_gouy(
    complex_t k,
    int n1, int m1, int n2, int m2,
    complex_t qx1, complex_t qy1,
    complex_t qx2, complex_t qy2
) nogil:
    cdef:
        double gouy_qx1 = Gaussian.gouy(qx1)
        double gouy_qy1 = Gaussian.gouy(qy1)
        double gouy_qx2 = Gaussian.gouy(qx2)
        double gouy_qy2 = Gaussian.gouy(qy2)
        double phase, phase1, phase2

    phase1 = (n1 + 0.5) * gouy_qx1 + (m1 + 0.5) * gouy_qy1
    phase2 = (n2 + 0.5) * gouy_qx2 + (m2 + 0.5) * gouy_qy2
    phase = phase2 - phase1

    return ComplexMath.rotate(k, phase)

cdef complex_t S_g(complex_t X, complex_t XS, complex_t F, complex_t FS, int m1, int m2) nogil:
    cdef:
        int s1, s2
        complex_t z, z1, z2
        complex_t nom1, nom2
        double denom1, denom2
        int i, j, k # loop indices

    if m1 == 0 or m1 == 1:
        s1 = 0
    else:
        if m1 % 2: # m1 odd
            s1 = int((m1 - 1) / 2)
        else: # m1 even
            s1 = int(m1 / 2)

    if m2 == 0 or m2 == 1:
        s2 = 0
    else:
        if m2 % 2: # m2 odd
            s2 = int((m2 - 1) / 2)
        else: # m2 even
            s2 = int(m2 / 2)

    z = COMPLEX_0
    # optimised for-loop
    # see: https://stackoverflow.com/questions/21382180/cython-pure-c-loop-optimization
    for i in range(1 + s1):
        for j in range(1 + s2):
            nom1 = (
                ComplexMath.pow_complex(XS, m1 - 2 * i) *
                ComplexMath.pow_complex(X, m2 - 2 * j)
            )
            denom1 = (
                Math.factorial(m1 - 2 * i) *
                Math.factorial(m2 - 2 * j)
            )
            z1 = nom1 * Math.msign(i) / denom1
            z2 = COMPLEX_0
            for k in range(1 + Math.nmin(i , j)):
                nom2 = (
                    ComplexMath.pow_complex(FS, i - k) *
                    ComplexMath.pow_complex(F, j - k)
                )
                denom2 = (
                    Math.factorial(2 * k) *
                    Math.factorial(i - k) *
                    Math.factorial(j - k)
                )
                z2 += nom2 * Math.msign(k) / denom2
            z += z1 * z2

    return z

cdef complex_t S_u(complex_t X, complex_t XS, complex_t F, complex_t FS, int m1, int m2) nogil:
    cdef:
        int s3, s4
        complex_t z, z1, z2
        complex_t nom1, nom2
        double denom1, denom2
        int i, j, k # loop indices

    if m1 == 0 or m2 == 0:
        return COMPLEX_0

    if m1 == 1 or m1 == 2:
        s3 = 0
    else:
        if m1 % 2: # m1 odd -> (m1 - 1) even
            s3 = int((m1 - 1) / 2)
        else: # m1 even -> (m1 - 1) odd
            s3 = int((m1 - 2) / 2)

    if m2 == 1 or m2 == 2:
        s4 = 0
    else:
        if m2 % 2: # m2 odd -> (m2 -1) even
            s4 = int((m2 - 1) / 2)
        else:
            s4 = int((m2 - 2) / 2)

    z = COMPLEX_0
    for i in range(1 + s3):
        for j in range(1 + s4):
            nom1 = (
                ComplexMath.pow_complex(XS, m1 - 2 * i - 1) *
                ComplexMath.pow_complex(X, m2 - 2 * j - 1)
            )
            denom1 = Math.factorial(m1 - 2 * i - 1) * Math.factorial(m2 - 2 * j - 1)
            z1 = nom1 * Math.msign(i) / denom1
            z2 = COMPLEX_0
            for k in range(1 + Math.nmin(i, j)):
                nom2 = (
                    ComplexMath.pow_complex(FS, i - k) *
                    ComplexMath.pow_complex(F, j - k)
                )
                denom2 = (
                    Math.factorial(2 * k + 1) *
                    Math.factorial(i - k) *
                    Math.factorial(j - k)
                )
                z2 += nom2 * Math.msign(k) / denom2
            z += z1 * z2

    return z

cdef complex_t k_mm(
    int m1, int m2,
    complex_t q1, complex_t q2,
    double gamma,
    double nr, double lambda0
) nogil:
    cdef:
        double Theta, K0
        double gg0, z2z0
        double zr
        double out1
        complex_t K
        complex_t XS, X, FS, F
        complex_t ex
        complex_t k1, x1, x2
        complex_t out2
        complex_t sg, su

    # TODO: (sjr) confusing set of computations, I need to document this properly (using Living
    #       review Section 9.16 and/or Bayer-Helms coupling coeff. paper)

    if ComplexMath.ceq(q1, q2):
        K = COMPLEX_0
        K0 = 0.0
        k1 = COMPLEX_1
        FS = COMPLEX_0
        F = COMPLEX_0
    else:
        zr = 1.0 / cimag(q2)
        K = (
            0.5 * (cimag(q1) - cimag(q2)) * zr +
            0.5 * (creal(q1) - creal(q2)) * zr * 1.0j
        )
        K0 = 2.0 * creal(K)
        # NOTE not possible for conj(K) to be -1 * COMPLEX_1,
        #      so we're fine to use unsafe version of inverse here
        k1 = ComplexMath.inverse_unsafe(csqrt(COMPLEX_1 + conj(K)))
        FS = 0.5 * K / (1.0 + K0)
        F = 0.5 * conj(K)

    Theta = Gaussian.w0_size(q2, nr, lambda0) / Gaussian.z_R(q2)

    if gamma == 0.0:
        x1 = x2 = XS = X = ex = COMPLEX_0
        gg0 = 0.0
    else:
        # gamma/gamma^hat_0
        gg0 = gamma / Theta
        # z^hat_2 / z^hat_0
        z2z0 = Gaussian.z_q(q2) / Gaussian.z_R(q2)

        # inner bracket of X^hat
        x1 = (z2z0 + 0.0j) - COMPLEX_I
        # inner bracket of X
        x2 = (z2z0 + 0.0j) + (COMPLEX_1 + 2.0 * conj(K))*COMPLEX_I

        # multiply by k1 which is (1 + K*)^(-0.5)
        # also multiply inner brackets calculated above by gamma/gamma^hat_0 factor
        XS = gg0 * x1 * k1 # X^hat
        X = gg0 * x2 * k1 # X
        ex = 0.5 * XS * X

    out1 = Math.msign(m2)
    out2 = (
        ComplexMath.pow_complex(COMPLEX_1 + conj(K), 0.5 * (-m1 - m2 -1)) *
        sqrt(Math.factorial(m1) * Math.factorial(m2) * (1 + K0)**(m1 + 0.5))
    )

    sg = S_g(X, XS, F, FS, m1, m2)
    su = S_u(X, XS, F, FS, m1, m2)

    return out1 * exp(-creal(ex)) * ComplexMath.rotate(out2 * (sg - su), -cimag(ex))

cdef complex_t k_nmnm(
    int n1, int m1,
    int n2, int m2,
    complex_t qx1, complex_t qy1,
    complex_t qx2, complex_t qy2,
    double xgamma, double ygamma,
    double nr, double lambda0
) nogil:
    cdef:
        complex_t z1, z2

    if xgamma == 0 and ygamma == 0:
        if ComplexMath.ceq(qx1, qx2) and ComplexMath.ceq(qy1, qy2):
            if n1 == n2 and m1 == m2:
                return COMPLEX_1
            else:
                return COMPLEX_0

    z1 = k_mm(n1, n2, qx1, qx2, xgamma, nr, lambda0)
    z2 = k_mm(m1, m2, qy1, qy2, -ygamma, nr, lambda0)

    return z1 * z2

cpdef void run_bayer_helms(
    complex_t[:, ::1] out,
    complex_t qx1, complex_t qy1,
    complex_t qx2, complex_t qy2,
    double xgamma, double ygamma,
    double nr_out,
    int[:, ::1] all_tem_HG,
    double lambda0,
    bint reverse_gouy
):
    """
    run_bayer_helms(out, qx1, qy1, qx2, qy2, xgamma, ygamma, nr_out, all_tem_HG, lambda0)

    Compute the knmn'm' matrix using the Bayer-Helms analytic method.

    Parameters
    ----------
    out : np.ndarray
        Array (2D) in which the coupling coefficient matrix is stored.

    qx1 : np.complex128
        Input beam parameter in tangential plane.

    qy1 : np.complex128
        Input beam parameter in sagittal plane.

    qx2 : np.complex128
        Output beam parameter in tangential plane.

    qy2 : np.complex128
        Output beam parameter in sagittal plane.

    xgamma : float
        Misalignment angle from optical axis in tangential plane.

    ygamma : float
        Misalignment angle from optical axis in sagittal plane.

    nr_out : float
        Index of refraction of output section.

    all_tem_HG : np.ndarray
        Array of HG modes to compute couplings between, formatted as
        `[(n0, m0), (n1, m1), ...]`.

    lambda0 : float
        Default wavelength of the light field, in metres.
    """
    cdef:
        Py_ssize_t N = all_tem_HG.shape[0]
        int n1, n2, m1, m2
        Py_ssize_t i, j
        complex_t knm_single

    #for i in prange(N, nogil=True):
    for i in range(N):
        n1 = all_tem_HG[i][0]
        m1 = all_tem_HG[i][1]
        for j in range(N):
            n2 = all_tem_HG[j][0]
            m2 = all_tem_HG[j][1]
            knm_single = k_nmnm(
                n1, m1, n2, m2,
                qx1, qy1, qx2, qy2,
                xgamma, ygamma,
                nr_out,
                lambda0
            )
            if reverse_gouy:
                knm_single = rev_gouy(
                    knm_single,
                    n1, m1, n2, m2,
                    qx1, qy1, qx2, qy2
                )
            out[i][j] = knm_single

cpdef void zero_tem00_phase(complex_t[:, ::1] knm_mat, complex_t[:, ::1] out=None):
    """
    zero_tem00_phase(knm_mat, all_tem_HG, turn)

    Rotates all coupling coefficients in the matrix `knm_mat` by the phase of
    the :math:`k_{0000}` coupling coefficient.

    Parameters
    ----------
    knm_mat : np.ndarray
        The array (2D) of coupling coefficients.

    all_tem_HG : np.ndarray
        Array of HG modes that the couplings were computed between, formatted as
        `[(n0, m0), (n1, m1), ...]`.

    turn : bool
        Flag indicating whether to check for the need to flip specific elements
        of the coupling coefficient matrix; set to `True` for any :class:`.Mirror`
        or :class:`.Beamsplitter` reflection couplings.

    out : np.ndarray, optional
        Optional array in which store the computed results. Defaults
        to `None` such that `knm_mat` itself is modified. Note that,
        if specified, `out.shape` must be identical to `knm_mat.shape`.
    """
    cdef:
        Py_ssize_t i, j
        complex_t offset_knm
        Py_ssize_t N = knm_mat.shape[0]
        double phase_k0000 = carg(knm_mat[0][0])

    #for i in prange(N, nogil=True):
    for i in range(N):
        for j in range(N):
            offset_knm = ComplexMath.rotate(knm_mat[i][j], -phase_k0000)

            if out is None:
                knm_mat[i][j] = offset_knm
            else:
                out[i][j] = offset_knm

cpdef void flip_odd_horizontal(complex_t[:, ::1] knm_mat, int[:, ::1] all_tem_HG,
                               complex_t[:, ::1] out=None):
    cdef:
        Py_ssize_t i, j
        int n2
        Py_ssize_t N = knm_mat.shape[0]

    for i in range(N):
        for j in range(N):
            n2 = all_tem_HG[j][0]

            if Math.msign(n2) == -1:
                if out is None:
                    knm_mat[i][j] = -knm_mat[i][j]
                else:
                    out[i][j] = -knm_mat[i][j]

cpdef void rev_all_gouy(
    complex_t[:, ::1] knm_mat,
    int[:, ::1] all_tem_HG,
    complex_t qx1, complex_t qy1,
    complex_t qx2, complex_t qy2,
    complex_t[:, ::1] out=None
):
    """
    rev_all_gouy(knm_mat, all_tem_HG, qx1, qy1, qx2, qy2, out)

    Adjust the phase of all coupling coefficients in the matrix `knm_mat` with
    respect to the Gouy phases.

    This is required for k_nmnm calculations because in Finesse the Gouy phase
    is added explicitly to the amplitude coefficients in a :class:`.Space` whereas
    the coupling coefficients are derived using a formula in which the Gouy phase
    resides in the equation for the spatial profile.

    Parameters
    ----------
    knm_mat : np.ndarray
        The matrix of coupling coefficients.

    all_tem_HG
        Array of HG modes that the couplings were computed between, formatted as
        `[(n0, m0), (n1, m1), ...]`.

    qx1 : np.complex128
        Input beam parameter in tangential plane.

    qy1 : np.complex128
        Input beam parameter in sagittal plane.

    qx2 : np.complex128
        Output beam parameter in tangential plane.

    qy2 : np.complex128
        Output beam parameter in sagittal plane.

    out : np.ndarray, optional
        Optional array in which store the computed results. Defaults
        to `None` such that `knm_mat` itself is modified. Note that,
        if specified, `out.shape` must be identical to `knm_mat.shape`.
    """
    cdef:
        Py_ssize_t i, j
        int n1, m1, n2, m2
        complex_t gouy_rev_knm
        Py_ssize_t N = knm_mat.shape[0]

    #for i in prange(N, nogil=True):
    for i in range(N):
        n1 = all_tem_HG[i][0]
        m1 = all_tem_HG[i][1]
        for j in range(N):
            n2 = all_tem_HG[j][0]
            m2 = all_tem_HG[j][1]

            gouy_rev_knm = rev_gouy(
                knm_mat[i][j],
                n1, m1, n2, m2,
                qx1, qy1, qx2, qy2
            )

            if out is None:
                knm_mat[i][j] = gouy_rev_knm
            else:
                out[i][j] = gouy_rev_knm

cpdef void knm_loss(
    complex_t[:, ::1] knm_mat,
    double[::1] out
):
    """
    knm_loss(knm_mat, out)

    Computes total losses from each mode (n, m) to all
    (ni, mi) coupling. Note this function requires that
    the array underlying the view `out` be initialised
    with all ones.

    Parameters
    ----------
    knm_mat : np.ndarray
        The matrix of coupling coefficients.

    out : np.ndarray
        The output array of coupling losses.
    """
    cdef:
        Py_ssize_t i, j
        Py_ssize_t N = knm_mat.shape[0]

    for i in range(N):
        for j in range(N):
            out[i] -= ComplexMath.norm(knm_mat[i][j])

def log_knm_matrix(
    complex_t[:, ::1] knm,
    int[:, ::1] homs,
    unicode pre_string,
    int[:, ::1] couplings=None
):
    cdef:
        Py_ssize_t i, j, k
        int n1, m1, n2, m2, nc1, mc1, nc2, mc2
        Py_ssize_t N = homs.shape[0]

    log_string = pre_string

    for i in range(N):
        n1 = homs[i][0]
        m1 = homs[i][1]
        for j in range(N):
            n2 = homs[j][0]
            m2 = homs[j][1]

            if couplings is None:
                log_string += f"\telement [{i}][{j}]: coupling {n1} {m1} -> {n2} {m2} = {knm[i][j]}\n"
            else:
                for k in range(couplings.shape[0]):
                    nc1 = couplings[k][0]
                    mc1 = couplings[k][1]
                    nc2 = couplings[k][2]
                    mc2 = couplings[k][3]
                    if n1 == nc1 and m1 == mc1 and n2 == nc2 and m2 == mc2:
                        log_string += f"\telement [{i}][{j}]: coupling {n1} {m1} -> {n2} {m2} = {knm[i][j]}\n"
                        break

    return log_string
