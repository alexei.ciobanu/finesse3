'''
Random collection of operator related classes and methods
'''

from numbers import Number
import copy
import numpy as np

from finesse.fun_lib.planewave_operator import planewave_operator
from finesse.fun_lib.LCT_funs import LCT_operator
from finesse.fun_lib.HG_operator import HG_operator

import finesse.fun_lib.graph_funs as gf

class DiGraphOperator(object):
    '''
    * Base class for DiGraph operators.
    * Acting with a DiGraphOperator on any other operator should just
    change the node connections.
    '''
    def __init__(self, node1=None, node2=None):
        self.node1 = node1
        self.node2 = node2

    def __matmul__(self, other, copy_target=None):
        if self.node1 != other.node2:
            raise Exception(f"node {self.node1} doesn't match node {other.node2}")
        else:
            if copy_target is None:
                copy_target = other
            op = copy.copy(copy_target) # avoiding potential side effects
            op.node2 = self.node2
            return op

class AbstractOperator(DiGraphOperator):
    '''
    * Abstract operators are any operators that cannot be built
    without a context.

    * Common examples are the Identity and Null operators.

    * Acting with them and adding them should still be defined.
    
    * Acting an abstract operator on a concrete operator should
    result in a concrete operator of the same type. Addition is
    lazy and resolved at build time.
    '''
    def __init__(self, scalar=None, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)
        self._scalar = scalar

    @property
    def scalar(self):
        return self._scalar

    def __matmul__(self, other):
        if isinstance(other, DiGraphOperator):
            op = super().__matmul__(other)
            op._scalar *= self._scalar
            return op
        elif isinstance(other, OperatorSumList):
            op_list = gf.recursive_map(lambda x: self@x, other)
            return OperatorSumList(op_list)

    def __add__(self, other):
        '''
        __add__ doesn't enforce types to allow the SumList to
        do type conversions at build time.
        '''
        if isinstance(other, DiGraphOperator):
            if self.node1 != other.node1:
                raise Exception(f"initial node {self.node1} doesn't \
                    match initial node {other.node1}")
            if self.node2 != other.node2:
                raise Exception(f"final node {self.node2} doesn't match final node {other.node2}")
            op_list = [self, other]
            return OperatorSumList(op_list)
        elif isinstance(other, OperatorSumList):
            # this way does the node checks for free
            op_list = (self + other[0]) + other[1:]

class IdentityOperator(AbstractOperator):
    '''
    Mostly used for debugging.
    '''
    def __init__(self, node1=None, node2=None):
        super().__init__(scalar=1, node1=node1, node2=node2)

class NullOperator(AbstractOperator):
    '''
    * Very useful in bookkeeping.
    '''
    def __init__(self, node1=None, node2=None):
        super().__init__(scalar=0, node1=node1, node2=node2)

class ConcreteOperator(DiGraphOperator):
    '''
    * Concrete operators are any operators that can be built.
    * Concrete operators can only act on other concrete operators
    or abstract operators.
    * Addition between operators is lazy and resolved at build
    time.
    * This class is intended to be subclassed but can be instantiated for
    a scalar operator that just returns its scalar value when built.
    '''
    def __init__(self, scalar=None, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)
        self._scalar = scalar

    @property
    def scalar(self):
        return self._scalar

    def build(self):
        return self.scalar

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = self
        if isinstance(other, AbstractOperator):
            op = super().__matmul__(other, copy_target=copy_target)
            op._scalar *= other.scalar
            return op
        elif isinstance(other, self.__class__):
            op = super().__matmul__(other, copy_target=copy_target)
            op._scalar *= other.scalar
            return op
        elif isinstance(other, OperatorSumList):
            op_list = gf.recursive_map(lambda x: self@x, other)
            return OperatorSumList(op_list)
        else:
            raise Exception(f"action of operator {other} not defined on operator {self}")

class OperatorSumList(list):
    '''
    * Class for containing list pf lazily summed operators.

    * The summing is performed on build.

    * For now unsure if it needs any other methods. (might
    need to add two lists together)

    * The list also contains the context (type) of the operators
    inside it. This is used to coerce any abstract operators
    in the list to their concrete counterpart.

    * The list should check types of operators added to it and
    keep track of the context. Once the context is concrete it
    should not allow other concrete operators in the list and adding
    an abstract operator should not change the context.
    '''
    def __init__(self, iterable=(NullOperator(),), context=AbstractOperator):
        super().__init__(iterable)
        self._context = context

    def build(self):
        out = self[0].build()
        for op in self[1:]:
            out += op.build()
        return out

class OperatorMatrix(np.ndarray):
    '''
    Needs to provide context for building operators
    '''
    def __init__(self):
        super().__init__(self)
        pass

def identity_operator(op_type='planewave',node1=None,node2=None,nodes=None):
    if op_type is 'planewave':
        return planewave_operator(node1=node1,node2=node2,nodes=nodes)
    if op_type is 'LCT':
        raise NotImplementedError
    if op_type is 'HG':
        raise NotImplementedError

def null_operator(op_type='planewave',node1=None,node2=None,nodes=None):
    if op_type is 'planewave':
        return 0 * planewave_operator(node1=node1,node2=node2,nodes=nodes)
    if op_type is 'LCT':
        return 0 * LCT_operator()
    if op_type is 'HG':
        raise NotImplementedError