import numpy as np

from .optics_funs import q_propag, BH_knm

class HG_operator:
    def __init__(self,Mx=None,My=None,qx=None,qy=None,lam=1064e-9,sum_list=None):
        self.Mx = Mx
        self.My = My
        self.lam = lam
        self.scalar = 1
        self.qx = qx
        self.qy = qy
        if sum_list is None:
            self.sum_list = [self]
        
    def __matmul__(self,other):
        assert(type(self) is type(other))
        Op = HG_operator()
        Op.Mx = self.Mx@other.Mx
        Op.My = self.My@other.My
        Op.qx = self.qx
        Op.qy = self.qy
        Op.scalar = self.scalar*other.scalar
        return Op

    def __add__(self,other):
        assert(type(self) is type(other))
        # when HG operators are added they should always start from the same node
        assert(self.qx == other.qx)
        assert(self.qy == other.qy)
        Op = HG_operator(sum_list=self.sum_list+[other])
        Op.qx = self.qx
        Op.qy = self.qy
        return Op
            
    def __mul__(self,other):
        self.scalar *= other
        return self
             
    def __rmul__(self,other):
        return self.__mul__(other)
            
    def build(self,maxtem=2):
        q1x = self.qx
        q1y = self.qy
        q2x = q_propag(self.Mx,q1x)
        q2y = q_propag(self.My,q1y)
        D = self.scalar * BH_knm(q1x,q1y,q2x,q2y,maxtem=maxtem)
        return D