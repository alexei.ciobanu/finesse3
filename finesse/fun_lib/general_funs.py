import numpy as np
import scipy
import pandas as pd
import matplotlib.pyplot as plt
import scipy.integrate as intgr
import scipy.special
import itertools

class mytime:
    '''
    A small library for converting between HH:MM:SS and seconds and 
    other useful manipulations. Made it for adding up hours on 
    timesheets.
    '''
    def __init__(self):
	    pass
	    
    def t2s(h=0,m=0,s=0):
        ''' HH:MM:SS to seconds '''
        return h*60*60 + m*60 + s

    def s2t(s):
        ''' seconds to HH:MM:SS '''
        s_rem = s
    #     d = s_rem//(24*60*60)
    #     s_rem = s%(24*60*60)
        h = s_rem//(60*60)
        s_rem = s_rem%(60*60)
        m = s_rem//60
        s_rem = s_rem%(60)
        return (h,m,s_rem)

    def td(t1,t2):
        '''
        h1=[14,14,10,10,12,14,17,10,14,10,11,16,10,14,16]
        m1=[45,50,10,25,0,20,55,20,10,0,0,40,50,35,30]

        h2=[16,16,11,11,13,17,18,13,17,10,13,18,13,15,18]
        m2=[20,15,0,45,15,25,45,15,25,30,25,40,30,20,40]
        
        acc_s = 0
        acc_t = 0
        for i in range(len(h1)):
            t = td([h1[i],m1[i]],[h2[i],m2[i]])
            acc_s += np.array(t2s(*t))
            acc_t += np.array(t)
            print(t,acc_s,s2t(acc_s))
        print(acc_t,s2t(acc_s))
        '''
        return s2t(abs(t2s(*t1)-t2s(*t2)))

def nd_argmax(array):
    return np.unravel_index(array.argmax(), array.shape)

def binary_search(search_limits,test_fun,verbose=False):
    # test_fun has to return [True,...,True,False,...,False]
    # on the search range, where the binary search will find the
    # last interger in the search range for which test_fun returns
    # true, if it's all True then it will return the upper search 
    # limit and lower limit if it's all False
    L,R = search_limits
    while True:
        m = np.floor((L+R)/2).astype(int)
        if verbose:
            print(L,R,m)
        if L > R:
            return m
        else:
            if test_fun(m):
                L = m + 1
            else:
                R = m - 1 

def append_zip(zipped_list,new_list):
    # unzip zipped_list and pass the individual lists 
    # as a tuple into zip() together with the new list
    unzipped_list = zip(*zipped_list)
    return zip(*unzipped_list,new_list)

def unzip_selector(zipped_list,index):
    unzip = zip(*zipped_list)
    return unzip[index]
    
def ring_append(ring_arr,apnd_array):
    '''Append an nd_array to another nd_array in a
    ring buffer fashion'''
    N = apnd_array.size
    assert(N < ring_arr.size)
    rold_arr = np.roll(ring_arr,-N)
    rold_arr[-N:] = apnd_array
    return rold_arr
    
def polyfit2d(x, y, z, order=3, mode_list=None):
    # x :: (1,N) array
    # y :: (1,M) array
    # z :: (1,N*M) array
    # m :: (1,(order+1)**2) array
    
    if mode_list is None:
        ij = list(itertools.product(range(order+1), range(order+1)))
    else:
        ij = mode_list
        
    ncols = np.shape(np.array(ij))[0]
    
    G = np.zeros((z.size, ncols))
    for k, (i,j) in enumerate(ij):
        G[:,k] = np.multiply.outer(x**i,y**j).ravel()
        
    #print(G.shape,z.shape)
    m, a, b, c = np.linalg.lstsq(G, z, rcond=-1)
    return m

def polyval2d(x, y, m, mode_list=None):
    # x :: (1,N) array
    # y :: (1,M) array
    # m :: (1,(order+1)**2) array
    # z :: (1,N*M) array
    order = int(np.sqrt(len(m))) - 1
    
    if mode_list is None:
        ij = list(itertools.product(range(order+1), range(order+1)))
    else:
        ij = mode_list
    
    z = np.zeros_like(x)
    for a, (i,j) in zip(m, ij):
        z += a * x**i * y**j
    return z
    
def polyfit3d(x, y, z, f, order=3):
    # x :: (1,N) array
    # y :: (1,M) array
    # z :: (1,N*M) array
    # m :: (1,(order+1)**2) array
    ncols = (order + 1)**3
    G = np.zeros((x.size, ncols))
    ijk = itertools.product(range(order+1), range(order+1), range(order+1))
    for n, (i,j,k) in enumerate(ijk):
        G[:,n] = x**i * y**j * z**k
    m, _, _, _ = np.linalg.lstsq(G, f, rcond=-1)
    return m
    
def polyval3d(x, y, z, m):
    # x :: (1,N) array
    # y :: (1,M) array
    # m :: (1,(order+1)**2) array
    # z :: (1,N*M) array
    order = int(np.sqrt(len(m))) - 1
    ijk = itertools.product(range(order+1), range(order+1), range(order+1))
    f = np.zeros_like(x)
    for a, (i,j,k) in zip(m, ijk):
        f += a * x**i * y**j * z**k
    return f
    
def inplace_print(*args):
    string = ''
    for arg in args:
        string += str(arg) + ' '
    print(string,'\r',end='')
    return
    
############################### FILTER STUFF #######################################



####################################################################################

def apply_filt(u,v):
    return np.real(np.fft.ifft(np.fft.fft(u)*v))

def rect(x,mu=0,w=1):
	r = np.ones_like(x)
	r[x < mu - w] = 0
	r[x > mu + w] = 0
	return r
	
def jinc(x):
    if not x:
        return 1.0
    else:
        return 2*scipy.special.j1(x)/x
jinc = np.vectorize(jinc)

def lorentzian(fwhm,x,x0=0):
    '''maximum of lorentzian is 2/(pi*fwhm)'''
    return 0.5/np.pi*fwhm/((x-x0)**2+(0.5*fwhm)**2)
    
def lorentzian_fft(fwhm,x,x0=0):
    '''maximum of lorentzian is 2/(pi*fwhm)'''
    dx = x[1]-x[0]
    xf = np.fft.fftfreq(len(x))
    l_fft = np.exp(-np.abs(xf)*(fwhm/dx)*np.pi) * np.exp(-1j*(x0/dx)*(xf*2*np.pi) )
    return np.real(np.fft.ifft(l_fft))
    
def lorentzian2(fwhm,x,x0=0):
    '''maximum of lorentzian is 1.0'''
    return (fwhm/2)**2/((x-x0)**2+(fwhm/2)**2)
    
def airy(x,fwhm,fsr,x0):
    '''maximum of airy is 2/(pi*fwhm)'''
    return 0.5/np.pi*fwhm/(np.sin((x-x0)*np.pi/fsr)**2+(0.5*fwhm)**2)

def airy2(x,fwhm,fsr,x0):
    '''maximum of airy is 1.0'''
    return (1/4)*fwhm**2/(np.sin((x-x0)*np.pi/fsr)**2+(0.5*fwhm)**2)

def gaussian_deriv_poly(x,order):
    if order%2 == 0:
        return np.polynomial.hermite_e.hermeval(x,np.r_[np.zeros(order),1])
    else:
        return np.polynomial.hermite_e.hermeval(x,np.r_[np.zeros(order),-1])
    
def gaussian(x,mu=0,sigma=1,order=0):
    return 1/(sigma*np.sqrt(2*np.pi)) * np.exp(-0.5*((x-mu)/sigma)**2) * gaussian_deriv_poly(x,order)

def gaussian2(x,mu=0,sigma=1,order=0):
    return np.exp(-0.5*((x-mu)/sigma)**2) * gaussian_deriv_poly(x,order)

def log_gaussian(x,mu,sigma):
    return -0.5*((x-mu)/sigma)**2 - 0.5*np.log(2*np.pi*sigma**2)
    
def log_likelihood(x,mu,sigma):
    n1 = len(x)
    n2 = len(mu)
    assert n1 == n2
    return -0.5/sigma**2*np.sum((x-mu)**2) - 0.5*n1*np.log(2*np.pi*sigma**2)
    
def lorentzian_filt(fwhm,N):
    # if used as a conv filter then the fft spectrum is 
    # exp(-fwhm/(1*N/pi) * range(N))
    x = np.arange(N)
    x0 = N/2
    return lorentzian(fwhm,x,x0)
    
def sinc_filt(b,N):
    # if used as a conv filter then the fft spectrum is 
    # rect(N/b) ie b is the fraction of the frequencies kept
    x = np.arange(N)-N/2
    return 1/b * np.sinc(x/b)
    
def myconv(u,v,mode='same'):
    # numpy.convolve is time-domain for some stupid reason
    u_fft = np.fft.fft(u)
    v_fft = np.fft.fft(v)
    return np.fft.fftshift(np.fft.ifft(u_fft*v_fft))
    
def myconv2(u,v,mode='same'):
    # numpy.convolve is time-domain for some stupid reason
    u_fft = np.fft.fft2(u)
    v_fft = np.fft.fft2(v)
    return np.fft.fftshift(np.fft.ifft2(u_fft*v_fft))   
    
def mycorr(u,v,mode='same'):
    # numpy.convolve is time-domain for some stupid reason
    u_fft = np.fft.fft(u)
    v_fft = np.fft.fft(v)
    return np.fft.fftshift(np.fft.ifft(u_fft*np.conj(v_fft)))
    
# def myconv(u,v):
    # return scipy.convolve(u,v,'valid')
    
def hann_wn(N):
    n = np.arange(N)
    return 0.5 - 0.5*np.cos(2*np.pi*n/(N-1))
    
def hamming_wn(N):
    alpha = 25/46
    beta = 1 - alpha
    n = np.arange(N)
    return alpha - beta*np.cos(2*np.pi*n/(N-1))
    
def sinc_filt_fiir(b,N,M=100,wn_fun=hamming_wn):
    # if used as a conv filter then the fft spectrum is 
    # rect(N/b) ie b is the fraction of the frequencies kept
    full_filt = np.zeros(N)
    wn = wn_fun(M)
    filt = sinc_filt(b,M)*wn
    filt = filt / np.sum(filt) # renormalize filter for unity gain
    full_filt[0:M] = filt
    return np.roll(full_filt,np.ceil(N/2-M/2).astype(int))

def brick_lowpass(signal,b=30,M=100,wn_fun=hamming_wn,plot_response=False,remove_edge=True):
    '''
    b is the fraction of the low frequencies kept
    1 keeps all freqs, 1/2, keeps half of freqs, 1/10 keeps a tenth ...
    M is the length of the filter as M approaches N the filter approaches an ideal brick_lowpass
    in practice M should be in range of [10,100]
    '''
    N = len(signal)
    full_filt = sinc_filt_fiir(b,N,M,wn_fun)
    if plot_response:
        plt.figure()
        plt.semilogy(np.abs(np.fft.fft(full_filt))[0:N//2])
        plt.show()
    
    return myconv(signal,full_filt)
    
####### GEOMETRY STUFF #######
    
def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)    
    
def norm_vec(v):
    return v/np.sqrt(np.sum(v**2))

def gen_rand_norm_vec():
    r = np.random.rand(3)-0.5
    return norm_vec(r)
    
def gen_rand_norm_mat():
    # random matrix where every column is normalized
    r1 = gen_rand_norm_vec()
    r2 = gen_rand_norm_vec()
    r3 = gen_rand_norm_vec()
    return np.vstack([r1,r2,r3]).T

def gen_rand_rot_mat():
    # random matrix where every colum is normalized and 
    # orthogonal to each other
    v1 = gen_rand_norm_vec()
    
    r2 = gen_rand_norm_vec()    
    # subtract off the component of r2 parallel to v1
    v2 = norm_vec(r2 - r2.dot(v1)*v1)
    
    # generate the last vector via cross product
    v3 = np.cross(v1,v2)
    
    return np.vstack([v1,v2,v3]).T
    
def solve_for_euler_angles(R):
    # Follows R = R_z(phi)*R_y(theta)*R_x(psi) convention.
    # Note that there are always two sets of euler angles for
    # any non-degenerate (cos(theta) != 0) rotation matrix.
    # Code taken from Slabaugh's article
    if abs(R[2,0]) != 1:
        theta1 = - np.arcsin(R[2,0])
        theta2 = np.pi - theta1
        psi1 = np.arctan2(R[2,1]/np.cos(theta1),R[2,2]/np.cos(theta1))
        psi2 = np.arctan2(R[2,1]/np.cos(theta2),R[2,2]/np.cos(theta2))
        phi1 = np.arctan2(R[1,0]/np.cos(theta1),R[0,0]/np.cos(theta1))
        phi2 = np.arctan2(R[1,0]/np.cos(theta2),R[0,0]/np.cos(theta2))
    else:
        # abs(R[2,0]) == 1 -> degenerate case
        phi = 0 # could be anything
        if R[2,0] == -1:
            theta = np.pi/2
            psi = phi + np.arctan2(R[0,1],R[0,2])
        else:
            theta = - np.pi/2
            psi = -phi + np.arctan2(-R[0,1],-R[0,2])
            
    return (psi1,theta1,phi1)
    
def R2(theta):
    '''
    2D rotation matrix
    '''
    return np.matrix([\
                    [np.cos(theta),-np.sin(theta)],\
                    [np.sin(theta),np.cos(theta)]\
                    ])
    
def R_x(psi):
    return np.matrix([\
                     [1,0,0],\
                     [0,np.cos(psi),-np.sin(psi)],\
                     [0,np.sin(psi),np.cos(psi)]\
                    ])
def R_y(theta):
    return np.matrix([\
                     [np.cos(theta),0,np.sin(theta)],\
                     [0,1,0],\
                     [-np.sin(theta),0,np.cos(theta)]\
                    ])

def R_z(phi):
    return np.matrix([\
                     [np.cos(phi),-np.sin(phi),0],\
                     [np.sin(phi),np.cos(phi),0],\
                     [0,0,1]\
                    ])
                    
#######################################################################################################################
# MATPLOTLIB FUNS

def get_xy_from_fig(fig):
    '''
    output dimensions : [x/y, point]
    '''
    ax = fig.axes[0]
    line = ax.get_lines()[0]
    return line.get_xydata().T
    
def get_xy_from_fig_deep(fig):
    '''
    output dimensions : [subplot, line, x/y, point]
    '''
    out = []
    axs = fig.axes
    for ax in axs:
        lines = ax.get_lines()
        temp = []
        for line in lines:
            temp.append(line.get_xydata().T)
        out.append(temp)    
    return out
