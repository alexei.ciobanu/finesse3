import numpy as np

class LCT_operator:
    def __init__(self,x1s=None,x2s=None,M=None,type=None,lam=1064e-9,op_list=None):
        self.M = M
        self.x1s = x1s
        self.x2s = x2s
        self.lam = lam
        self.scalar = 1
        if op_list is None:
            self.type = type
            self.op_list = [self]
        elif op_list == []:
            self.type = 'LCT'
            self.M = LCT_space(0)
        else:
            self.type = 'compound'
            self.op_list = op_list
        
    def __matmul__(self,other):
        assert(type(self) is type(other))
        if self.type == other.type == 'LCT':
            Op = LCT_operator(type='LCT')
            Op.M = self.M@other.M
            Op.scalar = self.scalar*other.scalar
            return Op
        else:
            Op = LCT_operator(type='compound')
            # print(self.op_list[-1].type,other.op_list[0].type)
            if self.op_list[-1].type == other.op_list[0].type == 'LCT':
                Op.op_list = self.op_list[:-1] + [self.op_list[-1]@other.op_list[0]] + other.op_list[1:]
            else:
                Op.op_list = self.op_list + other.op_list
            return Op
            
    def __mul__(self,other):
        self.scalar *= other
        return self
             
    def __rmul__(self,other):
        return self.__mul__(other)
            
    def build(self,direction='x'):
        # only LCT is tested
        if self.type == 'LCT':
            return self.scalar**0.5 * LCT1D(self.x1s,self.x2s,self.M)
        elif self.type == 'map':
            return self.scalar * self.map()
        elif self.type == 'compound':
            return build(self.op_list[0]) @ build(LCT_operator(op_list=self.op_list[1:]))
        else:
            raise Exception(f"don't know how to build operator type: {self.type}")

def abcd_space(d,nr=1):
    if isinstance(d,np.ndarray):
        N = np.size(d)
        M = np.zeros([N,2,2])
        A = M[:,0,0]
        B = M[:,0,1]
        C = M[:,1,0]
        D = M[:,1,1]
        
        A.real = 1
        B.real = d/nr
        C.real = 0
        D.real = 1
        return M
    else:
        return np.array(\
                       [[1,d/nr],\
                        [0,1]]\
                       )

def abcd_lens(f):
    if isinstance(f,np.ndarray):
        N = np.size(f)
        M = np.zeros([N,2,2])
        A = M[:,0,0]
        B = M[:,0,1]
        C = M[:,1,0]
        D = M[:,1,1]
        
        A.real = 1
        B.real = 0
        C.real = -1/f
        D.real = 1
        return M
    else:
        return np.array(\
                   [[1,0],\
                    [-1/f,1]]\
                   )

def abcd_mirror(Rc,nr=1):
    if isinstance(Rc,np.ndarray):
        N = np.size(Rc)
        M = np.zeros([N,2,2])
        A = M[:,0,0]
        B = M[:,0,1]
        C = M[:,1,0]
        D = M[:,1,1]
        
        A.real = 1
        B.real = 0
        C.real = -2*nr/Rc
        D.real = 1
        return M
    else:
        return np.array(\
                   [[1,0],\
                    [-2*nr/Rc,1]]\
                   )

def abcd_general_refraction(n1,n2,Rc=None,p=None):
    assert(Rc is not None or p is not None)
    if p is None:
        p = 1/Rc
    return np.array(\
        [[1,0],\
         [(n2-n1)*p,1]]\
        )

def LCT_space(z,nr=1,lam=1064e-9):
    return abcd_space(z*lam,nr=nr)

def LCT_lens(f,lam=1064e-9):
    return abcd_lens(f*lam)
    
def LCT_mirror(Rc,nr=1,lam=1064e-9):
    return abcd_mirror(Rc*lam,nr=nr)

def LCT_general_refraction(nr1, nr2, Rc=None ,p=None, lam=1064e-9):
    assert(Rc is not None or p is not None)
    if p is None:
        p = 1/Rc
    return abcd_general_refraction(nr1,nr2,p=p/lam)

def LCT_inverse(M):
    a,b,c,d = M.flatten()
    return np.array([[d,-b],[-c,a]])
    
def QP_kernel(x,f=None,R=None,lam=1064e-9):
    if not R is None:
        f = R/2        
    f = f*lam
    
    return np.diag(np.exp(1j*np.pi/f*x**2))
        
def LCT1D(x1s,x2s,M,lam=1064e-9):
    a,b,c,d = M.flatten()
    
    if b == 0:
        assert(a == d == 1)
        if c == 0:
            return np.eye(len(x1s))
        else:
            return QP_kernel(x1s,f=-1/(c*lam))
    
    beta = 1/b
    gamma = a*beta
    alpha = d*beta
    
    N1 = np.size(x1s)
    N2 = np.size(x2s)
    
    dx1 = x1s[1] - x1s[0]
    dx2 = x2s[1] - x2s[0]
    
    DLCT = np.zeros([N2,N1],dtype=np.complex128)
    for i,x1 in enumerate(x1s):
        DLCT[:,i] = dx1*np.sqrt(beta+0j)*np.exp(1j*np.pi/4) * np.exp(-1j*np.pi*(alpha*x2s**2 - 2*beta*x1*x2s + gamma*x1**2))
        
    return DLCT

def DLCT(x1s,x2s,M,lam=1064e-9):
    return LCT1D(x1s,x2s,M,lam=1064e-9)

def F_CC_kernel(x1s,d,lam=1064e-9):
    beta = 1/(d*lam)
    
    N = x1s.size
    dx = x1s[1] - x1s[0]
    f1s = (np.fft.fftfreq(N,dx))
    k = 2*np.pi/lam
    plD = np.pi*lam*d
    diag = np.exp(1j*plD*f1s**2)
    
    kernel = np.sign(d)*np.diag(diag)
    return (kernel)