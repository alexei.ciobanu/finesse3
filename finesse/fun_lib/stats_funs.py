import numpy as np
import scipy
import pandas as pd
import matplotlib.pyplot as plt
import scipy.integrate as intgr
import itertools

import sys
import os

sys.path.append( os.path.abspath('.') )

import general_funs

def covar_to_corr(M):
    M_c = np.copy(M)
    for i in range(M.shape[0]):
        for j in range(M.shape[1]):
            M_c[j,i] = M[j,i]/(np.sqrt(M[j,j]*M[i,i]))        
    return M_c

def histogram(a,**kwargs):
    '''identical to numpy histogram but instead return the 
    center of the bins instead the edges so you can plot the bloody histogram'''
    f,bins = np.histogram(a,**kwargs)
    x = bins[1:] - (bins[1]-bins[0])/2
    return x,f
    
def kde(X,x,h=None):
    X_sort = np.sort(X)
    X_sort_grad = np.diff(X_sort)
    X_sort_grad_med = np.median(X_sort_grad)
    
    magic_constant = 0.382
    
    N = 1/(X_sort_grad_med*magic_constant)
    
    if h is None:
        h = N**(-1/5)
    elif str(h).lower() == 'silverman':
        h = 1.06*len(X)**(-1/5)
    
    phi = lambda x: general_funs.gaussian(x,0,1)
    kernel = lambda x,h: np.mean(phi((x-X)/h)/h)
    kpdf = np.vectorize(lambda x,h: kernel(x,h))
    f4 = kpdf(x,h)
    
    dx = x[1] - x[0]
    
    f4 = f4/(np.sum(f4)*dx)
    
    return f4
    
def char_fun(X,ts):
    return np.mean(np.exp(-1j*np.multiply.outer(ts,X)),axis=1)
    
def char_to_pdf(X,ts,xs, w= lambda x: np.ones(len(x))):
    char = char_fun(X,ts)
    dt = ts[1] - ts[0]
    
    pdf = 1/(2*np.pi) * np.sum( w(ts)*char*np.exp(1j*np.multiply.outer(xs,ts)),axis=1 ) * dt
    return pdf
    
def char_to_pdf_ifft(X,ts, w= lambda x: np.ones(len(x))):
    char = char_fun(X,ts)
    
    freq = np.fft.fftshift(scipy.fftpack.fftfreq(len(ts),dt)*2*np.pi)
    df = freq[1] - freq[0]
    
    pdf = np.fft.fftshift(np.abs(np.fft.ifft(w(ts)*char))) / df
    return freq,pdf
