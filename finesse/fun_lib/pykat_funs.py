import numpy as np
import optics_funs
import pykat

class pykat_types:
    # Dan claims that sticking all the types in one place is indicative of how 
    # I don't know how python "works"
    lock = pykat.commands.lock
    cav = pykat.commands.cavity
    func = pykat.commands.func
    constant = pykat.commands.Constant
    pd = pykat.detectors.pd # gets pd1s as well
    ad = pykat.detectors.ad

def pykat_gen_ad_block(node,maxtem=0,freq=0,mode_list=None):
    '''if user provides mode_list overwrite the asked maxtem'''
    s = '%%% FTblock ad_' + node + '\n'
    if mode_list is None:
        mode_list = optics_funs.mode_list2(maxtem)
    for pair in mode_list:
        n,m = pair
        ad_name = 'ad_' + node + '_' + str(int(n))+'_'+str(int(m))
        s += 'ad '+ad_name+' '+str(int(n))+' '+str(int(m))+' ' + str(freq) + ' '+node+' \n'
    s += '%%% FTend ad_' + node + '\n'
    return s
    
def pykat_gen_TEM_block(input_name,df_H,maxtem):
    s = '%%% FTblock' + input_name + '_HOM\n'
    for i in range(len(df_H)):
        p,d,n,m = df_H.loc[i]
        if n+m <= maxtem:
            s += 'tem '+input_name+' '+str(int(n))+' '+str(int(m))+' '+str(p)+' '+str(d)+' \n'
    s += '%%% FTend HOM\n'
    return s
    
def pykat_BH_TEM(input_name, q1, q2, dx=0, dy=0, gammax=0, gammay=0, maxtem=10):
    H = optics_funs.BH_DHT(q1, q2, dx=dx, dy=dy, gammax=gammax, gammay=gammay, maxtem=maxtem)
    df_H = optics_funs.parse_DHT(H,sortby='n,m',power=True)
    df_H['n+m'] = df_H['n']+df_H['m']
    df_H = df_H.loc[df_H['n+m'] <= maxtem]
    df_H = df_H.drop('n+m', axis=1).reset_index(drop=True)
    return pykat_gen_TEM_block(input_name,df_H,maxtem=maxtem)

def pykat_recover_H(kat,out,node,maxtem):
    # only works in yaxis re:im
    # only works with the gen_ad_block
    kat=kat.deepcopy()
    det_list = kat.getAll(pykat.detectors.BaseDetector,'name')
    det_list = [det for det in det_list if 'ad_'+node in det]
    
#     maxtem = max([int(det[-1]) for det in det_list])
    
    H = np.zeros([maxtem+1,maxtem+1],dtype=np.complex128)
    
    for det in det_list:
        n,m = det[-2:]
        n = int(n)
        m = int(m)
        H[n,m] = out[det]
        
    return H
    
def compute_cav_params(kat):
    # computes all the params that you can from a cav command
    # ie eigmode, rt guoy, finesse, fsr 

    import pandas as pd
    
    kat = kat.deepcopy()
    kat.verbose = False
    kat.noxaxis = True
    kat.removeBlock('locks',False)
#     kat.maxtem = 0
    
    cavs = kat.getAll(pykat.commands.cavity)
    
    cp_cmds = ''
    for cav in cavs:
        cp_cmds += '''cp {0}_x_g {0} x stability
                    cp {0}_y_g {0} y stability
                    cp {0}_x_B {0} x B
                    cp {0}_y_B {0} y B
                    cp {0}_x_fsr {0} x fsr
                    cp {0}_y_fsr {0} y fsr
                    cp {0}_x_q {0} x q
                    cp {0}_y_q {0} y q
                    cp {0}_x_fwhm {0} x fwhm
                    cp {0}_y_fwhm {0} y fwhm
                    cp {0}_x_finesse {0} x finesse
                    cp {0}_y_finesse {0} y finesse
                    
                    yaxis re:im
                    '''.format(cav)
        
    kat.parse(cp_cmds)
    
    out = kat.run()
    
#     return kat.getAll(pykat.detectors.BaseDetector,'name')

    gs = []
    Bs = []
    psi_rts = []
    FSRs = []
    fwhms = []
    finesses = []
    qs = []
    delta_fs = []
    cav_names = []
    append_ax = lambda name: [name + '_x', name + '_y']
    for cav in cavs:

        g_x = np.abs((out[cav.name+'_x_g']+1)/2)
        B_x = np.abs(out[cav.name+'_x_B'])
        psi_rt_x = np.abs(2*np.arccos(np.sign(B_x)*np.sqrt(g_x)))
        FSR_x = np.abs(out[cav.name+'_x_fsr'])
        fwhm_x = np.abs(out[cav.name+'_x_fwhm'])
        finesse_x  = np.abs(out[cav.name+'_x_finesse'])
        q_x = out[cav.name+'_x_q']
        
        g_y = np.abs((out[cav.name+'_y_g']+1)/2)
        B_y = np.abs(out[cav.name+'_y_B'])
        psi_rt_y = np.abs(2*np.arccos(np.sign(B_y)*np.sqrt(g_y)))
        FSR_y = np.abs(out[cav.name+'_y_fsr'])
        fwhm_y = np.abs(out[cav.name+'_y_fwhm'])
        finesse_y  = np.abs(out[cav.name+'_y_finesse'])
        q_y = out[cav.name+'_y_q']
        
        if np.abs(psi_rt_x/(2*np.pi)) > 1/2:
            delta_f_x = psi_rt_x/(2*np.pi) * FSR_x - FSR_x*np.sign(psi_rt_x)
        else:
            delta_f_x = psi_rt_x/(2*np.pi) * FSR_x
            
        if np.abs(psi_rt_y/(2*np.pi)) > 1/2:
            delta_f_y = psi_rt_y/(2*np.pi) * FSR_y - FSR_y*np.sign(psi_rt_y)
        else:
            delta_f_y = psi_rt_y/(2*np.pi) * FSR_y
            
        gs.extend([g_x,g_y])
        Bs.extend([B_x, B_y])
        psi_rts.extend([psi_rt_x, psi_rt_y])
        FSRs.extend([FSR_x, FSR_y])
        delta_fs.extend([delta_f_x, delta_f_y])
        finesses.extend([finesse_x,finesse_y])
        qs.extend([q_x,q_y])
        fwhms.extend([fwhm_x,fwhm_y])
        cav_names.extend(append_ax(cav.name))
    
    # return pd.DataFrame({'g': [g_x, g_y], 'B': [B_x, B_y], \
                         # 'psi_rt': [psi_rt_x, psi_rt_y], 'FSR': np.array([FSR_x, FSR_y])\
                         # , 'delta_f': [delta_f_x, delta_f_y] , 'q': [q_x,q_y], 'fwhm': [fwhm_x,fwhm_y]})
                         
    return pd.DataFrame({'cav': cav_names, 'g': gs, 'B': Bs, \
                 'psi_rt': psi_rts, 'FSR': FSRs\
                 , 'delta_f': delta_fs , 'q': qs, 'fwhm': fwhms, 'finesse': finesses})
                 
                 
###############################################################################
#
# Lock Dragging stuff
#
###############################################################################

def get_arm_powers(_kat):
    '''
    just sticks two circ PDs and spits out their reading
    '''
    kat = _kat.deepcopy()
    kat.removeBlock('locks')
    kat.verbose=False
    kat.noxaxis=True
    
    kat.parse('''
    pd Y nETMY1
    pd X nETMX1
    
    yaxis abs
    noxaxis
    ''')
    
    out = kat.run()
    return out['X'],out['Y']

def run_locks(_kat,verbose = False):
    '''assumes there is a locks block in _kat'''
    kat = _kat.deepcopy()
    kat.noxaxis = True
    kat.verbose = False
    
    old_tunings = list(kat.IFO.get_tunings().values())
    
    if verbose:
        print(('Old tunings | ' +'{: 1.3e} | '*7).format(*old_tunings))
    
    out = kat.run()
    kat.IFO.apply_lock_feedback(out)
    
    new_tunings = list(kat.IFO.get_tunings().values())
    
    if verbose:
        print(('New tunings | ' +'{: 1.3e} | '*7).format(*new_tunings))
        
    return kat

def ffs(val,prefix=''):
    '''
    Finesse fix sign. 
    Used to compute algebraic sign flips on input variables
    to the finesse function parser.
    
    val : value to feed into parser as a function constant
    prefix : 
    '''
    if val < 0:
        if prefix == '':
            s = f'(-1)*{abs(val)}'
        if prefix == '+':
            s = f'- {abs(val)}'
        if prefix == '-':
            s = f'+ {abs(val)}'
    else:
        if prefix == '':
            s = f'{abs(val)}'
        if prefix == '+':
            s = f'+ {abs(val)}'
        if prefix == '-':
            s = f'- {abs(val)}'
    
    # if val was printed in exp form the e needs to
    # be capitalised for the parser to understand
    s = s.replace('e','E')
            
    return s

def optimize_demod_phase(_kat,dofs=['CARM', 'PRCL', 'MICH', 'SRCL'],verbose = True):
    '''
    returns a kat object with optimized demod phases
    
    _kat : assumed to be an kat IFO object with fully 
    implemented DOFs
    '''

    base = _kat.deepcopy()

    if verbose:
        print('DOF  | old phase | new phase')
        print('-'*30)

    for dof in dofs:
        kat = base.deepcopy()
        dof = kat.IFO.DOFs[dof]
        kat.noxaxis = True
        kat.removeBlock('locks')
        kat.parse( dof.fsig(fsig=1) )
        kat.parse( dof.transfer() )
        code = f"""
        maximize max {dof.transfer_name()} re {dof.transfer_name()} phase1 -1000 1000 1e-3
        """
        kat.parse(code)
        out = kat.run()

        dof.port.phase = out['max'] % 360 - 180
        if dof.quad == 'Q':
            dof.port.phase += 90
            
        old_phase = base.IFO.DOFs[dof.name].port.phase
        new_phase = dof.port.phase
        
        if verbose:
            print(f"{dof.name:4s} |  {old_phase:8.3f} | {new_phase:8.3f}")
        
        # apply the demod phase
        base.IFO.DOFs[dof.name].port.phase = dof.port.phase
        
#         need to regenerate errsigs
        errsigs_cmds = base.IFO.add_errsigs_block()

    return base

def set_components(_kat,compnts,attrs,final_state,ld_out):
    kat = _kat.deepcopy()
    
    kat = load_state(kat,ld_out,-1)
    
    # get initial state of relevant params and add their func blocks
    for compnt,attr,final_val in zip(compnts,attrs,final_state):      
        setattr(kat.components[compnt],attr,final_val)        
    
    return kat

def load_state(_kat,out,components,attributes,ind):
    '''
    Recovers a model state at any point during a lock drag.
    Useful for troubleshooting lock drags. Made with 
    lock_drag_2 in mind.
    
    _kat : initial _kat fed into lock drag
    out  : final run file from the lock drag
    components : same as what went into lock drag
    attributes : same as what went into lock drag
    ind : which index from the lock drag state to load
    '''
    kat = _kat.deepcopy()
    
    old_tunings = kat.IFO.get_tunings()
    old_tunings['PRM']  +=  out['PRM_lock'][ind]
    old_tunings['ITMX'] +=  out['ITMX_lock'][ind]
    old_tunings['ETMX'] +=  out['ETMX_lock'][ind]
    old_tunings['ITMY'] +=  out['ITMY_lock'][ind]
    old_tunings['ETMY'] +=  out['ETMY_lock'][ind]
    old_tunings['SRM']  +=  out['SRM_lock'][ind]
    kat.IFO.apply_tunings(old_tunings)
    
    for c,a in zip(components,attributes):
        set_val = out[f'f_{c}_{a}'][ind]
        setattr(getattr(kat,c),a,set_val)
    
    return kat

def run_lock_drag_2(_kat,compnts,attrs,final_state,n_steps=30,step_exponent=1,debug=False):
    '''
    _kat : kat object to perform the lock drag on. Initial param values 
    will be pulled from here. Assume kat object has errsigs and locks. 
    
    compnts : list of finesse components to include in lock drag. If several 
    attributes of the same component have to be varied the component name 
    has to be duplicated for each extra attribute
    
    attrs : list of finesse attributes to alter. These attributes correspond to
    the component from the compnts list
    
    n_steps : number of steps to take to go from initial params to final 
    params.
    
    step_exponent : > 1 will make the steps finer at the beginning and larger at the end
    < 1 will make steps larger at beginning and smaller at the end
    '''
    
    kat = _kat.deepcopy()
    
    code = '''
var dummy 0
xaxis dummy re lin 0 1 {} \n'''.format(n_steps)
    
    init_params = []
    
    # get initial state of relevant params and add their func blocks
    for compnt,attr,final_val in zip(compnts,attrs,final_state):
        
        init_val = getattr(getattr(kat.components[compnt],attr),'value')
        init_params.append(init_val)
        
        code += f'\rfunc f_{compnt}_{attr} = $x1^{step_exponent} * ({ffs(final_val,"")} {ffs(init_val,"-")}) {ffs(init_val,"+")} \n'     
     
    code += '\n'
    
    # add put block
    for compnt,attr in zip(compnts,attrs):
        code += '''\rput {0} {1} $f_{0}_{1} 
'''.format(compnt,attr)
        
    kat.parse(code)
    kat.verbose=True
    
    out = kat.run()
    
    if debug:
        return code,init_params,out,kat
    else:
        return out
