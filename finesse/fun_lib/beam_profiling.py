import numpy as np
import scipy
import pandas as pd
import matplotlib.pyplot as plt
import scipy.integrate

from general_funs import nd_argmax

def gaussian(x,y,x0,y0,wx,wy):
    return np.exp(-2*np.add.outer((y-y0)**2/wy**2,(x-x0)**2/wx**2))

def alexei_beam_profile(im,init=None,w=10,old_size=None,debug=False):
    '''
    Uses combination of linear and nonlinear least squares to fit a gaussian
    starts by considering small section of image near maximum and iteratively expands to get a 3*w coverage
    '''

    Ny,Nx = np.shape(im)
    x = np.arange(Nx)
    y = np.arange(Ny)

    ymax,xmax = nd_argmax(im)
    im2 = im[ymax-w:ymax+w,xmax-w:xmax+w]

    def obj_fun(theta,im):
        Ny,Nx = np.shape(im)
        x = np.arange(Nx)
        y = np.arange(Ny)
        x0,y0,wx,wy = theta

        model = gaussian(x,y,x0,y0,wx,wy)

        N = Nx*Ny

        G = np.zeros([N,1])
        G[:,0] = model.ravel()

        phi,_,_,_ = np.linalg.lstsq(G,im.ravel(),rcond=None)

        a = phi

        res = np.sum((a*model - im)**2)

        return res

    if init is None:
        init = [xmax,ymax,w,w]

    theta_init = np.array(init)
    theta_init[0] += -xmax+w
    theta_init[1] += -ymax+w

    soln = scipy.optimize.minimize(obj_fun,theta_init,args=im2,method='Powell')

    wx,wy = soln.x[2:]

    new_size = np.array(wx,wy)

    if debug:
        print(new_size,old_size)

    new_init = soln.x
    new_init[0] += xmax-w
    new_init[1] += ymax-w

    if (old_size is None) or np.sum((old_size-new_size)**2) > 0.1:
        
        new_w = int(np.mean(new_size))*3
        
        return alexei_beam_profile(im,new_init,w=new_w,old_size=new_size)
    else:
        return new_init

def get_first_moment(M,order=1):
    x = np.arange(0,M.shape[1])
    y = np.arange(0,M.shape[0])
    x.shape = (1,len(x))
    y.shape = (len(y),1)
    I1 = scipy.integrate.trapz(scipy.integrate.trapz(M,axis=1))
    x_bar = scipy.integrate.trapz(scipy.integrate.trapz((M * x)))/I1
    y_bar = scipy.integrate.trapz(scipy.integrate.trapz((M * y)))/I1
    
    return np.array([x_bar,y_bar])

def get_second_moment(M,x0,y0,order=2):
    x = np.arange(0,M.shape[1])
    y = np.arange(0,M.shape[0])
    x.shape = (1,len(x))
    y.shape = (len(y),1)
    I1 = scipy.integrate.trapz(scipy.integrate.trapz(M,axis=1))
    x_bar = 2*np.sqrt(scipy.integrate.trapz(scipy.integrate.trapz(M * (x - x0)**2))/I1)
    y_bar = 2*np.sqrt(scipy.integrate.trapz(scipy.integrate.trapz(M * (y - y0)**2))/I1)
    
    return np.array([x_bar,y_bar])


def get_second_moment(M,x0,y0,order=2):
    x = np.arange(0,M.shape[1])
    y = np.arange(0,M.shape[0])
    x.shape = (1,len(x))
    y.shape = (len(y),1)
    I1 = scipy.integrate.trapz(scipy.integrate.trapz(M,axis=1))
    x_2  = scipy.integrate.trapz(scipy.integrate.trapz(M * (x - x0)**2))/I1
    y_2  = scipy.integrate.trapz(scipy.integrate.trapz(M * (y - y0)**2))/I1
    xy_bar = scipy.integrate.trapz(scipy.integrate.trapz(M * (x - x0)*(y - y0)))/I1
    
    gamma = np.sign(x_2-y_2)
    wx = np.sqrt(2)*np.sqrt(x_2+y_2+gamma*np.sqrt((x_2-y_2)**2+4*xy_bar**2))
    wy = np.sqrt(2)*np.sqrt(x_2+y_2-gamma*np.sqrt((x_2-y_2)**2+4*xy_bar**2))
    phi = 0.5 * np.arctan(2*xy_bar/(x_2-y_2))/np.pi*180
    
    return np.array([x_2,y_2,xy_bar,wx,wy,phi])
    
def iterative_d4s(M,return_cropped_im=False,verbose=1,aprt=3):
    
    M2 = np.array(M)
    yum_old,ylm_old,xum_old,xlm_old = 0,0,0,0
    ymax_old,xmax_old,wy_old,wx_old,xy_bar_old,x_2_old,y_2_old = 0,0,0,0,0,0,0
    
    for i in range(0,10):

        ymax,xmax = get_first_moment(M2) 
        y_2,x_2,xy_bar,wx_2,wy_2,phi = get_second_moment(M2,ymax,xmax)
        
        print(ymax,xmax,y_2,x_2,xy_bar,wx_2,wy_2,phi)
        
        wx = np.sqrt(x_2)*2
        wy = np.sqrt(y_2)*2
        
        tol_test = np.abs(np.min(np.array([ymax_old,xmax_old,wy_old,wx_old,xy_bar_old])-np.array([ylm_old+ymax,xlm_old+xmax,wy,wx,xy_bar])))
        if verbose >= 1:
            print(i, tol_test)
        
        if verbose >= 2:
            print(ymax,xmax,wy,wx)  
        
        if np.min(np.abs(np.array([ymax_old,xmax_old,wy_old,wx_old])-np.array([ylm_old+ymax,xlm_old+xmax,wy,wx]))) < 1e-6:
            break
            
        print(ymax,xmax,y_2,x_2,xy_bar,wx_2,wy_2,phi)
#         print(wy_2)
            
    
        ylm = int(ylm_old + ymax-wy*aprt) if int(ylm_old + ymax-wy*aprt) > 0 else 0
        yum = int(ylm_old + ymax+wy*aprt) if int(ylm_old + ymax+wy*aprt) < M.shape[1] else M.shape[1]
        xlm = int(xlm_old + xmax-wx*aprt) if int(xlm_old + xmax-wx*aprt) > 0 else 0
        xum = int(xlm_old + xmax+wx*aprt) if int(xlm_old + xmax+wx*aprt) < M.shape[0] else M.shape[0]
        
        if verbose >= 2:
            print([yum_old,ylm_old,xum_old,xlm_old],[yum,ylm,xum,xlm])

        M2 = M[xlm:xum,ylm:yum]

        yum_old,ylm_old,xum_old,xlm_old = yum,ylm,xum,xlm
        ymax_old,xmax_old,wy_old,wx_old,xy_bar_old = ylm_old+ymax,xlm_old+xmax,wy,wx,xy_bar

        if verbose >= 3:
            plt.figure()
            plt.imshow(M2)
            plt.show()
            
    if verbose >= 1:
        plt.figure(figsize=[12,4])
        plt.subplot(121)
        plt.imshow(M2)
        plt.colorbar()
        plt.subplot(122)
        
        I = scipy.integrate.trapz(scipy.integrate.trapz(M,axis=1))
        u = I * gauss_intens_2(np.arange(0,xum-xlm),np.arange(0,yum-ylm),x0=xmax,y0=ymax,wx=wx,wy=wy)
        
        print(wx*aprt*2)
        print(wy*aprt*2,ymax+wy*aprt)
        
        plt.imshow(M2 - u)
        plt.axis('scaled')
        plt.colorbar()
        plt.show()
    
    if return_cropped_im:
        return ylm_old+ymax,xlm_old+xmax,wy,wx,xy_bar,wy_2,wx_2
    else:
        return ylm_old+ymax,xlm_old+xmax,wy,wx,xy_bar,wy_2,wx_2
		
		
def iterative_d4s2(im,wx=10,wy=10,debug=0):

    im_bak = im.copy()
    M,N = np.shape(im)
    ymax,xmax = nd_argmax(im)
    ymax,xmax = np.round([ymax,xmax]).astype(int)
    
    sw = 3
    ylm = 0 if ymax-wy*sw < 0 else ymax-wy*sw
    yum = M if ymax+wy*sw > M else ymax+wy*sw
    xlm = 0 if xmax-wx*sw < 0 else xmax-wx*sw
    xum = N if xmax+wx*sw > N else xmax+wx*sw
    im2 = im[ylm:yum,xlm:xum]

    y_bar,x_bar = get_first_moment(im2)
     
    x_2,y_2,xy_bar,_,_,_ = get_second_moment(im2,y_bar,x_bar)

    old_wx,old_wy = wx,wy

    new_wx = int(np.round(np.sqrt(x_2*4)))
    new_wy = int(np.round(np.sqrt(y_2*4)))

    if debug > 0:
        print(ymax,xmax,old_wx,new_wx)

    if (old_wx == new_wx) and (old_wy == new_wy):
        if debug > 1:
            plt.figure()
            plt.imshow(im2)
            plt.show()
        y_bar,x_bar = get_first_moment(im2)
        return y_bar+ylm,x_bar+xlm,x_2,y_2,xy_bar
    else:
        return iterative_d4s2(im_bak,wx=new_wx,wy=new_wy,debug=debug)
        
def beam_profile(img,ref=None,full=False,debug=0):
    if not ref is None:
        M = img - ref
    else:
        M = img
    M = M - np.median(M)

    y_bar,x_bar,x_2,y_2,xy_bar = iterative_d4s2(M,debug=debug)

    wx = np.sqrt(x_2*4)
    wy = np.sqrt(y_2*4)
    phi = 0.5 * np.arctan(2*xy_bar/(x_2-y_2))/np.pi*180

    return y_bar,x_bar,wy,wx,phi