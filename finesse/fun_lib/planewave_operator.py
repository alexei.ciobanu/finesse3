import numpy as np
from numbers import Number

class planewave_operator(object):
    def __init__(self,scalar=None,lam=1064e-9,sum_list=None,node1=None,node2=None,nodes=None):
        self.lam = lam
        self.op_size = 1
        if nodes is None:
            self.node1 = node1
            self.node2 = node2
        else:
            self.node1 = nodes[0]
            self.node2 = nodes[1]
        if scalar is None:
            self.scalar = 1
        else: 
            self.scalar = scalar
        if sum_list is None:
            self.sum_list = [self]
        else:
            self.sum_list = sum_list
        
    def __matmul__(self,other):
        if type(other) is type(self):
            if self.node1 is not other.node2:
                pass
                #raise Exception(f"node {self.node1} doesn't match {other.node2}")
            Op = planewave_operator()
            Op.scalar = self.scalar*other.scalar
            Op.node1 = other.node1
            Op.node2 = self.node2
            return Op
        elif type(other) is type(planewave_field()):
            if other.node is not None:
                if self.node1 is not other.node:
                    pass
                    #raise Exception(f"node {self.node1} doesn't match {other.node}")
            field = planewave_field()
            field.amplitude = other.amplitude
            field.amplitude *= self.scalar
            field.node = self.node2
            return field
        else:
            raise Exception('what are you doing?')

    def __add__(self,other):
        assert(type(self) is type(other))
        Op = planewave_operator(sum_list=self.sum_list+[other])
        return Op
            
    def __mul__(self,other):
        if isinstance(other,planewave_field):
            return self@other
        elif isinstance(other,Number):
            self.scalar *= other
            return self
        else:
            raise Exception('undefined')
             
    def __rmul__(self,other):
        return self.__mul__(other)
            
    def build(self):
        D = 0
        for Op in self.sum_list:
            D += Op.scalar
        return D

class planewave_field:
    def __init__(self,amplitude=1,node=None):
        self.amplitude = amplitude
        self.op_size = 1
        self.node = node

    def build(self):
        return self.amplitude

    def __mul__(self,other):
        if isinstance(other,Number):
            self.amplitude *= other
            return self
        else:
            raise Exception('undefined')
             
    def __rmul__(self,other):
        if isinstance(other,Number):
            return self.__mul__(other)
        else:
            raise Exception('undefined')

    def __rmatmul__(self,other):
        if type(other) is type(planewave_operator()):
            return other.__matmul__(self)