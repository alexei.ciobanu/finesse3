'''
A module for lazy expressions in python. Currently Non-operational.

The goal is to create a specification of mutable numeric types in python.
Combining that with lazy evaluation we can create expressions in which the
variables can change value without recreating the entire expression.

The expressions of course must be evaluated at some point at which point
they cease being lazy.

finesse.element almost has the kind of implementation that I want.
There is also a 'lazy' pypi package that does something similar without
mutable numeric types.

I know I can fake a mutable float by just making an object with a value
attribute that can be modified. But that object has to support most of
the methods possible with floats which will require a lot of rewritting.
I want a more simple and dumb solution to mutable variables.
'''

from numbers import Number
import copy
import numpy as np

class LazyProperty(object):
    '''
    Pretty much the same as cachedproperty in boltons.

    Example

    class bunch:
    def __init__(self):
        self._a = 1
    
    @cachedproperty
    def a(self):
        print('computing stuff')
        return 4

    obj = bunch()
    print(obj.a) -> computes self.a()
    print(obj.a) -> returns cached __dict__ value
    '''

    def __init__(self, func):
        self._func = func
        self.__name__ = func.__name__
        self.__doc__ = func.__doc__

    def __get__(self, obj, objtype=None):
        if obj is None: return None
        result = obj.__dict__[self.__name__] = self._func(obj)
        return result

class LazyVariable(object):
    pass

class LazyNumber(Number):
    def __init__(self, value):
        assert(isinstance(value, Number))
        super().__init__()
        self._value = value

    def __add__(self, other):
        return LazyNumber(self._value + other)

    def __mul__(self, other):
        return LazyNumber(self._value * other)

    def __repr__(self):
        return str(self._value)

    def __str__(self):
        return str(self._value)

class LazyFloat(float):
    def __new__(self, value):
        return float.__new__(self, value)
    def __init__(self, value):
        float.__init__(value)