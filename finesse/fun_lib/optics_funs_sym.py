import numpy as np
import pandas as pd
import scipy.integrate as intgr
import sympy as sp

def abcd_grin(d,n0,n2):
    # siegman p586 "h"
    return sp.Matrix(\
                    [[sp.cos(d*sp.sp.sqrt(n2/n0)),sp.sp.sin(d*sp.sp.sqrt(n2/n0))/(sp.sp.sqrt(n2*n0))],\
                     [-sp.sp.sqrt(n2*n0)*sp.sp.sin(d*sp.sp.sqrt(n2/n0)),sp.cos(d*sp.sp.sqrt(n2/n0))]]\
                   )

def abcd_space(d):
    return sp.Matrix(\
                   [[1,d],\
                    [0,1]]\
                   )
                   
def abcd_mirror(Rc):
    return sp.Matrix(\
                   [[1,0],\
                    [-2/Rc,1]]\
                   )
                   
def abcd_lens(f):
    return sp.Matrix(\
                   [[1,0],\
                    [-1/f,1]]\
                   )
                   
def abcd_lens_p(p):
    return sp.Matrix(\
                   [[1,0],\
                    [-p,1]]\
                   )
                   
def abcd_eig(M):
    A,B,C,D = M
    one_on_q_eig = (-(A-D) - sp.sp.sqrt((A-D)**2 + 4*B*C))/(2*B)
    return  1/(one_on_q_eig)
    
def q_propag(q,M):
    A,B,C,D = M
    return (A*q + B)/(C*q + D)
    
def k_fun(n1,n2,q1,q2,lam=sp.Symbol('lambda',positive=True),gamma=0,dx=0):
    
    fac = sp.factorial
    
    # rip out subcomponents of q
    z1 = sp.re(q1)
    z2 = sp.re(q2)
    zR1 = sp.im(q1)
    zR2 = sp.im(q2)
    w01 = sp.sqrt(lam*zR1/sp.pi)
    w02 = sp.sqrt(lam*zR2/sp.pi)
    
    # Bayer-Helms sub terms (use original paper, finesse manual gets F and F_bar wrong way around)
    K2 = (z1- z2)/zR2
    K0 = (zR1 - zR2)/zR2  
    K = sp.I*sp.conjugate(q1-q2)/(2*sp.im(q2))
    K = (K0 + sp.I*K2)/2
    #X_bar = (1j*zR2 - z2)*sp.sin(gamma)/(sp.sqrt(1+np.conj(K))*w0)
    #X = (1j*zR2 + z2)*sp.sin(gamma)/(sp.sqrt(1+np.conj(K))*w0)
    X_bar = (dx/w02-(z2/zR2 - sp.I)*sp.sin(gamma)*zR2/w02)/(sp.sqrt(1+sp.conjugate(K)))
    X =     (dx/w02-(z2/zR2 + sp.I*(1+2*sp.conjugate(K)))*sp.sin(gamma)*zR2/w02)/(sp.sqrt(1+sp.conjugate(K)))
    F_bar = K/(2*(1+K0))
    F = sp.conjugate(K)/2
    
    E_x = sp.exp(-(X*X_bar)/2 - sp.I*dx/w02 * sp.sin(gamma)*zR2/w02)
     
    def S_g(n1,n2):
        s1 = 0
        for mu1 in range(0, (n1//2 if n1 % 2 == 0 else (n1-1)//2) + 1):
            for mu2 in range(0, (n2//2 if n2 % 2 == 0 else (n2-1)//2) + 1):
                t1 = ((-1)**mu1*X_bar**(n1-2*mu1)*X**(n2-2*mu2))/(fac(n1-2*mu1)*fac(n2-2*mu2))
                s2 = 0
                for sigma in range(0,min(mu1,mu2)+1):
                    s2 += ((-1)**sigma * F_bar**(mu1-sigma)*F**(mu2-sigma))/(fac(2*sigma)*fac(mu1-sigma)*fac(mu2-sigma))
    #                 print(mu1,mu2,sigma)
                s1 += t1 * s2
        return s1

    def S_u(n1,n2):
        s1 = 0
        for mu1 in range(0, ((n1-1)//2 if (n1-1) % 2 == 0 else ((n1-1)-1)//2) + 1):
            for mu2 in range(0, ((n2-1)//2 if (n2-1) % 2 == 0 else ((n2-1)-1)//2) + 1):
                t1 = ((-1)**mu1*X_bar**((n1-1)-2*mu1)*X**((n2-1)-2*mu2))/(fac((n1-1)-2*mu1)*fac((n2-1)-2*mu2))
                s2 = 0
                for sigma in range(0,min(mu1,mu2)+1):
                    s2 += ((-1)**sigma*F_bar**(mu1-sigma)*F**(mu2-sigma))/(fac(2*sigma+1)*fac(mu1-sigma)*fac(mu2-sigma))
#                     print(mu1,mu2,sigma)
                s1 += t1 * s2
        return s1
    
    expr = (-1)**n2 * E_x * sp.sqrt(fac(n1)*fac(n2)) \
    * (1 + K0)**(sp.Rational(n1,2) + sp.Rational(1,4)) * (1+sp.conjugate(K))**(sp.Rational(-(n1+n2+1),2)) \
    * (S_g(n1,n2) - S_u(n1,n2))
    
    return expr
    
def k_nmnm(n1,m1,n2,m2,q1,q2,lam=sp.Symbol('lambda',positive=True),gammax=0,gammay=0,dx=0,dy=0):
    return k_fun(n1,n2,q1,q2,lam=lam,gamma=gammax,dx=dx) * k_fun(m1,m2,q1,q2,lam=lam,gamma=gammay,dx=dy)
