'''
optical operators
'''
import numpy as np

from finesse.fun_lib.abstract_operators3 import AbstractScalarOperator, OperatorMatrix, NullOperator
from finesse.element import Symbol
import copy


class AbstractOpticalOperator(AbstractScalarOperator):
    '''
    Optical operators will need to inherit this
    '''

    def __init__(self, scalar=1, Mx=None, My=None, node1=None, node2=None):
        super().__init__(scalar=scalar, node1=node1, node2=node2)
        self.Mx = Mx
        self.My = My
        if self.Mx is not None:
            if self.My is None:
                self.My = np.eye(2)
        else:
            if self.My is not None:
                self.Mx = np.eye(2)

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = self
        op = super().__matmul__(other, copy_target=copy_target)
        if isinstance(other, AbstractOpticalOperator):
            if not all(x is None for x in (self.Mx, self.My)):
                op.Mx = self.Mx @ other.Mx
                op.My = self.My @ other.My
        return op

    def __repr__(self):
        string = super().__repr__()
        return string.replace('I', 'AOpt')

    def __str__(self):
        string = super().__str__()
        return string.replace('I', 'AOpt')


class PlanewaveOperator(AbstractOpticalOperator):
    '''
    Frequency list is typically generated from the model object and owned
    by the simulation class. I guess these concrete operators need to
    store a reference to the simulation object. They will also need to
    store a reference to the model object in case they need to access
    component or node parameters (such as the q parameters).

    Frequencies are unresolved.
    '''

    def __init__(self,
                 scalar=1,
                 node1=None,
                 node2=None,
                 N=None,
                 vec=None,
                 vec_mat=None,
                 *args,
                 **kwargs):
        super().__init__(scalar=scalar, node1=node1, node2=node2)
        vec_args_none = [x is not None for x in [N,vec,vec_mat]]
        if sum(vec_args_none) > 1 :
            raise Exception("can't set both N and vec at the same time")
        if vec_args_none == [True,False,False]:
            self.vec_mat = np.diag(np.ones(N))
        elif vec_args_none == [False,True,False]:
            self.vec_mat = np.diag(vec)
        elif vec_args_none == [False,False,True]:
            self.vec_mat = np.array(vec_mat)
        else:
            self.vec_mat = np.array([[1]])

        self.context['N'] = len(self.vec_mat)

        if self.context['N'] > 1:
            self.op_mat = OperatorMatrix([[PlanewaveOperator(
                scalar=el,
                node1=self.node1,
                node2=self.node2
                ) if el != 0 else NullOperator() for el in row
                ] for row in self.vec_mat])
        else:
            # can't call PlanewaveOperator in base case
            # otherwise will create infinite recursive loop
            # this will be the hackiest of hacks
            op = copy.copy(self)
            op.op_mat = None
            op._scalar = self.vec_mat[0,0]
            self.op_mat = OperatorMatrix([[op]])

    def _build_op_mat(self):
        self.op_mat = OperatorMatrix([[PlanewaveOperator(
            scalar=el,
            node1=self.node1,
            node2=self.node2
            ) if el != 0 else NullOperator() for el in row
            ] for row in self.vec_mat])


    def __matmul__(self, other):
        op = super().__matmul__(other)
        if isinstance(other, PlanewaveOperator):
            op.vec_mat = self.vec_mat @ other.vec_mat
            if self.op_mat is None or other.op_mat is None:
                op.op_mat = None
            else:
                op.op_mat = self.op_mat @ other.op_mat
        return op

    def __repr__(self):
        try:
            if self.context['N'] == 1:
                value = self.scalar#*self.op_mat[0,0].scalar
            else:
                value = self.scalar
            if isinstance(value, Symbol):
                return f'({value})Planewave'
            else:
                return f'({value:.4g})Planewave'
        except TypeError:
            return 'failed __repr__'

    def build(self, context=None):
        if isinstance(self.scalar, Symbol):
            return self.scalar.value
        else:
            return self.scalar

    def build(self, context=None):
        if self.context['N'] == 1:
            if isinstance(self.scalar, Symbol):
                return (self._scalar*self.op_mat[0,0].scalar).value
            else:
                return self._scalar*self.op_mat[0,0].scalar
        else:
            return (self.scalar*self.op_mat).build()


class HermiteGaussOperator(AbstractOpticalOperator):
    '''
    Still haven't figured out how to deal with maps and q parameters.
    It's gonna need to pull q parameters from the nodes since ABCD
    matrices alone aren't enough to compute HOM scattering matrices.

    Each operator needs to know what q1,q2 the initial and final q are.
    It will then propagate q1 through it's abcd to get q2' and then compute
    the scattering matrix between q2 and q2'. We can do a check if they are
    identical and just return identity for a small speedup.

    Spoke to Dan about how to handle maps with changing q's. He said that
    Finesse2 by default deals with this by recomputing the map scattering matrix
    every time the q changes with other fancier methods (like updating the scattering
    matrix with bayerhelms) available if specified. Seems like it's good enough for
    me just to implement the default method for now.

    There is still a sort of catch 22 since the operators need to know what the q's
    are to do bayerhelms but to get the q's you need to do the ABCD tracing with
    very similar operators. Right now I chose the lazy route of using the existing
    Finesse3 tracing algorithm to determine the q's. This makes this implementation
    of the HG operators very Finesse3 centric. Upside is that it makes it much
    more comparable to the other Finessse3 solvers since the also use the same
    tracer. Downside is that this implementation can't be reused outside of Finesse3
    without a hefty rewrite.
    '''
    pass

    def __init__(self,
                 scalar=1,
                 N_freqs=1,
                 hom_list=((0, 0)),
                 node1=None,
                 node2=None,
                 simulation=None,
                 *args,
                 **kwargs):
        super().__init__(scalar=scalar, node1=node1, node2=node2)
        self._simulation = simulation
        self.N_freqs = N_freqs
        self.hom_list = hom_list
        self.N_homs = len(self.hom_list)
        self.context['N'] = self.N_freqs * self.N_homs


class LCTOperator(AbstractOpticalOperator):
    pass
