import numpy as np
import pandas as pd
import scipy.integrate as intgr

def abcd_grin(d,n0,n2):
    # siegman p586 "h"
    return np.array(\
                    [[np.cos(d*np.sqrt(n2/n0)),np.sin(d*np.sqrt(n2/n0))/(np.sqrt(n2*n0))],\
                     [-np.sqrt(n2*n0)*np.sin(d*np.sqrt(n2/n0)),np.cos(d*np.sqrt(n2/n0))]]\
                   )
                                      
def abcd_space(d):
    if isinstance(d,np.ndarray):
        N = np.size(d)
        M = np.zeros([N,2,2])
        A = M[:,0,0]
        B = M[:,0,1]
        C = M[:,1,0]
        D = M[:,1,1]
        
        A.real = 1
        B.real = d
        C.real = 0
        D.real = 1
        return M
    else:
        return np.array(\
                       [[1,d],\
                        [0,1]]\
                       )

def abcd_lens(f):
    if isinstance(f,np.ndarray):
        N = np.size(f)
        M = np.zeros([N,2,2])
        A = M[:,0,0]
        B = M[:,0,1]
        C = M[:,1,0]
        D = M[:,1,1]
        
        A.real = 1
        B.real = 0
        C.real = -1/f
        D.real = 1
        return M
    else:
        return np.array(\
                   [[1,0],\
                    [-1/f,1]]\
                   )
                   
def abcd_mirror(Rc):
    return np.array(\
                   [[1,0],\
                    [-2/Rc,1]]\
                   )
                   
def abcd_lens_p(p):
    return np.array(\
                   [[1,0],\
                    [-p,1]]\
                   )
                   
def abcd_eig(M):
    A,B,C,D = M.flatten()
    one_on_q_eig = (-(A-D) - np.lib.scimath.sqrt((A-D)**2 + 4*B*C))/(2*B)
    return  1/(one_on_q_eig)
    
def q_propag(q,M):
    ndim = np.size(np.shape(M))
    if ndim == 3:
        A = M[:,0,0]
        B = M[:,0,1]
        C = M[:,1,0]
        D = M[:,1,1]
        return (A*q + B)/(C*q + D)
    elif ndim == 2:
        A,B,C,D = M.flatten()
        return (A*q + B)/(C*q + D)
    else:
        raise Exception(f'unrecognized type {type(M)} for M')
    
def abcd_reverse(M):
    # from Tovar & Casperson (1994) Table 2
    A,B,C,D = M.flatten()
    return 1/(A*D-B*C) * np.array(\
                                   [[D,B],\
                                    [C,A]]\
                                   ) 
    
def make_cavity(q1,d=1):
    '''
    This will try to find the mirror curvatures of a fabry perot
    whos eigenmode q2 matches the closest for a particular input q1.
    
    Not guaranteed to converge for a particular choice of d, though if it does the solution should be unique.
    '''
    import scipy.optimize
    L1 = abcd_space(d)
    def iterfun(Rs):
        R1,R2 = Rs
        M1 = abcd_mirror(R1)
        M2 = abcd_mirror(R2)
        Mtot = np.matmul(np.matmul(np.matmul(M2,L1),M1),L1)
        q2 = abcd_eig(Mtot)
#         print(q2,mode_mismatch(q2,q1))
        if q2.imag == 0:
            return np.inf
        else:
            return mode_mismatch(q2,q1)
    soln = scipy.optimize.minimize(iterfun,(2,2),method='Nelder-Mead',options={'xatol' : 1e-7, 'ftol' : 1e-7})   
    return soln
    
def make_cavity2(q1):
    '''
    This will try to find the mirror curvatures and separation of a fabry perot
    whos eigenmode q2 matches the closest for a particular input q1.
    
    Guaranteed to converge for any physical q1, though the solution is not unique.
    '''
    import scipy.optimize
    def iterfun(Rs):
        R1,R2,d = Rs
        M1 = abcd_mirror(R1)
        L1 = abcd_space(d)
        M2 = abcd_mirror(R2)
        Mtot = np.matmul(np.matmul(np.matmul(M1,L1),M2),L1)
        q2 = abcd_eig(Mtot)
#         print(q2,mode_mismatch(q2,q1))
        if q2.imag == 0:
            return np.inf
        else:
            return mode_mismatch(q2,q1)
    soln = scipy.optimize.minimize(iterfun,(4,4,2),method='Nelder-Mead',options={'xatol' : 1e-7, 'ftol' : 1e-7, 'adaptive':True})   
    return soln
	
def rt_gouy(M):
	A,B,C,D = M.flatten()
	xi = np.sign(B)*np.arccos((A+D)/2)
	return xi
	
def rt_gouy2(M):
	A,B,C,D = M.flatten()
	xi = 2*np.arccos(np.sign(B)*np.sqrt((A+D+2)/4))
	return xi
    
def herm(n,x):
    from numpy.polynomial import hermite
    
    if n == -1:
        np.exp(x**2)
        return 0.5 * np.sqrt(np.pi) * np.exp(x**2) * (-np.vectorize(np.math.erf)(x) + 1)
    elif n == -2:
        return 0.25 * np.sqrt(np.pi) * ((2*(np.sqrt(np.pi)*np.exp(x**2)*x*np.math.erf(x) + 1))/np.sqrt(np.pi) - 2*np.exp(x**2)*x)
    
    c = np.zeros(n+1)
    c[-1] = 1
    return hermite.hermval(x,c)
    
def laguerre_coeff(n,alpha):
    '''
    stole this one from a mathworks post
    
    Geert Van Damme (4 Aug 2008)
    
    I'd like to propose the following elegant alternative: determine the coefficients of the associated Laguerre polynomial of order n, by determining the coefficients of the characteristic polynomial of its companion matrix:

    function [c] = Laguerre_coeff(n, alpha)

    i = 1:n;
    a = (2*i-1) + alpha;
    b = sqrt( i(1:n-1) .* ((1:n-1) + alpha) );
    CM = diag(a) + diag(b,1) + diag(b,-1);

    c = (-1)^n/factorial(n) * poly(CM);
    
    '''

    i = np.arange(1,n+1)
    a = (2*i-1) + alpha
    b = np.sqrt( i[0:n-1] * (np.arange(1,n) + alpha) )
    CM = np.diag(a) + np.diag(b,1) + np.diag(b,-1)

    if CM.size < 1: # CM is empty
        return np.array([1])
    else:
        c = (-1)**n/np.math.factorial(n) * np.poly(CM)
        return c
        
def laguerre(p,l,x):
    return np.polyval(laguerre_coeff(p,l),x)

def u_n(n,x,z,w0,lam=1064e-9):
    import numpy as np
    
    zR = np.pi*w0**2/lam
    k = 2*np.pi/lam
    w = w0*np.sqrt(1+(z/zR)**2)
    
    if z == 0:
        R = np.inf
    else:
        R = (z+np.spacing(0.0))*(1+(zR/z)**2)
    psi = np.arctan(z/zR)
    
    t1 = np.sqrt(np.sqrt(2/np.pi))
    t2 = np.sqrt(np.exp(1j*(2*n+1)*psi)/(2**n*np.math.factorial(n)*w))
    t3 = herm(n,np.sqrt(2)*x/w)
    a1 = 0 # -1j*k*z
    a2 = 1j*k*x**2/(2*R)
    a3 = x**2/w**2
    t4 = np.exp(a1 - a2 - a3)
    E = t1 * t2 * t3 * t4
    
    return E

def u_nm(x,y,z,w0,n=0,m=0,lam=1064e-9,gamma_x=0):
    return np.outer(u_n(m,y,z,w0,lam),u_n(n,x,z,w0,lam))
    
def u_n_q(n,x,q,lam=1064e-9):
    # siegmann eq 16.54
    import numpy as np
    factorial = np.math.factorial
    
    q0 = q - np.real(q)
    w0 = np.sqrt(lam*np.imag(q0)/np.pi)
    w = np.sqrt(-lam/(np.pi*np.imag(1/q)))
    k = 2*np.pi/lam
    
    t1 = np.sqrt(np.sqrt(2/np.pi))
    t2 = np.sqrt(1.0/(2.0**n*factorial(n)*w0))
    t3 = np.sqrt(q0/q)
    t4 = (q0/np.conj(q0) * np.conj(q)/q)**(n/2)
    t5 = herm(n,np.sqrt(2)*x/w) * np.exp(-1j*k*x**2/(2*q))
    
    E = t1 * t2 * t3 * t4 * t5
    
    return E
    
def u_n_q(n,x,q,lam=1064e-9):
    # siegmann eq 16.54
    
    w = q2w(q)
    k = 2*np.pi/lam
    
    norm = gauss_norm(n,q,lam=lam)
    u = herm(n,np.sqrt(2)*x/w) * np.exp(-1j*k*x**2/(2*q))
    
    E = norm * u
    
    return E
    
def u_nm_q(x,y,q,n=0,m=0,lam=1064e-9):
    return np.outer(u_n_q(m,y,q,lam),u_n_q(n,x,q,lam))
    
def u_nm_q_astig(x,y,qx,qy,n=0,m=0,lam=1064e-9):
    if np.ndim(x) == 1 and np.ndim(x) == 1:
        return np.outer(u_n_q(m,y,qy,lam),u_n_q(n,x,qx,lam))       
    else:
        return u_n_q(m,y,qy,lam) * u_n_q(n,x,qx,lam)
        
def u_pl(p,l,r,phi,z,w0,lam=1064e-9):
    import numpy as np
    factorial = np.math.factorial

    N = 2*p + np.abs(l)
    psi_z = (N+1)*np.arctan(z/zR(w0,lam))
    w = w_z(w0,z,lam)
    k = 2*np.pi/lam
    C_LG_lp = np.sqrt(2*factorial(p)/(np.pi*factorial(p+np.abs(l))))
    
    t1 = C_LG_lp/w
    t2 = (r*np.sqrt(2)/w)**(np.abs(l))
    t3 = np.exp(-r**2/w**2)
    t4 = laguerre(p,np.abs(l),2*r**2/w**2)
    t5 = np.exp(-1j*k*r**2/(2*R_z(w0,z,lam)))
    t6 = np.exp(1j*(-k*z + psi_z))
    
    return np.multiply.outer(t1*t2*t3*t4*t5*t6,np.exp(-1j*l*phi))
    
def zR(w0,lam=1064e-9):
    return np.pi * w0**2 / lam
    
def w_z(w0,z,lam=1064e-9):
    return w0*np.sqrt(1+(z/zR(w0,lam))**2)
    
def R_z(w0,z,lam=1064e-9):
    return z*(1+(zR(w0,lam)/z)**2)
     
def q_create(z,w0,lam=1064e-9):
    return z + 1j*np.pi*w0**2/lam
    
def q_create2(R,w,lam=1064e-9):
    return (1/R - 1j*lam/(np.pi*w**2))**-1
    
def q2guoy(q,n=0,m=0):
    z = np.real(q)
    zR = np.imag(q)
    psi = np.arctan(z/zR)
    return np.exp(1j*((n+1/2))*psi) * np.exp(1j*((m+1/2))*psi)
    # return np.sqrt(np.exp(1j*(2*n+1))*psi) * np.sqrt(np.exp(1j*(2*m+1))*psi)
    
def q2w(q,lam=1064e-9):
    import numpy as np
    return np.sqrt(-lam/(np.pi*np.imag(1/q)))

def q2w0(q,lam=1064e-9):
    import numpy as np
    q0 = q - np.real(q)
    return np.sqrt(-lam/(np.pi*np.imag(1/q0)))

def q2Theta(q,lam=1064e-9):
    w0 = q2w0(q,lam)
    return lam/(np.pi*w0)
    
def accum_guoy(q,d):
    '''
    Calculates the amount of guoy phase that is accumilated by a TEM00
    Gaussian beam with basis q as it propagates a length d. Originally written
    to calculate guoy phase shifts in LCT cavity scans.
    '''
    p0 = q2guoy(q)
    p1 = q2guoy(q+d)
    p2 = np.conj(p0)*p1
    return p2

def DHT(F, x, y, z, w0, lam=1064e-9, maxorder=None, mode_list=None, type='Riemann'):
    dx = abs(x[1] - x[0])
    dy = abs(y[1] - y[0])

    # legacy
    if not maxorder is None:
        N = maxorder+1
        H = np.zeros([N,N],dtype=np.complex128)

        for ii in range(N):
            for jj in range(N):
                herm = u_nm(x,y,z,w0,n,m,lam)
                if type == 'Simpson':
                    H[ii,jj] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
                elif type == 'Riemann':
                    H[ii,jj] = np.sum((F*np.conj(herm)))*dx*dy           
        return H
        
    # new: mode_lists
    assert(not mode_list is None)
    mode_arr = np.array(mode_list)
    arr_shape = mode_arr.shape
    
    if len(arr_shape) == 3: # N x M x 2
        H = np.zeros([arr_shape[0],arr_shape[1]],dtype=np.complex128)
        for ii in mode_arr:
            for jj in ii:
                n,m = jj
                herm = u_nm(x,y,z,w0,n,m,lam)
                if type == 'Simpson':
                    H[n,m] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
                elif type == 'Riemann':
                    H[n,m] = np.sum((F*np.conj(herm)))*dx*dy 
    elif len(arr_shape) == 2: # N x 2
        H = np.zeros([arr_shape[0]],dtype=np.complex128)
        for j,ii in enumerate(mode_arr):
            n,m = ii
            herm = u_nm(x,y,z,w0,n,m,lam)
            if type == 'Simpson':
                H[j] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
            elif type == 'Riemann':
                H[j] = np.sum((F*np.conj(herm)))*dx*dy  
                
    return H              
    
    
def DHT_q(F, x, y, q, lam=1064e-9, maxorder=None, mode_list=None, type='Riemann'):
    dx = abs(x[1] - x[0])
    dy = abs(y[1] - y[0])

    # legacy
    if not maxorder is None:
        N = maxorder+1
        H = np.zeros([N,N],dtype=np.complex128)

        for ii in range(N):
            for jj in range(N):
                herm = u_nm_q(x,y,q,ii,jj,lam)
                if type == 'Simpson':
                    H[ii,jj] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
                elif type == 'Riemann':
                    H[ii,jj] = np.sum((F*np.conj(herm)))*dx*dy           
        return H
        
    # new: mode_lists
    assert(not mode_list is None)
    mode_arr = np.array(mode_list)
    arr_shape = mode_arr.shape
    
    if len(arr_shape) == 3: # N x M x 2
        H = np.zeros([arr_shape[0],arr_shape[1]],dtype=np.complex128)
        for ii in mode_arr:
            for jj in ii:
                n,m = jj
                herm = u_nm_q(x,y,q,n,m,lam)
                if type == 'Simpson':
                    H[n,m] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
                elif type == 'Riemann':
                    H[n,m] = np.sum((F*np.conj(herm)))*dx*dy 
    elif len(arr_shape) == 2: # N x 2
        H = np.zeros([arr_shape[0]],dtype=np.complex128)
        for j,ii in enumerate(mode_arr):
            n,m = ii
            herm = u_nm_q(x,y,q,n,m,lam)
            if type == 'Simpson':
                H[j] = intgr.simps(intgr.simps(F*np.conj(herm)))*dx*dy
            elif type == 'Riemann':
                H[j] = np.sum((F*np.conj(herm)))*dx*dy  
    return H  
    
def BH_DHT(q1,q2,n1=0,m1=0,dx=0,dy=0,gammax=0,gammay=0,maxtem=None,mode_list=None):
    # Bayer-Helms DHT (mostly for debugging Bayer-Helms)
     
    # legacy
    if not maxtem is None: 
        N = maxtem+1
        H = np.zeros([N,N],dtype=np.complex128)

        for ii in range(N):
            for jj in range(N):
                H[jj,ii] = k_fun(n1,jj,q1,q2,dx=dx,gamma=gammax)*k_fun(m1,ii,q1,q2,dx=dy,gamma=gammay)
                
        return H
    else:
        assert(not mode_list is None)
        mode_arr = np.array(mode_list)
        arr_shape = mode_arr.shape
        
        H = np.zeros([arr_shape[0]],dtype=np.complex128)
        for j,ii in enumerate(mode_arr):
            n2,m2 = ii
            H[j] = k_fun(n1,n2,q1,q2,dx=dx,gamma=gammax)*k_fun(m1,m2,q1,q2,dx=dy,gamma=gammay)
        return H
    
def BH_DHT_astig(q1x,q1y,q2x,q2y,n1=0,m1=0,dx=0,dy=0,gammax=0,gammay=0,maxtem=10):
    # Bayer-Helms DHT (mostly for debugging Bayer-Helms)
    N = maxtem+1
    H = np.zeros([N,N],dtype=np.complex128)   

    for ii in range(N):
        for jj in range(N):
            H[jj,ii] = k_fun(n1,jj,q1x,q2x,dx=dx,gamma=gammax)*k_fun(m1,ii,q1y,q2y,dx=dy,gamma=gammay)
            
    return H  

def BH_knm(q1x,q1y,q2x,q2y,dx=0,dy=0,gammax=0,gammay=0,maxtem=10):
    m_list = mode_list2(maxtem)
    N = len(m_list)
    H = np.zeros([N,N],dtype=np.complex128)

    for i,nm1 in enumerate(m_list):
        for j,nm2 in enumerate(m_list):
            n1,m1 = nm1
            n2,m2 = nm2
            H[i,j] = k_fun(n1,n2,q1x,q2x,dx=dx,gamma=gammax)*k_fun(m1,m2,q1y,q2y,dx=dy,gamma=gammay)
    return H    

def iDHT(H, x, y, z, w0, lam=1064e-9):
    N = len(H)
    F = 0
    for m in range(N):
        for n in range(N):
            F = F + H[m,n]*u_nm(x,y,z,w0,n,m,lam)
    return F

def iDHT_q(H, x, y, q, lam=1064e-9, mode_list=None):
    F = 0
    
    # legacy
    if mode_list is None:
        N = len(H)
        for m in range(N):
            for n in range(N):
                F = F + H[m,n]*u_nm_q(x,y,q,n,m,lam)
    else:
        for h,jj in zip(H,mode_list):
            n,m = jj
            F = F + h*u_nm_q(x,y,q,n,m,lam)
    return F

def parse_DHT(H,power=False,sortby='magnitude'):
    out_list = []
    for n in range(len(H)):
        for m in range(len(H)):
            out_list.append([abs(H[n,m]),np.angle(H[n,m],deg=True),n,m])
    df = pd.DataFrame(out_list)
    df.columns = ['abs','deg','n','m']
    if sortby == 'magnitude':
        df = df.sort_values('abs',ascending=False).reset_index(drop=True)
    elif sortby == 'n,m':
        df = df.sort_values(by=['m','n'],ascending=True).reset_index(drop=True)
    if power:
        df['abs'] = df['abs']**2
        df.columns = ['power','deg','n','m']
    return df
    
def maxtem_scan(F, x, y, z, w0, lam=1064e-9, maxtem=None):
    iDHT_s = lambda H: iDHT(H, x, y, z, w0, lam)
    DHT_s = lambda F, maxtem: DHT(F,x,y,z,w0,lam,maxtem)
    dx = np.abs(x[0] - x[1])
    dy = np.abs(y[0] - y[1])
    maxtems = np.arange(0,maxorder+1,1)
    out_list = []
    for maxtem in maxtems:
        residual = iDHT_s(DHT_s(F,maxtem)) - F
        res_int = np.sum(np.abs(residual)**2)*dx*dy
        out_list.append([maxtem,res_int])
    return out_list
    
def radial_zernike(n,m,r):
    
    r = np.asarray(r)
    scalar_input = False
    if r.ndim == 0:
        r = r[None]  # Makes x 1D
        scalar_input = True

    # The magic happens here
    
#     r[r>1] = np.nan
    
    fac = np.math.factorial
    t = 0
    if np.mod(n-m,2) == 0:
        for k in range((n-m)//2+1):
            num = ((-1)**k * fac(n-k))
            denom = (fac(k)*fac((n+m)/2-k)*fac((n-m)/2-k))
            t += num/denom * r**(n-2*k)  
        return t
        if scalar_input:
            return np.squeeze(t)
        else: return t
    else:
        return np.zeros(np.shape(r))
    
def zernike(n,m,r,phi):

    if m >= 0:
#         return np.einsum('i,j->ij',radial_zernike(n,m,r),np.cos(m*phi))
        return np.multiply.outer(radial_zernike(n,m,r),np.cos(m*phi))
    else:
#         return np.einsum('i,j->ij',radial_zernike(n,-m,r),np.sin(m*phi))
        return np.multiply.outer(radial_zernike(n,-m,r),np.sin(m*phi))
        
def DZT(F, r, phi, maxorder):
    # Discrete Zernike Transform
    # n -> [0:N]
    # m -> [-N:N]
    N = maxorder+1
    H = np.zeros([N,N*2-1])
    dr = abs(r[1] - r[0])
    dphi = abs(phi[1] - phi[0])

    for n in range(N):
        for m in range(-n,n+1):
#             print(ii,jj,np.shape(r),np.shape(phi))
            if m == 0:
                epsilon_m = 2
            else:
                epsilon_m = 1            
            norm_fac = (2*n+2)/(np.pi*epsilon_m)
#             norm_fac = 1
            
            test_fun = zernike(n,m,r,phi)
            H[n,m] = intgr.simps(intgr.simps(F*np.conj(test_fun)*r[:,None]))*dr*dphi * norm_fac
            
    return H

def iDZT(Z,r,phi):
    N = np.shape(Z)[0]
    F = 0
    for n in range(N):
        for m in range(-n,n+1):
            F = F + Z[n,m]*zernike(n,m,r,phi)            
    return F
    
def parse_DZT(Z):
    N = np.shape(Z)[0]
    out_list = []
    for n in range(N):
        for m in range(-n,n+1):
            out_list.append([abs(Z[n,m]),n,m])
    df = pd.DataFrame(out_list)
    df.columns = ['abs','n','m']
    return df.sort_values('abs',ascending=False)
    
# numeric version of coupling coeffs
def k_fun(n1,n2,q1,q2,lam=1064e-9,gamma=0,dx=0):
    
    import numpy as np
    fac = np.math.factorial
    sqrt = np.lib.scimath.sqrt
    Rational = lambda x,y: x/y
    
    # rip out subcomponents of q
    z1 = np.real(q1)
    z2 = np.real(q2)
    zR1 = np.imag(q1)
    zR2 = np.imag(q2)
    w01 = sqrt(lam*zR1/np.pi)
    w02 = sqrt(lam*zR2/np.pi)
    
    # Bayer-Helms sub terms
    K2 = (z1-z2)/zR2
    K0 = (zR1 - zR2)/zR2  
    K = 1j*np.conj(q1-q2)/(2*np.imag(q2))
    K = (K0 + 1j*K2)/2
    #X_bar = (1j*zR2 - z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    #X = (1j*zR2 + z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    X_bar = (dx/w02-(z2/zR2 - 1j)*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    X = (dx/w02-(z2/zR2 + 1j*(1+2*np.conj(K)))*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    F_bar = K/(2*(1+K0))
    F = np.conj(K)/2
    
    E_x = np.exp(-(X*X_bar)/2 - 1j*dx/w02 * np.sin(gamma)*zR2/w02)
    # note that there is a typo in BH paper for the E_x term where he uses w02 (ie bar(w0))
    # in the 1j*dx/w0 term instead of w01 (ie w0 without a bar)
    # upon closer inspection it looks like BH agrees with Riemann if the last index matches 
    # that of the term multiplying it 
    
    # i.e. 
    # 1j*dx/w02 * np.sin(gamma)*zR2/w02 (choose this one arbitrarily)
    # or 
    # 1j*dx/w01 * np.sin(gamma)*zR1/w01
    # give identical results equivelant with Riemann 
    
    # whereas
    # 1j*dx/w02 * np.sin(gamma)*zR1/w01 (orginal BH)
    # or
    # 1j*dx/w01 * np.sin(gamma)*zR2/w02
    # do not
    
    def S_g(n1,n2):
        s1 = 0
        for mu1 in range(0, (n1//2 if n1 % 2 == 0 else (n1-1)//2) + 1):
            for mu2 in range(0, (n2//2 if n2 % 2 == 0 else (n2-1)//2) + 1):
                t1 = ((-1)**mu1*X_bar**(n1-2*mu1)*X**(n2-2*mu2))/(fac(n1-2*mu1)*fac(n2-2*mu2))
                s2 = 0
                for sigma in range(0,min(mu1,mu2)+1):
                    s2 += ((-1)**sigma * F_bar**(mu1-sigma)*F**(mu2-sigma))/(fac(2*sigma)*fac(mu1-sigma)*fac(mu2-sigma))
    #                 print(mu1,mu2,sigma)
                s1 += t1 * s2
        return s1

    def S_u(n1,n2):
        s1 = 0
        for mu1 in range(0, ((n1-1)//2 if (n1-1) % 2 == 0 else ((n1-1)-1)//2) + 1):
            for mu2 in range(0, ((n2-1)//2 if (n2-1) % 2 == 0 else ((n2-1)-1)//2) + 1):
                t1 = ((-1)**mu1*X_bar**((n1-1)-2*mu1)*X**((n2-1)-2*mu2))/(fac((n1-1)-2*mu1)*fac((n2-1)-2*mu2))
                s2 = 0
                for sigma in range(0,min(mu1,mu2)+1):
                    s2 += ((-1)**sigma*F_bar**(mu1-sigma)*F**(mu2-sigma))/(fac(2*sigma+1)*fac(mu1-sigma)*fac(mu2-sigma))
                    # print(mu1,mu2,sigma)
                s1 += t1 * s2
        return s1
        
        
    # print('S_g = ', S_g(n1,n2))
    # print('S_u = ', S_u(n1,n2))
    
    expr = (-1)**n2 * E_x * sqrt(float(fac(n1)*fac(n2))) \
    * (1 + K0)**(n1/2 + 1/4) * (1+np.conj(K))**(-(n1+n2+1)/2) \
    * (S_g(n1,n2) - S_u(n1,n2))
    
    return expr

def k_nmnm(n1,m1,n2,m2,q1,q2,gammax=0,gammay=0,dx=0,dy=0):
    return k_fun(n1,n2,q1,q2,gamma=gammax,dx=dx) * k_fun(m1,m2,q1,q2,gamma=gammay,dx=dy)
    
def k_00(q1,q2,gamma=0,dx=0,lam=1064e-9):
    import numpy as np
    sqrt = np.lib.scimath.sqrt
    
    z1 = np.real(q1)
    z2 = np.real(q2)
    zR1 = np.imag(q1)
    zR2 = np.imag(q2)
    w01 = sqrt(lam*zR1/np.pi)
    w02 = sqrt(lam*zR2/np.pi)
    
    # Bayer-Helms sub terms
    K2 = (z1-z2)/zR2
    K0 = (zR1 - zR2)/zR2  
    K = 1j*np.conj(q1-q2)/(2*np.imag(q2))
    K = (K0 + 1j*K2)/2
    #X_bar = (1j*zR2 - z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    #X = (1j*zR2 + z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    X_bar = (dx/w02-(z2/zR2 - 1j)*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    X = (dx/w02-(z2/zR2 + 1j*(1+2*np.conj(K)))*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    F_bar = K/(2*(1+K0))
    F = np.conj(K)/2
    
    E_x = np.exp(-(X*X_bar)/2 - 1j*dx/w02 * np.sin(gamma)*zR2/w02)
    
    return E_x*(1+K0)**(1/4.0)*(1+np.conj(K))**(-1/2.0)
    
def k_01(q1,q2,gamma=0,dx=0,lam=1064e-9):
    import numpy as np
    sqrt = np.lib.scimath.sqrt
    
    z1 = np.real(q1)
    z2 = np.real(q2)
    zR1 = np.imag(q1)
    zR2 = np.imag(q2)
    w01 = sqrt(lam*zR1/np.pi)
    w02 = sqrt(lam*zR2/np.pi)
    
    # Bayer-Helms sub terms
    K2 = (z1-z2)/zR2
    K0 = (zR1 - zR2)/zR2  
    K = 1j*np.conj(q1-q2)/(2*np.imag(q2))
    K = (K0 + 1j*K2)/2
    #X_bar = (1j*zR2 - z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    #X = (1j*zR2 + z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    X_bar = (dx/w02-(z2/zR2 - 1j)*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    X = (dx/w02-(z2/zR2 + 1j*(1+2*np.conj(K)))*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    F_bar = K/(2*(1+K0))
    F = np.conj(K)/2
    
    E_x = np.exp(-(X*X_bar)/2 - 1j*dx/w02 * np.sin(gamma)*zR2/w02)
    
    return -E_x * (1+K0)**(1/4.0) * (1+np.conj(K))**(-1) * (X)
    
def k_02(q1,q2,gamma=0,dx=0,lam=1064e-9):
    import numpy as np
    sqrt = np.lib.scimath.sqrt
    
    z1 = np.real(q1)
    z2 = np.real(q2)
    zR1 = np.imag(q1)
    zR2 = np.imag(q2)
    w01 = sqrt(lam*zR1/np.pi)
    w02 = sqrt(lam*zR2/np.pi)
    
    # Bayer-Helms sub terms
    K2 = (z1-z2)/zR2
    K0 = (zR1 - zR2)/zR2  
    K = 1j*np.conj(q1-q2)/(2*np.imag(q2))
    K = (K0 + 1j*K2)/2
    #X_bar = (1j*zR2 - z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    #X = (1j*zR2 + z2)*np.sin(gamma)/(sqrt(1+np.conj(K))*w0)
    X_bar = (dx/w02-(z2/zR2 - 1j)*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    X = (dx/w02-(z2/zR2 + 1j*(1+2*np.conj(K)))*np.sin(gamma)*zR2/w02)/(sqrt(1+np.conj(K)))
    F_bar = K/(2*(1+K0))
    F = np.conj(K)/2
    
    E_x = np.exp(-(X*X_bar)/2 - 1j*dx/w02 * np.sin(gamma)*zR2/w02)
    
    return E_x * 2**(-1/2.0) * (1+K0)**(1/4.0)*(1+np.conj(K))**(-3/2.0) * (X**2 + 2*F_bar)
    
def mode_mismatch(q1,q2):
    return (np.abs(q2-q1)**2)/(np.abs(q2-np.conj(q1))**2)
    
def multigauss_overlap(q1,q2):
    return 1/2 + 1/4*k_nmnm(0,0,0,0,q1,q2) + 1/4*k_nmnm(0,0,0,0,q2,q1)
    
def multigauss_mismatch(q1,q2):
    # fractional power loss from adding two gaussian beams together
    return 1/2 - 1/4*k_nmnm(0,0,0,0,q1,q2) - 1/4*k_nmnm(0,0,0,0,q2,q1)
    
def mode_list(maxtem,even_only=False):
    modes_list = []
    for n in range(0,maxtem+1):
        for m in range(0,maxtem+1):
            if n+m <= maxtem:
                if even_only:
                    if np.mod(n,2)==0 and np.mod(m,2)==0 :
                        modes_list.append([n,m])
                else:
                    modes_list.append([n,m])
    return modes_list
    
def mode_list2(maxtem,even_only=False):
    '''This one counts up by mode order'''
    modes_list = []
    for i in range(0,maxtem+1):
        left = i
        right = 0
        while left >= 0:
            modes_list.append([left,right])
            left -= 1
            right += 1            
    return modes_list
    
def mode_list_maxorder(maxorder,linearise=False):
    '''
    maxorder is easier to plot but doesnt really mean anything
    '''
    N = maxorder + 1
    m_list = []
    for ii in range(N):
        temp = []
        for jj in range(N):
            if linearise:
                m_list.append([ii,jj])
            else:
                temp.append([ii,jj])
        if not linearise:
            m_list.append(temp)
    return m_list
    
def lin_mode_index(mode_list):
    lin_mode_list = []
    for n,m in mode_list:
        lin_mode_list.append((n+m) + m/(n+m+1))
    return lin_mode_list
    
def lin_mode_index2(mode_list):
    '''excludes mapping to boundaries'''
    lin_mode_list = []
    for n,m in mode_list:
        lin_mode_list.append((n+m) + (m+1)/(n+m+2))
    return lin_mode_list
    
def zr2m(zr1,M):
    return zr1*(2*np.sqrt(M) - M - 1)/(M - 1)

def zr2p(zr1,M):
    return -zr1*(2*np.sqrt(M) + M + 1)/(M - 1)

def z2m(zr1,z1,M):
    return (M*z1 + 2.0*zr1*np.sqrt(-M*(M - 1.0)) - z1)/(M - 1.0)

def z2p(zr1,z1,M):
    return (M*z1 - 2.0*zr1*np.sqrt(-M*(M - 1.0)) - z1)/(M - 1.0)
    
# the general one
def mismatch_circle(q1,M,t):
    z1 = np.real(q1)
    zR1 = np.imag(q1)
    r = (2*np.sqrt(M)*zR1)/(1-M)
    y0 = ((M+1)*zR1)/(1-M)
    x0 = z1
    q2 = r*np.cos(t) + x0 + 1j*(r*np.sin(t) + y0)
    return q2
    
def gauss_intens(x,y,wx=1,wy=1,x0=0,y0=0,a=1,floor=0):
    gauss = floor + a*np.exp(- np.add.outer(2*(x-x0)**2/wx**2,2*(y-y0)**2/wy**2))
    return gauss
    
def gauss_intens_2(x,y,wx=1,wy=1,x0=0,y0=0,a=1,floor=0,theta=0):
    "the integral of this is normalized to 1"
    xp = np.add.outer((x-x0)*np.cos(theta/180*np.pi),-(y-y0)*np.sin(theta/180*np.pi))
    yp = np.add.outer((x-x0)*np.sin(theta/180*np.pi), (y-y0)*np.cos(theta/180*np.pi))
    gauss = np.abs(u_nm_q_astig(xp,yp,qx=np.pi*wx**2*1j,qy=np.pi*wy**2*1j,lam=1))**2
    return gauss

def gauss_spot_propag(z,z0,w0,M_2=1,lam=1064e-9):
    return np.sqrt(w0**2 + M_2**2 * (lam/(np.pi*w0))**2 * (z-z0)**2)
    
def b_nm(n,m,a,b):

    def gee(n,m,x):
        '''a part of an iterative analytical solution to an indefinite integral 
        of a product of two hermite polynomials and a gaussian'''
        return -herm(n,x)*herm(m,x)*np.exp(-x**2)

    # make sure n >= m always
    if n < m:
        n,m = m,n
    
    s = 0
    for i in range(0,m+1):
        s += 2**i * np.math.factorial(m)/np.math.factorial(m-i) * (gee(n-1-i,m-i,b)-gee(n-1-i,m-i,a))
    return s
    
def gauss_norm(n,q,lam=1064e-9):
    q0 = np.imag(q)*1j
    w0 = q2w(q0,lam=lam)
    
    t1 = np.sqrt(np.sqrt(2/np.pi))
    t2 = np.sqrt(1.0/(2.0**n*np.math.factorial(n)*w0))
    t3 = np.sqrt(q0/q)
    t4 = (q0/np.conj(q0) * np.conj(q)/q)**(n/2)
    
    return t1*t2*t3*t4
    
def c_nm(n,m,q,a,b,lam=1064e-9):
    w = q2w(q,lam=lam)
    
    # we integrate with respect to dx_bar = sqrt(2)/w * dx
    # so change the limits of integration to be in barred units
    
    a_bar = a*np.sqrt(2)/w
    b_bar = b*np.sqrt(2)/w
    
    # the factor of w/np.sqrt(2) comes from the fact we changed a dx into a dx_bar
    return gauss_norm(n,q,lam) * np.conj(gauss_norm(m,q,lam)) * b_nm(n,m,a_bar,b_bar) * w/np.sqrt(2)
    
def g_rt(rs,phi):
    return np.product(rs)*np.exp(-1j*phi)

def E_trans(r1,r2,phi):
    t1 = np.sqrt(1-r1**2)
    t2 = np.sqrt(1-r2**2)
    return -t1*t2/np.sqrt(r1*r2) * np.sqrt(g_rt([r1,r2],phi))/(1-g_rt([r1,r2],phi))
