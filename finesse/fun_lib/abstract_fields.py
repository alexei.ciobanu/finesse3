'''
I started writing this thinking that I would need field objects in addition to
operator objects, but then I realised that the fields could quite trivially
implemented with operators
'''

from numbers import Number
import copy
import numpy as np

from finesse.element import Symbol
from finesse.densematrix import DenseMatrix
import finesse.fun_lib.func_funs as ff

class FieldException(Exception):
    pass

class DiGraphField(object):
    def __init__(self, node=None):
        self.node = node
        self._context = {'type': self.__class__}

    @property
    def context(self):
        return self._context

    def __matmul__(self, other, copy_target=None):
        self._assert_node_matmul(other)
        if copy_target is None:
            copy_target = other
        if copy_target is other:
            op = copy.copy(copy_target)  # avoiding potential side effects
            op.node2 = self.node2
        elif copy_target is self:
            op = copy.copy(copy_target)  # avoiding potential side effects
            op.node1 = other.node1
        else:
            return NotImplemented
        return op