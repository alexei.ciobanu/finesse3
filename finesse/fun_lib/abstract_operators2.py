'''
Second rewrite of the DiGraph operator specification. For the first one I
was hesitant in constructing new data types and instead relied on a bunch of
functions and conventional lists and arrays with a particular formatting that
conformed to those functions. I eventually realised that I implicitly defined
new data types that weren't specified but adhered to.

This rewrite explicitly defines the specification of the data types used
to represent the various operators in the old code. I am very happy with how
this turned out.

* finesse symbols are implemented
* SumList should subclass tuple instead of lists because I don't use
mutability (in fact using mutability breaks a lot of features).
* finish OperatorMatrix
* (wish feature) adding a Number to an operator converts the Number to
an AbstractScalarOperator with the appropriate scalar and the same nodes
as the operator added to
'''

from numbers import Number
import copy
import numpy as np

from finesse.element import Symbol
from finesse.simulations.dense import DenseMatrix
import finesse.fun_lib.graph_funs as gf

class OperatorException(Exception):
    pass

class DiGraphOperator(object):
    '''
    * Base class for DiGraph operators.
    * Acting with a DiGraphOperator on any other operator should just
    change the node connections.
    '''
    def __init__(self, node1=None, node2=None):
        self.node1 = node1
        self.node2 = node2
        self.__context = {'type':self.__class__}

    @property
    def context(self):
        return self.__context

    @property
    def nodes(self):
        return(self.node1, self.node2)

    def __matmul__(self, other, copy_target=None):
        if self.node1 != other.node2:
            raise OperatorException(f"node {self.node1} doesn't match node {other.node2}")
        else:
            if copy_target is None:
                copy_target = other
            if copy_target is other:
                op = copy.copy(copy_target) # avoiding potential side effects
                op.node2 = self.node2
            elif copy_target is self:
                op = copy.copy(copy_target) # avoiding potential side effects
                op.node1 = other.node1
            else:
                raise OperatorException("undefined")
            return op

    def __add__(self, other):
        '''
        __add__ doesn't enforce types to allow the SumList to
        do type conversions at build time.
        '''
        if isinstance(other, DiGraphOperator):
            if self.node1 != other.node1:
                raise OperatorException(f"initial node {self.node1} doesn't \
match initial node {other.node1}")
            if self.node2 != other.node2:
                raise OperatorException(f"final node {self.node2} doesn't match final node {other.node2}")
            op_list = [self, other]
            return OperatorSumList(op_list)
        elif isinstance(other, OperatorSumList):
            op_list = (self,) + other
            return OperatorSumList(iterable=op_list)
        else:
            return NotImplemented

class AbstractScalarOperator(DiGraphOperator):
    '''
    * Abstract operators are any operators that cannot be built
    without a context.

    * Common examples are the Identity and Null operators.

    * Acting with them and adding them should still be defined.

    * Acting an abstract operator on a concrete operator should
    result in a concrete operator of the same type. Addition is
    lazy and resolved at build time.
    '''
    def __init__(self, scalar=None, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)
        self._scalar = scalar
        self.__context = {'type':self.__class__}

    @property
    def context(self):
        return self.__context

    @property
    def scalar(self):
        return self._scalar

    def build(self, context=None):
        # print(context['type'])
        # print(issubclass(context['type'], AbstractScalarOperator))
        if context is None:
            raise OperatorException(f"Can't build {self.__class__} without a context.")
        elif issubclass(context['type'], AbstractScalarOperator):
            raise OperatorException(f"Can't build {self.__class__} with an abstract context")
        else:
            op_type = context['type']
            op_kwargs = {key:val for key, val in context.items() if key != 'type'}
            op = op_type(node1=self.node1, node2=self.node2, **op_kwargs)
            if isinstance(self.scalar, Symbol):
                op = type(self.scalar.value)(self.scalar) * op
            elif isinstance(self.scalar, Number):
                op = self.scalar * op
            else:
                raise OperatorException("undefined")
            return op.build()

    def __matmul__(self, other, copy_target=None):
        if isinstance(other, AbstractScalarOperator):
            if copy_target is None:
                if isinstance(other, IdentityOperator):
                    # identity operator is a special case
                    copy_target = self
                else:
                    copy_target = other
            op = super().__matmul__(other, copy_target=copy_target)
            op._scalar = self.scalar * other.scalar
            return op
        elif isinstance(other, OperatorSumList):
            op_list = gf.recursive_map(lambda x: self@x, other)
            return OperatorSumList(op_list)
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, DiGraphOperator):
            return self @ other
        elif isinstance(other, (Number, Symbol)):
            if other == 0:
                return NullOperator(node1=self.node1, node2=self.node2)
            else:
                op = AbstractScalarOperator(\
                    scalar=self._scalar*other, node1=self.node1, node2=self.node2)
                return op
        else:
            # let the caller's __rmul__ figure it out
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, Number):
            return self*other
        else:
            return NotImplemented

    def __neg__(self):
        return (-1)*self

    def __repr__(self):
        return f'({self.scalar:.4g})I'

    def __str__(self):
        return f'({self.scalar:.4g})I'

class IdentityOperator(AbstractScalarOperator):
    '''
    Mostly used for debugging.
    '''
    def __init__(self, node1=None, node2=None):
        super().__init__(scalar=1, node1=node1, node2=node2)

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = other
        return super().__matmul__(other, copy_target=copy_target)

    def __repr__(self):
        return 'I'

    def __str__(self):
        return 'I'

class NullOperator(AbstractScalarOperator):
    '''
    * Very useful in bookkeeping.
    '''
    def __init__(self, node1=None, node2=None):
        super().__init__(scalar=0, node1=node1, node2=node2)

    # def __mul__(self, other):
    #     pass
    def __mul__(self, other):
        if isinstance(other, Number):
            return self
        else:
            return super().__mul__(other)

    def __rmul__(self, other):
        if isinstance(other, Number):
            return self
        else:
            return super().__rmul__(other)

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            if isinstance(other, AbstractScalarOperator):
                copy_target = self
            else:
                copy_target = other
        return super().__matmul__(other, copy_target=copy_target)

    def __repr__(self):
        return '0'

    def __str__(self):
        return '0'

class ConcreteScalarOperator(DiGraphOperator):
    '''
    * Concrete operators are any operators that can be built.

    * Concrete operators can only act on other concrete operators
    of the same context or abstract operators.

    * Addition between operators is lazy and resolved at build
    time.

    * This class is intended to be subclassed but can be instantiated for
    a scalar operator that just returns its a diagonal matrix with the
    scalar value when built.
    '''
    def __init__(self, scalar=1, N=1, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)
        self._scalar = scalar
        self.__N = N
        self.__context = {'type':self.__class__, 'N':self.__N}

    @property
    def context(self):
        return self.__context

    @property
    def scalar(self):
        return self._scalar

    def build(self, context=None):
        if context is None:
            context = self.context
        if context != self.context:
            raise OperatorException(f"Tried to build a concrete operator that has context \
{self.context} with incompatible context {context}")
        return self.scalar * np.eye(self.__N, dtype=type(self._scalar))

    def __mul__(self, other):
        if isinstance(other, DiGraphOperator):
            return self@other
        elif isinstance(other, (Number, Symbol)):
            op = copy.copy(self)
            # print(op._scalar, other)
            op._scalar *= other
            return op
        else:
            raise OperatorException(f"undefined {self} __mul__ on {other}")

    def __rmul__(self, other):
        if isinstance(other, (Number, Symbol)):
            return self*other

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = self
        if isinstance(other, AbstractScalarOperator):
            op = super().__matmul__(other, copy_target=copy_target)
            op._scalar *= other.scalar
            return op
        elif isinstance(other, self.__class__):
            if self.context != other.context:
                raise OperatorException(f"Tried to compose two operators with incompatible contexts. \
context1 = {self.context}, context2 = {other.context}")
            op = super().__matmul__(other, copy_target=copy_target)
            op._scalar *= other.scalar
            return op
        elif isinstance(other, OperatorSumList):
            op_list = gf.recursive_map(lambda x: self@x, other)
            return OperatorSumList(op_list)
        else:
            raise OperatorException(f"action of operator {other} not defined on operator {self}")

class OperatorSumList(tuple):
    '''
    * Class for containing list pf lazily summed operators.

    * The summing is performed on build.

    * List should be immutable. Though the elements may be mutable.
    It is the users responsibility to rerun assertions if they start
    mutating list elements.

    * The list also contains the context (type) of the operators
    inside it. This is used to coerce any abstract operators
    in the list to their concrete counterpart.

    * The list should check types of operators added to it and
    keep track of the context. Once the context is concrete it
    should not allow other concrete operators in the list and adding
    an abstract operator should not change the context.
    '''
    def __new__(cls, iterable=(), context=None, node1=None, node2=None):
        # argument list of __new__ and __init__ need to match
        # otherwise python complain
        return super(OperatorSumList, cls).__new__(cls, iterable)

    def __init__(self, iterable=(), context=None, node1=None, node2=None):
        # asserting context and nodes on every instance is probably
        # overkill but I'm too lazy to write this more efficiently
        # and this will catch a lot of bugs
        # super().__init__(iterable)
        super().__init__()

        self.__node1 = node1
        self.__node2 = node2

        if context is None:
            self.__context = self.assert_context()
        else:
            self.assert_context()

        if self.nodes == (None, None):
            self.__node1, self.__node2 = self.assert_nodes()
        else:
            self.assert_nodes()

    @property
    def node1(self):
        return self.__node1

    @property
    def node2(self):
        return self.__node2

    @property
    def nodes(self):
        return(self.node1, self.node2)

    @property
    def context(self):
        return self.__context

    def __add__(self, other):
        if isinstance(other, DiGraphOperator):
            return OperatorSumList(self + [other])
        elif isinstance(other, (tuple, list)):
            op_sumlist = OperatorSumList(iterable=list(self)+list(other))
            return op_sumlist
        else:
            raise OperatorException(f"__add__ undefined between {self} and {other}")

    def __mul__(self, other):
        # multiplication just distributes across the list
        op_list = gf.recursive_map(lambda x: x*other, self)
        return OperatorSumList(op_list)

    def __rmul__(self, other):
        # multiplication just distributes across the list
        op_list = gf.recursive_map(lambda x: other*x, self)
        return OperatorSumList(op_list)

    def assert_nodes(self):
        '''
        Checks for consistency of nodes in the SumList.
        Also returns the tuple of (node1, node2) if passes.
        '''
        if not self:
            # bool(iterable) returns False if empty
            # "the most pythonic"TM way of checking if empty
            nodes = (None, None)
            return nodes

        if self.nodes != (None, None):
            # compare against header
            current_nodes = self.nodes
            nodes_loc = 'header'
        else:
            # compare against first operator
            current_nodes = self[0].nodes
            nodes_loc = 'i=0'
        for i, element in enumerate(self[1:]):
            if current_nodes != element.nodes:
                raise OperatorException(f"Inconsistent nodes in {self} between \
{nodes_loc} {current_nodes} and i={i+1} {element.nodes}")
        return current_nodes

    def assert_context(self):
        '''
        Checks for consistency of contexts in the SumList.
        Also returns the defining context if passes.
        '''
        if not self:
            # bool(iterable) returns False if empty
            # "the most pythonic"TM way of checking if empty
            return {}

        current_context = self[0].context
        for element in self[1:]:
            if issubclass(current_context['type'], AbstractScalarOperator):
                # current context is Abstract, just overwrite it
                # with the next one
                current_context = element.context
            else:
                # current context is Concrete, need to do comparison
                if current_context != element.context:
                    if issubclass(element.context['type'], AbstractScalarOperator):
                        # context is already concrete ignore further abstract contexts
                        pass
                    else:
                        raise OperatorException(f"Incompatible contexts in SumList. context1 = \
{current_context}, context2 = {element.context}")
        return current_context

    def build(self, context=None):
        if not self:
            raise OperatorException("building empty lists is undefined (no operators to build)")
        if context is None:
            context = self.context
        out = self[0].build(context=context)
        for op in self[1:]:
            out += op.build(context=context)
        return out

    def __repr__(self):
        return 'OpSumList'+super().__repr__()

class OperatorMatrix(np.ndarray):
    '''
    note: currently OperatorMatrix.__mul__(x) just distributes the product with
    x across the matrix elements (inherited from np.ndarray) which is what I
    would expect for Numbers but feels a bit strange for DiGraphOperators.
    I would expect __mul__ to call __matmul__ but that doesn't make sense because an
    OperatorMatrix is not a subset of DiGraphOperator.
    The node signatures between Matrix and Operator don't compose in general.
    Only in the case where the Matrix node1_arr and node2_arr are constant can
    a __matmul__ make sense.
    However that case doesn't come up in my usecases.

    So I guess I will leave it as it is for now and code in that __matmul__ when
    I need it.
    '''
    def __new__(cls, obj=((),), context=None, node1_arr=None, node2_arr=None):
        arr = np.array(obj, dtype=object)
        op_arr = arr.view(OperatorMatrix)
        return op_arr

    def __init__(self, obj=((),), context=None, node1_arr=None, node2_arr=None):
        self.__context = context
        self.node1_arr = node1_arr
        self.node2_arr = node2_arr

        if node1_arr is None:
            self.node1_arr = self.assert_node1_arr()
        else:
            self.assert_node1_arr()
        if node2_arr is None:
            self.node2_arr = self.assert_node2_arr()

    def Number2OpMat(self, num):
        if isinstance(num, Number):
            num_mat = np.zeros(np.shape(self), dtype=object)
            for j, row in enumerate(num_mat):
                for i, el in enumerate(row):
                    if i == j:
                        num_mat[i, i] = AbstractScalarOperator(scalar=num,\
                            node1=self[i, j].node1, node2=self[i, j].node2)
                    else:
                        num_mat[i, j] = NullOperator(node1=self[i, j].node1, node2=self[i, j].node2)
            return OperatorMatrix(num_mat)
        else:
            return OperatorException(f"object num {num} needs to be of type Number not {type(num)}")

    def __add__(self, other):
        if isinstance(other, Number):
            return self + self.Number2OpMat(other)
        elif isinstance(other, OperatorMatrix):
            return super().__add__(other)
        else:
            return NotImplemented

    def __radd__(self, other):
        if isinstance(other, Number):
            return self.Number2OpMat(other) + self
        elif isinstance(other, OperatorMatrix):
            return super().__add__(other)
        else:
            return NotImplemented

    def __neg__(self):
        return (-1)*self

    def __sub__(self, other):
        return self + -other

    def __rsub__(self, other):
        return other + -self

    @property
    def nodes_arr(self):
        return np.dstack([self.node1_arr, self.node2_arr])

    def assert_context(self):
        pass

    def assert_node1_arr(self):
        if self.node1_arr is None:
            new_node1_arr = np.zeros(np.shape(self), dtype=object)
            node1s = [x.node1 for x in self[:, 0]]
        else:
            node1s = [x for x in self.node1_arr[:, 0]]

        # print(node1s)
        for j, row in enumerate(self):
            for i, el in enumerate(row):
                new_node1_arr[i, j] = el.node1
                # print((el.node1, el.node2), node1s[i])
                if el.node1 != node1s[j]:
                    raise OperatorException(f"node1 {el.node1} in OperatorMatrix {self} \
at {i, j} is inconsistent with {node1s[j]} at {i, 0}")
        return new_node1_arr

    def assert_node2_arr(self):
        if self.node2_arr is None:
            new_node2_arr = np.zeros(np.shape(self), dtype=object)
            node2s = [x.node2 for x in self[0, :]]
        else:
            node2s = [x for x in self.node2_arr[0, :]]

        for j, row in enumerate(self):
            for i, el in enumerate(row):
                new_node2_arr[i, j] = el.node2
                if el.node2 != node2s[i]:
                    raise OperatorException(f"node2 {el.node2} in OperatorMatrix {self} \
at {i, j} is inconsistent with {node2s[i]} at {0, j}")
        return new_node2_arr

    @property
    def context(self):
        return self.__context

    def build_hard(self, context=None):
        '''
        Slow build with no memory preallocation trickery.
        But that also means no memory related bugs. I left this
        in because the preallocated build was rumored to have strange
        memory related bugs.
        '''
        built_arr = np.block(gf.recursive_map(lambda x: x.build(context=context), self.tolist()))
        return built_arr

    def allocate_build_memory(self, context=None):
        arr = DenseMatrix("name")
        N = context['N']
        build_shape = np.shape(self)
        for i in range(build_shape[0]):
            arr.add_block(N, i, "name")
        
        return arr

    def build_into(self, arr, context=None):
        '''
        arr should be of type DenseMatrix of the appropriate size

        for the current implementation of memory preallocated matrices
        the procedure is as follows

        0. Allocate the output matrix (appropriate size)
        1. Get all the submatrix views
        2. Call DM.construct()
        3. Populate the views with the elements
        '''

        views_mat = np.zeros(self.shape, dtype=object)
        for j, row in enumerate(self):
            for i, el in enumerate(row):
                views_mat[i, j] = arr.get_sub_matrix_view(i, j, "name")
        arr.construct()
        for j, row in enumerate(self):
            for i, el in enumerate(row):
                views_mat[i, j][:] = el.build(context=context)

        return arr

    def build(self, context=None):
        '''
        The "soft" build that allocates memory for the build matrix.
        Returns a dense matrix instance arr that can be updated with
        self.build_into(arr).
        This method should only be called once in a simulation to
        get the build matrix. All subsequent calls should be build_into.
        '''
        arr = self.allocate_build_memory(context=context)

        return self.build_into(arr, context=context)
