"""
The ``finesse`` module collects the most commonly used classes and functions of
Finesse into a single namespace for convenience. Access to the full packages'
components can be obtained through directly importing the relevant sub-modules which
are listed in the table below.

.. autosummary::
    {contents}

--------
"""

import logging
import locale
from datetime import datetime

# TODO: decide what is to be exposed by "import finesse"
# - below seem fine so far but will want to add more
from finesse.constants import values as constants
from finesse.element import ModelElement, Parameter, model_parameter, Rebuild
from finesse.gaussian import BeamParam
from finesse.model import Model
from finesse.enums import SpatialType
from finesse.plotting import init as init_plotting
from finesse.parse import parse
from finesse.kat import Kat

def add_log_handler(logger, handler, format_str="%(name)-25.25s:%(levelname)-8.8s: %(message)s"):
    """Add log handler to specified logger."""
    handler.setFormatter(logging.Formatter(format_str))
    logger.addHandler(handler)

def set_log_verbosity(level, logger=None):
    """Enable logging to stdout with a certain level"""
    if logger is None:
        logger = LOGGER
    logger.setLevel(level)

# Suppress warnings when code does not include a handler. This is the catch-all root logger.
logging.getLogger().addHandler(logging.NullHandler())

# Create base logger for the package.
LOGGER = logging.getLogger(__name__)
add_log_handler(LOGGER, logging.StreamHandler())
set_log_verbosity(logging.WARNING)

# Set default locale (required for number formatting in log warnings).
locale.setlocale(locale.LC_ALL, "")

# Set version.
try:
    from .version import version, git_revision
    if git_revision == "Unknown":
        __version__ = version
    else:
        __version__ = "r" + git_revision
except Exception:
    # Fallback to default.
    __version__ = '?.?.?'

PROGRAM = "finesse"
DESCRIPTION = "Simulation program for laser interferometers."

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    def compile_toc(entries):
        toc = ''
        for section, objs in entries:
            toc += '\n\n.. rubric:: {}\n\n'.format(section)
            toc += '.. autosummary::\n\n'
            for obj in objs:
                toc += '\t~{}.{}\n'.format(obj.__module__, obj.__name__)
        return toc

    # TODO redo the summary of key exposed classes, functions in the documentation
    #      when the code is in a more stable state
    #from finesse.utilities import check_name
    #from finesse.components import Node, Connector

    #toc = (
    #    ('Components', (
    #        Node, Connector,
    #    )),
    #    ('Element of a Model', (
    #        ModelElement,
    #    )),
    #    ('Gaussian Beams', (
    #        BeamParam,
    #    )),
    #    ('Configuration and Simulation Handlers', (
    #        Model, Simulation,
    #    )),
    #    ('Utilities', (
    #        check_name,
    #    )),
    #)

    __doc__ = __doc__.format(
        contents=_collect_submodules(),
        #toc=compile_toc(toc)
    )
