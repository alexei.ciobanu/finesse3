"""AC analyses"""

import abc
import logging

import numpy as np

from ..solutions import ArraySolution
from ..exceptions import ParameterLocked
from .base import BaseAnalysis

LOGGER = logging.getLogger(__name__)

class BaseXAxis(BaseAnalysis, metaclass=abc.ABCMeta):
    """Abstract multi-axis analysis.

    This runs the model by scanning multiple parameters over individual axes. This is similar in
    functionality to the xaxis, x2axis, x3axis commands in Finesse v2 except it is far more general.
    Any number of axes and parameters can be scanned over if requested.
    """
    def _run(self, *args, **kwargs):
        """Run simulation.

        Parameters
        ----------
        *args
            triples of parameter, array to scan it over, and offset to
            array values
        **kwargs
            Passed to `Model.build`

        Returns
        -------
        :class:`.ArraySolution`
            The results of the axis sweep.
        """
        if len(args) == 0 and self.model is not None:
            models = [self.model]

        elif len(args) % 3 != 0:
            raise Exception("Arguments must be triples of parameter, array of "
                            "values to scan over, and offset to array values.")

        params = args[::3]
        axes   = tuple(np.atleast_1d(_) for _ in args[1::3])
        offsets = args[2::3]

        LOGGER.info("Scanning parameters %s", list(params))

        if len(args) > 0:
            models = [p._model for p in np.atleast_1d(params)]

            if models.count(models[0]) != len(models):
                raise Exception("Parameters provided are associated with different models")

            if models[0].is_built:
                for p in params:
                    if not p.is_tunable:
                        raise ParameterLocked(f"{repr(p)} must set as tunable "
                                              "before building the simulation")
            else:
                for p in params:
                    p.is_tunable = True

        ifo = models[0]
        out_shape = tuple(np.size(_) for _ in axes)

        if ifo.is_built:
            allocated = True
            carrier = ifo.carrier_simulation
            try:
                signal  = ifo.signal_simulation
            except AttributeError:
                signal = None
        else:
            allocated = False
            sims = ifo.build(**kwargs)

            if len(sims) == 1:
                carrier, signal = sims[0], None
            elif len(sims) == 2:
                carrier, signal = sims
            else:
                raise Exception("Unexpected number of Simulation objects")

        self._solution = ArraySolution(ifo, out_shape)

        for i, (value, param) in enumerate(zip(axes, params)):
            setattr(self._solution, f"x{i+1}", value)
            setattr(self._solution, f"p{i+1}", param)

        try:
            if not allocated:
                carrier.__enter__()
                if signal is not None:
                    signal.__enter__()

            # Now we loop over the actual simulation and run each point
            shapes = tuple(np.size(_) for _ in axes)

            for idx in np.ndindex(shapes):
                for p, ax, i, offset in zip(params, axes, idx, offsets):
                    p.value = ax[i] + offset

                carrier.run()

                if signal is not None:
                    signal.run(carrier)

                self._solution.update(idx)
        finally:
            # If the passed model was already built then we need
            # to clean up after ourselves
            if not allocated:
                carrier.__exit__(None, None, None)
                if signal is not None:
                    signal.__exit__(None, None, None)

                ifo.unbuild()

                for p in params:
                    p.is_tunable = False

    @staticmethod
    def sweep_vector(start, stop, steps, mode):
        steps += 1
        if mode == 'lin':
            sweep = np.linspace(start, stop, steps)
        else:
            sweep = np.logspace(np.log10(start), np.log10(stop), steps)
        return sweep


class X0Axis(BaseXAxis):
    pass


class X1Axis(BaseXAxis):
    """Runs a model to scan a parameter between two points for a number of steps.
    The model that is run is retrieved from the parameter reference.

    This should provide an equivalent to the xaxis command in Finesse v2.
    """
    def __init__(self, param, start, stop, steps, mode='lin', offset=0, **kwargs):
        """
        Initialises an single xaxis simulation to later run.

        Parameters
        ----------
        param : :class:`.ModelParameter`
            Parameter of component to scan
        start, stop : float
            Start and end values of the scan
        steps : int
            Number of steps between start and end
        mode : str
            'lin' or 'log' for linear or logarithmic step sizes
        offset : float, optional
            Offset to scanned values. For a given xaxis point,
            `param` will be set to `x[i] + offset`.
        **kwargs
            Passed to `Model.Build` when runs
        """
        super().__init__(param._model)

        self.param = param
        self.start = start
        self.stop  = stop
        self.steps = steps
        self.mode  = mode
        self.offset = offset
        self.kwargs = kwargs


    def _run(self):
        """Run simulation.

        Returns
        -------
        :class:`.ArraySolution`
            The results of the axis sweep.
        """
        return super()._run(self.param,
                            self.sweep_vector(self.start, self.stop, self.steps, self.mode),
                            self.offset,
                            **self.kwargs)


class X2Axis(BaseXAxis):
    """Runs a model to scan a parameter between two points for a number of steps.
    The model that is run is retrieved from the parameter reference.

    This should provide an equivalent to the x2axis command in Finesse v2.
    """

    def __init__(self, param1, start1, stop1, steps1, param2, start2, stop2,
            steps2, mode1='lin', mode2='lin', offset1=0, offset2=0, **kwargs):
        """
        Initialises a 2D parameter sweep simulation to later run.
        Param2 is in the inner loop and param1 in the outer loop.

        Parameters
        ----------
        param1, param2 : :class:`.ModelParameter`
            Parameter of component to scan
        start1, stop1, start2, stop2 : float
            Start and end values of the scan
        steps1, steps2 : int
            Number of steps between start and end
        mode1, mode2 : str
            'lin' or 'log' for linear or logarithmic step sizes for axis 1 and 2
        offset1, offset2 : float, optional
            Offsets to scanned values. For a given xaxis point,
            `param` will be set to `x[i] + offset`.
        **kwargs
            Passed to `Model.Build` when  run
        """
        assert(param1._model is param2._model)
        super().__init__(param1._model)

        self.param1 = param1
        self.start1 = start1
        self.stop1  = stop1
        self.steps1 = steps1
        self.mode1  = mode1
        self.offset1 = offset1
        self.param2 = param2
        self.start2 = start2
        self.stop2  = stop2
        self.steps2 = steps2
        self.mode2  = mode2
        self.offset2 = offset2
        self.kwargs = kwargs

    def _run(self):
        """Run simulation.

        Returns
        -------
        :class:`.ArraySolution`
            The results of the axis sweep.
        """
        return super()._run(self.param1, self.sweep_vector(self.start1, self.stop1, self.steps1, self.mode1), self.offset1,
                            self.param2, self.sweep_vector(self.start2, self.stop2, self.steps2, self.mode2), self.offset2,
                            **self.kwargs)


class X3Axis(BaseXAxis):
    """Runs a model to scan a parameter between two points for a number of steps.
    The model that is run is retrieved from the parameter reference.

    This should provide an equivalent to the x3axis command in Finesse v2.
    """

    def __init__(self, param1, start1, stop1, steps1, param2, start2, stop2,
            steps2, param3, start3, stop3, steps3, mode1='lin', mode2='lin',
            mode3='lin', offset1=0, offset2=0, offset3=0, **kwargs):
        """
        Initialises a 3D parameter sweep simulation to later run.
        Param3 is in the inner loop and param1 in the outer loop.

        Parameters
        ----------
        param1, param2, param3 : :class:`.ModelParameter`
            Parameter of component to scan
        start1, stop1, start2, stop2, start3, stop3 : float
            Start and end values of the scan
        steps1, steps2, steps3 : int
            Number of steps between start and end
        mode1, mode2, mode3 : str
            'lin' or 'log' for linear or logarithmic step sizes for each axis
        offset1, offset2, offset3 : float, optional
            Offsets to scanned values. For a given xaxis point,
            `param` will be set to `x[i] + offset`.
        **kwargs
            Passed to `Model.Build` when  run
        """
        assert(param1._model is param2._model)
        super().__init__(param1._model)

        self.param1 = param1
        self.start1 = start1
        self.stop1  = stop1
        self.steps1 = steps1
        self.mode1  = mode1
        self.offset1 = offset1
        self.param2 = param2
        self.start2 = start2
        self.stop2  = stop2
        self.steps2 = steps2
        self.mode2  = mode2
        self.offset2 = offset2
        self.param3 = param3
        self.start3 = start3
        self.stop3  = stop3
        self.steps3 = steps3
        self.mode3  = mode3
        self.offset3 = offset3
        self.kwargs = kwargs

    def _run(self):
        """Run simulation.

        Returns
        -------
        :class:`.ArraySolution`
            The results of the axis sweep.
        """
        return super()._run(self.param1, self.sweep_vector(self.start1, self.stop1, self.steps1, self.mode1), self.offset1,
                            self.param2, self.sweep_vector(self.start2, self.stop2, self.steps2, self.mode2), self.offset2,
                            self.param3, self.sweep_vector(self.start3, self.stop3, self.steps3, self.mode3), self.offset3,
                            **self.kwargs)
