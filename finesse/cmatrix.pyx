"""
Sparse matrix objects with factorisation and solving routines performed
via KLU.
"""
import weakref

import numpy as np
cimport numpy as np
import cython
from cython cimport view

from cython.operator cimport dereference as deref
from libc.stdio cimport printf

import array                # Python builtin module
from cpython cimport array  # array.pxd / arrayarray.h

from finesse.cmath cimport complex_t # type: np.complex128_t (i.e. double complex)

cdef extern from "stdlib.h":
    void  free(void* ptr)
    void* malloc(size_t size)
    void* calloc(size_t nitems, size_t size)
    void* realloc(void* ptr, size_t size)


cdef extern from "klu.h":
    cdef int KLU_OK = 0
    cdef int KLU_SINGULAR = 1
    cdef int KLU_OUT_OF_MEMORY = -2
    cdef int KLU_INVALID = -3
    cdef int KLU_TOO_LARGE = -4

    ctypedef long SuiteSparse_long

    ctypedef struct klu_l_symbolic:
        pass

    ctypedef struct klu_l_numeric:
        pass

    ctypedef struct klu_l_common:
        int ordering
        int scale
        int btf
        int maxwork
        int status

    cdef int klu_l_defaults(klu_l_common* Common)
    cdef klu_l_symbolic *klu_l_analyze(SuiteSparse_long num_eqs,
                                       SuiteSparse_long *col_ptr,
                                       SuiteSparse_long *row_idx,
                                       klu_l_common* Common)

    cdef klu_l_numeric *klu_zl_factor(SuiteSparse_long *col_ptr,
                                      SuiteSparse_long *row_idx,
                                      double *values,
                                      klu_l_symbolic *Symbolic,
                                      klu_l_common *Common)


    cdef void        klu_zl_refactor(SuiteSparse_long *col_ptr,
                                     SuiteSparse_long *row_idx,
                                     double *values,
                                     klu_l_symbolic *Symbolic,
                                     klu_l_numeric *Numeric,
                                     klu_l_common *Common)


    cdef int klu_zl_sort(klu_l_symbolic *Symbolic, klu_l_numeric *Numeric, klu_l_common* Common);

    cdef int klu_zl_free_numeric(klu_l_numeric **Numeric, klu_l_common *Common)
    cdef int klu_l_free_symbolic(klu_l_symbolic **Symbolic, klu_l_common *Common)

    cdef SuiteSparse_long klu_zl_solve (klu_l_symbolic *, klu_l_numeric *, SuiteSparse_long, SuiteSparse_long, double *, klu_l_common *) ;
    cdef SuiteSparse_long klu_zl_tsolve(klu_l_symbolic *, klu_l_numeric *, SuiteSparse_long, SuiteSparse_long, double *, SuiteSparse_long, klu_l_common * ) ;

cdef sortkey(x):
    return int(x.to)

cdef status_string(int status):
    if status == KLU_SINGULAR:
        return "KLU_SINGULAR"
    elif status == KLU_OUT_OF_MEMORY:
        return "KLU_OUT_OF_MEMORY"
    elif status == KLU_INVALID:
        return "KLU_INVALID"
    elif status == KLU_TOO_LARGE:
        return "KLU_TOO_LARGE"
    else:
        return "UNKNOWN"

ctypedef np.npy_intp SIZE_t

class _Column(object):
    def __init__(self, size, index, name):
        assert(size>0)
        self.size = size
        self.index = index
        self.name = name
        self.submatrices = []


class _SubMatrix(object):
    def __init__(self, type_, from_size, to_size, to, name):
        assert(from_size>0)
        assert(to_size>0)
        self.to = to
        self.type = type_
        self.name = name
        self.columns = from_size #
        self.rows = to_size

cdef class CCSMatrix(object):
    cdef:
        unicode __name
        dict __indexes

        SuiteSparse_long num_eqs
        int allocated
        int num_nodes

        SuiteSparse_long   *row_idx
        SuiteSparse_long   *col_ptr
        complex_t *values
        complex_t *rhs
        np.ndarray rhs_view

        SuiteSparse_long   __nnz
        int[:, :] viewa
        dict sub_columns
        dict diag_map
        list __callbacks
        object __weakref__


    def __init__(self, name):
        self.__name = name
        self.__indexes = {}
        self.sub_columns = {}
        self.num_nodes   = 0
        self.num_eqs     = 0
        self.allocated   = 0

        self.values  = NULL
        self.col_ptr = NULL
        self.row_idx = NULL
        self.diag_map = {}
        self.__nnz  = 0
        self.__callbacks = []

    @property
    def num_equations(self): return int(self.num_eqs)
    
    @property
    def indexes(self): return self.__indexes

    @property
    def name(self): return self.__name

    def get_sub_matrix_view(self, Py_ssize_t from_node, Py_ssize_t to_node, unicode name):
        mat = SubCCSMatrixView(self, from_node, to_node, name)
        self.add_submatrix(from_node, to_node, name, mat, type_='m')
        return mat
    
    def get_sub_diagonal_view(self, Py_ssize_t from_node, Py_ssize_t to_node, unicode name):
        mat = SubCCSMatrixViewDiagonal(self, from_node, to_node, name)
        self.add_submatrix(from_node, to_node, name, mat, type_='d')
        return mat

    def __dealloc__(self):
        if self.col_ptr: free(self.col_ptr)
        if self.row_idx: free(self.row_idx)
        if self.values:  free(self.values)
        if self.rhs:     free(self.rhs)

    cpdef add_diagonal_elements(
        self,
        SuiteSparse_long Neqs,
        SuiteSparse_long index,
        unicode name,
        is_diagonal = True
    ):
        """
        Adds a submatrix to the matrix along its diagonal. This defines what equations exist
        in the matrix, the submatrix values default to `1`, but a diagonal view is returned that
        allows them to be modified. Before other submatrices can be added to the matrix the
        diagonal must be specfied and how many equations it represents.

        Parameters
        ----------
        Neqs : Py_ssize_t
            Number of equations this submatrix represents
        _index : long
            Subcolumn index
        name : unicode
            Name used to indentify this coupling in the matrix for debugging
        """
        if index in self.sub_columns:
            raise Exception("Diagonal elements already specified at index {}".format(index))

        self.sub_columns[index] = _Column(Neqs, index, name)
        if is_diagonal:
            self.sub_columns[index].submatrices.append(_SubMatrix('d', Neqs, Neqs, index, name))
            mat = SubCCSMatrixViewDiagonal(self, index, index, name)
        else:
            self.sub_columns[index].submatrices.append(_SubMatrix('m', Neqs, Neqs, index, name))
            mat = SubCCSMatrixView(self, index, index, name)
        # Record what RHS index/number of equations this submatrix will start in
        self.diag_map[index] = self.num_eqs
        self.num_eqs += Neqs

        self.__callbacks.append(mat)
        return mat

    cpdef add_submatrix(self, SuiteSparse_long _from, SuiteSparse_long _to,
                        unicode name, callback=None, type_='m'):
        """
        Adds a submatrix to the matrix. The nomenclature of `_from` and `_to` refer to the variable
        dependency of the equations this submatrix represents, i.e. the equations in submatrix
        `_to` depends on the values in `-from`. Therefore `_from` is the subcolumn index and `_to`
        is the subrow index.

        Parameters
        ----------
        _from : long
            Subcolumn index
        _to : long
            Subcolumn index
        name : unicode
            Name used to indentify this coupling in the matrix for debugging
        callback : function()
            A callback function that will be called once the matrix has been constructed
        type_ : char (optional)
            Either 'm' for a full submatrix or 'd' for a diagonal element only submatrix
        """
        if _from not in self.sub_columns:
            raise Exception("Must add a diagonal submatrix at index {} first for this subcolumn".format(_from))
        if _to   not in self.sub_columns:
            raise Exception("Must add a diagonal submatrix at index {} first for this subcolumn".format(_to))

        _to_size = self.sub_columns[_to].size
        _from_size = self.sub_columns[_from].size

        self.sub_columns[_from].submatrices.append(_SubMatrix(type_, _from_size, _to_size, _to, name))

        if callback: self.__callbacks.append(callback)

    cpdef set_rhs(self, index, value):
        self.rhs_view[index] = value

    cpdef construct(self):
        """
        Constructing the matrix involves taking the metadata submatrix positions
        throughout the matrix and allocating the memory and building the various
        CCS matrix structures. After this the matrix can be populated and solved.

        Notes
        -----
        ddb: This only works if all the submatrices have the same size!
        """

        cdef SuiteSparse_long i      = 0
        cdef SuiteSparse_long j      = 0
        cdef SuiteSparse_long cnnz   = 0 # current element number
        cdef SuiteSparse_long crow   = 0 # current row
        cdef SuiteSparse_long ccol   = 0 # current column in a submatrix
        cdef SuiteSparse_long nsc    = len(self.sub_columns) # number of sub_columns
        cdef SuiteSparse_long nnz    = 0

        #self.num_eqs = 0


        for col in self.sub_columns.values():
            # sort so that the the submatrices are in row order
            col.submatrices.sort(key=sortkey)
            assert(col.size > 0)
            #self.num_eqs += col.size

            for sm in col.submatrices:
                # count how many elements per column for allocating memory
                if sm.type == 'm': # matrix
                    nnz += sm.rows * sm.columns
                elif sm.type == 'd': # diagonal
                    nnz += col.size
                else:
                    raise Exception("Unhandled")

        assert(self.num_eqs > 0)

        self.col_ptr  = <SuiteSparse_long*> malloc(sizeof(SuiteSparse_long) * (self.num_eqs + 1))
        self.row_idx  = <SuiteSparse_long*> malloc(sizeof(SuiteSparse_long) * nnz)
        self.values   = <complex_t*>        calloc(nnz, sizeof(complex_t))
        self.rhs      = <complex_t*>        calloc(self.num_eqs, sizeof(complex_t))
        self.rhs_view = np.asarray(<complex_t[:self.num_eqs]>self.rhs)
        
        #print(self.diag_map)

        for i, col in enumerate(self.sub_columns.values()): # For each subcolumn...
            col.start = cnnz # record index where this column starts

            for j in range(col.size): # then for each actual column in the subcolumn
                # set the starting location of the column in the pointer vector
                self.col_ptr[ccol] = cnnz

                for sm in col.submatrices: # select each submatrix...
                    crow = self.diag_map[sm.to]

                    # then set the elements in the column for...
                    if sm.type == 'm': # a matrix
                        for k in range(sm.rows):
                            self.row_idx[cnnz+k] = crow + k
                            # Set a default position, real=col, imag=row, helps with debugging
                            self.values[cnnz+k] = complex(ccol, self.row_idx[cnnz+k])

                        # keep track of how many nnz we have actually done...
                        cnnz += sm.rows
                        crow += sm.rows
                    elif sm.type == 'd': # a diagonal
                        self.row_idx[cnnz] = crow + j # set the diagonal position
                        # set all diagonals to be -I, saves setting the main diagonal later anyway
                        # any component diagonals will be overwritten by the components
                        self.values[cnnz] = complex(-1,0)
                        cnnz += 1
                        crow += 1
                    else:
                        raise Exception("Unhandled")

                # increment to the next column
                ccol += 1

        self.col_ptr[self.num_eqs] = cnnz
        self.__nnz = cnnz

        for cb in self.__callbacks:
            cb._updateview_()

    cpdef get_matrix(self, SuiteSparse_long _from, SuiteSparse_long _to):
        cdef:
            SuiteSparse_long cdx = 0 # index where subcolumn starts in values array
            SuiteSparse_long rowcount = 0

            str _type = ""
            SuiteSparse_long sdx = 0 # actual row index where submatrix starts

            complex_t[:] ptr

        # actual index in sparse format where this subcolumn starts
        cdx = self.sub_columns[_from].start

        # Loop over each submatrix in the subcolumn
        for _ in self.sub_columns[_from].submatrices:
            if _.to == _to:
                _type = _.type
                sdx   = rowcount
                rows  = _.rows

            if _.type == "d":
                rowcount += 1
            elif _.type == "m":
                rowcount += _.rows
            else:
                raise Exception("Unexpected result {}".format(_[0]))

        cols = self.sub_columns[_from].size

        # get a memoryview of the entire subcolumn
        ptr = <complex_t[:(rowcount*cols)]>(self.values + cdx)
        # numpy array view of the matrix which we reshape into the proper
        # subcolumn size
        arr = np.asarray(ptr).reshape(cols, rowcount).T

        # now we return a numpy view of just the part of the matrix requested
        if _type == "d":
            return arr[sdx, :]
        elif _type == "m":
            return arr[sdx:(sdx+rows), :]
        else:
            raise Exception("Submatrix connecting {} -> {} does not exist".format(_from, _to))

    @property
    def num_equations(self):
        return self.num_eqs

    def get_matrix_elements(self):
        vals = []
        ccol = -1
        for i in range(self.__nnz):
            if self.col_ptr[ccol+1] == i:
                ccol += 1
            vals.append((ccol, self.row_idx[i], self.values[i]))
            
        return vals
        
    def print_matrix(self):
        cidx = {}
        C = 0

        for _ in self.sub_columns:
            for __ in self.sub_columns[_].submatrices:
                if __.name[0] == 'I':
                    for i in range(__.rows):
                        cidx[C] = f"{__.name} mode={i}"
                        C+=1


        N = len(self.sub_columns)
        Ms = np.zeros((N,N),dtype=str)
        Ms[:] = ' '

        for _ in self.sub_columns:
            for __ in self.sub_columns[_].submatrices:
                Ms[__.to, self.sub_columns[_].index] = __.type

        #print(Ms)

        print("")
        print(f"Matrix {self.name}: nnz={self.__nnz} neqs={self.num_eqs}")
        print("    (col, row) = value")
        ccol = -1
        for i in range(self.__nnz):
            if self.col_ptr[ccol+1] == i:
                ccol += 1

            print(f"    ({ccol}, {self.row_idx[i]}) = {self.values[i]} : {cidx[ccol]}->{cidx[self.row_idx[i]]}")

    def print_rhs(self):
        print("")
        print(f"Vector {self.name}: neqs={self.num_eqs}")
        print("    (row) = value")

        for i in range(self.num_eqs):
            print(f"    ({i}) = {self.values[i]}")


    def clear_rhs(self):
        self.rhs_view[:] = 0


cdef class SubCCSMatrixView:
    """
    This class represents a sub-matrix view of a CCS sparse matrix. This allows
    code to access and set values without worrying about the underlying
    sparse compression being used. Although so far this is just for CCS
    formats.

    This object will get a view of a n-by-m sub-matrix starting at index (i,j).
    The values of his matrix will be set initially to the coordinates.
    """
    cdef object M
    cdef Py_ssize_t _from, _to
    cpdef np.ndarray A

    def __init__(self, CCSMatrix Matrix, Py_ssize_t _from, Py_ssize_t _to, unicode name):
        self.M      = weakref.ref(Matrix)
        self._from  = _from
        self._to    = _to

        #Matrix.add_submatrix(_from, _to, name, self, type_='m')

    @property
    def from_idx(self): return self._from
    @property
    def to_idx(self): return self._to

    @property
    def shape(self):
        return self.M().get_matrix(self._from, self._to).shape

    @property
    def view(self):
        return self.A

    cpdef _updateview_(self):
        self.A = self.M().get_matrix(self._from, self._to)

    def __getitem__(self, key):
        return self.A[key]

    def __setitem__(self, slice key, np.ndarray[np.complex128_t, ndim=2, mode="c"] value):
        if key == slice(None,None,None):
            # replace all elements
            self.A[:] = value
        else:
            print(key, value)
            raise NotImplemented()


cdef class SubCCSMatrixViewDiagonal:
    """
    This class represents a sub-matrix view of a CCS sparse matrix. This allows
    code to access and set values without worrying about the underlying
    sparse compression being used. Although so far this is just for CCS
    formats.

    This object will get a view of a n-by-m sub-matrix starting at index (i,j).
    The values of his matrix will be set initially to the coordinates.
    """
    cdef object M
    cdef Py_ssize_t _from, _to
    cpdef np.ndarray A

    def __init__(self, CCSMatrix Matrix, Py_ssize_t _from, Py_ssize_t _to, unicode name):
        self.M      = weakref.ref(Matrix)
        self._from  = _from
        self._to    = _to

        #Matrix.add_submatrix(_from, _to, name, self, type_='d')

    @property
    def from_idx(self): return self._from
    @property
    def to_idx(self): return self._to

    @property
    def shape(self):
        return self.M().get_matrix(self._from, self._to).shape

    @property
    def view(self):
        return self.A

    cpdef _updateview_(self):
        self.A = self.M().get_matrix(self._from, self._to)

    def __getitem__(self, key):
        return self.A[key]

    def __setitem__(self, slice key, np.ndarray[np.complex128_t, ndim=1, mode="c"] value):
        if key == slice(None,None,None):
            # replace all elements
            self.A[:] = value
        else:
            print(key, value)
            raise NotImplemented()


cdef class KLUMatrix(CCSMatrix):
    """An object representation of a CCS matrix with methods to factor
    and solve the matrix via KLU.
    """
    cdef klu_l_common Common
    cdef klu_l_numeric* Numeric
    cdef klu_l_symbolic* Symbolic

    def __cinit__(self, unicode name, int klu_ordering=0, int klu_scale=2, int klu_btf=1, int klu_maxwork=0):
        self.Symbolic = NULL
        self.Numeric  = NULL

        klu_l_defaults(&self.Common)

        self.Common.ordering = klu_ordering
        self.Common.scale    = klu_scale
        self.Common.btf      = klu_btf
        self.Common.maxwork  = klu_maxwork

    def __dealloc__(self):
        if self.Numeric:  klu_zl_free_numeric(&self.Numeric, &self.Common)
        if self.Symbolic: klu_l_free_symbolic(&self.Symbolic, &self.Common)

    def factor(self):
        """Factor the matrix."""
        self.Symbolic = klu_l_analyze(self.num_eqs,
                                      self.col_ptr,
                                      self.row_idx,
                                      &self.Common)

        if self.Common.status != KLU_OK:
            raise Exception("An error occurred in KLU during analysis: STATUS={} ({})".format(self.Common.status, status_string(self.Common.status)))

        self.Numeric = klu_zl_factor(self.col_ptr,
                                     self.row_idx,
                                     <double*>self.values,
                                     self.Symbolic,
                                     &(self.Common))

        if self.Common.status != KLU_OK:
            raise Exception("An error occurred in KLU during factoring: STATUS={} ({})".format(self.Common.status, status_string(self.Common.status)))

        klu_zl_sort(self.Symbolic, self.Numeric, &(self.Common))

        if self.Common.status != KLU_OK:
            raise Exception("Sort failed. STATUS={} ({})".format(self.Common.status, status_string(self.Common.status)))

    cpdef refactor(self):
        """Re-factor the matrix."""
        klu_zl_refactor(self.col_ptr,
                        self.row_idx,
                         <double*>self.values,
                         self.Symbolic,
                         self.Numeric,
                         &(self.Common))

        if self.Common.status != KLU_OK:
            raise Exception("An error occurred in KLU during refactor: STATUS={} ({})".format(self.Common.status, status_string(self.Common.status)))



    cpdef solve(self, int transpose=False, conjugate=False):
        """
        solve(self, transpose, conjugate)

        Solve the matrix with options for transposing and conjugating.

        If `transpose` is False, solves the linear system :math:`Ax = b` using the
        ``Symbolic`` and ``Numeric`` objects stored by this class.

        Otherwise, solves the linear system :math:`A^T x = b` or :math:`A^H x = b`. The
        `conjugate` option is zero for :math:`A^T x = b` or non-zero for :math:`A^H x = b`.

        Parameters
        ----------
        transpose : bool
            Flag determining whether to solve the transpose of the matrix.

        conjugate : bool
            Flag determining whether to solve :math:`A^T x =b` or :math:`A^H x = b`
            for the transposed linear system.

        Returns
        -------
        out : np.ndarray
            The (negative) solution vector.
        """
        if transpose:
            klu_zl_tsolve(self.Symbolic, self.Numeric, self.num_eqs, 1, <double*>self.rhs, conjugate, &self.Common);
        else:
            klu_zl_solve( self.Symbolic, self.Numeric, self.num_eqs, 1, <double*>self.rhs, &self.Common);

        rtn = self.rhs_view.copy()
        rtn.flags.writeable = False

        return rtn
