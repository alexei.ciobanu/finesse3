"""
A sub-module containing the configuration container
class :class:`.Model` which is used for building
and manipulating interferometer systems.
"""

from dataclasses import dataclass
import enum
import numbers
import weakref
import numpy as np
import networkx as nx
import logging

from finesse import constants

from finesse.components import Port, Connector, Space, FrequencyGenerator, Cavity, Wire, Joint, Variable
from finesse.components.node import QSetMode, NodeType
from finesse.components.general import CouplingType

from finesse.detectors import Detector, AmplitudeDetector

from finesse.element import ModelElement, Symbol
from finesse.enums import SpatialType
from finesse.exceptions import NodeException, ComponentNotConnected
from finesse.freeze import canFreeze, Freezable
from finesse.gaussian import BeamParam
from finesse.paths import OpticalPath
from finesse.tracing import Tracer, TraceSolution, TraceType, TraceDataEntry
from finesse.utilities import valid_name, pairwise
from finesse.frequency import Fsig

from finesse.simulations import KLUSimulation

from collections import Iterable, OrderedDict, defaultdict
from copy import deepcopy

LOGGER = logging.getLogger(__name__)

@dataclass
class PhaseConfig:
    ZERO_K00: bool
    ZERO_TEM00_GOUY: bool

@dataclass
class BeamTraceLogging:
    ABCDs: bool
    results: bool
    trace_info: bool

@canFreeze
class Model(object):
    """
    Optical configuration class for handling models of interferometers.

    This class stores the interferometer configuration as a directed
    graph and contains methods to interface with this data structure.

    .. note::
        The module :mod:`pickle` **cannot** be used on instances of this class.
    """
    def __init__(self, maxtem=None, selected_modes=None):
        """Constructs a new, empty `Model` instance.

        If both `maxtem` and `selected_modes` are `None` then the model will
        represent a plane-wave model, otherwise it will be a modal model. Note
        that this can be altered after construction via :attr:`Model.maxtem` or
        :meth:`Model.select_modes`.

        Parameters
        ----------
        maxtem : int, optional
            The maximum transverse electromagnetic mode order, defaults to `None`
            indicating a plane wave model. If this value is specified, then the model
            will automatically be switched to a modal basis.

        selected_modes : sequence or str, optional
            Specific HOMs to select for modelling. Can be given as a sequence of
            strings (e.g: ``["00", "21"]`` to select the TEM00 and TEM21 modes), or
            a sequence of two-element tuples (e.g: ``[(0,0), (1, 3)]`` to select the
            TEM00 and TEM13 modes).

            OR a string identifier for the type of modes to include. This can
            be one of `"even"`, `"odd"`, `"tangential"`, `"sagittal"`, `"x"` or `"y"`.
        """

        # graph storage
        self.__network = nx.DiGraph()
        # need to store optical network separately for tracing correct
        # paths through optical-optical couplings only
        self.__optical_network = nx.DiGraph()

        # components and detectors
        self.__cavities = OrderedDict()
        self.__components = OrderedDict()
        self.__detectors = OrderedDict()

        # frequency storage
        self.__frequencies = set()
        self.__frequency_change_callbacks = []
        self.__freq_map = None
        self._frequency_generators = []

        # HOM related attributes
        self.__homs = np.zeros(1, dtype=(np.intc, 2)) # [(0, 0)]
        self.__homs_map = OrderedDict()
        self.__update_homs_map()

        self.alternate_name_map = {}

        if maxtem is not None and selected_modes is not None:
            raise ValueError(f"maxtem and selected_modes arguments cannot be "
                             "specified simulataneously.")

        # set to plane-wave model here just for initialisation reasons, this
        # will be reset in either maxtem setter or self.select_modes method anyway
        # if either of these arguments were specified
        self.__spatial_type = SpatialType.PLANE
        self.__maxtem = None

        if maxtem is not None:
            self.maxtem = maxtem
        if selected_modes is not None:
            self.select_modes(selected_modes)

        self.phase_config = PhaseConfig(ZERO_K00=True, ZERO_TEM00_GOUY=True)

        ## beam tracing attributes
        self._tracer = Tracer(self)
        self.__gauss_commands = OrderedDict()
        self.__last_trace = None
        self._retrace_required = True
        self._trace_changed = False
        self.__force_retrace_all = False
        self.__no_retrace = False
        self.__beam_trace_args = {
            "startnode" : None,
            "order" : None,
            "set_symmetric" : True,
            "symbolic" : False,
            "store" : True
        }
        self.__prev_beam_trace_args = None
        self.beam_trace_logging = BeamTraceLogging(
            ABCDs=False, results=False, trace_info=False
        )

        self.__simulations = []  # list of simulations this model is associated with

        self.xaxis  = None
        self.x2axis = None
        self.yaxis  = None

        # constants
        # TODO (sjr) determine where to store and set these properly
        self.__lambda0 = 1064e-9
        # Arbitrary factor epsilon, for conversion between e.g. mechanical
        # motion and modulation at a mirror's surface.
        self.__epsilon = self.__lambda0 / (2 * np.pi)
        # ratio of epsilon_0/c, used in converting
        # between power and optical fields. Typically
        # use just renormalise and use 1, putting
        # optical fields in units of sqrt{W}
        self._EPSILON0_C = 1
        self._UNIT_VACUUM = 1

        self.__elements = OrderedDict() # list of all model elements

        # signal element
        self.add(Fsig('fsig', None))

        self._freeze()

    def __deepcopy__(self, memo):
        new = object.__new__(type(self))
        memo[id(self)] = new
        new.__dict__.update(deepcopy(self.__dict__, memo))

        # update all the weakrefs we have in the network
        for n in new.network.nodes:
            new.network.nodes[n]['weakref'] = weakref.ref(memo[id(self.network.nodes[n]['weakref']())])
            new.network.nodes[n]['owner'] = weakref.ref(memo[id(self.network.nodes[n]['owner']())])

        for e in new.network.edges:
            new.network.edges[e]['in_ref'] = weakref.ref(memo[id(self.network.edges[e]['in_ref']())])
            new.network.edges[e]['out_ref'] = weakref.ref(memo[id(self.network.edges[e]['out_ref']())])
            new.network.edges[e]['owner'] = weakref.ref(memo[id(self.network.edges[e]['owner']())])

        # need this to make sure optical_network of new is current
        # but commenting out for now as untested
        #new._update_optical_network()

        return new

    @property
    def parameters(self):
        return {p.ref.name : p.ref for c in self.components for p in c.parameters}

    @property
    def elements(self):
        """
        Dictionary of all the model elements with the keys as their names
        """
        return self.__elements.copy()

    @property
    def network(self):
        """The directed graph object containing the optical
        configuration as a :class:`networkx.DiGraph` instance.

        The `network` stores :class:`.Node` instances as nodes and
        :class:`.Space` instances as edges, where the former has
        access to its associated component via :attr:`Node.component`.

        See the :mod:`networkx` documentation for further details and
        a reference to the data structures and algorithms within this
        module.

        :getter: Returns the directed graph object containing the configuration
                 (read-only).
        """
        return self.__network

    @property
    def optical_network(self):
        """A copy of the directed graph object stored by :attr:`Model.network`
        but only containing nodes of type :class:`.OpticalNode` and only with
        edges that have couplings to optical nodes.

        :getter: Returns the optical-only directed graph (read-only).
        """
        return self.__optical_network

    def __nodes_of(self, node_type):
        return [attr['weakref']() for _, attr in self.__network.nodes(data=True)
                if attr['weakref']().type == node_type]

    @property
    def optical_nodes(self):
        """The optical nodes stored in the model.

        :getter: Returns a list of all the optical nodes in the model (read-only).
        """
        return self.__nodes_of(NodeType.OPTICAL)

    @property
    def mechanical_nodes(self):
        """The mechanical nodes stored in the model.

        :getter: Returns a list of all the mechanical nodes in the model (read-only).
        """
        return self.__nodes_of(NodeType.MECHANICAL)

    @property
    def electrical_nodes(self):
        """The electrical nodes stored in the model.

        :getter: Returns a list of all the electrical nodes in the model (read-only).
        """
        return self.__nodes_of(NodeType.ELECTRICAL)

    # NOTE: using the same naming convention as Finesse 2 for now
    @property
    def gauss_commands(self):
        """A dictionary of optical nodes that have beam parameters set manually - stores
        the nodes themselves as keys and the corresponding beam parameters as values.

        :getter: Returns the dictionary of user set beam parameter nodes (read-only).
        """
        return self.__gauss_commands

    @property
    def lambda0(self):
        """The default wavelength to use for the model.

        :getter: Returns wavelength in meters
        """
        return self.__lambda0

    @property
    def f0(self):
        """The default frequency to use for the model.

        :getter: Returns frequency in Hertz
        """
        return constants.C_LIGHT / self.__lambda0

    @property
    def epsilon(self):
        """Scaling factor for mechanical motion to optical modulation.

        :getter: Returns epsilon.
        :setter: Sets epsilon.
        """
        return self.__epsilon

    @epsilon.setter
    def epsilon(self, value):
        self.__epsilon = value

    @property
    def spatial_type(self):
        """The spatial type of the model - i.e. either plane wave or
        modal based.

        :getter: The model spatial type (read-only).
        """
        return self.__spatial_type

    @property
    def is_modal(self):
        return self.__spatial_type == SpatialType.MODAL

    @property
    def homs(self):
        """An array of higher-order modes (HOMs) included in the model.

        :getter: Returns an array of the HOMs in the model.
        :setter: Sets the HOMs to be included in the model. See :meth:`Model.select_modes`
                 for the options available.
        """
        return self.__homs

    @homs.setter
    def homs(self, value):
        self.select_modes(value)

    def __update_homs_map(self):
        self.__homs_map.clear()
        for i, (n, m) in enumerate(self.__homs):
            self.__homs_map[(n, m)] = i

    @property
    def mode_index_map(self):
        """An ordered dictionary where the key type is the modes in the model
        and the mapped type is the index of the mode.

        :getter: Returns the map of modes to indices (read-only).
        """
        return self.__homs_map

    @property
    def maxtem(self):
        """The maximum Tranverse Electro-magnetic Mode (TEM) order
        associated with the model.

        :getter: Returns the maximum modal order of the model.
        :setter: Sets the maximum modal order of the model by
                 filling :attr:`Model.homs` with all the mode indices
                 up to the specified order.
        """
        return self.__maxtem

    @maxtem.setter
    def maxtem(self, value):
        if value is None or (isinstance(value, str) and value.casefold() == "off"):
            if self.__spatial_type == SpatialType.MODAL:
                self.__spatial_type = SpatialType.PLANE
                LOGGER.info(f"Turning off HOMs and switching model: {self} to plane waves")
            self.__homs = np.zeros(1, dtype=(np.intc, 2))
            self.__maxtem = None

        else:
            if (value < 0
                    or (hasattr(value, "is_integer")
                        and not value.is_integer()
                        and not isinstance(value, numbers.Integral))):
                raise ValueError("maxtem must be a non-negative integer")

            if self.spatial_type == SpatialType.PLANE:
                self.__spatial_type = SpatialType.MODAL
                LOGGER.info(f"Turning on HOMs and switching model: {self} to modal")

            value = int(value)
            N = int((1 + value) * (2 + value) / 2)
            self.__homs = np.zeros(N, dtype=(np.intc, 2))

            count = 0
            for n in range(1 + value):
                for m in range(1 + value):
                    if n + m <= value:
                        self.__homs[count] = (n, m)
                        count += 1

            self.__maxtem = value

        self.__update_homs_map()

    def select_modes(self, select, maxtem=None):
        """Select the HOMs to include in the model.

        Either an iterable of mode indices (`modes` arg) or a string (`stype` arg)
        indicating which modes to include, can be specified.

        Parameters
        ----------
        select : sequence or str
            An iterable of mode indices formatted as `[(n1, m1), (n2, m2), ...]`. This
            can also be given as a list of strings, e.g. `["00", "11", ...]`.

            OR a string identifier for the type of modes to include. This can
            be one of `"even"`, `"odd"`, `"tangential"`, `"sagittal"`, `"x"` or `"y"`.

        maxtem : int
            Optional maximum mode order, applicable only for when `select` is a string.
            If this is not given when `select` is a string, then the maximum mode order
            will be `self.maxtem`.
        """
        if select is None:
            raise ValueError("Argument: select must not be None.")

        if self.__spatial_type == SpatialType.PLANE:
            self.__spatial_type = SpatialType.MODAL
            LOGGER.info(f"Turning on HOMs and switching model: {self} to modal.")

        if isinstance(select, str):
            switch = {
                "even" : self.select_even_modes, "odd" : self.select_odd_modes,
                "tangential" : self.select_tangential_modes,
                "sagittal" : self.select_sagittal_modes,
                "x" : self.select_tangential_modes, "y" : self.select_sagittal_modes
            }
            if select.casefold() not in switch:
                raise ValueError("Unrecognised select string given - see the Model.select_modes "
                                 "documentation for all of the available options.")

            switch[select.casefold()](maxtem)
        else:
            if maxtem is not None:
                LOGGER.warn("Ignoring maxtem argument given to select_modes.")

            self.__homs = np.zeros(len(select), dtype=(np.intc, 2))
            for i, mode in enumerate(select):
                self.__homs[i] = (int(mode[0]), int(mode[1]))

            self.__maxtem = np.max(self.__homs[:,0] + self.__homs[:,1])
            self.__update_homs_map()

    def __select_modes_check(self, maxtem=None):
        if maxtem is None:
            if self.maxtem is None:
                raise ValueError("No maxtem has been specified yet for this model.")
            maxtem = self.maxtem

        if not isinstance(maxtem, numbers.Integral) or maxtem < 0:
            raise ValueError("maxtem must be a positive integer")

        if self.__spatial_type == SpatialType.PLANE:
            self.__spatial_type = SpatialType.MODAL
            LOGGER.info(f"Turning on HOMs and switching model: {self} to modal.")

        return maxtem

    def select_tangential_modes(self, maxtem=None):
        r"""Select only tangential modes up to a given maximum order.

        The following modes will be selected up to :math:`N=\mathrf{maxtem}`::

            [(0, 0), (1, 0), (2, 0), ..., (N, 0)]

        If `maxtem` is specified then this will be used as the maximum order,
        otherwise the current `maxtem` value held by the model will be used.

        Parameters
        ----------
        maxtem : int
            Optional maximum mode order.
        """
        self.__maxtem = self.__select_modes_check(maxtem)

        N = 1 + self.maxtem
        self.__homs = np.zeros(N, dtype=(np.intc, 2))

        for n in range(N):
            self.__homs[n] = (n, 0)

        self.__update_homs_map()

    def select_sagittal_modes(self, maxtem=None):
        r"""Select only sagittal modes up to a given maximum order.

        The following modes will be selected up to :math:`N=\mathrf{maxtem}`::

            [(0, 1), (0, 1), (0, 2), ..., (0, N)]

        If `maxtem` is specified then this will be used as the maximum order,
        otherwise the current `maxtem` value held by the model will be used.

        Parameters
        ----------
        maxtem : int
            Optional maximum mode order.
        """
        self.__maxtem = self.__select_modes_check(maxtem)

        N = 1 + self.maxtem
        self.__homs = np.zeros(N, dtype=(np.intc, 2))

        for n in range(N):
            self.__homs[n] = (0, n)

        self.__update_homs_map()

    def select_even_modes(self, maxtem=None):
        r"""Select only even modes up to a given maximum order.

        The following modes will be selected up to :math:`N=\text{even}(\mathrf{maxtem})`::

            [(0, 0), (0, 2), (0, 4), ..., (2, 0), (2, 2), (2, 4), ...]

        If `maxtem` is specified then this will be used as the maximum order,
        otherwise the current `maxtem` value held by the model will be used.

        Parameters
        ----------
        maxtem : int
            Optional maximum mode order.
        """
        self.maxtem = self.__select_modes_check(maxtem)

        self.__homs = np.array([(n, m) for n, m in self.__homs
                                if not n % 2 and not m % 2])
        self.__maxtem = np.max(self.__homs[:,0] + self.__homs[:,1])

        self.__update_homs_map()

    def select_odd_modes(self, maxtem=None):
        r"""Select only odd modes up to a given maximum order.

        The following modes will be selected up to :math:`N=\text{odd}(\mathrf{maxtem})`::

            [(0, 0), (0, 1), (0, 3), ..., (1, 0), (1, 1), (1, 3), ...]

        If `maxtem` is specified then this will be used as the maximum order,
        otherwise the current `maxtem` value held by the model will be used.

        Parameters
        ----------
        maxtem : int
            Optional maximum mode order.
        """
        self.maxtem = self.__select_modes_check(maxtem)

        self.__homs = np.array([(n, m) for n, m in self.__homs
                                if (n % 2 or not n) and (m % 2 or not m)])
        self.__maxtem = np.max(self.__homs[:,0] + self.__homs[:,1])

        self.__update_homs_map()

    def add_all_ad(self, node, f):
        """Adds amplitude detectors at the specified `node` and frequency `f`
        for all Higher Order Modes in the model.

        Parameters
        ----------
        node : :class:`.OpticalNode`
            Node to add the detectors at.

        f : scalar, :class:`.Parameter` or :class:`.ParameterRef`
            Frequency of the field to detect.
        """
        node_name = node.tag
        if node_name is None or not valid_name(node_name):
            node_name = node.full_name.replace('.', '_')

        for n, m in self.__homs:
            self.add(AmplitudeDetector(f"ad{n}{m}_{node_name}", node, f, n, m))

    @property
    def phase_level(self):
        """An integer corresponding to the phase level given
        to the phase command of a Finesse 2 kat script.

        :getter: Returns the phase level.
        :setter: Sets the phase level - turns on/off specific flags
                 for the scaling of coupling coefficient and Gouy phases.
        """
        lvl = 0
        if self.phase_config.ZERO_K00: lvl += 1
        if self.phase_config.ZERO_TEM00_GOUY: lvl += 2
        return lvl

    @phase_level.setter
    def phase_level(self, value):
        if value == 0:
            self.phase_config.ZERO_K00 = False
            self.phase_config.ZERO_TEM00_GOUY = False
        elif value == 1:
            self.phase_config.ZERO_K00 = True
            self.phase_config.ZERO_TEM00_GOUY = False
        elif value == 2:
            self.phase_config.ZERO_K00 = False
            self.phase_config.ZERO_TEM00_GOUY = True
        elif value == 3:
            self.phase_config.ZERO_K00 = True
            self.phase_config.ZERO_TEM00_GOUY = True

    @property
    def Nhoms(self):
        """Number of higher-order modes (HOMs) included in the model.

        :getter: Returns the number of HOMs in the model (read-only).
        """
        return self.__homs.shape[0]

    @property
    def frequencies(self):
        """The frequencies stored in the model as a :class:`list`
        instance.

        :getter: Returns a list of the model frequencies (read-only).
        """
        return tuple(self.__frequencies)

    @property
    def source_frequencies(self):
        """The source frequencies stored in the model as a
        :class:`Dictionary` instance, with frequencies as keys, and a list of
        components that depend on a given frequency as values.

        :getter: Returns a dict of the model source frequencies (read-only).
        """
        return tuple(self.__source_frequencies.keys())

    @property
    def components(self):
        """The components stored in the model as a tuple object.

        :getter: Returns a tuple of the components in the model (read-only).
        """
        return tuple(self.__components.keys())

    @property
    def detectors(self):
        """The detectors stored in the model as a tuple object.

        :getter: Returns a tuple of the detectors in the model (read-only).
        """
        return tuple(self.__detectors.keys())

    @property
    def cavities(self):
        """The cavities stored in the model as a tuple object.

        :getter: Returns a tuple of the cavities in the model (read-only).
        """
        return tuple(self.__cavities.keys())

    @property
    def is_built(self):
        """
        Flag indicating whether the model has been built.

        When this evaluates to `True`, the structure of the underlying matrix
        should not be changed.

        Returns
        -------
        bool
            `True` if the model has been built, `False` otherwise.
        """
        return nx.is_frozen(self.network)

    @property
    def beam_trace_args(self):
        return self.__beam_trace_args

    @property
    def last_trace(self):
        """An instance of :class:`.TraceSolution` containing the output of the
        most recent beam trace performed on the model. Stores nodes as keys and tuples
        of (qx, qy, QSetMode) as values.

        :getter: Returns the most recent beam trace output (read-only).
        """
        return self.__last_trace

    def force_retrace(self, all_points=False):
        """Force a beam retrace of the model on the next
        call (internal or otherwise) to :meth:`Model.beam_trace`.

        Parameters
        ----------
        all_points : bool, optional
            Forces a retrace for every data point in a scan, regardless
            of whether a parameter which doesn't affect a trace is being
            changed.
        """
        self.__no_retrace = False
        self._retrace_required = True
        self.__force_retrace_all = all_points

    def retrace_off(self):
        """Switch off retracing of the beam.

        This forces the model to keep the first beam trace results only
        and stop any retracing even if a parameter is changing which
        would affect the trace.
        """
        self.__no_retrace = True

    def tag_node(self, node, tag):
        """Tag a node with a unique name.

        Access to this node can then be performed with:

            node = model.<tag_name>

        Parameters
        ----------
        node : :class:`.Node`
            An instance of a node already present in the model.

        tag : str
            Unique tag name of the node.
        """
        self.__node_exists_check(node)

        if hasattr(self, tag):
            raise Exception(f"Tagged name: {tag} already exists in the model.")

        node._set_tag(tag)

        self._unfreeze()
        setattr(self, tag, node)
        self._freeze()

        # TODO (sjr) set an attribute of the component that the node belongs to
        #            as well (i.e. setattr(node.component, tag, node))?

    def remove(self, obj):
        """Removes an object from the model.

        Parameters
        ----------
        obj : :class:`.Frequency` or sub-class of :class:`.ModelElement`
            The object to remove from the model.

        Raises
        ------
        Exception
            If the matrix has already been built or there is no component
            with the given name in the model.
        """

        raise NotImplementedError()
        """
        This isn't working properly yet, it just removes objects from a dictionary
        not all the graph, informing any connected components it has been removed, etc.
        """
        # if isinstance(obj, ModelElement):
        #     if self.is_built:
        #         raise Exception("Matrix has been built")
        #
        #     if obj.name in self.__components:
        #         del (self.__components[obj])
        #     elif obj.name in self.detectors:
        #         del (self.__detectors[obj])
        #     elif obj.name in self.__cavities:
        #         del (self.__cavities[obj])
        #     else:
        #         raise Exception("Element with name {} not in this model".format(obj.name))

    def add(self, obj):
        """Adds an element (or sequence of elements) to the model - these can be
        :class:`.ModelElement` sub-class instances.

        When the object is added, an attribute defined by `obj.name` is set within
        the model allowing access to the object just added via `model.obj_name` where
        `obj_name = obj.name`.

        Parameters
        ----------
        obj : Sub-class of :class:`.ModelElement` (or sequence of)
            The object(s) to add to the model.

        Raises
        ------
        Exception
            If the matrix has already been built, the component has already
            been added to the model or `obj` is not of a valid type.
        """
        if isinstance(obj, Iterable):
            for o in obj: self.add(o)
            return

        if self.is_built:
            raise Exception("Matrix has been built")

        try:
            if obj._model is not None:
                raise Exception(f"Element {obj.name} already thinks it is attached to a different model")
        except ComponentNotConnected:
            pass

        if obj.name in self.__elements:
            raise Exception(f"An element with the name {obj.name} is already present"
                            f" in the model ({self.__elements[obj.name]})")

        assert(isinstance(obj, ModelElement))

        obj._set_model(self)

        if isinstance(obj, Space) or isinstance(obj, Wire) or isinstance(obj, Joint):
            for node in obj.nodes.values():
                self.__add_node_to_graph(node, obj)

            for key in obj._registered_connections:
                From, To = obj._registered_connections[key]
                From_name, To_name = obj._registered_connections[key]
                From = obj.nodes[From_name]
                To   = obj.nodes[To_name]
                self.__add_connection_to_graph(key, From, To, obj)

            self._update_optical_network()

            # self.__spaces._unfreeze()
            #
            # if hasattr(self.__spaces, obj.name):
            #     raise Exception("Space name {} already added".format(obj.name))
            #
            # setattr(self.__spaces, obj.name, obj)
            # self.__spaces._freeze()

        elif isinstance(obj, Detector):
            # Tell component to associate itself with this model
            if obj.name in self.__detectors:
                raise Exception( "Element with name {} already added to this model".format( obj.name ))

            self.__detectors[obj] = len(self.__detectors)

        elif isinstance(obj, Cavity):
            if obj.name in self.__cavities:
                raise Exception("Cavity with name {} already added to this model".format(obj.name))

            self.__cavities[obj] = len(self.__cavities)

            # if model trace not yet performed, skip trying to do a trace on the
            # cavity itself as previous q values required for the trace are not
            # yet computed
            obj.update()

            self._retrace_required = True

        elif isinstance(obj, Connector):
            if obj.name in self.__components:
                raise Exception("Element with name {} already added to this model".format(obj.name))

            for _ in obj.nodes.values():
                self.__add_node_to_graph(_, obj)

            for key in obj._registered_connections:
                From_name, To_name = obj._registered_connections[key]
                From = obj.nodes[From_name]
                To   = obj.nodes[To_name]
                self.__add_connection_to_graph(key, From, To, obj)

            self._update_optical_network()

            self.__components[obj] = len(self.__components)

            if isinstance(obj, FrequencyGenerator):
                self._frequency_generators.append(obj)

        elif isinstance(obj, ModelElement):
            # Model elements come in a variety of forms, which are dealt with above
            # if the object isn't any of these the bare minimum contract is that we
            # tell it to associate itself with this model and call _on_add to let it
            # do some initialisation if necessary
            pass
        else:
            raise Exception("Could not add object {}".format(str(obj)))

        if obj._add_to_model_namespace:
            # If the elment requests asks it will be added to the model namespace
            if hasattr(self, obj.name):
                raise Exception(f"Not a valid {obj.__class__.__name__} name. An attribute"
                                "called `{obj.name}` already exists in the Model")

            parent = self

            for _ in obj._namespace.split('.'):
                if len(_)>0:
                    if hasattr(parent, _):
                        curr = getattr(parent, _)
                    else:
                        curr = Freezable()
                    parent._unfreeze()
                    setattr(parent, _, curr)
                    parent._freeze()
                    parent = curr

            parent._unfreeze()
            setattr(parent, obj.name, obj)
            parent._freeze()

        if hasattr(obj, "_on_add"): obj._on_add(self)
        if hasattr(obj, "_on_frequency_change"): self.__frequency_change_callbacks.append(obj._on_frequency_change)

        self.__elements[obj.name] = obj

    def __add_node_to_graph(self, node, owner):
        if self.is_built:
            raise Exception("Matrix has been built")

        if not self.network.has_node(node.full_name):
            ref = node.full_name
            self.network.add_node(ref, weakref=weakref.ref(node), owner=weakref.ref(owner))

            if node.type == NodeType.OPTICAL:
                self.network.node[ref]['optical'] = True
            elif node.type == NodeType.MECHANICAL:
                self.network.node[ref]['mechanical'] = True
            elif node.type == NodeType.ELECTRICAL:
                self.network.node[ref]['electrical'] = True
            else:
                raise Exception("Type unhandled")
        else:
            # Spaces have duplicate optical nodes so don't complain about them
            if not(isinstance(owner, Space) or isinstance(owner, Wire) or isinstance(owner, Joint)):
                if node.type == NodeType.OPTICAL:
                    raise Exception("Node {} already added".format(node))

    def __add_connection_to_graph(self, name, From, To, owner):
        if self.is_built:
            raise Exception("Matrix has been built")

        for _ in [From, To]:
            if not self.network.has_node(_.full_name):
                raise Exception("Node {} hasn't been added".format(_))

        self.network.add_edge(From.full_name, To.full_name, name=name,
                                    in_ref=weakref.ref(From), out_ref=weakref.ref(To),
                                    owner=weakref.ref(owner), length=1,
                                    coupling_type= owner.coupling_type(From, To))

    def _update_optical_network(self):
        self.__optical_network = nx.DiGraph(
            [e for e in self.network.edges
             if self.network[e[0]][e[1]]["coupling_type"] == CouplingType.OPTICAL_TO_OPTICAL]
        )
        # if only a single component exists in the network then no edges will have
        # been added, so add the optical nodes of that component directly here
        if not len(self.__optical_network.nodes):
            self.__optical_network.add_nodes_from([
                n for n, attr_dict in self.network.nodes(data=True) if "optical" in attr_dict
            ])

    # TODO: add overloaded + operator to perform a merge between two model instances
    # returning a third instance which is the merged object - but should not invalidate
    # the two objects being merged.
    def merge(self, other, from_comp, from_port, to_comp, to_port, name=None, L=0, nr=1):
        """Merges the model `other` with this model using a connection
        at the specified ports.

        .. note::

            Upon completion of this method call the `Model` instance `other` will
            be invalidated. All components and nodes within `other` will be associated
            with **only** this model.

        Parameters
        ----------
        other : :class:`.Model`
            A model configuration to merge into this model instance.

        from_comp : Sub-class of :class:`.Connector`
            The component to start a connection from.

        from_port : int
            Port of `from_comp` to initiate the connection from.

        to_comp : Sub-class of :class:`.Connector`
            The component to bridge the connection to.

        to_port : int
            Port of `to_comp` to bridge the connection to.

        name : str
            Name of connecting :class:`.Space` instance.

        L : float
            Length of the connecting space.

        nr : float
            Index of refraction of the connecting space.
        """
        self._unfreeze()
        self.__homs += other.homs

        # combine components/detectors
        for node in list(other.network.nodes):
            try: self.add(node.component)
            except Exception: continue
        # combine spaces
        for edge_tuple in list(other.network.edges):
            for edge in edge_tuple:
                try: self.add(edge.space)
                except Exception: continue

        # combine cavities
        for cav in other.cavities:
            self.add(cav)

        self.__network = nx.compose(self.__network, other.network)

        self.connect(from_comp, from_port, to_comp, to_port, name=name, L=L, nr=nr)
        self._freeze()

    def add_frequency(self, freq):
        """Add a :class:`.SourceFrequency` to the model description.

        Parameters
        ----------
        freq: :class:`.SourceFrequency`
            The frequency to add.
        """
        if freq in self.__frequencies:
            LOGGER.warn(f"Frequency {freq.name} already added to model")
        else:
            self.__frequencies.add(freq)

    def get_frequency_object(self, frequency_value):
        if frequency_value in self.__freq_map:
            return self.__freq_map[frequency_value][0]
        else:
            return None

    def chain(self, *args, start=None, port=None):
        """
        Utility function for connecting multiple connectable objects in
        a sequential list together. Between each item the connection
        details can be specfied, such as length or refractive index.
        This function also adds the elements to the model and returns
        those as a tuple to for the user to store if required.

        Examples
        --------
        Make a quick 1m cavity and store the added components into
        variables::

            l1, m1, m2 = ifo.chain(Laser('l1'), Mirror('m1'), 1, Mirror('m2'))

        Or be more specific about connection parameters by providing a
        dictionary. This dictionary is passed to the
        :meth:`Model.connect` method as kwargs so see there for which
        options you can specify. For optical connections we can set
        lengths and refractive index of the space::

            ifo.chain(
                Laser('l1'),
                Mirror('AR'),
                {'L':1e-2, 'nr':1.45},
                Mirror('HR')
            )

        A pre-constructed :class:`.Space`, :class:`.Wire` or
        :class:`.Joint` can also be used::

            ifo.chain(
                Laser('l1'),
                Mirror('AR'),
                Space('scav', L=1e-2, nr=1.45),
                Mirror('HR')
            )

        The starting point of the chain can be specfied for more
        complicated setups like a Michelson::

            ifo = Model()
            ifo.chain(Laser('lsr'), Beamsplitter('bs'))

            # connecting YARM to BS
            ifo.chain(
                1,
                Mirror('itmy'),
                1,
                Mirror('etmy'),
                start=ifo.bs,
                port=2,
            )

            # connecting XARM to BS
            ifo.chain(
                1,
                Mirror('itmx'),
                1,
                Mirror('etmx'),
                start=ifo.bs,
                port=3,
            )

        Parameters
        ----------
        start: component, optional
            This is the component to start the chain from. If None, then
            a completely new chain of components is generated.
        port: int, optional (required if `start` defined)
            The port number at the `start` component provided to start
            the chain from. This must be a free unconnected port at the
            `start` component or an exception will be thrown.

        Returns
        -------
        tuple
            A tuple containing the objects added. The `start` component
            is never returned.
        """
        import numbers

        connectors = []
        connections = []
        was_prev_connector = False

        if start is not None:
            if start not in self.components:
                raise Exception("Component %s is not in this model" % start)

            if port is None:
                raise Exception("Port keyword argument must also be"
                                "provided if specifying a start for"
                                "the chain.")

            connectors.append(start)
            was_prev_connector = True

        for i, item in enumerate(args):
            if (isinstance(item, Space)
                    or isinstance(item, Wire)
                    or isinstance(item, Joint)):
                connections.append({'connector': item})
                was_prev_connector = False
            elif isinstance(item, Connector):
                if was_prev_connector:
                    if i == 0 and start is not None:
                        connections.append({'port': port})
                    else:
                        connections.append({})

                connectors.append(item)
                was_prev_connector = True
                self.add(item)
            elif isinstance(item, numbers.Number):
                connections.append({'L':item})

                if i == 0 and start is not None:
                    connections[-1]['port'] = port

                was_prev_connector = False
            else:
                connections.append(item)
                was_prev_connector = False

        pairs = list(pairwise(connectors))

        # There should always be an equal number of connections
        # for each pair of components, otherwise we are missing something...
        assert(len(pairs) == len(connections))

        for (a, b), conn in zip(pairs, connections):
            if 'port' in conn:
                port = conn['port']
                del conn['port']
            else:
                if len(a.optical_nodes) == 2:
                    port = a.p1
                elif len(a.optical_nodes) == 4:
                    port = a.p2
                elif len(a.optical_nodes) > 4:
                    break
                else:
                    raise Exception("Unhandled: " + str(a, len(a.nodes)))

            self.connect(port, b.p1, **conn)

        # Don't return the starting point the user specified
        if start is None:
            return connectors
        else:
            return connectors[1:]

    def connect(self, A, B, name=None, L=0, nr=1, connector=None):
        """
        Connects two ports in a model together. The ports should be of the same
        type, e.g. both optical ports.

        This method will also accept components from the user, in such cases it
        will loop through the ports and use the first one in `.ports` that is
        currently unconnected.

        Parameters
        ----------
        A : :class:`.Connector` or :class:`.Port`
            Index of `compA` port to connect from.

        B : :class:`.Connector` or :class:`.Port`
            Second component to connect (target).

        name : str, optional
            Name of newly created :class:`.Space` or :class:`.Wire`
            instance.

        L : float, optional
            Length of newly created :class:`.Space` or :class:`.Wire`
            instance.

        nr : float, optional
            Index of refraction of newly created :class:`.Space` or
            :class:`.Wire` instance.

        connector : :class:`.Space` or :class:`.Wire`, optional
            Existing component to use for connection. If this is None, a
            connecting :class:`.Space` or :class:`.Wire` will be created.

        Raises
        ------
        Exception
            If matrix has already been built, either of `compA` or
            `compB` are not present in the model, either of `portA` or
            `portB` are already connected or either of `portA` or
            `portB` are not valid options at the specified component(s).
        """
        if self.is_built:
            raise Exception("Matrix has already been built, you must finish"
                            "the simulation before altering its layout.")
        ports = [None, None]

        for item in [A, B]:
            # Check if these have been added to a model, if not add to this
            #if item._model is None:
            try: _ = item._model
            except ComponentNotConnected:
                if hasattr(item, 'component'):
                    # In case some port or node has been provided instead
                    # get the actual component to add
                    self.add(item.component)
                else:
                    self.add(item)

        # Aim here is to try and be smart and accept some more user friendly
        # inputs rather than always specifying the port name.
        for i, obj in enumerate([A, B]):
            if isinstance(obj, Port): # User gave us a port so just use that
                ports[i] = obj
            elif isinstance(obj, ModelElement):
                Np = len(A.ports)
                if Np == 1:
                    ports[i] = obj.ports[0]
                elif Np > 1:
                    for p in obj.ports:
                        if not p.is_connected:
                            ports[i] = p
                            break

                    if ports[i] is None:
                        raise Exception("No unconnected ports left at {}".format(obj.name))
                else:
                    raise Exception("Model element {} has no ports".format(obj.name))
            else:
                raise Exception("Don't know how to handle object of type {}".format(obj.__class__))

        assert(ports[0]._model == ports[1]._model)
        assert(ports[0].type == ports[1].type)

        if connector is None:
            if (ports[0].type == NodeType.OPTICAL):
                connector = Space(name, *ports, L=L, nr=nr)
            elif (ports[0].type == NodeType.ELECTRICAL):
                connector = Wire(name, *ports)
            elif (ports[0].type == NodeType.MECHANICAL):
                connector = Joint(name, *ports)
        else:
            connector.connect(*ports)

        self.add(connector)

    def plot_graph(self):
        """Plots the configuration with nodes positioned using the
        Fruchterman-Reingold force-directed algorithm."""
        import matplotlib.pyplot as plt

        pos = nx.drawing.layout.planar_layout(self.network)
        nx.draw(self.network, pos)
        labels=nx.draw_networkx_labels(self.network, pos)
        plt.show()

    def _register_simulation(self, sim):
        self.__simulations.append(weakref.ref(sim))

    def __toggle_param_locks(self, state):
        """
        Togglin
        """
        LOGGER.info(f"Toggle parameter lock to {state}")
        for k in self.__elements:
            el = self.__elements[k]
            for p in el._params:
                # if this is marked as tunable then don't lock it
                if p.is_tunable and state == True:
                    p._locked = False
                    LOGGER.info(f"{repr(p)} is not being locked")
                else:
                    p._locked = state

    def get_changing_edges_elements(self):
        """
        Returns
        -------
        tuple(set of (node1-name, node2-name) edges, dict(weakref(element):list))
            Returns a list of the network edges that will be changing, further information
            on the edge can be retreived directly from the model network. Also returned is
            a dictionary of elements and which parameter is changing
        """
        from collections import defaultdict
        changing_elements = defaultdict(set)
        changing_edges = set()

        for _ in self.network.edges():
            owner = self.network.edges[_]['owner']
            if owner() not in changing_elements:
                for p in owner().parameters:
                    if p.is_changing:
                        changing_elements[owner].add(p)
                        changing_edges.add(_)
        return changing_edges, changing_elements

    def unbuild(self):
        """
        If a model has been built then this function undoes the process so
        the model can be changed and rebuilt if required.
        """
        if not self.is_built:
            raise Exception("Model has not been built")

        # determine if all simulations have finished before we unbuild
        # the model
        for sim in self.__simulations:
            if sim() is not None and sim().active():
                raise Exception(f"Simulation object {hex(id(sim))} is still active")

        self.__simulations.clear()
        del self.__carrier
        del self.__signal
        # To unfreeze a graph you have to rebuild it
        self.__network = nx.DiGraph(self.__network)
        #self.__toggle_param_locks(False)

    def build(self, frequencies=None, carrier_sim_type=KLUSimulation, signal_sim_type=KLUSimulation):
        """
        This is the first step required to run a
        simulation of the model. At this stage the layout and connections
        of the model must be finalised so that the underlying sparse matrix
        can be allocated and laid out. If this changes the model must be
        rebuilt.

        What this method returns are Simulation objects. These are the
        matrix representation of the model which are used to generate
        numerical outputs. The Simulation and Model objects are linked
        together, changing the Model parameters will result in the
        Simulation objects using the new values when they are solved.

        A custom list of carrier frequencies can be specfied to run the simulations
        with. Audio frequencies will be generated from this list separately by the code. `None` will
        result in the default algorithm computing the required frequency bins,
        :func:`.frequency.generate_frequency_list`, which can be
        called by the user and added to if additional frequency bins are required.

        Parameters
        ----------
        frequencies : list of :class:`finesse.element.Symbol`
            Custom list of carrier frequencies can be specfied to run the simulations with.

        carrier_sim_type, signal_sim_type : object of instance :class:`finesse.simulation.BaseSimulation`
            The type of simulations that should be built using this model.

        Returns
        -------
        tuple(Carrier, Signal) : :class:`.carrier_sim_type`, :class:`.signal_sim_type`
            Two Simulation objects can be returned. The signal simulation
            is only returned if the Model is modelling signal or noise
            features.

        Raises
        ------
        Exception
            If the model is in a built state already. It must be destroyed
            first before it can be built again.
        """
        if self.is_built:
            raise Exception("Model has already been built")

        nx.freeze(self.network)
        self.__toggle_param_locks(True)

        self._unfreeze()
        self.__carrier = carrier_sim_type(self, "carrier", frequencies=frequencies)
        self.__signal = None
        self._freeze()

        if self.fsig.f.value is not None:
            self.__signal  = signal_sim_type(self, "signal",
                                                    frequencies=frequencies,
                                                    is_audio=True)
            self.__signal.DC = self.__carrier
            return (self.__carrier, self.__signal)
        else:
            return (self.__carrier,)

    @property
    def carrier_simulation(self):
        """
        When the model has been built this property returns the carrier simulation

        Returns
        -------
        sim : :class:`.Simulation`

        Raises
        ------
        If the carrier simulation isn't available an AttributeError is raised
        """
        if self.is_built:
            try:
                return self.__carrier
            except AttributeError:
                raise AttributeError("Model does not contain any carrier simulation")
        else:
            raise Exception("Model has not been built")

    @property
    def signal_simulation(self):
        """
        When the model has been built this property returns the signal simulation

        Returns
        -------
        sim : :class:`.Simulation`

        Raises
        ------
        If the signal simulation isn't available an AttributeError is raised
        """
        if self.is_built:
            try:
                return self.__signal
            except AttributeError:
                raise AttributeError("Model does not contain any signal simulation")
        else:
            raise Exception("Model has not been built")

    def __node_exists_check(self, node):
        if not self.network.has_node(node.full_name):
            raise NodeException("Node {} is not in the model".format(node), node)

    def path(self, from_node, to_node, via_node=None):
        """Retrieves an ordered container of the path trace between the
        specified nodes.

        The return type is an :class:`.OpticalPath` instance which stores an underlying
        list of the path data (see the documentation for :class:`.OpticalPath` for details).

        Parameters
        ----------
        from_node : :class:`.Node`
            Node to trace from.

        to_node : :class:`.Node`
            Node to trace to.

        via_node : :class:`.Node` (or sequence of)
            Node(s) to traverse via in the path.

        Returns
        -------
        out : :class:`.OpticalPath`
            A container of the nodes and components between `from_node` and `to_node`
            in order.

        Raises
        ------
        e1 : :class:`.NodeException`
            If either of `from_node`, `to_node` are not contained within the model.

        e2 : :obj:`networkx.NetworkXNoPath`
            If no path can be found between `from_node` and `to_node`.
        """
        try:
            self.__node_exists_check(from_node)
            self.__node_exists_check(to_node)
        except NodeException: raise

        from_node = from_node.full_name
        to_node = to_node.full_name

        if via_node is None:
            try: path = nx.shortest_path(self.optical_network, from_node, to_node)
            except nx.exception.NetworkXNoPath: raise

            path_between = [
                (self.network.node[node]["weakref"](),
                 self.network.node[node]["weakref"]().component) for node in path
            ]

            fullpath = []
            for node, comp in path_between:
                if node.is_input:
                    fullpath.append((node, comp))
                else:
                    fullpath.append((node, node.space))

            return OpticalPath(fullpath)

        if not hasattr(via_node, '__getitem__'): via_node = [via_node]

        via_node = [via.full_name for via in via_node]

        try:
            paths = [
                nx.shortest_path(
                    self.optical_network, from_node, via_node[0]
                )
            ]
            # find intermediate paths
            for i, node in enumerate(via_node[:-1]):
                paths.append(
                    nx.shortest_path(
                        self.optical_network, node, via_node[i+1]
                    )
                )
            paths.append(
                nx.shortest_path(
                    self.optical_network, via_node[-1], to_node
                )
            )
        except nx.exception.NetworkXNoPath: raise

        path_between = [
            (self.network.node[node]["weakref"](),
             self.network.node[node]["weakref"]().component) for node in paths[0]
        ]

        for path in paths[1:]:
            path_between += [
                (self.network.node[node]["weakref"](),
                 self.network.node[node]["weakref"]().component) for node in path
            ][1:] # don't want to repeat via node

        fullpath = []
        for node, comp in path_between:
            if node.is_input:
                fullpath.append((node, comp))
            else:
                fullpath.append((node, node.space))

        return OpticalPath(fullpath)

    def sub_model(self, from_node, to_node):
        """Obtains a subgraph of the complete configuration
        graph between the two specified nodes.

        Parameters
        ----------
        from_node : :class:`.Node`
            Node to trace from.

        to_node : :class:`.Node`
            Node to trace to.

        Returns
        -------
        G : ``networkx.graphviews.SubDiGraph``
            A SubGraph view of the subgraph between `from_node` and `to_node`.
        """
        try:
            self.__node_exists_check(from_node)
            self.__node_exists_check(to_node)
        except NodeException: raise

        nodes_between_set = {
            node
            for path in nx.all_simple_paths(
                self.__network, source=from_node, target=to_node
            )
            for node in path
        }
        return self.__network.subgraph(nodes_between_set)

    def create_mismatch(self, node, w0_mm=0, z_mm=0):
        """Sets the beam parameters such that a mismatch of the
        specified magnitude (in terms of :math:`w_0` and :math:`z`)
        exists at the given `node`.

        Parameters
        ----------
        node : :class:`.OpticalNode`
            The node to to create the mismatch at.

        w0_mm : float or sequence, optional
            The percentage magnitude of the mismatch in the waist size. This
            can also be a two-element sequence specifying the waist size mismatches
            for an astigmatic beam. Defaults to zero percent for both planes.

        z_mm : float or sequence, optional
            The percentage magntiude of the mismatch in the distance to
            the waist. This can also be a two-element sequence specifying the distance
            to waist mismatches for an astigmatic beam. Defaults to zero percent for
            both planes.
        """
        if not self.__network.has_node(node.full_name):
            raise NodeException("Specified node does not exist within the model", node)

        trace = self.beam_trace(store=False)
        qx, qy, _ = trace[node]
        qx_w0, qy_w0 = qx.w0, qy.w0
        qx_z, qy_z = qx.z, qy.z

        if not hasattr(w0_mm, "__getitem__"):
            qx_w0 *= 1 + (w0_mm / 100)
            qy_w0 *= 1 + (w0_mm / 100)
        else:
            qx_w0 *= 1 + (w0_mm[0] / 100)
            qy_w0 *= 1 + (w0_mm[1] / 100)

        if not hasattr(z_mm, "__getitem__"):
            qx_z *= 1 + (z_mm / 100)
            qy_z *= 1 + (z_mm / 100)
        else:
            qx_z *= 1 + (z_mm[0] / 100)
            qy_z *= 1 + (z_mm[1] / 100)

        qx_bp = BeamParam(w0=qx_w0, z=qx_z)
        qy_bp = BeamParam(w0=qy_w0, z=qy_z)

        self.set_q_at(node, (qx_bp, qy_bp))

    def set_q_at(self, node, q, direction=None):
        """Updates the beam parameter at a specified `node` in the
        configuration.

        This method is similar in functionality to the Finesse 2.x
        `gauss` command. Any node beam parameters set either via
        this method directly, or using the setter of :attr:`OpticalNode.q`,
        will notify the beam tracing routine accordingly.

        Parameters
        ----------
        node : :class:`.Node`

            Node to update the beam parameter for.

        q : :class:`.BeamParam` or list/tuple of two beam parameters

            Value of the beam parameter to set.

        direction : str, optional

            Plane to set the parameter for.
        """
        if not self.__network.has_node(node.full_name):
            raise NodeException("Specified node does not exist within the model", node)

        if hasattr(q, "__getitem__") and not isinstance(q, complex):
            self.__gauss_commands[node] = TraceDataEntry(q[0], q[1], QSetMode.USER)
        else:
            if direction is None:
                raise ValueError("direction arg must not be None when setting the beam "
                                 "parameter for a node in a given plane.")
            if direction == "both":
                self.__gauss_commands[node] = TraceDataEntry(q, q, QSetMode.USER)
            elif direction == 'x':
                qy = None
                if node in self.__gauss_commands:
                    qy = self.__gauss_commands[node].qy
                self.__gauss_commands[node] = TraceDataEntry(q, qy, QSetMode.USER)
            elif direction == 'y':
                qx = None
                if node in self.__gauss_commands:
                    qx = self.__gauss_commands[node].qx
                self.__gauss_commands[node] = TraceDataEntry(qx, q, QSetMode.USER)

        self._retrace_required = True

    def ABCD(self, from_node, to_node, via_node=None, direction=None):
        """Computes the ABCD matrix between `from_node` and `to_node`,
        traversing through `via_node` optionally.

        Parameters
        ----------
        from_node : :class:`.Node`
            Node to trace from.

        to_node : :class:`.Node`
            Node to trace to.

        via_node : :class:`.Node`, optional
            Optional node to trace via.

        directon : str, optional
            Direction of ABCD matrix computation (can be 'x', 'y' or `None`), defaults
            to `None` so that the matrices for both planes are returned.

        Returns
        -------
        matrix : :class:`numpy.ndarray` (or two element tuple of)
            ABCD matrix between the specified nodes (or a tuple of
            matrices for both planes if `direction` is `None`).
        """
        ABCD_x, ABCD_y = self._tracer.run(
            TraceType.ABCD,
            from_node=from_node, to_node=to_node, via_node=via_node
        )
        if direction == 'x':
            return ABCD_x
        if direction == 'y':
            return ABCD_y
        return ABCD_x, ABCD_y

    def beam_trace(self,
        startnode=None,
        order=None,
        set_symmetric=True,
        symbolic=False,
        store=True
    ):
        """Performs a beam trace setting/updating entries for all optical
        nodes in the :attr:`Model.last_trace` dictionary.

        Sets the entries for all node-parameter pairs in :attr:`Model.gauss_commands`
        first and computes eigenmodes for all the cavities within the model. Then the
        beam is propagated from the starting point through the rest of the optical
        configuration - see :ref:`tracing_manual` for details on the beam tracing
        algorithm.

        Parameters
        ----------
        startnode : :class:`.OpticalNode`, optional
            The node from which to start the trace, defaults to `None`.

        order : array like, optional
            A list of cavity instances or names on which to perform traces
            first (items appearing earlier in this sequence take
            precendence over later instances). Defaults to `None`.

        set_symmetric : bool, optional
            Flag to determine whether opposite node direction
            beam parameters get set to `-q.conjugate()` when
            tracing (i.e. flipping the sign of the distance
            to the waist).

        symbolic : bool, optional
            Flag determining whether to perform the beam trace symbolically,
            returning a :class:`.TraceSolution` object with analytic solutions
            for the trace at each node. Note that if `symbolic` is true, then
            then the :attr:`Model.last_trace` attribute is NOT updated.

        store : bool, optional
            Flag to determine whether to store the results of a beam trace in
            in the :attr:`Model.last_trace` property.

        Raises
        ------
        e1 : :class:`.NodeException`
            If there are no nodes in the configuration with
            a beam parameter defined.

        e2 : RuntimeError
            If a cavity within the model is unstable or if not
            all nodes have been traced upon completion.

        Returns
        -------
        out : :class:`.TraceSolution`
            An object representing the results of the tracing routine.
        """
        if symbolic:
            raise NotImplementedError()

        state = (startnode, order, set_symmetric, symbolic, store)
        if state != self.__prev_beam_trace_args:
            self._retrace_required = True

        if not self._retrace_required or (self.__no_retrace and self.__last_trace is not None):
            self._trace_changed = False
            LOGGER.info("No beam trace dependent parameters have changed, skipping retrace")
            return self.__last_trace

        if not self.__cavities and not self.__gauss_commands:
            raise Exception("No cavity given nor manual beam parameter set, unable "
                            "to perform a beam trace")

        any_stable = any((cav.is_stable and not cav.is_critical for cav in self.__cavities.keys()))
        if not any_stable and not self.__gauss_commands:
            raise Exception("No stable cavity given nor manual beam parameter set, unable "
                            "to perform a beam trace.")

        LOGGER.info(f"{'Symbolic b' if symbolic else 'B'}eam trace triggered on {self}")

        trace_results = OrderedDict()

        self._tracer.symbolic = symbolic

        for node, tde in self.__gauss_commands.items():
            trace_results[node] = tde
            if set_symmetric:
                trace_results[node.opposite] = TraceDataEntry(
                    -tde.qx.conjugate(), -tde.qy.conjugate(), tde.set_mode
                )

        for cav in self.__cavities.keys(): # compute eigenmodes first
            cav.update()

            # stability checks
            if not cav.is_stable or cav.is_critical:
                self._tracer.symbolic = False
                LOGGER.warn(f"Cavity {cav.name} is {'critical' if cav.is_critical else 'unstable'} "
                            f"(g= {cav.stability}), skipping its trace.")
                continue

            # don't set the node to cavities' eigenmode if this node
            # is in gauss_commands => use user set param preferentially
            if cav.source in self.__gauss_commands.keys(): continue
            trace_results[cav.source] = TraceDataEntry(
                cav.eigenmode_x, cav.eigenmode_y, QSetMode.AUTO
            )
            if set_symmetric:
                trace_results[cav.source.opposite] = TraceDataEntry(
                    -cav.eigenmode_x.conjugate(),
                    -cav.eigenmode_y.conjugate(),
                    QSetMode.AUTO
                )

        # start trace from explicitly user-defined node...
        if startnode is not None:
            if self.beam_trace_logging.trace_info:
                LOGGER.info(f"Starting beam trace from user-defined node: {startnode}")

            self._tracer.run(
                TraceType.SIMULATION,
                current=startnode,
                trace_results=trace_results,
                from_user=True,
                set_symmetric=set_symmetric
            )

        other_cavities = self.__cavities.keys()
        traced_cavities = set()
        # ... then start tracing from the cavities given
        # in the order parameter...
        if order is not None:
            if not hasattr(order, '__getitem__'): order = [order]

            for cav in order:
                # user can provide cavity names rather than instances
                # so convert them to a Cavity object here
                if not isinstance(cav, Cavity):
                    cav = getattr(self, cav)

                if not cav.is_stable or cav.is_critical:
                    LOGGER.warn(f"Cavity {cav.name} in order sequence is unstable, skipping "
                                 "the beam trace branch started from this cavity.")
                    continue

                if self.beam_trace_logging.trace_info:
                    LOGGER.info(f"Tracing the beam from cavity: {cav.name}")

                self._tracer.run(
                    TraceType.SIMULATION,
                    current=cav.source,
                    trace_results=trace_results,
                    from_cav=cav,
                    traced_cavs=traced_cavities,
                    set_symmetric=set_symmetric
                )

            other_cavities = list(set(self.__cavities.keys()).difference(order))
        # ... and then trace from the remaining cavities...
        for cav in other_cavities:
            if not cav.is_stable or cav.is_critical:
                continue

            if self.beam_trace_logging.trace_info:
                LOGGER.info(f"Tracing the beam from cavity: {cav.name}")

            self._tracer.run(
                TraceType.SIMULATION,
                current=cav.source,
                trace_results=trace_results,
                from_cav=cav,
                traced_cavs=traced_cavities,
                set_symmetric=set_symmetric
            )

        # ... finally trace from user defined node q values
        for node in self.__gauss_commands.keys():
            if self.beam_trace_logging.trace_info:
                LOGGER.info(f"Tracing the beam from user-set beam parameter node: {node}")

            self._tracer.run(
                TraceType.SIMULATION,
                current=node,
                trace_results=trace_results,
                from_user=True,
                set_symmetric=set_symmetric
            )

        self.__check_qs_set(trace_results, set_symmetric)

        trace = TraceSolution(trace_results, symbolic, self, TraceType.SIMULATION)

        if self.beam_trace_logging.results:
            LOGGER.info(f"\n{trace}")

        self._tracer.symbolic = False
        if symbolic:
            # all cavities will have symbolic properties at this point if symbolic is True
            # so recalculate the properties with the numeric values
            for cav in self.__cavities.keys():
                cav.update()
        else:
            if store:
                self.__last_trace = trace
                self._trace_changed = True
                if not self.__force_retrace_all:
                    self._retrace_required = False

        self.__prev_beam_trace_args = state

        return trace

    def __check_qs_set(self, trace_results, set_symmetric, predecessors_traced=False):
        if len(trace_results) == len(self.optical_nodes):
            return

        diff = list(set(self.optical_nodes).difference(trace_results.keys()))
        # if we're not setting opposite node qs to -q* ...
        if not set_symmetric:
            for node in diff:
                # ... but we've encountered a node which has no successors,
                # then set node.opposite.q to -q* as it's the only option
                if not list(self.optical_network.successors(node.opposite.full_name)):
                    tde = trace_results[node.opposite]
                    trace_results[node] = TraceDataEntry(
                        -tde.qx.conjugate(), -tde.qy.conjugate(), QSetMode.AUTO
                    )
                    diff.remove(node)
        for node, tde in trace_results.items():
            if tde.qx is None or tde.qy is None:
                diff.append(node)
        if diff:
            if not predecessors_traced:
                for node in self.__gauss_commands.keys():
                    self._tracer.run(
                        TraceType.SIMULATION,
                        current=node.opposite,
                        trace_results=trace_results,
                        from_user=True,
                        set_symmetric=set_symmetric
                    )

                self.__check_qs_set(trace_results, set_symmetric, predecessors_traced=True)
            else:
                self._tracer.symbolic = False
                raise RuntimeError("Fatal error in beam tracing, not all "
                                   "node q values have been set! The following "
                                   "nodes have not been set: {}".format(diff))

    def beam_trace_path(self, q_in, from_node, to_node, via_node=None, direction=None):
        """Traces a beam of arbitrary geometry (specified with `q_in`) through
        the path defined by `from_node, `to_node` and `via_node`.

        This method returns an object of type :class:`.BeamTrace` which
        contains a dictionary (`data`) consisting of the beam parameter,
        lengths and Gouy phases at each node and component along the path traced.

        Parameters
        ----------
        q_in : :class:`.BeamParam` or complex
            Input beam parameter at `from_node`.

        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        via_node : :class:`.OpticalNode` (or sequence of)
            Node(s) to traverse via in the path.

        direction : str, optional
            Direction of ABCD matrix computation (can be 'x', 'y' or `None`), defaults
            to `None` which performs a beam trace path for both directions 'x' and 'y'.

        Returns
        -------
        out : :class:`.TraceSolution` (or two-element tuple of)
            An object containing the data of the beam traced along
            the given path. If `direction = None` then a two-element
            tuple of :class:`.TraceSolution` instances is returned, the
            first element containing the 'x' direction trace and the
            second element containing the 'y' direction trace.

        Raises
        ------
        e1 : :class:`.NodeException`
            If either of `from_node`, `to_node` are not contained within the model.

        e2 : :class:`networkx.NetworkXNoPath`
            If no path can be found between `from_node` and `to_node`.
        """
        if q_in is None:
            raise ValueError("q_in arg must not be None.")
        if direction is None:
            datax = OrderedDict()
            datay = OrderedDict()
        elif direction == 'x':
            datax = OrderedDict()
            datay = None
        elif direction == 'y':
            datax = None
            datay = OrderedDict()
        else: raise ValueError("Argument direction must be None, 'x' or 'y'.")

        return self._tracer.run(
            TraceType.UTILITY,
            from_node=from_node, to_node=to_node, via_node=via_node,
            q_in=q_in,
            datax=datax, datay=datay)
