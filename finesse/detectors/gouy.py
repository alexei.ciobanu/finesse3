"""Accumulated Gouy phase detector."""

import numpy as np

from finesse.components import Space
from finesse.detectors.general import Detector

class Gouy(Detector):
    """Detector to measure the accumulated gouy phase across
    a sequence of specified spaces."""
    def __init__(self, name, *args, direction='x'):
        Detector.__init__(self, name, dtype=np.float64,
                          unit="degrees", label="Gouy phase")

        self.__direction = direction

        if any((not isinstance(arg, Space) for arg in args)):
            raise TypeError("All args must be of type Space.")
        self.__spaces = args

    def get_output(self, DC, AC):
        if self.__direction == 'x':
            return np.sum(np.array([space.gouy_x for space in self.__spaces]))

        return np.sum(np.array([space.gouy_y for space in self.__spaces]))
