import enum

import numpy as np

from finesse.detectors.general import Detector

class BeamProperty(enum.Enum):
    SIZE = 0
    WAISTSIZE = 1
    DISTANCE = 2
    RAYLEIGH = 3
    GOUY = 4
    DIVERGENCE = 5
    ROC = 6
    Q = 7

class BeamPropertyDetector(Detector):
    def __init__(self, name, prop, node, direction='x'):
        if isinstance(prop, str):
            CONVERT = {
                "w" : BeamProperty.SIZE,
                "w0" : BeamProperty.WAISTSIZE,
                "z" : BeamProperty.DISTANCE,
                "zr" : BeamProperty.RAYLEIGH,
                "gouy" : BeamProperty.GOUY,
                "div" : BeamProperty.DIVERGENCE,
                "Rc" : BeamProperty.ROC,
                "q" : BeamProperty.Q
            }
            if prop not in CONVERT:
                raise ValueError(f"Unrecognised property: {prop}, expected "
                                 f"one of: {CONVERT.keys()}")
            prop = CONVERT[prop]

        if prop == BeamProperty.Q:
            dtype = np.complex128
            units = ''
        else:
            dtype = np.float64
            if prop == BeamProperty.GOUY or prop == BeamProperty.DIVERGENCE:
                units = "radians"
            else:
                units = 'm'

        property_to_label = {
            BeamProperty.SIZE : "Beam size",
            BeamProperty.WAISTSIZE : "Beam waist-size",
            BeamProperty.DISTANCE : "Distance to beam waist",
            BeamProperty.RAYLEIGH : "Rayleigh range",
            BeamProperty.GOUY : "Gouy phase",
            BeamProperty.DIVERGENCE : "Divergence angle",
            BeamProperty.ROC : "Beam radius of curvature",
            BeamProperty.Q : "Beam parameter"
        }
        if prop not in property_to_label:
            raise ValueError(f"Unrecognised property: {prop}, expected "
                             f"one of: {property_to_label.keys()}")

        Detector.__init__(self, name, node, dtype=dtype, unit=units, label=property_to_label[prop])
        self.__prop = prop
        self.direction = direction

    @property
    def detecting(self):
        """The property of the beam which is being detected.

        :getter: Returns the detected property (read-only).
        """
        return self.__prop

    def get_output(self, DC, AC):
        node_q_map = DC.model.last_trace
        qx, qy, _ = node_q_map[self._node]

        if self.__prop == BeamProperty.SIZE:
            return qx.w if self.direction == 'x' else qy.w
        elif self.__prop == BeamProperty.WAISTSIZE:
            return qx.w0 if self.direction == 'x' else qy.w0
        elif self.__prop == BeamProperty.DISTANCE:
            return qx.z if self.direction == 'x' else qy.z
        elif self.__prop == BeamProperty.RAYLEIGH:
            return qx.zr if self.direction == 'x' else qy.zr
        elif self.__prop == BeamProperty.GOUY:
            return qx.gouy() if self.direction == 'x' else qy.gouy()
        elif self.__prop == BeamProperty.DIVERGENCE:
            return qx.divergence if self.direction == 'x' else qy.divergence
        elif self.__prop == BeamProperty.ROC:
            return qx.Rc if self.direction == 'x' else qy.Rc
        elif self.__prop == BeamProperty.Q:
            return qx if self.direction == 'x' else qy
