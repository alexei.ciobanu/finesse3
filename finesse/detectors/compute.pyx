#cython: boundscheck=False, wraparound=False, initializedcheck=False

from finesse.cmath.cmath cimport conj
from finesse.cmath.cmath cimport sqrt
from finesse.cmath cimport complex_t
from finesse.cmath cimport Math
from finesse.cmath cimport ComplexMath
from finesse.cmath cimport Gaussian

cdef extern from "../constants.h":
    double complex COMPLEX_0

cdef complex_t monochromatic_beam_pixel(
    object DC, object node,
    double [::1] freqs, int[:, ::1] homs,
    complex_t qx, complex_t qy,
    double x, double y,
    double f,
    double nr, double lambda0,
    bint zero_tem00_gouy
):
    cdef:
        int n, m
        Py_ssize_t freq_idx, field_idx
        double freq
        complex_t at
        complex_t z_ij = COMPLEX_0
        double phase
        double inv_root2 = 1.0 / sqrt(2.0)
        Py_ssize_t Nfreqs = freqs.shape[0]
        Py_ssize_t Nfields = homs.shape[0]

    for freq_idx in range(Nfreqs):
        freq = freqs[freq_idx]
        if Math.float_eq(freq, f):
            at = COMPLEX_0
            for field_idx in range(Nfields):
                n = homs[field_idx][0]
                m = homs[field_idx][1]

                if zero_tem00_gouy:
                    phase = n * Gaussian.gouy(qx) + m * Gaussian.gouy(qy)
                else:
                    phase = (n + 0.5) * Gaussian.gouy(qx) + (m + 0.5) * Gaussian.gouy(qy)

                at += (
                    ComplexMath.u_nm(n, m, qx, qy, x, y, nr, lambda0)
                    * ComplexMath.rotate(
                        DC.get_DC_out(node, freq_idx, field_idx), -phase
                    )
                    * inv_root2
                )

            z_ij += at

    return z_ij

cdef complex_t spectral_beam_pixel(
    object DC, object node,
    double [::1] freqs, int[:, ::1] homs,
    complex_t qx, complex_t qy,
    double x, double y,
    double nr, double lambda0,
    bint zero_tem00_gouy
):
    cdef:
        int n, m
        Py_ssize_t freq_idx, field_idx
        complex_t at = COMPLEX_0
        complex_t z_ij = COMPLEX_0
        double phase
        double inv_root2 = 1.0 / sqrt(2.0)
        Py_ssize_t Nfreqs = freqs.shape[0]
        Py_ssize_t Nfields = homs.shape[0]

    for freq_idx in range(Nfreqs):
        for field_idx in range(Nfields):
            n = homs[field_idx][0]
            m = homs[field_idx][1]

            if zero_tem00_gouy:
                phase = n * Gaussian.gouy(qx) + m * Gaussian.gouy(qy)
            else:
                phase = (n + 0.5) * Gaussian.gouy(qx) + (m + 0.5) * Gaussian.gouy(qy)

            at += (
                ComplexMath.u_nm(n, m, qx, qy, x, y, nr, lambda0)
                * ComplexMath.rotate(
                    DC.get_DC_out(node, freq_idx, field_idx), -phase
                )
                * inv_root2
            )

        z_ij += at * conj(at)

    return z_ij

cpdef complex_t pixel_output(
    DC, node, double[::1] freqs, int[:, ::1] homs, complex_t qx, complex_t qy,
    double x, double y,
    double nr, double lambda0,
    bint zero_tem00_gouy, f=None
):
    cdef double given_freq

    if f is not None:
        given_freq = <double>f
        return monochromatic_beam_pixel(
            DC, node, freqs, homs, qx, qy, x, y, given_freq,
            nr, lambda0, zero_tem00_gouy
        )

    else:
        return spectral_beam_pixel(
            DC, node, freqs, homs, qx, qy, x, y,
            nr, lambda0, zero_tem00_gouy
        )


def ccd_output(
    DC, node, double[::1] freqs, int[:, ::1] homs, complex_t qx, complex_t qy, double nr,
    double lambda0, double[::1] x, double[::1] y, bint zero_tem00_gouy, complex_t[:, ::1] out,
    f=None
):
    """
    ccd_output(DC, node, freqs, homs, qx, qy, nr, lambda0, x, y, zero_tem00_gouy, out, f)

    Computes the output image on a CCD at a specified `node` with the given `x` and `y`
    co-ordinate arrays.
    """
    cdef:
        Py_ssize_t i, j
        double given_freq
        double xval, yval
        Py_ssize_t Npts = x.shape[0]

    if f is not None:
        given_freq = <double>f
        for i in range(Npts):
            xval = x[i]
            for j in range(Npts):
                yval = y[j]

                out[j][i] = monochromatic_beam_pixel(
                    DC, node, freqs, homs, qx, qy, xval, yval, given_freq,
                    nr, lambda0, zero_tem00_gouy
                )
    else:
        for i in range(Npts):
            xval = x[i]
            for j in range(Npts):
                yval = y[j]

                out[j][i] = spectral_beam_pixel(
                    DC, node, freqs, homs, qx, qy, xval, yval,
                    nr, lambda0, zero_tem00_gouy
                )
