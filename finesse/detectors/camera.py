"""Detectors for capturing images, slices and single pixels of
a beam.
"""

from abc import ABC, abstractmethod
import numbers

import numpy as np
import logging

LOGGER = logging.getLogger(__name__)

from finesse.element import model_parameter, Rebuild

from finesse.detectors.general import Detector
from finesse.detectors.compute import ccd_output, pixel_output
from finesse.utilities.misc import find_nearest

@model_parameter("f", None, Rebuild.Frequencies, units="Hz")
class Camera(Detector, ABC):
    """Base camera class."""
    def __init__(self, name, node, scale_to_waist, dtype, f=None):
        Detector.__init__(self, name, node, dtype)
        self.__waist_scaled = scale_to_waist
        self.f = f
        self._nr = self._node.space.nr if self._node.space is not None else 1.0
        self.__single_freq = f is not None

    @abstractmethod
    def get_output(self, DC, AC):
        pass

    @property
    def is_waist_scaled(self):
        """Flag for whether the x and y co-ordinates have been scaled by the waist-size.

        :getter: Returns `True` if x and y have been scaled by the beam waist, `False` otherwise.
        """
        return self.__waist_scaled

    @property
    def is_monochromatic(self):
        """Flag indicating whether the camera is configured to detect a single
        frequency field or multiple frequencies.

        :getter: Returns `True` if only detecting a single frequency of the light field.
        """
        return self.__single_freq


# FIXME (sjr) annoying to have to redefine model parameters for derived classes
@model_parameter("f", None, Rebuild.Frequencies, units="Hz")
class CCD(Camera):
    """Detector to capture an image of the beam at a specified node."""
    def __init__(self,
        name, node,
        xlim, ylim, npts,
        f=None,
        scale_to_waist=True
    ):
        """Constructs a new instance of `CCD` with the specified properties.

        Parameters
        ----------
        name : str
            Name of the CCD.

        node : :class:`.OpticalNode`
            Node to detect at.

        xlim : sequence or scalar
            Limits of the x-dimension of the image. If a single
            number is given then this will be `xlim = [-|xlim|, +|xlim|]`.

        ylim : sequence or scalar
            Limits of the y-dimension of the image. If a single
            number is given then this will be `ylim = [-|ylim|, +|ylim|]`.

        npts : int
            Number of points for each dimension.

        f : scalar or :class`.Parameter` or :class:`.ParameterRef`, optional
            Frequency of the field to detect. Defaults to `None` such that all
            fields are summed.

        scale_to_waist : bool, optional
            Flag determining whether to scale the x and y co-ordinates to the
            waist-size of the beam. Defaults to `True`.
        """
        Camera.__init__(self, name, node, scale_to_waist, dtype=np.ndarray, f=f)

        xlim = self.__limits_check(xlim)
        ylim = self.__limits_check(ylim)

        self.__x = np.linspace(xlim[0], xlim[1], npts)
        self.__y = np.linspace(ylim[0], ylim[1], npts)
        self._out = np.zeros((npts, npts), dtype=np.complex128)

    def __limits_check(self, value):
        if isinstance(value, numbers.Number):
            value = [-abs(value), abs(value)]

        elif hasattr(value, "__getitem__"):
            if len(value) != 2:
                raise TypeError("Expected limit to be a single number or sequence of size 2 "
                                f"but got a sequence of size: {len(value)}")

        else:
            raise TypeError("Unrecognised type for limit value.")

        return value

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def get_output(self, DC, AC):
        # TODO (sjr) need to do some caching here otherwise taking an image
        #            of the beam every data point is very slow
        values, is_changing = self._eval_parameters()
        f = values['f']

        freq = DC.frequency_map.get(f, None)
        sim = DC

        if freq is None and AC is not None:
            freq = AC.frequency_map.get(f, None)
            sim = AC

        if freq is None and self.is_monochromatic:
            LOGGER.warning(f"CCD: {self.name} is looking for a frequency: {f} Hz"
                            " that is not being modelled, returning None")
            return None

        node_q_map = sim.model.last_trace
        qx, qy, _ = node_q_map[self._node]

        if self.is_waist_scaled:
            x = qx.w0 * self.x.copy()
            y = qy.w0 * self.y.copy()
        else:
            x = self.x
            y = self.y

        # TODO (sjr) should probably cache this rather than re-constructing
        #            this array with every call to get_output
        freqs = np.array([float(freq_.f) for freq_ in sim.frequencies])

        ccd_output(
            sim, self._node, freqs, sim.homs, qx.q, qy.q, self._nr, sim.model.lambda0, x, y,
            sim.model.phase_config.ZERO_TEM00_GOUY,
            out=self._out, f=None if not self.is_monochromatic else freq.f
        )

        return self._out.copy()

    def at(self, x=None, y=None, magnitude=False):
        """Retrieves a slice or single pixel of the output image.

        Parameters
        ----------
        x : scalar, optional
            Value indicating where to take a y-slice of the image or,
            if used in conjunction with `y`, which pixel to return. Defaults
            to `None`.

        y : scalar, optional
            Value indicating where to take a x-slice of the image or,
            if used in conjunction with `x`, which pixel to return. Defaults
            to `None`.

        magnitude : bool, optional
            Returns the amplitude of the detected field if `True`. Otherwise
            returns the full complex description.

        Returns
        -------
        out : :class:`numpy.ndarray` or float
            Either a slice of the image or a single pixel at the specified co-ordinates.
        """
        if x is None and y is None:
            return np.abs(self._out) if magnitude else self._out

        if x is None:
            nearest_idx = find_nearest(self.y, y, index=True)
            values = self._out[nearest_idx][:]
            return np.abs(values) if magnitude else values

        if y is None:
            nearest_idx = find_nearest(self.x, x, index=True)
            values = self._out[:,nearest_idx]
            return np.abs(values) if magnitude else values

        nearest_indices = find_nearest(self.x, x, index=True), find_nearest(self.y, y, index=True)
        values = self._out[nearest_indices]
        return np.abs(values) if magnitude else values


@model_parameter('x', None, Rebuild.HOM & Rebuild.Frequencies)
@model_parameter('y', None, Rebuild.HOM & Rebuild.Frequencies)
@model_parameter("f", None, Rebuild.Frequencies, units="Hz")
class Pixel(Camera):
    """Detector to capture a single pixel of the image of a beam at a specified node."""
    def __init__(self,
        name, node,
        x=0, y=0,
        f=None,
        scale_to_waist=True
    ):
        """Constructs a new instance of `Pixel` with the specified properties.

        Parameters
        ----------
        name : str
            Name of the Pixel detector.

        node : :class:`.OpticalNode`
            Node to detect at.

        x : float
            The x co-ordinate of the pixel to measure. Defaults to `x = 0`.

        y : float
            The y co-ordinate of the pixel to measure. Defaults to `y = 0`.

        f : scalar or :class`.Parameter` or :class:`.ParameterRef`, optional
            Frequency of the field to detect. Defaults to `None` such that all
            fields are summed.

        scale_to_waist : bool, optional
            Flag determining whether to scale the x and y co-ordinates to the
            waist-size of the beam. Defaults to `True`.
        """
        if not isinstance(x, numbers.Number) or not isinstance(y, numbers.Number):
            raise TypeError("Both xlim and ylim must be single numbers when creating "
                            "a single pixel CCD.")

        Camera.__init__(self, name, node, scale_to_waist, dtype=np.complex128, f=f)

        self.x = x
        self.y = y

    # TODO (sjr) move common code to Camera.get_output (or another suitably named method)
    def get_output(self, DC, AC):
        values, is_changing = self._eval_parameters()
        x = values["x"]
        y = values["y"]
        f = values["f"]

        freq = DC.frequency_map.get(f, None)
        sim = DC

        if freq is None and AC is not None:
            freq = AC.frequency_map.get(f, None)
            sim = AC

        if freq is None and self.is_monochromatic:
            LOGGER.warning(f"Pixel detector: {self.name} is looking for a frequency: {f} Hz"
                            " that is not being modelled, returning None")
            return None

        node_q_map = sim.model.last_trace
        qx, qy, _ = node_q_map[self._node]

        if self.is_waist_scaled:
            x *= qx.w0
            y *= qy.w0

        # TODO (sjr) should probably cache this rather than re-constructing
        #            this array with every call to get_output
        freqs = np.array([float(freq_.f) for freq_ in sim.frequencies])

        return pixel_output(
            sim, self._node, freqs, sim.homs, qx.q, qy.q,
            x, y,
            self._nr, sim.model.lambda0,
            sim.model.phase_config.ZERO_TEM00_GOUY,
            f=None if not self.is_monochromatic else freq.f
        )
