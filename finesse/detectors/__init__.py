"""
The ``detectors`` sub-module contains various non-physical detectors
that can be used to probe to simulation at any point.

Listed below are all the sub-modules within ``detectors`` with brief
descriptions of what is contained in each.

.. autosummary::
    :toctree: detectors/
    {contents}

--------

The tables below list the available classes and functions exposed by the ``detectors``
sub-module of the Finesse package.

{toc}
"""

from finesse.detectors.general import Detector, SymbolDetector
from finesse.detectors.amplitude_detector import AmplitudeDetector
from finesse.detectors.bpdetector import BeamProperty, BeamPropertyDetector
from finesse.detectors.camera import CCD, Pixel
from finesse.detectors.gouy import Gouy
from finesse.detectors.powerdetector import PowerDetector
from finesse.detectors.quantum_noise_detector import QuantumNoiseDetector

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    def compile_toc(entries):
        toc = ''
        for section, objs in entries:
            toc += '\n\n.. rubric:: {}\n\n'.format(section)
            toc += '.. autosummary::\n\n'
            for obj in objs:
                toc += '\t~{}.{}\n'.format(obj.__module__, obj.__name__)
        return toc

    toc = (
        ('General Detectors', (
            Detector,
        )),
        ('Detectors', (
            AmplitudeDetector,
            PowerDetector,
            QuantumNoiseDetector,
        )),
    )

    __doc__ = __doc__.format(
        contents=_collect_submodules("detectors"),
        toc=compile_toc(toc),
    )
