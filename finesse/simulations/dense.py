"""
Sub-module consisting of the :class:`.Simulation` for
performing specific executions of a :class:`.Model`.
"""

import logging
import weakref
import numpy as np

from finesse.densematrix import DenseMatrix
from finesse.simulations.base import BaseMatrixSimulation

LOGGER = logging.getLogger(__name__)

class DenseSimulation(BaseMatrixSimulation):
    def solve(self):
        if not self.model.is_built:
            raise Exception("Model has not been built")

        # As we put -1 on diagonals we actually solve the negative
        # of the system, so inverse that here...
        # -1 here because we compute the negative of the interferometer
        # matrix. Why? Because the -1 sign only gets applied once along the
        # diagonal rather than for every non-diagonal element. Which means
        # equations are written as they are in math.
        self.out = -1*self._M.solve()
        return self.out

    def __enter__(self):
        """
        When entered the Simulation object will create the matrix to be used in
        the simulation.

        This is where the matrix gets allocated and constructed. It will expect
        that the model structure does not change after this until it has been
        exited.
        """
        # Set the matrix method we'll be using to run this simulation
        self._M = DenseMatrix(self.name)
        # Initialising the simulation expects there to be a self._M class that handles the
        # matrix build/memory/etc. This must be set before initialising.
        self._initialise()

        self._M.construct()
        self._fill()
        global M
        M = self._M.M
        return self

    def __exit__(self, type_, value, traceback):
        self._unbuild()

        self._M           = None
        self._Mq          = None
        self._submatrices = None

