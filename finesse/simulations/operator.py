import networkx as nx
import numpy as np
from collections import OrderedDict, namedtuple
from functools import partial
import logging

import finesse
from finesse.simulations.base import BaseSimulation

import finesse.utilities.operators.func_funs as ff
import finesse.utilities.operators.graph_funs as gf

from finesse.utilities.operators.abstract_operators import (
    OperatorMatrix
)

from finesse.utilities.operators.optical_operators import (
    PlanewaveOperator,
)

LOGGER = logging.getLogger(__name__)


class OperatorSimulation(BaseSimulation):
    '''
    There are three core methods that need to be implemented.

    * self.run() calls self.solve() as well as any preparation it needs.
    For DenseSimulation that means calling self._fill(). For OperatorSimulation
    I imagine the closest equivelant would be building the operators.

    * self.solve() should perform the matrix inversion as well as perform any
    propagations to nodes requested (could propagations be lazy?). DenseSimulation
    inverts the matrix and sets it to self.out (solve() itself returns None)

    * lazy propagations can be implemented through lazy property decorators.
    Specifically @cachedproperty in the python boltons package. Of course for that
    I will have to modify the node class (either the source or at runtime).

    * All nodes have "values"/"data"/"vector". Optical nodes have fields which 
    indexes the node value by the optical frequency

    * self.get_DC_out(node, freq) in DenseSimulation performs an array lookup on
    self.out using self.field(node, freq) to get the index

    I still don't know when the graph should be constructed. It's either on
    __init__ or __enter__. It looks like __enter__ is the one
    '''

    def __init__(self, model, name, op_type=PlanewaveOperator, frequencies=None, is_audio=False):
        super().__init__(model, name, frequencies, is_audio)
        self.op_type = op_type
        self.graph = gf.get_optical_network(self.model)
        self.optical_node_names = [
            x.full_name for x in self.model.optical_nodes]
        self.rev_graph = self.graph.reverse()
        self.laser_nodes = [x.p1.o.full_name for x in self.model.components if type(
            x) is finesse.components.laser.Laser]
        self.align_nodes = None

        self.out_dict = {}

    def run(self):
        self.solve()

    def solve(self):
        if not nx.is_frozen(self.model.network):
            raise Exception("Model has not been built")

        if self.N_cycles > 0:
            self._build_operators()

            # self.rhs_solve = self._M.solve()
            self._np_rhs_solve[:] = np.linalg.solve(self._np_M, self._np_rhs)
            self.rhs_solve = self._np_rhs_solve

        self._fill_other_nodes_fast()

    def _fill_other_nodes(self):
        self.out_fields = OperatorMatrix([gf.vec2field(
            self.rhs_solve, self.rhs_op.context, out_nodes=self.n_couple_diag)]).T
        self.out_fields.__init__()
        self.solved_fields = np.vstack([self.source_fields, self.out_fields])
        self.solved_fields.__init__()
        self.all_fields = self.solved_op_mat @ self.solved_fields

        self.out = self.all_fields.build_hard()

    def _fill_other_nodes_fast(self):
        self.source_fields.build_into_numpy(self.source_vec)
        self.solved_op_mat.build_into_numpy(self.solved_prop)

        if self.N_cycles > 0:
            self.solved_vec[:] = np.vstack([self.source_vec, self.rhs_solve])
        else:
            self.solved_vec[:] = self.source_vec

        self.out = self.solved_prop@self.solved_vec

    def field(self, node, freq=0, hom=0):
        """
        Returns simulation unique index of a field at a particular frequency
        index at this node.

        Parameters
        ----------
        node : :class:`.Node`
            Node object to get the index of.
        freq : int
            Frequency index.
        hom : int, optional
            Higher Order Mode index, defaults to zero.
        """
        Nf = self._node_info[node].nfreqs
        Nh = self._node_info[node].nhoms
        assert(freq < Nf)  # Requested frequency not available for this node
        assert(hom < Nh)   # Requested hom not available for this node
        return self._node_info[node].rhs_index + freq * Nh + hom

    def get_DC_out(self, node, freq=0, hom=0):
        return self.out[self.field(node, freq, hom)]

    def _build_graph(self):
        '''
        This method gets called only once on __enter__
        It can only be called once self._setup_frequencies() is called.
        '''
        if self.align_nodes is None:
            self.align_nodes = gf.only_unique_nodes(
                [cav.path.nodes_only[0] for cav in self.model.cavities])
            self.align_nodes = [node.full_name for node in self.align_nodes]

        # bookeeping for debugging
        self.all_cycles = list(nx.simple_cycles(self.graph))
        self.initial_solved_nodes = gf.get_solved_nodes(
            all_cycles=self.all_cycles)
        self.p_cycles = gf.prune_invalid_paths(
            self.all_cycles, self.initial_solved_nodes, del_invalid=True)
        self.a_cycles = gf.cycles2paths(
            gf.align_paths(self.p_cycles, self.align_nodes))
        self.g_cycles = ff.list_groupby(lambda x: x[0], self.a_cycles)

        # TODO rewrite n_couple

        # n_couple is the important one
        self.n_couple = gf.make_n_couple(
            self.g_cycles, self.graph).tolist()
        # self.n_couple = gf.graph2n_couple(self.graph, self.align_nodes)
        self.n_couple_diag = gf.get_n_couple_diag(self.n_couple)
        self.N_cycles = len(self.n_couple)

        self.red_op = gf.get_reduced_path_operators(
            self.graph, self.n_couple, op_type=self.op_type)
        self.red_sum_op = ff.recursive_list_map(
            lambda l: ff.recursive_reduce(lambda x, y: x+y, l, init=0),
            self.red_op)

        self.op_couple = 1 - OperatorMatrix(np.array(self.red_sum_op))

        if self.N_cycles > 0:
            self.op_couple.concretise()
            self.op_couple.keep_changing_symbols()
            self.op_couple.simplify_symbols(self.model.parameters)

    def _build_rhs(self):
        self.source_nodes = self.laser_nodes  # could have other sources
        self.solved_nodes = self.source_nodes + self.n_couple_diag

        self.source_fields = OperatorMatrix([
            [
                self.op_type(
                    node1=None,
                    node2=node,
                    scalar=gf.get_node_obj(self.graph, node).component.get_amplitude())
            ] for node in self.source_nodes])

        self.rhs_all_paths = gf.get_all_valid_paths(self.graph, self.source_nodes, self.n_couple_diag)
        self.rhs_valid_paths = gf.prune_invalid_paths(self.rhs_all_paths, self.n_couple_diag)
        self.rhs_red_op = gf.get_reduced_path_operators(
            self.graph, self.rhs_valid_paths, op_type=self.op_type)
        self.rhs_red_sum_op = ff.recursive_list_map(
            lambda l: ff.recursive_reduce(lambda x, y: x+y, l, init=0),
            self.rhs_red_op)

        self.rhs_op = OperatorMatrix(np.array(self.rhs_red_sum_op))

        if self.N_cycles > 0:
            self.rhs_fields = self.rhs_op @ self.source_fields
        else:
            self.rhs_fields = self.source_fields

        self.rhs_fields.concretise()
        self.source_fields.concretise()

        self.rhs_fields.keep_changing_symbols()
        self.source_fields.keep_changing_symbols()

        self.rhs_fields.simplify_symbols(self.model.parameters)
        self.source_fields.simplify_symbols(self.model.parameters)

    def _build_solution_propagations(self):
        self.solved_paths_list = gf.get_all_valid_paths(
            self.graph, self.solved_nodes, self.optical_node_names)
        self.solved_op_mat = OperatorMatrix(gf.get_reduced_summed_operators(
            self.graph, self.solved_paths_list, self.op_type))

        self.solved_op_mat.concretise()
        self.solved_op_mat.keep_changing_symbols()
        self.solved_op_mat.simplify_symbols(self.model.parameters)

    def _allocate_matrix(self):
        '''
        This method gets called only once on __enter__ since the values should
        automatically update when any relevant model paramter changes.

        This method needs to know which frequencies are present in order to determine
        the size of the cycle coupling matrix.
        '''
        pass
        self.op_size = self.op_type().context['N']
        # self._M = self.op_couple.allocate_dense_build_memory()

        if self.N_cycles > 0:
            self._np_M = self.op_couple.allocate_numpy_build_memory()
            self._np_rhs = self.rhs_fields.allocate_numpy_build_memory()
            self._np_rhs_solve = self.rhs_fields.allocate_numpy_build_memory()

        self.out = np.zeros(
            [len(self.optical_node_names), 1], dtype=np.complex128)
        self.solved_vec = np.zeros(
            [len(self.solved_nodes), 1], dtype=np.complex128)
        self.source_vec = np.zeros(
            [len(self.source_nodes), 1], dtype=np.complex128)
        self.solved_prop = np.zeros(
            [len(self.optical_node_names), len(self.solved_nodes)], dtype=np.complex128)

    def _build_operators(self):
        '''
        This method will be called every time self.solve()
        is called.
        '''
        # self.op_couple.build_into_dense(self._M)
        # self._M.set_rhs_vec(self.rhs_fields.build_hard())

        self.op_couple.build_into_numpy(self._np_M)
        self.rhs_fields.build_into_numpy(self._np_rhs)

    def _initialise(self):
        self._components = self.model.components
        self._detectors = self.model.detectors
        self._node_info = OrderedDict()
        self.NodeInfoEntry = namedtuple(
            "NodeInfoEntry", "index rhs_index freq_index nfreqs nhoms")
        self._network = self.model.network

        if not self.is_audio:
            LOGGER.info("Building DC simulation")
            # For now we just get all the optical nodes to simulate in the DC matrix
            # convert from weakrefs
            self._nodes = {n: self._network.nodes[n]['weakref']()
                           for n in self._network.nodes
                           if 'optical' in self._network.nodes[n]}
        else:
            LOGGER.info("Building AC simulation")
            # For audio matrix we solve all types
            self._nodes = {
                n: self._network.nodes[n]['weakref']() for n in self._network.nodes}

        self._setup_frequencies()
        self._frequency_map = {f.f: f for f in self.frequencies}

        # this is needed to call _initialise_nodes which sets self._node_info
        # by calling self.NodeInfoEntry() on all of the nodes
        if self.is_modal:
            self._Nhom = self._HOMs.shape[0]
        else:
            self._Nhom = 1

        self._initialise_nodes()

    def __enter__(self):
        self._initialise()

        self.N_freqs = len(self.frequencies)
        if self.N_freqs > 1:
            raise Exception(
                "Alexei hasn't made operators work for multiple frequencies yet")

        self._build_graph()
        self._build_rhs()
        self._build_solution_propagations()

        self._allocate_matrix()

        return self

    def __exit__(self, type_, value, traceback):
        pass
