import matplotlib.pyplot as plt
from os import path
import finesse
import numpy as np
import matplotlib.pyplot as plt
from finesse.analysis import (
    xaxis,
)

from finesse.simulations import (
    DenseSimulation,
    KLUSimulation,
)

from finesse.simulations.digraph import (
    DigraphSimulationReducing,
    DigraphSimulationReducingCSC,
)

import pytest

@pytest.fixture()
def finesse_setup():
    # initialise matplotlib rcParams to appropriate values for a "display" mode
    finesse.plotting.init()
    finesse.LOGGER.setLevel("WARN")

# ## Finesse 3 Introduction --- Fabry-Perot Cavity
#
# This notebook is intended as an introduction to the Finesse 3 syntax and features by way of a simple example of a Fabry-Perot cavity configuration.
#
# **Key Points**
#
# - `.kat` files with largely the same syntax as Finesse 2 can be parsed into a Finesse 3 [Model](https://finesse.readthedocs.io/en/latest/api/finesse.model.html#module-finesse.model).
# - This model object stores the [network](https://finesse.readthedocs.io/en/latest/api/generated/finesse.model.Model.network.html#finesse.model.Model.network) which is a directed graph
#   where the `node_type` is the [full_name](https://finesse.readthedocs.io/en/latest/api/components/node/generated/finesse.components.node.Node.full_name.html#finesse.components.node.Node.full_name) property
#   of a Node object. See [this documentation](https://finesse.readthedocs.io/en/latest/usage/advanced_usage/nodesystem/index.html) for in-depth details on the port and node system of Finesse 3.
# - Parameter references can be set up in `.kat` files by using the `$` syntax:
#   - track the parameter of another component using `$<comp_name>.param` -- e.g: `ad ampdet $EOM.f n1` tells Finesse to
#     create an amplitude detector where the detection frequency is set up to track the frequency of the modulator named `EOM`.
#   - more complicated referencing / tracking can be performed using `$$<eqn>$$` -- e.g. `ad ampdef $$EOM1.f + 2*EOM2.f$$ n1` tells Finesse
#     to create an amplitude detector where the detection frequency is set to track a frequency equal to EOM1 modulation freq. plus two times EOM2 modulation freq.
# - Simulations can be run over ranges of parameters with the `xaxis`, `x2axis` functions. Building of the Simulation object is performed internally within
#   these functions using the model associated with the tunable parameters.

# In the following cell we set up a Finesse model of a cavity using the familiar `.kat` style syntax. The amplitude detectors `Cp1` and `Cm1` are initialised such that the frequencies of the field that they detect track the positive and negative values of the modulator `eo1`, respectively. This means that if `eo1.f` is modified in any way later on, then `Cp1` and `Cm1` will still detect the upper and lower sidebands of the carrier circulating in the cavity.
#
# *Note: When we have different solvers (e.g. network reduction based solver) there may be an optional argument `solver="name_of_solver"` in xaxis, x2axis etc. which allows the user to choose which routine they want to use - defaulting to the current KLUMatrix solver acting on the whole coupling matrix.*


def test_FP_DGred(finesse_setup, tpath):
    ifo = finesse.parse(kat_noHOMs).model

    # run a simulation where we vary the tuning of m1 from -100 deg to 280 deg with 1000 steps
    analysis = xaxis(ifo.m1.phi, -100, 280, 1000, carrier_sim_type = DigraphSimulationReducingCSC)
    out = analysis.run()

    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(1, 1, 1)
    ax.semilogy(out.x1, abs(out['R']), label="reflected")
    ax.semilogy(out.x1, abs(out['C']), label="circulating")
    ax.semilogy(out.x1, abs(out['Cp1']), label="circulating upper sideband")
    ax.semilogy(out.x1, abs(out['Cm1']), label="circulating lower sideband")
    ax.semilogy(out.x1, abs(out['T']), label="transmitted")
    ax.legend()
    ax.set_xlabel(r"m1 $\phi$ [deg]")
    ax.set_ylabel(r"amplitude [$\sqrt{W}$]")

    fig.savefig(path.join(tpath, 'Fabry_scan.pdf'))


def test_FP_DGred_HOMs(finesse_setup, tpath):
    ifo = finesse.parse(kat_HOMs).model

    # run a simulation where we vary the tuning of m1 from -100 deg to 280 deg with 1000 steps
    analysis = xaxis(ifo.m1.phi, -100, 280, 1000, carrier_sim_type = DigraphSimulationReducingCSC)
    out = analysis.run()

    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(1, 1, 1)
    ax.semilogy(out.x1, abs(out['R']), label="reflected")
    ax.semilogy(out.x1, abs(out['C']), label="circulating")
    ax.semilogy(out.x1, abs(out['Cp1']), label="circulating upper sideband")
    ax.semilogy(out.x1, abs(out['Cm1']), label="circulating lower sideband")
    ax.semilogy(out.x1, abs(out['T']), label="transmitted")
    ax.legend()
    ax.set_xlabel(r"m1 $\phi$ [deg]")
    ax.set_ylabel(r"amplitude [$\sqrt{W}$]")

    fig.savefig(path.join(tpath, 'Fabry_scan.pdf'))


def test_FP_DGred_matcheck(finesse_setup, tpath, plot):
    ifoKLU = finesse.parse(kat_HOMs).model
    ifoDG = finesse.parse(kat_HOMs).model

    ifoKLU.m1.phi.is_tunable = True
    ifoDG.m1.phi.is_tunable = True

    DCsimKLU, = ifoKLU.build(carrier_sim_type = DenseSimulation)
    DCsimDG, = ifoDG.build(carrier_sim_type = DigraphSimulationReducingCSC)

    with DCsimKLU as xDCsimKLU, DCsimDG as xDCsimDG:
        for phi in np.linspace(-100, -1, 4):
            ifoKLU.m1.phi.value = phi
            ifoDG.m1.phi.value = phi

            DCsimDG.run()
            DCsimKLU.run()

        print(DCsimDG._presolve)

        from icecream import ic
        def map(k):
            #print(k)
            n, p2p, f1, f2 = k
            return n.name, p2p, f1.f, f2.f
        sKLU = {map(k):v for k,v in DCsimKLU._submatrices.items()}
        sDG = {map(k):v['iodx_semantic'] for k,v in DCsimDG._submatrix_semantic_map.items()}

        #for k, M_klu in sKLU.items():
        #    ekey = tuple(sDG[k])
        #    M_DG = DCsimDG._M_edges[ekey]
        #    M_DGklu = DCsimDG._presolve.edgesKLU.get(ekey, None)
        #    if M_DGklu is not None:
        #        #ic(M_klu.view - M_DGklu.view)
        #        #ic(M_klu.view - M_DGklu.view)
        #        pass
        #    #ic(M_klu)
        #    #ic(M_DG)
        #    pass
        #for ekey, M_DGklu in DCsimDG._presolve.edgesKLU.items():
        #    n_fr, n_to = ekey
        #    #if n_fr == n_to:
        #    #    ic(n_fr, M_DGklu.view)

        #M1 = DCsimKLU.M().M
        #plt.imshow(abs(M1), origin='upper')
        #plt.colorbar()
        #plt.savefig('test1.png', dpi = 1000)
        #plt.clf()

        #KM = DCsimDG._presolve.sumKLU
        #N = KM.num_equations
        #vals = KM.get_matrix_elements()
        #M2 = np.zeros((N,N), dtype=complex)
        #for _ in vals:
        #    M2[_[1],_[0]] = _[2]
        #shape1 = M1.shape
        #M2x = M2[:shape1[0], :shape1[0]]
        #plt.imshow(abs(M2x - M1), origin='upper')
        #plt.colorbar()
        #plt.savefig('test2.png', dpi = 1000)

        ##print(, )
        ##print(DCsimDG._presolve.betaCSC.tocoo())
        #ic(np.any(abs(M2x - M1) > 1e-8))
        ##ic(DCsimKLU.M().rhs)
        ##ic(M2[:, -1])
        ic(DCsimDG._presolve.out)
        ic(DCsimKLU.out)

    
#def test_FP_DGred_matcheck(finesse_setup, tpath, plot):
#    ifoKLU = finesse.parse(kat_HOMs)
#
#    ifoKLU.m1.phi.is_tunable = True
#
#    DCsimKLU, = ifoKLU.build(carrier_sim_type = KLUSimulation)
#
#    with DCsimKLU as xDCsimKLU:
#        for phi in np.linspace(-100, 280, 1000):
#            ifoKLU.m1.phi.value = phi
#
#            DCsimKLU.run()
#
#        KM = DCsimKLU.M()
#        N = KM.num_equations
#        vals = KM.get_matrix_elements()
#        M2 = np.zeros((N,N), dtype=complex)
#        for _ in vals:
#            M2[_[1],_[0]] = _[2]
#        plt.imshow(abs(M2), origin='upper')
#        plt.colorbar()
#        plt.savefig('test1.png', dpi = 1000)

    
#def test_FP_DGred_matcheck(finesse_setup, tpath, plot):
#    ifoKLU = finesse.parse(kat_HOMs)
#
#    ifoKLU.m1.phi.is_tunable = True
#
#    DCsimKLU, = ifoKLU.build(carrier_sim_type = DenseSimulation)
#
#    with DCsimKLU as xDCsimKLU:
#        for phi in np.linspace(-100, 280, 10):
#            ifoKLU.m1.phi.value = phi
#
#            DCsimKLU.run()
#
#        print("FILL")
#        #DCsimKLU.fill(carrier = DCsimKLU)
#
#        plt.matshow(
#            abs(DCsimKLU.M().M),
#            origin='upper',
#            interpolation='nearest',
#        )
#        #plt.colorbar()
#        plt.savefig('test1.png', dpi = 1000)



kat_HOMs = (
"""
l l1 1 0 n0
s s1 0 n0 n1
mod eo1 100M .1 1 pm n1 n2
s s2 1 n2 n3
m m1 0.99 0.01 0 n3 n4
s s3 1 n4 n5
m m2 0.99 0.01 0 n5 n6
attr m1 Rc -10
attr m2 Rc 10
cav cav m1 n4 m2 n5
maxtem 0

ad R 0 n3 # amplitude detector for reflected field at carrier frequency
ad C 0 n5 # amplitude detector for circulating field at carrier frequency
ad Cp1 $eo1.f n5 # amplitude detector for circulating field at frequency = tracking mod freq. of eo1 (upper sideband)
ad Cm1 $-1*eo1.f n5 # amplitude detector for circulating field at frequency = -1 * tracking mod freq. of eo1 (lower sideband)
ad T 0 n6 # amplitude detector for transmitted field at carrier frequency
"""
)

kat_noHOMs = (
"""
l l1 1 0 n0
s s1 0 n0 n1
mod eo1 100M .1 1 pm n1 n2
s s2 1 n2 n3
m m1 0.99 0.01 0 n3 n4
s s3 1 n4 n5
m m2 0.99 0.01 0 n5 n6

ad R 0 n3 # amplitude detector for reflected field at carrier frequency
ad C 0 n5 # amplitude detector for circulating field at carrier frequency
ad Cp1 $eo1.f n5 # amplitude detector for circulating field at frequency = tracking mod freq. of eo1 (upper sideband)
ad Cm1 $-1*eo1.f n5 # amplitude detector for circulating field at frequency = -1 * tracking mod freq. of eo1 (lower sideband)
ad T 0 n6 # amplitude detector for transmitted field at carrier frequency
"""
)
