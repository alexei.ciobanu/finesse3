# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals, absolute_import
from os import path

from finesse.simulations.matrix_digraph import (
    SREdense_numeric_inverse,
    SRE_graphs,
)

import numpy as np
import scipy.sparse as scisparse

from help_SREtest import sparse_gen

def test_SREdense_inversion(ic, tpath):

    N = 100

    N_nodes = 10
    node_avg_size = N // N_nodes

    node_names = ["node_{}".format(i) for i in range(N_nodes)]
    node_sizes = [node_avg_size for i in range(N_nodes)]

    node_offset_cnt = 0
    node_offsets = []
    for idx, node in enumerate(node_names):
        node_offsets.append(node_offset_cnt)
        node_offset_cnt += node_sizes[idx]

    node_sizes_d   = dict(zip(node_names, node_sizes))
    node_offsets_d = dict(zip(node_names, node_offsets))

    sparseG = sparse_gen(
        N              = N,
        node_avg_size  = node_avg_size,
        node_names     = node_names,
        node_sizes_d   = node_sizes_d,
        node_offsets   = node_offsets,
        node_offsets_d = node_offsets_d,
    )

    #add the diagonal
    for i in range(N):
        sparseG['insert_value'](i, i, 1)

    first_idx = 0
    last_idx = np.random.randint(0, N)
    sparseG['insert_value'](first_idx, last_idx, 1)
    for i in range(15):
        next_idx = np.random.randint(0, N)
        sparseG['insert_value'](last_idx, next_idx, -.5)
        #have a small chance to reset a loop
        if np.random.randint(0, 10) == 0:
            sparseG['insert_value'](next_idx, first_idx, 1)
            first_idx = np.random.randint(0, N)
            last_idx = np.random.randint(0, N)
            sparseG['insert_value'](first_idx, last_idx, .1)
        else:
            last_idx = next_idx

    SRE_graphs.SRE_graph(
        path.join(tpath, 'testRAND.pdf'),
        sparseG['make_SREdense'](),
    )
    A_csc = sparseG['make_csc']()
    dense = A_csc.todense()
    idense = np.linalg.inv(dense)
    #print(dense)

    metamat = np.zeros_like(dense)
    seq, req, edges = sparseG['make_SREdense']()
    print(edges)
    iseq, ireq, iedges = SREdense_numeric_inverse(
        seq, req, edges,
        inputs     = sparseG['node_names'],
        outputs    = sparseG['node_names'],
        node_order = node_names,
        metamat    = metamat,
    )

    data_ind = []
    row_ind  = []
    col_ind  = []
    ikm_dense2 = np.zeros_like(dense)
    for (n_fr, n_to), edge in iedges.items():
        offset_R = node_offsets_d[n_to]
        offset_C = node_offsets_d[n_fr]
        edgem = edge
        ikm_dense2[offset_R : offset_R + edgem.shape[-2], offset_C : offset_C + edgem.shape[-1]] = edgem
        for idx_R, idx_C in np.argwhere(edgem):
            val = edgem[idx_R, idx_C]
            data_ind.append(val)
            row_ind.append(idx_R + offset_R)
            col_ind.append(idx_C + offset_C)
    ikm_csc = scisparse.csc_matrix(
        (data_ind, (row_ind, col_ind)),
        shape = (N, N),
        dtype = float,
    )
    ikm_dense = ikm_csc.todense()

    #print((ikm_dense @ dense))

    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax0 = fig.add_subplot(2, 2, 1)
    ax1 = fig.add_subplot(2, 2, 2)
    ax2 = fig.add_subplot(2, 2, 3)
    ax3 = fig.add_subplot(2, 2, 4)
    ax0.matshow(dense != 0)
    ax1.matshow(idense != 0)
    ax2.matshow((ikm_dense2 @ dense))
    ax3.matshow(ikm_dense - idense)
    fig.savefig(path.join(tpath, 'testDENSE.pdf'))
