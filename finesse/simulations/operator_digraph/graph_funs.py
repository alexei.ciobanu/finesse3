
import finesse
import numpy as np
import networkx as nx

from .abstract_operators import NullOperator, IdentityOperator
from .func_funs import (
    recursive_list_map,
    recursive_reduce,
    list_roll,
    list_flatten,
    ordered_intersect,
    recursive_list_comprehension,
    recursive_map
)


def get_node_obj(G, node_str):
    node_obj = G.nodes[node_str]['weakref']()
    return node_obj


def get_optical_network(model):
    optical_network = model.network.copy()
    for node_str in model.network.nodes:
        node_obj = get_node_obj(optical_network, node_str)
        # 0 being the enum for optical node
        if node_obj.type.value == 0:
            # keep optical node
            pass
        else:
            # remove non optical node
            # the edges should be updated accordingly by networkx
            optical_network.remove_node(node_str)
    return optical_network


def get_edge_data(G, u, v, data=True):
    if data is True:
        # return all dict members
        return G.edges[u, v]
    else:
        # return the "data" member
        return G.edges[u, v][data]


def get_edge_operator(G, u, v, op_type='LCT'):
    if (u is None or v is None):
        # special case
        return NullOperator(node1=u, node2=v)
    elif u == v:
        # Another special case (very dangeorus)
        # We implicitly make an assumption that all
        # nodes couple to themselves via the identity op,
        # which may not be true for all graphs.
        #
        # This was introduced as a hack for constructing
        # operator linear equations.
        return IdentityOperator(node1=u, node2=v)
    else:
        edge_owner_weakref = get_edge_data(G, u, v, 'owner')
        return edge_owner_weakref().get_operator(u, v, op_type=op_type)


def get_path_operators(G, path, op_type='LCT'):
    if path == []:
        return []
    elif type(path[0]) is list:
        return [get_path_operators(G, path[0], op_type=op_type)] + get_path_operators(G, path[1:], op_type=op_type)
    else:
        t = []
        N = len(path)
        for i in range(1, N):
            u = path[i-1]
            v = path[i]
            t.append(get_edge_operator(G, u, v, op_type=op_type))
        return t


def reduce_operator(op_list):
    acc = op_list[0]
    for op in op_list[1:]:
        acc = op@acc
    return acc


def get_reduced_path_operators(G, path, op_type='LCT', debug=False):
    reduced_operators = recursive_list_map(
        reduce_operator, get_path_operators(G, path, op_type=op_type))
    return reduced_operators


def get_reduced_summed_operators(G, path, op_type='LCT', debug=False):
    reduced_operators = get_reduced_path_operators(
        G,
        path,
        op_type=op_type,
        debug=debug)
    summed_reduced_operators = recursive_list_map(
        lambda l: recursive_reduce(lambda x, y: x+y, l, init=0),
        reduced_operators)
    return summed_reduced_operators


def get_edge_operator_description(G, u, v):
    edge_owner_weakref = get_edge_data(G, u, v, 'owner')
    return edge_owner_weakref().get_operator(u, v, op_type='str')


def get_path_operator_descriptions(G, path):
    if path == []:
        return []
    elif type(path[0]) is list:
        return [get_path_operator_descriptions(G, path[0])] + get_path_operator_descriptions(G, path[1:])
    else:
        t = []
        N = len(path)
        for i in range(1, N):
            u = path[i-1]
            v = path[i]
            t.append(get_edge_operator_description(G, u, v))
        return t


def get_cycle_operators(G, cycle):
    t = []
    for u, v in zip(cycle, list_roll(cycle)):
        t.append(get_edge_operator(G, u, v))
    return t


def get_all_cycle_couple_paths(G, A, B):
    A = list_flatten(A)
    B = list_flatten(B)
    return list(nx.all_simple_paths(G, A[0], B[0]))


def get_cycle_couple_path(G, A, B):
    # just get the first one
    return get_all_cycle_couple_paths(G, A, B)[0]


def get_all_node_paths(G, node1, node2):
    return list(nx.all_simple_paths(G, node1, node2))


def are_nodes_unique(path):
    seen = []
    for node in path:
        if node not in seen:
            seen.append(node)
        else:
            return False
    return True


def only_unique_nodes(path):
    seen = []
    for node in path:
        if node not in seen:
            seen.append(node)
    return seen


def cycle_contains_cycle(A, B):
    if len(A) < len(B):
        return False
    else:
        o_isec = ordered_intersect(A, B)
        if o_isec == []:
            return False
        m = max(o_isec, key=len)
        if len(m) == len(B):
            return True
        else:
            return False


def alexei_cycles_prune(cycles):
    cycles = cycles.copy()
    cycles.sort(key=len, reverse=True)  # sort so longest cycles are first
    del_ind = []
    # find all cycles that contain another cycle
    for i in range(len(cycles)):
        for j in range(i, len(cycles)):
            if cycle_contains_cycle(cycles[i], cycles[j]):
                del_ind.append(i)
                break
    return [x for i, x in enumerate(cycles) if i not in del_ind]


def alexei_paths_prune(paths, del_paths=False):
    paths = paths.copy()
    paths.sort(key=len, reverse=True)  # sort so longest paths are first
    del_ind = []
    # find all cycles that contain another cycle
    for i in range(len(paths)):
        for j in range(i, len(paths)):
            if cycle_contains_cycle(paths[i], paths[j]):
                del_ind.append(i)
                break
    if del_paths:
        return [x for i, x in enumerate(paths) if i not in del_ind]
    else:
        return [x if i not in del_ind else [x[0], None, x[-1]] for i, x in enumerate(paths)]


def get_overlapping_node(A, B):
    for i, ni in enumerate(A):
        for j, nj in enumerate(B):
            if ni is nj:
                return ni, i, j
    return None, None, None


def append_node(node, g_path):
    if g_path == []:
        return []
    else:
        if type(g_path[0]) is list:
            return [append_node(node, g_path[0])] + append_node(node, g_path[1:])
        else:
            return g_path + [node]


def cycle2path(g_cycle):
    c = g_cycle[0]
    while type(c) is list:
        c = c[0]
    return append_node(c, g_cycle)


def alexei_cycle_group(cycles):
    # group all the pruned cycles that share a node
    # also make sure the grouped cycles start at the shared node
    cycles = cycles.copy()  # avoid making changes to state
    if cycles == []:
        return []
    N = len(cycles)
    # print(N,cycles)
    ci = cycles[0]
    for j, cj in enumerate(cycles[1:]):
        nk, k, m = get_overlapping_node(ci, cj)
        if nk is not None:
            # roll the cycles so that they both start on the overlapping node
            ti = list_roll(ci, k)
            tj = list_roll(cj, m)
            assert(nk is ti[0] is tj[0])
            if N < 2:
                # base case for recursion
                return [[ti, tj]]
            else:
                # append the coupled cycles and recurse
                del cycles[j]
                return [[ti, tj]] + alexei_cycle_group(cycles[1:])
    if N < 2:
        return [[ci]]
    else:
        return [[ci]] + alexei_cycle_group(cycles[1:])


def get_cycle_couple_matrix(graph, g_cycles):
    # g_cycles are the pruned and grouped cycles
    # returns a matrix of the
    N = len(g_cycles)
    m_couple = [[None for x in range(N)] for y in range(N)]  # preallocate list
    for i, ci in enumerate(g_cycles):
        for j, cj in enumerate(g_cycles):
            if i == j:
                # append the first node to diagonal entries to turn them from cycles to paths
                m_couple[i][j] = cycle2path(ci)
            else:
                all_paths = get_all_cycle_couple_paths(graph, ci, cj)
                remaining_cycles = [c for ind, c in enumerate(
                    g_cycles) if ind not in [i, j]]
                del_ind = []
                for k, path in enumerate(all_paths):
                    if any([node in list_flatten(remaining_cycles) for node in path if node not in list_flatten([ci, cj])]):
                        # remove any paths that go through any cavity other than ci or cj
                        del_ind.append(k)
                m_couple[j][i] = [path for ind, path in enumerate(
                    all_paths) if ind not in del_ind]
    return m_couple


def get_connectivity_matrix(m_couple):
    '''Mostly for debugging. The elements of the connectivity elements
    indicate the amount of terms for that coupling.'''
    N = len(m_couple)
    m_conn = [[None for x in range(N)] for y in range(N)]
    m_head = [[None for x in range(N)] for y in range(N)]
    for i, col in enumerate(m_couple):
        for j, el in enumerate(col):
            if el == []:
                m_conn[i][j] = 0
            elif type(el[0]) is list:
                m_conn[i][j] = len(el)
                m_head[i][j] = el[0][0]
            else:
                m_conn[i][j] = 1
                m_head[i][j] = el[0]
    return np.array(m_conn), m_head


def alexei_cycles_align(p_cycles, align_nodes=[]):
    a_cycles = []
    align_nodes = only_unique_nodes(align_nodes)
    for path in p_cycles:
        incidence_count = np.sum(
            [align_node in path for align_node in align_nodes])
        if incidence_count > 1:
            raise Exception(
                f'Multiple align nodes from {align_nodes} in path {path}. Cannot align path to multiple nodes.')
        elif incidence_count == 0:
            a_cycles.append(path)
        elif incidence_count == 1:
            align_node = [
                align_node for align_node in align_nodes if align_node in path][0]
            while path[0] != align_node:
                path = list_roll(path, 1)
            a_cycles.append(path)
        else:
            raise Exception(f'Undefined alignment')
    return a_cycles


def graph2n_couple(graph, align_nodes=[], debug=False):
    cycles = list(nx.simple_cycles(graph))
    p_cycles = alexei_paths_prune(cycles, del_paths=True)
    if align_nodes == []:
        if debug:
            print('Warning: no alignment nodes given. Node order will be \
    determined randomly by networkx. Networkx generates random seed \
    for node ordering at import time.')
    # print(p_cycles)
    a_cycles = alexei_cycles_align(p_cycles, align_nodes)
    # print(a_cycles, align_nodes)
    g_cycles = alexei_cycle_group(a_cycles)
    # print(g_cycles)
    n_couple = get_cycle_couple_matrix(graph, g_cycles)
    # print(get_n_couple_diag(n_couple))
    return n_couple


def get_n_couple_diag(n_couple):
    diag = []
    for i in range(len(n_couple)):
        diag.append(n_couple[i][i][0][0])
    return diag


def model2n_couple(ifo):
    graph = get_optical_network(ifo)
    return graph2n_couple(graph)


def build_m_couple(ifo):
    graph = get_optical_network(ifo)
    n_couple = graph2n_couple(graph)
    p_couple = get_path_operator_descriptions(graph, n_couple)
    return p_couple


def get_rhs_fields(model, n_couple, op_type='planewave', graph=None):
    laser_nodes = [x.p1.o.full_name for x in model.components if type(
        x) is finesse.components.laser.Laser]
    if len(laser_nodes) > 1:
        raise Exception(
            "Alexei didn't add support multiple laser components yet")

    if graph is None:
        graph = get_optical_network(model)

    fields = [get_node_obj(graph, node).component.get_field(op_type)
              for node in laser_nodes]
    return np.array(fields)


def get_all_rhs_paths(graph, n_couple, source_nodes):
    '''
    Returns an (N, M) matrix where N is the size of the n_couple
    matrix (the number of cycles in system). M is the number of source
    nodes. The elements of the matrix are lists of paths that sum to
    that matrix element.

    The intention is to do a recursive_list_map on this matrix to get
    the RHS path operator matrix, which is then acts on the RHS source
    field vector to get the RHS fields which can be built to get the RHS
    vector of the linear system.

    * first index: row of rhs path matrix
    * second index: column of rhs path matrix
    * third index: list of paths that sum to get that matrix element
    '''
    n_couple_diag = get_n_couple_diag(n_couple)

    rev_graph = graph.reverse()

    rhs_paths = []
    for target_node in n_couple_diag:
        temp = []
        for source_node in source_nodes:
            temp.append(list(nx.all_simple_paths(
                rev_graph, target_node, source_node)))
        rhs_paths.append(temp)

    return recursive_list_map(lambda x: x[::-1], rhs_paths)


def get_all_rhs_paths2(graph, n_couple, source_nodes):
    '''the old version'''
    n_couple_diag = get_n_couple_diag(n_couple)

    rev_graph = graph.reverse()

    rhs_paths = []
    for node in n_couple_diag:
        rhs_paths.append(
            list(nx.all_simple_paths(rev_graph, node, source_nodes)))

    return recursive_list_map(lambda x: x[::-1], rhs_paths)


def prune_rhs_path(rhs_path, solved_nodes):
    if any([node in rhs_path[1:-1] for node in solved_nodes]):
        return [rhs_path[0], None, rhs_path[-1]]
    else:
        return rhs_path


def prune_rhs_paths(all_rhs_paths, solved_nodes):
    return recursive_list_map(lambda path: prune_rhs_path(path, solved_nodes), all_rhs_paths)


def get_rhs_path(model, n_couple, graph=None):
    # need to get the laser nodes
    laser_nodes = [x.p1.o.full_name for x in model.components if type(
        x) is finesse.components.laser.Laser]
    if len(laser_nodes) > 1:
        raise Exception(
            "Alexei didn't add support multiple laser components yet")

    if graph is None:
        graph = get_optical_network(model)

    laser_node = laser_nodes[0]

    N_cav = len(n_couple)
    cav_list = [n_couple[i][i] for i in range(0, N_cav)]

    filtered_paths = [[] for i in range(N_cav)]

    for i in range(N_cav):
        ci_nodes = list_flatten(n_couple[i][i])
        ci_head = ci_nodes[0]
        cav_list = [n_couple[j][j] for j in range(N_cav) if j != i]
        test_paths = list(nx.all_simple_paths(graph, laser_node, ci_head))
        for test_path in test_paths:
            redundant_path = False
            for node in test_path:
                if node in list_flatten(cav_list):
                    redundant_path = True
                    break
            if not redundant_path:
                filtered_paths[i].append(test_path)
        if filtered_paths[i] == []:
            # if no valid paths found append start node and end node for bookkeeping
            filtered_paths[i].append([laser_node, None, ci_head])
#         filtered_path = [path for path in test_paths if any([node for node in path if node not in list_flatten(cav_list)])]

    # append None to all empty paths to mark where the null operators should go
    #filtered_paths = [[None] if path == [] else path for path in filtered_paths]

    return filtered_paths


def get_rhs_vec(model, n_couple, op_type='planewave'):
    graph = get_optical_network(model)

    rhs_path = get_rhs_path(model, n_couple, graph=graph)
    rhs_op = np.array(get_reduced_path_operators(
        graph, get_rhs_path(model, n_couple), op_type=op_type))

    rhs_fields = np.array(get_rhs_fields(model, n_couple, op_type=op_type))

    # @ operator doesn't work on numpy object arrays until next version
    # use np.dot for now
    rhs_vec = np.dot(rhs_op, rhs_fields)
    return rhs_vec


def build_n_couple(n_couple, graph, op_type='planewave'):

    red_o_couple = get_reduced_path_operators(graph, n_couple, op_type=op_type)

    N_cav = len(n_couple)
    op_size = red_o_couple[0][0][0].op_size

    d_couple = np.zeros([N_cav*op_size, N_cav*op_size], dtype=np.complex128)

    for i in range(N_cav):
        for j in range(N_cav):
            d_couple[i*op_size:(i+1)*op_size, j*op_size:(j+1)
                     * op_size] = red_o_couple[i][j][0].build()
    return d_couple


def build_rhs_vec(model, n_couple, op_type='planewave'):
    rhs_vec = get_rhs_vec(model, n_couple, op_type=op_type)

    N = len(rhs_vec)
    op_size = rhs_vec[0].op_size

    d_rhs = np.zeros([N*op_size], dtype=np.complex128)
    for i in range(N):
        d_rhs[i*op_size:(i+1)*op_size] = rhs_vec[i].build()
    return d_rhs

###############################################################################


def prune_invalid_path(path, solved_nodes, del_invalid=False):
    if any(node in path[1:] for node in solved_nodes):
        if del_invalid:
            return None
        else:
            return [path[0], None, path[-1]]
    else:
        return path


def prune_invalid_paths(paths, solved_nodes, del_invalid=False):
    out_paths = recursive_list_map(
        lambda path: prune_invalid_path(
            path, solved_nodes, del_invalid),
        paths)

    if del_invalid:
        out_paths = recursive_list_comprehension(
            lambda x: x, out_paths, lambda x: x is not None)

    return out_paths


def get_all_valid_paths(G, sources, targets, del_invalid=False):
    out_list = []
    for target_node in targets:
        temp = []
        for source_node in sources:
            if source_node == target_node:
                pruned_paths = [[source_node, target_node]]
            else:
                paths = list(nx.all_simple_paths(G, source_node, target_node))
                if paths == []:
                    paths = [[source_node, None, target_node]]
                pruned_paths = prune_invalid_paths(paths, sources, del_invalid)
            temp.append(pruned_paths)
        out_list.append(temp)

    # return prune_invalid_paths(out_list, sources, del_invalid)
    return out_list

###################################################################################


def vec2field(vec, op_context, out_nodes=None):
    N = op_context['N']
    field_type = op_context['type']
    vec_len = len(vec)
    if vec_len % N != 0:
        raise Exception(
            f"field size {N} doesn't evenly divide vector length {vec_len}")
    num_fields = vec_len//N
    out_fields = []
    if out_nodes is not None:
        if len(out_nodes) != num_fields:
            raise Exception(
                f"number of nodes {len(out_nodes)} has to match the number of fields {num_fields}")
    for i in range(num_fields):
        if out_nodes is not None:
            out_node = out_nodes[i]
        else:
            out_node = None
        vec_slice = vec[i*N:(i+1)*N]
        vec_slice = np.ravel(vec_slice)
        field = field_type(vec=vec_slice, node1=None, node2=out_node)
        out_fields.append(field)

    return out_fields
