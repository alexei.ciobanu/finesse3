'''
Want to make some changes to v2 but this will probably break a lot in the process.
I got a bit too carried away with subclassing. The base class should be
AbstractScalar not DiGraph. The biggest change being that ConcreteScalar will
now subclass AbstractScalar instead of DiGraph. I would also like AbstractOptical
to subclass AbstractScalar. AbstractOptical will then be subclassed by the various
ConcreteOptical type classes.

This leaves a small dilema for the Planewave class. It makes sense for it to be a
subclass of AbstractOptical, because it is a concrete implementation of an optical
operator. But it doesn't use ABCD matrices... so need to write AbstractOptical to
not do any matrix multplication in that case.

I also want to drop the special treatment of identity and null. It's annoying because
every operator class needs to know if it's being acted on by identity or null. I could
probably make them subclass DiGraph instead of AbstractScalar... that makes way more sense.
'''

from numbers import Number
import copy
import numpy as np

from finesse.element import Symbol, finesse2sympy, sympy2finesse, my_measure
from finesse.densematrix import DenseMatrix
import finesse.utilities.operators.func_funs as ff

class OperatorException(Exception):
    pass

class DiGraphOperator(object):
    '''
    * Base class for DiGraph operators.
    * Acting with a DiGraphOperator on any other operator should just
    change the node connections.
    '''

    def __init__(self, node1=None, node2=None):
        self.node1 = node1
        self.node2 = node2
        self._context = {'type': self.__class__}

    @property
    def context(self):
        return self._context

    @property
    def nodes(self):
        return(self.node1, self.node2)

    def concretise(self, context=None):
        if context is None:
            raise OperatorException("Can't concretise without a context")
        elif context['type'] in abstract_operators:
            raise OperatorException("Can't concretise with respect to an abstract context")
        else:
            return context['type'](node1=self.node1, node2=self.node2)

    def __matmul__(self, other, copy_target=None):
        self._assert_node_matmul(other)
        if copy_target is None:
            copy_target = other
        if copy_target is other:
            op = copy.copy(copy_target)  # avoiding potential side effects
            op.node2 = self.node2
        elif copy_target is self:
            op = copy.copy(copy_target)  # avoiding potential side effects
            op.node1 = other.node1
        else:
            raise OperatorException("undefined")
        return op

    def _assert_node_matmul(self, other):
        if self.node1 != other.node2:
            raise OperatorException(
                f"node {self.node1} doesn't match node {other.node2}")

    def _assert_nodes_add(self, other):
        if self.node1 != other.node1:
            raise OperatorException(f"initial node {self.node1} doesn't \
match initial node {other.node1}")
        if self.node2 != other.node2:
            raise OperatorException(
                f"final node {self.node2} doesn't match final node {other.node2}")

    def __add__(self, other):
        '''
        __add__ doesn't enforce types to allow the SumList to
        do type conversions at build time.
        '''
        if isinstance(other, DiGraphOperator):
            self._assert_nodes_add(other)
            if isinstance(other, NullOperator):
                return self
            else:
                op_list = [self, other]
                return OperatorSumList(op_list)
        elif isinstance(other, OperatorSumList):
            op_list = (self,) + other
            return OperatorSumList(iterable=op_list)
        else:
            return NotImplemented

    def __radd__(self, other):
        if isinstance(other, DiGraphOperator):
            self._assert_nodes_add(other)
            op_list = [other, self]
            return OperatorSumList(op_list)
        elif isinstance(other, OperatorSumList):
            op_list = other + (self,)
            return OperatorSumList(iterable=op_list)
        else:
            return NotImplemented


class IdentityOperator(DiGraphOperator):
    '''
    Mostly used for debugging.
    '''

    def __init__(self, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)

    def concretise(self, context=None):
        op = super().concretise(context=context)
        op._scalar = 1
        return op

    def __add__(self, other):
        if isinstance(other, Number):
            if other == 0:
                return self
        else:
            return super().__add__(other)

    def __radd__(self, other):
        if isinstance(other, Number):
            if other == 0:
                # don't promote to ScalarOperator
                return self
        else:
            return NotImplemented

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = other
        return super().__matmul__(other, copy_target=copy_target)

    def __rmatmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = other
        # kind of a hack
        return DiGraphOperator.__matmul__(other, self, copy_target=copy_target)

    def __mul__(self, other):
        if isinstance(other, Number):
            return AbstractScalarOperator(scalar=other, node1=self.node1, node2=self.node2)
        elif isinstance(other, DiGraphOperator):
            return self@other
        else:
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, Number):
            return self*other
        elif isinstance(other, DiGraphOperator):
            return other@self
        else:
            return NotImplemented

    def __repr__(self):
        return 'I'

    def __str__(self):
        return 'I'

    def build(self, context=None):
        # print(self, context)
        if context is None:
            raise OperatorException("can't do that")
        elif context['type'] in (AbstractScalarOperator, NullOperator, IdentityOperator):
            raise OperatorException("also can't do that")
        else:
            op_type = context['type']
            op_kwargs = {key: val for key,
                         val in context.items() if key != 'type'}
            op = op_type(scalar=1, node1=self.node1, node2=self.node2, **op_kwargs)
            return op.build()

class NullOperator(DiGraphOperator):
    '''
    * Very useful in bookkeeping.
    '''

    def __init__(self, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)

    def concretise(self, context=None):
        op = super().concretise(context=context)
        op._scalar = 0
        return op

    def __add__(self, other):
        '''
        Null is the additive identity
        '''
        if isinstance(other, (DiGraphOperator, OperatorSumList)):
            self._assert_nodes_add(other)
            return other
        if isinstance(other, Number):
            if other == 0:
                return self
        else:
            return super().__add__(other)

    def __radd__(self, other):
        '''
        In case we add Null to a number
        '''
        if isinstance(other, Number):
            if other == 0:
                # don't promote to ScalarOperator
                return self
        else:
            return NotImplemented

    def __mul__(self, other):
        if isinstance(other, Number):
            return self
        elif isinstance(other, DiGraphOperator):
            return self@other
        else:
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, Number):
            return self
        elif isinstance(other, DiGraphOperator):
            return other@self
        else:
            return NotImplemented

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = self
        return super().__matmul__(other, copy_target=copy_target)

    def __rmatmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = self
        # kind of a hack
        return DiGraphOperator.__matmul__(other, self, copy_target=copy_target)

    def __repr__(self):
        return 'Null'

    def __str__(self):
        return 'Null'

    def build(self, context=None):
        # print(self, context)
        if context is None:
            raise OperatorException("can't do that")
        elif context['type'] in (AbstractScalarOperator, NullOperator, IdentityOperator):
            raise OperatorException("also can't do that")
        else:
            op_type = context['type']
            op_kwargs = {key: val for key,
                         val in context.items() if key != 'type'}
            op = op_type(scalar=0, node1=self.node1, node2=self.node2, **op_kwargs)
            return op.build()


class AbstractScalarOperator(DiGraphOperator):
    '''
    * Abstract operators are any operators that cannot be built
    without a context.

    * Acting with them and adding them should still be defined.

    * Acting an abstract operator on a concrete operator should
    result in a concrete operator of the same type. Addition is
    lazy and resolved at build time.
    '''

    def __init__(self, scalar=None, node1=None, node2=None):
        super().__init__(node1=node1, node2=node2)
        self._scalar = scalar
        self._context = {'type': self.__class__}

    @property
    def context(self):
        return self._context

    @property
    def scalar(self):
        return self._scalar

    def keep_changing_symbols(self):
        if isinstance(self.scalar, Symbol):
            self._scalar = self.scalar.eval(keep_changing_symbols=True)

    def simplify_symbols(self, symbol_dict={}):
        if isinstance(self.scalar, Symbol):
            sym_expr = finesse2sympy(self.scalar)
            sym_expr = sym_expr.simplify(measure=my_measure)
            self._scalar = sympy2finesse(sym_expr, symbol_dict)

    def concretise(self, context=None):
        op = super().concretise(context=context)
        op._scalar = self.scalar
        return op

    def build(self, context=None):
        # print(context['type'])
        # print(issubclass(context['type'], AbstractScalarOperator))
        if context is None:
            raise OperatorException(
                f"Can't build {self.__class__} without a context.")
        elif context['type'] in [AbstractScalarOperator, NullOperator, IdentityOperator]:
            raise OperatorException(
                f"Can't build {self.__class__} with an abstract context")
        else:
            op_type = context['type']
            op_kwargs = {key: val for key,
                         val in context.items() if key != 'type'}
            op = op_type(node1=self.node1, node2=self.node2, **op_kwargs)
            if isinstance(self.scalar, Symbol):
                op = type(self.scalar.value)(self.scalar) * op
            elif isinstance(self.scalar, Number):
                op = self.scalar * op
            else:
                raise OperatorException("undefined")
            return op.build()

    def __matmul__(self, other, copy_target=None):
        if isinstance(other, AbstractScalarOperator):
            if copy_target is None:
                copy_target = other
            op = super().__matmul__(other, copy_target=copy_target)
            op._scalar = self.scalar * other.scalar
            return op
        elif isinstance(other, OperatorSumList):
            op_list = ff.recursive_map(lambda x: self@x, other)
            return OperatorSumList(op_list)
        else:
            # let the caller's __rmatmul__ do it
            return NotImplemented

    def __add__(self, other):
        if isinstance(other, Number):
            return self + other * AbstractScalarOperator(
                scalar=1,
                node1=self.node1,
                node2=self.node2)
        else:
            return super().__add__(other)

    def __radd__(self, other):
        if isinstance(other, Number):
            return other * AbstractScalarOperator(
                scalar=1,
                node1=self.node1,
                node2=self.node2) + self
        else:
            return super().__radd__(other)

    def __mul__(self, other):
        if isinstance(other, DiGraphOperator):
            return self @ other
        elif isinstance(other, (Number, Symbol)):
            if other == 0:
                op = NullOperator(node1=self.node1, node2=self.node2)
                op._context=self.context
                return op
            else:
                op = copy.copy(self)
                op._scalar *= other
                return op
        else:
            # let the caller's __rmul__ figure it out
            return NotImplemented

    def __rmul__(self, other):
        if isinstance(other, Number):
            return self*other
        else:
            return NotImplemented

    def __neg__(self):
        return (-1)*self

    def __repr__(self):
        try:
            if isinstance(self.scalar, Symbol):
                return f'({self.scalar})I'
            else:
                return f'({self.scalar:.4g})I'
        except TypeError:
            return 'failed __repr__'

    def __str__(self):
        return self.__repr__()

# TODO still rewriting this

abstract_operators = (AbstractScalarOperator, NullOperator, IdentityOperator)

class ConcreteScalarOperator(AbstractScalarOperator):
    '''
    * Concrete operators are any operators that can be built.

    * Concrete operators can only act on other concrete operators
    of the same context or abstract operators.

    * Addition between operators is lazy and resolved at build
    time.

    * This class is intended to be subclassed but can be instantiated for
    a scalar operator that just returns its a diagonal matrix with the
    scalar value when built.
    '''

    def __init__(self, scalar=1, N=1, node1=None, node2=None):
        super().__init__(scalar=scalar, node1=node1, node2=node2)
        self._N = N
        self._context['N'] = self._N

    @property
    def N(self):
        return self._N

    def __matmul__(self, other, copy_target=None):
        if copy_target is None:
            copy_target = self
        if isinstance(other, DiGraphOperator):
            return super().__matmul__(other, copy_target=copy_target)
        else:
            return NotImplemented

    def build(self, context=None):
        if context is None:
            context = self.context
        if context != self.context:
            raise OperatorException(f"Tried to build a concrete operator that has context \
{self.context} with incompatible context {context}")
        return self.scalar * np.eye(self._N, dtype=type(self._scalar))

    def __repr__(self):
        string = super().__repr__()
        return string+f'_{self.N},{self.N}_'

    def __str__(self):
        return self.__repr__()


class OperatorSumList(list):
    '''
    * Class for containing list pf lazily summed operators.

    * The summing is performed on build.

    * List should be immutable. Though the elements may be mutable.
    It is the users responsibility to rerun assertions if they start
    mutating list elements.

    * The list also contains the context (type) of the operators
    inside it. This is used to coerce any abstract operators
    in the list to their concrete counterpart.

    * The list should check types of operators added to it and
    keep track of the context. Once the context is concrete it
    should not allow other concrete operators in the list and adding
    an abstract operator should not change the context.
    '''

    ### uncomment if want to subclass tuple
    # def __new__(cls, iterable=(), context=None, node1=None, node2=None):
    #     # argument list of __new__ and __init__ need to match
    #     # otherwise python complain
    #     return super(OperatorSumList, cls).__new__(cls, iterable)

    def __init__(self, iterable=(), context=None, node1=None, node2=None):
        # asserting context and nodes on every instance is probably
        # overkill but I'm too lazy to write this more efficiently
        # and this will catch a lot of bugs
        # super().__init__(iterable)
        super().__init__(iterable)

        self.node1 = node1
        self.node2 = node2

        if context is None:
            self._context = self.assert_context()
        else:
            self.assert_context()

        if self.nodes == (None, None):
            self.node1, self.node2 = self.assert_nodes()
        else:
            self.assert_nodes()

    @property
    def nodes(self):
        return(self.node1, self.node2)

    @property
    def context(self):
        return self._context

    def keep_changing_symbols(self):
        for el in self:
            el.keep_changing_symbols()

    def simplify_symbols(self, symbol_dict={}):
        for el in self:
            el.simplify_symbols(symbol_dict=symbol_dict)

    def __add__(self, other):
        if isinstance(other, DiGraphOperator):
            return OperatorSumList(self + [other])
        elif isinstance(other, (tuple, list)):
            op_sumlist = OperatorSumList(iterable=list(self)+list(other))
            return op_sumlist
        elif isinstance(other, Number) and other == 0:
            return self
        elif isinstance(other, NullOperator):
            return self
        else:
            return NotImplemented

    def __radd__(self, other):
        if isinstance(other, Number) and other == 0:
            return self
        elif isinstance(other, NullOperator):
            return self
        else:
            return NotImplemented

    def __mul__(self, other):
        # multiplication just distributes across the list
        op_list = ff.recursive_map(lambda x: x*other, self)
        return OperatorSumList(op_list)

    def __rmul__(self, other):
        # multiplication just distributes across the list
        op_list = ff.recursive_map(lambda x: other*x, self)
        return OperatorSumList(op_list)

    def concretise(self, context=None):
        if self.context is None:
            raise OperatorException("Can't concretise without a context")
        elif self.context['type'] in abstract_operators:
            raise OperatorException("Can't concretise with respect to an abstract context")
        else:
            for i, el in enumerate(self):
                if el.context == self.context:
                    # element could be list with a concrete operator or a concrete operator
                    if isinstance(el, OperatorSumList):
                        el.concretise()
                    elif isinstance(el, DiGraphOperator):
                        pass
                    else:
                        raise OperatorException("undefined")
                else:
                    # try to concretise element
                    op = el.concretise(context=self.context)
                    self[i] = op

    def assert_nodes(self):
        '''
        Checks for consistency of nodes in the SumList.
        Also returns the tuple of (node1, node2) if passes.
        '''
        if not self:
            # bool(iterable) returns False if empty
            # "the most pythonic"TM way of checking if empty
            nodes = (None, None)
            return nodes

        if self.nodes != (None, None):
            # compare against header
            current_nodes = self.nodes
            nodes_loc = 'header'
        else:
            # compare against first operator
            current_nodes = self[0].nodes
            nodes_loc = 'i=0'
        for i, element in enumerate(self[1:]):
            if current_nodes != element.nodes:
                raise OperatorException(f"Inconsistent nodes in {self} between \
{nodes_loc} {current_nodes} and i={i+1} {element.nodes}")
        return current_nodes

    def assert_context(self):
        '''
        Checks for consistency of contexts in the SumList.
        Also returns the defining context if passes.
        '''
        if not self:
            # bool(iterable) returns False if empty
            # "the most pythonic"TM way of checking if empty
            return {}

        current_context = self[0].context
        for element in self[1:]:
            if current_context['type'] in (AbstractScalarOperator, NullOperator, IdentityOperator):
                # current context is Abstract, just overwrite it
                # with the next one
                current_context = element.context
            else:
                # current context is Concrete, need to do comparison
                if current_context != element.context:
                    if element.context['type'] in (AbstractScalarOperator, NullOperator, IdentityOperator):
                        # context is already concrete ignore further abstract contexts
                        pass
                    else:
                        raise OperatorException(f"Incompatible contexts in SumList. context1 = \
{current_context}, context2 = {element.context}")
        return current_context

    def build(self, context=None):
        if not self:
            raise OperatorException(
                "building empty lists is undefined (no operators to build)")
        if context is None:
            context = self.context
        out = self[0].build(context=context)
        for op in self[1:]:
            out += op.build(context=context)
        return out

    def __repr__(self):
        return 'OpSumList'+super().__repr__()


class OperatorMatrix(np.ndarray):
    '''
    note: currently OperatorMatrix.__mul__(x) just distributes the product with
    x across the matrix elements (inherited from np.ndarray) which is what I
    would expect for Numbers but feels a bit strange for DiGraphOperators.
    I would expect __mul__ to call __matmul__ but that doesn't make sense because an
    OperatorMatrix is not a subset of DiGraphOperator.
    The node signatures between Matrix and Operator don't compose in general.
    Only in the case where the Matrix node1_arr and node2_arr are constant can
    a __matmul__ make sense.
    However that case doesn't come up in my usecases.

    So I guess I will leave it as it is for now and code in that __matmul__ when
    I need it.
    '''
    def __new__(cls, obj=((),), context=None, node1_arr=None, node2_arr=None):
        arr = np.array(obj, dtype=object)
        op_arr = arr.view(OperatorMatrix)
        return op_arr

    def __init__(self, obj=((),), context=None, node1_arr=None, node2_arr=None):
        self._context = context
        self.node1_arr = node1_arr
        self.node2_arr = node2_arr

        if context is None:
            self._context = self.assert_context()
        else:
            self.assert_context()

        # if node1_arr is None:
        #     self.node1_arr = self.assert_node1_arr()
        # else:
        #     self.assert_node1_arr()
        # if node2_arr is None:
        #     self.node2_arr = self.assert_node2_arr()

    @property
    def context(self):
        return self._context

    @property
    def nodes_arr(self):
        return np.dstack([self.node1_arr, self.node2_arr])

    def Number2OpMat(self, num):
        if isinstance(num, Number):
            num_mat = np.zeros(np.shape(self), dtype=object)
            for j, row in enumerate(num_mat):
                for i, el in enumerate(row):
                    if i == j:
                        num_mat[i, i] = AbstractScalarOperator(scalar=num,
                                                               node1=self[i, j].node1, node2=self[i, j].node2)
                    else:
                        num_mat[i, j] = NullOperator(
                            node1=self[i, j].node1, node2=self[i, j].node2)
            return OperatorMatrix(num_mat)
        else:
            return OperatorException(f"object num {num} needs to be of type Number not {type(num)}")

    def __matmul__(self, other):
        out = super().__matmul__(other)
        out.__init__()
        return out

    def __mul__(self, other):
        out = super().__mul__(other)
        out.__init__()
        return out

    def __rmul__(self, other):
        out = super().__rmul__(other)
        out.__init__()
        return out

    def __add__(self, other):
        if isinstance(other, Number):
            return self + self.Number2OpMat(other)
        elif isinstance(other, OperatorMatrix):
            out = super().__add__(other)
            out.__init__()
            return out
        else:
            return NotImplemented

    def __radd__(self, other):
        if isinstance(other, Number):
            return self.Number2OpMat(other) + self
        elif isinstance(other, OperatorMatrix):
            return super().__add__(other)
        else:
            return NotImplemented

    def __neg__(self):
        return (-1)*self

    def __sub__(self, other):
        return self + -other

    def __rsub__(self, other):
        return other + -self

    def keep_changing_symbols(self):
        for row in self:
            for el in row:
                el.keep_changing_symbols()

    def simplify_symbols(self, symbol_dict={}):
        for row in self:
            for el in row:
                el.simplify_symbols(symbol_dict=symbol_dict)

    def concretise(self):
        if self.context is None:
            raise OperatorException("Can't concretise without a context")
        elif self.context['type'] in abstract_operators:
            raise OperatorException("Can't concretise with respect to an abstract context")
        else:
            for j, row in enumerate(self):
                for i, el in enumerate(row):
                    if el.context == self.context:
                        # element could be list with a concrete operator or a concrete operator
                        if isinstance(el, OperatorSumList):
                            el.concretise()
                        elif isinstance(el, DiGraphOperator):
                            pass
                        else:
                            raise OperatorException(f"undefined for {type(el)}")
                    else:
                        # try to concretise element
                        op = el.concretise(context=self.context)
                        self[j, i] = op

    def assert_context(self, context=None):
        if self.size == 0:
            return {}

        if context is None:           
            current_context = self[0,0].context
        else:
            current_context = context

        for i,row in enumerate(self):
            for j,el in enumerate(row):
                if current_context['type'] in abstract_operators:
                    # current context is Abstract, just overwrite it
                    # with the next one
                    current_context = el.context
                else:
                    # current context is Concrete, need to do comparison
                    if current_context != el.context:
                        if el.context['type'] in abstract_operators:
                            # context is already concrete ignore further abstract contexts
                            pass
                        else:
                            raise OperatorException(f"Incompatible contexts. context1 = \
    {current_context}, context2 = {el.context}")
        return current_context

    def assert_node1_arr(self):
        if self.node1_arr is None:
            new_node1_arr = np.zeros(np.shape(self), dtype=object)
            node1s = [x.node1 for x in self[:, 0]]
        else:
            node1s = [x for x in self.node1_arr[:, 0]]

        # print(node1s)
        for j, row in enumerate(self):
            for i, el in enumerate(row):
                new_node1_arr[j, i] = el.node1
                # print((el.node1, el.node2), node1s[i])
                if el.node1 != node1s[j]:
                    raise OperatorException(f"node1 {el.node1} in OperatorMatrix \
at {i, j} is inconsistent with {node1s[j]} at {i, 0}")
        return new_node1_arr

    def assert_node2_arr(self):
        if self.node2_arr is None:
            new_node2_arr = np.zeros(np.shape(self), dtype=object)
            node2s = [x.node2 for x in self[0, :]]
        else:
            node2s = [x for x in self.node2_arr[0, :]]

        for j, row in enumerate(self):
            for i, el in enumerate(row):
                new_node2_arr[j, i] = el.node2
                if el.node2 != node2s[i]:
                    raise OperatorException(f"node2 {el.node2} in OperatorMatrix {self} \
at {i, j} is inconsistent with {node2s[i]} at {0, j}")
        return new_node2_arr

    def build_hard(self, context=None):
        '''
        Slow build with no memory preallocation trickery.
        But that also means no memory related bugs. I left this
        in because the preallocated build was rumored to have strange
        memory related bugs.

        This is also the only way to build non square matrices as DenseMatrix
        doesn't support this.
        '''
        if context is None:
            context = self.context
        block_list = [[x.build(context=context) for x in row] for row in self.tolist()]
        # print('\nbals \n', block_list, '\nbals\n')
        built_arr = np.block(block_list)
        return built_arr

    def allocate_dense_build_memory(self, context=None):
        if context is None:
            context = self.context
        arr = DenseMatrix("name")
        N = context['N']
        build_shape = np.shape(self)
        for i in range(build_shape[0]):
            arr.add_block(N, i, "name")
        return arr

    def build_into_dense(self, dense_arr, context=None):
        '''
        dense_arr should be of type DenseMatrix of the appropriate size

        for the current implementation of memory preallocated matrices
        the procedure is as follows

        0. Allocate the output matrix (appropriate size)
        1. Get all the submatrix views
        2. Call DM.construct()
        3. Populate the views with the elements
        '''

        views_mat = np.zeros(self.shape, dtype=object)
        for j, row in enumerate(self):
            for i, el in enumerate(row):
                views_mat[i, j] = dense_arr.get_sub_matrix_view(i, j, "name")
        dense_arr.construct()
        for j, row in enumerate(self):
            for i, el in enumerate(row):
                views_mat[i, j][:] = el.build(context=context)

        return dense_arr

    def allocate_numpy_build_memory(self, context=None):
        if context is None:
            context = self.context
        N = context['N']
        op_mat_shape = np.array(np.shape(self))
        arr = np.zeros(op_mat_shape*N, dtype=np.complex128)
        return arr

    def build_into_numpy_old(self, numpy_arr, context=None):
        '''
        keeping this because this one is more shape independent
        than the new one
        '''
        if context is None:
            context = self.context
        self_list = self.tolist()
        block_list = []
        for row in self_list:
            temp = []
            for el in row:
                temp.append(el.build(context=context))
            block_list.append(temp)
        # block_list = [[x.build(context=context) for x in row] for row in self.tolist()]
        numpy_arr[:] = np.block(block_list)
        return numpy_arr

    def build_into_numpy(self, numpy_arr, context=None):
        if context is None:
            context = self.context
        N = context['N']
        for j,row in enumerate(self):
            for i,el in enumerate(row):
                numpy_arr[j*N:(j+1)*N,i*N:(i+1)*N] = el.build()
        return numpy_arr

    def build_dense(self, context=None):
        '''
        The "soft" build that allocates memory for the build matrix.
        Returns a dense matrix instance arr that can be updated with
        self.build_into(arr).
        This method should only be called once in a simulation to
        get the build matrix. All subsequent calls should be build_into.
        '''
        arr = self.allocate_desnse_build_memory(context=context)

        return self.build_into_dense(arr, context=context)

    def build_numpy(self, context=None):
        arr = self.allocate_numpy_build_memory(context=context)
        return self.build_into_numpy(arr, context=context)

    def build(self, context=None):
        return self.build_numpy(context=context)