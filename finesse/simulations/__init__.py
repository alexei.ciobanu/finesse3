# -*- coding: utf-8 -*-
"""
Holds the various instances of simulation classes.

Listed below are all the sub-modules of the ``simulations`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: simulations/
    {contents}
"""

from finesse.simulations.base import BaseSimulation
from finesse.simulations.KLU import KLUSimulation
from finesse.simulations.digraph import DigraphSimulation, DigraphSimulationBase
from finesse.simulations.debug import DebugSimulation
from finesse.simulations.dense import DenseSimulation
from finesse.simulations.operator import OperatorSimulation

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    __doc__ = __doc__.format(
        contents=_collect_submodules("simulations")
    )
