# pylint: disable=no-member

"""Model path tracing tests."""

from unittest import TestCase
from networkx.exception import NetworkXNoPath

from finesse import Model
from finesse.components import Beamsplitter, Laser, Mirror

class PathTracingTestCase(TestCase):
    """Path finding tests."""
    def setUp(self):
        self.reset()

    def reset(self):
        """Reset model."""
        self.model = Model()

    def _construct_fabryperot(self):
        """Convenience method for constructing a Fabry-Perot cavity."""
        self.model.add([Laser("L0"), Mirror("ITM"), Mirror("ETM")])
        self.model.connect(self.model.L0.p1, self.model.ITM.p1)
        self.model.connect(self.model.ITM.p2, self.model.ETM.p1, L=1)

    def _construct_simple_michelson(self):
        """Convenience method for constructing a simple Michelson interferometer."""
        self.model.add([Laser("L0"), Beamsplitter("BS"), Mirror("MX"), Mirror("MY")])
        self.model.connect(self.model.L0.p1, self.model.BS.p1)
        self.model.connect(self.model.BS.p2, self.model.MY.p1, L=1)
        self.model.connect(self.model.BS.p3, self.model.MX.p1, L=1)

    def test_fullpath_laser_to_mirror(self):
        """Test path finding from a single laser to a single mirror."""
        self.model.add([Laser("L0"), Mirror("M")])
        self.model.connect(self.model.L0.p1, self.model.M.p1)

        # transmitted through mirror
        result_transmit = self.model.path(self.model.L0.p1.o, self.model.M.p2.o).data
        target_transmit = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.M.p1.i, self.model.M),
            (self.model.M.p2.o, None)
        ]
        self.assertListEqual(result_transmit, target_transmit)

        # reflected from mirror
        result_reflect = self.model.path(self.model.L0.p1.o, self.model.M.p1.o).data
        target_reflect = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.M.p1.i, self.model.M),
            (self.model.M.p1.o, self.model.L0.p1.o.space)
        ]
        self.assertListEqual(result_reflect, target_reflect)

        # impossible path
        self.assertRaises(
            NetworkXNoPath,
            callable=self.model.path,
            args=(self.model.L0.p1.o, self.model.M.p2.i)
        )

    def test_fullpath_fabry_perot(self):
        """Test path finding from a single laser to a Fabry-Perot cavity."""
        self._construct_fabryperot()

        # transmitted through Fabry-Perot
        result_transmit_all = self.model.path(self.model.L0.p1.o, self.model.ETM.p2.o).data
        target_transmit_all = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.ITM.p1.i, self.model.ITM),
            (self.model.ITM.p2.o, self.model.ITM.p2.o.space),
            (self.model.ETM.p1.i, self.model.ETM),
            (self.model.ETM.p2.o, None)
        ]
        self.assertListEqual(result_transmit_all, target_transmit_all)

        # reflected back to ITM from ETM
        result_half_roundtrip = self.model.path(self.model.L0.p1.o, self.model.ITM.p2.i).data
        target_half_roundtrip = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.ITM.p1.i, self.model.ITM),
            (self.model.ITM.p2.o, self.model.ITM.p2.o.space),
            (self.model.ETM.p1.i, self.model.ETM),
            (self.model.ETM.p1.o, self.model.ETM.p1.o.space),
            (self.model.ITM.p2.i, self.model.ITM)
        ]
        self.assertListEqual(result_half_roundtrip, target_half_roundtrip)

    def test_fullpath_fabry_perot_with_via_nodes(self):
        """Test path finding from a single laser to a Fabry-Perot cavity via specified nodes."""
        self._construct_fabryperot()

        # full roundtrip
        result_roundtrip = self.model.path(
            self.model.L0.p1.o, self.model.ETM.p2.o,
            via_node=self.model.ITM.p2.i
        ).data
        target_roundtrip = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.ITM.p1.i, self.model.ITM),
            (self.model.ITM.p2.o, self.model.ITM.p2.o.space),
            (self.model.ETM.p1.i, self.model.ETM),
            (self.model.ETM.p1.o, self.model.ETM.p1.o.space),
            (self.model.ITM.p2.i, self.model.ITM),
            (self.model.ITM.p2.o, self.model.ITM.p2.o.space),
            (self.model.ETM.p1.i, self.model.ETM),
            (self.model.ETM.p2.o, None)
        ]
        self.assertListEqual(result_roundtrip, target_roundtrip)

        # reflection from ITM but via ETM
        result_ITM_refl = self.model.path(
            self.model.L0.p1.o, self.model.ITM.p1.o,
            via_node=self.model.ETM.p1.i
        ).data
        target_ITM_refl = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.ITM.p1.i, self.model.ITM),
            (self.model.ITM.p2.o, self.model.ITM.p2.o.space),
            (self.model.ETM.p1.i, self.model.ETM),
            (self.model.ETM.p1.o, self.model.ETM.p1.o.space),
            (self.model.ITM.p2.i, self.model.ITM),
            (self.model.ITM.p1.o, self.model.ITM.p1.o.space)
        ]
        self.assertListEqual(result_ITM_refl, target_ITM_refl)

        # impossible path because of via node
        self.assertRaises(
            NetworkXNoPath,
            callable=self.model.path,
            args=(self.model.L0.p1.o, self.model.ITM.p1.o, self.model.ETM.p2.i)
        )

    def test_fullpath_simple_michelson(self):
        """Test path finding for a simple Michelson interferometer (no arm cavities)."""
        self._construct_simple_michelson()

        # trace to end of XARM
        result_XARM = self.model.path(self.model.L0.p1.o, self.model.MX.p2.o).data
        target_XARM = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.BS.p1.i, self.model.BS),
            (self.model.BS.p3.o, self.model.BS.p3.o.space),
            (self.model.MX.p1.i, self.model.MX),
            (self.model.MX.p2.o, None)
        ]
        self.assertListEqual(result_XARM, target_XARM)

        # trace to end of YARM
        result_YARM = self.model.path(self.model.L0.p1.o, self.model.MY.p2.o).data
        target_YARM = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.BS.p1.i, self.model.BS),
            (self.model.BS.p2.o, self.model.BS.p2.o.space),
            (self.model.MY.p1.i, self.model.MY),
            (self.model.MY.p2.o, None)
        ]
        self.assertListEqual(result_YARM, target_YARM)

        # impossible path through beam splitter
        self.assertRaises(
            NetworkXNoPath,
            callable=self.model.path,
            args=(self.model.MX.p1.o, self.model.MY.p1.i)
        )

    def test_fullpath_simple_michelson_with_via_nodes(self):
        """Test path finding for a simple Michelson interferometer using specified nodes."""
        self._construct_simple_michelson()

        # trace to reflection port of IFO via YARM
        result_IFO_refl = self.model.path(
            self.model.L0.p1.o, self.model.BS.p1.o,
            via_node=self.model.MY.p1.o
        ).data
        target_IFO_refl = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.BS.p1.i, self.model.BS),
            (self.model.BS.p2.o, self.model.BS.p2.o.space),
            (self.model.MY.p1.i, self.model.MY),
            (self.model.MY.p1.o, self.model.MY.p1.o.space),
            (self.model.BS.p2.i, self.model.BS),
            (self.model.BS.p1.o, self.model.BS.p1.o.space)
        ]
        self.assertListEqual(result_IFO_refl, target_IFO_refl)

        # trace to AS port of IFO via XARM
        result_AS = self.model.path(
            self.model.L0.p1.o, self.model.BS.p4.o,
            via_node=self.model.MX.p1.i
        ).data
        target_AS = [
            (self.model.L0.p1.o, self.model.L0.p1.o.space),
            (self.model.BS.p1.i, self.model.BS),
            (self.model.BS.p3.o, self.model.BS.p3.o.space),
            (self.model.MX.p1.i, self.model.MX),
            (self.model.MX.p1.o, self.model.MX.p1.o.space),
            (self.model.BS.p3.i, self.model.BS),
            (self.model.BS.p4.o, None)
        ]
        self.assertListEqual(result_AS, target_AS)

if __name__ == '__main__':
    import unittest
    unittest.main()

