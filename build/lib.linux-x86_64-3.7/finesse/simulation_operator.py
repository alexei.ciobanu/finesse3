import finesse.fun_lib.graph_funs as gf
import networkx as nx
import numpy as np
import finesse

class OperatorSimulation:
    '''
    notes:
    . only xaxis and x2axis supported
    . assumes yaxis re:im
    '''
    def __init__(self,model,op_type='planewave'):
        self.op_type = op_type
        self.model = model
        self.graph = gf.get_optical_network(self.model)
        self.laser_nodes = [x.p1.o.full_name for x in self.model.components if type(x) is finesse.components.laser.Laser]

        # reverse graph is used to obtain detector outputs by propagating
        # backwards through the network to the solved fields
        self.rev_graph = self.graph.reverse()
        self.solved = False

        self.align_nodes = gf.only_unique_nodes([cav.path.nodes_only[0] for cav in self.model.cavities])
        self.align_nodes = [node.full_name for node in self.align_nodes]

        # bookeeping for debugging
        self.all_cycles = list(nx.simple_cycles(self.graph))
        self.p_cycles = gf.alexei_cycles_prune(self.all_cycles)
        self.a_cycles = gf.alexei_cycles_align(self.p_cycles,self.align_nodes)
        self.g_cycles = gf.alexei_cycle_group(self.a_cycles)

    def build(self):
        '''
        other simulation classes name something like this as _initialise()
        '''
        # n_couple is the most important
        self.n_couple = gf.graph2n_couple(self.graph,align_nodes=self.align_nodes)
        self.n_couple_diag = gf.get_n_couple_diag(self.n_couple)
        self.N_cav = len(self.n_couple)

        self.op_couple = gf.get_reduced_path_operators(self.graph,self.n_couple,op_type=self.op_type)
        self.op_size = self.op_couple[0][0][0].op_size

        # allocate memory for run()
        self.sys_mat = np.zeros([self.N_cav*self.op_size,self.N_cav*self.op_size],dtype=np.complex128)
        self.rhs_vec = np.zeros(self.N_cav*self.op_size,dtype=np.complex128)
        self.sol_vec = np.zeros(self.N_cav*self.op_size,dtype=np.complex128)

        self.all_rhs_paths = gf.get_all_rhs_paths(self.graph,self.n_couple,self.laser_nodes)
        self.rhs_paths = gf.list_groupby(lambda x: x[-1], gf.alexei_paths_prune(gf.list_denest(self.all_rhs_paths)))

        # these should be automatically updated when model changes
        self.rhs_op = gf.get_rhs_vec(self.model,self.n_couple,op_type=self.op_type)
        self.sys_op = gf.get_reduced_path_operators(self.graph,self.n_couple,op_type=self.op_type)

    def solve(self):
        '''
        Solves all the circulating fields in an OperatorSimulation which corresponds 
        to the only part of the simulation that requires a matrix inverse.

        All other fields that will be requested have to be propagated.
        '''
        #raise NotImplementedError

        for i in range(self.N_cav):
            self.rhs_vec[i*self.op_size:(i+1)*self.op_size] = self.rhs_op[i].build()

        for i in range(self.N_cav):
            for j in range(self.N_cav):
                self.sys_mat[i*self.op_size:(i+1)*self.op_size,j*self.op_size:(j+1)*self.op_size] = self.sys_op[i][j][0].build()

        #####
        self.sol_vec = np.linalg.solve(np.eye(self.op_size*self.N_cav) - self.sys_mat,self.rhs_vec)
        #####

        self.solved_nodes = []
        for i in range(self.N_cav):
            self.solved_nodes.append(self.n_couple[i][i][0][0])

        self.solved_nodes.extend(self.laser_nodes)

        self.solved = True

    def get_node_path(self,node):
        rev_paths = list(nx.all_simple_paths(self.rev_graph,node,self.solved_nodes))
        return gf.recursive_list_map(lambda x: x[::-1], rev_paths)

    def get_node_field(self,node):
        '''
        other simulation classes name something like this as get_DC_out
        '''
        return list(nx.all_simple_paths(self.rev_graph,node,self.solved_nodes))

    def run(self):
        '''
        Dan said the run method should 'solve' the interferometer matrix for a particular
        set of model parameters. 
        '''
        raise NotImplementedError
        if self.solved is False:
            self.solve()