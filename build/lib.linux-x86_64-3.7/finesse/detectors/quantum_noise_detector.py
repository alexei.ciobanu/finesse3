"""
Photodiode quantum noise detector.
"""

import numpy as np
import scipy.constants as constants
from finesse.detectors.general import Detector


class QuantumNoiseDetector(Detector):
    """
    A class representing a quantum noise detector, that calculates the
    amplitude spectral density of the photocurrent noise of a DC or
    demodulated photodiode output.
    """

    def __init__(self, name, node, freqs=[], phases=[], shot_only=False):
        """
        Constructs a new `QuantumNoiseDetector` instance with the
        specified properties.

        Parameters
        ----------
        name : str
            Name of newly created :class:`.QuantumNoiseDetector`
            instance.

        node : :class:`.Node`
            Node to read output from.

        freqs : list of float or :class:`.Frequency`, optional
            List of mixer demodulation frequencies (in Hz).

        phases : list of float, optional
            List of mixer demodulation phases (in Hz).

        shot_only : bool, optional
            If True, detect only vacuum noise contributions.
        """
        Detector.__init__(self, name, node, dtype=np.float64, unit="W")

        if len(freqs) == 0:
            self.__mode = "dc"
        elif len(phases) == len(freqs):
            self.__mode = "mixer"
        else:
            raise ValueError("'phases' must be as long as 'freqs'.")
        # TODO: Make these private / have getter & setter?
        for i, f in enumerate(freqs):
            if isinstance(f, Frequency):
                pass
            elif isinstance(f, SourceFrequency):
                f = Frequency.from_source(f)
            else:
                f = SourceFrequency(self.name + "_carrier", f)
                f = Frequency.from_source(f)
            freqs[i] = f
        self.freqs = np.array(freqs)
        self.phases = np.array(phases)
        self.shot_only = shot_only

    def get_output(self, sim):
        """Computes the output of the quantum noise detector.

        Parameters
        ----------
        sim : :class:`.Simulation`
            The simulation object to probe.

        carrier : :class:`.Simulation`, optional
            The associated carrier object, if this is an audio
            simulation.

        Returns
        -------
        np.complex128
            The output of this `QuantumNoiseDetector`.
        """
        if not sim.is_audio:
            return 0
            
        carrier = sim.DC

        self.__fill_demod_f_table(sim, carrier)
        self.__fill_carrier_qnoise_contributions(sim, carrier)

        rtn = 0
        if not self.shot_only:
            # Fill the selection vector s
            # This contains non-zero elements only at the indices where we will
            # be measuring quantum noise from (i.e. at this detector's node)
            self.__fill_qnoised_selection_vector(sim, carrier)

            # Compute the weighting vector w = M^-1^T s
            # This describes how quantum noise at each node in the system
            # couples to the detector
            sim.M().set_rhs(slice(None), self.s)
            w = -1 * sim.M().solve(transpose=True, conjugate=True)

            # Apply the quantum noise input matrix to the weighting vector
            v = sim.Mq * w

            # And finally solve the matrix with the now correct quantum noise
            # inputs
            sim.M().set_rhs(slice(None), v)
            v = -1 * sim.M().solve()

            rtn = np.sum(v * np.conj(self.s)).real

        # Now we must loop over the contributions from the demodulation for
        # frequencies that are not signal sidebands, i.e. we pick up pure
        # vacuum noise and demodulate that into our signal.
        for el in self.demod_vac_contri.values():
            if len(el["c_idx"]) > 1:
                for j in range(max(len(sim.homs), 1)):
                    val = 0
                    for k in el["c_idx"]:
                        val += (
                            carrier.out[carrier.field(self._node, k, j)]
                        ).conjugate() * np.exp(
                            1j * np.deg2rad(self.demod_phi[el["phi_idx"][j]])
                        )
                    if not self.shot_only:
                        # TODO: why is this factor needed?
                        val *= np.sqrt(2)
                    rtn += abs(val) * (1 + el["f"] / sim.model.f0)**2
            else:
                val = 0
                for j in range(max(len(sim.homs), 1)):
                    #val += abs(carrier.out[carrier.field(self._node, el["c_idx"][0], j)])**2
                    val += abs(carrier.get_DC_out(self._node, el["c_idx"][0], j))**2
                rtn += val * (1 + el["f"] / sim.model.f0)

        for f in self.freqs:
            # Compensate for demod 0.5 factor if demodulating a signal
            # frequency as this is what a network analyser would do.
            if np.isclose(f.f, sim.model.fsig.f):
                rtn *= 2

        return (
            sim.model._UNIT_VACUUM
            * rtn
            * constants.h
            * sim.model.f0
            * 0.25 ** len(self.freqs)
        ) ** 0.5

    def __fill_demod_f_table(self, sim, carrier):
        Nf = len(self.freqs)
        Ndm = 2 ** Nf
        self.demod_f = np.zeros((len(carrier.frequencies), Ndm))
        self.demod_phi = np.zeros(Ndm)

        for f in carrier.frequencies:
            self.demod_f[f.index][:] = f.f

        for j in range(Ndm):
            for k in range(Nf):
                # Use bit shifting here to determine whether we add a plus
                # or minus to the total frequency and phases
                if (j >> k) & 0x01:
                    self.demod_phi[j] += self.phases[k]
                    for f in carrier.frequencies:
                        self.demod_f[f.index][j] += self.freqs[k].f
                else:
                    self.demod_phi[j] -= self.phases[k]
                    for f in carrier.frequencies:
                        self.demod_f[f.index][j] -= self.freqs[k].f

    def __fill_carrier_qnoise_contributions(self, sim, carrier):
        Nf = len(self.freqs)
        Ndm = 2 ** Nf
        self.demod_f_sig = np.zeros((len(carrier.frequencies), Ndm), dtype=int)
        self.demod_vac_contri = {}

        # Check all frequencies to see if they match up to any signal
        # sidebands
        for car in carrier.frequencies:
            i = car.index
            for j in range(Ndm):
                self.demod_f_sig[i][j] = -1
                # Check frequencies against any existing signals, if we're not
                # only considering pure vacuum states
                if not self.shot_only:
                    for k, f in enumerate(sim.frequencies):
                        if self.demod_f[i][j] == f.f:
                            self.demod_f_sig[i][j] = k
                            break

                # If no signal frequency could be found then this is a pure
                # vacuum noise field so need to add it to the contribution list
                if self.demod_f_sig[i][j] == -1:
                    # Check if there are any other contributions listed already
                    # for this carrier
                    # TODO: remove direct comparison of floats
                    if self.demod_f[i][j] in self.demod_vac_contri:
                        # If we have already found another item with this
                        # carrier frequency then these noises are correlated
                        elt = self.demod_vac_contri[self.demod_f[i][j]]
                        elt["c_idx"].append(i)
                        elt["phi_idx"].append(j)
                    else:
                        elt = {
                            "c_idx": [i],
                            "phi_idx": [j],
                            "f": self.demod_f[i][j],
                        }
                        self.demod_vac_contri[self.demod_f[i][j]] = elt

    def __fill_qnoised_selection_vector(self, sim, carrier):
        # For each carrier field check if the corresponding demodulated
        # frequency is a signal sideband. If so, we need to include it in the
        # selection vector.
        self.s = np.zeros(len(sim.out), dtype=np.complex128)
        for j, fc in enumerate(carrier.frequencies):
            for k in range(2 ** len(self.freqs)):
                if self.demod_f_sig[j][k] >= 0:
                    # If this is a signal sideband frequency this is the
                    # product between the j'th carrier and this.
                    fs = sim.frequencies[self.demod_f_sig[j][k]]
                    if fs.order > 0:
                        for i in range(max(len(sim.homs), 1)):
                            c_idx = carrier.field(self._node, fc.index, i)
                            s_idx = sim.field(self._node, fs.index, i)
                            self.s[s_idx] += (
                                carrier.out[c_idx]
                                * np.sqrt(2)
                                * np.exp(-1j * np.deg2rad(self.demod_phi[k]))
                            )
                    else:
                        for i in range(max(len(sim.homs), 1)):
                            c_idx = carrier.field(self._node, fc.index, i)
                            s_idx = sim.field(self._node, fs.index, i)
                            self.s[s_idx] += np.conj(
                                carrier.out[c_idx]
                                * np.sqrt(2)
                                * np.exp(-1j * np.deg2rad(self.demod_phi[k]))
                            )
