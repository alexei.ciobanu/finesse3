"""
Single-frequency amplitude and phase detector.
"""

import logging

import numpy as np

from finesse.detectors.general import Detector
from finesse.element import model_parameter, Rebuild

LOGGER = logging.getLogger(__name__)


@model_parameter("f", None, Rebuild.Frequencies, units="Hz")
class AmplitudeDetector(Detector):
    """
    A class representing an amplitude detector which calculates the amplitude
    and phase of light fields at one frequency.
    """
    def __init__(self, name, node, f, n=None, m=None):
        """
        Constructs a new `AmplitudeDetector` instance with the
        specified properties.

        Parameters
        ----------
        name : str
            Name of newly created :class:`.AmplitudeDetector` instance.

        node : :class:`.Node`
            Node to read output from.

        f : float
            Frequency of light to detect (in Hz).

        n : int, optional
            TEM mode number in the x-direction. Defaults to 0.

        m : int, optional
            TEM mode number in the y-direction. Defaults to 0.
        """
        Detector.__init__(self, name, node, dtype=np.complex128, label="Amplitude")

        self.f = f
        self.n = n
        self.m = m

    def get_output(self, DC, AC):
        """Computes the output of the amplitude detector.

        Parameters
        ----------
        sim : :class:`finesse.simulation.Simulation`

        Returns
        -------
        np.complex128
            The output of this `AmplitudeDetector`.
        """
        fv  = float(self.f)
        freq = DC.frequency_map.get(fv, None)
        sim = DC

        if freq is None and AC is not None:
            freq = AC.frequency_map.get(fv, None)
            sim = AC

        if freq is None:
            LOGGER.warning(f"AmplitudeDetector {self.name} is looking for a frequency {self.f}Hz"
                            " that is not being modelled, returning None")
            return None

        if sim.is_modal:
            res = complex(0)
            if self.n is None and self.m is None:
                for i, _ in enumerate(sim.homs):
                    res += sim.get_DC_out(self._node, freq.index, i)

            elif self.n is None and self.m is not None:
                for i, (_, m) in enumerate(sim.homs):
                    if m == self.m:
                        res += sim.get_DC_out(self._node, freq.index, i)

            elif self.n is not None and self.m is None:
                for i, (n, _) in enumerate(sim.homs):
                    if n == self.n:
                        res += sim.get_DC_out(self._node, freq.index, i)
            else:
                res = sim.get_DC_out(self._node, freq.index, sim.model.mode_index_map[(self.n, self.m)])
        else:
            res = sim.get_DC_out(self._node, freq.index, 0)

        if sim.is_audio and freq.audio_order < 0: res = res.conj()

        return np.sqrt(self._model._EPSILON0_C / 2) * res
