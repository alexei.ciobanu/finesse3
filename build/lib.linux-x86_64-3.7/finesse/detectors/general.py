"""
Top-level objects which specific detectors should inherit from.
"""

from abc import ABC, abstractmethod

import numpy as np
from scipy.special import jn

from finesse import ModelElement

class Detector(ABC, ModelElement):
    """
    Components that produce a numerical output should inherit this class. The
    simulation will then generate a dictionary of output values.
    """
    def __init__(self, name, node=None, dtype=np.complex128, unit='arb.', label=None):
        """
        Constructs a new `Detector` instance (only called by sub-class
        constructors).

        Parameters
        ----------
        name : str
            Name of newly created :class:`Detector` instance.

        node : :class:`.Node`
            Node to read output from.

        dtype : type, optional
            The numpy datatype for which this output result will be stored in

        unit : str, optional
            A human readable unit for the output. E.g. W, m, m/rtHz
        """
        ModelElement.__init__(self, name)
        self._node = node
        self.__dtype = dtype
        self.__unit = unit
        self.__label = label

    @property
    def dtype(self): return self.__dtype

    @property
    def unit(self): return self.__unit

    @property
    def label(self): return self.__label

    @abstractmethod
    def get_output(self, DC, AC):
        """
        Calculates the output of the detector. Detectors should override this
        method with the relevant functions.

        Returns
        -------
        self.dtype
            The detectors output.
        """

class SymbolDetector(Detector):
    def __init__(self, name, symbol, dtype=np.complex128):
        Detector.__init__(self, name, None, dtype=dtype)
        self.symbol = symbol

    def get_output(self, *args):
        return self.dtype(self.symbol.eval())




