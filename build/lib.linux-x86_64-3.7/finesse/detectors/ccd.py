import numpy as np
import logging

LOGGER = logging.getLogger(__name__)

from finesse.detectors.general import Detector
from finesse.detectors.compute import ccd_output
from finesse.utilities.misc import find_nearest

class CCD(Detector):
    """Detector to capture an image of the beam at a specified node."""
    def __init__(self, name, node, xlim, ylim, npts, f=None, scale_to_waist=True):
        """Constructs a new instance of `CCD` with the specified properties.

        Parameters
        ----------
        name : str
            Name of the CCD.

        node : :class:`.OpticalNode`
            Node to detect at.

        xlim : sequence or scalar
            Limits of the x-dimension of the image. If a single number
            is given then this will be `xlim = [-|xlim|, +|xlim|]`.

        ylim : sequence or scalar
            Limits of the y-dimension of the image. If a single number
            is given then this will be `ylim = [-|ylim|, +|ylim|]`.

        npts : int
            Number of points for each dimension.

        f : scalar or :class`.Parameter` or :class:`.ParameterRef`, optional
            Frequency of the field to detect. Defaults to `None` such that all
            fields are summed.

        scale_to_waist : bool, optional
            Flag determining whether to scale the x and y co-ordinates to the
            waist-size of the beam. Defaults to `True`.
        """
        Detector.__init__(self, name, node, dtype=np.ndarray)
        self.f = f

        if not hasattr(xlim, "__getitem__"):
            xlim = [-abs(xlim), abs(xlim)]
        else:
            if len(xlim) != 2:
                raise TypeError("expected xlim to be a single number of sequence of size 2 "
                                f"but got a sequence of size {len(xlim)}")
        if not hasattr(ylim, "__getitem__"):
            ylim = [-abs(ylim), abs(ylim)]
        else:
            if len(ylim) != 2:
                raise TypeError("expected ylim to be a single number of sequence of size 2 "
                                f"but got a sequence of size {len(ylim)}")

        self.x = np.linspace(xlim[0], xlim[1], npts)
        self.y = np.linspace(ylim[0], ylim[1], npts)
        self._out = np.zeros((npts, npts), dtype=np.complex128)
        self.__nr = self._node.space.nr if self._node.space is not None else 1.0
        self.__do_waist_scale = scale_to_waist

    @property
    def is_waist_scaled(self):
        """Flag for whether the x and y co-ordinates have been scaled by the waist-size.

        :getter: Returns `True` if x and y have been scaled by the beam waist, `False` otherwise.
        """
        return self.__do_waist_scale

    def get_output(self, DC, AC):
        node_q_map = DC.model.last_trace
        qx, qy, _ = node_q_map[self._node]

        if self.__do_waist_scale:
            x = qx.w0 * self.x.copy()
            y = qy.w0 * self.y.copy()
        else:
            x = self.x
            y = self.y

        # TODO (sjr) should probably cache this rather than re-constructing
        #            this array with every call to get_output
        freqs = np.array([float(freq.f) for freq in DC.frequencies])

        ccd_output(
            DC, self._node, freqs, DC.homs, qx.q, qy.q, self.__nr, DC.model.lambda0, x, y,
            DC.model.phase_config.ZERO_TEM00_GOUY,
            out=self._out, f=self.f
        )
        # TODO (sjr) do proper scaling of sqrt(2) above (in masking factor of finesse 2)
        self._out /= np.sqrt(2)

        return self._out

    def at(self, x=None, y=None, magnitude=False):
        """Retrieves a slice or single pixel of the output image.

        Parameters
        ----------
        x : scalar, optional
            Value indicating where to take a y-slice of the image or,
            if used in conjunction with `y`, which pixel to return. Defaults
            to `None`.

        y : scalar, optional
            Value indicating where to take a x-slice of the image or,
            if used in conjunction with `x`, which pixel to return. Defaults
            to `None`.

        magnitude : bool, optional
            Returns the amplitude of the detected field if `True`. Otherwise
            returns the full complex description.

        Returns
        -------
        out : :class:`numpy.ndarray` or float
            Either a slice of the image or a single pixel at the specified co-ordinates.
        """
        if x is None and y is None:
            return np.abs(self._out) if magnitude else self._out

        if x is None:
            nearest_idx = find_nearest(self.y, y, index=True)
            values = self._out[nearest_idx][:]
            return np.abs(values) if magnitude else values

        if y is None:
            nearest_idx = find_nearest(self.x, x, index=True)
            values = self._out[:,nearest_idx]
            return np.abs(values) if magnitude else values

        nearest_indices = find_nearest(self.x, x, index=True), find_nearest(self.y, y, index=True)
        values = self._out[nearest_indices]
        return np.abs(values) if magnitude else values
