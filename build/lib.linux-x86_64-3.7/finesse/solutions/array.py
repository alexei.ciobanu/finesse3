"""Simulation output"""

import numpy as np
import logging
from .base import BaseSolution

from finesse.detectors import Detector, CCD
from finesse.detectors import BeamProperty, BeamPropertyDetector

LOGGER = logging.getLogger(__name__)


class ArraySolution(BaseSolution):
    """
    This in an object that holds outputs from running a simulation. It is
    essentially a wrapped up Numpy structured array whose named elements are
    the names of outputs in a model.

    Detectors are stored in the array by their name. So you can use::

        output['detector_name']

    or, if the key has an attribute called `name` (as all Finesse.detectors do)
    it will use that, so using::

        output[ifo.detector]

    will return the same values.

    The underlying storage format is a Numpy structured array. You can select
    runs by::

        output[ifo.detector][a:b:c]

    where `a:b:c` is your slice. Or you can select multiple outputs with::

        output[['det1', 'det2']][a:b:c]
    """

    def __init__(self, model, shape):
        """
        Constructs a new `Output` object from the specified `model`.

        Parameters
        ----------
        model : :class:`.Model`
            Model object to get simulation data from

        shape : :class:`numpy.ndarray`
            The shape of the underlying data array. use a single integer for 1D
            outputs, N-dimensional outputs can be specified by using tuples, i.e.
            (10,5,100) for a 3D array with the requested sizes.
        """
        self._outputs = {}
        self._num = -1
        self._model = model
        self._units  = []
        self._axes = None

        dtypes = []

        for det in model.detectors:
            dtypes.append((det.name, det.dtype))

        LOGGER.info("Outputting shape=%s dtype=%s", shape, dtypes)

        self._dtype   = np.dtype(dtypes)
        self._outputs = np.zeros(shape, dtype=self._dtype)

    def __getattr__(self, key):
        return self[key]

    def __str__(self):
        return str(self._outputs)

    @property
    def axes(self): return self._axes

    @property
    def entries(self):
        """
        The number of outputs that have been stored in here so far
        """
        return self._num + 1

    @property
    def shape(self):
        return self._outputs.shape

    @property
    def outputs(self):
        """
        Returns all the outputs that have been stored.
        """
        return self._Output__outputs.dtype.names

    def expand(self, shape):
        """
        Expands the output buffer by `shape` elements. This will be slow if you
        call repeatedly to increase by just a few elements as the whole array
        is copied into a new block of memory.

        Parameters
        ----------
        shape
            Shape of new array to make
        """
        self._outputs = np.append(self._outputs, np.empty(shape, dtype=self._dtype))

    def __getitem__(self, key):
        if hasattr(key, 'name'):
            return self._outputs[key.name]
        else:
            return self._outputs[key]

    def update(self, index):
        """
        Calling this will compute all detector outputs and add an entry to
        the outputs stored. Calling it multiple times without re-running
        the simulation will result in duplicate entries.

        Parameters
        ----------

        index : (int, ...)
           Index to calculate the outputs for, use tuples of N-size for
           N-dimensional outputs
        """
        for det in self._model.detectors:
            o = det.get_output(self._model.carrier_simulation, self._model.signal_simulation)
            self[det][index] = o

        self._num += 1

    def plot(
        self, detectors=None,
        index=None,
        logx=False, logy=False, degrees=True,
        figsize_scale=None,
        show=True
    ):
        r"""Plots the outputs from the specified `detectors`, or all detectors in the executed
        model if `detectors` is `None`.

        Detectors are sorted by their type and the outputs of each are plotted on their own figure
        accordingly - i.e. all amplitude detector outputs are plotted on one figure, all power detector
        outputs on another figure etc.

        .. note::

            It is recommended to use this method alongside the features of the :mod:`finesse.plotting`
            sub-module. This then means that all figures produced by this function will use matplotlib
            rcParams corresponding to the style selected with :func:`finesse.plotting.use`.

            For example::

                import finesse
                finesse.plotting.use("default")

                model = finesse.parse(\"""
                l L0 P=1

                s s0 L0.p1 ITM.p1

                m ITM R=0.99 T=0.01 Rc=inf
                s CAV ITM.p2 ETM.p1 L=1
                m ETM R=0.99 T=0.01 Rc=10

                maxtem 4

                gauss L0.p1.o q=(-0.4+2j)
                cav FP ITM.p2.o ITM.p2.i

                xaxis &L0.f (-100M) 100M 1000 lin

                ad ad00 ETM.p1.i f=&L0.f n=0 m=0
                ad ad02 ETM.p1.i f=&L0.f n=0 m=2

                pd C ETM.p1.i
                \""")
                model.run().plot(logy=True, figsize_scale=2)

            will produce two figures (one for the power-detector output and another for the
            amplitude detectors) which use rcParams from the `default` style-sheet. Using
            `figsize_scale` here then scales these figures whilst keeping the proportions
            defined in this style-sheet constant.

        Parameters
        ----------
        detectors : sequence or str or :class:`.Detector`, optional
            An iterable of strings (corresponding to detector names) or :class:`.Detector`
            instances. Defaults to `None` so that all detector outputs are plotted.

        index : int, optional
            Applicable only for multi-dimensional analyses. Specifies which index of the run
            to plot an image plot for.

        logx : bool, optional
            Use log-scale on x-axis if appropriate, defaults to `False`.

        logy : bool, optional
            Use log-scale on y-axis if appropriate, defaults to `False`.

        degrees : bool, optional
            Plots angle and phase outputs in degrees, defaults to `True`.

        figsize_scale : scalar, optional
            Scales the size of all produced figures by a fixed amount. This has the advantage
            of keeping the original figure proportions intact.

        show : bool, optional
            Shows all the produced figures if true, defaults to `True`.

        Returns
        -------
        figures : dict
            A dictionary of type(detector) : figure mappings. If there any :class:`.BeamPropertyDetector`
            instances in the model then the figures of these will appear in this return dictionary as
            (BeamPropertyDetector, detector.detecting) : figure mappings (see :attr:`.BeamPropertyDetector.detecting`).
        """
        import matplotlib.pyplot as plt

        if detectors is None:
            detectors = self._model.detectors

        if isinstance(detectors, str) or isinstance(detectors, Detector):
            detectors = [detectors]

        detector_type_map = {}
        bp_detector_map = {}
        for detector in detectors:
            if isinstance(detector, str):
                detector = self._model.elements[detector]
            if isinstance(detector, BeamPropertyDetector):
                if detector.detecting in bp_detector_map:
                    bp_detector_map[detector.detecting].append(detector)
                else:
                    bp_detector_map[detector.detecting] = [detector]
            else:
                if type(detector) in detector_type_map:
                    detector_type_map[type(detector)].append(detector)
                else:
                    detector_type_map[type(detector)] = [detector]

        figures = {}

        for (det_type, det_prop), det_list in bp_detector_map.items():
            if det_prop == BeamProperty.Q:
                continue

            fig = plt.figure()

            for det in det_list:
                plt.plot(self.x1, self[det], label=det.name)

            if det.label is not None:
                plt.ylabel(f"{det.label} [{det.unit}]")
            plt.legend()

            if self.p1.units:
                plt.xlabel(f"{self.p1.component.name} {self.p1.name} [{self.p1.units}]")
            else:
                plt.xlabel(f"{self.p1.component.name} {self.p1.name}")

            figures[(det_type, det_prop)] = fig

        # TODO (sjr) do something different if we have a multi-dimensional parameter scan
        for det_type, det_list in detector_type_map.items():
            if det_type == CCD:
                plot_fn = plt.imshow

                for det in det_list:
                    fig = plt.figure()

                    if index is None: data = self[det]
                    else: data = self[det][index]

                    # TODO (sjr) pass normalisation argument depending upon logx, logy flags
                    plot_fn(
                        np.abs(data),
                        extent=[det.x.min(), det.x.max(), det.y.min(), det.y.max()],
                        origin="lower"
                    )

                    plt.axis(aspect="image")
                    plt.xlabel("$x$ " + "[$w_0$]" if det.is_waist_scaled else "[m]")
                    plt.ylabel("$y$ " + "[$w_0$]" if det.is_waist_scaled else "[m]")
                    # TODO (sjr) colour-bar
                    figures[det_type] = fig
            else:
                fig = plt.figure()

                mag_and_phase = any([det.dtype == np.complex128 for det in det_list])
                if mag_and_phase:
                    mag_ax = fig.add_subplot(211)
                    phase_ax = fig.add_subplot(212, sharex=mag_ax)

                if logx and logy:
                    plot_fn = mag_ax.loglog if mag_and_phase else plt.loglog
                elif logx:
                    plot_fn = mag_ax.semilogx if mag_and_phase else plt.semilogx
                elif logy:
                    plot_fn = mag_ax.semilogy if mag_and_phase else plt.semilogy
                else:
                    plot_fn = mag_ax.plot if mag_and_phase else plt.plot

                for det in det_list:
                    if mag_and_phase:
                        plot_fn(self.x1, np.abs(self[det]), label=det.name + " (abs)")
                        mag_ax.set_ylabel(r"Amplitude [$\sqrt{W}$]")
                        mag_ax.legend()

                        phase = np.angle(self[det])
                        phase_ax.plot(self.x1, np.degrees(phase) if degrees else phase, label=det.name + " (phase)")
                        phase_ax.set_ylabel(f"Phase [{'deg' if degrees else 'rad'}]")
                        phase_ax.legend()
                    else:
                        plot_fn(self.x1, self[det], label=det.name)

                if not mag_and_phase:
                    if det.label is not None:
                        plt.ylabel(f"{det.label} [{det.unit}]")
                    plt.legend()

                if self.p1.units:
                    plt.xlabel(f"{self.p1.component.name} {self.p1.name} [{self.p1.units}]")
                else:
                    plt.xlabel(f"{self.p1.component.name} {self.p1.name}")

                figures[det_type] = fig

        if figsize_scale is not None:
            for fig in figures.values():
                fig.set_size_inches(figsize_scale * fig.get_size_inches())

        if show:
            plt.show()

        return figures
