"""Base solution interface.

Solution classes contain the output from a Finesse simulation, and convenience methods for
accessing, plotting and serialising that output.

Solutions intentionally do not contain references to the model that produced its results. This is so
that the solution can be serialised without requiring the model that produced it itself be
serialisable.
"""

import abc


class BaseSolution(metaclass=abc.ABCMeta):
    pass
