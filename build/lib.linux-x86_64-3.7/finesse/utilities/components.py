"""Utility functions related to component objects."""

def refractive_indices(curr, nxt, symbols=False):
    """Obtains the refractive indices of the spaces attached to
    the ports/nodes `curr` and `nxt`.

    Parameters
    ----------
    curr : :class:`.Port` or :class:`.Node`
        Current port or node.

    nxt : :class:`.Port` or :class:`.Node`
        Next port or node.

    Returns
    -------
    nr : tuple
        A two-element tuple containing the refractive indices
        of the spaces attached to each port/node.
    """
    nr1 = 1.0
    nr2 = 1.0

    curr_space = curr.space
    next_space = nxt.space

    if curr_space is not None:
        if symbols:
            nr1 = curr_space.nr.symbol
        else:
            nr1 = curr_space.nr.value
    if next_space is not None:
        if symbols:
            nr2 = next_space.nr.symbol
        else:
            nr2 = next_space.nr.value

    return nr1, nr2
