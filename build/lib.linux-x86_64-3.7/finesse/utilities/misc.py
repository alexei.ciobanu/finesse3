"""Miscellaneous utility functions for any part of Finesse."""

import re
from itertools import tee

import numpy as np

def check_name(name):
    """Checks the validity of a component or node name.

    Parameters
    ----------
    name : str
        The name to check.

    Returns
    -------
    name : str
        The name passed to this function if valid.

    Raises
    ------
    Exception
        If `name` contains non-alphanumeric / underscore characters.
    """
    if not re.compile("^[a-zA-Z0-9_]*$").match(name):
        raise Exception("Name `{}` is not valid. Alphanumeric and underscores only".format(name))
    return name

def pairwise(iterable):
    """Iterates through each pair in a iterable.

    Parameters
    ----------
    iterable : Sub-class of :class:`collections.Iterable`
        An iterable object.

    Returns
    -------
    zip
        A zip object whose `.next()` method returns a tuple where the i-th
        element comes from the i-th iterable argument.
    """
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)

def find(x, value):
    """Finds `value` in the list `x` and returns its index, returning
    `None` if `value` is not in the list."""
    try:
        return x.index(value)
    except ValueError:
        return None

def find_nearest(x, value, index=False):
    idx = np.argmin(np.abs(x - value))
    if index: return idx
    return x[idx]

def _collect_submodules(name=''):
    import os
    import finesse

    if name and not name.endswith('/'): name += '/'
    autodocs = ''
    submods = []

    exclude = ("__init__.py", "__main__.py", "version.py")

    for file in os.listdir(finesse.__file__.replace("__init__.py", name)):
        if file.endswith(".pyx") or (file.endswith(".py") and file not in exclude):
            submods.append(file.split('.')[0])
        elif not '.' in file and file != "__pycache__":
            if not name:
                if "__init__.py" in os.listdir(finesse.__file__.replace("__init__.py", file + '/')):
                    submods.append(file)

    # alphabetically sort sub-module list
    submods.sort()
    for submod in submods:
        if name:
            autodocs += "\n\tfinesse.{}.{}".format(
               name.split('/')[0], submod
            )
        else:
            autodocs += f"\n\tfinesse.{submod}"

    return autodocs
