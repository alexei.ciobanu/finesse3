"""
Plotting tools for Finesse, providing convenient style templates for
:mod:`matplotlib.pyplot` and functions for quick visualisation of detector
and/or probe outputs.
"""

import logging

from finesse.plotting.style import list_styles, use, context

LOGGER = logging.getLogger(__name__)

def _in_ipython():
    """Checks whether the current script is running under IPython.

    Returns
    -------
    bool
        True if __IPYTHON__ is defined, otherwise False.
    """
    try:
        __IPYTHON__
    except NameError:
        return False
    else:
        return True


def init(mode="display", dpi=None, fmts=['svg']):
    """Sets up default plotting parameters for a desired display mode.

    Parameters
    ----------
    mode : str
        Display mode to use, either 'display' or 'paper'.

    dpi : int, optional
        DPI (Dots per inch) to display and save figures with. Defaults to
        current setting.

    fmts : list of str, optional
        List of image formats to allow for display when running under IPython.
        Defaults to ['svg'].
    """
    import matplotlib as mpl

    if _in_ipython():
        try:
            from IPython.display import set_matplotlib_formats
            ipy = get_ipython()
            set_matplotlib_formats(*fmts)
            try:
                ipy.magic("matplotlib inline")
            except:
                try:
                    ipy.magic("matplotlib qt")
                except:
                    LOGGER.warn('Could not set matplotlib backend. Tried inline and Qt.')
        except NameError:
            pass

    if mode == "display":
        use(["default"])
    elif mode == "paper":
        mpl.use("pgf")
        use(["default", "paper"])
    else:
        raise (BaseException(
            "Plotting mode must be either 'display' or 'paper'."))

    if dpi is None:
        dpi = mpl.rcParams['figure.dpi']
    else:
        dpi = int(dpi)
        mpl.rcParams.update({'figure.dpi': dpi})
        mpl.rcParams.update({'savefig.dpi': dpi})

    if (mpl.__version__ < '1.5'):
        # prop_cycle doesn't exist pre-1.5, and unknown rcParams are ignored,
        # so we have to hardcode the color cycle
        # TODO: this can probably be removed, as we have a python 3.6+
        # dependency, which was released after 1.5
        mpl.rcParams.update({
            'axes.color_cycle':
            ['0000FF', 'FF0000', '000000', '00FF00', 'FF00FF']
        })
