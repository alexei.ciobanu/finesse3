"""Custom exception types raised by different
Finesse functions and class methods.
"""

class ComponentNotConnected(Exception): pass
class ParameterLocked(Exception): pass

class NodeException(Exception):
    """
    Custom exception type associated with :class:`.Node` related
    run-time errors.

    Objects of type `NodeException` store the error message as well
    as an optional reference to the node(s) which caused the exception
    to be raised.
    """
    def __init__(self, message, node=None):
        """Constructs a new instance of `NodeException` with the specified properties.

        Parameters
        ----------
        message : str
            The error message.

        node : :class:`.Node`, optional
            A reference to the offending node(s), defaults to `None`. This can
            be a single node or a sequence of nodes.
        """
        super().__init__(message)
        self.__node = node

    @property
    def node(self):
        """The node(s) responsible for raising this exception instance.

        :getter: Returns the node(s) (either a single :class:`.Node` object
                 or a sequence of these objects) responsible for the exception
                 (read-only).
        """
        return self.__node

class TotalReflectionError(Exception):
    """
    Exception type indicating total reflection of a beam at a component
    when performing beam tracing.
    """
    def __init__(self, message, from_node, to_node):
        """Constructs a new instance of `TotalReflectionError` with the specified properties.

        Parameters
        ----------
        message : str
            The error message.

        from_node : :class:`.Node`
            A reference to the offending source node.

        to_node : :class:`.Node`
            A reference to the offending target node.
        """
        super().__init__(message)
        self.__from_node = from_node
        self.__to_node = to_node

    @property
    def coupling(self):
        """The tuple of (from, to) nodes responsible for the total reflection error.

        :getter: Returns the nodes responsible for the exception (read-only).
        """
        return (self.__from_node, self.__to_node)
