"""
Fast C functions for common mathematical routines
used across all the various Cython extensions of Finesse.

.. autosummary::
    :toctree: cmath/

    finesse.cmath.cmath
"""

#if __doc__:
#    from finesse.utilities.misc import _collect_submodules
#
#    __doc__ = __doc__.format(contents=_collect_submodules("cmath"))
