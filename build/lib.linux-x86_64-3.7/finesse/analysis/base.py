"""Base analysis interface.

Analysis classes define a particular type of simulation to perform with Finesse.
They run the Finesse model on behalf of the user and provide them with a solution
containing the results.
Advanced users may define special analyses that e.g. solve multiple models and
combine the results together, provide special solution types, etc. These should
always subclass the base analysis defined here.
"""

import abc
import logging

LOGGER = logging.getLogger(__name__)


class BaseAnalysis(metaclass=abc.ABCMeta):
    """Base analysis.

    This defines a common interface to the Finesse model solver to be used for most concrete
    analyses.

    Parameters
    ----------
    model : :class:`.Model`
        The Finesse model to run.
    """
    def __init__(self, model):
        self._solution = None
        self._constraints = []
        self.model = model

    @property
    def solution(self):
        if self._solution is None:
            raise Exception("Solution has not been computed. Call run().")
        return self._solution

    def add_constraint(self, constraint):
        if constraint in self._constraints:
            raise ValueError(f"{constraint} is already present")
        self._constraints.append(constraint)

    def run(self, *args, validate=True, **kwargs):
        """Run the simulation.

        Parameters
        ----------
        validate : :class:`bool`, optional
            Whether to validate the simulation results agains the constaints defined in this
            analysis.

        Returns
        -------
        :class:`.BaseSolution`
            The simulation results.
        """
        self._pre_run(**kwargs)
        self._run(*args, **kwargs)
        self._post_run(**kwargs)
        if validate:
            self._eval_constraints()
        return self.solution

    def _pre_run(self, **kwargs):
        """Runs before the main simulation."""
        pass

    @abc.abstractmethod
    def _run(self):
        """Runs the simulation.

        This method should populate self._solution.
        """
        raise NotImplementedError

    def _post_run(self, **kwargs):
        """Runs after the main simulation."""
        pass

    def _eval_constraints(self):
        """Evaluates the simulation results against the constraints defined in this analysis."""
        if self._solution is None:
            raise ValueError("No solution available.")
        statuses = [constraint.evaluate(self._solution) for constraint in self._constraints]
        if all([flag for flag, msg in statuses]):
            return
        # Some constraints failed.
        failures = ", ".join([msg for flag, msg in statuses if not flag])
        LOGGER.error(f"The following constraint(s) failed: {failures}")
