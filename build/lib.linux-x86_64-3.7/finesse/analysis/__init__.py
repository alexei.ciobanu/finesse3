"""
Contains the different analyses which can be performed on a model.

Listed below are all the sub-modules of the ``analysis`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: analysis/
    {contents}
"""

from finesse.analysis.axes import X0Axis as noxaxis, X1Axis as xaxis, X2Axis as x2axis

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    __doc__ = __doc__.format(
        contents=_collect_submodules("analysis")
    )

