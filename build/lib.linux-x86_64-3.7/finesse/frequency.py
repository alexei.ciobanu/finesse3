import numbers
import weakref
import numpy as np

from finesse.element import ModelElement, model_parameter, Rebuild, Symbol

def generate_frequency_list(model):
    """
    For a given model a symbolic list of frequencies is generated. 
    The result can be used to generate a set of frequencies bins to
    be modelled in a simulation.
    
    This method relies on using :class:`finesse.element.Symbol`. Using
    symbolic statements this method attempts to isolate uniqe frequency
    bins whilst leaving those changing during a simulation present.
    
    Returns
    -------
    List of :class:`finesse.element.Symbol` 
    """
    
    def unique_indices(arr):
        """
        Simple unique element finder which doesn't require any
        greater or less than operations. This isn't tuned for
        efficiency at all.
        """
        lst  = list(arr)
        lst2 = list(arr)
        for i in range(len(lst)):
            if lst.count(lst2[i]) > 1:
                lst[i] = None
           
        return [_ for _,__ in enumerate(lst) if __ is not None]
    
    fn_eval  = np.vectorize(lambda x: x.eval())
    fn_eval2 = np.vectorize(lambda x: x.eval(keep_changing_symbols=True))
    fn_subs = np.vectorize(lambda x, **kwargs: x.eval(keep_changing_symbols=True, subs=kwargs))
    fn_is_changing = np.vectorize(lambda x: x.is_changing)

    source_frequencies = []
    source_components  = []
    modulation_frequencies = []

    for comp in model._frequency_generators:
        s = comp._source_frequencies()
        source_frequencies.extend(s)
        source_components.extend((comp,)*len(s))

        m = comp._modulation_frequencies()
        modulation_frequencies.extend(m)

    # Now to prune the frequency list
    Nm = len(modulation_frequencies)
    Ns = len(source_frequencies)
    
    if Ns == 0:
        raise Exception("There are no source frequencies present in the model")
        
    if Nm == 0:
        Fsym = np.array(source_frequencies)
    else:
        # First we make a list with all possible combinations of frequencies
        Fsym = (np.vstack((np.atleast_2d(modulation_frequencies),)*Ns) + np.hstack((np.atleast_2d(source_frequencies).T, )*Nm)).flatten()
        Fsym = np.hstack((source_frequencies, Fsym))

    # Take all the frequency values which definitely won't be changing
    not_changing = Fsym[np.bitwise_not(fn_is_changing(Fsym))]
    if len(not_changing) == 0:
        not_changing = []
    else:
        # ... and select all those which are unique
        values, idx, rev, counts = np.unique(fn_eval(not_changing), True, True, True)
        not_changing = not_changing[idx]

    # First select only the changing frequency bins
    changing = Fsym[fn_is_changing(Fsym)]
    if len(changing) == 0:
        changing = []
    else:
        # We need to use a different unique finding function that doesn't rely on
        # using > or < than comparisons like np.unique. I'm not sure how to implement
        # > and < for Symbols sensibly
        idx = unique_indices(fn_eval2(changing))
        changing = changing[idx]

    # Finally sort the indicies so that upper and lower sidebands are grouped together. This
    # help with numerical errors when iterating outputs later so that similarly sized elements are grouped
    # this isn't always true though, just generally in regards to upper and lower sidebands.
    final = np.hstack((changing, not_changing))
    srt_idx = np.argsort(abs(fn_eval(final)))
    return final[srt_idx]
 
@model_parameter('f', None, Rebuild.PlaneWave, '_validate_fsig')
class Fsig(ModelElement):
    def __init__(self, name, value):
        super().__init__(name)
        self.f = value
        
    def _validate_fsig(self, value):
        if value is None or isinstance(value, Symbol):
            return value
        elif value <= 0:
            raise Exception("fsig value must be > 0 Hz")
        else:
            return value

class Frequency(object):
    """An object representation of a frequency "bin" with a specific index.

    The value of the frequency is calculated from the name of the
    frequency and any :class:`.SourceFrequency` objects present.
    """
    def __init__(self, name, sim, symbol, index=None, audio=False, audio_carrier_index=None, audio_order=0):
        """Constructs a new :class:`Frequency` object with the specified
        values.

        Parameters
        ----------
        name : str
            Name of the frequency.

        order : int, optional
            The order of the frequency, defaults to zero.
        """
        self.__name = name
        self.__symbol = symbol
        self.__index = index
        self.__is_audio = audio
        self.__sim = sim
        self.__symbol_changing = symbol.is_changing
        self.__start_value = self.__symbol.eval()
        
        if audio:
            if audio_carrier_index is None:
                raise Exception("Audio frequency carrier must be specified")
            
            if audio_order not in (-1, 1):
                raise Exception("Audio frequency order must be -1 or +1")
        
            self.__order = audio_order
            self.__carrier = audio_carrier_index
        else:
            self.__order = None
            self.__carrier = None

    def __repr__(self):
        return f"<{self.__class__.__name__} {self.name} (f={self.symbol}={self.f}) at { hex(id(self)) }>"

    def __str__(self):
        return f"<{self.name} is {self.f}Hz (Frequency)>"

    def __deepcopy__(self, memo):
        new = object.__new__(type(self))
        memo[id(self)] = new 
        new.__dict__.update(deepcopy(self.__dict__, memo))
        # Manually update the weakrefs to be correct
        if self.__carrier is not None:
            new.__carrier = weakref.ref(memo[id(self.__carrier())])
        return new
        
    @property
    def simulation(self): return self.__sim
    
    @property
    def symbol(self): return self.__symbol

    @property
    def f(self):
        if self.__symbol_changing:
            return self.__symbol.eval()
        else:
            return self.__start_value
        
    @property
    def is_audio(self):
        """Is this an audio sideband frequency?"""
        return self.__is_audio

    @property
    def audio_carrier_index(self):
        """The carrier frequency.

        :getter: Returns the carrier frequency index (read-only).
        """
        return self.__carrier

    @property
    def name(self):
        """Name of the frequency object.

        :getter: Returns the name of the frequency (read-only).
        """
        return self.__name

    @property
    def audio_order(self):
        """Audio modulation order of this frequency.

        :getter: Returns the order of the frequency (read-only).
        """
        return self.__order

    @property
    def index(self):
        """Index of the frequency object.

        :getter: Returns the index of the frequency (read-only).
        """
        return self.__index
