class Kat(object):
    def __init__(self, model, analyses):
        self.__model = model
        self.analyses = list(analyses)

    @property
    def model(self): return self.__model

    def run(self):
        outs = tuple((_.run() for _ in self.analyses))

        if len(outs) > 1:
            return outs
        else:
            return outs[0]
