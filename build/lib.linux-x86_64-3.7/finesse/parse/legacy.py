"""
Contains the :class:`.KatParser` class, which is used for
constructing a :class:`.Model` from kat code.
"""

from collections import OrderedDict
import logging
import re

from sly import Lexer, Parser

import finesse.analysis as analysis
import finesse.components as components
import finesse.detectors as detectors
import finesse.element as element
import finesse.gaussian as gaussian
from finesse.model import Model
from finesse.frequency import Frequency
from finesse.kat import Kat

LOGGER = logging.getLogger(__name__)


class KatParser():
    """Kat file lexer, parser and builder."""

    def __init__(self):
        """Construct a new parser object."""
        self.lexer = _KatLEX()
        self.parser = _KatYACC()
        self.reset()

    def reset(self):
        """
        Delete all parsed code, resetting the parser to a newly constructed
        state.
        """
        self.blocks = None
        self.parser.reset()
        self.lexer.reset()

    def parse(self, text=None, path=None):
        """
        Parses a kat file into an intermediate internal representation, which
        can then be built by :meth:`.KatParser.build`. One of either `text` or
        `path` must be provided.

        Parameters
        ----------
        text : str, optional
            String containing the kat code to be parsed.

        path : str, optional
            Path of a file containing the kat code to be parsed.

        Raises
        ------
        ValueError
            If either both or neither of `text` and `path` are specified.

        KatParserError
            If an error occurs during parsing.
        """
        if text is None and path is None:
            raise ValueError("must provide either text or a path")

        if path is not None:
            if text is not None:
                raise ValueError("cannot specify both text and a file to parse")

            with open(path, "r") as obj:
                text = obj.read()

            source = path
        else:
            source = "from user input"

        # As we have no way of detecting EOF and calling pop_state() from
        # within the component lexer, we must ensure that all files end in a
        # newline
        text = f"{text}\n"

        # Trim any whitespace within $$ strings
        matches = re.findall(r"\$\$[^$]+\$\$", text)
        for match in matches:
            text = text.replace(match, re.sub(r"\s+", "", match))

        tokens = self.lexer.tokenize(text)
        self.parser.parse(tokens)
        errors = sorted(self.lexer.errors + self.parser.errors,
                        key=lambda tup: tup[1])
        if len(errors) > 0:
            raise KatParserError(errors, text)
        self.blocks = self.parser.blocks

        LOGGER.info("Parsed %s", source)

    def build(self, ignored_blocks=[]):
        """
        Constructs a :class:`.Model` from parsed kat code.

        Parameters
        ----------
        ignored_blocks : list, optional
            A list of names of ``FTBLOCK`` sections in the kat code to leave
            out of the model.

        Returns
        -------
        Model
            The constructed :class:`.Model`
        """
        def parse_parameter(param):
            if type(param) is not str or '$' not in param:
                return param
            local = dict(map(lambda f: (f.name, f), model.frequencies))
            local.update(model.alternate_name_map)
            local = {**local, **model.elements}
            if param.endswith("$"):
                p = re.sub("([a-zA-Z_][a-zA-Z0-9_:.]*)", r"\1.ref", param)
            else:
                p = param
            return eval(f"{p}".replace("$", ""), local)

        LOGGER.info("Building model")

        component_constructors = {
                "lasers": components.Laser,
                "mirrors": components.Mirror,
                "beam_splitters": components.Beamsplitter,
                # "directional_beam_splitters": None,
                "isolators": components.Isolator,
                "modulators": components.Modulator,
                "lenses": components.Lens,
                }

        detector_constructors = {
                "amplitude_detectors": detectors.AmplitudeDetector,
                "quantum_noise_detectors": detectors.QuantumNoiseDetector,
                "power_detectors": detectors.PowerDetector
                }
        model = Model()
        node_names = ["node", "node1", "node2", "node3", "node4"]
        nodes = {}
        consts = {}
        blocks = {}
        for k in self.blocks:
            if k not in ignored_blocks:
                blocks[k] = self.blocks[k]
            else:
                LOGGER.debug(f"Ignoring block '{k}'")

        # First pass, just grab constants and variables
        for block, d in blocks.items():
            for k, v in d["constants"].items():
                consts[k] = v
            for k, v in d["variables"].items():
                model.add(components.Variable(k, v))

        # Next grab source frequencies
        for block, d in blocks.items():
            for f in d["frequencies"]:
                model.add_frequency(Frequency(f["name"], model, element.Constant(f["f"])))

        # Next construct all frequencies & normal components,
        # and get node names
        for block, d in blocks.items():
            for name, constructor in component_constructors.items():
                for comp in d[name]:
                    for k, v in comp.items():
                        if v in consts:
                            comp[k] = consts[v]
                        elif type(v) is str and '$' in v:
                            comp[k] = 0
                            d["puts"].append({
                                    "component": comp["name"],
                                    "parameter": k,
                                    "variable": v,
                                    "add": False})
                        else:
                            comp[k] = v
                    args = []
                    ns = []
                    for k in comp:
                        if k in node_names:
                            ns.append(comp[k])
                        else:
                            args.append(comp[k])
                    model.add(constructor(*args))
                    for i, n in enumerate(ns):
                        if n in nodes and n != "dump":
                            LOGGER.error(f"In block '{block}': {args[0]}: "
                                         f"Node '{n}' already assigned to"
                                         f"'{nodes[n][0]}'.")
                        else:
                            nodes[n] = (args[0], ns.index(n) + 1)
                            # get the output node object corresponding to
                            # this node and tag it
                            _ni = getattr(
                                getattr(getattr(model, comp["name"]),f"p{i + 1}"), 'o'
                            )
                            try: model.tag_node(_ni, n)
                            except: pass

        # Connect all of the components up with spaces
        for block, d in blocks.items():
            for space in d["spaces"]:
                for k, v in space.items():
                    if v in consts:
                        space[k] = consts[v]
                    elif type(v) is str and '$' in v:
                        space[k] = 0
                        d["puts"].append({
                                "component": space["name"],
                                "parameter": k,
                                "variable": v,
                                "add": False})
                    else:
                        space[k] = v
                try:
                    comp1 = nodes[space["node1"]]
                    node1 = getattr(getattr(model, comp1[0]), f"p{comp1[1]}")
                except KeyError:
                    name = space["name"] + "_" + space["node1"]
                    comp1 = components.Nothing(name)
                    model.add(comp1)
                    node1 = comp1.p1
                    nodes[space["node1"]] = (args[0], ns.index(n) + 1)
                try:
                    comp2 = nodes[space["node2"]]
                    node2 = getattr(getattr(model, comp2[0]), f"p{comp2[1]}")
                except KeyError:
                    name = space["name"] + "_" + space["node2"]
                    comp2 = components.Nothing(name)
                    model.add(comp2)
                    node2 = comp2.p1
                    nodes[space["node2"]] = (args[0], ns.index(n) + 1)
                args = []
                for k in space:
                    if k not in node_names:
                        args.append(space[k])

                model.connect(node1, node2, *args)

        # Create detectors
        for block, d in blocks.items():
            for name, constructor in detector_constructors.items():
                for det in d[name]:
                    for k, v in det.items():
                        if v in consts:
                            det[k] = consts[v]
                        elif type(v) is str and '$' in v:
                            det[k] = 0
                            d["puts"].append({
                                    "component": det["name"],
                                    "parameter": k,
                                    "variable": v,
                                    "add": False})
                        else:
                            det[k] = v
                    args = {}
                    freqs = []
                    phases = []
                    for k in det:
                        if k not in node_names:
                            if re.fullmatch("f[0-9]+", k) is not None:
                                freqs.append(det[k])
                            elif re.fullmatch("phase[0-9]+", k) is not None:
                                phases.append(det[k])
                            else:
                                args[k] = det[k]
                        else:
                            direction = "o"
                            if v.endswith("*"):
                                v = v.strip("*")
                                direction = "i"
                            node = f"p{nodes[v][1]}.{direction}"
                            n = model.elements[nodes[v][0]]
                            for attr in node.split("."):
                                n = getattr(n, attr)
                            args["node"] = n
                    if len(freqs) > 0:
                        args["freqs"] = freqs
                    if len(phases) > 0:
                        args["phases"] = phases
                    model.add(constructor(**args))

        # Apply attributes
        for block, d in blocks.items():
            for k, v in d["attributes"].items():
                component = model.elements[k]
                for attr, val in v:
                    setattr(component, attr, val)

        # Create cavities
        for block, d in blocks.items():
            for cav in d["cavities"]:
                try:
                    comp1 = nodes[cav["node1"]]
                except KeyError:
                    raise KeyError(f"Node '{cav['node11']}' not in model.")
                try:
                    comp2 = nodes[cav["node2"]]
                except KeyError:
                    raise KeyError(f"Node '{cav['node2']}' not in model.")
                node1 = getattr(getattr(model, comp1[0]), f"p{comp1[1]}")
                node2 = getattr(getattr(model, comp2[0]), f"p{comp2[1]}")
                if (comp1[0] != comp2[0]):
                    model.add(components.Cavity(cav["name"],
                                                node1.o, node1.i, node2.i))
                else:
                    model.add(components.Cavity(cav["name"], node1.o, node2.i))

        # Gauss commands
        for block, d in blocks.items():
            for gauss in d["gauss"]:
                try:
                    comp = nodes[gauss["node"]]
                except KeyError:
                    raise KeyError(f"Node '{gauss['node']}' not in model.")
                node = getattr(getattr(model, comp[0]), f"p{comp[1]}").o
                if "qy_re" in gauss:
                    node.q = (gauss["qx_re"] + 1j * gauss["qx_im"],
                              gauss["qy_re"] + 1j * gauss["qy_im"])
                elif "qx_re" in gauss:
                    node.q = gauss["qx_re"] + 1j * gauss["qx_im"]
                elif "w0y" in gauss:
                    node.q = (gaussian.BeamParam(w0=gauss["w0x"], z=gauss["zx"]),
                              gaussian.BeamParam(w0=gauss["w0y"], z=gauss["zy"]))
                else:
                    node.q = gaussian.BeamParam(w0=gauss["w0x"], z=gauss["zx"])

        # Create fsigs
        for block, d in blocks.items():
            for fsig in d["fsigs"]:
                if model.fsig.f.value is None:
                    model.fsig.f = parse_parameter(fsig["f"])
                elif model.fsig.f.value != parse_parameter(fsig["f"]):
                    raise ValueError("Cannot have more than one signal frequency.")

                # fsig command is specifying an input not just a frequency
                if len(fsig.keys()) == 2:
                    # Need to make some dummy object here to reference
                    # the model fsig value legacy issues of dealing with
                    # a single parameter that can have multiple names
                    model.fsig.f = fsig['f']
                    model.alternate_name_map[fsig['name']] = model.fsig
                else:
                    signal = components.SignalGenerator(fsig["name"], fsig["amp"], fsig["phase"])
                    model.add(signal)
                    comp = getattr(model, fsig["component"])
                    mod_type = fsig["mod_type"]
                    if mod_type == "amp":
                        node = comp.amp_sig
                    elif mod_type == "phase":
                        node = comp.phase_sig
                    elif mod_type == "freq":
                        node = comp.freq_sig
                    elif mod_type is None:
                        # TODO: determine this based on component type?
                        node = comp.phase_sig
                    else:
                        raise ValueError(f"Unkown signal type '{mod_type}'.")
                    model.connect(signal.p1, node)

        # Max TEM
        for block, d in blocks.items():
            model.maxtem = parse_parameter(d["maxtem"])

        # Phase command
        for block, d in blocks.items():
            model.phase_level = parse_parameter(d["phase"])

        # Input TEMs
        for block, d in blocks.items():
            for tem in d["tems"]:
                args = dict(tem)
                component = model.elements[args.pop("component")]
                component.tem(**args)

        # Detector masks
        for block, d in blocks.items():
            for mask in d["masks"]:
                args = dict(mask)
                detector = model.elements[args.pop("detector")]
                detector.mask(**args)

        # Puts
        for block, d in blocks.items():
            for put in d["puts"]:
                value = parse_parameter(put["variable"])
                if put["add"]:
                    component = model.elements[put["component"]]
                    param = getattr(component, put["parameter"])
                    setattr(component, put["parameter"], param + value)
                else:
                    component = model.elements[put["component"]]
                    setattr(component, put["parameter"], value)

        analyses = []

        # Xaxis
        for block, d in blocks.items():
            for xaxis in d["xaxis"]:
                if model.xaxis is not None:
                    raise ValueError("Cannot have more than one xaxis command.")

                xaxis["steps"] = int(xaxis["steps"])

                if xaxis['component'] in model.alternate_name_map:
                    comp = model.alternate_name_map[xaxis['component']]
                else:
                    comp = getattr(model, f"{xaxis['component']}")

                xaxis['parameter'] = getattr(comp, f"{xaxis['parameter']}")

                del xaxis['component']

                analyses.append(analysis.xaxis(xaxis['parameter'],
                                               xaxis['min'],
                                               xaxis['max'],
                                               xaxis['steps'],
                                               xaxis['scale']))

        # Yaxis
        for block, d in blocks.items():
            for yaxis in d["yaxis"]:
                if model.yaxis is not None:
                    raise ValueError("Cannot have more than one yaxis command.")
                model.yaxis = yaxis

        if len(analyses) == 0:
            analyses.append(analysis.noxaxis(model))

        return Kat(model, analyses)

class _KatLEX(Lexer):
    """Kat file lexer, default state."""

    tokens = {"AMPLITUDE_DETECTOR", "ATTRIBUTE", "BEAM_SPLITTER", "CAVITY",
              "CONSTANT", "DIRECTIONAL_BEAM_SPLITTER", "SOURCE_FREQUENCY",
              "FTBLOCK_END", "FTBLOCK_START", "FUNCTION", "FSIG", "GAUSS",
              "ISOLATOR", "LASER", "LENS", "LOCK", "MASK", "MAXTEM", "MIRROR",
              "MODULATOR", "NOPLOT", "NOXAXIS", "PHASE", "POWER_DETECTOR",
              "QUANTUM_NOISE_DETECTOR", "QUANTUM_SHOT_NOISE_DETECTOR", "PUT",
              "SPACE", "TEM", "VARIABLE", "XAXIS", "YAXIS"}

    # In order to allow components which are substrings of other components
    # (e.g. 'l' and 'lens'), these should be sorted in length order, from
    # longest to shortest.
    VARIABLE = r"variable"
    FTBLOCK_START = r"FTblock"
    FTBLOCK_END = r"FTend"
    NOXAXIS = r"noxaxis"
    QUANTUM_NOISE_DETECTOR = r"qnoised"
    POWER_DETECTOR = r"pd[0-9]*"
    MAXTEM = r"maxtem"
    PHASE = r"phase"
    NOPLOT = r"noplot"
    GAUSS = r"gauss\*?"
    QUANTUM_SHOT_NOISE_DETECTOR = r"qshot"
    XAXIS = r"xaxis"
    YAXIS = r"yaxis"
    ISOLATOR = r"isol"
    LOCK = r"lock\*?"
    LENS = r"lens"
    ATTRIBUTE = r"attr"
    CONSTANT = r"const"
    FUNCTION = r"func"
    FSIG = r"fsig"
    SOURCE_FREQUENCY = r"freq"
    MASK = r"mask"
    PUT = r"put\*?"
    CAVITY = r"cav"
    DIRECTIONAL_BEAM_SPLITTER = r"dbs"
    MODULATOR = r"mod"
    TEM = r"tem"
    AMPLITUDE_DETECTOR = r"ad"
    BEAM_SPLITTER = r"bs[1-2]?"
    MIRROR = r"m[1-2]?"
    LASER = r"l"
    SPACE = r"s"

    # Ignored patterns.
    ignore = "[ \t]"
    ignore_ftblock = "%%%"
    ignore_comment = "#.*"

    def __init__(self):
        self.reset()

    def reset(self):
        self.errors = []

    @_(r"\n+")
    def ignore_newline(self, t):
        self.lineno += t.value.count('\n')

    def error(self, t):
        command = t.value.split("\n")[0].split(" ")[0]
        self.errors.append((f"Command '{command}' unrecognised", self.lineno, self.index))
        self.index += len(command)
        return t

    def eof(self, t):
        print("EOF")

    # Command type tokens.
    def AMPLITUDE_DETECTOR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def ATTRIBUTE(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def BEAM_SPLITTER(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def CAVITY(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def CONSTANT(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def DIRECTIONAL_BEAM_SPLITTER(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def SOURCE_FREQUENCY(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def FTBLOCK_START(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def FTBLOCK_END(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def FUNCTION(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def FSIG(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def GAUSS(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def ISOLATOR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def LASER(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def LENS(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def LOCK(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def MASK(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def MAXTEM(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def MIRROR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def MODULATOR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def NOPLOT(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def NOXAXIS(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def PHASE(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def POWER_DETECTOR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def PUT(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def QUANTUM_NOISE_DETECTOR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def QUANTUM_SHOT_NOISE_DETECTOR(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def SPACE(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def TEM(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def VARIABLE(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def XAXIS(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def YAXIS(self, t):
        self.push_state(_KatComponentLEX)
        return t


class _KatComponentLEX(Lexer):
    """Kat file lexer, component state."""

    tokens = {"FUNCTIONSTRING", "NUMBER", "STRING"}
    # Top level tokens.
    FUNCTIONSTRING = r"=[^=\n]+"

    ignore = ' \t'
    ignore_comment = r"\#.*"

    @_(r"\n+")
    def ignore_newline(self, t):
        self.lineno += t.value.count('\n')
        self.pop_state()

    @_(r"[$0-9\-+*(][a-zA-Z0-9_\-+*$().]+")
    def NUM_PARAM(self, t):
        if "$" not in t.value:
            # This is a number
            t.type = "NUMBER"
            return self.NUMBER(t)
        t.type = "NUMBER"
        return t

    # Number token including scientific notation, float,
    # or +/- inf (all states). Alternatively, any string starting with $
    @_(r"[+-]?inf",
       r"[+-]?(\d+\.\d*|\d*\.\d+|\d+)([eE]-?\d*\.?\d*)?([pnumkMGT])?",
       r"\$[.\s]+")
    def NUMBER(self, t):
        if t.value.startswith("$"):
            return t
        if re.match(".*[pnumkMGT]$", t.value):
            t.value = t.value.replace("p", "e-12")
            t.value = t.value.replace("n", "e-9")
            t.value = t.value.replace("u", "e-6")
            t.value = t.value.replace("m", "e-3")
            t.value = t.value.replace("k", "e3")
            t.value = t.value.replace("M", "e6")
            t.value = t.value.replace("G", "e9")
            t.value = t.value.replace("T", "e12")
        if "j" in t.value:
            t.value = complex(t.value)
        else:
            t.value = float(t.value)
            if t.value.is_integer():
                t.value = int(t.value)
        return t

    @_(r"[a-zA-Z_][a-zA-Z0-9_:]*\*?", "inf")
    def STRING(self, t):
        if t.value == "inf":
            t.type = "NUMBER"
            return self.NUMBER(t)
        return t

    def error(self, t):
        line = t.value.split("\n")[0].split(" ")[0]
        self.errors.append((f"Illegal character '{t.value[0]}'", self.lineno, self.index))
        self.index += len(line)
        return t


class _KatYACC(Parser):
    """Kat file parser."""

    tokens = set.union(_KatLEX.tokens, _KatComponentLEX.tokens)

    def __init__(self):
        self.reset()

    def reset(self):
        """
        Delete all parsed code, resetting the parser to a newly constructed
        state.
        """
        self.noxaxis = False
        self.block = None
        self.blocks = OrderedDict()
        self.blocks[self.block] = self._default_components()
        self.errors = []

    def _default_components(self):
        return {
                # Default simulation components.
                "lasers": [],
                "spaces": [],
                "mirrors": [],
                "beam_splitters": [],
                "directional_beam_splitters": [],
                "isolators": [],
                "modulators": [],
                "lenses": [],
                "amplitude_detectors": [],
                "power_detectors": [],
                "quantum_noise_detectors": [],
                "cavities": [],
                # Non-component commands
                "frequencies": [],
                "constants": {},
                "attributes": {},
                "variables": {},
                "functions": {},
                "fsigs": [],
                "gauss": [],
                "locks": [],
                "puts": [],
                "maxtem": None,
                "phase" : 3,
                "tems": [],
                "masks": [],
                "noplots": [],
                "xaxis": [],
                "yaxis": [],
                }

    @_("instruction", "statement instruction")
    def statement(self, p):
        pass

    # List of one or more numbers
    @_("NUMBER", "number_list NUMBER")
    def number_list(self, p):
        if len(p) == 1:
            return [p.NUMBER]
        else:
            p.number_list.append(p.NUMBER)
            return p.number_list

    # List of attribute-value pairs
    @_("STRING NUMBER", "attribute_list STRING NUMBER")
    def attribute_list(self, p):
        if len(p) == 2:
            return [(p.STRING, p.NUMBER)]
        else:
            p.attribute_list.append((p.STRING, p.NUMBER))
            return p.attribute_list

    # Frequencies can either be a number or a combination of source frequencies
    @_("NUMBER")
    def freq_num(self, p):
        return p[0]

    @_("FTBLOCK_START STRING")
    def instruction(self, p):
        if self.block is not None:
            LOGGER.warn(f"Already in FTblock {self.block}")
        if p.STRING in self.blocks:
            LOGGER.warn(f"Duplicate FTblock {p.STRING}")
        self.block = p.STRING
        self.blocks[self.block] = self._default_components()

    @_("FTBLOCK_END STRING")
    def instruction(self, p):
        if (self.block != p.STRING):
            message = (f"Invalid command 'FTend {p.STRING}': currently in "
                       f"FTblock '{self.block}'")
            self.errors.append((message, p.lineno, p.index))
        self.block = None

    @_("SOURCE_FREQUENCY STRING NUMBER")
    def instruction(self, p):
        params = ["name", "f"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["frequencies"].append(dict(zip(params, values)))

    @_("LASER STRING NUMBER freq_num optnum STRING")
    def instruction(self, p):
        # Phase not specified.
        if p.optnum is None:
            p.optnum = 0

        params = ["name", "P", "f", "phase", "node"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["lasers"].append(dict(zip(params, values)))

    @_("SPACE STRING NUMBER optnum STRING STRING")
    def instruction(self, p):
        params = ["name", "L", "n", "node1", "node2"]
        values = [p[i] for i in range(1, len(p))]

        if values[2] is None:
            # Index of refraction not specified.
            # TODO:phil: should we be specifying 1 as the default here, or
            # checking for None in the space constructor later?
            values[2] = 1

        block = self.block
        self.blocks[block]["spaces"].append(dict(zip(params, values)))

    @_("MIRROR STRING NUMBER NUMBER NUMBER STRING STRING")
    def instruction(self, p):
        params = ["name", "R", "T", "L", "phi", "node1", "node2"]
        values = [p[i] for i in range(1, len(p))]

        if p[0] == "m":
            # R / T
            values.insert(3, None)
        elif p[0] == "m1":
            # T / Loss.
            values.insert(1, None)
        elif p[0] == "m2":
            # R / Loss.
            values.insert(2, None)

        block = self.block
        self.blocks[block]["mirrors"].append(dict(zip(params, values)))

    @_("BEAM_SPLITTER STRING NUMBER NUMBER NUMBER NUMBER STRING STRING STRING STRING")
    def instruction(self, p):
        params = ["name", "R", "T", "L", "phi", "alpha", "node1", "node2", "node3", "node4"]
        values = [p[i] for i in range(1, len(p))]

        if p[0] == "bs":
            # R / T
            values.insert(3, None)
        elif p[0] == "bs1":
            # T / Loss.
            values.insert(1, None)
        elif p[0] == "bs2":
            # R / Loss.
            values.insert(2, None)

        block = self.block
        self.blocks[block]["beam_splitters"].append(dict(zip(params, values)))

    @_("DIRECTIONAL_BEAM_SPLITTER STRING STRING STRING STRING STRING")
    def instruction(self, p):
        params = ["name", "node1", "node2", "node3", "node4"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["directional_beam_splitters"].append(dict(zip(params, values)))

    @_("ISOLATOR STRING NUMBER STRING STRING")
    def instruction(self, p):
        params = ["name", "S", "node1", "node2"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["isolators"].append(dict(zip(params, values)))

    @_("MODULATOR STRING freq_num NUMBER NUMBER STRING optnum STRING STRING")
    def instruction(self, p):
        params = ["name", "f", "midx", "order", "type", "phase", "node1", "node2"]
        values = [p[i] for i in range(1, len(p))]

        # TODO:phil: is no phase actually the same as 0 phase?
        if values[5] is None:
            values[5] = 0

        block = self.block
        self.blocks[block]["modulators"].append(dict(zip(params, values)))

    @_("LENS STRING NUMBER STRING STRING")
    def instruction(self, p):
        params = ["name", "f", "node1", "node2"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["lenses"].append(dict(zip(params, values)))

    @_("AMPLITUDE_DETECTOR STRING NUMBER NUMBER freq_num STRING",
       "AMPLITUDE_DETECTOR STRING freq_num STRING")
    def instruction(self, p):
        params = ["name", "n", "m", "f", "node"]
        values = [p[i] for i in range(1, len(p))]

        if len(values) == 3:
            # Mode numbers not specified.
            values.insert(1, None)
            values.insert(2, None)

        block = self.block
        self.blocks[block]["amplitude_detectors"].append(dict(zip(params, values)))

    @_("POWER_DETECTOR STRING STRING",
       "POWER_DETECTOR STRING number_list STRING")
    def instruction(self, p):
        # Parameters shared by all photodetectors.
        params = ["name", "node"]
        values = [p[i] for i in range(1, len(p))]

        ps = ["f{}", "phase{}"]
        if len(values) == 3:
            for n in range(len(values[1])):
                params.insert(n + 1, ps[n % 2].format(n // 2))
            values = [values[0], *values[1], values[2]]

        block = self.block
        self.blocks[block]["power_detectors"].append(dict(zip(params, values)))

    @_("QUANTUM_NOISE_DETECTOR STRING STRING",
       "QUANTUM_NOISE_DETECTOR STRING number_list STRING")
    def instruction(self, p):
        # Parameters shared by all quantum noise detectors.
        params = ["name", "node"]
        values = [p[i] for i in range(1, len(p))]

        ps = ["f{}", "phase{}"]
        if len(values) == 3:
            for n in range(len(values[1])):
                params.insert(n + 1, ps[n % 2].format(n // 2))
            values = [values[0], *values[1], values[2]]

        block = self.block
        self.blocks[block]["quantum_noise_detectors"].append(OrderedDict(zip(params, values)))

    @_("QUANTUM_SHOT_NOISE_DETECTOR STRING STRING",
       "QUANTUM_SHOT_NOISE_DETECTOR STRING number_list STRING")
    def instruction(self, p):
        # Parameters shared by all quantum noise detectors.
        params = ["name", "node"]
        values = [p[i] for i in range(1, len(p))]

        ps = ["f{}", "phase{}"]
        if len(values) == 3:
            for n in range(len(values[1])):
                params.insert(n + 1, ps[n % 2].format(n // 2))
            values = [values[0], *values[1], values[2]]

        params.insert(-1, "shot_only")
        values.insert(-1, True)

        block = self.block
        self.blocks[block]["quantum_noise_detectors"].append(OrderedDict(zip(params, values)))

    @_("CAVITY STRING STRING STRING STRING STRING")
    def instruction(self, p):
        params = ["name", "component1", "node1", "component2", "node2"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["cavities"].append(dict(zip(params, values)))

    @_("CONSTANT STRING NUMBER",
       "CONSTANT STRING STRING")
    def instruction(self, p):
        name = p[1]
        val = p[2]

        block = self.block
        self.blocks[block]["constants"]["$" + name] = val

    @_("ATTRIBUTE STRING attribute_list")
    def instruction(self, p):
        comp = p[1]
        attrs = p[2]

        block = self.block
        if comp in self.blocks[block]["attributes"]:
            self.blocks[block]["attributes"][comp].extend(attrs)
        else:
            self.blocks[block]["attributes"][comp] = attrs

    @_("VARIABLE STRING NUMBER",
       "VARIABLE STRING STRING")
    def instruction(self, p):
        name = p[1]
        value = p[2]

        block = self.block
        self.blocks[block]["variables"][name] = value

    @_("FUNCTION STRING FUNCTIONSTRING")
    def instruction(self, p):
        name = p[1]
        function_string = p[2]

        # Trim the starting "=" from the function string, and any whitespace
        block = self.block
        self.blocks[block]["functions"][name] = function_string[1:].strip()

    @_("FSIG STRING STRING optstr freq_num NUMBER optnum",
       "FSIG STRING NUMBER")
    def instruction(self, p):
        if len(p) == 3:
            params = ["name", "f"]
            values = [p[i] for i in range(1, len(p))]

            block = self.block
            self.blocks[block]["fsigs"].append(dict(zip(params, values)))
        else:
            if p.optnum is None:
                # Set default amplitude to 1
                p.optnum = 1
            params = ["name", "component", "mod_type", "f", "phase", "amp"]
            values = [p[i] for i in range(1, len(p))]

            block = self.block
            self.blocks[block]["fsigs"].append(dict(zip(params, values)))

    @_("LOCK STRING NUMBER NUMBER NUMBER")
    def instruction(self, p):
        params = ["name", "variable", "gain", "accuracy", "starred"]
        values = [p[i] for i in range(1, len(p))]

        if p[0].endswith("*"):
            values.append(True)
        else:
            values.append(False)
        block = self.block
        self.blocks[block]["locks"].append(dict(zip(params, values)))

    @_("GAUSS STRING STRING STRING NUMBER NUMBER",
       "GAUSS STRING STRING STRING NUMBER NUMBER NUMBER NUMBER")
    def instruction(self, p):
        values = [p[i] for i in range(1, len(p))]

        if p[0].endswith("*"):
            params = ["name", "component", "node", "qx_re", "qx_im", "qy_re", "qy_im"]
        else:
            params = ["name", "component", "node", "w0x", "zx", "w0y", "zy"]

        block = self.block
        self.blocks[block]["gauss"].append(dict(zip(params, values)))

    @_("PUT STRING STRING NUMBER")
    def instruction(self, p):
        params = ["component", "parameter", "variable", "add"]
        values = [p[i] for i in range(1, len(p))]

        if p[0].endswith("*"):
            values.append(True)
        else:
            values.append(False)
        block = self.block
        self.blocks[block]["puts"].append(dict(zip(params, values)))

    @_("MAXTEM NUMBER")
    def instruction(self, p):
        block = self.block
        self.blocks[block]["maxtem"] = p[1]

    @_("PHASE NUMBER")
    def instruction(self, p):
        block = self.block
        self.blocks[block]["phase"] = p[1]

    @_("TEM STRING NUMBER NUMBER NUMBER NUMBER")
    def instruction(self, p):
        params = ["component", "n", "m", "factor", "phase"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["tems"].append(dict(zip(params, values)))

    @_("MASK STRING NUMBER NUMBER NUMBER")
    def instruction(self, p):
        params = ["detector", "n", "m", "factor"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["masks"].append(dict(zip(params, values)))

    @_("NOPLOT STRING")
    def instruction(self, p):
        params = ["output"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["noplots"].append(dict(zip(params, values)))

    @_("NOXAXIS")
    def instruction(self, p):
        self.noxaxis = True

    @_("XAXIS STRING STRING STRING NUMBER NUMBER NUMBER")
    def instruction(self, p):
        params = ["component", "parameter", "scale", "min", "max", "steps"]
        values = [p[i] for i in range(1, len(p))]

        block = self.block
        self.blocks[block]["xaxis"].append(dict(zip(params, values)))

    # TODO: placing the optstr before the STRING like in Finesse 2 causes a
    # shift/reduce conflict - can we solve this?
    @_("YAXIS STRING optstr")
    def instruction(self, p):
        params = ["scale", "axes"]
        values = [p[i] for i in range(1, len(p))]

        if values[1] is None:
            values.insert(0, "lin")

        block = self.block
        self.blocks[block]["yaxis"].append(dict(zip(params, values)))

    def error(self, p):
        print(dir(self))
        if p is None:
            msg = "Unexpected end of file"
            self.errors.append((msg, None, None))
            return
        elif p.type == "ERROR":
            return
        msg = f"got unexpected token {p.value} of type {p.type}"
        self.errors.append((msg, p.lineno, p.index))

    @_('')
    def empty(self, p):
        pass

    @_('STRING')
    def optstr(self, p):
        return p.STRING

    @_('empty')
    def optstr(self, p):
        pass

    @_('NUMBER')
    def optnum(self, p):
        return p.NUMBER

    @_('empty')
    def optnum(self, p):
        pass


class KatParserError(ValueError):
    """Kat file parser error"""
    def __init__(self, errors, text, **kwargs):
        message = "\n"
        for error in errors:
            lineno = error[1]
            idx = error[2]
            if lineno is None:
                # There is no lineno, as this was an end-of-file error,
                # so assume error was on last non-empty lin
                line = re.findall(r"[^\s]", text)[-1]
                pos = len(text) - 1
            else:
                line = text.split("\n")[lineno - 1]
                pos = self.find_column(text, idx)
            expected = ""
            for pattern, exp in self.expected.items():
                if exp is not None and re.match(pattern, line) is not None:
                    expected = f", expected '{exp}'"
                    break
            message += f"{lineno}:{pos}: " + error[0] + expected + "\n"
            message += line + "\n"
            message += " " * (pos - 1) + "^\n"

        super().__init__(message.rstrip("\n"), **kwargs)

    @staticmethod
    def find_column(text, index):
        last_cr = text.rfind('\n', 0, index)
        if last_cr < 0:
            last_cr = 0
        column = (index - last_cr)
        return column

    expected = {
        "ad":       "ad name [n m] f node[*]",
        "attr":     "attr component parameter value",
        "bs2":      "bs2 name R L phi alpha node1 node2 node3 node4",
        "bs1":      "bs1 name T L phi alpha node1 node2 node3 node4",
        "bs":       "bs name R T phi alpha node1 node2 node3 node4",
        "cav":      "cav name component1 node component2 node",
        "const":    "const name value",
        "dbs":      "dbs name node1 node2 node3 node4",
        "freq":     None,
        "FTblock":  "FTblock name",
        "FTend":    "FTend name",
        "func":     "func name = function-string",
        "fsig":     ("fsig name component [type] f phase [amp]", "fsig name f"),
        "gauss*":   "gauss* name component node q [qy]",
        "gauss":    "gauss name component node w0 z [wy0 zy]",
        "isol":     "isol name S [Loss] node1 node2 [node3]",
        "lock":     "lock name function/set gain accuracy [offset]",
        "lens":     "lens name f node1 node2",
        "l":        "l name I f [phase] node",
        "maxtem":   "maxtem order",
        "m2":       "m2 name R L phi node1 node2",
        "m1":       "m1 name T L phi node1 node2",
        "m":        "m name R T phi node1 node2",
        "mod":      "mod name f midx order am/pm/yaw/pitch node1 node2",
        "noplot":   "noplot output",
        "noxaxis":  None,
        "pd":       "pdtype detector type-name",
        "phase":    "phase 0-7",
        "put":      "put component parameter function/set/axis",
        "qnoised":  "qnoised name num_demods f1 phase1 [f2 phase2 [...]] node[*]",
        "qshot":    "",
        "s":        "s name L [n] node1 node2",
        "tem":      "tem input n m factor phase",
        "variable": "variable name value",
        "xaxis":    "xaxis component parameter lin/log min max steps",
        "yaxis":    "yaxis [lin/log] abs:deg/db:deg/re:im/abs/db/deg",
    }
