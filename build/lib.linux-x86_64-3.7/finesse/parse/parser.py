import logging
import re
import operator
import numpy as np

from sly import Lexer, Parser

import finesse.analysis as analysis
import finesse.components as components
import finesse.detectors as detectors
import finesse.gaussian as gaussian
from finesse.model import Model
from finesse.kat import Kat

LOGGER = logging.getLogger(__name__)


def maxtem(model, value):
    model.maxtem = value


def gauss(model, node, **kwargs):
    if "q" in kwargs:
        if len(kwargs) > 1:
            raise ValueError("Cannot specify both q and another parameter")
        node.q = kwargs["q"]
    elif len({"qx", "qy"} & kwargs.keys()) > 0:
        if len({"qx", "qy"} & kwargs.keys()) != 2:
            raise ValueError("Must specify both qx and qy, or just q")
        elif len(kwargs != 2):
            raise ValueError("Cannot specify qx, qy and another parameter")
        node.q = (kwargs["qx"], kwargs["qy"])
    elif len({"w0", "z"} & kwargs.keys()) > 0:
        if len({"w0", "z"} & kwargs.keys()) != 2:
            raise ValueError("Must specify both w0 and z")
        elif len(kwargs != 2):
            raise ValueError("Cannot specify w0, z and another parameter")
        node.q = gaussian.BeamParam(**kwargs)
    elif len({"w0x", "w0y", "zx", "zy"} & kwargs.keys()) > 0:
        if len({"w0x", "w0y", "zx", "zy"} & kwargs.keys()) != 4:
            raise ValueError("Must specify all of w0[xy] and z[xy]")
        elif len(kwargs != 4):
            raise ValueError("Cannot specify w0[xy], z[xy] and another parameter")
        node.q = (gaussian.BeamParam(w0=kwargs["w0x"], z=kwargs["zx"]),
                  gaussian.BeamParam(w0=kwargs["w0y"], z=kwargs["zy"]),)
    else:
        raise ValueError("Invalid arguments to gauss")


class KatParser:
    """Kat file lexer, parser and builder."""

    component_constructors = [
        {
            "laser": components.Laser,
            "l": components.Laser,
            "mirror": components.Mirror,
            "m": components.Mirror,
            "beam_splitter": components.Beamsplitter,
            "bs": components.Beamsplitter,
            "isolator": components.Isolator,
            "isol": components.Isolator,
            "modulator": components.Modulator,
            "mod": components.Modulator,
            "lens": components.Lens,
        },
        {
            "space": components.Space,
            "s": components.Space,
            "cavity": components.Cavity,
            "cav": components.Cavity,
            "amplitude_detector": detectors.AmplitudeDetector,
            "ad": detectors.AmplitudeDetector,
            "power_detector": detectors.PowerDetector,
            "pd": detectors.PowerDetector,
            "quantum_noise_detector": detectors.QuantumNoiseDetector,
            "qd": detectors.QuantumNoiseDetector,
            "quantum_shot_noise_detector": None,
            "qshot": None,
        },
    ]

    commands = {
        "maxtem": maxtem,
        "gauss": gauss
    }

    analysis_constructors = {
        "xaxis": analysis.xaxis,
    }

    def __init__(self):
        self.lexer = _KatLEX()
        self.parser = _KatYACC()
        self.reset()

    def reset(self):
        """
        Delete all parsed code, resetting the parser to a newly constructed
        state.
        """
        self.parser.reset()
        self.lexer.reset()

    def parse(self, text=None, path=None):
        """
        Parses a kat file into an intermediate internal representation, which
        can then be built by :meth:`.KatParser.build`. One of either `text` or
        `path` must be provided.

        Parameters
        ----------
        text : str, optional
            String containing the kat code to be parsed.

        path : str, optional
            Path of a file containing the kat code to be parsed.

        Raises
        ------
        ValueError
            If either both or neither of `text` and `path` are specified.

        KatParserError
            If an error occurs during parsing.
        """
        if text is None and path is None:
            raise ValueError("must provide either text or a path")

        if path is not None:
            if text is not None:
                raise ValueError("cannot specify both text and a file to parse")

            with open(path, "r") as obj:
                text = obj.read()

            source = path
        else:
            source = "from user input"

        # As we have no way of detecting EOF and calling pop_state() from
        # within the component lexer, we must ensure that all files end in a
        # newline
        text = f"{text}\n"

        tokens = self.lexer.tokenize(text)
        self.parser.parse(tokens)
        errors = sorted(self.lexer.errors + self.parser.errors,
                        key=lambda tup: tup[1])
        if len(errors) > 0:
            raise KatParserError(errors, text)

        LOGGER.info("Parsed %s", source)

    def build(self):
        """
        Constructs a :class:`.Model` from parsed kat code.

        Returns
        -------
        Model
            The constructed :class:`.Model`
        """
        def resolve_ports(model, args, kwargs):
            for i, v in enumerate(args):
                if isinstance(v, str):
                    port = model
                    for attr in v.split("."):
                        port = getattr(port, attr)
                    args[i] = port
            for k, v in kwargs.items():
                if isinstance(v, str):
                    port = model
                    for attr in v.split("."):
                        port = getattr(port, attr)
                    kwargs[k] = port

        def resolve_parameters(model, args, kwargs):
            for i, v in enumerate(args):
                if callable(v):
                    args[i] = v(model)
                    try:
                        args[i] = args[i].parameter
                    except AttributeError:
                        pass
            for k, v in kwargs.items():
                if callable(v):
                    kwargs[k] = v(model).parameter
                    try:
                        kwargs[k] = kwargs[k].parameter
                    except AttributeError:
                        pass

        model = Model()
        for constructor_pass in self.component_constructors:
            for component in self.parser.components:
                if component[0] not in constructor_pass:
                    continue
                constructor = constructor_pass[component[0]]
                name = component[1]
                args = component[2][0] or []
                kwargs = component[2][1] or {}

                resolve_ports(model, args, kwargs)
                model.add(constructor(name, *args, **kwargs))

        for command in self.parser.commands:
            fn = self.commands[command[0]]
            args = command[1][0] or []
            kwargs = command[1][1] or {}

            resolve_ports(model, args, kwargs)
            fn(model, *args, **kwargs)

        analyses = []

        for _analysis in self.parser.analyses:
            constructor = self.analysis_constructors[_analysis[0]]
            args = _analysis[1][0] or []
            kwargs = _analysis[1][1] or {}
            resolve_parameters(model, args, kwargs)
            analyses.append(constructor(*args, **kwargs))

        if len(analyses) == 0:
            analyses.append(analysis.noxaxis(model))

        for comp in model.elements.values():
            for param in comp._params:
                param.resolve()

        return Kat(model, analyses)


class _KatLEX(Lexer):
    """Kat lexer, default state."""

    tokens = {"ANALYSIS", "COMMAND", "COMPONENT", "END"}

    # Ignored patterns.
    ignore = " \t"
    ignore_ftblock = "%%%"
    ignore_comment = "#.*"

    def __init__(self):
        self.reset()

    def reset(self):
        self.errors = []
        self.nesting = []

    @_(r"\n+")
    def ignore_newline(self, t):
        self.lineno += t.value.count('\n')

    @_("xaxis")
    def ANALYSIS(self, t):
        self.push_state(_KatComponentLEX)
        return t

    @_("maxtem", "gauss")
    def COMMAND(self, t):
        self.push_state(_KatComponentLEX)
        return t

    @_("quantum_shot_noise_detector", "directional_beam_splitter",
       "quantum_noise_detector", "amplitude_detector", "signal_generator",
       "power_detector", "beam_splitter", "attribute", "modulator", "constant",
       "function", "isolator", "variable", "cavity", "maxtem", "mirror",
       "const", "laser", "qshot", "space", "xaxis", "fsig", "func", "isol",
       "lens", "lock", "mask", "cav", "dbs", "mod", "put", "tem", "var", "ad",
       "bs", "pd", "qd", "sg", "l", "m", "s")
    def COMPONENT(self, t):
        self.push_state(_KatComponentLEX)
        return t

    def error(self, t):
        command = t.value.split("\n")[0].split(" ")[0]
        self.errors.append((f"Command '{command}' unrecognised", self.lineno, self.index))
        self.index += len(command)
        return t

class _KatComponentLEX(Lexer):
    tokens = {"PLUS", "MINUS", "TIMES", "DIVIDE", "FUNCTION_NAME",
              "PARAMETER", "NUMBER", "STRING", "LPAREN", "RPAREN", "LBRACKET",
              "RBRACKET"}
    literals = {'='}

    FUNCTION_NAME = "cos|sin"
    PARAMETER = "[$&][a-zA-Z_][a-zA-Z0-9_.]*"
    PLUS = r"\+"
    MINUS = r"-"
    TIMES = r"\*"
    DIVIDE = r"/"

    # Ignored patterns.
    ignore = " \t"
    ignore_ftblock = "%%%"
    ignore_comment = "#.*"

    def reset(self):
        self.pop_state()
        self.reset()

    # Number token including scientific notation, float,
    # or +/- inf (all states).
    @_(r"[+-]?inf",
       r"[+-]?(\d+\.\d*|\d*\.\d+|\d+)([eE]-?\d*\.?\d*)?j?([pnumkMGT])?")
    def NUMBER(self, t):
        if t.value.startswith("$"):
            return t
        if re.match(".*[pnumkMGT]$", t.value):
            t.value = t.value.replace("p", "e-12")
            t.value = t.value.replace("n", "e-9")
            t.value = t.value.replace("u", "e-6")
            t.value = t.value.replace("m", "e-3")
            t.value = t.value.replace("k", "e3")
            t.value = t.value.replace("M", "e6")
            t.value = t.value.replace("G", "e9")
            t.value = t.value.replace("T", "e12")
        if "j" in t.value:
            t.value = complex(t.value)
        else:
            t.value = float(t.value)
            if t.value.is_integer():
                t.value = int(t.value)
        return t

    @_(r"[a-zA-Z_][a-zA-Z0-9_.]*\*?", "inf")
    def STRING(self, t):
        if t.value == "inf":
            t.type = "NUMBER"
            return self.NUMBER(t)
        return t

    @_("\n")
    def END(self, t):
        self.lineno += t.value.count('\n')
        if len(self.nesting) == 0:
            self.pop_state()
            return t

    @_(r'\(')
    def LPAREN(self, t):
        self.nesting.append(('(', self.lineno))
        self.push_state(_KatComponentLEX)
        return t

    @_(r'\)')
    def RPAREN(self, t):
        try:
            assert(self.nesting.pop()[0] == "(")
            self.pop_state()
        except (IndexError, AssertionError):
            self.errors.append((f"Extraneous ')'", self.lineno, self.index))
        return t

    @_(r'\[')
    def LBRACKET(self, t):
        self.nesting.append(('[', self.lineno))
        return t

    @_(r'\]')
    def RBRACKET(self, t):
        try:
            self.nesting.pop()
        except IndexError:
            self.errors.append((f"Extraneous ']'", self.lineno, self.index))
        return t


class _KatYACC(Parser):
    # Silence shift/reduce conflict warning
    log = logging.getLogger()
    log.setLevel(logging.ERROR)

    tokens = set.union(_KatLEX.tokens, _KatComponentLEX.tokens)

    precedence = (
       ('left', "RPAREN"),
       ('left', "PLUS", "MINUS"),
       ('left', "TIMES", "DIVIDE"),
       ('right', "UMINUS"),
    )

    _operators = {
            "+": operator.add,
            "-": operator.sub,
            "*": operator.mul,
            "/": operator.truediv,
            }

    _functions = {
            "cos": np.cos,
            "sin": np.sin,
            }

    def __init__(self):
        self.reset()

    def reset(self):
        self.components = []
        self.commands = []
        self.analyses = []
        self.errors = []

    @_("instruction END", "statement instruction END")
    def statement(self, p):
        pass

    @_("expr",
       "STRING",
       "tags")
    def value(self, p):
        return p[0]

    @_("expr PLUS expr",
       "expr MINUS expr",
       "expr TIMES expr",
       "expr DIVIDE expr",)
    def expr(self, p):
        operation = self._operators[p[1]]
        lhs = p[0]
        rhs = p[2]
        if not callable(lhs) and not callable(rhs):
            return operation(lhs, rhs)
        if not callable(lhs):
            lhs_fn = lambda _: lhs
        else:
            lhs_fn = lhs
        if not callable(rhs):
            rhs_fn = lambda _: rhs
        else:
            rhs_fn = rhs
        return lambda model: operation(lhs_fn(model), rhs_fn(model))

    @_("MINUS expr %prec UMINUS")
    def expr(self, p):
        rhs = p[1]
        if not callable(rhs):
            return -rhs
        return lambda model: -rhs(model)

    @_("LPAREN expr RPAREN")
    def expr(self, p):
        return p[1]

    @_("NUMBER",
       "function")
    def expr(self, p):
        return p[0]

    @_("PARAMETER")
    def expr(self, p):
        string = p[0]
        def fn(model, string):
            comp = model
            for attr in string.lstrip("$&").split("."):
                comp = getattr(comp, attr)
            if string.startswith('&'):
                return comp.ref
            return comp.eval()
        return lambda model: fn(model, string)

    @_("FUNCTION_NAME LPAREN expr RPAREN")
    def function(self, p):
        expr = p.expr
        fn = self._functions[p[0]]
        if not callable(expr):
            return fn(expr)
        return lambda model: fn(expr(model))

    @_("value", "value_list value")
    def value_list(self, p):
        if len(p) == 1:
            return [p[0]]
        p[0].append(p[1])
        return p[0]

    @_("STRING '=' value")
    def key_value_pair(self, p):
        return {p.STRING: p.value}

    @_("key_value_pair", "key_value_list key_value_pair")
    def key_value_list(self, p):
        if len(p) == 1:
            return p[0]
        p[0].update(p[1])
        return p[0]

    @_("value_list",
       "value_list key_value_list",
       "key_value_list")
    def params(self, p):
        try:
            vlist = p.value_list
        except KeyError:
            vlist = None
        try:
            kvlist = p.key_value_list
        except KeyError:
            kvlist = None
        return (vlist, kvlist)

    @_("STRING", "tag_list STRING")
    def tag_list(self, p):
        if len(p) == 1:
            return [p[0]]
        p[0].append(p[1])
        return p[0]

    @_("LBRACKET tag_list RBRACKET")
    def tags(self, p):
        return p.tag_list

    @_("COMPONENT STRING params",
       "COMPONENT STRING LPAREN params RPAREN")
    def instruction(self, p):
        self.components.append((p.COMPONENT, p.STRING, p.params))

    @_("COMMAND params",
       "COMMAND LPAREN params RPAREN")
    def instruction(self, p):
        self.commands.append((p.COMMAND, p.params))

    @_("ANALYSIS params",
       "ANALYSIS LPAREN params RPAREN")
    def instruction(self, p):
        self.analyses.append((p.ANALYSIS, p.params))

    def error(self, p):
        if p is None:
            msg = "Unexpected end of file"
            self.errors.append((msg, None, None))
            return
        elif p.type == "ERROR":
            return
        msg = f"got unexpected token {p.value} of type {p.type}"
        self.errors.append((msg, p.lineno, p.index))


class KatParserError(ValueError):
    """Kat file parser error"""
    def __init__(self, errors, text, **kwargs):
        message = "\n"
        for error in errors:
            lineno = error[1]
            idx = error[2]
            if lineno is None:
                # There is no lineno, as this was an end-of-file error,
                # so assume error was on last non-empty line
                line = re.findall(r"[^\s]", text)[-1]
                pos = len(text) - 1
            else:
                line = text.split("\n")[lineno - 1]
                pos = self.find_column(text, idx)
            expected = ""
            for pattern, exp in self.expected.items():
                if exp is not None and re.match(pattern, line) is not None:
                    expected = f", expected '{exp}'"
                    break
            message += f"{lineno}:{pos}: " + error[0] + expected + "\n"
            message += line + "\n"
            message += " " * (pos - 1) + "^\n"

        super().__init__(message.rstrip("\n"), **kwargs)

    @staticmethod
    def find_column(text, index):
        last_cr = text.rfind('\n', 0, index)
        if last_cr < 0:
            last_cr = 0
        column = (index - last_cr)
        return column

    expected = {
    }
