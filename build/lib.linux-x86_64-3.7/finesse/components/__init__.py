"""
The ``components`` module contains all the component type of an interferometer
configuration including the general objects required to connect them and register
node connections.

These include not only optical components such as mirrors and lasers but also electrical
and mechanical component types found in physical interferometers.

Listed below are all the sub-modules of the ``components`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: components/
    {contents}

--------

The tables below list the available classes and functions exposed by the ``components``
sub-module of the Finesse package.

{toc}
"""

from finesse.components.general import Connector, FrequencyGenerator, Surface, Variable
from finesse.components.node import Node, NodeType, NodeDirection, Port
from finesse.components.beamsplitter import Beamsplitter
from finesse.components.cavity import Cavity
from finesse.components.isolator import Isolator
from finesse.components.laser import Laser
from finesse.components.lens import Lens
from finesse.components.mirror import Mirror
from finesse.components.modulator import Modulator
from finesse.components.nothing import Nothing
from finesse.components.photodiode import Photodiode
from finesse.components.signal import SignalGenerator
from finesse.components.space import Space
from finesse.components.wire import Wire
from finesse.components.electronics import FilterZPK
from finesse.components.mechanical import PZT, Joint

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    def compile_toc(entries):
        toc = ''
        for section, objs in entries:
            toc += '\n\n.. rubric:: {}\n\n'.format(section)
            toc += '.. autosummary::\n\n'
            for obj in objs:
                toc += '\t~{}.{}\n'.format(obj.__module__, obj.__name__)
        return toc

    toc = (
        ('Base Components', (
            Connector, FrequencyGenerator,
        )),
        ('Beamsplitters', (
            Beamsplitter,
        )),
        ('Cavities', (
            Cavity,
        )),
        ('Isolators', (
            Isolator,
        )),
        ('Lasers', (
            Laser,
        )),
        ('Lenses', (
            Lens,
        )),
        ('Mirrors', (
            Mirror,
        )),
        ('Modulators', (
            Modulator,
        )),
        ('Photodiodes', (
            Photodiode,
        )),
        ('Ports and Nodes', (
            Port, Node,
        )),
        ('Signals', (
            SignalGenerator,
        )),
        ('Spaces', (
            Space, Nothing,
        )),
        ('Wires', (
            Wire,
        )),
    )

    __doc__ = __doc__.format(
        contents=_collect_submodules("components"),
        toc=compile_toc(toc)
    )
