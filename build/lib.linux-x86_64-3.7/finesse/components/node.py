"""
Objects for connecting and registering connections between components.
"""

import enum
import weakref
import finesse

from collections import OrderedDict
from copy import deepcopy
from finesse.exceptions import ComponentNotConnected
from finesse.freeze import canFreeze, Freezable
from finesse.gaussian import BeamParam
from finesse.utilities import check_name

class QSetMode(enum.Enum):
    """
    Enum describing how the q (beam-parameter) value
    of a :class:`.Node` instance has been set.
    """
    USER = 0
    USER_TRACED = 1
    AUTO = 2
    NOTSET = 3

class NodeType(enum.Enum):
    """
    Enum describing the physical connection type of a :class:`.Node`
    """
    OPTICAL = 0
    ELECTRICAL = 1
    MECHANICAL = 2


class NodeDirection(enum.Enum):
    """
    Enum describing the direction that information at a :class:`.Node`
    flows.

    This is largely a description to help understand how external information
    flows in and out of a component. Inside a component all nodes will
    couple to one another in more complex ways.

    Input nodes are those going into a component, whereas output
    describe those leaving. For example incident and reflected light fields.

    Bidrectional takes information either direction. For example a mechanical
    degree of freedom, external forces can be applied to it, or
    its motion can be coupled to some external system.
    """
    INPUT = 0
    OUTPUT = 1
    BIDIRECTIONAL = 2


class Port(Freezable):
    """A collection of all the nodes at a specific point/surface of
    a component.
    """
    def __init__(self, name, component, node_type):
        """Constructs a new instance of `Port` with the specified properties.

        Parameters
        ----------
        name : str
            Name of this node

        component : Sub-class of :class:`.Connector`
            The component that this node belongs to

        node_type : :class:`.NodeType`
            Physical node type
        """
        self._unfreeze()
        self.__component = weakref.ref(component)
        self.__name = check_name(name)
        self.__type = node_type
        self.__nodes = OrderedDict()
        self.__enabled = True
        self._freeze()

    def __deepcopy__(self, memo):
        new = object.__new__(type(self))
        memo[id(self)] = new
        new.__dict__.update(deepcopy(self.__dict__, memo))
        # Manually update the weakrefs to be correct
        new.__component = weakref.ref(memo[id(self.__component())])
        return new

    def _add_node(self, name, direction=None, node=None):
        """
        Adds a new node to this port. Once called the new node
        can be access with:

            obj.port.name

        Parameters
        ----------
        name : str
            Name of the new node
        """
        if name in self.__nodes:
            raise Exception("Node already added to this component")

        if node is None:
            if self.__type == NodeType.OPTICAL:
                if direction is None:
                    raise Exception("Node direction must be specified for optical nodes")
                node = OpticalNode(name, self, direction)
            elif self.__type == NodeType.MECHANICAL:
                node = MechanicalNode(name, self)
            elif self.__type == NodeType.ELECTRICAL:
                node = ElectricalNode(name, self)
            else:
                raise Exception("Unexpected node type")

        assert(node is not None)

        if node.type != self.type:
            raise Exception("Node and port type must be the same")

        self.__nodes[name] = node
        self._unfreeze()
        assert(not hasattr(self, name))
        setattr(self, name, node)
        self._freeze()

    def __repr__(self):
        return f"<Port {self.component.name}.{self.name} @ {hex(id(self))}>"

    @property
    def enabled(self):
        return self.__enabled

    @property
    def type(self):
        """:class:`.NodeType` of the port object.

        :getter: Returns the node-type of the port (read-only).
        """
        return self.__type

    @property
    def name(self):
        """Name of the port object.

        :getter: Returns the name of the port (read-only).
        """
        return self.__name

    @property
    def _model(self): return self.__component()._model

    @property
    def component(self):
        """The component which has ownership of this port.

        :getter: Returns the component that this port belongs to (read-only).
        """
        return self.__component()

    @property
    def is_connected(self):
        """Flag indicating whether the port is attached to another component.

        :getter: Returns true if this port is attached to another component (read-only).
        """
        return self.attached_to is not None

    @property
    def attached_to(self):
        """Component that the port is attached to.

        :getter: Returns the component this port is attached to, or returns None
                 if no such connected component exists (read-only).
        """
        if self.type == NodeType.OPTICAL:
            spaces = [_.space for _ in self.nodes]
            if all([spaces[0] == _ for _ in spaces]):
                return spaces[0]
            else:
                raise Exception("Nodes are somehow connected to different "
                                "spaces which should not happen")
        else:
            raise NotImplementedError()

    @property
    def space(self):
        """Space that the port is attached to. Equivalent to :attr:`.Port.attached_to`.

        :getter: Returns the space that this port is attached to (read-only).
        """
        if self.type != NodeType.OPTICAL:
            raise Exception("Port type is not optical, cannot retrieve attached space.")
        return self.attached_to

    @property
    def nodes(self):
        """Nodes associated with the port.

        :getter: Returns a tuple of the associated nodes at this port (read-only).
        """
        return tuple(self.__nodes.values())


class Node(object):
    """
    An object representation of a specific connection at a component. Mathematically
    a node represents a single equation in the interferometer matrix.

    A `Node` represents a connection point of a component and can only
    be owned by a single component instance - with weak references stored
    by the connected components.
    """
    def __init__(self, name, port, node_type, direction):
        """Constructs a new instance of `Node` with the specified properties.

        Parameters
        ----------
        name : str
            Name of this node

        component : :class:`.Port`
            The port that this node belongs to

        node_type : :class:`.NodeType`
            Physical node type
        """
        self.__port = weakref.ref(port)
        self.__component = weakref.ref(port.component)
        self.__name = check_name(name)
        self.__type = node_type
        self.__direction = direction
        self.__full_name = "{}.{}.{}".format(port.component.name, port.name, name)
        self.__port_name = "{}.{}".format(port.name, name)
        self.__tag_name = None

    def __deepcopy__(self, memo):
        new = object.__new__(type(self))
        memo[id(self)] = new
        new.__dict__.update(deepcopy(self.__dict__, memo))
        # Manually update the weakrefs to be correct
        new.__port = weakref.ref(memo[id(self.__port())])
        new.__component = weakref.ref(memo[id(self.__component())])
        return new

    def __repr__(self):
        if self.__type == NodeType.OPTICAL: cls    = "OpticalNode"
        if self.__type == NodeType.MECHANICAL: cls = "MechanicalNode"
        if self.__type == NodeType.ELECTRICAL: cls = "ElectricalNode"

        return f"<{self.__class__.__name__} {self.full_name} @ {hex(id(self))}>"

    @property
    def full_name(self):
        """
        :getter: Returns a full name of the node: {component name}.{port name}.{node name}
        """
        return self.__full_name

    @property
    def port_name(self):
        """
        :getter: Returns a shortened name of the node: {port name}.{node name}
        """
        return self.__port_name

    @property
    def type(self):
        """:class:`.NodeType` of the node object.

        :getter: Returns the node-type of the node (read-only).
        """
        return self.__type

    @property
    def direction(self):
        """
        :class:`.NodeDirection` of this node. This is largely a
        description to help understand how external information flow
        in and out of a component. Inside a component all nodes will
        couple to one another in more complex ways.

        Input nodes are those going into a component, whereas output
        describe those leaving. For example incident and reflected light fields.

        Bidrectional takes information either direction. For example a mechanical
        degree of freedom, external forces can be applied to it, or
        its motion can be coupled to some external system.

        :getter: Returns the directionality of the node (read-only).
        """
        return self.__direction

    @property
    def port(self):
        """:class:`.Port` this node is attached to.

        :getter: Returns the port of this node (read-only).
        """
        return self.__port()

    @property
    def name(self):
        """Name of the node object.

        :getter: Returns the name of the node (read-only).
        """
        return self.__name

    @property
    def tag(self):
        """Tagged name of the node object.

        :getter: Returns the tagged (user-defined) name of the node (read-only).
        """
        return self.__tag_name

    def _set_tag(self, tag):
        self.__tag_name = tag

    @property
    def _model(self): return self.__component()._model

    @property
    def component(self):
        """The component which has ownership of this node.

        :getter: Returns the component that this node belongs to (read-only).
        """
        return self.__component()

    def is_neighbour(self, node):
        """Checks if `node` is a connected by an edge to this node.

        Parameters
        ----------
        node : :class:`.Node`
            Node with which to check connection.

        Returns
        -------
        flag : bool
            True if `node` is connected to this node, False otherwise.
        """
        # if not associated with a model yet, check registered connections directly
        try:
            a = self._model.network.has_node(self.full_name)
            b = node.full_name in self._model.network.neighbors(self.full_name)
            return a and b
        except ComponentNotConnected:
            return (self.full_name, node.full_name) in self.__component()._registered_connections.values()
        ##if not self.has_model():
        ##    return (self, node) in self.__component()._registered_connections.values()
        ##return (self._model.network.has_node(self) and
        ##        node in self._model.network.neighbors(self))

class MechanicalNode(Node):
    """
    An object representation of a specific mechanical degree of freedom of a
    component.
    """
    def __init__(self, name, port):
        """Constructs a new instance of `MechanicalNode` with the specified properties.

        Parameters
        ----------
        name : str
            Name of the mechanical motion

        port : :class:`.Port`
            The port that this node belongs to
        """
        super().__init__(name, port, NodeType.MECHANICAL, NodeDirection.BIDIRECTIONAL)

class ElectricalNode(Node):
    """
    An object representation of a specific electrical degree of freedom of a
    component.
    """
    def __init__(self, name, port):
        """Constructs a new instance of `MechanicalNode` with the specified properties.

        Parameters
        ----------
        name : str
            Name of the mechanical motion

        port : :class:`.Port`
            The port that this node belongs to
        """
        super().__init__(name, port, NodeType.ELECTRICAL, NodeDirection.BIDIRECTIONAL)


class OpticalNode(Node):
    """
    An object representation of a specific optical port connection at a component.
    OpticalNodes also have additional physical
    properties such as the beam parameter (of type :class:`.BeamParam`)
    at the nodes' position within the interferometer.
    """
    def __init__(self, name, port, direction):
        """Constructs a new instance of `OpticalNode` with the specified properties.

        Parameters
        ----------
        name : str
            Name of this node
        port : :class:`.Port`
            The port that this node belongs to
        direction : :class:`.NodeDirection`
            True if the field at this node is going into the component
        """
        super().__init__(name, port, NodeType.OPTICAL, direction)

    @property
    def is_input(self):
        """Flag indicating whether this node is an input to
        the associated component.

        :getter: Returns `True` if the field at this node
                 goes into `self.component` (read-only).
        """
        return self.direction == NodeDirection.INPUT

    @property
    def opposite(self):
        """The opposite direction node.

        :getter: Returns the opposite direction node to this one.
        """
        return getattr(self.port, 'o' if self.is_input else 'i')

    def __q_set_check(self, value):
        if isinstance(value, complex):
            return BeamParam(q=value)
        if isinstance(value, BeamParam):
            return value
        raise TypeError(
            "The type of q must be either "
            "finesse.gaussian.BeamParam or complex."
        )

    @property
    def q_mode(self):
        """The set-mode(s) of the nodes' `q` value.

        :getter: Returns the `q` set-mode (read-only). If the set-modes
                 for the tangential and sagittal planes are different then it
                 returns a tuple of the modes for each of these parameters.
        """
        qsm = None
        if self._model.last_trace is not None:
            _, _, qsm = self._model.last_trace.get(self, (None, None, None))
        # if beam tracing not run on the model yet then grab qsm
        # value from gauss_commands dict of model
        if qsm is None:
            _, _, qsm = self._model.gauss_commands.get(self, (None, None, None))
        return qsm

    @property
    def q(self):
        """Beam parameter value at this node.

        :getter: Returns the beam parameter at this node. If the beam
                 parameters in the tangential and sagittal planes are
                 different then it returns a tuple of the two parameters.
        :setter: Sets the beam parameter at this node. If the argument
                 provided is a 2-tuple then the parameter is set astigmatically
                 for the node.
        """
        if self.qx is None:
            return None
        if self.qx.q == self.qy.q:
            return self.qx
        return (self.qx, self.qy)

    @q.setter
    def q(self, value):
        if hasattr(value, '__getitem__'): # both qx and qy specified
            try:
                self.qx = value[0]
                self.qy = value[1]
            except TypeError: raise
        else: # only one q specified
            try:
                self._model.set_q_at(self, self.__q_set_check(value), direction="both")
            except TypeError: raise

    @property
    def qx(self):
        """Beam parameter value in the tangential plane.

        :getter: Returns the beam parameter at the node in the
                 tangential plane.
        :setter: Sets the beam parameter at the node in the
                 tangential plane.
        """
        qx, _, _ = self._model.gauss_commands.get(self, (None, None, None))
        if qx is None:
            qx, _, _ = self._model.last_trace.get(self, (None, None, None))
        return qx

    @qx.setter
    def qx(self, value):
        try:
            self._model.set_q_at(self, self.__q_set_check(value), direction='x')
        except TypeError: raise

    @property
    def qy(self):
        """Beam parameter value in the sagittal plane.

        :getter: Returns the beam parameter at the node in the
                 sagittal plane.
        :setter: Sets the beam parameter at the node in the
                 sagittal plane.
        """
        _, qy, _ = self._model.gauss_commands.get(self, (None, None, None))
        if qy is None:
            _, qy, _ = self._model.last_trace.get(self, (None, None, None))
        return qy

    @qy.setter
    def qy(self, value):
        try:
            self._model.set_q_at(self, self.__q_set_check(value), direction='y')
        except TypeError: raise

    # FIXME (sjr) this is quite slow (run playground/beam_tracing/simulation_traces/aligo/trace_aligo.py
    #             to get the profiling results - OpticalNode.space consumes ~25% of the cumulative time)
    @property
    def space(self):
        """A reference to the :class:`.Space`
        object attached to this node.

        :getter: Returns a reference to the :class:`.Space`
                 object attached to the node (read-only).
        """
        try:
            if self.is_input:
                edges = self._model.network.in_edges(self.full_name)
            else:
                edges = self._model.network.out_edges(self.full_name)

            for edge in edges:
                edge_data = self._model.network.get_edge_data(*edge)
                owner = edge_data['owner']()

                if isinstance(owner, finesse.components.Space):
                    return owner
        except ComponentNotConnected:
            pass

        return None
