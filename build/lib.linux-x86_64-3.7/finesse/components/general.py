"""
Top-level objects which specific optical, and otherwise, components
should inherit from.
"""

from abc import ABC
from copy import copy, deepcopy
from collections import OrderedDict
import enum
import weakref

import numpy as np

from finesse import ModelElement
from finesse.components.node import NodeType, OpticalNode, MechanicalNode, Port
from finesse.element import model_parameter, Rebuild
from finesse.exceptions import ComponentNotConnected, NodeException
from finesse.utilities import check_name

class CouplingType(enum.Enum):
    """An enum describing the type of coupling
    between two nodes.
    """
    OPTICAL_TO_OPTICAL = 0
    OPTICAL_TO_ELECTRICAL = 1
    OPTICAL_TO_MECHANICAL = 2
    ELECTRICAL_TO_ELECTRICAL = 3
    ELECTRICAL_TO_OPTICAL = 4
    ELETRICAL_TO_MECHANICAL = 5
    MECHANICAL_TO_MECHANICAL = 6
    MECHANICAL_TO_OPTICAL = 7
    MECHANICAL_TO_ELECTRICAL = 8

class SurfaceType(enum.Enum):
    """The type of a :class:`.Surface` - either MIRROR or BEAMSPLITTER."""
    MIRROR = 1
    BEAMSPLITTER = 2


def determine_coupling_type(from_node, to_node):
    """Retrieves the type of coupling (see :class:`.CouplingType`)
    between two nodes.

    Parameters
    ----------
    from_node : :class:`.Node`
        Node which couples into `to_node`.

    to_node : :class:`.Node`
        Node which has a coupling from `from_node`.

    Returns
    -------
    coupling_t : :class:`.CouplingType`
        The type of coupling between the two given nodes.
    """
    convert = {
        (NodeType.OPTICAL, NodeType.OPTICAL) : CouplingType.OPTICAL_TO_OPTICAL,
        (NodeType.OPTICAL, NodeType.ELECTRICAL) : CouplingType.OPTICAL_TO_ELECTRICAL,
        (NodeType.OPTICAL, NodeType.MECHANICAL) : CouplingType.OPTICAL_TO_MECHANICAL,
        (NodeType.ELECTRICAL, NodeType.ELECTRICAL) : CouplingType.ELECTRICAL_TO_ELECTRICAL,
        (NodeType.ELECTRICAL, NodeType.OPTICAL) : CouplingType.ELECTRICAL_TO_OPTICAL,
        (NodeType.ELECTRICAL, NodeType.MECHANICAL) : CouplingType.ELETRICAL_TO_MECHANICAL,
        (NodeType.MECHANICAL, NodeType.MECHANICAL) : CouplingType.MECHANICAL_TO_MECHANICAL,
        (NodeType.MECHANICAL, NodeType.OPTICAL) : CouplingType.MECHANICAL_TO_OPTICAL,
        (NodeType.MECHANICAL, NodeType.ELECTRICAL) : CouplingType.MECHANICAL_TO_ELECTRICAL
    }
    return convert[(from_node.type, to_node.type)]

class InteractionType(enum.Enum):
    """An enum describing the type of interaction
    between two nodes.
    """
    REFLECTION = 0
    TRANSMISSION = 1


class FrequencyGenerator(object):
    """
    The base class for components which generate optical frequencies.

    A component inheriting from this class will allow the model to query
    the component to ask what frequencies it wants to use. Frequency generation
    comes in the form of either a laser or something that modulates.
    """
    def __init__(self):
        """Constructs a new, empty instance of `FrequencyGenerator`."""
        super().__init__()

    def _couples_frequency(self, sim, connection, frequency_in, frequency_out):
        """
        This method returns whether this element is coupling frequencies for
        a particular connection.

        For example, a modulator would modulator frequencies when a field
        passes through it. Or a suspended mirror would couple the upper and
        lower sidebands on reflection due to radiation pressure effects.

        Parameters
        ----------
        connection : str
            Name of the connection being queried
        frequency_in : :class:`.Frequency`
            Input frequency
        frequency_out : :class:`.Frequency`
            Output frequency

        Returns
        -------
        bool
            True if frequencies couple at this element
        """
        return False

    def _modulation_frequencies(self):
        return []

    def _source_frequencies(self):
        return []

    def _on_response(self, freqWeakRef):
        """
        Callback function that model calls to say whether requested
        frequency was granted or not. If so a weak reference to the
        frequency object is returned.

        Parameters
        ----------
        freqWeakRef: Weakref.ref
            Weak reference to the assigned Frequency object

        Raises
        ------
        Exception
            If passed a name to a frequency this element did not
            ask for originally.
        """
        #if freqWeakRef() in self.__requested_frequencies:
        self.__frequencies.append(freqWeakRef)
        #else:
        #    raise Exception(f"{self} never requested a frequency {freqWeakRef()}")


class Connector(ModelElement):
    """
    The base class for any component which connects nodes together.

    Internally it stores the nodes and the connections associated with the
    component. During the matrix build this class will then ensure that
    the matrix elements for each coupling requested are allocated and
    and the required matrix view for editing their values is retrieved.

    The inheriting class should call ``_register_node`` and ``_register_coupling``
    to define the connections it wants to use.
    """
    def __init__(self, name):
        """Constructs a new `Connector` instance with the specified `name`.

        Parameters
        ----------
        name : str
            Name of the new `Connector` instance.
        """
        super().__init__(name)

        self.__connections = OrderedDict()
        self.__ports = OrderedDict()
        self.__interaction_types = {}
        self._abcd_matrices = {}

        # flag for recomputing ABCD (rather than grabbing from _abcd_matrices dict)
        self._recompute_abcd = True

    def __repr__(self):
        return "<'{}' @ {} ({})>".format(self.name, hex(id(self)), self.__class__.__name__)

    def __nodes_of(self, node_type):
        return tuple([node for node in self.nodes.values() if node.type == node_type])

    @property
    def optical_nodes(self):
        """The optical nodes stored by the connector.

        :getter: Returns a list of the stored optical nodes (read-only).
        """
        return self.__nodes_of(NodeType.OPTICAL)

    @property
    def mechanical_nodes(self):
        """The mechanical nodes stored by the connector.

        :getter: Returns a list of the stored mechanical nodes (read-only).
        """
        return self.__nodes_of(NodeType.MECHANICAL)

    @property
    def electrical_nodes(self):
        """The electrical nodes stored by the connector.

        :getter: Returns a list of the stored electric nodes (read-only).
        """
        return self.__nodes_of(NodeType.ELECTRICAL)

    @property
    def ports(self):
        """
        Retrieves the ports available at the object.

        Returns
        -------
        tuple
            Read-only tuple of the ports available at this object.
        """
        return tuple(self.__ports.values())

    def _add_port(self, name, type):
        """
        Creates either an electrical, mechanical, or optical port for this component.
        Each port can then have multiple different nodes associated with - each node
        being an equation to solve for in the linear system.

        For example, the input and output optical field at one part of a component
        would be one port, i.e. reflection from one side of a mirror.

        The port is added to the component object directly to be referenced at a later
        time when the users is connecting components or definining couplings in a component.
        """
        check_name(name)

        if name in self.ports:
            raise Exception("Port %s already exists for this object" % name)

        if hasattr(self, name):
            raise Exception("Port name %s already exists as an attribute in of this object" % name)

        p = Port(name, self, type)
        self.__ports[name] = p

        #self._unfreeze()
        assert(not hasattr(self, name))
        setattr(self, name, p)
        #self._freeze()

    @property
    def _registered_connections(self):
        return copy(self.__connections)

    def coupling_type(self, from_node, to_node):
        """Obtains the type of coupling (see :class:`.CouplingType`)
        between the two specified nodes at this component.

        Parameters
        ----------
        from_node : :class:`.Node`
            Node which has a forwards coupling to `to_node`.

        to_node : :class:`.Node`
            Node which has a backwards coupling from `from_node`.

        Returns
        -------
        coupling_t : :class:`.CouplingType`
            The type of coupling between the specified nodes.
        """
        # TODO: no easy way to check for non-existent connections now that forced_name
        #       is an option for _register_node_coupling - ignoring space connections for now
        #if from_node.component == to_node.component:
        #    if f"{from_node.full_name}->{to_node.full_name}" not in self.__connections:
        #if not from_node.is_neighbour(to_node):
        #    return None
        try:
            return determine_coupling_type(from_node, to_node)
        except KeyError:
            return None

    def interaction_type(self, from_node, to_node):
        """Obtains the type of interaction (see :class:`.InteractionType`)
        between the two specified nodes at this component.

        Parameters
        ----------
        from_node : :class:`.Node`
            Node which has a forwards coupling to `to_node`.

        to_node : :class:`.Node`
            Node which has a backwards coupling from `from_node`.

        Returns
        -------
        interaction_t : :class:`.InteractionType`
            The type of interaction between the specified nodes.
        """
        try:
            return self.__interaction_types[(from_node.full_name, to_node.full_name)]
        except KeyError:
            return None

    @property
    def nodes(self):
        """
        All the nodes of all the ports at this component. Order is likely to
        be the order in which the ports and nodes were created, but this is not
        guaranteed.

        Returns
        -------
        nodes : tuple(:class:`.Node`)
        """
        nodes = []

        for port in self.__ports.values():
            nodes.extend(port.nodes)

        return {n.full_name: n for n in nodes}


    def _register_node_coupling(self, from_node, to_node, interaction_type=None, forced_name=None):
        """
        Registers that this element will connect the output of
        one of its nodes to the input of another.

        Each element is responsible for requesting connections
        to be made within a Model. In practice this means the
        model will allocate the required matrix elements for
        this element to fill in.

        Once the model knows which connections the element
        will want, when it builds the matrix it will provide
        this element with a SubMatrixView. For example:

            self._register_node_coupling(self.n1.i, self.n1.o)

        this will take the input of node n1 and connect it to
        the output of node n1. For a Mirror component, this would be a
        reflection.

        Parameters
        ----------
        from_node : :class:`.Node`
            The input node

        to_node : :class:`.Node`
            The output node

        interaction_type : :class:`.InteractionType`, optional
            The type of interaction between the nodes if applicable.

        forced_name : str, optional
            Can force the name of a connection. Used by spaces/wires/joints for
            using the correct name as it doesn't own the node
        """
        from finesse.components import Space, Wire

        if forced_name is None:
            name = "{}->{}".format(from_node.full_name, to_node.full_name)
        else:
            name = forced_name

        try:
            if self._model is not None:
                raise Exception("Component has already been added to a model")
        except ComponentNotConnected:
            pass

        if name in self.__connections:
            raise Exception("Connection called {} already set at component {}".format(self.name, name))

        # TODO: decide whether we actually need this - shouldn't be possible
        #       to connect non-existing ports without python complaining about
        #       an undefined parameter first anyway
        if isinstance(self, Space) or isinstance(self, Wire):
            if from_node.port not in from_node.port.component.ports:
                raise Exception()
            if to_node.port not in to_node.port.component.ports:
                raise Exception()
        else:
            if from_node.port.name not in self.__ports:
                raise Exception("Node {}.{} is not available at component `{}`".format(from_node.port.name, from_node.name, self.name))
            if to_node.port.name not in self.__ports:
                raise Exception("Node {}.{} is not available at component `{}`".format(to_node.port.name, to_node.name, self.name))

        self.__connections[name] = (from_node.full_name, to_node.full_name)

        if interaction_type is not None:
            self.__interaction_types[(from_node.full_name, to_node.full_name)] = interaction_type


    def _ABCD_prechecks(self, from_node, to_node):
        """Performs common checks on `from_node` and `to_node`
        before allowing an ABCD matrix computation to proceed.

        Checks that `from_node` and `to_node` are contained within
        the associated components' ``nodes.values()`` attribute. Also
        checks that `from_node` and `to_node` are not both inputs or
        both outputs.

        Parameters
        ----------
        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        Raises
        ------
        err : :class:`.NodeException`
            If either `from_node` or `to_node` are not in ``self.nodes``
            or if `from_node` and `to_node` are both inputs or both outputs.
        """
        if from_node not in self.nodes.values():
            raise NodeException(
                "Node {} is not associated with component {}".format(from_node, self),
                from_node
            )

        if to_node not in self.nodes.values():
            raise NodeException(
                "Node {} is not associated with component {}".format(to_node, self),
                to_node
            )

        if from_node.is_input and to_node.is_input:
            raise NodeException(
                "Cannot compute ABCD matrix when from_node"
                "and to_node are both input nodes.",
                [from_node, to_node]
            )

        if not from_node.is_input and not to_node.is_input:
            raise NodeException(
                "Cannot compute ABCD matrix when from_node"
                "and to_node are both output nodes.",
                [from_node, to_node]
            )

        if not from_node.is_neighbour(to_node):
            raise NodeException(
                f"No coupling exists between {from_node} -> {to_node}",
                [from_node, to_node]
            )

    def _store_ABCD(self, M, from_node, to_node, direction):
        self._abcd_matrices[(from_node, to_node, direction)] = M
        self._recompute_abcd = False

    def select_loggable_knm_matrices(self, loggable, couplings=None):
        """Select the knm matrices to log and, optionally, which mode
        couplings to log in these matrices.

        Parameters
        ----------
        loggable : sequence
            An iterable of strings corresponding to coupling coefficient
            matrices - e.g. `["K11", "K22"]` for a :class:`.Mirror` would
            then log the K11, K22 matrices for that mirror.

        couplings : array like, optional
            A list or array of mode couplings to log. This must be formatted
            as `[(n1, m1, n2, m2), ...]`. Defaults to `None` such that
            all mode couplings are logged.
        """
        if not hasattr(self, "_knm_logging_info"):
            raise Exception(f"Component: {self.name} has no computable knm matrices.")

        if isinstance(loggable, str):
            loggable = [loggable]

        self._knm_logging_info["matrices"] = set(loggable)

        if couplings is not None:
            if not hasattr(couplings[0], "__getitem__"):
                couplings = [couplings]

            self._knm_logging_info["couplings"] = np.array(couplings, dtype=np.intc)

    def ABCD(self, from_node, to_node, direction='x', symbolic=False):
        """ABCD matrix computation for a generic component, simply returns
        a 2x2 identity matrix.

        This is a convenience function which prevents needing to implement an
        identical method for a component which doesn't modify the beam parameter.

        Parameters
        ----------
        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        direction : str
            Direction of ABCD matrix computation, default is 'x'. Note the
            ABCD matrix returned is dimension-independent in the generic case,
            thus this method will return the same result for both 'x' and 'y'.

        Returns
        -------
        out : :class:`numpy.ndarray`
            The 2x2 identity matrix.

        Raises
        ------
        err : :class:`.NodeException`
            If either `from_node` or `to_node` are not in ``self.nodes``
            or if `from_node` and `to_node` are both inputs or both outputs.
        """
        self._ABCD_prechecks(from_node, to_node)

        return np.eye(2)

@model_parameter('value', 1, Rebuild.NA, '_validate')
class Variable(ModelElement):
    def __init__(self, name, value, validator=None):
        super().__init__(name)
        self.__fn_validate = validator
        self.value = value

    def _validate(self, value):
        if self.__fn_validate is not None:
            return self.__fn_validate(value)
        else:
            return value

    def __repr__(self):
        return "<'{}' @ {} ({})>".format(self.name, hex(id(self)), self.__class__.__name__)

@model_parameter('R', 0.99, Rebuild.PlaneWave, "_check_R")
@model_parameter('T', 0.01, Rebuild.PlaneWave, "_check_T")
@model_parameter('L', 0.0, Rebuild.PlaneWave, "_check_L")
@model_parameter('phi', 0, Rebuild.PlaneWave, "_check_phi")
@model_parameter('Rcx', np.inf, Rebuild.HOM, "_check_Rc")
@model_parameter('Rcy', np.inf, Rebuild.HOM, "_check_Rc")
@model_parameter('xbeta', 0.0, Rebuild.HOM, "_check_xbeta")
@model_parameter('ybeta', 0.0, Rebuild.HOM, "_check_ybeta")
class Surface(ABC, Connector):
    """
    An abstract optical surface interface providing an object with common properties
    for :class:`.Mirror` and :class:`.Beamsplitter` to inherit from.

    Attributes
    ----------
    R : :class:`.parameter`
        Reflectivity of the surface.

    T : :class:`.parameter`
        Transmittance of the surface.

    L : :class:`.parameter`
        Optical loss of the surface.

    phi : :class:`.parameter`
        Detunining (in degrees).

    Rcx : :class:`.parameter`
        Radius of curvature in tangential plane (in metres).

    Rcy : :class:`.parameter`
        Radius of curvature in sagittal plane (in metres).
    """
    def __init__(self, name, R, T, L, phi, Rc, xbeta, ybeta, stype):
        """
        Parameters
        ----------
        name : str
            Name of newly created `Surface` instance.

        R : float, optional
            Reflectivity of the surface.

        T : float, optional
            Transmissivity of the surface.

        L : float, optional
            Loss of the surface.

        phi : float, optional
            Microscopic tuning of the surface [in degrees].

        Rc : float, optional
            Radius of curvature [in metres], defaults to ``numpy.inf`` to
            indicate a planar surface. An astigmatic surface can be set with
            `Rc = (Rcx, Rcy)`.
        """
        super().__init__(name)

        self.__which_specified = set()
        if R is None and T is None and L is None:
            self.R = 0.5
            self.T = 0.5
            self.__which_specified.add('R')
            self.__which_specified.add('T')
        elif R is not None and T is not None:
            self.R = R
            self.T = T
            self.__which_specified.add('R')
            self.__which_specified.add('T')
        elif R is not None and L is not None:
            self.R = R
            self.T = 1.0 - L - R
            self.__which_specified.add('R')
            self.__which_specified.add('L')
        elif T is not None and L is not None:
            self.T = T
            self.R = 1.0 - L - T
            self.__which_specified.add('T')
            self.__which_specified.add('L')
        else:
            raise ValueError("Incorrect combination of R, T, L values. You must "
                             "specify one of: R and T, R and L or T and L.")

        self._type = stype

        self.phi = phi
        self.Rc = Rc
        self.mass = np.inf
        self.xbeta = xbeta
        self.ybeta = ybeta

        self._angle_changed = False

    def _check_R(self, value):
        #self._r = np.sqrt(value)
        #if 'T' in self.__which_specified:
        #    self._t = np.sqrt(1.0 - value)
        #    self._it = 1j * self._t
        return value

    def _check_T(self, value):
        #self._t = np.sqrt(value)
        #self._it = 1j * self._t
        #if 'R' in self.__which_specified:
        #    self._r = np.sqrt(1.0 - value)
        return value

    def _check_L(self, value):
        # don't know which of R, T to adjust if L was not specified
        # at construction time - so throw exception instead
        #if 'L' not in self.__which_specified:
        #    raise ValueError("To change the surface optical loss directly, the loss"
        #                     " must be specified during construction of the surface.")
        #if 'R' in self.__which_specified:
        #    self._r = np.sqrt(1.0 - value - self.T)
        #elif 'T' in self.__which_specified:
        #    self._t = np.sqrt(1.0 - value - self.R)
        #    self._it = 1j * self._t

        return value

    def _check_phi(self, value):
        rad_phi = np.radians(value)
        if self.type == SurfaceType.MIRROR:
            self._tuning = np.exp(-2j * rad_phi)
        else:
            self._tuning = np.exp(2j * rad_phi * self._cos_alpha)
        self._ctuning = self._tuning.conj()

        return value

    def _check_Rc(self, value):
        if self.has_model:
            self._model._retrace_required = True
        self._recompute_abcd = True

        return value

    @property
    def type(self):
        return self._type

    @property
    def Rc(self):
        """The radius of curvature of the mirror in metres. If the
        mirror is astigmatic this is a tuple of the tangential and
        sagittal radii of curvature.

        :getter: Returns the radius of curvature of the mirror in metres (a
                 tuple of values if the mirror is astigmatic).

        :setter: Sets the radius of curvature.

        Examples
        --------
        The following sets the radii of curvature of an object `obj`, which
        is a sub-instance of `Surface`, in both directions to 2.5 m:

        >>> obj.Rc = 2.5

        Whilst this would set the radius of curvature in the x-direction (tangential
        plane) to 2.5 and the radius of curvature in the y-direction (sagittal plane)
        to 2.7:

        >>> obj.Rc = (2.5, 2.7)
        """
        if np.isclose(self.Rcx.value, self.Rcy.value):
            return self.Rcx
        return (self.Rcx, self.Rcy)

    @Rc.setter
    def Rc(self, value):
        try:
            self.Rcx = value[0]
            self.Rcy = value[1]
        except (IndexError, TypeError):
            self.Rcx = value
            self.Rcy = value

        self._check_Rc(value)

    @property
    def mass(self):
        """Mass of the mirror in kg.

        :getter: Returns the mass of the mirror.
        :setter: Returns the mass of the mirror.
        """
        return self._mass

    @mass.setter
    def mass(self, value):
        self._mass = value

    def _check_xbeta(self, value):
        self._angle_changed = True
        return value

    def _check_ybeta(self, value):
        self._angle_changed = True
        return value
