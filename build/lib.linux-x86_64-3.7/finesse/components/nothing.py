"""
Contains the class :class:`.Nothing` which represents an empty/null
point in a configuration.
"""

import numpy as np

from finesse.components.general import Connector, CouplingType, InteractionType
from finesse.components.node import NodeType, NodeDirection

class Nothing(Connector):
    """
    An object representing an empty point in the interferometer
    configuration.

    `Nothing` is just some point in space that can be connected to.
    For example, you can use this to propagate a beam from some component out to
    some arbitrary point to make a measurement at. You can
    also use this to split spaces up, if you wanted to measure
    something inbetween two components. It can also be used
    to replace a component, for example if you want to remove
    a lens or a mirror in some beam path.
    """
    def __init__(self, name):
        """
        Constructs a new instance of `Nothing`.

        Parameters
        ----------
        name : str
            Name of this instance of `Nothing`.
        """
        super().__init__(name)

        # Here we register that fact this component will
        # want to have some ports and nodes as well as
        # the couplings between them
        self._add_port('p1', NodeType.OPTICAL) # front
        self._add_port('p2', NodeType.OPTICAL) # back
        # input and output optical fields at port 1 (Front face)
        self.p1._add_node("i", NodeDirection.INPUT)
        self.p1._add_node("o", NodeDirection.OUTPUT)
        # input and output optical fields at port 2 (Back face)
        self.p2._add_node("i", NodeDirection.INPUT)
        self.p2._add_node("o", NodeDirection.OUTPUT)
        # Optic to optic couplings
        self._register_node_coupling(self.p1.i, self.p2.o,
                                     interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p2.i, self.p1.o,
                                     interaction_type=InteractionType.TRANSMISSION)

    def _on_build(self, sim):
        self.__I = np.eye(sim.nhoms, dtype=np.complex128)

    def _fill_matrix(self, sim):
        for freq in sim.frequencies:
            with sim.component_edge_fill(self, 'p1.i->p2.o', freq, freq) as mat:
                mat[:] = self.__I
            with sim.component_edge_fill(self, 'p2.i->p1.o', freq, freq) as mat:
                mat[:] = self.__I
