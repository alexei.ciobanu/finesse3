"""
Wire-type objects representing electrical connections between components.
"""

import numpy as np
import logging

from finesse.components import Connector, NodeType

LOGGER = logging.getLogger(__name__)


class Wire(Connector):
    """
    A class representing a wire between two electrical components in the
    interferometer configuration.
    """

    def __init__(self, name=None, portA=None, portB=None):
        """
        Constructs a new instance of `Wire` with the specified properties.

        Parameters
        ----------
        name : str, optional
            Name of newly created :class:`.Wire` instance.

        portA : :class:`.Port`, optional
            Port to connect

        portB : :class:`.Port`, optional
            Port to connect
        """
        if (portA is None) != (portB is None):
            LOGGER.warn("Can't construct a wire with only one port "
                        "connected, ignoring ports.")
            portA = None
            portB = None

        if portA is not None and portA.type != NodeType.ELECTRICAL:
            raise Exception("PortA argument is not an electrical port")
        if portB is not None and portB.type != NodeType.ELECTRICAL:
            raise Exception("PortB argument is not an electrical port")

        if name is None:
            if portA is not None and portB is not None:
                compA = portA.component.name
                compB = portB.component.name
                name = f"{compA}_{portA.name}__{compB}_{portB.name}"
            else:
                raise ValueError("Cannot create an unconnected wire without "
                                 "providing a name")

        super().__init__(name)

        self._add_to_model_namespace = False
        self._add_port('p1', NodeType.ELECTRICAL)
        self._add_port('p2', NodeType.ELECTRICAL)

        if portA is not None and portB is not None:
            self.connect(portA, portB)

    def connect(self, portA, portB):
        """
        Sets the ports of this `Wire`.

        Parameters
        ----------
        portA : :class:`.Port`, optional
            Port to connect

        portB : :class:`.Port`, optional
            Port to connect
        """
        # From the Wire's perspective the input and output
        # nodes are swapped around for its ports
        self.p1._add_node('i', None, portA.o)
        self.p1._add_node('o', None, portA.i)
        self.p2._add_node('i', None, portB.o)
        self.p2._add_node('o', None, portB.i)

        self._register_node_coupling(
            self.p1.i,
            self.p2.o,
            forced_name=f'{self.name}.p1.i->{self.name}.p2.o')
        self._register_node_coupling(
            self.p2.i,
            self.p1.o,
            forced_name=f'{self.name}.p2.i->{self.name}.p1.o')

    def _fill_matrix(self, sim):
        if not sim.is_audio:
            return

        I = np.eye(sim.nhoms, dtype=np.complex128)
        sim[(self, "p1.i->p2.o", None, None)][:] = I
        sim[(self, "p2.i->p1.o", None, None)][:] = I
