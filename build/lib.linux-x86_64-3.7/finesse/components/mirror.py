"""
Dielectric interface type components representing physical mirrors.
"""

from dataclasses import dataclass
import enum
import logging

import numpy as np

from finesse import constants
from finesse.element import model_parameter, Rebuild
from finesse.knm import run_bayer_helms, rev_all_gouy, zero_tem00_phase, flip_odd_horizontal, knm_loss
from finesse.knm import log_knm_matrix
from finesse.utilities import refractive_indices
from finesse.utilities.maths import apply_ABCD as apply

from finesse.components.matrixfill import mirror_fill
from finesse.components.general import Surface, InteractionType, SurfaceType
from finesse.components.node import NodeDirection, NodeType

from finesse.fun_lib.LCT_funs import LCT_operator, LCT_mirror, LCT_general_refraction
from finesse.fun_lib.HG_operator import HG_operator
from finesse.fun_lib.planewave_operator import planewave_operator
from finesse.fun_lib.graph_funs import get_node_obj

LOGGER = logging.getLogger(__name__)

class KnmMirrorNodeDirection(enum.IntEnum):
    MR11 = 1
    MR12 = 2
    MR21 = 3
    MR22 = 4

@dataclass
class MirrorKnmMatrices:
    # coupling coefficients for reflection & transmission
    # these are the final coefficients after combining others
    K11: np.ndarray
    K12: np.ndarray
    K21: np.ndarray
    K22: np.ndarray

    K11_loss: np.ndarray
    K12_loss: np.ndarray
    K21_loss: np.ndarray
    K22_loss: np.ndarray

    # coupling coefficients as computed via Bayer-Helms
    K11_bh: np.ndarray
    K12_bh: np.ndarray
    K21_bh: np.ndarray
    K22_bh: np.ndarray

    # coupling coefficients for maps
    K11_map: np.ndarray
    K12_map: np.ndarray
    K21_map: np.ndarray
    K22_map: np.ndarray

    # coupling coefficients for apertures
    K11_aperture: np.ndarray
    K12_aperture: np.ndarray
    K21_aperture: np.ndarray
    K22_aperture: np.ndarray


# FIXME (sjr) these have to be repeated currently as the descriptors
#             are not inherited from Surface
@model_parameter('R', 0.99, Rebuild.PlaneWave)
@model_parameter('T', 0.01, Rebuild.PlaneWave)
@model_parameter('L', 0.0, Rebuild.PlaneWave)
@model_parameter('phi', 0, Rebuild.PlaneWave, units="degrees", validate="_check_phi")
@model_parameter('Rcx', np.inf, Rebuild.HOM, units='m', validate="_check_Rc")
@model_parameter('Rcy', np.inf, Rebuild.HOM, units='m', validate="_check_Rc")
@model_parameter('xbeta', 0.0, Rebuild.HOM, units='radians', validate="_check_xbeta")
@model_parameter('ybeta', 0.0, Rebuild.HOM, units='radians', validate="_check_ybeta")
class Mirror(Surface):
    """
    A class representing a mirror optical component with associated
    properties such as reflectivity, tuning and radius of curvature.
    """
    def __init__(self,
        name,
        R=None, T=None, L=None,
        phi=0.0,
        Rc=np.inf,
        xbeta=0.0, ybeta=0.0
    ):
        """
        Constructs a new `Mirror` instance with the specified properties.

        Parameters
        ----------
        name : str
            Name of newly created `Mirror` instance.

        R : float, optional
            Reflectivity of the mirror, default is 0.5.

        T : float, optional
            Transmittance of the mirror, default is 0.5.

        L : float, optional
            Loss of the mirror, default is 0.0.

        phi : float, optional
            Tuning of the mirror [in degrees], default is 0.0.

        Rc : float or container of two floats, optional
            The radius of curvature of the mirror (in metres), default
            is a flat mirror (`Rc=np.inf`). Astigmatic mirrors can also
            be set with `Rc=(Rcx, Rcy)`.
        """
        super().__init__(name, R, T, L, phi, Rc, xbeta, ybeta, SurfaceType.MIRROR)

        self._add_port('p1', NodeType.OPTICAL)
        self.p1._add_node("i",  NodeDirection.INPUT)
        self.p1._add_node("o",  NodeDirection.OUTPUT)

        self._add_port('p2', NodeType.OPTICAL)
        self.p2._add_node("i",  NodeDirection.INPUT)
        self.p2._add_node("o",  NodeDirection.OUTPUT)

        # Optic to optic couplings
        self._register_node_coupling(self.p1.i, self.p1.o, interaction_type=InteractionType.REFLECTION)
        self._register_node_coupling(self.p2.i, self.p2.o, interaction_type=InteractionType.REFLECTION)
        self._register_node_coupling(self.p1.i, self.p2.o, interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p2.i, self.p1.o, interaction_type=InteractionType.TRANSMISSION)

        # Mirror motion couplings
        self._add_port('mech', NodeType.MECHANICAL)
        self.mech._add_node("z")
        self.mech._add_node("yaw")
        self.mech._add_node("pitch")

        # Modulation inputs couple to the mirror's mechanical motion
        self._add_port('phase_sig', NodeType.ELECTRICAL)
        self.phase_sig._add_node("i", NodeDirection.INPUT)
        self.phase_sig._add_node("o", NodeDirection.OUTPUT)

        self._register_node_coupling(self.phase_sig.i, self.mech.z)

        # Optic to motion couplings
        # self._register_node_coupling(self.p1.i, self.mech.z)
        # self._register_node_coupling(self.p2.i, self.mech.z)
        # self._register_node_coupling(self.p1.o, self.mech.z)
        # self._register_node_coupling(self.p2.o, self.mech.z)
        # motion to optic coupling: phase coupling on reflection
        self._register_node_coupling(self.mech.z, self.p1.o)
        self._register_node_coupling(self.mech.z, self.p2.o)

        self._knm_logging_info = {
            "matrices" : [], #set(["K11", "K22", "K12", "K21"]),
            "couplings" : None
        }

    def get_operator(self,from_node,to_node,op_type='LCT'):
        nr1, nr2 = refractive_indices(self.p1.i, self.p2.o)
        Rc = self.Rc.value
        valid_edges = [
            (self.p1.i.full_name,self.p1.o.full_name),
            (self.p1.i.full_name,self.p2.o.full_name),
            (self.p2.i.full_name,self.p2.o.full_name),
            (self.p2.i.full_name,self.p1.o.full_name)
            ]
        edge_op_dict = {
            valid_edges[0] : None,
            valid_edges[1] : None,
            valid_edges[2] : None,
            valid_edges[3] : None,
            }
        if op_type is 'LCT':
            edge_op_dict[valid_edges[0]] = self._r  * self._tuning  * LCT_operator(type='LCT',M=LCT_mirror(Rc,nr=nr1))
            edge_op_dict[valid_edges[1]] = self._it *                 LCT_operator(type='LCT',M=LCT_general_refraction(nr1,nr2,Rc))
            edge_op_dict[valid_edges[2]] = self._r  * self._ctuning * LCT_operator(type='LCT',M=LCT_mirror(-Rc,nr=nr2))
            edge_op_dict[valid_edges[3]] = self._it *                 LCT_operator(type='LCT',M=LCT_general_refraction(nr2,nr1,-Rc))
        elif op_type is 'HG':
            first_nodes = [n1 for n1,n2 in valid_edges]
            first_qxs = [self.nodes[node].qx for node in first_nodes]
            first_qys = [self.nodes[node].qy for node in first_nodes]
            edge_op_dict[valid_edges[0]] = self._r  * self._tuning *  HG_operator(Mx=self.ABCD(*valid_edges[0],direction='x'),My=self.ABCD(*valid_edges[0],direction='y'),qx=first_qxs[0],qy=first_qys[0])
            edge_op_dict[valid_edges[1]] = self._it *                 HG_operator(Mx=self.ABCD(*valid_edges[1],direction='x'),My=self.ABCD(*valid_edges[1],direction='y'),qx=first_qxs[1],qy=first_qys[1])
            edge_op_dict[valid_edges[2]] = self._r  * self._ctuning * HG_operator(Mx=self.ABCD(*valid_edges[2],direction='x'),My=self.ABCD(*valid_edges[2],direction='y'),qx=first_qxs[2],qy=first_qys[2])
            edge_op_dict[valid_edges[3]] = self._it *                 HG_operator(Mx=self.ABCD(*valid_edges[3],direction='x'),My=self.ABCD(*valid_edges[3],direction='y'),qx=first_qxs[3],qy=first_qys[3])
        elif op_type is 'str':
            edge_op_dict[valid_edges[0]] = f"{self.name} Front Reflection   {self.R}, Rc = {self.Rc.value}"
            edge_op_dict[valid_edges[1]] = f"{self.name} Front Transmission {self.T}, Rc = Nan"
            edge_op_dict[valid_edges[2]] = f"{self.name} Back  Reflection   {self.R}, Rc = {-self.Rc.value}"
            edge_op_dict[valid_edges[3]] = f"{self.name} Back  Transmission {self.T}, Rc = Nan"
        elif op_type is 'planewave':
            edge_op_dict[valid_edges[0]] = self._r  * self._tuning  * planewave_operator(nodes=valid_edges[0])
            edge_op_dict[valid_edges[1]] = self._it *                 planewave_operator(nodes=valid_edges[1])
            edge_op_dict[valid_edges[2]] = self._r  * self._ctuning * planewave_operator(nodes=valid_edges[2])
            edge_op_dict[valid_edges[3]] = self._it *                 planewave_operator(nodes=valid_edges[3])
        else:
            raise Exception(f"operator type {op_type} is undefined")
        
        return_val = edge_op_dict.get((from_node,to_node),None)
        if return_val is None:
            raise Exception(f"edge {(from_node,to_node)} not found in object {self}")
        else:
            return return_val

    def get_LCT_operator(self,from_node,to_node):
        nr1, nr2 = refractive_indices(self.p1.i, self.p2.o)
        Rc = self.Rc.value
        edge_op_dict = {
        (self.p1.i.full_name,self.p1.o.full_name) : self._r * self._tuning * LCT_operator(type='LCT',M=LCT_mirror(Rc,nr=nr1)),
        (self.p1.i.full_name,self.p2.o.full_name) : self._it * LCT_operator(type='LCT',M=LCT_general_refraction(nr1,nr2,Rc)),
        (self.p2.i.full_name,self.p2.o.full_name) : self._r * self._ctuning * LCT_operator(type='LCT',M=LCT_mirror(-Rc,nr=nr2)),
        (self.p2.i.full_name,self.p1.o.full_name) : self._it * LCT_operator(type='LCT',M=LCT_general_refraction(nr2,nr1,-Rc)),
        }
        return_val = edge_op_dict.get((from_node,to_node),None)
        if return_val is None:
            raise Exception(f"edge {(from_node,to_node)} not found in object {self}")
        else:
            return return_val
        
    def get_operator_description(self,from_node,to_node):
        edge_op_dict = {
        (self.p1.i.full_name,self.p1.o.full_name) : f"{self.name} Front Reflection   {self.R}, Rc = {self.Rc.value}",
        (self.p1.i.full_name,self.p2.o.full_name) : f"{self.name} Front Transmission {self.T}, Rc = Nan",
        (self.p2.i.full_name,self.p2.o.full_name) : f"{self.name} Back  Reflection   {self.R}, Rc = {-self.Rc.value}",
        (self.p2.i.full_name,self.p1.o.full_name) : f"{self.name} Back  Transmission {self.T}, Rc = Nan",
        }
        return_val = edge_op_dict.get((from_node,to_node),None)
        if return_val is None:
            raise Exception(f"edge {(from_node,to_node)} not found in object {self}")
        else:
            return return_val

    def ABCD(self, from_node, to_node, direction='x', symbolic=False):
        r"""Computes and returns the ABCD matrix of the mirror. For transmission
        this is given by,

        .. math::
            M_{t} = \begin{pmatrix}
                        1 & 0 \\
                        \frac{n_2 - n_1}{R_c} & 1
                    \end{pmatrix},

        where :math:`n_2` and :math:`n_1` are the indices of refraction of the spaces
        connected to the mirror and :math:`R_c` is the radius of curvature of the mirror.

        For reflection this is given by,

        .. math::
            M_{r} = \begin{pmatrix}
                        1 & 0 \\
                        -\frac{2n_1}{R_c} & 1
                    \end{pmatrix}.

        The sign of the radius is defined such that :math:`R_c` is negative if the centre of
        the sphere is located in the direction of propagation.

        Parameters
        ----------
        from_node : :class:`.OpticalNode`
            Node to trace from.

        to_node : :class:`.OpticalNode`
            Node to trace to.

        direction : str, optional
            Direction of ABCD matrix computation, default is 'x'.

        Returns
        -------
        out : :class:`numpy.ndarray`
            The ABCD matrix for the mirror.

        Raises
        ------
        err : :class:`.NodeException`
            If either `from_node` or `to_node` are not in ``self.nodes``
            or if `from_node` and `to_node` are both inputs or both outputs.
        """
        if symbolic:
            raise NotImplementedError()
        if not symbolic:
            if not self._recompute_abcd:
                stored_M = self._abcd_matrices.get((from_node, to_node, direction), None)
                if stored_M is not None:
                    return stored_M

        if type(from_node) is str:
            from_node = self.nodes[from_node]
        if type(to_node) is str:
            to_node = self.nodes[to_node]

        self._ABCD_prechecks(from_node, to_node)

        if direction == 'x':
            Rc = self.Rcx.value
        else:
            Rc = self.Rcy.value

        if self.interaction_type(from_node, to_node) == InteractionType.REFLECTION:
            fn_space = from_node.space
            if fn_space is None:
                nr = 1.0
            else:
                nr = fn_space.nr.value

            underlying = [
                [1.0, 0.0],
                [-2 * nr / Rc if from_node.port is self.p1 else 2 * nr / Rc, 1.0]
            ]

            if not symbolic:
                M = np.array(underlying)
                self._store_ABCD(M, from_node, to_node, direction)

            return M

        # transmission

        nr1, nr2 = refractive_indices(from_node, to_node, symbols=symbolic)

        underlying = [
            [1.0, 0.0],
            [(nr2-nr1)/Rc if from_node.port is self.p1 else -(nr2-nr1)/Rc, 1.0]
        ]

        if not symbolic:
            M = np.array(underlying)
            self._store_ABCD(M, from_node, to_node, direction)

        return M

    def _gather(self, DC_sim, AC_sim):
        DC_sim.needs_source(...)
        DC_sim.needs_DC_for_AC(...)
        DC_sim.needs_DC_for_sweep(...)
        AC_sim.needs_drive((), ())

        if self.t.is_changing():
            DC_sim.changing_edge((), ())


    def _on_build(self, sim):
        nhom = sim.nhoms
        values, is_changing = self._eval_parameters()

        self.__Is = np.ones((sim.nhoms, 1))

        self._allocate_knm_matrices(sim)
        #self._compute_knm_matrices(sim)

        # set references
        self.K11 = self.knm_matrices.K11
        self.K12 = self.knm_matrices.K12
        self.K21 = self.knm_matrices.K21
        self.K22 = self.knm_matrices.K22

        self.K11_loss = self.knm_matrices.K11_loss
        self.K12_loss = self.knm_matrices.K12_loss
        self.K21_loss = self.knm_matrices.K21_loss
        self.K22_loss = self.knm_matrices.K22_loss

    def _fill_matrix(self, sim):
        values, is_changing = self._eval_parameters()

        mirror_fill(self, sim, values, sim.model.lambda0)

        """_it = 1j*np.sqrt(values['T'])
        _r  = np.sqrt(values['R'])
        f0 = constants.C_LIGHT / self._model.lambda0
        phi = np.deg2rad(values['phi'])

        for freq in sim.frequencies:
            phi_scaled = phi*(1 + freq.f / f0)

            _tuning    = np.exp(-2j*phi_scaled)
            _ctuning   = np.conj(_tuning)

            with sim.component_edge_fill(self, 'p1.i->p1.o', freq, freq) as mat:
                np.multiply(_r * _tuning,  self.K11, out=mat[:])

            with sim.component_edge_fill(self, 'p2.i->p2.o', freq, freq) as mat:
                np.multiply(_r * _ctuning, self.K22, out=mat[:])

            with sim.component_edge_fill(self, 'p1.i->p2.o', freq, freq) as mat:
                np.multiply(_it, self.K12, out=mat[:])

            with sim.component_edge_fill(self, 'p2.i->p1.o', freq, freq) as mat:
                np.multiply(_it, self.K21, out=mat[:])

            if sim.is_audio:
                carrier = sim.DC

                k = 2*np.pi/sim.model.lambda0  # wavenumber

                # -------------------------------------------------
                # Mechanical to optical connections
                # -------------------------------------------------
                # - Longitudinal
                # -------------------------------------------------
                # Need to modulate the reflected carrier field
                _i = carrier.field(self.p1.i, freq.audio_carrier_index, 0)
                c_p1_i = np.atleast_2d(carrier.out[slice(_i, _i+sim.nhoms, 1)]).T

                _i = carrier.field(self.p2.i, freq.audio_carrier_index, 0)
                c_p2_i = np.atleast_2d(carrier.out[slice(_i, _i+sim.nhoms, 1)]).T

                fac = 1j * k * _r  # scaling factor for longitudinal motion

                # As we've already computed the scattering matrix on
                # reflection we should use that
                with sim.component_edge_fill(self, 'mech.z->p1.o', None, freq) as mat:
                    mat_refl = sim.component_edge_get(self, 'p1.i->p1.o', freq, freq)[:]
                    np.dot(mat_refl, fac * c_p1_i, out=mat[:])

                # extra 180 phase here as we're doing the opposite
                # modulation when looked at from the other side of the mirror
                with sim.component_edge_fill(self, 'mech.z->p2.o', None, freq) as mat:
                    mat_refl = sim.component_edge_get(self, 'p2.i->p2.o', freq, freq)[:]
                    np.dot(mat_refl, fac * c_p2_i, out=mat[:])

                # -------------------------------------------------

                # -------------------------------------------------
                # Optical to mechanical connections
                # -------------------------------------------------
                # - Longitudinal
                # -------------------------------------------------
                # np.multiply(0, 0, out=sim[(self, 'p1.i->mech.z', freq, None)][:])
                # np.multiply(0, 0, out=sim[(self, 'p1.o->mech.z', freq, None)][:])
                # np.multiply(0, 0, out=sim[(self, 'p2.i->mech.z', freq, None)][:])
                # np.multiply(0, 0, out=sim[(self, 'p2.o->mech.z', freq, None)][:])
                # -------------------------------------------------
        """

    def _fsig(self, sim, carrier_sim, phase, mod_type, amp=1):
        # TODO: for now, just assuming phase fsig on a fixed mirror, ignoring
        # mass, knm matrix etc.
        for freq in sim.frequencies.values():
            ph = 90 + freq.order * phase
            # TODO: Replace factor of 1 with epsilon (is this EPSILON0_C?)
            factor = amp * 1 * 0.5
            # TODO: Remove hardcoded f0 value
            f0 = constants.C_LIGHT / 1064e-9
            factor *= 2 * (1 + freq.carrier.f / f0) * self._r
            ph1 = self.phi * ((1 + freq.f / f0) + (1 + freq.carrier.f / f0))

            for i, hom in enumerate(sim.homs):
                tn = int(hom[0])
                turn = -1**tn
                car = carrier_sim.get_output(self.p1.i, freq.carrier.index, i)
                sig = (turn * factor) * car * np.exp(1j * np.deg2rad(ph + ph1))
                if (freq.order < 0):
                    # Conjugate lower sideband
                    sig = sig.conj()
                #sim.M().set_rhs(sim.field(self.p1.o, freq.index, i), sig)
                sim.set_source(
                    field_idx = (self.p1.o, freq.index, i),
                    vector = sig,
                )

            for i, hom in enumerate(sim.homs):
                tn = int(hom[0])
                turn = -1**tn
                idx = carrier_sim.field(self.p2.i, freq.carrier.index, i)
                car = carrier_sim.out[idx]
                sig = (turn * factor) * car * np.exp(1j * np.deg2rad(ph - ph1 + 180 * freq.order))
                if (freq.order < 0):
                    # Conjugate lower sideband
                    sig = sig.conj()
                #sim.M().set_rhs(sim.field(self.p2.o, freq.index, i), sig)
                sim.set_source(
                    field_idx = (self.p2.o, freq.index, i),
                    vector = sig,
                )

    def _fill_qnoise_rhs(self, sim):
        if sim.is_modal:
            for freq in sim.frequencies:
                for k in range(sim.nhoms):
                    L1 = self.R * self.K11_loss[k] + self.T * self.K21_loss[k]
                    L2 = self.R * self.K22_loss[k] + self.T * self.K12_loss[k]
                    sim.Mq[sim.field(self.p1.o, freq.index, k)] = (L1 + self.L) / 2
                    sim.Mq[sim.field(self.p2.o, freq.index, k)] = (L2 + self.L) / 2
        else:
            for freq in sim.frequencies:
                sim.Mq[sim.field(self.p1.o, freq.index, 0)] = self.L / 2
                sim.Mq[sim.field(self.p2.o, freq.index, 0)] = self.L / 2

    def _allocate_knm_matrices(self, sim):
        I = np.eye(sim.nhoms, dtype=np.complex128)
        losses = np.ones(sim.nhoms)

        self._prev_bh = {}
        self._prev_bh[KnmMirrorNodeDirection.MR11] = ()
        self._prev_bh[KnmMirrorNodeDirection.MR12] = ()
        self._prev_bh[KnmMirrorNodeDirection.MR21] = ()
        self._prev_bh[KnmMirrorNodeDirection.MR22] = ()

        self.knm_matrices = MirrorKnmMatrices(
            K11=I.copy(), K12=I.copy(), K21=I.copy(), K22=I.copy(),
            K11_loss=losses.copy(), K12_loss=losses.copy(), K21_loss=losses.copy(), K22_loss=losses.copy(),
            K11_bh=I.copy(), K12_bh=I.copy(), K21_bh=I.copy(), K22_bh=I.copy(),
            K11_map=I.copy(), K12_map=I.copy(), K21_map=I.copy(), K22_map=I.copy(),
            K11_aperture=I.copy(), K12_aperture=I.copy(), K21_aperture=I.copy(), K22_aperture=I.copy(),
        )

    def _compute_knm_matrix_bayer_helms(
        self, sim,
        qx1, qy1, qx2, qy2,
        nr1, nr2,
        direction, Knm
    ):
        state = (qx1, qy1, qx2, qy2, nr1, nr2, self.xbeta.value, self.ybeta.value)

        if state == self._prev_bh[direction]:
            return
        else:
            self._prev_bh[direction] = state

        # port p1 -> p1 (reflection)
        if direction == KnmMirrorNodeDirection.MR11:
            xgamma = 2.0 * self.xbeta
            ygamma = 2.0 * self.ybeta
            nr = nr1
        # port p2 -> p2 (reflection)
        elif direction == KnmMirrorNodeDirection.MR22:
            xgamma = 2.0 * self.xbeta
            ygamma = -2.0 * self.ybeta
            nr = nr2
        # port p1 -> p2 (transmission)
        elif direction == KnmMirrorNodeDirection.MR12:
            GAMMA_FACTOR = -(1 - nr1 / nr2)
            xgamma = GAMMA_FACTOR * self.xbeta
            ygamma = GAMMA_FACTOR * self.ybeta
            nr = nr2
        # port p2 -> p1 (transmission)
        elif direction == KnmMirrorNodeDirection.MR21:
            GAMMA_FACTOR = (1 - nr2 / nr1)
            xgamma = -GAMMA_FACTOR * self.xbeta
            ygamma = GAMMA_FACTOR * self.ybeta
            nr = nr1

        run_bayer_helms(
            Knm,
            qx1, qy1, qx2, qy2,
            xgamma, ygamma,
            nr,
            sim.homs,
            sim.model.lambda0,
            reverse_gouy=False
        )

    def _apply_knm_sequence(self, sim, qx1, qy1, qx2, qy2, nr1, nr2, direction):
        """Compute the coupling coefficient matrices in the correct order."""
        # Get the correct knm matrices
        if direction == KnmMirrorNodeDirection.MR11:
            Knm = self.knm_matrices.K11
            Knm_loss = self.knm_matrices.K11_loss
            Knm_bh = self.knm_matrices.K11_bh
            Knm_map = self.knm_matrices.K11_map
            Knm_aperture = self.knm_matrices.K11_aperture
            turn = True
        elif direction == KnmMirrorNodeDirection.MR22:
            Knm = self.knm_matrices.K22
            Knm_loss = self.knm_matrices.K22_loss
            Knm_bh = self.knm_matrices.K22_bh
            Knm_map = self.knm_matrices.K22_map
            Knm_aperture = self.knm_matrices.K22_aperture
            turn = True
        elif direction == KnmMirrorNodeDirection.MR12:
            Knm = self.knm_matrices.K12
            Knm_loss = self.knm_matrices.K12_loss
            Knm_bh = self.knm_matrices.K12_bh
            Knm_map = self.knm_matrices.K12_map
            Knm_aperture = self.knm_matrices.K12_aperture
            turn = False
        elif direction == KnmMirrorNodeDirection.MR21:
            Knm = self.knm_matrices.K21
            Knm_loss = self.knm_matrices.K21_loss
            Knm_bh = self.knm_matrices.K21_bh
            Knm_map = self.knm_matrices.K21_map
            Knm_aperture = self.knm_matrices.K21_aperture
            turn = False

        # TODO (sjr) map coupling coefficient calculations
        # compute coupling coefficients via Bayer-Helms
        self._compute_knm_matrix_bayer_helms(
            sim, qx1, qy1, qx2, qy2, nr1, nr2, direction, Knm_bh
        )

        # TODO (sjr) aperture calculations

        # compute combined Knm matrix
        Knm_no_rgouy = Knm_aperture @ Knm_bh @ Knm_map

        # apply the reverse gouy phase
        rev_all_gouy(Knm_no_rgouy, sim.homs, qx1, qy1, qx2, qy2, out=Knm)
        # zero the phase of TEM00 -> TEM00 coefficient
        if sim.model.phase_config.ZERO_K00: zero_tem00_phase(Knm)

        if turn: flip_odd_horizontal(Knm, sim.homs)

        # compute total losses from each (n,m) to all
        # couplings (for quantum noise calcs)
        knm_loss(Knm, out=Knm_loss)

    def _compute_knm_matrices(self, sim):
        if not sim.is_modal or (not self._model._trace_changed and not self._angle_changed):
            LOGGER.info("No knm dependent parameters have changed, skipping recomputation "
                        f"of knm matrices for: {self.name}")
            return

        LOGGER.info(f"Computing knm matrices for: {self.name}")

        # get beam parameters...
        node_q_map = sim.model.last_trace
        qx_p1o, qy_p1o, _ = node_q_map[self.p1.o]
        qx_p1i, qy_p1i, _ = node_q_map[self.p1.i]
        qx_p2o, qy_p2o, _ = node_q_map[self.p2.o]
        qx_p2i, qy_p2i, _ = node_q_map[self.p2.i]

        # ... and refractive indices of spaces attached to mirror
        nr1, nr2 = refractive_indices(self.p1, self.p2)

        if nr1 == 0:
            raise ValueError(f"refractive index nr1 zero in _compute_knm_matrices of {self}")
        if nr2 == 0:
            raise ValueError(f"refractive index nr2 zero in _compute_knm_matrices of {self}")

        # compute propagations of q -> q' for each coupling
        # to be used as the input bases for computing knm
        qx_p1i_prefl = apply(self.ABCD(self.p1.i, self.p1.o, 'x'), qx_p1i.q, nr1, nr1)
        qy_p1i_prefl = apply(self.ABCD(self.p1.i, self.p1.o, 'y'), qy_p1i.q, nr1, nr1)
        qx_p2i_prefl = apply(self.ABCD(self.p2.i, self.p2.o, 'x'), qx_p2i.q, nr2, nr2)
        qy_p2i_prefl = apply(self.ABCD(self.p2.i, self.p2.o, 'y'), qy_p2i.q, nr2, nr2)
        qx_p1i_ptrns = apply(self.ABCD(self.p1.i, self.p2.o, 'x'), qx_p1i.q, nr1, nr2)
        qy_p1i_ptrns = apply(self.ABCD(self.p1.i, self.p2.o, 'y'), qy_p1i.q, nr1, nr2)
        qx_p2i_ptrns = apply(self.ABCD(self.p2.i, self.p1.o, 'x'), qx_p2i.q, nr2, nr1)
        qy_p2i_ptrns = apply(self.ABCD(self.p2.i, self.p1.o, 'y'), qy_p2i.q, nr2, nr1)

        # K11 calculations
        self._apply_knm_sequence(
            sim, qx_p1i_prefl, qy_p1i_prefl, qx_p1o.q, qy_p1o.q,
            nr1, nr2,
            KnmMirrorNodeDirection.MR11
        )

        # K22 calculations
        self._apply_knm_sequence(
            sim, qx_p2i_prefl, qy_p2i_prefl, qx_p2o.q, qy_p2o.q,
            nr1, nr2,
            KnmMirrorNodeDirection.MR22
        )

        # K12 calculations
        self._apply_knm_sequence(
            sim, qx_p1i_ptrns, qy_p1i_ptrns, qx_p2o.q, qy_p2o.q,
            nr1, nr2,
            KnmMirrorNodeDirection.MR12
        )

        # K21 calculations
        self._apply_knm_sequence(
            sim, qx_p2i_ptrns, qy_p2i_ptrns, qx_p1o.q, qy_p1o.q,
            nr1, nr2,
            KnmMirrorNodeDirection.MR21
        )

        matrices_to_log = self._knm_logging_info["matrices"]
        couplings_to_log = self._knm_logging_info["couplings"]
        if "K11" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K11, sim.homs, "K11 = \n", couplings_to_log))
        if "K22" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K22, sim.homs, "K22 = \n", couplings_to_log))
        if "K12" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K12, sim.homs, "K12 = \n", couplings_to_log))
        if "K21" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K21, sim.homs, "K21 = \n", couplings_to_log))

