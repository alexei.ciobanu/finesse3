import numpy as np
import logging

from finesse.components.general import Connector
from finesse.components.node import NodeDirection, NodeType

LOGGER = logging.getLogger(__name__)


class Joint(Connector):
    """
    A class representing a mechanical joint between two mechanical ports
    in the interferometer configuration.

    TODO:
        - Need some way of specifying transfer functions for coupling
          between the two ports in question.
    """

    def __init__(self, name=None, portA=None, portB=None, connections=None):
        """
        Constructs a new instance of `Joint` with the specified properties.

        Parameters
        ----------
        name : str, optional
            Name of newly created :class:`.Joint` instance.

        portA : :class:`.Port`, optional
            Port to connect

        portB : :class:`.Port`, optional
            Port to connect

        connections : ?
        """
        if (portA is None) != (portB is None):
            LOGGER.warn("Can't construct a joint with only one port "
                        "connected, ignoring ports.")
            portA = None
            portB = None

        if portA is not None and portA.type != NodeType.MECHANICAL:
            raise Exception("PortA argument is not a mechanical port")
        if portB is not None and portB.type != NodeType.MECHANICAL:
            raise Exception("PortB argument is not a mechanical port")

        if name is None:
            if portA is not None and portB is not None:
                compA = portA.component.name
                compB = portB.component.name
                name = f"{compA}_{portA.name}__{compB}_{portB.name}"
            else:
                raise ValueError("Cannot create an unconnected wire without "
                                 "providing a name")

        super().__init__(name)
        self._add_to_model_namespace = False
        """
        The mechanical ports have nodes that are the motion degrees of
        freedom, so there can potentially be many, but the usual are z,
        yaw, pitch. We just make copies of the nodes here that are in
        each mechanical port.
        """
        self._add_port('n1', NodeType.MECHANICAL)
        self._add_port('n2', NodeType.MECHANICAL)

        if portA is not None and portB is not None:
            self.connect(portA, portB)

    def connect(self, portA, portB):
        """
        Sets the ports of this `Joint`.

        Parameters
        ----------
        portA : :class:`.Port`, optional
            Port to connect

        portB : :class:`.Port`, optional
            Port to connect
        """
        for n in portA.nodes:
            self.n1._add_node(n.name, None, n)

        for n in portB.nodes:
            self.n2._add_node(n.name, None, n)
        """
        I'm not sure of the perfect way to link up two mechanical ports.
        For now I'm going to link up matching names. So z motion of one
        mech port is linked to the z node of the other.

        If there is only one node in a port in both then just connect
        them up.
        """
        if len(portA.nodes) == len(portB.nodes) == 1:
            self._register_node_coupling(portA.nodes[0], portB.nodes[0])
        else:
            for na in portA.nodes:
                for nb in portB.nodes:
                    if na.name == nb.name:
                        self._register_node_coupling(na, nb)

    def _on_build(self, sim):
        # TODO: (sjr) does this need to have rows > 1 for HOM simulations?
        self.__I = np.eye(len(sim.homs) if sim.is_modal else 1, dtype=np.complex128)

    def _fill_matrix(self, sim):
        if not sim.is_audio:
            return
        for c in list(self._registered_connections):
            with sim.component_edge_fill(self, c, None, None) as mat:
                mat[:] = self.__I


class PZT(Connector):
    def __init__(self, name):

        super().__init__(name)

        self._add_port('pzt', NodeType.ELECTRICAL)
        self.p1._add_node("i", NodeDirection.INPUT)

        self._add_port('mech', NodeType.MECHANICAL)
        self.mech._add_node("z")

        self._register_node_coupling(self.p1.i, self.mech.o)

    def _fill_matrix(self, sim):
        if sim.is_audio:
            for freq in sim.frequencies:
                with sim.component_edge_fill(self, 'pzt.i->mech.z', freq, None) as mat:
                    mat[:] = self.__I
