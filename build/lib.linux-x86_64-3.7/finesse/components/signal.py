"""
Signal-type electrical component for producing signal inputs.
"""

import numpy as np

from finesse.components.general import Connector
from finesse.components.node import NodeType, NodeDirection


class SignalGenerator(Connector):
    """
    An object which produces a signal with a given amplitude and phase.
    """
    def __init__(self, name, node, amplitude=1, phase=0):
        """
        Constructs a new `SignalGenerator` element which is used to inject signals into
        a model. The

        Parameters
        ----------
        name : str
            Name of the SignalGenerator instance.
        
        node : .class:`finesse.components.node.Node`
            A node to inject a signal into
        
        amplitude : float, optional
            Amplitude of the signal, units depends on the type of the `node` or `type` parameter

        phase : float, optional
            Phase-offset of the signal from the default, defaults to zero.
        
        """
        Connector.__init__(self, name)
        
        self._add_port('port', node.type)
        self.port._add_node('o', None, node)

        self.__amplitude = amplitude
        self.__phase = phase

    @property
    def amplitude(self):
        """The amplitude of the signal (in V).

        :getter: Returns the signal amplitude.
        :setter: Sets the signal amplitude.
        """
        return self.__amplitude

    @amplitude.setter
    def amplitude(self, value):
        self.__amplitude = value

    @property
    def phase(self):
        """The phase of the signal (in degrees).

        :getter: Returns the signal phase.
        :setter: Sets the signal phase.
        """
        return self.__phase

    @phase.setter
    def phase(self, value):
        self.__phase = value

    def _fill_rhs(self, sim):
        if not sim.is_audio:
            return

        A = self.amplitude * np.exp(1j * self.phase * np.pi/180)
        sim.M().set_rhs(sim.field(self.port.o), A)
