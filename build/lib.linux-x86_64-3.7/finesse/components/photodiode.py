"""
A components sub-module containing classes for detecting power at
a physical point in a configuration.
"""

import numpy as np

from finesse.components.general import Connector, Surface
from finesse.components.node import NodeDirection, NodeType

class Photodiode(Connector):
    def __init__(self, name, qeff=1):

        super().__init__(name)

        self.qeff = qeff
        self._add_port('p1', NodeType.OPTICAL)
        self.p1._add_node("i",  NodeDirection.INPUT)
        self.p1._add_node("o",  NodeDirection.OUTPUT)

        self._add_port('p2', NodeType.ELECTRICAL)
        self.p2._add_node("i",  NodeDirection.INPUT)
        self.p2._add_node("o",  NodeDirection.OUTPUT)

        self._register_node_coupling(self.p1.i, self.p2.o)
        self._register_node_coupling(self.p2.i, self.p1.o)

    def _on_build(self, sim):
        # Probably need to do some bullseye, QPD calculations/setup here
        # This vector should describe how all the HOM beat to produce a
        # photocurrent
        self.__I = np.eye(sim.nhoms, dtype=np.complex128)

    def _fill_matrix(self, sim):
        if sim.is_audio:
            for freq in sim.frequencies:
                # Get the carrier HOMs for this frequency
                #E = carrier.out[carrier.field(self.p1.i, freq.audio_carrier_index)]
                E = sim.DC.get_DC_out(self.p1.i, freq.audio_carrier_index)
                with sim.component_edge_fill(self, 'p1.i->p2.o', freq, None) as mat:
                    mat[:] = E * self.qeff * self.__I

