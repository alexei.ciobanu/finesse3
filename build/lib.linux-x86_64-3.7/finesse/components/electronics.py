import numpy as np
import signal

from finesse.components.general import Connector
from finesse.components.node import NodeDirection, NodeType

class FilterZPK(Connector):
    def __init__(self, name, zpk):
        super().__init__(name)

        self.zpk = list(zpk)

        self._add_port("p1", NodeType.ELECTRICAL)
        self.p1._add_node('i', NodeDirection.INPUT)
        self.p1._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p2", NodeType.ELECTRICAL)
        self.p2._add_node('i', NodeDirection.INPUT)
        self.p2._add_node('o', NodeDirection.OUTPUT)

        self._register_node_coupling(self.p1.i, self.p2.o)
        self._register_node_coupling(self.p2.i, self.p1.o)

    def eval(self, f):
        import scipy
        return scipy.signal.freqs_zpk(*self.zpk, 2*np.pi*f)[1]

    def _on_build(self, sim):
        # TODO: (sjr) does this need to have rows > 1 for HOM simulations?
        self.__I = np.eye(sim.nhoms, dtype=np.complex128)

    def _fill_matrix(self, sim):
        if sim.is_audio:
            Hf = self.__I * self.eval(float(sim.model.fsig.f))

            for freq in sim.frequencies:
                with sim.component_edge_fill(self, 'p1.i->p2.o', None, None) as mat:
                    mat[:] = Hf
                with sim.component_edge_fill(self, 'p2.i->p1.o', None, None) as mat:
                    mat[:] = Hf
