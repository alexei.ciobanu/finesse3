"""
Optical components performing modulation of beams.
"""
from dataclasses import dataclass
import enum
import logging

import numpy as np

from finesse.components.general import InteractionType, Connector, FrequencyGenerator
from finesse.components.node import NodeType, NodeDirection
from finesse.element import ModelElement, model_parameter, Rebuild
from finesse.knm import run_bayer_helms, rev_all_gouy, zero_tem00_phase
from finesse.knm import log_knm_matrix
from finesse.utilities.maths import apply_ABCD as apply
from finesse.utilities import refractive_indices

LOGGER = logging.getLogger(__name__)

class KnmModulatorNodeDirection(enum.IntEnum):
    MD12 = 1
    MD21 = 2

@dataclass
class ModulatorKnmMatrices:
    K12: np.ndarray
    K21: np.ndarray

@model_parameter('f',     None, Rebuild.Frequencies & Rebuild.PlaneWave, units='Hz')
@model_parameter('midx',  None, Rebuild.PlaneWave)
@model_parameter('phase', None, Rebuild.PlaneWave, units="degrees")
class Modulator(Connector, FrequencyGenerator):
    """
    A class representing a modulator optical component with associated
    properties such as modulation frequency, index and order.
    """
    def __init__(self, name, f, midx, order=1, mod_type="pm", phase=0):
        """Constructs a new `Modulator` with the specified properties.

        Parameters
        ----------
        name : str
            Name of newly created `Modulator` instance.

        f : float or :class:`.SourceFrequency` or :class:`.Frequency`, optional
            Frequency of the modulation (in Hz) or :class:`.SourceFrequency` or
            :class:`.Frequency` object.

        midx : float
            Modulation index, >= 0.

        order : int, optional
            Maximum order of modulations to produce. Must be 1 for
            amplitude modulation. Defaults to 1.

        mod_type : str, optional
            Modulation type, either 'am' (:ref:`amplitude
            modulation<amp_mod>`) or 'pm' (:ref:`phase
            modulation<phase_mod>`). Defaults to 'am'.

        phase : float, optional
            Relative phase of modulation (in degrees). Defaults to 0.
        """
        Connector.__init__(self, name)
        FrequencyGenerator.__init__(self)

        if mod_type == "am" and order > 1:
            raise ValueError("Amplitude modulation can only have order 1")

        self.f = f
        self.midx = midx
        self.phase = phase

        self.order = order
        self.mod_type = mod_type

        self._add_port("p1", NodeType.OPTICAL)
        self.p1._add_node('i', NodeDirection.INPUT)
        self.p1._add_node('o', NodeDirection.OUTPUT)

        self._add_port("p2", NodeType.OPTICAL)
        self.p2._add_node('i', NodeDirection.INPUT)
        self.p2._add_node('o', NodeDirection.OUTPUT)

        self.__coupling_orders = {}

        # optic to optic couplings
        self._register_node_coupling(self.p1.i, self.p2.o,
                                     interaction_type=InteractionType.TRANSMISSION)
        self._register_node_coupling(self.p2.i, self.p1.o,
                                     interaction_type=InteractionType.TRANSMISSION)

        self._knm_logging_info = {
            "matrices" : [], #set(["K12", "K21"]),
            "couplings" : None
        }

    def _modulation_frequencies(self):
        orders = list(range(-self.order, 1+self.order))
        orders.pop(self.order) # remove 0
        fm = np.dot(self.f.ref, orders)
        return fm

    def _couples_frequency(self, sim, connection, f_in, f_out):
        if f_in == f_out:
            return True
        elif (f_in, f_out) in self.__coupling_orders[sim]:
            return True
        elif (f_out, f_in) in self.__coupling_orders[sim]:
            return True
        else:
            return False

    @property
    def f(self):
        """:class:`.SourceFrequency` representing modulation.

        Returns
        -------
        :class:`.SourceFrequency`
            The modulation frequency.
        """
        return self.__f

    @f.setter
    def f(self, value):
        self.__f = value

    @property
    def order(self):
        """The maximum order of modulations produced by
        the modulator.

        Returns
        -------
        int
            The maximum modulation order.
        """
        return self.__order

    @order.setter
    def order(self, value):
        self.__order = int(value)

    @property
    def phase(self):
        """The relative phase of modulation (in degrees).

        Returns
        -------
        float
            Relative modulation phase of the modulator.
        """
        np.rad2deg(self.__phase)

    @phase.setter
    def phase(self, value):
        self.__phase = np.radians(value)

    def _on_build(self, sim):
        self.__coupling_orders[sim] = {}

        self.__I = np.eye(sim.nhoms, dtype=np.complex128)
        self._allocate_knm_matrices(sim)
        #self._compute_knm_matrices(sim)
        self.K12 = self.knm_matrices.K12
        self.K21 = self.knm_matrices.K21

        for f1 in sim.frequencies:
            self.__coupling_orders[sim][f1, f1] = 0
            for f2 in sim.frequencies:
                df = f2.f - f1.f
                order = df / float(self.f)

                if not order.is_integer(): continue

                order = int(round(order))

                if abs(order) <= self.order:
                    self.__coupling_orders[sim][f1, f2] =  order
                    self.__coupling_orders[sim][f2, f1] = -order

    def _on_unbuild(self, sim):
        if sim in self.__coupling_orders: del self.__coupling_orders[sim]

    def _update_source_frequency(self, sim, freq):
        cporder = self.__coupling_orders[sim]

        for f2 in self._frequencies():
            if (freq, f2) not in cporder:
                continue

            df = f2.f - freq.f
            order = df / self.f.f.eval()

            if not order.is_integer(): continue

            order = int(round(order))

            if abs(order) <= self.order:
                cporder[freq, f2] =  order
                cporder[f2, freq] = -order

    def _fill_matrix(self, sim):
        from scipy.special import jn
        # TODO: Test phases
        p1p2 = "p1.i->p2.o"
        p2p1 = "p2.i->p1.o"
        orders = np.zeros(2 * self.order + 1)
        cporders  = self.__coupling_orders[sim]

        for i in range(-self.order, self.order + 1):
            orders[i] = i

        if self.mod_type == "am":
            factors = (self.midx.eval() / 4) * np.exp(1j * self.phase.value * orders)
            factors[0] = 1 - self.midx.eval() / 2
        elif self.mod_type == "pm":
            factors = np.array(jn(orders, self.midx.eval()), dtype=np.complex128)
            factors *= np.exp(-1j * (self.phase.value - np.pi / 2) * orders)

        factors = np.multiply.outer(factors, self.__I)

        for (f1, f2), order in cporders.items():
            with sim.component_edge_fill(self, p1p2, f1, f2) as mat:
                mat[:] = factors[order][:]
            with sim.component_edge_fill(self, p2p1, f1, f2) as mat:
                mat[:] = factors[order][:]

    def _allocate_knm_matrices(self, sim):
        I = np.eye(sim.nhoms, dtype=np.complex128)

        self.knm_matrices = ModulatorKnmMatrices(
            K12=I.copy(), K21=I.copy()
        )

    def _compute_knm_matrix_bayer_helms(
        self, sim,
        qx1, qy1, qx2, qy2,
        nr, Knm
    ):
        run_bayer_helms(
            Knm,
            qx1, qy1, qx2, qy2,
            0.0, 0.0,
            nr,
            sim.homs,
            sim.model.lambda0,
            reverse_gouy=False
        )

    def _apply_knm_sequence(self, sim, qx1, qy1, qx2, qy2, nr1, nr2, direction):
        if direction == KnmModulatorNodeDirection.MD12:
            Knm = self.knm_matrices.K12
            nr = nr2
        elif direction == KnmModulatorNodeDirection.MD21:
            Knm = self.knm_matrices.K21
            nr = nr1

        self._compute_knm_matrix_bayer_helms(
            sim, qx1, qy1, qx2, qy2, nr, Knm
        )

        # TODO (sjr) do pitch/yaw or w0/z mod type calculations - see lines 3007 - 3216 in kat_aa.c

        rev_all_gouy(Knm, sim.homs, qx1, qy1, qx2, qy2)
        # zero the phase of TEM00 -> TEM00 coefficient
        if sim.model.phase_config.ZERO_K00: zero_tem00_phase(Knm)

    def _compute_knm_matrices(self, sim):
        if not sim.is_modal or not self._model._trace_changed:
            LOGGER.info("No knm dependent parameters have changed, "
                        "skipping recomputation of knm matrices for:"
                        f"{self.name}")
            return

        LOGGER.info(f"Computing knm matrices for: {self.name}")

        # get beam parameters
        node_q_map = sim.model.last_trace
        qx_p1o, qy_p1o, _ = node_q_map[self.p1.o]
        qx_p1i, qy_p1i, _ = node_q_map[self.p1.i]
        qx_p2o, qy_p2o, _ = node_q_map[self.p2.o]
        qx_p2i, qy_p2i, _ = node_q_map[self.p2.i]

        nr1, nr2 = refractive_indices(self.p1, self.p2)

        # compute propagations of q -> q' for each coupling
        # to be used as the input bases for computing knm
        qx_p1i_ptrns = apply(self.ABCD(self.p1.i, self.p2.o, 'x'), qx_p1i.q, nr1, nr2)
        qy_p1i_ptrns = apply(self.ABCD(self.p1.i, self.p2.o, 'y'), qy_p1i.q, nr1, nr2)
        qx_p2i_ptrns = apply(self.ABCD(self.p2.i, self.p1.o, 'x'), qx_p2i.q, nr2, nr1)
        qy_p2i_ptrns = apply(self.ABCD(self.p2.i, self.p1.o, 'y'), qy_p2i.q, nr2, nr1)

        # K12 calculations
        self._apply_knm_sequence(
            sim, qx_p1i_ptrns, qy_p1i_ptrns, qx_p2o.q, qy_p2o.q,
            nr1, nr2, KnmModulatorNodeDirection.MD12
        )

        # K21
        self._apply_knm_sequence(
            sim, qx_p2i_ptrns, qy_p2i_ptrns, qx_p1o.q, qy_p1o.q,
            nr1, nr2, KnmModulatorNodeDirection.MD21
        )

        matrices_to_log = self._knm_logging_info["matrices"]
        couplings_to_log = self._knm_logging_info["couplings"]
        if "K12" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K12, sim.homs, "K12 = \n", couplings_to_log))
        if "K21" in matrices_to_log:
            LOGGER.info(log_knm_matrix(self.knm_matrices.K21, sim.homs, "K21 = \n", couplings_to_log))
