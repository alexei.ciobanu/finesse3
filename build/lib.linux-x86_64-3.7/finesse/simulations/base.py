"""
Sub-module consisting of the :class:`.Simulation` for
performing specific executions of a :class:`.Model`.
"""

import logging
import weakref
import numpy as np
import re
import networkx as nx
import contextlib

from collections import OrderedDict, namedtuple
from finesse.components import FrequencyGenerator, Space
from finesse.components.node import NodeType
from finesse.freeze import canFreeze
from finesse.frequency import Frequency, generate_frequency_list
from finesse.enums import SpatialType
from finesse.element import Symbol

LOGGER = logging.getLogger(__name__)


@canFreeze
class BaseSimulation(object):
    """
    Base Simulation class for executing a model.

    The `Simulation` class uses the :class:`.Model` to build
    and fill the sparse matrix and is used as a reference for
    detectors to compute outputs to return to the user.

    .. note::
        Instances of this class **cannot** be copied, they are generated
        from and operate on a :class:`.Model` object in a particular
        built state.

    .. note::
        The module :mod:`pickle` **cannot** be used on instances of this class.
    """
    def __init__(self, model, name, frequencies=None, is_audio=False):
        self._model       = model
        self._name        = name
        self._to_update   = []
        self._audio_freq  = bool(is_audio)
        self._input_frequencies = frequencies
        self._frequency_map = None

        # register that this simulation is dependant on this model
        model._register_simulation(self)

    @property
    def is_audio(self):
        return self._audio_freq

    @property
    def is_modal(self):
        return self.model.spatial_type == SpatialType.MODAL

    @property
    def frequency_map(self):
        return self._frequency_map

    @property
    def model(self):
        """A reference to the underlying :class:`.Model` instance.

        :getter: Returns a reference to the :class:`.Model` instance
                 used by this simulation (read-only).
        """
        return self._model

    @property
    def nodes(self):
        """The nodes of the underlying model.

        :getter: Returns a list of the simulation's nodes (read-only).
        """
        return self._nodes

    @property
    def homs(self):
        return self._HOMs

    @property
    def nhoms(self):
        return self._HOMs.shape[0]

    @property
    def frequencies(self):
        """The :class:`.Frequency` objects present in this simulation.

        :getter: Returns a list of the simulation's frequencies (read-only).
        """
        return self._frequencies

    @property
    def name(self):
        """Name of the simulation.

        :getter: Returns the name of the simulation (read-only).
        """
        return self._name

    @property
    def num_equations(self):
        r"""
        The number of equations the configuration consists of, given by,

        .. math::
            n_{\mathrm{eq}} = n_{\mathrm{nodes}} n_{\mathrm{freq}} n_{\mathrm{HOM}},

        where :math:`n_{\mathrm{nodes}}` is the number of nodes in the network,
        :math:`n_{\mathrm{freq}}` is the number of frequencies and
        :math:`n_{\mathrm{HOM}}` is the number of higher-order modes.

        :getter: Returns the numbers of equations describing the system (read-only).
        """
        return len(self.model.network.nodes) * len(self._frequencies) * self._HOMs.shape[0]

    def findex(self, node, freq):
        """
        Returns simulation unique index for a given frequency at this node.
        Used to refer to submatrices of HOMs in the interferometer matrix.

        Parameters
        ----------
        node : :class:`.Node`
            Node object to get the index of.
        freq : int
            Frequency index.

        Returns
        -------
        index : int
            Index of the `node` for a given frequency.
        """
        assert(freq < self._node_info[node].nfreqs)  # Requested frequency not available for this node
        return self._node_info[node].freq_index + freq

    def field(self, node, freq=0, hom=0):
        """
        Returns simulation unique index of a field at a particular frequency
        index at this node.

        Parameters
        ----------
        node : :class:`.Node`
            Node object to get the index of.
        freq : int
            Frequency index.
        hom : int, optional
            Higher Order Mode index, defaults to zero.
        """
        Nf = self._node_info[node].nfreqs
        Nh = self._node_info[node].nhoms
        assert(freq < Nf)  # Requested frequency not available for this node
        assert(hom < Nh)   # Requested hom not available for this node
        return self._node_info[node].rhs_index + freq * Nh + hom

    def get_DC_out(self, node, freq=0, hom=0):
        return self.out[self.field(node, freq, hom)]

    #def __getitem__(self, key):
    #    return self._submatrices[key]

    @contextlib.contextmanager
    def component_edge_fill(self, comp, edgestr, f1, f2, conjugate = False):
        """
        Returns a matrix for the submatrix an element has requested
        for different connections it needs. The key is:

            (element, connection_name, ifreq, ofreq)

        element : finesse.component.Connector
            Is the object reference that created the requests
        connection_name : str
            String name given to the connection
        ifreq : finesse.Frequency
            Incoming frequency
        ofreq : finesse.Frequency
            Outgoing frequency

        this is a context manager, to be used like
        with sim.component_edge_fill(key) as mat:
          mat[:] = computations

        Returns
        -------
        matrix
        """
        #the two-stage nature of this will make some fill checks and hooks
        #much more safe (and powerfull)

        #this will be helpful for on-demand filling and will also help improve
        #make DC simulation of squeezers work (because post-fill transformations
        #will be needed)
        key = (comp, edgestr, f1, f2)
        mat = self._submatrices[key]
        yield mat
        #check things, or transform things here
        if conjugate:
            mat[:].imag *= -1
        return

    def component_edge_get(self, comp, edgestr, f1, f2):
        """
        Returns a matrix for the submatrix an element has requested
        for different connections it needs. The key is:

            (element, connection_name, ifreq, ofreq)

        element : finesse.component.Connector
            Is the object reference that created the requests
        connection_name : str
            String name given to the connection
        ifreq : finesse.Frequency
            Incoming frequency
        ofreq : finesse.Frequency
            Outgoing frequency

        this is a context manager, to be used like
        with sim.component_edge_fill(key) as mat:
          mat[:] = computations

        Returns
        -------
        matrix
        """
        key = (comp, edgestr, f1, f2)
        return self._submatrices[key]

    def compute_knm(self):
        for fn in self._compute_knm_matrices:
            fn(self)

    def iter_edges(self):
        _done = {}
        for owner in self._edge_owners:
            if owner in _done:
                continue

            couples_f = isinstance(owner, FrequencyGenerator)

            # For each connection this element wants...
            for name in owner._registered_connections:
                nio = tuple((owner.nodes[_] for _ in owner._registered_connections[name]))  # convert weak ref (input, output)
                # If we are a carrier matrix only compute optics, no AC
                if not self.is_audio:
                    if (nio[0].type is not NodeType.OPTICAL or nio[1].type is not NodeType.OPTICAL):
                        continue

                # Loop over all the frequencies we can couple between and add
                # submatrixes to the overall model
                for i, ifreq in enumerate(self.frequencies):
                    for j, ofreq in enumerate(self.frequencies):
                        #for k in range(Nhom):
                        # For each input and output frequency check if our
                        # element wants to couple them at this
                        if couples_f and not owner._couples_frequency(self, name, ifreq, ofreq):
                            continue
                        elif not couples_f and ifreq != ofreq:
                            # If it doesn't couple frequencies and the
                            # frequencies are different then ignore
                            continue

                        iodx_semantic = []  # submatrix indices
                        # Get simulation unique indices for submatrix
                        # position. How we get these depends on the type of
                        # the nodes involved
                        for freq, node in zip((ifreq, ofreq), nio):
                            if node.type is NodeType.OPTICAL:
                                iodx_semantic.append((node, freq))
                            else:
                                # Mechanical and electrical don't have multiple
                                # freqs, so always use the zeroth frequency index
                                iodx_semantic.append((node, self._frequencies[0]))

                        n_fr, n_to = iodx_semantic
                        yield (n_fr, n_to)
            _done[owner] = True

    def get_changing_edges(self):
        """
        Returns
        -------
        set() of the node-obj, freq-obj pairs
        (TODO should perhaps be node-name and freq-idx)
        """
        edges_changing = set()
        edges_phys, nodes  = self.model.get_changing_edges_elements()
        #TODO
        for Mn_fr, Mn_to in self.iter_edges():
            n_fr, f_fr = Mn_fr
            n_to, f_to = Mn_to
            #print(n_fr, n_to)
            if (n_fr.full_name, n_to.full_name) in edges_phys:
                edges_changing.add((Mn_fr, Mn_to))
        return edges_changing

    def set_lasertem_spaces_gouy_phase(self):
        for fn in self._set_gouy_phases:
            fn(self)

    def fill(self, fill_all=True):
        if fill_all:
            todo = self._edge_owners
        else:
            todo = self._to_update

        for owner in todo:
            owner._fill_matrix(self)

    def fill_rhs(self):
        for fn in self._fill_rhs:
            fn(self)

    def fill_qnoise_rhs(self):
        if self.is_modal:
            Nhom = self._HOMs.shape[0]
        else:
            Nhom = 1

        for comp in self._components:
            # Fill in all open port vacuum contributions
            for port in comp.ports:
                if port.type != NodeType.OPTICAL or port.is_connected:
                    continue
                for freq in self.frequencies:
                    for hom in range(Nhom):
                        idx = self.field(port.i, freq.index, hom)
                        self._Mq[idx] = self.model._UNIT_VACUUM / 2 * (1 + freq.carrier.f / self.model.f0)
            # Then ask components for their noise contributions
            if hasattr(comp, "_fill_qnoise_rhs"):
                comp._fill_qnoise_rhs(self)

    def get_frequency_object(self, frequency):
        """
        This returns a :class:`finesse.frequency.Frequency` object.

        Parameters
        ----------
        f : Number or :class:`finesse.element.Symbol`
            Frequency to search for in this simulation
        """
        if isinstance(frequency, Symbol):
            if frequency.is_changing:
                # if it's tunable we want to look for the symbol that is just this
                # lasers frequency, as it will be changing
                for f in self.frequencies:
                    if f.symbol == frequency:
                        return f

        f_value = float(frequency)
        # otherwise do some value comparisons
        for f in self.frequencies:
            if float(f.f) == f_value:
                return f

        return None

    def _initialise(self):
        self._HOMs         = self.model.homs.copy()
        self._components   = self.model.components
        self._detectors    = self.model.detectors
        self._submatrices  = OrderedDict()
        self._node_info    = OrderedDict()
        self.NodeInfoEntry = namedtuple("NodeInfoEntry", "index rhs_index freq_index nfreqs nhoms")

        network = self.model.network

        if not self.is_audio:
            LOGGER.info("Building DC simulation")
            # For now we just get all the optical nodes to simulate in the DC matrix
            # convert from weakrefs
            self._nodes = {n: network.nodes[n]['weakref']()
                           for n in network.nodes
                           if 'optical' in network.nodes[n]}
        else:
            LOGGER.info("Building AC simulation")
            # For audio matrix we solve all types
            self._nodes = {n: network.nodes[n]['weakref']() for n in network.nodes}

        self._setup_frequencies()

        if self.is_modal:
            self._Nhom = self._HOMs.shape[0]
        else:
            self._Nhom = 1

        self._initialise_nodes()
        self._initialise_elements()
        self._initialise_submatrices()

        self.out = np.atleast_2d(np.zeros((self.num_equations, 1), dtype=complex))

    def _setup_frequencies(self):
        # If no frequencies were specified then generate a default set
        if self._input_frequencies is None:
            self._input_frequencies = generate_frequency_list(self.model)

        self._frequencies = []

        LOGGER.info('Generating simulation with carrier frequencies %s', self._input_frequencies)
        # Now loop over the requested frequencies for this simulation
        # and generate the frequency bin objects
        if not self.is_audio:
            for i, f in enumerate(self._input_frequencies):
                self._frequencies.append(
                    Frequency(str(f.eval(keep_changing_symbols=True)), self, f, index=i)
                )
        else:
            for i, f in enumerate(self._input_frequencies):
                fp = f + self.model.fsig.f.ref
                fm = f - self.model.fsig.f.ref
                self._frequencies.append(
                    Frequency(str(fp.eval(keep_changing_symbols=True)),
                              self, fp, index=2*i, audio_order=1,
                              audio=True, audio_carrier_index=i)
                )
                self._frequencies.append(
                    Frequency(str(fm.eval(keep_changing_symbols=True)),
                              self, fm, index=2*i+1, audio_order=-1,
                              audio=True, audio_carrier_index=i)
                )

        self._Nf = len(self._frequencies)

    def _initialise_nodes(self):
        Nhom = self._Nhom
        Nf   = self._Nf

        assert(Nhom > 0)
        assert(Nf > 0)

        s_rhs_idx = 0
        s_f_idx = 0

        for i, n in enumerate(self.nodes.values()):
            if n.type is NodeType.OPTICAL:
                #(index, RHS index, frequency index, num freqs, num HOM)
                Nsm   = Nf  # Each frequency is a submatrix of HOMs
                Neq = Nhom  # Number of equations in each submatrix
            elif n.type is NodeType.MECHANICAL:
                # Mechanics only has single-sided audio frequency term
                # TODO ddb: I suppose you could also have HOM mechanical resonances such as PI here
                Nsm = 1
                Neq = 1
            elif n.type is NodeType.ELECTRICAL:
                Nsm = 1  # TODO ddb: add electronic frequencies, will need new frequency list in the models
                Neq = 1  # no higher order modes of electronics as far as I'm aware...
            else:
                raise Exception("Not handled")

            self._node_info[n] = self.NodeInfoEntry(i, s_rhs_idx, s_f_idx, Nsm, Neq)
            s_rhs_idx += Neq * Nsm  # Track how many equations we are going through
            s_f_idx   += Nsm  # keep track of how many frequencies*nodes
            #print(n, self._node_info[n])

    def _initialise_elements(self):
        # Store all the edge owners we'll need to loop over and call fill on
        self._edge_owners = []
        # use set for unique edges
        for el in set(nx.get_edge_attributes(self.model.network, "owner").values()):
            self._edge_owners.append(el())

        # Store all the elements that might fill the RHS
        self._fill_rhs = []
        self._compute_knm_matrices = []
        self._set_gouy_phases = []

        # Get any callbacks for the elements in the model
        # tell the element that we have now built the model and it
        # should do some initialisations for running simulations
        for el in self.model.elements.values():
            if hasattr(el, "_fill_rhs"):
                self._fill_rhs.append(el._fill_rhs)

            if hasattr(el, "_compute_knm_matrices"):
                self._compute_knm_matrices.append(el._compute_knm_matrices)

            if hasattr(el, "_set_gouy_phase"):
                self._set_gouy_phases.append(el._set_gouy_phase)
            if hasattr(el, "_update_tem_gouy_phases"):
                self._set_gouy_phases.append(el._update_tem_gouy_phases)

            if hasattr(el, "_on_build"):
                el._on_build(self)

    def solve(self):
        """
        Solves the current state of the simulation. `self.out` should be populated with the solution vector
        once this has been called.
        """
        raise NotImplementedError()

    def __enter__(self):
        """
        When entered the Simulation object will create the matrix to be used in
        the simulation.

        This is where the matrix gets allocated and constructed. It will expect
        that the model structure does not change after this until it has been
        exited.
        """
        raise NotImplementedError()

    def _unbuild(self):
        # let elements know that we have now finished this
        # simulation and do any cleanup if they need to
        for el in self.model.elements.values():
            if hasattr(el, "_on_unbuild"):
                el._on_unbuild(self)

    def __exit__(self, type_, value, traceback):
        raise NotImplementedError

    def _fill(self, carriers=None, zero_rhs=True, fill_rhs=True, fill_matrix=True):
        if self.is_modal:
            self.model.beam_trace(**self.model.beam_trace_args)
            self.compute_knm()
            self.set_lasertem_spaces_gouy_phase()

        self._frequency_map = {f.f : f for f in self.frequencies}

        # fill_all True until we get a better method for detecting what stuff is being updated
        if fill_matrix:
            self.fill(fill_all=True)
        if zero_rhs:
            self._clear_rhs()
        if fill_rhs:
            self.fill_rhs()

    def run(self, carriers=None, zero_rhs=True, fill_rhs=True, fill_matrix=True):
        """Executes the simulation for model in its current state.

        Takes the following steps to compute an output:
         * Clears the RHS vector (if zero_rhs = True)
         * Fills the matrix (if keep_matrix = True)
         * Fills the RHS vector (if fill_rhs = True)
         * Solves
         * Fills the detector output list

        Results are saved in `Simulation.out` property.

        Parameters
        ----------
        carriers : :class:`Simulation`
            If running an audio sideband simulation then the carrier fields
            simulation object must be supplied.
        zero_rhs : boolean
            If true the input vector is not zeroed before filling. This is to enable you
            to inject custom intput vectors if desired.
        fill_rhs: boolean
            If True the input vector is filled, if not the current memory state will be used
        fill_matrix: boolean
            If True the matrix will be filled, if not the current memory state will be used
        Returns
        -------
        out : :class:`numpy.ndarray`
            The solved RHS vector.
        """
        if not self.model.is_built:
            raise Exception("Model has not been built")

        self._fill(carriers, zero_rhs, fill_rhs, fill_matrix)

        self.solve()

        # TODO ddb: if audio and quantum calcs needed
        if self.is_audio and False:
            self.fill_qnoise_rhs()

        return self.out

    def __copy__(self):
        raise Exception("Simulation objects can not be copied")

    def __deepcopy__(self, memo):
        raise Exception("Simulation objects can not be deepcopied")


class BaseMatrixSimulation(BaseSimulation):
    def active(self):
        return self._M is not None

    def print_matrix(self):
        self._M.print_matrix()

    def _clear_rhs(self):
        return self.M().clear_rhs()

    def set_source(self, field_idx, vector):
        self.M().set_rhs(self.field(*field_idx), vector)
        return

    def solve(self):
        if not nx.is_frozen(self.model.network):
            raise Exception("Model has not been built")

        # As we put -1 on diagonals we actually solve the negative
        # of the system, so inverse that here...
        # -1 here because we compute the negative of the interferometer
        # matrix. Why? Because the -1 sign only gets applied once along the
        # diagonal rather than for every non-diagonal element. Which means
        # equations are written as they are in math.
        self._M.refactor()
        self.out = -1*self._M.solve()
        return

    @property
    def M(self):
        """
        A weak reference to the underlying Matrix object.

        .. note::

            References to the Matrix should not be kept.

        :getter: Returns a weak reference to the underlying matrix (read-only).
        """
        return weakref.ref(self._M)

    @property
    def Mq(self):
        """
        A reference to the underlying quantum Matrix object.

        :getter: Returns a reference to the quantum matrix (read-only).
        """
        return self._Mq

    def _initialise_submatrices(self):
        # Add in the diagonal elements of the matrix
        for n, node_inf in self._node_info.items():
            Nsm = node_inf.nfreqs
            Neq = node_inf.nhoms
            for freq in range(Nsm):
                fidx = self.findex(n, freq)  # Get index for this submatrix
                self.M().add_diagonal_elements(Neq, fidx, f"I,node={n.full_name},f={freq},fidx={fidx},Neq={Neq}")

        _done = {}
        # Loop over every edge in the network which represents a bunch of
        # connections between frequencies and HOMs between two nodes
        for owner in self._edge_owners:
            if owner in _done:
                continue

            couples_f = isinstance(owner, FrequencyGenerator)

            # For each connection this element wants...
            for name in owner._registered_connections:
                # convert weak ref (input, output)
                nio = tuple((owner.nodes[_] for _ in owner._registered_connections[name]))

                # If we are a carrier matrix only compute optics, no AC electronics or mechanics
                if not self.is_audio:
                    if (nio[0].type is not NodeType.OPTICAL or nio[1].type is not NodeType.OPTICAL):
                        continue

                # Loop over all the frequencies we can couple between and add
                # submatrixes to the overall model
                for i, ifreq in enumerate(self.frequencies):
                    for j, ofreq in enumerate(self.frequencies):
                        # For each input and output frequency check if our
                        # element wants to couple them at this
                        if couples_f and not owner._couples_frequency(self, name, ifreq, ofreq):
                            continue
                        elif not couples_f and ifreq != ofreq:
                            # If it doesn't couple frequencies and the
                            # frequencies are different then ignore
                            continue

                        iodx = []  # submatrix indices
                        tags = []  # descriptive naming tags for submatrix key
                        key_name = re.sub(r"^[^.]*\.", "", name)
                        key_name = re.sub(r">[^.]*\.", ">", key_name)
                        key  = [owner, key_name]

                        # Get simulation unique indices for submatrix
                        # position. How we get these depends on the type of
                        # the nodes involved
                        for freq, node in zip((ifreq, ofreq), nio):
                            if node.type is NodeType.OPTICAL:
                                iodx.append(self.findex(node, freq.index))
                                tags.append(freq.name)
                                key.append(freq)
                            else:
                                # Mechanical and electrical don't have multiple
                                # freqs, so always use the zeroth frequency index
                                iodx.append(self.findex(node, 0))
                                tags.append('audio')
                                key.append(None)

                        assert(len(iodx) == 2)
                        assert(len(key) == 4)

                        if tuple(key) not in self._submatrices:
                            smname = "{}__{}__{}".format(name, *tags)

                            #print("Requesting:", *iodx, self.name, smname, i, j, nio)
                            # Then we get a view of the underlying matrix which we set the values with.
                            # Store one for each frequency. By requesting this view we are telling
                            # the matrix that these elements should be non-zero in the model

                            #TODO, there should be a better registry for diagonals
                            #support
                            if isinstance(owner, Space):
                                self._submatrices[tuple(key)] = self._M.get_sub_diagonal_view(*iodx, smname)
                            else:
                                self._submatrices[tuple(key)] = self._M.get_sub_matrix_view(*iodx, smname)
                        else:
                            # Check if we've just requested the same submatrix.
                            sm = self._submatrices[tuple(key)]
                            if sm.from_idx != iodx[0] or sm.to_idx != iodx[1]:
                                raise Exception("Requested submatrix has already been requested,"
                                                "but new one has different indices")
                            else:
                                continue

            _done[owner] = True



