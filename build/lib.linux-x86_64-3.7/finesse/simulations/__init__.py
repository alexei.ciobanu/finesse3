# -*- coding: utf-8 -*-
"""
Holds the various instances of simulation classes.

Listed below are all the sub-modules of the ``simulations`` module with
a brief description of the contents of each.

.. autosummary::
    :toctree: simulations/
    {contents}
"""

from .base import BaseSimulation
from .KLU import KLUSimulation
from .digraph import DigraphSimulation, DigraphSimulationBase
from .debug import DebugSimulation
from .dense import DenseSimulation

if __doc__:
    from finesse.utilities.misc import _collect_submodules

    __doc__ = __doc__.format(
        contents=_collect_submodules("simulations")
    )
