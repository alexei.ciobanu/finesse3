# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals
import numpy as np
import scipy.sparse
import scipy.sparse.linalg

from finesse.cmatrix import KLUMatrix

from .SREdense_numeric_inverse import SREdense_numeric_inverse

from .lin_op import (
    DeferredMatrixStore,
    MatrixStoreTypes,
)


class SREPresolvedCSC(object):
    @classmethod
    def from_inverter(
        cls, inv,
        inputs_offsets   = None,
        internal_offsets = None,
        outputs_offsets  = None,
    ):
        def offsets_gen(nset):
            offsets = dict()
            idx = 0
            for Mnode in nset:
                offsets[Mnode] = idx
                idx += inv.sizes[Mnode]
            offsets['size'] = idx
            return offsets

        if inputs_offsets is None:
            inputs_offsets = offsets_gen(inv.inputs)
        else:
            inputs_offsets['size']  = max(v + inv.sizes[n] for n, v in inputs_offsets.items())

        if outputs_offsets is None:
            outputs_offsets = offsets_gen(inv.outputs)
        else:
            outputs_offsets['size']  = max(v + inv.sizes[n] for n, v in outputs_offsets.items())

        internalKLUfidx = []
        if internal_offsets is None:
            #need to duplicate the offsets gen code to generate a KLU matrix
            #at the same time
            internal_nodes = set(inv.seq.keys()) | set(inv.req.keys())
            internal_offsets = dict()
            internal_idx = 0
            internalKLU = KLUMatrix('partial solve')
            edgesKLU = dict()
            for Mnode in internal_nodes:
                internal_offsets[Mnode] = internal_idx
                size = inv.sizes[Mnode]
                internal_idx += inv.sizes[Mnode]
                fidx = len(internalKLUfidx)
                internalKLUfidx.append(Mnode)
                edge = inv.edges.get((Mnode, Mnode), None)
                edge = np.asarray(edge)

                if len(edge.shape) == 0:
                    is_diagonal = True
                elif len(edge.shape) == 1:
                    is_diagonal = True
                else:
                    is_diagonal = False

                mat = internalKLU.add_diagonal_elements(
                    size, fidx,
                    f"I={Mnode}",
                    is_diagonal = is_diagonal
                )
                edgesKLU[Mnode, Mnode] = mat
            internal_offsets['size'] = internal_idx
        else:
            internalKLU = KLUMatrix('partial solve')
            edgesKLU = dict()
            for Mnode in sorted(internal_offsets.keys(), key = lambda k : internal_offsets[k]):
                size = inv.sizes[Mnode]
                fidx = len(internalKLUfidx)
                internalKLUfidx.append(Mnode)
                edge = inv.edges.get((Mnode, Mnode), None)

                #TODO
                assert(edge is not None)

                if edge.ms_type == MatrixStoreTypes.MATRIX:
                    is_diagonal = False
                else:
                    is_diagonal = True

                mat = internalKLU.add_diagonal_elements(
                    size, fidx,
                    f"I={Mnode}",
                    is_diagonal = is_diagonal
                )
                edgesKLU[Mnode, Mnode] = mat
            internal_offsets['size']  = max(v + inv.sizes[n] for n, v in internal_offsets.items())
        internal_node2fidx = {n: i for i, n in enumerate(internalKLUfidx)}

        def iter_gamma():
            yield outputs_offsets['size'], inputs_offsets['size']
            for inode in inv.inputs:
                winode = ('INPUT', inode)
                n_fr   = winode
                idx_fr = inputs_offsets[inode]
                size_fr = inv.sizes[inode]
                for n_to in inv.seq_gamma.get(n_fr, ()):
                    #unwrap the output node
                    onode = n_fr[1]
                    idx_to = outputs_offsets[onode]
                    edge = inv.edges[n_fr, n_to]
                    yield idx_fr, idx_to, size_fr, edge
        gammaCSC = to_csc(iter_gamma())

        def build_sum():
            for n_fr, sset in inv.seq.items():
                for n_to in sset:
                    edgeO = inv.edges[n_fr, n_to]
                    edgeC = inv.edges_changing.get((n_fr, n_to), None)
                    if edgeC is None:
                        edge = edgeO
                    else:
                        edge = edgeO.matadd(edgeC)

                    if edge.ms_type == MatrixStoreTypes.MATRIX:
                        is_diagonal = False
                    else:
                        is_diagonal = True

                    if n_to == n_fr:
                        continue
                    #what about the actual diagonal
                    if is_diagonal:
                        mat = internalKLU.get_sub_diagonal_view(
                            internal_node2fidx[n_fr],
                            internal_node2fidx[n_to],
                            str((n_to, n_fr)),
                        )
                    else:
                        mat = internalKLU.get_sub_matrix_view(
                            internal_node2fidx[n_fr],
                            internal_node2fidx[n_to],
                            str((n_to, n_fr)),
                        )
                    edgesKLU[n_fr, n_to] = mat
        build_sum()

        def iter_alpha():
            yield internal_offsets['size'], inputs_offsets['size']
            for n_to, rset in inv.req_alpha.items():
                idx_to = internal_offsets[n_to]
                size_to = inv.sizes[n_to]
                for n_fr in rset:
                    inode = n_fr[1]
                    idx_fr = inputs_offsets[inode]
                    edge = inv.edges[n_fr, n_to]
                    yield idx_fr, idx_to, size_to, edge
        alphaCSC = to_csc(iter_alpha())

        def iter_beta():
            yield outputs_offsets['size'], internal_offsets['size']
            for n_fr, sset in inv.seq_beta.items():
                idx_fr = internal_offsets[n_fr]
                size_fr = inv.sizes[n_fr]
                for n_to in sset:
                    onode = n_to[1]
                    idx_to = outputs_offsets[onode]
                    edge = inv.edges[n_fr, n_to]
                    yield idx_fr, idx_to, size_fr, edge
        betaCSC = to_csc(iter_beta())

        internalKLU.construct()

        edges_filled = set()
        def fill_sum():
            #make a function just to have a private namespace
            for (n_fr, n_to), M_klu in edgesKLU.items():
                edgeO = inv.edges[n_fr, n_to]
                edgeC = inv.edges_changing.get((n_fr, n_to), None)
                if edgeC is None:
                    edge = edgeO
                else:
                    edge = edgeO.matadd(edgeC)

                edges_filled.add((n_fr, n_to))
                mat = edgesKLU[n_fr, n_to]

                #TODO, fix broadcasting logic here
                if edge.ms_type == MatrixStoreTypes.SCALAR:
                    edge = np.broadcast_to(edge.matrix, mat.shape)
                    edge = np.asarray(edge, dtype = complex)
                    mat[:] = edge
                elif edge.ms_type == MatrixStoreTypes.DIAGONAL:
                    mat[:] = edge.matrix
                else:
                    mat[:] = edge.matrix
        fill_sum()
        #print("BOOM?", set(edgesKLU.keys()) - edges_filled)

        if alphaCSC.shape[1] != 1:
            raise RuntimeError("KLU doesn't support multiple RHS yet")
        coo = alphaCSC.tocoo()
        def setup_rhs():
            internalKLU.clear_rhs()
            for idxR, val in zip(coo.row, coo.data):
                internalKLU.set_rhs(idxR, val)
        setup_rhs()
        internalKLU.factor()

        return cls(
            setup_rhs      = setup_rhs,
            sumKLU         = internalKLU,
            edgesKLU       = edgesKLU,
            betaCSC        = betaCSC,
            gammaCSC       = gammaCSC,
            offsets_out    = outputs_offsets,
            edges_original = inv.edges,
            sizes          = inv.sizes,
            edges          = set(inv.edges_changing.keys()),
        )

    def __init__(
            self,
            setup_rhs,
            sumKLU,
            edgesKLU,
            betaCSC,
            gammaCSC,
            edges,
            edges_original,
            offsets_out,
            sizes,
    ):
        self.setup_rhs = setup_rhs
        self.sumKLU         = sumKLU
        self.edgesKLU       = edgesKLU
        self.betaCSC        = betaCSC
        self.gammaCSC       = gammaCSC
        self.edges          = edges
        self.edges_original = edges_original
        self.offsets_out    = offsets_out
        self.sizes          = sizes
        return

    def update_solve(self, edges):
        def update_sum():
            for ekey in self.edges:
                edgeC = edges[ekey]
                edgeO = self.edges_original[ekey]
                edge = edgeC.matadd(edgeO)
                n_fr, n_to = ekey

                mat = self.edgesKLU[n_fr, n_to]

                #assumes that the KLU matrix is the correct shape for
                #this edge shape
                if edge.ms_type == MatrixStoreTypes.SCALAR:
                    edge = np.broadcast_to(np.asarray(edge.matrix), mat.shape)
                    edge = np.asarray(edge, dtype = complex)
                    mat[:] = edge
                elif edge.ms_type == MatrixStoreTypes.DIAGONAL:
                    mat[:] = edge.matrix
                else:
                    mat[:] = edge.matrix

        self.setup_rhs()
        update_sum()
        self.sumKLU.refactor()

        out = self.sumKLU.solve().reshape(-1, 1)
        out = scipy.sparse.csc_matrix(out)
        out = np.asarray((self.betaCSC @ out + self.gammaCSC).todense()).reshape(-1)
        self.out = out
        return None


def to_csc(edge_iter):
    rows = []
    cols = []
    data = []
    #first iteration is the final shape
    shape_rows, shape_cols = next(edge_iter)
    for idx_in, idx_out, size_in, edge in edge_iter:
        if edge.ms_type == MatrixStoreTypes.SCALAR:
            data.append(edge.matrix * np.ones((size_in,), dtype = np.complex))
            rows.append(np.arange(size_in) + idx_out)
            cols.append(np.arange(size_in) + idx_in)
        elif edge.ms_type == MatrixStoreTypes.DIAGONAL:
            data.append(edge.matrix)
            rows.append(np.arange(len(edge.matrix)) + idx_out)
            cols.append(np.arange(len(edge.matrix)) + idx_in)
        else:
            coo = scipy.sparse.coo_matrix(edge.matrix)
            data.append(coo.data)
            rows.append(coo.row + idx_out)
            cols.append(coo.col + idx_in)

    if not cols:
        csc = scipy.sparse.csc_matrix(([], ([], [])), shape = (shape_rows, shape_cols))
        return csc
    else:
        cols = np.concatenate(cols)
        rows = np.concatenate(rows)
        data = np.concatenate(data)
        csc = scipy.sparse.csc_matrix((data, (rows, cols)), shape = (shape_rows, shape_cols))
        return csc
