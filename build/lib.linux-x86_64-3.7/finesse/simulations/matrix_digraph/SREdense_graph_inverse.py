# -*- coding: utf-8 -*-
"""
"""
from __future__ import division, print_function, unicode_literals
import collections
import numbers
import copy

from . import SRABE_GE

from .utilities import (
    pre_purge_inplace,
    purge_inplace,
    check_seq_req_balance,
    dictset_copy,
    #SRE_copy,
)

from .SREdense_numeric_inverse import SREdense_numeric_inverse

from .lin_op import (
    DeferredMatrixStore,
    MatrixStoreTypes,
)


def nullprint(*p):
    return


class SREDenseInverter(object):
    N_limit_rel = 100
    safe_GE = True

    def __init__(
        self,
        seq, req, edges, sizes, inputs, outputs,
        vprint  = nullprint,
    ):

        pre_purge_inplace(seq, req, edges)

        req_alpha = collections.defaultdict(set)
        seq_beta  = collections.defaultdict(set)
        seq_gamma = collections.defaultdict(set)

        if inputs is None:
            inputs = set(seq.keys())

        if outputs is None:
            outputs = set(req.keys())

        #first dress the input and output nodes
        for inode in inputs:
            winode = ('INPUT', inode)
            req_alpha[inode].add(winode)
            edges[winode, inode] = DeferredMatrixStore(
                ms_type = MatrixStoreTypes.SCALAR,
                matrix = 1,
                needs_build = False
            )

        for onode in outputs:
            wonode = ('OUTPUT', onode)
            seq_beta[onode].add(wonode)
            edges[onode, wonode] = DeferredMatrixStore(
                ms_type = MatrixStoreTypes.SCALAR,
                matrix = -1,
                needs_build = False
            )

        purge_inplace(
            seq       = seq,
            req       = req,
            seq_beta  = seq_beta,
            req_alpha = req_alpha,
            edges     = edges,
        )

        if __debug__:
            check_seq_req_balance(seq, req, edges)

        self.sizes          = sizes
        self.inputs         = inputs
        self.outputs        = outputs
        self.seq            = seq
        self.req            = req
        self.req_alpha      = req_alpha
        self.seq_beta       = seq_beta
        self.seq_gamma      = seq_gamma
        self.edges          = edges
        self.SRABGE         = (seq, req, req_alpha, seq_beta, seq_gamma, edges)
        self.edges_changing = dict()
        self.nodes_exclude  = set()

    def __copy__(self):
        other = self.__class__.__new__(self.__class__)
        #doesn't modify these
        other.sizes          = self.sizes
        other.inputs         = self.inputs
        other.outputs        = self.outputs
        #will modify these (so copy them)
        other.seq            = dictset_copy(self.seq)
        other.req            = dictset_copy(self.req)
        other.req_alpha      = dictset_copy(self.req_alpha)
        other.seq_beta       = dictset_copy(self.seq_beta)
        other.seq_gamma      = dictset_copy(self.seq_gamma)
        other.edges          = copy.copy(self.edges)
        other.edges_changing = copy.copy(self.edges_changing)
        other.nodes_exclude  = copy.copy(self.nodes_exclude)
        other.SRABGE         = (
            other.seq,
            other.req,
            other.req_alpha,
            other.seq_beta,
            other.seq_gamma,
            other.edges
        )
        return other

    def set_changing_edges(self, elist):
        for ekey in elist:
            Mnode_fr, Mnode_to = ekey
            self.nodes_exclude.add(Mnode_fr)
            self.nodes_exclude.add(Mnode_to)
            if Mnode_to in self.seq[Mnode_fr]:
                edge = self.edges[ekey]
                self.edges[ekey] = DeferredMatrixStore(
                    ms_type = MatrixStoreTypes.SCALAR,
                    matrix = 0,
                    needs_build = False,
                )
                self.edges_changing[ekey] = edge
        return

    def reinject_changed_edges(self, edges):
        """
        The changed edges were all set to zero to be populated at this stage
        """
        #from icecream import ic
        for ekey in self.edges_changing.keys():
            edge_new = edges[ekey]
            edge_old = self.edges[ekey]
            self.edges[ekey] = self.matadd(edge_new, edge_old)
            #ic(ekey, edge_old, edge_new)

        self.edges_changing.clear()
        self.nodes_exclude.clear()
        return

    def simplify_trivial(self):
        return SRABE_simplify_trivial(
            SRABGE      = self.SRABGE,
            exclude     = self.nodes_exclude,
            safe_GE     = self.safe_GE,
            N_limit_rel = self.N_limit_rel,
            #vprint = print,
        )

    def plot_graph(self, fname):
        from .SRE_graphs import SRABGE_graph
        return SRABGE_graph(fname, self.SRABGE)

    def numeric_finish(self):
        seq_i, req_i, edge_map_i = SREdense_numeric_inverse(
            seq = self.seq,
            req = self.req,
            edges = self.edges,
            outputs = self.seq_beta.keys(),
            inputs = self.req_alpha.keys(),
            node_sizes = self.sizes,
        )

        #copy over the orignal in-to-out form matrix
        seq_beta_clean = collections.defaultdict(set)
        edges_clean = dict()
        for inode in self.inputs:
            winode = ('INPUT', inode)
            n_fr = winode
            for n_to in self.seq_gamma.get(n_fr, ()):
                edge = self.edges[n_fr, n_to]

                #print('adding edge', n_fr, n_to, edge)
                edges_clean[n_fr, n_to] = edge
                seq_beta_clean[n_fr].add(n_to)

        req_alpha2 = collections.defaultdict(set)
        edges_alpha = dict()
        #now, perform a multiply of the alphas with the inversion
        for node, sset in seq_i.items():
            for n_to in sset:
                edge2 = -edge_map_i[node, n_to]
                for n_fr in self.req_alpha[node]:
                    edge1 = self.edges[n_fr, node]
                    edge_prev = edges_alpha.get((n_fr, n_to), None)
                    if edge_prev is None:
                        edges_alpha[n_fr, n_to] = edge2.matmul(edge1)
                    else:
                        edges_alpha[n_fr, n_to] = edge2.matmuladd(edge1, edge_prev)
                    req_alpha2[n_to].add(n_fr)
        #now, perform a multiply with the betas
        for node, rset in req_alpha2.items():
            for n_to in self.seq_beta[node]:
                edge2 = self.edges[node, n_to]
                for n_fr in rset:
                    edge1 = edges_alpha[n_fr, node]
                    edge_prev = edges_clean.get((n_fr, n_to), None)
                    if edge_prev is None:
                        edges_clean[n_fr, n_to] = edge2.matmul(edge1)
                    else:
                        edges_clean[n_fr, n_to] = edge2.matmuladd(edge1, edge_prev)
                    seq_beta_clean[n_fr].add(n_to)
        del req_alpha2
        del edges_alpha

        #now unwrap the nodes
        unwrapped_edge_map = dict()
        unwrapped_seq_map  = collections.defaultdict(set)
        unwrapped_req_map  = collections.defaultdict(set)

        for inode in self.inputs:
            winode = ('INPUT', inode)
            #Could get exceptions here if we don't purge and the input maps have spurious
            #outputs (nodes with no seq) other than the wrapped ones generated here
            for wonode in seq_beta_clean[winode]:
                k, onode = wonode
                assert(k == 'OUTPUT')
                #the sorting is because the dense matrices are already in the
                #canonical ordering for the key groups. That was preserved from the
                #original construction of the dense matrices out of the KeyGroup
                unwrapped_edge_map[inode, onode] = edges_clean[winode, wonode]
                unwrapped_seq_map[inode].add(onode)
                unwrapped_req_map[onode].add(inode)

        #return SRE
        return (
            unwrapped_seq_map,
            unwrapped_req_map,
            unwrapped_edge_map,
        )


def SRABE_simplify_trivial(
    SRABGE,
    N_limit_rel,
    safe_GE = True,
    exclude = set(),
    vprint = nullprint,
):
    seq, req, req_alpha, seq_beta, seq_gamma, edges, = SRABGE

    dsets = traverse_simple_diagonals_reqs(SRABGE)
    reduced_any = False

    if safe_GE:
        GE_func = SRABE_GE.reduceGE_testing
    else:
        GE_func = SRABE_GE.reduceGE

    for node in dsets["low_edge_nodes"]:
        if node in exclude:
            continue
        if len(req.get(node, ())) > 2:
            continue

        vprint("REDUCING: ", node)

        if GE_func(
            SRABGE,
            node = node,
            N_limit_rel = N_limit_rel,
        ):
            vprint("SUCCESS: ", node)
        else:
            vprint("FAIL: ", node)
        reduced_any = True

    for node in dsets["diags_1"]:
        if node in exclude:
            continue
        if node not in req.get(node, ()):
            continue
        diag = edges[node, node]
        if isinstance(diag, numbers.Number):
            if diag == 1:
                pass
            elif diag == -1:
                pass
        else:
            continue

        vprint("REDUCING: ", node)

        if GE_func(
            SRABGE,
            node = node,
            N_limit_rel = N_limit_rel,
        ):
            vprint("SUCCESS: ", node)
        else:
            vprint("FAIL: ", node)
        reduced_any = True

    return reduced_any


def traverse_simple_diagonals_reqs(SRABGE):
    (seq, req, req_alpha, seq_beta, seq_gamma, edges) = SRABGE

    low_edge_nodes = set()
    diags_1 = set()
    for node, rset in req.items():
        if node not in rset:
            #if there is no diagonal, then we cannot remove the node without
            #a pivot or Q-op
            continue
        if len(rset) <= 2:
            low_edge_nodes.add(node)
        diag = edges[node, node]
        if isinstance(diag, numbers.Number):
            if diag == 1:
                diags_1.add(node)
            elif diag == -1:
                diags_1.add(node)
    return dict(
        low_edge_nodes = low_edge_nodes,
        diags_1 = diags_1,
    )

