"""
Sub-module consisting of the :class:`.Simulation` for
performing specific executions of a :class:`.Model`.
"""

import logging
import numpy as np
import networkx as nx
import gc

from finesse.cmatrix import KLUMatrix
from .base import BaseMatrixSimulation

LOGGER = logging.getLogger(__name__)


class KLUSimulation(BaseMatrixSimulation):
    def solve(self):
        if not nx.is_frozen(self.model.network):
            raise Exception("Model has not been built")

        # As we put -1 on diagonals we actually solve the negative
        # of the system, so inverse that here...
        # -1 here because we compute the negative of the interferometer
        # matrix. Why? Because the -1 sign only gets applied once along the
        # diagonal rather than for every non-diagonal element. Which means
        # equations are written as they are in math.
        self._M.refactor()
        self.out = -1*self._M.solve()
        return

    def __enter__(self):
        """
        When entered the Simulation object will create the matrix to be used in
        the simulation.

        This is where the matrix gets allocated and constructed. It will expect
        that the model structure does not change after this until it has been
        exited.
        """
        # Set the matrix method we'll be using to run this simulation
        self._M = KLUMatrix(self.name)
        # Initialising the simulation expects there to be a self._M class that handles the
        # matrix build/memory/etc. This must be set before initialising.

        self._initialise()

        self._M.construct()
        self._fill()
        #self.print_matrix()
        self._M.factor()

        # TODO ddb: this should be chcking audio and that we have quantum calculations going on
        # TODO ddb: create sparse noise source matrix
        # TODO ddb: noise calculations probably more generic than just quantum
        if self.is_audio:
            self._Mq = np.zeros(self._M.num_equations)

        return self

    def __exit__(self, type_, value, traceback):
        self._unbuild()

        _ref = self._M

        self._M           = None
        self._Mq          = None
        self._submatrices = None

        refs = gc.get_referrers(_ref)
        Nref = len(refs)

        if Nref > 0:
            LOGGER.warn(f"Something other than the Simulation object (N={Nref}) is keeping"
                        f" a reference to the matrix object {repr(self._M)}."
                        " Could lead to excessive memory usage if not released.")
            for _ in refs:
                LOGGER.warn(f" - {repr(_)}")

        del _ref
