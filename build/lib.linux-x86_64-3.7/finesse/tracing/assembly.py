"""
Optimiser for geometric properties of optical components in a model.
"""

import abc
from collections import OrderedDict

import numpy as np
from scipy.optimize import minimize


class Assembly:
    """A class for optimising arbitrary geometric properties
    of a :class:`.Model` instance via beam tracing calls.

    Attributes
    ----------
    free_parameters : :class:`list`
        Sequence of :class:`.FreeParameter`
    target_parameters : :class:`list`
        Sequence of :class:`.TargetParameter`
    """
    def __init__(self, model):
        """Creates a new optical assembly using the speficied model.

        Parameters
        ----------
        model : :class:`.Model`
            The model to trace.
        """
        self.model = model
        self.free_parameters = []   # order is important!
        self.target_parameters = [] # order is important!

    def allow(self, comp, attr, bounds):
        """Set a free parameter for the optical assembly.

        Parameters
        ----------
        comp : :class:`.Connector`
            An instance of a connector such as a :class:`.Mirror`.

        attr : str
            An attribute of the class `comp` as a string.

        bounds : tuple or array like
            Two element tuple or other iterable defining the
            boundaries of the free parameter space to explore.
        """
        self.free_parameters.append(FreeParameter(comp, attr, bounds))

    def target(self, node, item, param, value):
        """Set a target parameter for the optical assembly.

        Parameters
        ----------
        node : :class:`.Node`
            The node owning the item (and its attribute) to target.

        item : any
            The item that possesses the attribute to target.

        param : any
            A suitable target parameter - typically an attribute or
            method of :class:`.BeamParam`.

        value : float
            The target value for the parameter.
        """
        self.target_parameters.append(TargetParameter(node, item, param, value))

    def _set_model_values(self, values):
        """Set specified parameter values in the model."""
        for parameter, value in zip(self.free_parameters, values):
            setattr(parameter.param[0], parameter.param[1], value)

    def _trace(self, update_values=None):
        """Trace system using specified values."""
        if update_values is not None:
            # Update model parameter values.
            self._set_model_values(update_values)

        self.model.beam_trace()

    def _cost(self, values):
        # Compute beam parameters.
        self._trace(update_values=values)

        # Total cost.
        return np.sqrt(sum([parameter.cost() ** 2 for parameter in self.target_parameters]))

    @property
    def _bounds(self):
        return [parameter.bounds for parameter in self.free_parameters]

    def solve(self, tol=None, options=None):
        """Run the minimisation routine on the optical assembly.

        This will attempt to minimise the :meth:`.TargetParameter.cost` function
        of each target parameter in the system.

        Parameters
        ----------
        tol : float, optional
            Tolerance for termination.
        options : dict, optional
            Solver-specific options.

        Returns
        -------
        result : array like
            An array of solutions for all the free parameters.
        """
        result = minimize(
            self._cost,
            [getattr(fp.param[0], fp.param[1]) for fp in self.free_parameters],
            bounds=self._bounds, tol=tol, options=options
        )

        if result.success:
            self._trace(update_values=result.x)
        else:
            raise Exception("unable to converge on solution: %s" % result.message)

        solutions = OrderedDict.fromkeys(
            [fp.param[0].name +'.'+fp.param[1] for fp in self.free_parameters]
        )
        for k, r in zip(solutions, result.x):
            solutions[k] = r

        return solutions


class FreeParameter:
    """A free parameter (or degree of freedom) of a :class:`.Assembly` instance."""
    def __init__(self, comp, attr, bounds):
        """Creates a new free parameter from the given component-attribute
        pair.

        Parameters
        ----------
        comp : :class:`.Connector`
            An instance of a connector such as a :class:`.Mirror`.

        attr : str
            An attribute of the class `comp` as a string.

        bounds : tuple or array like
            Two element tuple or other iterable defining the
            boundaries of the free parameter space to explore.
        """
        self.param = [comp, attr]
        self.bounds = bounds


class TargetParameter:
    """A target parameter of a :class:`.Assembly` instance."""
    def __init__(self, node, item, param, target):
        """Creates a new target parameter with the specified target value.

        Parameters
        ----------
        node : :class:`.Node`
            The node owning the item (and its attribute) to target.

        item : any
            The item that possesses the attribute to target.

        param : any
            A suitable target parameter - typically an attribute or
            method of :class:`.BeamParam`.

        target : float
            The target value for the parameter.
        """
        self.node = node
        self.item = item
        self.variable = param
        self.target_value = target

    @property
    def value(self):
        """Current value of the target parameter.

        :getter: Returns the current value.
        """
        param = getattr(getattr(self.node, self.item), self.variable)
        if callable(param):
            param = param()
        return param

    def cost(self):
        """The cost function for minimisation of the difference between
        the current value and the target value.

        Returns
        -------
        out : float
            Absolute difference between :attr:`TargetParameter.value` and
            the target value of the parameter.
        """
        return np.abs(self.value - self.target_value)
