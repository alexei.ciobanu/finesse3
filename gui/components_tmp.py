#TODO: Change default values to use kat.ini values
#TODO: Add the rest of the components

import numpy as np
from collections import deque

class Component:
    def __str__(self):
        return self.name

class Laser(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.P = float(args.popleft())
        self.f = float(args.popleft())
        if (len(args) > 1):
            self.phase = float(args.popleft())
        else:
            self.phase = 0
        self.nodes.append(args.popleft())

class Squeezer(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.f = float(args.popleft())
        self.r = float(args.popleft())
        self.angle = float(args.popleft())
        self.nodes.append(args.popleft())

class Mirror(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.R = float(args.popleft())
        self.T = float(args.popleft())
        self.phi = float(args.popleft())
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())

class Space(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.L = float(args.popleft())
        if (len(args) > 2):
            self.n = float(args.popleft())
        else:
            self.n = 1
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())

class Beamsplitter(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.R = float(args.popleft())
        self.T = float(args.popleft())
        self.phi = float(args.popleft())
        self.alpha = float(args.popleft())
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())

class Isolator(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.S = float(args.popleft())
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())

class Modulator(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.f = float(args.popleft())
        self.midx = float(args.popleft())
        self.order = int(args.popleft())
        self.mode = args.popleft()
        if (len(args) > 2):
            self.phase = float(args.popleft())
        else:
            self.phase = 0
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())

class Lens(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.f = float(args.popleft())
        self.nodes.append(args.popleft())
        self.nodes.append(args.popleft())

class Photodiode(Component):
    def __init__(self, line):
        self.nodes = []
        self.f = []
        self.phase = []
        args = deque(line.strip().split())
        self.mode = args.popleft()
        self.name = args.popleft()
        while (len(args) > 1):
            self.f.append(float(args.popleft()))
            if (len(args) > 1):
                self.phase.append(float(args.popleft()))
            else:
                self.phase.append(0)
        self.nodes.append(args.popleft())

class AmplitudeDetector(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        if (len(args) > 5):
            self.n = float(args.popleft())
            self.m = float(args.popleft())
        else:
            self.n = 0
            self.m = 0
        self.f = float(args.popleft())
        self.nodes.append(args.popleft())

class QuantumQuadratureDetector(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.f = float(args.popleft())
        self.phase = float(args.popleft())
        self.nodes.append(args.popleft())

class SqueezingDetector(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.f = float(args.popleft())
        if (len(args) > 5):
            self.n = float(args.popleft())
            self.m = float(args.popleft())
        else:
            self.n = 0
            self.m = 0
        self.nodes.append(args.popleft())

class ShotNoiseDetector(Component):
    def __init__(self, line):
        self.nodes = []
        args = deque(line.strip().split())
        args.popleft()
        self.name = args.popleft()
        self.nodes.append(args.popleft())

class PhotodiodeQuantumNoise(Component):
    def __init__(self, line):
        self.nodes = []
        self.f = []
        self.phase = []
        args = deque(line.strip().split())
        self.mode = args.popleft()
        self.name = args.popleft()
        self.n = args.popleft()
        while (len(args) > 1):
            self.f.append(float(args.popleft()))
            if (len(args) > 1):
                self.phase.append(float(args.popleft()))
            else:
                self.phase.append(0)
        self.nodes.append(args.popleft())

class QuantumShotNoiseDetector(Component):
    def __init__(self, line):
        self.nodes = []
        self.f = []
        self.phase = []
        args = deque(line.strip().split())
        self.mode = args.popleft()
        self.name = args.popleft()
        self.n = args.popleft()
        while (len(args) > 1):
            self.f.append(float(args.popleft()))
            if (len(args) > 1):
                self.phase.append(float(args.popleft()))
            else:
                self.phase.append(0)
        self.nodes.append(args.popleft())
