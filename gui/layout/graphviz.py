"""Graphviz layout manager"""

from networkx.drawing import nx_agraph

from .layout import BaseLayout

class Graphviz(BaseLayout):
    """Graphviz layout manager

    Uses pygraphviz to call a local installation of graphviz to generate a
    series of coordinates for nodes and edges, then translates these into
    primitives to display on the canvas.
    """

    def layout(self, *args, **kwargs):
        node_params, edge_params = self._graphviz_parameters(*args, **kwargs)

        for node, params in node_params.items():
            # unpack parameters
            x, y, width, height = params

            # convert scale from graphviz (inches?) to pixels
            scale = 72
            width *= scale
            height *= scale

            self.draw_node(x, y, width, height, label=node)

        for edge, params in edge_params.items():
            self.draw_edge(params)

    def _graphviz_parameters(self, *args, **kwargs):
        # create pygraphviz agraph (abstract graph) object from NetworkX graph
        pgv_graph = nx_agraph.to_agraph(self.graph)

        # layout graph
        pgv_graph.layout(*args, **kwargs)

        # return calculated positions
        return self._pgv_node_params(pgv_graph), self._pgv_edge_params(pgv_graph)

    def _pgv_node_params(self, pgv_graph):
        """Get laid out node positions from graphviz via pygraphviz

        Node positions have form "x,y", e.g. "-25,135" -> (-25, 135). This
        method returns thes coordinates as a dict containing each node, and a
        tuple containing its coordinates as floats.
        """

        def _pgv_node_pos_iter(node):
            x_str, y_str = node.attr["pos"].split(",")
            w_str = node.attr["width"]
            h_str = node.attr["height"]

            return float(x_str), float(y_str), float(w_str), float(h_str)

        return {node: _pgv_node_pos_iter(node) for node in pgv_graph.nodes()}

    def _pgv_edge_params(self, pgv_graph):
        """Get laid out edge positions from graphviz via pygraphviz

        Edge positions from pygraphviz are Bézier curves with the form
        "e,[x1,y1] [x2,y2] ... [xn,yn]". This method returns a list of tuples
        represnting the coordinates of each Bézier curve parameter.
        """

        def _pgv_edge_pos_iter(edge):
            pos_strs = edge.attr["pos"].lstrip("e,").split()

            for coordinates in pos_strs:
                x, y = coordinates.split(",")
                yield float(x), float(y)

        return {edge: list(_pgv_edge_pos_iter(edge)) for edge in pgv_graph.edges()}

    def draw_node(self, *args, **kwargs):
        # nodes are ellipses
        self.canvas.draw_ellipse(*args, **kwargs)

    def draw_edge(self, *args, **kwargs):
        # edges are Bézier curves
        self.canvas.draw_bezier(*args, **kwargs)
