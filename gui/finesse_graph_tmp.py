"""finesse_graph.py modified to use as an input to the GUI code

Sean Leavey
"""

from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt
import networkx as nx
import copy

from components_tmp import *

# TODO: add optional argument for (estimated) number of
# optical components in given configuration such that
# we can use np.array instead of lists for e.g. local
# parameter `config` -- issue here is we don't know how
# many connections each component will have, so not sure
# if this can be done in any precise way
def build_graph(fcode):
    """Reads a string of Finesse code and
    builds the configuration into a directed
    graph.

    Parameters
    ----------
    fcode : str

      Finesse code block.

    Returns
    -------
    A `nx.DiGraph` representing the optical
    configuration given in `fcode`.
    """
    def _find_node_refs(comp, comp_list):
        """Finds all the references to the components in comp_list
        that are connected to comp, excluding comp itself.

        Parameters
        ----------
        comp : object

            Component to search for connections to.

        comp_list : list[object]

            List of components to search through.

        Returns
        -------
        A list of all components connected to `comp`.
        """
        connected_comps = []
        for node in comp.nodes:
            for other in comp_list:
                if other == comp:
                    continue
                if node in other.nodes:
                    connected_comps.append(other)
        return connected_comps
    assert isinstance(fcode, str)
    # maps components to numbers of nodes
    comp_num_dict = {'l' : 1, 'sq' : 1,
                    'm' : 2, 's' : 2,
                    'isol' : 2, 'mod' : 2,
                    'lens' : 2, 'bs' : 4,
                    'pd' : 1}
    # maps components to classes
    comp_class_dict = {'l' : Laser, 'sq' : Squeezer,
                     'm' : Mirror, 's' : Space,
                     'isol' : Isolator, 'mod' : Modulator,
                     'lens' : Lens, 'bs' : Beamsplitter,
                     'pd' : Photodiode}
    fcode_lines = fcode.splitlines()
    config = []
    comp_list = []
    for line in fcode_lines:
        # trim leading and trailing whitespace
        line = line.strip()
        line_split = line.split(' ')
        # empty line or comment
        if line_split[0] == '' or line_split[0] == '#':
            continue
        comp = comp_class_dict[line_split[0]](line)
        # TODO: determine what to do in the case of an
        # asterisk node
        if hasattr(comp, "nodes"):
            comp_list.append(comp)

    # TODO: figure out what to do in case of detectors
    for comp in comp_list:
        for connected in _find_node_refs(comp, comp_list):
            config.append((comp, connected))
    graph = nx.DiGraph(config)

    return graph

def get_test_graph():
    code = """
l laser 1 0 n0
s s1 1 n0 n1

bs bs1 0.5 0.5 0 45 n1 n2 n3 n4

# cavity 1
m itm1 0.986 0.014 0 n2 n5
s scv1 3994.5 n5 n6
m etm1 0.999995 0.000005 0 n6 n7

# cavity 2
m itm2 0.986 0.014 0 n3 n8
s scv2 3994.5 n8 n9
m etm2 0.999995 0.000005 0 n9 n10

# detector (fake numbers)
s sout 10 n4 n11
m srm  0.99 0.01 0 n11 n12
pd power n9
"""
    return build_graph(code)

## outputs:
##
## laser : {'s1'}
## s1 : {'bs1', 'laser'}
## bs1 : {'itm1', 's1', 'itm2'}
## itm1 : {'bs1', 'scv1'}
## scv1 : {'etn1', 'itm1'}
## etm1 : {'scv1'}
## itm2 : {'bs1', 'scv2'}
## scv2 : {'etm2', 'itm2'}
## etm2 : {'scv2'}
##
