# pylint: disable=no-member

"""Beam tracing test results."""

import sys
from collections import OrderedDict
from unittest import TestCase

from finesse import Model
from finesse.components import Beamsplitter, Cavity, Laser, Lens, Mirror
from finesse.exceptions import NodeException
from finesse.gaussian import BeamParam
from finesse.parse import get_parser
from finesse.utilities.maths import apply_ABCD

class ModelTestCase(TestCase):
    """Resettable test case for model linked tests."""
    def setUp(self):
        self.reset()

    def reset(self):
        """Reset model."""
        self.model = Model()

class SimpleTracesTestCase(ModelTestCase):
    """Simple beam tracing tests - i.e. single cavities, no beam splitters."""
    def test_broken_connection_trace(self):
        """Test beam tracing for a configuration with a broken connection."""
        self.model.add([Laser("L0"), Mirror("M")])
        self.model.L0.p1.o.q = BeamParam(q=(-1 + 1j))
        # don't connect the components - try a beam trace
        self.assertRaises(
            RuntimeError,
            callable=self.model.beam_trace
        )

    def test_laser_to_mirror_trace(self):
        """Test beam tracing for a laser -> mirror configuration with
        a beam parameter set at the laser."""
        m_roc = 2.5
        L0_q = -0.5 + 1j
        self.model.chain(Laser("L0"), 1, Mirror("M", Rc=m_roc))

        # first check that we get an exception when no
        # beam parameters are set anywhere
        self.assertRaises(
            NodeException,
            callable=self.model.beam_trace
        )

        self.model.L0.p1.o.q = L0_q
        result = self.model.beam_trace().data_qx

        # TODO: write generic function to compute the target trace
        # results for any given simple config
        target = OrderedDict()
        target[self.model.L0.p1.o] = BeamParam(q=L0_q)
        target[self.model.L0.p1.i] = -target[self.model.L0.p1.o].conjugate()
        target[self.model.M.p1.i] = apply_ABCD(
            self.model.L0.p1.o.space.ABCD(self.model.L0.p1.o, self.model.M.p1.i),
            target[self.model.L0.p1.o], 1, 1
        )
        target[self.model.M.p1.o] = -target[self.model.M.p1.i].conjugate()
        target[self.model.M.p2.o] = apply_ABCD(
            self.model.M.ABCD(self.model.M.p1.i, self.model.M.p2.o),
            target[self.model.M.p1.i], 1, 1
        )
        target[self.model.M.p2.i] = -target[self.model.M.p2.o].conjugate()

        for node, q in target.items():
            self.assertEqual(q, result[node])

    def test_fabry_perot_no_cav_trace(self):
        """Test beam tracing for a laser -> mirror -> space -> mirror configuration with
        a beam parameter set at the laser and no cavity object added."""
        m_roc = 2.5
        L0_q = -0.9 + 1.3j
        self.model.chain(Laser("L0"), 0.5, Mirror("ITM", Rc=-m_roc), 1, Mirror("ETM", Rc=m_roc))
        self.model.L0.p1.o.q = L0_q
        result = self.model.beam_trace().data_qx

        # TODO: write generic function to compute the target trace
        # results for any given simple config
        target = OrderedDict()
        # L0 p1
        target[self.model.L0.p1.o] = BeamParam(q=L0_q)
        target[self.model.L0.p1.i] = -target[self.model.L0.p1.o].conjugate()
        # ITM p1
        target[self.model.ITM.p1.i] = apply_ABCD(
            self.model.L0.p1.o.space.ABCD(self.model.L0.p1.o, self.model.ITM.p1.i),
            target[self.model.L0.p1.o], 1, 1
        )
        target[self.model.ITM.p1.o] = -target[self.model.ITM.p1.i].conjugate()
        # ITM p2
        target[self.model.ITM.p2.o] = apply_ABCD(
            self.model.ITM.ABCD(self.model.ITM.p1.i, self.model.ITM.p2.o),
            target[self.model.ITM.p1.i], 1, 1
        )
        target[self.model.ITM.p2.i] = -target[self.model.ITM.p2.o].conjugate()
        # ETM p1
        target[self.model.ETM.p1.i] = apply_ABCD(
            self.model.ITM.p2.o.space.ABCD(self.model.ITM.p2.o, self.model.ETM.p1.i),
            target[self.model.ITM.p2.o], 1, 1
        )
        target[self.model.ETM.p1.o] = -target[self.model.ETM.p1.i].conjugate()
        # ETM p2
        target[self.model.ETM.p2.o] = apply_ABCD(
            self.model.ETM.ABCD(self.model.ETM.p1.i, self.model.ETM.p2.o),
            target[self.model.ETM.p1.i], 1, 1
        )
        target[self.model.ETM.p2.i] = -target[self.model.ETM.p2.o].conjugate()

        for node, q in target.items():
            self.assertEqual(q, result[node])

    def test_fabry_perot_cav_trace(self):
        """Test beam tracing for a laser -> mirror -> space -> mirror configuration with
        a cavity object added."""
        m_roc = 2.5
        self.model.chain(Laser("L0"), 0.5, Mirror("ITM", Rc=-m_roc), 1, Mirror("ETM", Rc=m_roc))
        self.model.add(Cavity("FPC", self.model.ITM.p2.o, self.model.ITM.p2.i))
        result = self.model.beam_trace().data_qx

        # TODO: write generic function to compute the target trace
        # results for any given simple config
        target = OrderedDict()
        # ITM p2
        target[self.model.ITM.p2.o] = self.model.FPC.eigenmode
        target[self.model.ITM.p2.i] = -target[self.model.ITM.p2.o].conjugate()
        # ETM p1
        target[self.model.ETM.p1.i] = apply_ABCD(
            self.model.ITM.p2.o.space.ABCD(self.model.ITM.p2.o, self.model.ETM.p1.i),
            target[self.model.ITM.p2.o], 1, 1
        )
        target[self.model.ETM.p1.o] = -target[self.model.ETM.p1.i].conjugate()
        # ETM p2
        target[self.model.ETM.p2.o] = apply_ABCD(
            self.model.ETM.ABCD(self.model.ETM.p1.i, self.model.ETM.p2.o),
            target[self.model.ETM.p1.i], 1, 1
        )
        target[self.model.ETM.p2.i] = -target[self.model.ETM.p2.o].conjugate()
        # ITM p1
        target[self.model.ITM.p1.o] = apply_ABCD(
            self.model.ITM.ABCD(self.model.ITM.p2.i, self.model.ITM.p1.o),
            target[self.model.ITM.p2.i], 1, 1
        )
        target[self.model.ITM.p1.i] = -target[self.model.ITM.p1.o].conjugate()
        # L0 p1
        target[self.model.L0.p1.i] = apply_ABCD(
            self.model.ITM.p1.o.space.ABCD(self.model.ITM.p1.o, self.model.L0.p1.i),
            target[self.model.ITM.p1.o], 1, 1
        )
        target[self.model.L0.p1.o] = -target[self.model.L0.p1.i].conjugate()

        for node, q in target.items():
            self.assertEqual(q, result[node])

    def test_fabry_perot_cav_and_manual_trace(self):
        """Test beam tracing for a laser -> mirror -> space -> mirror configuration with
        a cavity object added and a beam parameter set at the laser."""
        m_roc = 2.5
        L0_q = -1.1 + 1.6j
        self.model.chain(Laser("L0"), 0.5, Mirror("ITM", Rc=-m_roc), 1, Mirror("ETM", Rc=m_roc))
        self.model.L0.p1.o.q = L0_q
        self.model.add(Cavity("FPC", self.model.ITM.p2.o, self.model.ITM.p2.i))
        result = self.model.beam_trace().data_qx

        # TODO: write generic function to compute the target trace
        # results for any given simple config
        target = OrderedDict()
        # L0 p1
        target[self.model.L0.p1.o] = BeamParam(q=L0_q)
        target[self.model.L0.p1.i] = -target[self.model.L0.p1.o].conjugate()
        # ITM p2
        target[self.model.ITM.p2.o] = self.model.FPC.eigenmode
        target[self.model.ITM.p2.i] = -target[self.model.ITM.p2.o].conjugate()
        # ETM p1
        target[self.model.ETM.p1.i] = apply_ABCD(
            self.model.ITM.p2.o.space.ABCD(self.model.ITM.p2.o, self.model.ETM.p1.i),
            target[self.model.ITM.p2.o], 1, 1
        )
        target[self.model.ETM.p1.o] = -target[self.model.ETM.p1.i].conjugate()
        # ETM p2
        target[self.model.ETM.p2.o] = apply_ABCD(
            self.model.ETM.ABCD(self.model.ETM.p1.i, self.model.ETM.p2.o),
            target[self.model.ETM.p1.i], 1, 1
        )
        target[self.model.ETM.p2.i] = -target[self.model.ETM.p2.o].conjugate()
        # ITM p1
        target[self.model.ITM.p1.i] = apply_ABCD(
            self.model.L0.p1.o.space.ABCD(self.model.L0.p1.o, self.model.ITM.p1.i),
            target[self.model.L0.p1.o], 1, 1
        )
        target[self.model.ITM.p1.o] = -target[self.model.ITM.p1.i].conjugate()

        for node, q in target.items():
            self.assertEqual(q, result[node])


class AdvancedTracesTestCase(ModelTestCase):
    """Advanced beam tracing tests - i.e. interferometers with multiple cavities and
    configurations with several user set nodes and cavity objects."""
    def test_michelson_trace(self):
        """Test beam tracing for a Michelson interferometer (no Fabry-Perot cavities)
        with a single user-defined beam parameter at the starting laser node."""
        L0_q = -1.4 + 2.5j
        MODEL = r"""
        l L0 1 0 nL0
        s s1 0.5 nL0 nBS1

        bs BS 0.5 0.5 0 0 nBS1 nBS2 nBS3 nBS4

        s sy0 1.0 nBS2 nMY1
        m MY 0.5 0.5 0 nMY1 nMY2
        attr MY Rc 2.5

        s sx0 1.0 nBS3 nMX1
        m MX 0.5 0.5 0 nMX1 nMX2
        attr MX Rc 2.5
        """
        kp = get_parser(finesse2=True)
        kp.parse(MODEL)
        kat = kp.build()
        self.model = kat.model
        self.model.L0.p1.o.q = L0_q
        result = self.model.beam_trace().data_qx

        target = OrderedDict()
        # L0 p1
        target[self.model.L0.p1.o] = BeamParam(q=L0_q)
        target[self.model.L0.p1.i] = -target[self.model.L0.p1.o].conjugate()
        # BS p1
        target[self.model.BS.p1.i] = apply_ABCD(
            self.model.L0.p1.o.space.ABCD(self.model.L0.p1.o, self.model.BS.p1.i),
            target[self.model.L0.p1.o], 1, 1
        )
        target[self.model.BS.p1.o] = -target[self.model.BS.p1.i].conjugate()
        # BS p3
        target[self.model.BS.p3.o] = apply_ABCD(
            self.model.BS.ABCD(self.model.BS.p1.i, self.model.BS.p3.o),
            target[self.model.BS.p1.i], 1, 1
        )
        target[self.model.BS.p3.i] = -target[self.model.BS.p3.o].conjugate()
        # MX p1
        target[self.model.MX.p1.i] = apply_ABCD(
            self.model.BS.p3.o.space.ABCD(self.model.BS.p3.o, self.model.MX.p1.i),
            target[self.model.BS.p3.o], 1, 1
        )
        target[self.model.MX.p1.o] = -target[self.model.MX.p1.i].conjugate()
        # MX p2
        target[self.model.MX.p2.o] = apply_ABCD(
            self.model.MX.ABCD(self.model.MX.p1.i, self.model.MX.p2.o),
            target[self.model.MX.p1.i], 1, 1
        )
        target[self.model.MX.p2.i] = -target[self.model.MX.p2.o].conjugate()
        # BS p2
        target[self.model.BS.p2.o] = apply_ABCD(
            self.model.BS.ABCD(self.model.BS.p1.i, self.model.BS.p2.o),
            target[self.model.BS.p1.i], 1, 1
        )
        target[self.model.BS.p2.i] = -target[self.model.BS.p2.o].conjugate()
        # MY p1
        target[self.model.MY.p1.i] = apply_ABCD(
            self.model.BS.p2.o.space.ABCD(self.model.BS.p2.o, self.model.MY.p1.i),
            target[self.model.BS.p2.o], 1, 1
        )
        target[self.model.MY.p1.o] = -target[self.model.MY.p1.i].conjugate()
        # MY p2
        target[self.model.MY.p2.o] = apply_ABCD(
            self.model.MY.ABCD(self.model.MY.p1.i, self.model.MY.p2.o),
            target[self.model.MY.p1.i], 1, 1
        )
        target[self.model.MY.p2.i] = -target[self.model.MY.p2.o].conjugate()
        # BS p4
        target[self.model.BS.p4.o] = apply_ABCD(
            self.model.BS.ABCD(self.model.BS.p2.i, self.model.BS.p4.o),
            target[self.model.BS.p2.i], 1, 1
        )
        target[self.model.BS.p4.i] = -target[self.model.BS.p4.o].conjugate()

        for node, q in target.items():
            self.assertEqual(q, result[node])

if __name__ == '__main__':
    import unittest
    unittest.main()
