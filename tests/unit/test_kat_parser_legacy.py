"""Legacy (Finesse 2) kat file parser tests."""

from unittest import TestCase
from finesse.parse import get_parser


class KatParserTestCase(TestCase):
    """Resettable kat parser."""
    def setUp(self):
        self.parser = get_parser(finesse2=True)

    def reset(self):
        self.parser.reset()


class LaserTestCase(KatParserTestCase):
    """Laser parse tests."""
    def test_parse_laser_with_phase(self):
        """Test laser parsing with phase argument."""
        self.parser.parse("l l1 1 0 0 n1")
        self.assertDictEqual(self.parser.blocks[None]["lasers"][0],
                             {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"})

    def test_parse_laser_without_phase(self):
        """Test laser parsing without phase argument."""
        self.parser.parse("l l1 1 0 n1")
        self.assertDictEqual(self.parser.blocks[None]["lasers"][0],
                             {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"})


class SpaceTestCase(KatParserTestCase):
    """Space parse tests."""
    def test_parse_space_with_refractive_index(self):
        """Test space parsing with refractive index argument."""
        self.parser.parse("s s1 1 1.45 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["spaces"][0],
                             {"name": "s1", "L": 1, "n": 1.45, "node1": "n1", "node2": "n2"})

    def test_parse_space_without_refractive_index(self):
        """Test space parsing without refractive index argument."""
        self.parser.parse("s s1 1 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["spaces"][0],
                             {"name": "s1", "L": 1, "n": 1, "node1": "n1", "node2": "n2"})


class MirrorTestCase(KatParserTestCase):
    """Mirror parse tests."""
    def test_parse_mirror_reflectivity_transmissivity(self):
        """Test mirror parsing with R & T specified."""
        self.parser.parse("m m1 0.99 0.01 0 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["mirrors"][0],
                             {"name": "m1", "R": 0.99, "T": 0.01, "L": None,
                              "phi": 0, "node1": "n1", "node2": "n2"})

    def test_parse_mirror_reflectivity_loss(self):
        """Test mirror parsing with R & L specified."""
        self.parser.parse("m2 m1 0.99 0 0 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["mirrors"][0],
                             {"name": "m1", "R": 0.99, "T": None, "L": 0,
                              "phi": 0, "node1": "n1", "node2": "n2"})

    def test_parse_mirror_transmissivity_loss(self):
        """Test mirror parsing with T & L specified."""
        self.parser.parse("m1 m1 0.01 0 0 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["mirrors"][0],
                             {"name": "m1", "R": None, "T": 0.01, "L": 0,
                              "phi": 0, "node1": "n1", "node2": "n2"})

class BeamSplitterTestCase(KatParserTestCase):
    """Beam splitter parse tests."""
    def test_parse_beam_splitter_reflectivity_transmissivity(self):
        """Test beam splitter parsing with R & T specified."""
        self.parser.parse("bs bs1 0.99 0.01 0 45 n1 n2 n3 n4")
        self.assertDictEqual(self.parser.blocks[None]["beam_splitters"][0],
                             {"name": "bs1", "R": 0.99, "T": 0.01, "L": None,
                              "phi": 0, "alpha": 45,
                              "node1": "n1", "node2": "n2",
                              "node3": "n3", "node4": "n4"})

    def test_parse_beam_splitter_reflectivity_loss(self):
        """Test beam splitter parsing with R & L specified."""
        self.parser.parse("bs2 bs1 0.99 0 0 45 n1 n2 n3 n4")
        self.assertDictEqual(self.parser.blocks[None]["beam_splitters"][0],
                             {"name": "bs1", "R": 0.99, "T": None, "L": 0,
                              "phi": 0, "alpha": 45,
                              "node1": "n1", "node2": "n2",
                              "node3": "n3", "node4": "n4"})

    def test_parse_beam_splitter_transmissivity_loss(self):
        """Test beam splitter parsing with T & L specified."""
        self.parser.parse("bs1 bs1 0.01 0 0 45 n1 n2 n3 n4")
        self.assertDictEqual(self.parser.blocks[None]["beam_splitters"][0],
                             {"name": "bs1", "R": None, "T": 0.01, "L": 0,
                              "phi": 0, "alpha": 45,
                              "node1": "n1", "node2": "n2",
                              "node3": "n3", "node4": "n4"})


class DirectionalBeamSplitterTestCase(KatParserTestCase):
    """Directional beam splitter parse tests."""
    def test_parse_directional_beam_splitter(self):
        """Test directional beam splitter parsing."""
        self.parser.parse("dbs dbs1 n1 n2 n3 n4")
        self.assertDictEqual(self.parser.blocks[None]["directional_beam_splitters"][0],
                             {"name": "dbs1",
                              "node1": "n1", "node2": "n2",
                              "node3": "n3", "node4": "n4"})


class ModulatorTestCase(KatParserTestCase):
    """Modulator parse tests."""
    def test_parse_modulator_without_phase(self):
        """Test modulator parsing without phase argument."""
        self.parser.parse("mod EOM 100 0.1 3 am n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["modulators"][0],
                             {"name": "EOM", "f": 100, "midx": 0.1,
                              "order": 3, "type": "am", "phase": 0,
                              "node1": "n1", "node2": "n2"})

    def test_parse_modulator_with_phase(self):
        """Test modulator parsing with phase argument."""
        self.parser.parse("mod EOM 100 0.1 3 am 10 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["modulators"][0],
                             {"name": "EOM", "f": 100, "midx": 0.1,
                              "order": 3, "type": "am", "phase": 10,
                              "node1": "n1", "node2": "n2"})


class LensTestCase(KatParserTestCase):
    """Lens parse tests."""
    def test_parse_lens(self):
        """Test lens parsing."""
        self.parser.parse("lens lens1 1 n1 n2")
        self.assertDictEqual(self.parser.blocks[None]["lenses"][0],
                             {"name": "lens1", "f": 1,
                              "node1": "n1", "node2": "n2"})


class AmplitudeDetectorTestCase(KatParserTestCase):
    """Amplitude detector parse tests."""
    def test_parse_amplitude_detector_with_nm(self):
        """Test amplitude detector parsing with n, m specified."""
        self.parser.parse("ad ad1 1 1 0 n1")
        self.assertDictEqual(self.parser.blocks[None]["amplitude_detectors"][0],
                             {"name": "ad1", "n": 1, "m": 1, "f": 0, "node": "n1"})

    def test_parse_amplitude_detector_without_nm(self):
        """Test amplitude detector parsing without n, m specified."""
        self.parser.parse("ad ad1 0 n1")
        self.assertDictEqual(self.parser.blocks[None]["amplitude_detectors"][0],
                             {"name": "ad1", "n": None, "m": None, "f": 0, "node": "n1"})


class PhotoDetectorTestCase(KatParserTestCase):
    """Photo detector parse tests."""
    def test_parse_power_detector_pd_no_demod(self):
        """Test power detector parsing specified as pd with no demodulation."""
        self.parser.parse("pd pd1 n1")
        self.assertDictEqual(self.parser.blocks[None]["power_detectors"][0],
                             {"name": "pd1", "node": "n1"})

    def test_parse_power_detector_pd0_no_demod(self):
        """Test power detector parsing specified as pd0 with no demodulation."""
        self.parser.parse("pd0 pd1 n1")
        self.assertDictEqual(self.parser.blocks[None]["power_detectors"][0],
                             {"name": "pd1", "node": "n1"})

    def test_parse_power_detector_pd1_one_demod(self):
        """Test power detector parsing specified as pd1 with one demodulation."""
        self.parser.parse("pd1 pd1 100 0 n1")
        self.assertDictEqual(self.parser.blocks[None]["power_detectors"][0],
                             {"name": "pd1",
                              "f0": 100, "phase0": 0,
                              "node": "n1"})

    def test_parse_power_detector_pd1_one_demod_no_phase(self):
        """Test power detector parsing specified as pd1 with one demodulation and no phase."""
        self.parser.parse("pd1 pd1 100 n1")
        self.assertDictEqual(self.parser.blocks[None]["power_detectors"][0],
                             {"name": "pd1",
                              "f0": 100,
                              "node": "n1"})

    def test_parse_power_detector_pd2_two_demod(self):
        """Test power detector parsing specified as pd2 with two demodulations."""
        self.parser.parse("pd2 pd1 100 0 200 180 n1")
        self.assertDictEqual(self.parser.blocks[None]["power_detectors"][0],
                             {"name": "pd1",
                              "f0": 100, "phase0": 0,
                              "f1": 200, "phase1": 180,
                              "node": "n1"})

    def test_parse_power_detector_pd2_two_demod_no_phase(self):
        """Test power detector parsing specified as pd2 with two demodulations and no phase."""
        self.parser.parse("pd2 pd1 100 0 200 n1")
        self.assertDictEqual(self.parser.blocks[None]["power_detectors"][0],
                             {"name": "pd1",
                              "f0": 100, "phase0": 0,
                              "f1": 200,
                              "node": "n1"})


class CavityTestCase(KatParserTestCase):
    """Cavity parse tests."""
    def test_parse_cavity(self):
        """Test cavity parsing."""
        self.parser.parse("cav cav1 m1 n2 m2 n3")
        self.assertDictEqual(self.parser.blocks[None]["cavities"][0],
                             {"name": "cav1",
                              "component1": "m1", "node1": "n2",
                              "component2": "m2", "node2": "n3"})


class MultilineTestCase(KatParserTestCase):
    """Multiple line kat file parse tests."""
    def test_parse_multiline_single_pass(self):
        """Test multiple line parsing with a single pass."""
        self.parser.parse("""
            l l1 1 0 0 n1
            s s1 1 n1 n2
            m m1 1 0 0 n2 n3
        """)
        self.assertDictEqual(self.parser.blocks[None]["lasers"][0],
                             {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"})
        self.assertDictEqual(self.parser.blocks[None]["spaces"][0],
                             {"name": "s1", "L": 1, "n": 1, "node1": "n1", "node2": "n2"})
        self.assertDictEqual(self.parser.blocks[None]["mirrors"][0],
                             {"name": "m1", "R": 1, "T": 0, "L": None,
                              "phi": 0, "node1": "n2", "node2": "n3"})

    def test_parse_multiline_two_pass(self):
        """Test multiple line parsing with two calls to parser."""
        self.parser.parse("""
            l l1 1 0 0 n1
            s s1 1 n1 n2
        """)
        self.parser.parse("m m1 1 0 0 n2 n3")
        self.assertDictEqual(self.parser.blocks[None]["lasers"][0],
                             {"name": "l1", "P": 1, "f": 0, "phase": 0, "node": "n1"})
        self.assertDictEqual(self.parser.blocks[None]["spaces"][0],
                             {"name": "s1", "L": 1, "n": 1, "node1": "n1", "node2": "n2"})
        self.assertDictEqual(self.parser.blocks[None]["mirrors"][0],
                             {"name": "m1", "R": 1, "T": 0, "L": None,
                              "phi": 0, "node1": "n2", "node2": "n3"})


class ConstantTestCase(KatParserTestCase):
    """Constant parse tests."""
    def test_parse_constant(self):
        """Test const parsing."""
        self.parser.parse("""
            const power 100
            const name srm
        """)
        self.assertDictEqual(self.parser.blocks[None]["constants"],
                             {"$power": 100, "$name": "srm"})


class AttributeTestCase(KatParserTestCase):
    """Attribute parse tests."""
    def test_parse_attribute(self):
        """Test attribute parsing."""
        self.parser.parse("""
            attr m1 Rcx 10 Rcy 9
        """)
        self.assertDictEqual(self.parser.blocks[None]["attributes"],
                             {"m1": [("Rcx", 10), ("Rcy", 9)]})


class VariableTestCase(KatParserTestCase):
    """Variable parse tests."""
    def test_parse_variable(self):
        """Test variable parsing."""
        self.parser.parse("""
            variable power 100
        """)
        self.assertDictEqual(self.parser.blocks[None]["variables"],
                             {"power": 100})


class FunctionTestCase(KatParserTestCase):
    """Function parse tests."""
    def test_parse_function(self):
        """Test function parsing."""
        self.parser.parse("""
            func f = ($x - 2) * 4
            func g = ln($x+3) / (8 * $y)
        """)
        self.assertDictEqual(self.parser.blocks[None]["functions"],
                             {"f": "($x - 2) * 4",
                              "g": "ln($x+3) / (8 * $y)"})


class LockTestCase(KatParserTestCase):
    """Lock command parse tests."""
    def test_parse_lock(self):
        """Test lock command parsing."""
        self.parser.parse("""
            lock loki $DARM_err 10 1n
            lock* thor $mx1 -3 3u
        """)
        self.assertDictEqual(self.parser.blocks[None]["locks"][0],
                             {"name": "loki", "variable": "$DARM_err",
                              "gain": 10, "accuracy": 1e-9,
                              "starred": False})
        self.assertDictEqual(self.parser.blocks[None]["locks"][1],
                             {"name": "thor", "variable": "$mx1",
                              "gain": -3, "accuracy": 3e-6,
                              "starred": True})


class PutTestCase(KatParserTestCase):
    """Put command parse tests."""
    def test_parse_put(self):
        """Test put command parsing."""
        self.parser.parse("""
            put l1 P $x1
            put* m1 phi $mx1
        """)
        self.assertDictEqual(self.parser.blocks[None]["puts"][0],
                             {"component": "l1", "parameter": "P",
                              "variable": "$x1", "add": False})
        self.assertDictEqual(self.parser.blocks[None]["puts"][1],
                             {"component": "m1", "parameter": "phi",
                              "variable": "$mx1", "add": True})


class NumberTestCase(KatParserTestCase):
    """Number format parse tests."""
    def test_parse_number(self):
        """Test number format parsing."""
        test_cases = ["1", "31.425", "-1", "-64.132", "3e5", "5.3e6", "-5e7",
                      "3.1e2", "4p", "5.3u", "6m", "+7k", "8.5M", "4.1G",
                      "9.999T", "+inf"]
        results = [1, 31.425, -1, -64.132, 3e5, 5.3e6, -5e7,
                   3.1e2, 4e-12, 5.3e-6, 6e-3, 7e3, 8.5e6, 4.1e9,
                   9.999e12, float("+inf")]
        for case, result in zip(test_cases, results):
            self.reset()
            self.parser.parse("lens lens1 {} n1 n2".format(case))
            self.assertDictEqual(self.parser.blocks[None]["lenses"][0],
                                 {"name": "lens1", "f": result,
                                  "node1": "n1", "node2": "n2"})


class FTblockTestCase(KatParserTestCase):
    """FTblock parse tests."""
    def test_parse_FTblock_single_line(self):
        """Test FTblock parsing around a single kat file line."""
        self.parser.parse(r"""
            l l1 1 0 n1
            s s1 1 n1 n2

            %%% FTblock MIRROR
            m m1 1 0 0 n2 n3
            %%% FTend MIRROR
        """)
        self.assertDictEqual(self.parser.blocks[None]["lasers"][0],
                             {"name": "l1", "P": 1, "f": 0, "phase": 0,
                              "node": "n1"})
        self.assertDictEqual(self.parser.blocks[None]["spaces"][0],
                             {"name": "s1", "n": 1, "L": 1,
                              "node1": "n1", "node2": "n2"})
        self.assertDictEqual(self.parser.blocks["MIRROR"]["mirrors"][0],
                             {"name": "m1", "R": 1, "T": 0, "L": None,
                              "phi": 0, "node1": "n2", "node2": "n3"})


class ParserReuseTestCase(KatParserTestCase):
    """Recycling of single parse instance tests."""
    def test_parse_different_models(self):
        """Test parsing of different models with single parser instance."""
        ifo1 = """
            l l1 1 0 0 n1
            %%% FTblock mirror
            s s1 1 n1 n2
            m m1 1 0 0 n2 n3
            %%% FTend mirror
            """

        ifo2 = """
            l l1 2 0 0 n1
            s s1 3 n1 n2
            m m1 4 0 0 n2 n3
            """

        # parse first ifo
        self.parser.parse(ifo1)

        # parse second ifo with same parser, but with reset state
        self.reset()
        self.parser.parse(ifo2)
        res2a = self.parser.blocks

        # parse second ifo using a newly instantiated parser
        self.reset()
        self.parser.parse(ifo2)
        res2b = self.parser.blocks

        self.assertDictEqual(res2a, res2b)

    def test_parse_same_model(self):
        """Test parsing of same model in stages with single parser instance."""
        ifo1 = """
            l l1 1 0 0 n1
            """

        ifo2 = """
            %%% FTblock mirror
            s s1 1 n1 n2
            m m1 1 0 0 n2 n3
            %%% FTend mirror
            """

        # parse first and second parts together
        self.reset()
        self.parser.parse(ifo1 + ifo2)
        res1a = self.parser.blocks

        # parse first and second parts subsequently
        self.reset()
        self.parser.parse(ifo1)
        self.parser.parse(ifo2)
        res1b = self.parser.blocks

        self.assertDictEqual(res1a, res1b)


if __name__ == '__main__':
    import unittest
    unittest.main()

