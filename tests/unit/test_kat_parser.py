"""Kat file parser tests."""

from unittest import TestCase
from finesse.parse import get_parser, KatParserError


class KatParserTestCase(TestCase):
    """Resettable kat parser."""
    def setUp(self):
        self.parser = get_parser()

    def reset(self):
        self.parser.reset()

    def assert_ok(self, syntax):
        self.parser.parse(syntax)

    def assert_syntax_error(self, *args, **kwargs):
        self.assertRaises(KatParserError, self.parser.parse, *args, **kwargs)


class CommandStructureTestCase(KatParserTestCase):
    """Test the syntax structure of the parser, but not the parsed objects themselves."""
    def test_multi_line_on_single_line(self):
        """Test mutli-line parameters work on single lines too."""
        self.assert_ok("l l1 (1 0)")
        self.assert_ok("l l1 ( 1 0 )")
        self.assert_ok("l l1 (1 0 k1=1 k2=2)")

    def test_whitespace_doesnt_matter(self):
        """Test whitespace doesn't matter."""
        # Single-line.
        self.assert_ok("   l      l1    1     0   ")
        # Multi-line.
        self.assert_ok("   l      l1    (    1   0  )  ")
        self.assert_ok("""
           l l1 (
            1
            0
        )   """)

    def test_positional_args_only(self):
        """Test positional arguments only, i.e. Finesse 2 style."""
        # Single-line.
        self.assert_ok("l l1 1 0")
        self.assert_ok("l l1 1 0 1 n1")
        # Multi-line.
        self.assert_ok("l l1 (1 0)")
        self.assert_ok("l l1 (1 0 1 n1)")

    def test_keyword_args_only(self):
        """Test keyword arguments only."""
        # Single-line.
        self.assert_ok("l l1 k1=1")
        self.assert_ok("l l1 k1=1 k2=2")
        self.assert_ok("l l1 k1 = 1 k2=2")
        self.assert_ok("l l1 k1 = 1 k2 = 2")
        # Multi-line.
        self.assert_ok("l l1 (k1=1)")
        self.assert_ok("l l1 (k1=1 k2=2)")
        self.assert_ok("l l1 (k1 = 1 k2=2)")
        self.assert_ok("l l1 (k1 = 1 k2 = 2)")

    def test_positional_and_keyword_args(self):
        """Test both positional and keyword arguments."""
        # Single-line.
        self.assert_ok("l l1 1 k1=1")
        self.assert_ok("l l1 1 0 k1=1 k2=2")
        # Multi-line.
        self.assert_ok("l l1 (1 k1=1)")
        self.assert_ok("l l1 (1 0 k1=1 k2=2)")

    def test_no_name_invalid(self):
        """Test with no name."""
        # Single-line.
        self.assert_syntax_error("l 1")
        self.assert_syntax_error("l 1 l1")
        self.assert_syntax_error("l 1 k1=1")
        # Multi-line.
        self.assert_syntax_error("l (1)")
        self.assert_syntax_error("l (1 l1)")
        self.assert_syntax_error("l (1 k1=1)")

    def test_positional_after_keyword_args_invalid(self):
        """Test that specifying a positional argument after a keyword argument is invalid."""
        # Single-line.
        self.assert_syntax_error("l l1 1 0 k1=1 k2=2 0")
        self.assert_syntax_error("l l1 1 0 k1=1 k2=2 0 1 k3=3")
        self.assert_syntax_error("l l1 1 0 k1=1 k2=2 str")
        # Multi-line.
        self.assert_syntax_error("l l1 (1 0 k1=1 k2=2 0)")
        self.assert_syntax_error("l l1 (1 0 k1=1 k2=2 0 1 k3=3)")
        self.assert_syntax_error("l l1 (1 0 k1=1 k2=2 str)")


if __name__ == '__main__':
    import unittest
    unittest.main()

