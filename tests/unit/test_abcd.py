# pylint: disable=no-member

"""ABCD matrix result tests."""

import sys
from unittest import TestCase
import numpy as np

from finesse import Model
from finesse.components import Beamsplitter, Lens, Mirror, Nothing
from finesse.exceptions import NodeException, TotalReflectionError

if __name__ == "__main__":
    from abcd_analytic import *
else:
    from .abcd_analytic import *

# TODO: moved ModelTestCase into both test_abcd and test_beam_tracing for now
#       -- figure out correct way to import from tests/ so that BeST and local
#       testing both work later
class ModelTestCase(TestCase):
    """Resettable test case for model linked tests."""
    def setUp(self):
        self.reset()

    def reset(self):
        """Reset model."""
        self.model = Model()

class SpaceABCDTestCase(ModelTestCase):
    """Space component ABCD matrix tests."""
    def _construct_space(self, name, L, nr):
        self.model.add([Nothing("NULL1"), Nothing("NULL2")])
        self.model.connect(self.model.NULL1.p2, self.model.NULL2.p1, name=name, L=L, nr=nr)

    def test_space_abcd_refractive_index_unity_transmit(self):
        """Test space ABCD matrix for refractive index = 1."""
        # length = 1m, nr = 1.0
        L1 = 1.0
        nr = 1.0
        self._construct_space(name='S', L=L1, nr=nr)
        result = self.model.elements['S'].ABCD(self.model.NULL1.p2.o, self.model.NULL2.p1.i)
        target = abcd_space(L1, nr)
        self.assertListEqual(result.tolist(), target.tolist())
        # check that reversing propagation direction yields same result
        result_transmit_reversed = self.model.elements['S'].ABCD(self.model.NULL2.p1.o, self.model.NULL1.p2.i)
        self.assertListEqual(result_transmit_reversed.tolist(), target.tolist())

        # length = 34.5544 m, nr = 1.0
        L2 = 34.5544
        self.model.elements['S'].L = L2
        result_L2 = self.model.elements['S'].ABCD(self.model.NULL1.p2.o, self.model.NULL2.p1.i)
        target_L2 = abcd_space(L2, nr)
        self.assertListEqual(result_L2.tolist(), target_L2.tolist())

    def test_space_abcd_refractive_index_non_unity_transmit(self):
        """Test space ABCD matrix for refractive index != 1."""
        # length = 2m, nr = 1.44
        L = 2.0
        nr = 1.44
        self._construct_space(name='S', L=L, nr=nr)
        result = self.model.elements['S'].ABCD(self.model.NULL1.p2.o, self.model.NULL2.p1.i)
        target = abcd_space(L, nr)
        self.assertListEqual(result.tolist(), target.tolist())
        # check that reversing propagation direction yields same result
        result_transmit_reversed = self.model.elements['S'].ABCD(self.model.NULL2.p1.o, self.model.NULL1.p2.i)
        self.assertListEqual(result_transmit_reversed.tolist(), target.tolist())

    def test_space_abcd_reflect(self):
        """Test space ABCD matrix reflection."""
        self._construct_space(name='S', L=1.0, nr=1.0)
        self.assertRaises(
            NodeException,
            self.model.elements['S'].ABCD,
            self.model.NULL1.p2.o, self.model.NULL1.p2.i
        )


class LensABCDTestCase(ModelTestCase):
    """Lens component ABCD matrix tests."""
    def test_lens_abcd_transmit(self):
        """Test lens ABCD matrix on transmission."""
        # focal length = 11m
        f1 = 11.0
        lens = Lens("lens", f1)
        result = lens.ABCD(lens.p1.i, lens.p2.o)
        target = abcd_lens(f1)
        self.assertListEqual(result.tolist(), target.tolist())
        # check that reversing propagation direction yields same result
        result_transmit_reversed = lens.ABCD(lens.p2.i, lens.p1.o)
        self.assertListEqual(result_transmit_reversed.tolist(), target.tolist())

        # focal length = 34.5km
        f2 = 34.5e3
        lens.f = f2
        result_f2 = lens.ABCD(lens.p1.i, lens.p2.o)
        target_f2 = abcd_lens(f2)
        self.assertListEqual(result_f2.tolist(), target_f2.tolist())

    def test_lens_abcd_reflect(self):
        """Test lens ABCD matrix on reflection."""
        lens = Lens("lens", 1.0)
        self.assertRaises(NodeException, lens.ABCD, lens.p1.i, lens.p1.o)


class MirrorABCDTestCase(ModelTestCase):
    """Mirror component ABCD matrix tests."""
    def test_mirror_abcd_transmission_no_spaces(self):
        """Test mirror ABCD matrix on transmission with no attached spaces."""
        # radius of curvature = 2.5m, nr1 = nr2 = 1.0
        RoC = 2.5
        mirror = Mirror('M', Rc=RoC)
        result = mirror.ABCD(mirror.p1.i, mirror.p2.o)
        target = abcd_mirror_t(1.0, 1.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_transmit_reversed = mirror.ABCD(mirror.p2.i, mirror.p1.o)
        target_transmit_reversed = abcd_mirror_t(1.0, 1.0, -RoC)
        self.assertListEqual(result_transmit_reversed.tolist(), target_transmit_reversed.tolist())

        # radius of curvature = -12.6m, nr1 = nr2 = 1.0
        RoC = -12.6
        mirror.Rc = RoC
        result_neg_roc = mirror.ABCD(mirror.p1.i, mirror.p2.o)
        target_neg_roc = abcd_mirror_t(1.0, 1.0, RoC)
        self.assertListEqual(result_neg_roc.tolist(), target_neg_roc.tolist())

    def test_mirror_abcd_reflection_no_spaces(self):
        """Test mirror ABCD matrix on reflection with no attached spaces."""
        # radius of curvature = 7.7m, nr1 = nr2 = 1.0
        RoC = 7.7
        mirror = Mirror('M', Rc=RoC)
        result = mirror.ABCD(mirror.p1.i, mirror.p1.o)
        target = abcd_mirror_r(1.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_refl_reversed = mirror.ABCD(mirror.p2.i, mirror.p2.o)
        target_refl_reversed = abcd_mirror_r(1.0, -RoC)
        self.assertListEqual(result_refl_reversed.tolist(), target_refl_reversed.tolist())

        # radius of curvature = -5.5m, nr1 = nr2 = 1.0
        RoC = -5.5
        mirror.Rc = RoC
        result_neg_roc = mirror.ABCD(mirror.p1.i, mirror.p1.o)
        target_neg_roc = abcd_mirror_r(1.0, RoC)
        self.assertListEqual(result_neg_roc.tolist(), target_neg_roc.tolist())

    def test_mirror_abcd_transmission_space_nr_unity_at_port1(self):
        """Test mirror ABCD matrix on transmission with attached space (nr = 1) at first port."""
        # radius of curvature = 3.4m, nr1 = nr2 = 1.0
        RoC = 3.4
        self.model.chain(Nothing("NULL"), 1, Mirror("M", Rc=RoC))
        result = self.model.M.ABCD(self.model.M.p1.i, self.model.M.p2.o)
        target = abcd_mirror_t(1.0, 1.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

    def test_mirror_abcd_transmission_space_nr_nonunity_at_port1(self):
        """Test mirror ABCD matrix on transmission with attached space (nr != 1) at first port."""
        # radius of curvature = -37.99m, nr1 = 1.08, nr2 = 1.0
        RoC = -37.99
        nr1 = 1.08
        self.model.chain(Nothing("NULL"), {'L':1, "nr":nr1}, Mirror("M", Rc=RoC))
        result = self.model.M.ABCD(self.model.M.p1.i, self.model.M.p2.o)
        target = abcd_mirror_t(1.08, 1.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = self.model.M.ABCD(self.model.M.p2.i, self.model.M.p1.o)
        target_trns_reversed = abcd_mirror_t(1.0, nr1, -RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_mirror_abcd_reflection_space_nr_unity_at_port1(self):
        """Test mirror ABCD matrix on reflection with attached space (nr = 1) at first port."""
        # radius of curvature = 4.2m, nr1 = nr2 = 1.0
        RoC = 4.2
        self.model.chain(Nothing("NULL"), 1, Mirror("M", Rc=RoC))
        result = self.model.M.ABCD(self.model.M.p1.i, self.model.M.p1.o)
        target = abcd_mirror_r(1.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

    def test_mirror_abcd_reflection_space_nr_nonunity_at_port1(self):
        """Test mirror ABCD matrix on reflection with attached space (nr != 1) at first port."""
        # radius of curvature = -14.7m, nr1 = 1.3, nr2 = 1.0
        RoC = -14.7
        nr1 = 1.3
        self.model.chain(Nothing("NULL"), {'L':1, "nr":nr1}, Mirror("M", Rc=RoC))
        result = self.model.M.ABCD(self.model.M.p1.i, self.model.M.p1.o)
        target = abcd_mirror_r(nr1, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_refl_reversed = self.model.M.ABCD(self.model.M.p2.i, self.model.M.p2.o)
        target_refl_reversed = abcd_mirror_r(1.0, -RoC)
        self.assertListEqual(result_refl_reversed.tolist(), target_refl_reversed.tolist())

    def test_mirror_abcd_transmission_spaces_nr_nonunity(self):
        """Test mirror ABCD matrix on transmission - attached spaces at both ports with (nr!=1)."""
        # radius of curvature = 17.67m, nr1 = 1.04, nr2 = 1.45
        RoC = 17.67
        nr1 = 1.04
        nr2 = 1.45
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Mirror("M", Rc=RoC),
            {'L':1, "nr":nr2},
            Nothing("NULL2")
        )
        result = self.model.M.ABCD(self.model.M.p1.i, self.model.M.p2.o)
        target = abcd_mirror_t(nr1, nr2, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = self.model.M.ABCD(self.model.M.p2.i, self.model.M.p1.o)
        target_trns_reversed = abcd_mirror_t(nr2, nr1, -RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_mirror_abcd_reflection_spaces_nr_nonunity(self):
        """Test mirror ABCD matrix on reflection - attached spaces at both ports with (nr!=1)."""
        # radius of curvature = -0.51m, nr1 = 1.11, nr2 = 1.55
        RoC = -0.51
        nr1 = 1.11
        nr2 = 1.55
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Mirror("M", Rc=RoC),
            {'L':1, "nr":nr2},
            Nothing("NULL2")
        )
        result = self.model.M.ABCD(self.model.M.p1.i, self.model.M.p1.o)
        target = abcd_mirror_r(nr1, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_refl_reversed = self.model.M.ABCD(self.model.M.p2.i, self.model.M.p2.o)
        target_refl_reversed = abcd_mirror_r(nr2, -RoC)
        self.assertListEqual(result_refl_reversed.tolist(), target_refl_reversed.tolist())


class BeamsplitterABCDTestCase(ModelTestCase):
    """Beam splitter component ABCD matrix tests."""
    def test_bs_abcd_no_coupling(self):
        """Test that beam splitter ABCD method raises an exception when calling with nodes that have no coupling."""
        bs = Beamsplitter("BS")

        self.assertRaises(NodeException, bs.ABCD, bs.p1.i, bs.p4.o)
        self.assertRaises(NodeException, bs.ABCD, bs.p2.i, bs.p3.o)
        self.assertRaises(NodeException, bs.ABCD, bs.p3.i, bs.p2.o)
        self.assertRaises(NodeException, bs.ABCD, bs.p4.i, bs.p1.o)

    def test_flat_bs_abcd_transmission(self):
        """Test flat beam splitter ABCD matrix on transmission."""
        # radius of curvature = inf, nr1 = nr2 = 1.0
        bs = Beamsplitter("BS")
        result_t = bs.ABCD(bs.p1.i, bs.p3.o)
        target_t = abcd_beamsplitter_tt(1.0, 1.0, 0.0, 0.0, np.inf)
        self.assertListEqual(result_t.tolist(), target_t.tolist())

        result_s = bs.ABCD(bs.p1.i, bs.p3.o)
        target_s = abcd_beamsplitter_ts(1.0, 1.0, 0.0, 0.0, np.inf)
        self.assertListEqual(result_s.tolist(), target_s.tolist())

    def test_flat_bs_abcd_reflection(self):
        """Test flat beam splitter ABCD matrix on transmission."""
        # radius of curvature = inf, nr1 = nr2 = 1.0
        bs = Beamsplitter("BS")
        result_t = bs.ABCD(bs.p1.i, bs.p2.o)
        target_t = abcd_beamsplitter_rt(1.0, 0.0, np.inf)
        self.assertListEqual(result_t.tolist(), target_t.tolist())

        result_s = bs.ABCD(bs.p1.i, bs.p3.o)
        target_s = abcd_beamsplitter_rs(1.0, 0.0, np.inf)
        self.assertListEqual(result_s.tolist(), target_s.tolist())

    def test_bs_abcd_transmission_tangential_no_spaces(self):
        """Test beam splitter ABCD matrix on transmission for the tangential plane
        with no attached spaces."""
        # radius of curvature = 2.5m, nr1 = nr2 = 1.0
        RoC = 2.5
        bs = Beamsplitter("BS", Rc=RoC)
        result = bs.ABCD(bs.p1.i, bs.p3.o)
        target = abcd_beamsplitter_tt(1.0, 1.0, 0.0, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = bs.ABCD(bs.p3.i, bs.p1.o)
        target_trns_reversed = abcd_beamsplitter_tt(1.0, 1.0, 0.0, 0.0, -RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_bs_abcd_transmission_sagittal_no_spaces(self):
        """Test beam splitter ABCD matrix on transmission for the sagittal plane
        with no attached spaces."""
        # radius of curvature = 13.8m, nr1 = nr2 = 1.0
        RoC = 13.8
        bs = Beamsplitter("BS", Rc=RoC)
        result = bs.ABCD(bs.p1.i, bs.p3.o, direction='y')
        target = abcd_beamsplitter_ts(1.0, 1.0, 0.0, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = bs.ABCD(bs.p3.i, bs.p1.o, direction='y')
        target_trns_reversed = abcd_beamsplitter_ts(1.0, 1.0, 0.0, 0.0, -RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_bs_abcd_reflection_tangential_no_spaces(self):
        """Test beam splitter ABCD matrix on reflection for the tangential plane
        with no attached spaces."""
        # radius of curvature = -18.9m, nr1 = nr2 = 1.0
        RoC = -18.9
        bs = Beamsplitter("BS", Rc=RoC)
        result = bs.ABCD(bs.p1.i, bs.p2.o)
        target = abcd_beamsplitter_rt(1.0, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_refl_reversed = bs.ABCD(bs.p2.i, bs.p1.o)
        target_refl_reversed = abcd_beamsplitter_rt(1.0, 0.0, RoC)
        self.assertListEqual(result_refl_reversed.tolist(), target_refl_reversed.tolist())

    def test_bs_abcd_reflection_sagittal_no_spaces(self):
        """Test beam splitter ABCD matrix on reflection for the sagittal plane
        with no attached spaces."""
        # radius of curvature = 1.6m, nr1 = nr2 = 1.0
        RoC = 1.6
        bs = Beamsplitter("BS", Rc=RoC)
        result = bs.ABCD(bs.p1.i, bs.p2.o, direction='y')
        target = abcd_beamsplitter_rs(1.0, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_refl_reversed = bs.ABCD(bs.p2.i, bs.p1.o, direction='y')
        target_refl_reversed = abcd_beamsplitter_rs(1.0, 0.0, RoC)
        self.assertListEqual(result_refl_reversed.tolist(), target_refl_reversed.tolist())

    def test_bs_abcd_transmission_tangential_spaces(self):
        """Test beam splitter ABCD matrix on transmission for the tangential plane
        with attached spaces."""
        # radius of curvature = 19.94m, nr1 = 1.04, nr2 = 1.45
        RoC = 19.94
        nr1 = 1.04
        nr2 = 1.45
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Beamsplitter("BS", Rc=RoC)
        )
        self.model.add(Nothing("NULL2"))
        self.model.connect(self.model.BS.p3, self.model.NULL2.p1, L=1, nr=nr2)
        result = self.model.BS.ABCD(self.model.BS.p1.i, self.model.BS.p3.o)
        target = abcd_beamsplitter_tt(nr1, nr2, 0.0, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = self.model.BS.ABCD(self.model.BS.p3.i, self.model.BS.p1.o)
        target_trns_reversed = abcd_beamsplitter_tt(nr1, nr2, 0.0, 0.0, -RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_bs_abcd_transmission_sagittal_spaces(self):
        """Test beam splitter ABCD matrix on transmission for the sagittal plane
        with attached spaces."""
        # radius of curvature = -9m, nr1 = 1.33, nr2 = 1.044
        RoC = -9.0
        nr1 = 1.33
        nr2 = 1.044
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Beamsplitter("BS", Rc=RoC)
        )
        self.model.add(Nothing("NULL2"))
        self.model.connect(self.model.BS.p3, self.model.NULL2.p1, L=1, nr=nr2)
        result = self.model.BS.ABCD(self.model.BS.p1.i, self.model.BS.p3.o, direction='y')
        target = abcd_beamsplitter_ts(nr1, nr2, 0.0, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = self.model.BS.ABCD(self.model.BS.p3.i, self.model.BS.p1.o, direction='y')
        target_trns_reversed = abcd_beamsplitter_tt(nr1, nr2, 0.0, 0.0, -RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_bs_abcd_reflection_tangential_spaces(self):
        """Test beam splitter ABCD matrix on reflection for the tangential plane
        with attached spaces."""
        # radius of curvature = 10.66m, nr1 = 1.07, nr2 = 1.70
        RoC = 10.66
        nr1 = 1.07
        nr2 = 1.70
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Beamsplitter("BS", Rc=RoC)
        )
        self.model.add(Nothing("NULL2"))
        self.model.connect(self.model.BS.p2, self.model.NULL2.p1, L=1, nr=nr2)
        result = self.model.BS.ABCD(self.model.BS.p1.i, self.model.BS.p2.o)
        target = abcd_beamsplitter_rt(nr1, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = self.model.BS.ABCD(self.model.BS.p2.i, self.model.BS.p1.o)
        target_trns_reversed = abcd_beamsplitter_rt(nr2, 0.0, RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_bs_abcd_reflection_sagittal_spaces(self):
        """Test beam splitter ABCD matrix on reflection for the sagittal plane
        with attached spaces."""
        # radius of curvature = 10.66m, nr1 = 1.07, nr2 = 1.70
        RoC = 10.66
        nr1 = 1.07
        nr2 = 1.70
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Beamsplitter("BS", Rc=RoC)
        )
        self.model.add(Nothing("NULL2"))
        self.model.connect(self.model.BS.p2, self.model.NULL2.p1, L=1, nr=nr2)
        result = self.model.BS.ABCD(self.model.BS.p1.i, self.model.BS.p2.o, direction='y')
        target = abcd_beamsplitter_rs(nr1, 0.0, RoC)
        self.assertListEqual(result.tolist(), target.tolist())

        # check reversed propagation
        result_trns_reversed = self.model.BS.ABCD(self.model.BS.p2.i, self.model.BS.p1.o)
        target_trns_reversed = abcd_beamsplitter_rs(nr2, 0.0, RoC)
        self.assertListEqual(result_trns_reversed.tolist(), target_trns_reversed.tolist())

    def test_bs_abcd_total_reflection(self):
        """Test beam splitter ABCD matrix with angles and refractive indices configured for total reflection."""
        nr1 = 1.45
        nr2 = 1.0
        alpha1 = 45.0
        self.model.chain(
            Nothing("NULL1"),
            {'L':1, "nr":nr1},
            Beamsplitter("BS", alpha=alpha1)
        )
        self.model.add(Nothing("NULL2"))
        self.model.connect(self.model.BS.p2, self.model.NULL2.p1, L=1, nr=nr2)

        self.assertRaises(
            TotalReflectionError,
            self.model.BS.ABCD,
            self.model.BS.p1.i, self.model.BS.p2.o
        )

if __name__ == '__main__':
    import unittest
    unittest.main()

